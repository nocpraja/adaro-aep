const Encore = require('@symfony/webpack-encore');

Encore
// the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the assets directory
    .setPublicPath('/adaro-aep-v2/build')
    .setManifestKeyPrefix('build/')

    .cleanupOutputBeforeBuild()
    .enableSingleRuntimeChunk()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .configureBabel(function (babelConfig) {
        babelConfig.plugins.push('@babel/plugin-transform-runtime');
    })

    // define the assets of the project
    .addEntry('admin-page', './assets/js/pages/AdminPage.js')
    .addStyleEntry('bootstrap-md', [
        'bootstrap/dist/css/bootstrap.min.css', './assets/scss/bootstrap.scss'
    ])
    .addStyleEntry('theme/greenish', './assets/scss/theme/greenish.scss')
    .addStyleEntry('login-page', './assets/scss/LoginPage.scss')
    .addStyleEntry('default-page', './assets/scss/DefaultPage.scss')

    // define other configurations
    .splitEntryChunks()
    .enableSassLoader()
    .enableVueLoader()
;

module.exports = Encore.getWebpackConfig();
