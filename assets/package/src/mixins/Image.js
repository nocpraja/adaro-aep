import Util from '../utils/Helper';

export default {
    props: {
        circle: Boolean,
        rounded: Boolean,
        imgSrc: String,
        size: {
            type: [Number, String, Object],
            default: 48
        }
    },
    computed: {
        imageClass() {
            return {
                'rounded-circle': this.circle || (!this.circle && !this.rounded),
                'rounded': this.rounded && !this.circle,
            }
        },
        imageSizeStyles() {
            if (!this.size) {
                return null;
            }
            const primitive = typeof this.size === 'string' || typeof this.size === 'number';

            return {
                height: primitive ? Util.sizeUnit(this.size) : Util.sizeUnit(this.size.height),
                width: primitive ? Util.sizeUnit(this.size) : Util.sizeUnit(this.size.width)
            }
        }
    }
}
