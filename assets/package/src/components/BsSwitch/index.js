import BsSwitch from "./BsSwitch";

export default Vue => {
    Vue.component(BsSwitch.name, BsSwitch);
};
