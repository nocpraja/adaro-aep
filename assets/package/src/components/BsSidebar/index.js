import BsSidebar from "./BsSidebar";
import BsSidebarNav from "./BsSidebarNav";
import BsSidebarNavItem from "./BsSidebarNavItem";

export default Vue => {
    Vue.component(BsSidebar.name, BsSidebar);
    Vue.component(BsSidebarNav.name, BsSidebarNav);
    Vue.component(BsSidebarNavItem.name, BsSidebarNavItem);
};