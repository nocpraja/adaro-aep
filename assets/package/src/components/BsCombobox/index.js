import BsCombobox from "./BsCombobox";

export default Vue => {
    Vue.component(BsCombobox.name, BsCombobox);
};
