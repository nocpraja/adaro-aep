import BsAlert from "./BsAlert";
import BsAvatar from "./BsAvatar";
import BsBadge from "./BsBadge";
import BsDivider from "./BsDivider";
import BsSubheader from "./BsSubheader";

export default Vue => {
    Vue.component(BsAlert.name, BsAlert);
    Vue.component(BsAvatar.name, BsAvatar);
    Vue.component(BsBadge.name, BsBadge);
    Vue.component(BsDivider.name, BsDivider);
    Vue.component(BsSubheader.name, BsSubheader);
};