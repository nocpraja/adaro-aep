import BsRadio from "./BsRadio";
import BsRadioGroup from "./BsRadioGroup";

export default Vue => {
    Vue.component(BsRadio.name, BsRadio);
    Vue.component(BsRadioGroup.name, BsRadioGroup);
};
