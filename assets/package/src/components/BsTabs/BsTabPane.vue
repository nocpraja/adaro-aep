<template>
    <transition :name="transition">
        <div v-show="active" :class="classNames" v-bind="attributes">
            <slot></slot>
        </div>
    </transition>
</template>

<script>
    export default {
        name: "BsTabPane",
        inject: ['tabs'],
        props: {
            id: String,
            tabref: String
        },
        data: () => ({
            isActive: false,
            itemIndex: -1
        }),
        computed: {
            transition() {
                return this.tabs.transition;
            },
            /**
             * Get computed binding's attributes.
             *
             * @return {Object}
             */
            attributes() {
                return {
                    'id': this.id,
                    'role': 'tabpanel',
                    'aria-labelledby': this.tabref
                }
            },
            /**
             * Get computed component's class names.
             *
             * @return {Object}
             */
            classNames() {
                return {
                    'tab-pane': true,
                    'active': this.active
                }
            },
            active: {
                get() {
                    return this.isActive;
                },
                set(value) {
                    this.isActive = value;
                }
            }
        },
        created() {
            const idx      = this.tabs.register(this);
            this.itemIndex = idx - 1;
        },
        beforeDestroy() {
            this.tabs.unregister(this.itemIndex);
        }
    }
</script>

<style scoped>

</style>