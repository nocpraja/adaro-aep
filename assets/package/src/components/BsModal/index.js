import BsModal from "./BsModal";

export default Vue => {
    Vue.component(BsModal.name, BsModal);
};