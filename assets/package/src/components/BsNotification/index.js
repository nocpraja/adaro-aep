import BsNotification from "./BsNotification";

BsNotification.install = function (Vue, options) {
    let myComponent = Vue.extend({
        template: '<bs-notification ref="notification"></bs-notification>',
        components: {BsNotification}
    });

    const component = new myComponent().$mount();
    document.body.appendChild(component.$el);
    Vue.prototype.$notification = component.$refs.notification;
};

export default BsNotification;
