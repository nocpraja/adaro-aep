import BsTextField from "./BsTextField";
import BsTextArea from "./BsTextArea";
import BsNumberField from "./BsNumberField";
import BsDateTimeField from "./BsDateTimeField";
import BsSearchField from "./BsSearchField";

export default Vue => {
    Vue.component(BsTextField.name, BsTextField);
    Vue.component(BsTextArea.name, BsTextArea);
    Vue.component(BsNumberField.name, BsNumberField);
    Vue.component(BsDateTimeField.name, BsDateTimeField);
    Vue.component(BsSearchField.name, BsSearchField);
};
