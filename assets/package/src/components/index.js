import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';

import BsAnimation from './BsAnimation';
import BsBasic from './BsBasic';
import BsButton from './BsButton';
import BsCard from './BsCard';
import BsCheckbox from './BsCheckbox';
import BsCombobox from './BsCombobox';
import BsField from './BsField';
import BsGrid from './BsGrid';
import BsPagination from './BsPagination';
import BsIcon from './BsIcon';
import BsList from './BsList';
import BsMenu from './BsMenu';
import BsModal from './BsModal';
import BsNotification from './BsNotification';
import BsPicker from './BsPicker';
import BsPopover from './BsPopover';
import BsRadio from './BsRadio';
import BsSidebar from './BsSidebar';
import BsSwitch from './BsSwitch';
import BsTabs from './BsTabs';
import BsTooltip from './BsTooltip';

library.add(fas);

export {
    BsAnimation,
    BsBasic,
    BsButton,
    BsCard,
    BsCheckbox,
    BsCombobox,
    BsField,
    BsGrid,
    BsIcon,
    BsList,
    BsMenu,
    BsModal,
    BsNotification,
    BsPagination,
    BsPicker,
    BsPopover,
    BsRadio,
    BsSidebar,
    BsSwitch,
    BsTabs,
    BsTooltip
};
