import BsCheckbox from "./BsCheckbox";

export default Vue => {
    Vue.component(BsCheckbox.name, BsCheckbox);
};
