import BsIcon from "./BsIcon";
import BsIconEyeToggle from "./BsIconEyeToggle";

export default Vue => {
    Vue.component(BsIcon.name, BsIcon);
    Vue.component(BsIconEyeToggle.name, BsIconEyeToggle);
};
