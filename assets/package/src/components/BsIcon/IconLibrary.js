const library = [
    {
        name: 'Add',
        class: 'icon-add',
        aliases: ['add'],
        paths: [
            {d: 'M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'}
        ]
    },
    {
        name: 'ArrowBack',
        class: 'icon-arrow-back',
        aliases: ['arrow-back', 'arrow_back'],
        paths: [
            {d: 'M0 0h24v24H0z', fill: 'none'},
            {d: 'M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z', fill: 'currentColor'}
        ]
    },
    {
        name: 'ArrowForward',
        class: 'icon-arrow-forward',
        aliases: ['arrow-forward', 'arrow_forward'],
        paths: [
            {d: 'M0 0h24v24H0z', fill: 'none'},
            {d: 'M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z', fill: 'currentColor'}
        ]
    },
    {
        name: 'ArrowLeft',
        class: 'icon-arrow-left',
        aliases: ['arrow-left', 'arrow_left'],
        paths: [
            {d: 'M14 7l-5 5 5 5V7z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'}
        ]
    },
    {
        name: 'ArrowRight',
        class: 'icon-arrow-right',
        aliases: ['arrow-right', 'arrow_right'],
        paths: [
            {d: 'M10 17l5-5-5-5v10z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'}
        ]
    },
    {
        name: 'Calendar',
        class: 'icon-calendar',
        aliases: ['date-range', 'date_range'],
        paths: [
            {d: 'M9 11H7v2h2v-2zm4 0h-2v2h2v-2zm4 0h-2v2h2v-2zm2-7h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V9h14v11z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'}
        ]
    },
    {
        name: 'ChevronLeft',
        class: 'icon-chevron-left',
        aliases: ['chevron-left', 'chevron_left', 'keyboard-arrow-left', 'keyboard_arrow_left', 'navigate-before', 'navigate_before'],
        paths: [
            {d: 'M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'}
        ]
    },
    {
        name: 'ChevronRight',
        class: 'icon-chevron-right',
        aliases: ['chevron-right', 'chevron_right', 'keyboard-arrow-right', 'keyboard_arrow_right', 'navigate-next', 'navigate_next'],
        paths: [
            {d: 'M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'}
        ]
    },
    {
        name: 'Clear',
        class: 'icon-clear',
        aliases: ['close', 'clear'],
        paths: [
            {d: 'M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'}
        ]
    },
    {
        name: 'Dashboard',
        class: 'icon-dashboard',
        aliases: ['dashboard'],
        paths: [
            {d: 'M0 0h24v24H0z', fill: 'none'},
            {d: 'M3 13h8V3H3v10zm0 8h8v-6H3v6zm10 0h8V11h-8v10zm0-18v6h8V3h-8z', fill: 'currentColor'},
        ]
    },
    {
        name: 'ExpandLess',
        class: 'icon-expand-less',
        aliases: ['expand-less', 'expand_less', 'chevron-up', 'chevron_up', 'keyboard-arrow-up', 'keyboard_arrow_up'],
        paths: [
            {d: 'M12 8l-6 6 1.41 1.41L12 10.83l4.59 4.58L18 14z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'},
        ]
    },
    {
        name: 'ExpandMore',
        class: 'icon-expand-more',
        aliases: ['expand-more', 'expand_more', 'chevron-down', 'chevron_down', 'keyboard-arrow-down', 'keyboard_arrow_down'],
        paths: [
            {d: 'M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0z', fill: 'none'},
        ]
    },
    {
        name: 'FirstPage',
        class: 'icon-first-page',
        aliases: ['first-page', 'first_page'],
        paths: [
            {d: 'M18.41 16.59L13.82 12l4.59-4.59L17 6l-6 6 6 6zM6 6h2v12H6z', fill: 'currentColor'},
            {d: 'M24 24H0V0h24v24z', fill: 'none'},
        ]
    },
    {
        name: 'LastPage',
        class: 'icon-last-page',
        aliases: ['last-page', 'last_page'],
        paths: [
            {d: 'M5.59 7.41L10.18 12l-4.59 4.59L7 18l6-6-6-6zM16 6h2v12h-2z', fill: 'currentColor'},
            {d: 'M0 0h24v24H0V0z', fill: 'none'},
        ]
    },
    {
        name: 'MenuBars',
        class: 'icon-menu-bars',
        aliases: ['menu-bars', 'menu_bars', 'menu'],
        paths: [
            {d: 'M0 0h24v24H0V0z', fill: 'none'},
            {d: 'M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z', fill: 'currentColor'},
        ]
    },
    {
        name: 'MoreHoriz',
        class: 'icon-more-horiz',
        aliases: ['more-horiz', 'more_horiz'],
        paths: [
            {d: 'M0 0h24v24H0V0z', fill: 'none'},
            {d: 'M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z', fill: 'currentColor'},
        ]
    },
    {
        name: 'MoreVert',
        class: 'icon-more-vert',
        aliases: ['more-vert', 'more_vert'],
        paths: [
            {d: 'M0 0h24v24H0V0z', fill: 'none'},
            {d: 'M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z', fill: 'currentColor'},
        ]
    },
];

export default library;
