const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const devMode = process.env.NODE_ENV !== 'production';

const babelLoader = [{
  loader: "babel-loader",
  options: {
    presets: [
      ["@babel/preset-env", {
        targets: {
          browsers: [
            "> 5%",
            "last 2 versions",
            "not ie <= 9"
          ]
        },
        corejs: "3",
        modules: "commonjs",
        useBuiltIns: "entry"
      }]
    ],
    plugins: [
      "@babel/plugin-syntax-dynamic-import"
    ]
  }
}];

const sassLoader = [
  'vue-style-loader',
  MiniCssExtractPlugin.loader,
  'css-loader',
  'sass-loader',
];

const cssLoader = [
  'vue-style-loader',
  MiniCssExtractPlugin.loader,
  'css-loader',
];

const config = {
  mode: devMode ? 'development' : 'production',
  context: path.resolve('./'),
  entry: {
    'vue-bs4': './src/index.js',
  },
  resolve: {
    extensions: [".js", ".jsx", ".vue", ".scss", ".css"],
    alias: {
      'axios': path.resolve(__dirname, './node_modules/axios'),
      'body-scroll-lock': path.resolve(__dirname, './node_modules/body-scroll-lock'),
      'lodash': path.resolve(__dirname, './node_modules/lodash'),
      'resize-observer-polyfill': path.resolve(__dirname, './node_modules/resize-observer-polyfill'),
      'vue': path.resolve(__dirname, './node_modules/vue'),
      '@fortawesome/fontawesome-svg-core': path.resolve(__dirname, './node_modules/@fortawesome/fontawesome-svg-core'),
      '@fortawesome/free-solid-svg-icons': path.resolve(__dirname, './node_modules/@fortawesome/free-solid-svg-icons'),
      '@fortawesome/vue-fontawesome': path.resolve(__dirname, './node_modules/@fortawesome/vue-fontawesome'),
    }
  },
  externals: {
    'axios': {
      commonjs: 'axios',
      commonjs2: 'axios',
      amd: 'axios',
      root: 'axios'
    },
    'body-scroll-lock': {
      commonjs: 'body-scroll-lock',
      commonjs2: 'body-scroll-lock',
      amd: 'body-scroll-lock',
      root: 'body-scroll-lock'
    },
    'lodash': {
      commonjs: 'lodash',
      commonjs2: 'lodash',
      amd: 'lodash',
      root: '_'
    },
    'resize-observer-polyfill': {
      commonjs: 'resize-observer-polyfill',
      commonjs2: 'resize-observer-polyfill',
      amd: 'resize-observer-polyfill',
      root: 'resize-observer-polyfill'
    },
    'vue': {
      commonjs: 'vue',
      commonjs2: 'vue',
      amd: 'vue',
      root: 'vue'
    },
    '@fortawesome/fontawesome-svg-core': {
      commonjs: '@fortawesome/fontawesome-svg-core',
      commonjs2: '@fortawesome/fontawesome-svg-core',
      amd: '@fortawesome/fontawesome-svg-core',
      root: '@fortawesome/fontawesome-svg-core'
    },
    '@fortawesome/free-solid-svg-icons': {
      commonjs: '@fortawesome/free-solid-svg-icons',
      commonjs2: '@fortawesome/free-solid-svg-icons',
      amd: '@fortawesome/free-solid-svg-icons',
      root: '@fortawesome/free-solid-svg-icons'
    },
    '@fortawesome/vue-fontawesome': {
      commonjs: '@fortawesome/vue-fontawesome',
      commonjs2: '@fortawesome/vue-fontawesome',
      amd: '@fortawesome/vue-fontawesome',
      root: '@fortawesome/vue-fontawesome'
    },
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: 'vue-bs4',
    libraryTarget: 'commonjs2'
  },
  optimization: {
    minimizer: [
      new TerserPlugin()
    ],
    mangleWasmImports: true,
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  module: {
    rules: [
      {
        // match .js and .jsx
        test: /\.jsx?$/,
        // exclude: /node_modules/,
        use: babelLoader
      },
      {
        test: /\.css$/,
        use: cssLoader
      },
      {
        test: /\.s[ac]ss$/,
        use: sassLoader
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: devMode ? '"development"' : '"production"',
        BABEL_ENV: devMode ? '"development"' : '"production"',
        PRODUCTION: devMode === false
      }
    }),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: devMode ? '[name].css' : '[name].min.css'
    })
  ]
};

module.exports = config;
