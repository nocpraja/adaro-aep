import Vue from "vue";
import Vuex from "vuex";
import modules from "./modules";
import { AxiosPlugin } from "vue-bs4";
import { BASE_URL } from "./modules/mutation-types";

Vue.use(Vuex);
Vue.use(AxiosPlugin, {baseURL: BASE_URL + 'app'});

export default new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production', // true,
    modules
});
