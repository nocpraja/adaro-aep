import Vue from "vue";
import showApiError from "./ApiError";
import {
    ASSIGN_VALUE,
    ASSIGN_VALUES,
    FETCH,
    GET_USER_EMAIL,
    GET_USER_FULLNAME,
    GET_USER_GROUP_NAME,
    GET_USER_PROFILE,
    SET_USER_EMAIL,
    SET_USER_FULLNAME,
    SET_USER_GROUP_NAME,
    SET_USER_PROFILE
} from "./mutation-types";

const state = {
    profile: {
        // username: null,
        fullName: null,
        email: null,
        groupName: null,
        lastVisited: null
    }
};

const actions = {
    async [FETCH]({commit}) {
        try {
            const response = await Vue.prototype.$http.get('/user/profile');
            commit(ASSIGN_VALUES, response.data.data);
            commit(SET_USER_GROUP_NAME, response.data.data.group.groupName);
        } catch (e) {
            showApiError(e, 'User Profile');
        }
    }
};

const mutations = {
    [ASSIGN_VALUE](state, payload) {
        if (state.profile.hasOwnProperty(payload.name)) {
            state.profile[payload.name] = payload.value;
        }
    },
    [ASSIGN_VALUES](state, payload) {
        state.profile.email       = payload.email;
        state.profile.fullName    = payload.fullName;
        state.profile.groupName   = payload.groupName;
        state.profile.lastVisited = payload.lastVisited;
    },
    [SET_USER_PROFILE](state, payload) {
        state.profile.email       = payload.email;
        state.profile.fullName    = payload.fullName;
        state.profile.groupName   = payload.groupName;
        state.profile.lastVisited = payload.lastVisited;
    },
    [SET_USER_EMAIL](state, email) {
        state.profile.email = email;
    },
    [SET_USER_FULLNAME](state, name) {
        state.profile.fullName = name;
    },
    [SET_USER_GROUP_NAME](state, name) {
        state.profile.groupName = name;
    },
};

const getters = {
    [GET_USER_PROFILE]: state => state.profile,
    [GET_USER_EMAIL]: state => state.profile.email,
    [GET_USER_FULLNAME]: state => state.profile.fullName,
    [GET_USER_GROUP_NAME]: state => state.profile.groupName,
};

export default {
    namespaced: true,
    state, actions, mutations, getters
}
