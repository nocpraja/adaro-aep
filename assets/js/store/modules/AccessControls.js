import Vue from "vue";
import showApiError from "./ApiError";
import {
    ADD_ITEM,
    CLEAR_COLLECTION,
    FETCH_COLLECTION,
    FIND_ACL,
    GET_COLLECTION,
    GET_ROLES,
    SET_ROLES
} from "./mutation-types";

function createItem(obj, parent = null) {
    return {
        id: obj.id,
        menuLabel: obj.menuLabel,
        routeUrl: obj.routeUrl,
        routeAlias: obj.routeAlias,
        accessRules: obj.accessRules,
        enabled: obj.enabled,
        leaf: obj.leaf,
        parent: parent
    }
}

function populateNodes(fn, nodes, parent = null) {
    for (const node of nodes) {
        const item = createItem(node, parent);
        fn(ADD_ITEM, item);

        if (node.leaf === false || node.children.length > 0) {
            populateNodes(fn, node.children, item);
        }
    }
}

const state = {
    lists: [],
    roles: []
};

const actions = {
    async [FETCH_COLLECTION]({commit}) {
        try {
            const response = await Vue.prototype.$http.get('/service/access-rights');
            commit(CLEAR_COLLECTION);
            commit(SET_ROLES, response.data.data.roles);
            populateNodes(commit, response.data.data.lists, null);
        } catch (e) {
            showApiError(e, 'hak akses');
        }
    }
};

const mutations = {
    [ADD_ITEM](state, item) {
        state.lists.push(item);
    },
    [CLEAR_COLLECTION](state) {
        state.lists = [];
        state.roles = [];
    },
    [SET_ROLES](state, roles) {
        state.roles = roles;
    }
};

const getters = {
    [GET_COLLECTION]: state => state.lists,
    [GET_ROLES]: state => state.roles,
    [FIND_ACL]: state => (value) => {
        return state.lists.find(item => {
            return item.id === value ||
                (item.routeAlias !== null && item.routeAlias === value.toString()) ||
                (item.routeUrl !== null && item.routeUrl === value.toString()) ||
                (item.routeUrl !== null && value.toString().startsWith(item.routeUrl)) ||
                item.menuLabel === value.toString()
        })
    }
};

export default {
    namespaced: true,
    state, actions, mutations, getters
}
