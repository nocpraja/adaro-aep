import {
    GET_ACTIVE_INSTITUSI,
    GET_ACTIVE_PRGCSR,
    GET_ACTIVE_PRGNAME,
    GET_ACTIVE_PRGSTATE,
    GET_ACTIVE_PROGRAM,
    GET_ACTIVE_YEAR,
    SET_ACTIVE_INSTITUSI,
    SET_ACTIVE_PRGCSR,
    SET_ACTIVE_PRGNAME,
    SET_ACTIVE_PRGSTATE,
    SET_ACTIVE_PROGRAM,
    SET_ACTIVE_YEAR
} from "./mutation-types";

const state = {
    institusi: null,
    programCsr: null,
    programId: null,
    programName: null,
    programStatus: null,
    tahun: null
};

const mutations = {
    [SET_ACTIVE_PROGRAM](state, program) {
        state.programId = program;
    },
    [SET_ACTIVE_PRGNAME](state, name) {
        state.programName = name;
    },
    [SET_ACTIVE_PRGCSR](state, csr) {
        state.programCsr = csr;
    },
    [SET_ACTIVE_PRGSTATE](state, status) {
        state.programStatus = status;
    },
    [SET_ACTIVE_INSTITUSI](state, institusi) {
        state.institusi = institusi;
    },
    [SET_ACTIVE_YEAR](state, tahun) {
        state.tahun = tahun;
    }
};

const getters = {
    [GET_ACTIVE_PRGCSR]: state => state.programCsr,
    [GET_ACTIVE_PROGRAM]: state => state.programId,
    [GET_ACTIVE_PRGNAME]: state => state.programName,
    [GET_ACTIVE_PRGSTATE]: state => state.programStatus,
    [GET_ACTIVE_INSTITUSI]: state => state.institusi,
    [GET_ACTIVE_YEAR]: state => state.tahun
};

export default {
    namespaced: true,
    state, mutations, getters
}
