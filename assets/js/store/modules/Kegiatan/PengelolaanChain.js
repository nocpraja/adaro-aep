import {
    GET_ACTIVE_BATCH,
    GET_ACTIVE_KEGIATAN,
    GET_ACTIVE_KEGIATAN_NAME,
    GET_ACTIVE_KEGIATAN_STATE,
    GET_ACTIVE_PRGNAME,
    GET_ACTIVE_PRGSTATE,
    GET_ACTIVE_PROGRAM,
    GET_ACTIVE_YEAR,
    GET_MGT_VALUE,
    SET_ACTIVE_BATCH,
    SET_ACTIVE_KEGIATAN,
    SET_ACTIVE_KEGIATAN_NAME,
    SET_ACTIVE_KEGIATAN_STATE,
    SET_ACTIVE_PRGNAME,
    SET_ACTIVE_PRGSTATE,
    SET_ACTIVE_PROGRAM,
    SET_ACTIVE_YEAR,
    SET_MGT_VALUE
} from "./mutation-types";

const state = {
    tahun: null,
    programId: null,
    programName: null,
    programStatus: null,
    kegiatanId: null,
    kegiatanName: null,
    kegiatanState: null,
    batchName: null,
    mgtValue: 5
};

const mutations = {
    [SET_ACTIVE_YEAR](state, tahun) {
        state.tahun = tahun;
    },
    [SET_ACTIVE_PROGRAM](state, program) {
        state.programId = program;
    },
    [SET_ACTIVE_PRGNAME](state, name) {
        state.programName = name;
    },
    [SET_ACTIVE_BATCH](state, name) {
        state.batchName = name;
    },
    [SET_ACTIVE_KEGIATAN](state, kegiatan) {
        state.kegiatanId = kegiatan;
    },
    [SET_ACTIVE_KEGIATAN_NAME](state, name) {
        state.kegiatanName = name;
    },
    [SET_ACTIVE_KEGIATAN_STATE](state, status) {
        state.kegiatanState = status;
    },
    [SET_MGT_VALUE](state, value) {
        state.mgtValue = value;
    },
    [SET_ACTIVE_PRGSTATE](state, status) {
        state.programStatus = status;
    }
};

const getters = {
    [GET_ACTIVE_YEAR]: state => state.tahun,
    [GET_ACTIVE_BATCH]: state => state.batchName,
    [GET_ACTIVE_PROGRAM]: state => state.programId,
    [GET_ACTIVE_PRGNAME]: state => state.programName,
    [GET_ACTIVE_PRGSTATE]: state => state.programStatus,
    [GET_ACTIVE_KEGIATAN]: state => state.kegiatanId,
    [GET_ACTIVE_KEGIATAN_NAME]: state => state.kegiatanName,
    [GET_ACTIVE_KEGIATAN_STATE]: state => state.kegiatanState,
    [GET_MGT_VALUE]: state => state.mgtValue,
};

export default {
    namespaced: true,
    state, mutations, getters
}
