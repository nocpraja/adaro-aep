import Locale from './Locale';
import PageTitle from './PageTitle';
import UserProfile from "./UserProfile";
import AccessControl from './AccessControls';
import Pengelolaan from './Kegiatan/PengelolaanChain';
import Beneficiary from './Kegiatan/BeneficiaryChain';
import ProgramCsr from './MasterData/ProgramCsr';
import TargetKpi from "./MasterData/TargetKpi";

export default {
    Locale,
    PageTitle,
    UserProfile,
    AccessControl,
    MasterData: {
        namespaced: true,
        modules: {
            ProgramCsr,
            TargetKpi
        }
    },
    Kegiatan: {
        namespaced: true,
        modules: {
            Pengelolaan,
            Beneficiary
        }
    }
}
