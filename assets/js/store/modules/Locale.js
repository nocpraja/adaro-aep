import { GET_LOCALE, SET_LOCALE } from "./mutation-types";

const state = {
    locale: 'id'
};

const mutations = {
    [SET_LOCALE](state, locale) {
        state.locale = locale;
    }
};

const actions = {
    [SET_LOCALE]({commit}, locale) {
        commit(SET_LOCALE, locale);
    }
};

const getters = {
    [GET_LOCALE]: state => state.locale
};

export default {
    state, mutations, actions, getters
}
