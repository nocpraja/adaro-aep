import { GET_ACTIVE_ITEM, SET_ACTIVE_ITEM } from "../mutation-types";

const defaultValue = Object.freeze({
    id: null,
    title: null,
    bidang: null
});

const state = {
    program: {...defaultValue}
};

const mutations = {
    [SET_ACTIVE_ITEM](state, program) {
        if (program === null) {
            state.program = {...defaultValue};
        } else {
            state.program = program;
        }
    }
};

const getters = {
    [GET_ACTIVE_ITEM]: state => state.program
};

export default {
    namespaced: true,
    state, mutations, getters
}
