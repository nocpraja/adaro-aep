import {
    GET_ACTIVE_BATCH,
    GET_ACTIVE_BATCH_NAME,
    GET_ACTIVE_PRGCSR,
    GET_ACTIVE_PRGCSR_NAME,
    SET_ACTIVE_BATCH,
    SET_ACTIVE_BATCH_NAME,
    SET_ACTIVE_PRGCSR,
    SET_ACTIVE_PRGCSR_NAME
} from "../Kegiatan/mutation-types";

const state = {
    batchId: null,
    batchName: null,
    programId: null,
    programName: null
};

const mutations = {
    [SET_ACTIVE_BATCH](state, batch) {
        state.batchId = batch;
    },
    [SET_ACTIVE_BATCH_NAME](state, name) {
        state.batchName = name;
    },
    [SET_ACTIVE_PRGCSR](state, csr) {
        state.programId = csr;
    },
    [SET_ACTIVE_PRGCSR_NAME](state, name) {
        state.programName = name;
    }
};

const getters = {
    [GET_ACTIVE_BATCH]: state => state.batchId,
    [GET_ACTIVE_BATCH_NAME]: state => state.batchName,
    [GET_ACTIVE_PRGCSR]: state => state.programId,
    [GET_ACTIVE_PRGCSR_NAME]: state => state.programName
};

export default {
    namespaced: true,
    state, mutations, getters
}
