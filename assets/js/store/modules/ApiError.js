import Vue from "vue";
import { STATE_ERROR } from "./mutation-types";

export default function showApiError(error, apiName) {
    if (error.response) {
        if (error.response.data.message) {
            Vue.prototype.$notification.error(error.response.data.message, STATE_ERROR);
        } else if (error.response.status === 404) {
            Vue.prototype.$notification.error(`API ${apiName} tidak ditemukan.`, STATE_ERROR);
        } else if (error.response.status === 401 || error.response.status === 403) {
            Vue.prototype.$notification.error('Access Denied. Anda tidak memiliki hak untuk mengakses API.', STATE_ERROR);
        } else if (error.response.status === 500) {
            Vue.prototype.$notification.error('Internal Server Error', STATE_ERROR);
        } else {
            Vue.prototype.$notification.error('Unknown error.', STATE_ERROR);
        }
    } else {
        console.warn(error);
        Vue.prototype.$notification.error('Gagal melakukan koneksi ke remote server', STATE_ERROR);
    }
}
