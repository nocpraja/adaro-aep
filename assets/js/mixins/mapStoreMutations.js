import { mapGetters, mapMutations } from "vuex";
import { GET_PAGE_TITLE, SET_PAGE_TITLE } from "../store/modules/mutation-types";

export default {
    computed: {
        ...mapGetters({
            'getPageTitle': GET_PAGE_TITLE
        })
    },
    methods: {
        ...mapMutations({
            'setPageTitle': SET_PAGE_TITLE
        })
    }
}
