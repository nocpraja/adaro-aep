import { STATE_ERROR } from "../store/modules/mutation-types";

export default {
    methods: {
        showErrorMessage(error) {
            if (error.response) {
                if (error.response.data.message) {
                    this.$notification.error(error.response.data.message, STATE_ERROR);
                } else if (error.response.status === 404) {
                    this.$notification.error('API URL tidak ditemukan.', STATE_ERROR);
                } else if (error.response.status === 401 || error.response.status === 403) {
                    this.$notification.error('Access Denied. Anda tidak memiliki hak untuk mengakses ataupun merubah data.', STATE_ERROR);
                } else if (error.response.status === 405) {
                    this.$notification.error('Bad method call, server menolak data yang dikirim.', STATE_ERROR);
                } else if (error.response.status === 500) {
                    this.$notification.error('Internal Server Error', STATE_ERROR);
                } else {
                    this.$notification.error('Unknown error.', STATE_ERROR);
                }
            } else {
                console.warn(error);
                this.$notification.error('Gagal melakukan koneksi ke remote server', STATE_ERROR);
            }
        }
    }
}
