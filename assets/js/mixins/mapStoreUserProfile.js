import { mapActions, mapGetters, mapMutations } from "vuex";
import {
    ASSIGN_VALUE,
    ASSIGN_VALUES,
    FETCH,
    GET_USER_EMAIL,
    GET_USER_FULLNAME,
    GET_USER_GROUP_NAME,
    GET_USER_PROFILE,
    SET_USER_EMAIL,
    SET_USER_FULLNAME,
    SET_USER_GROUP_NAME
} from "../store/modules/mutation-types";

export default {
    computed: {
        ...mapGetters('UserProfile', [
            GET_USER_PROFILE, GET_USER_FULLNAME, GET_USER_EMAIL, GET_USER_GROUP_NAME
        ]),
    },
    methods: {
        ...mapActions('UserProfile', [FETCH]),
        ...mapMutations('UserProfile', [
            ASSIGN_VALUE, ASSIGN_VALUES, SET_USER_EMAIL, SET_USER_FULLNAME, SET_USER_GROUP_NAME
        ])
    }
}
