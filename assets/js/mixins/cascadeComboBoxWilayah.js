import { BsStore } from "vue-bs4";

export default {
    data: () => ({
        /**
         * DataSource Provinsi untuk comboBox
         **/
        dsCboProvinsi: {
            schema: {
                displayField: 'namaProvinsi',
                valueField: 'provinsiId'
            },
            proxy: new BsStore({
                idProperty: 'provinsiId',
                dataProperty: 'data',
                totalProperty: 'total',
                restUrl: {
                    browse: '/master-data/wilayah/provinsi'
                }
            })
        },
        /**
         * DataSource Kabupaten untuk comboBox
         **/
        dsCboKabupaten: {
            schema: {
                displayField: 'namaKabupaten',
                valueField: 'kabupatenId',
                cascadeField: 'provinsi.provinsiId'
            },
            proxy: new BsStore({
                idProperty: 'kabupatenId',
                dataProperty: 'data',
                totalProperty: 'total',
                restUrl: {
                    browse: '/master-data/wilayah/kabupaten'
                }
            })
        },
        /**
         * DataSource Kecamatan untuk comboBox
         **/
        dsCboKecamatan: {
            schema: {
                displayField: 'namaKecamatan',
                valueField: 'kecamatanId',
                cascadeField: 'kabupaten.kabupatenId'
            },
            proxy: new BsStore({
                idProperty: 'kecamatanId',
                dataProperty: 'data',
                totalProperty: 'total',
                restUrl: {
                    browse: '/master-data/wilayah/kecamatan'
                }
            })
        },
        /**
         * DataSource Kelurahan untuk comboBox
         **/
        dsCboKelurahan: {
            schema: {
                displayField: 'namaKelurahan',
                valueField: 'kelurahanId',
                cascadeField: 'kecamatan.kecamatanId'
            },
            proxy: new BsStore({
                idProperty: 'kelurahanId',
                dataProperty: 'data',
                totalProperty: 'total',
                restUrl: {
                    browse: '/master-data/wilayah/kelurahan'
                }
            })
        }
    }),
    beforeDestroy() {
        this.dsCboProvinsi.proxy.destroy();
        this.dsCboProvinsi.proxy = null;
        this.dsCboProvinsi       = null;

        this.dsCboKabupaten.proxy.destroy();
        this.dsCboKabupaten.proxy = null;
        this.dsCboKabupaten       = null;

        this.dsCboKecamatan.proxy.destroy();
        this.dsCboKecamatan.proxy = null;
        this.dsCboKecamatan       = null;

        this.dsCboKelurahan.proxy.destroy();
        this.dsCboKelurahan.proxy = null;
        this.dsCboKelurahan       = null;
    }
}
