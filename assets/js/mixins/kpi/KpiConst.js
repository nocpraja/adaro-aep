export const RED       = 'danger';
export const YELLOW    = 'warning';
export const GREEN     = 'success';
export const EDIT_MODE = 'edit';
export const VIEW_MODE = 'view';

export const TRUE      = 'TRUE';
export const FALSE     = 'FALSE';
export const KUALITAS  = 'KUALITAS';
export const KUANTITAS = 'KUANTITAS';

export const SMK    = 'SMK';
export const POLTEK = 'POLTEK';
