import {Helper} from "vue-bs4";
import {GREEN, KUALITAS, KUANTITAS, POLTEK, RED, SMK, TRUE, YELLOW} from "./KpiConst";

export default {
    computed: {
        /**
         * Method umum untuk menghasilkan nilai akhir yang umum digunakan pada Program CSR.
         *
         * @return {Object} Nilai akhir KPI Kualitas
         */
        generalNilaiAkhirKualitas() {
            let nilaiAkhir = {
                daftar: {},
                total: null
            };

            this.kpiKualitas.forEach(kpiParam => {
                let persentaseCapaian = this.generalPersentaseCapaian(kpiParam.code, 'kualitas');

                if (persentaseCapaian === null) {
                    nilaiAkhir.daftar[kpiParam.code] = null;
                } else {
                    let bobot = this.paramValue(kpiParam, 'bobot');
                    let nilai = (persentaseCapaian / 100) * bobot;

                    this.targets[kpiParam.code].nilaiAkhir = nilai;
                    nilaiAkhir.daftar[kpiParam.code] = (nilai).toFixed(2);

                    if (nilaiAkhir.total === null) {
                        nilaiAkhir.total = 0;
                    }

                    nilaiAkhir.total += nilai;
                }
            });

            if (nilaiAkhir.total !== null) {
                nilaiAkhir.total = nilaiAkhir.total.toFixed(2);
            }

            return nilaiAkhir;
        },
        /**
         * Check parameter KPI, apakah sudah memiliki target atau belum.
         *
         * @return {boolean} TRUE jika sudah memiliki target, dan FALSE jika sebaliknya
         */
        haveTargets() {
            let total = 0;

            if (!Helper.isEmpty(this.targets)) {
                for (const item in this.targets) {
                    if (this.targets.hasOwnProperty(item)) {
                        total += Object.keys(this.targets[item]).length;
                    }
                }
            }

            return (total > 0);
        },
        /**
         * Filter array of KPI's parameters and returns a copy of new array.
         *
         * @return {Object[]} The filtered array
         */
        kpiKualitas() {
            return this.params.filter((el) => {
                return this.paramValue(el, 'grouping') === KUALITAS;
            });
        },
        /**
         * Filter array of KPI's parameters and returns a copy of new array.
         *
         * @return {Object[]} The filtered array
         */
        kpiKuantitas() {
            return this.params.filter((el) => {
                return this.paramValue(el, 'grouping') === KUANTITAS;
            });
        },
        /**
         * Filter array of KPI's parameters and returns a copy of new array.
         *
         * @return {Object[]} The filtered array
         */
        kpiSMK() {
            return this.params.filter((el) => {
                return this.paramValue(el, 'institusi') === SMK;
            });
        },
        /**
         * Filter array of KPI's parameters and returns a copy of new array.
         *
         * @return {Object[]} The filtered array
         */
        kpiPoltek() {
            return this.params.filter((el) => {
                return this.paramValue(el, 'institusi') === POLTEK;
            });
        },
        /**
         * Filter array of KPI's parameters and returns a copy of new array.
         *
         * @return {Object[]} The filtered array
         */
        kpiTargetKualitas() {
            return this.params.filter((el) => {
                return this.paramValue(el, 'targeted') === TRUE && this.paramValue(el, 'grouping') === KUALITAS;
            });
        },
        /**
         * Filter array of KPI's parameters and returns a copy of new array.
         *
         * @return {Object[]} The filtered array
         */
        kpiTargetKuantitas() {
            return this.params.filter((el) => {
                return this.paramValue(el, 'targeted') === TRUE && this.paramValue(el, 'grouping') === KUANTITAS;
            });
        },
        /**
         * Filter array of KPI's parameters and returns a copy of new array.
         *
         * @return {Object[]} The filtered array
         */
        kpiTargetPoltek() {
            return this.params.filter((el) => {
                return this.paramValue(el, 'targeted') === TRUE && this.paramValue(el, 'institusi') === POLTEK;
            });
        },
        /**
         * Filter array of KPI's parameters and returns a copy of new array.
         *
         * @return {Object[]} The filtered array
         */
        kpiTargetSMK() {
            return this.params.filter((el) => {
                return this.paramValue(el, 'targeted') === TRUE && this.paramValue(el, 'institusi') === SMK;
            });
        },
        periodeBatch() {
            const start = this.batch.tahunAwal;
            const end = this.batch.tahunAkhir;
            let years = [];

            for (let i = start; i <= end; i++) {
                years.push(i);
            }

            return years;
        },
    },
    methods: {
        /**
         * Create alphabet numeric from an integer value.
         *
         * @param {int} num           The input value
         *
         * @return {string} Alphabet numeric
         */
        integerToRoman(num) {
            if (typeof num !== 'number')
                return false;

            let digits = String(+num).split(""),
                key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
                    "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
                    "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
                roman_num = "",
                i = 3;
            while (i--)
                roman_num = (key[+digits.pop() + (i * 10)] || "") + roman_num;
            return Array(+digits.join("") + 1).join("M") + roman_num;
        },
        /**
         * Create alphabet numeric from an integer value.
         *
         * @param {int} value           The input value
         * @param {boolean} [upperCase] Result in uppercase or lowercase, default is lowercase
         *
         * @return {string} Alphabet numeric
         */
        alphabetNumbering(value, upperCase = false) {
            let baseChar = ("a").charCodeAt(0),
                letters = "";

            do {
                value -= 1;
                letters = String.fromCharCode(baseChar + (value % 26)) + letters;
                value = (value / 26) >> 0;
            } while (value > 0);

            return (upperCase ? letters.toUpperCase() : letters);
        },
        /**
         * Validate semua field pada tahun tertentu apakah sudah diisi.
         *
         * @param {int} targetTahun    Tahun anggaran
         * @return {boolean} TRUE if semua field terisi, FALSE jika terdapat field yang tidak terisi
         */
        checkMandatory(targetTahun) {
            const keys = Object.keys(targetTahun);
            let allFilled = true;

            for (const key of keys) {
                const field = targetTahun[key];
                if (field.target === null) {
                    allFilled = false;
                    break;
                }
            }

            return allFilled;
        },
        /**
         * Validate persentase jumlah target untuk semua tahun anggaran.
         *
         * @return {boolean} TRUE if all mandatory field filled, otherwise FALSE
         */
        checkMandatoryAll() {
            const tahuns = Object.keys(this.targets);
            let isOk = true;

            for (const tahun of tahuns) {
                const objTahun = this.targets[tahun];
                isOk = isOk && this.checkMandatory(objTahun);
            }

            return isOk;
        },
        /**
         * Validate persentase jumlah target untuk tahun anggaran tertentu.
         *
         * @param {int} targetTahun Tahun anggaran
         *
         * @return {boolean} TRUE if persentase jumlah target adalah 100, otherwise FALSE
         */
        checkPercent(targetTahun) {
            const keys = Object.keys(targetTahun);
            let percent = 0;

            for (const key of keys) {
                const field = targetTahun[key];
                const grouping = this.paramValue(field.dataRef, 'grouping');

                if (grouping === KUALITAS) {
                    percent += parseFloat(field.target);
                }
            }

            return (percent === 100.0);
        },
        /**
         * Validate persentase jumlah target untuk semua tahun anggaran.
         *
         * @return {boolean} TRUE if persentase jumlah target adalah 100, otherwise FALSE
         */
        checkPercentAll() {
            const tahuns = Object.keys(this.targets);
            let isOk = true;

            for (const tahun of tahuns) {
                const objTahun = this.targets[tahun];
                isOk = isOk && this.checkPercent(objTahun);
            }

            return isOk;
        },
        /**
         * Memperoleh warna TLA dengan membandingkan value terhadapa threshold yang diberikan.
         *
         * @param {Number[]} thresholds Array yang berisi threhold dengan index: 0 -> 1 : threshold paling kecil ke besar
         * @param {Number} value        Nilai yang ingin diklasifikasikan
         *
         * @return {String[]} Kelas TLA
         */
        classifyTLA(thresholds, value) {
            if (value === null) {
                return null;
            }

            let color = '';

            if (value > thresholds[1]) {
                color = GREEN;
            } else if (value >= thresholds[0] && value <= thresholds[1]) {
                color = YELLOW;
            } else if (value < thresholds[0]) {
                color = RED;
            }

            if (color === '') {
                return null;
            }

            return ['bg-' + color, 'text-white'];
        },
        /**
         * Fungsi umum untuk memperoleh persentase capaian yang digunankan di beberapa program CSR
         * percentage = capaian / target
         *
         * @param {int} targetIndex Index of target array
         * @param {String} type     Tipe KPI
         *
         * @return {*} Persentase capaian KPI
         */
        generalPersentaseCapaian(targetIndex, type = 'kuantitas') {
            let p;

            if (type === 'kuantitas') {
                for (let i = 4; i > 0; --i) {
                    p = this.targets[targetIndex]['p' + i];
                    if (p) {
                        break;
                    }
                }
            } else {
                let flag = this.periodeFull(4);
                let period = this.integerToRoman(flag);

                // let total = this.getTotalByIndex(this.kpiKualitas, period);
                // let total = 0;
                // this.kpiKualitas.forEach(kpiParam => {
                //     total += (this.targets[kpiParam.code].period[period]) ? this.targets[kpiParam.code].period[period] : 0;
                // });
                p = this.targets[targetIndex].period[period]
            }

            if (!p) {
                return null;
            }

            p *= 100;
            if (this.targets[targetIndex].target !== null) {
                // Check if data is TEFA Matrix
                if (this.targets[targetIndex].dataRef.code === 'KPI-02.014') {
                    let tefa = 0;
                    let baseline = this.paramValue(this.targets[targetIndex].dataRef, "baseline");
                    let idxTahun = this.targets[targetIndex].tahun - this.batch.tahunAwal + 1;

                    tefa = 3.00 + (0.25 * idxTahun);

                    for (let i = 4; i > 0; --i) {
                        p = this.targets[targetIndex]['p' + i];

                        if (p) {
                            break;
                        }
                    }
                    return (((p - baseline) / (tefa - baseline)) * 100).toFixed(2);
                } else {
                    if (this.targets[targetIndex].target !== 0) {
                        return (p / this.targets[targetIndex].target).toFixed(2);
                    } else {
                        return 100.00;
                    }
                }
            } else {
                return '';
            }
        },
        /**
         * Mendapatkan nilai total dalam satu kolom (pada tabel) yang mana kolom didefinisikan melalui
         * parameter index.
         *
         * @param {Object} kpi  Data model
         * @param {string} idx  Index penunjuk kolom yang ingin ditotalkan
         *
         * @return {String} Total capaian KPI
         */
        getTotalByIndex(kpi, idx) {
            let total = 0;

            kpi.forEach(kpiParam => {
                total += (this.targets[kpiParam.code][idx]) ? this.targets[kpiParam.code][idx] : 0;
            });

            return (total === 0) ? null : total;
        },
        /**
         * Mendapatkan nilai total bobot (Untuk model KpiKualitas).
         *
         * @param {Object} kpi Data Model
         *
         * @return {number} Total bobot
         */
        getTotalBobotKualitas(kpi) {
            let total = 0;

            kpi.forEach(kpiParam => {
                let bobot = this.paramValue(kpiParam, 'bobot') ? parseInt(this.paramValue(kpiParam, 'bobot')) : 0;
                total += bobot;
            });

            return total;
        },
        /**
         * Unpack parameter from dataref structure and get a value from its parameter's property.
         *
         * @param {Object} kpi Data model
         * @param {string} key The property to find
         *
         * @return {string|null} Parameter's value
         */
        paramValue(kpi, key) {
            if (kpi == null) {
                return null;
            } else if (!Array.isArray(kpi.params)) {
                return null;
            } else {
                let item = kpi.params.find((value) => {
                    return (value.property === key);
                });

                if (!item) {
                    return null;
                }
                return item.value;
            }
        },
        /**
         * Mengetahui periode mana yang datanya penuh.
         *
         * @param {int} totalPeriode Jumlah periode dalam program
         *
         * @return {int} Index tahun periode KPI kualitas
         */
        periodeFull(totalPeriode) {
            let flag        = 0;
            let periodeFull = [];

            for (let i = 1; i <= totalPeriode; ++i) {
                periodeFull[i] = true;
            }

            // Tentukan Flag
            const targets = this.targets;
            this.kpiKualitas.forEach(function (kpiParam) {
                for (let i = 1; i <= totalPeriode; ++i) {
                    periodeFull[i] = periodeFull[i] && (targets[kpiParam.code]['p' + i] !== null)
                }
            });

            for (let i = totalPeriode; i > 0; --i) {
                if (periodeFull[i]) {
                    return i;
                }
            }

            return flag;
        },
        /**
         * Mengetahui kelengkapan data capaian dalam suatu periode.
         *
         * @param {Array} parameters Array parameter yang diperiksa
         * @param {int} periode      Periode yang diperiksa
         *
         * @return {boolean} TRUE jika data capaian KPI untuk suatu periode telah diisi semua
         */
        isPeriodeComplete(parameters, periode) {
            let isFull = true;

            for (const param of parameters) {
                isFull = isFull && (this.targets[param.code]['p' + periode] !== null);
            }

            return isFull;
        },
        /**
         * Mengetahui apakah data capaian dalam periode dikosongkan untuk tidak di-save.
         *
         * @param {int} periode Periode yang diperiksa
         *
         * @return {boolean} TRUE jika data capaian KPI belum diisi
         */
        isPeriodeEmpty(periode) {
            let isEmpty = true;

            for (const param of this.params) {
                isEmpty = isEmpty && (this.targets[param.code]['p' + periode] === null)
            }

            return isEmpty;
        },
        /**
         * Periksa status verifikasi untuk tampilkan icon Verified atau tidak.
         *
         * @param {int} periode Periode untuk Semester-1, Semester-2
         *
         * @return {boolean} TRUE jika terverifikasi dan icon ditampilkan atau FALSE sebaliknya
         */
        isVerified(periode) {
            let verified = true;

            for (const param of this.params) {
                verified = verified && (this.targets[param.code]['p' + periode + 'Verified'] === true)
            }

            return verified;
        },
        /**
         * Periksa apakah total capaian sudah 100% di periode tsb.
         *
         * @param {Array} parameters Array parameter yang diperiksa
         * @param {int} periode      Periode untuk triwulan atau semester
         *
         * @return {boolean} TRUE jika sudah 100% atau FALSE sebaliknya
         */
        sudah100persen(parameters, periode) {
            let percent = 0;

            for (const param of parameters) {
                percent += parseFloat(this.targets[param.code]['p' + periode]);
            }

            return (percent === 100.0);
        },
        /**
         * Pesan validasi untuk periode yang menurut kelengkapan data capaian di periode tsb.
         *
         * @param {int} periode               Periode untuk triwulan atau semester
         * @param {boolean} checkJumlahPersen Optional
         *
         * @return {string} pesan validasi
         */
        validationMessage(periode, checkJumlahPersen = false) {
            if (this.isRequiringNoValidation(periode)) {
                // kosong atau sudah terverifikasi
                return '';
            } else if (!this.isPeriodeComplete(this.params, periode)) {
                return 'Belum terisi semua';
            } else if (checkJumlahPersen && !this.sudah100persen(this.kpiKualitas, periode)) {
                return 'Total ≠ 100%';
            }
            // else
            return 'Terisi';
        },
        /**
         * Periksa apakah capaian tidak dikenakan validasi di periode tsb.
         *
         * @param {int} periode Periode untuk triwulan atau semester
         *
         * @return {boolean} TRUE jika periode tidak perlu validasi atau FALSE sebaliknya
         */
        isRequiringNoValidation(periode) {
            return this.isVerified(periode)       // yang sudah diverifikasi tidak menggagalkan save
                || this.isPeriodeEmpty(periode);  // yang masih kosong juga tidak menggagalkan save
        },
        /**
         * Menentukan status tombol save di periode tsb.
         *
         * @param {int} n_periode Jumlah periode untuk triwulan atau semester
         * @return {boolean} TRUE jika boleh save atau FALSE sebaliknya
         */
        isSaveEnabled(n_periode) {
            for (let periode = 1; periode <= n_periode; periode++) {
                // cari periode yang tidak kosong dan belum verified
                if (!this.isRequiringNoValidation(periode)) {
                    return true;
                }
            }

            return false;
        }
    }
}
