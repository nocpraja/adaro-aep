import {
    CLOSED,
    STATE_SUCCESS,
    STEP_AKTIVASI,
    STEP_APPROVAL_1,
    STEP_APPROVAL_2,
    STEP_APPROVAL_3,
    STEP_CLOSING,
    STEP_REJECT,
    STEP_REJECT_REQUEST,
    STEP_VERIFY,
    STEP_VERIFY_REQUEST
} from "../../store/modules/mutation-types";
import DanaBatchModel from "../../models/Pendanaan/DanaBatchModel";

export default {
    /**
     * @property {BsModel} dataModel
     */

    data: () => ({
        approval: {
            dlgTitle: '',
            dlgMessage: '',
            action: '',
            reason: ''
        },
        showModalApproval: false,
        showModalDelete: false
    }),
    computed: {
        /**
         * Change submit button text based on dialog action.
         *
         * @return {string} Button text
         */
        submitBtnText() {
            if (this.approval.action.includes(STEP_VERIFY)) {
                return 'Verify'
            } else if (this.approval.action.includes('approval')) {
                return 'Approve';
            } else if (this.approval.action.includes(STEP_AKTIVASI)) {
                return 'Aktifkan';
            } else if (this.approval.action.includes(STEP_CLOSING)) {
                return 'Tutup';
            } else {
                return 'Submit';
            }
        },
        /**
         * Get a workflow.
         *
         * @return {Workflow} A workflow object instance
         */
        workflow() {
            return this.danaWorkflow.workflow;
        }
    },
    methods: {
        /**
         * Menampilkan modal dialog konfirmasi verifikasi, approval, aktivasi ataupun closing.
         *
         * @param {Object} item Data model
         * @param {String} action Step transition name
         * @return {void}
         */
        confirmApproval(item, action) {
            this.dataModel.assignValues(item);

            let title            = '';
            this.approval.action = action;
            this.approval.reason = null;

            if (this.dataModel instanceof DanaBatchModel) {
                title = this.dataModel.namaProgram;
            } else {
                title = this.dataModel.title;
            }

            switch (action) {
                case STEP_VERIFY:
                case STEP_VERIFY_REQUEST:
                    this.approval.dlgTitle   = 'Konfirmasi Verifikasi';
                    this.approval.dlgMessage = `Apakah anda yakin data <b>${title}</b> sudah benar ?`;
                    break;
                case STEP_AKTIVASI:
                    this.approval.dlgTitle   = 'Konfirmasi';
                    this.approval.dlgMessage = `Apakah anda yakin akan mengaktifkan <b>${title}</b> ?`;

                    break;
                case STEP_CLOSING:
                    this.approval.dlgTitle   = 'Konfirmasi';
                    this.approval.dlgMessage = `Apakah anda yakin akan menutup <b>${title}</b> ?`;
                    break;
                default:
                    this.approval.dlgTitle   = 'Konfirmasi Approval';
                    this.approval.dlgMessage = `Apakah anda yakin data <b>${title}</b> sudah benar ?`;

                    if (this.workflow.can(item, STEP_APPROVAL_1)) {
                        this.approval.action = STEP_APPROVAL_1;
                    } else if (this.workflow.can(item, STEP_APPROVAL_2)) {
                        this.approval.action = STEP_APPROVAL_2;
                    } else {
                        this.approval.action = STEP_APPROVAL_3;
                    }

                    break;
            }

            this.showModalApproval = true;
        },
        /**
         * Menampilkan modal dialog konfirmasi delete record Dana Bidang, Program ataupun Batch.
         *
         * @param {Object} item Data model
         * @return {void}
         */
        confirmDelete(item) {
            this.dataModel.assignValues(item);
            this.showModalDelete = true;

            if (item.namaProgram) {
                this.dataModel.title = item.namaProgram;
            }
        },
        /**
         * Proses flow approval Dana Bidang, Program ataupun Batch.
         *
         * @return {void}
         */
        doApproval() {
            const data = {
                action: this.approval.action,
                message: this.approval.reason
            };

            this.showModalApproval = false;

            this.dataModel.request('stepflow', 'patch', null, data).then(() => {
                this.$refs.datagrid.reload();
            }).catch(error => {
                this.showErrorMessage(error);
            });
        },
        /**
         * Proses REJECT pengelolaan/revisi Dana Bidang, Program ataupun Batch.
         *
         * @return {void}
         */
        doReject() {
            const action = this.approval.action;
            const data   = {
                action: action === STEP_VERIFY_REQUEST ? STEP_REJECT_REQUEST : STEP_REJECT,
                message: this.approval.reason
            };

            this.showModalApproval = false;

            this.dataModel.request('stepflow', 'patch', null, data).then(() => {
                this.$refs.datagrid.reload();
            }).catch(error => {
                this.showErrorMessage(error);
            });
        },
        /**
         * Proses hapus Dana Bidang, Program ataupun Batch.
         *
         * @return {void}
         */
        doDelete() {
            this.showModalDelete = false;

            this.dataModel.delete().then(response => {
                this.$notification.success(response.data.message, STATE_SUCCESS);
                this.$refs.datagrid.reload();
            }).catch(error => {
                this.showErrorMessage(error);
            });
        },
        alokasiDana(item) {
            return item.status === CLOSED ? item.realisasi : item.alokasi;
        },
        sisaDana(item) {
            return item.status === CLOSED ? item.sisaDana : item.danaTersedia;
        }
    }
}
