import mapAccessControls from "./mapAccessControls";

export default {
    mixins: [mapAccessControls],
    beforeRouteEnter(to, from, next) {
        next(async vm => {
            await vm.checkAcl();

            if (vm.verifyRouteEnter(to, 'EDIT')) {
                vm.verifyRouteUrlSmkPoltek(to, next);
            } else {
                vm.$router.push('/access-denied');
            }
        });
    },
    beforeRouteUpdate(to, from, next) {
        if (this.hasEditorAccessRights) {
            this.verifyRouteUrlSmkPoltek(to, next);
        } else {
            this.$router.push('/access-denied');
        }
    }
}
