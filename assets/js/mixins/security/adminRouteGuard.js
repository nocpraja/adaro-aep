import mapAccessControls from "./mapAccessControls";

export default {
    mixins: [mapAccessControls],
    beforeRouteEnter(to, from, next) {
        next(async vm => {
            await vm.checkAcl();
            if (vm.hasAdminRole) {
                next();
            } else {
                next('/access-denied');
            }
        });
    },
    beforeRouteUpdate(to, from, next) {
        if (this.hasAdminRole) {
            next();
        } else {
            next('/access-denied');
        }
    }
}
