import mapAccessControls from "./mapAccessControls";

export default {
    mixins: [mapAccessControls],
    beforeRouteEnter(to, from, next) {
        next(async vm => {
            await vm.checkAcl();
            if (vm.verifyRouteEnter(to, 'READ')) {
                next();
            } else {
                next('/access-denied');
            }
        });
    },
    beforeRouteUpdate(to, from, next) {
        if (this.hasAccess) {
            next();
        } else {
            next('/access-denied');
        }
    }
}
