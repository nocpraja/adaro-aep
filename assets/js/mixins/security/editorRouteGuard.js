import mapAccessControls from "./mapAccessControls";

export default {
    mixins: [mapAccessControls],
    beforeRouteEnter(to, from, next) {
        next(async vm => {
            await vm.checkAcl();
            if (vm.verifyRouteEnter(to, 'EDIT')) {
                next();
            } else {
                next('/access-denied');
            }
        });
    },
    beforeRouteUpdate(to, from, next) {
        if (this.hasEditorAccessRights) {
            next();
        } else {
            next('/access-denied');
        }
    }
}
