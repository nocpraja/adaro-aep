import { mapActions, mapGetters } from "vuex";
import { FETCH_COLLECTION, FIND_ACL, GET_COLLECTION, GET_ROLES } from "../../store/modules/mutation-types";
import { Helper } from "vue-bs4";
import {
    SET_ACTIVE_PRGCSR,
    SET_ACTIVE_PRGNAME, SET_ACTIVE_PRGSTATE,
    SET_ACTIVE_PROGRAM,
    SET_ACTIVE_YEAR
} from "../../store/modules/Kegiatan/mutation-types";

export default {
    computed: {
        ...mapGetters('AccessControl', [FIND_ACL, GET_COLLECTION, GET_ROLES]),
        /**
         * Memeriksa apakah aktif USER memiliki ROLE_ADMIN.
         *
         * @return {boolean} TRUE jika memiliki ROLE_ADMIN
         */
        hasAdminRole() {
            return this.hasRole('ROLE_ADMIN');
        },
        /**
         * Memeriksa apakah aktif USER memiliki ROLE_APPROVAL.
         *
         * @return {boolean} TRUE jika memiliki ROLE_APPROVAL
         */
        hasApprovalRole() {
            return this.hasRole('ROLE_APPROVAL');
        },
        /**
         * Memeriksa apakah aktif USER memiliki ROLE_MITRA.
         *
         * @return {boolean} TRUE jika memiliki ROLE_MITRA
         */
        hasMitraRole() {
            return this.hasRole('ROLE_MITRA');
        },
        /**
         * Memeriksa apakah aktif USER memiliki ROLE_VERIFICATOR.
         *
         * @return {boolean} TRUE jika memiliki ROLE_VERIFICATOR
         */
        hasVerificatorRole() {
            return this.hasRole('ROLE_VERIFICATOR');
        },
        /**
         * Memeriksa apakah aktif USER memiliki READ akses terhadap halaman yang dituju.
         *
         * isGrantAccess, isGrantEdit isGrantDelete isVerificator isApproval
         * @return {boolean} TRUE jika memiliki READ akses
         */
        hasAccess() {
            return this.hasRule('READ');
        },
        /**
         * Memeriksa apakah aktif USER memiliki hak akses DELETE terhadap halaman yang dituju.
         *
         * @return {boolean} TRUE jika memiliki hak akses DELETE
         */
        hasDeleteAccessRights() {
            return this.hasRule('DELETE');
        },
        /**
         * Memeriksa apakah aktif USER memiliki hak akses EDIT terhadap halaman yang dituju.
         *
         * @return {boolean} TRUE jika memiliki hak akses EDIT
         */
        hasEditorAccessRights() {
            return this.hasRule('EDIT');
        },
        /**
         * Memeriksa apakah aktif USER memiliki hak akses VERIFICATOR terhadap halaman yang dituju.
         *
         * @return {boolean} TRUE jika memiliki hak akses VERIFICATOR
         */
        hasVerificatorAccessRights() {
            return this.hasRule('VERIFICATOR');
        },
        /**
         * Memeriksa apakah aktif USER memiliki hak akses APPROVAL terhadap halaman yang dituju.
         *
         * @return {boolean} TRUE jika memiliki hak akses APPROVAL
         */
        hasApprovalAccessRights() {
            return this.hasRule('APPROVAL');
        },
        /**
         * Memeriksa apakah aktif USER memiliki hak akses DETAILVIEW terhadap halaman yang dituju.
         *
         * @return {boolean} TRUE jika memiliki hak akses DETAILVIEW
         */
        hasDetailViewAccessRights() {
            return this.hasRule('DETAILVIEW');
        },
    },
    methods: {
        ...mapActions('AccessControl', [FETCH_COLLECTION]),
        /**
         * Memeriksa apakah ACL untuk aktif USER sudah diload. Jika belum load ACL dari remote server.
         *
         * @return {void}
         */
        async checkAcl() {
            if (this[GET_COLLECTION].length === 0) {
                await this[FETCH_COLLECTION]();
            }
        },
        /**
         * Memeriksa apakah aktif USER memiliki ROLE tertentu.
         *
         * @param {string} role ROLE yang diperiksa
         * @return {boolean}    TRUE jika memiliki ROLE
         */
        hasRole(role) {
            return this[GET_ROLES].includes(role);
        },
        /**
         * Memeriksa apakah aktif USER memiliki Access Rule tertentu terhadap halaman yang dituju.
         *
         * @param {string} rule Access Rule
         * @return {boolean}    TRUE jika memiliki READ akses
         */
        hasRule(rule) {
            for (const matcher of this.$route.matched) {
                const result = this[FIND_ACL](matcher.path);

                if (!Helper.isEmpty(result) && result.accessRules.includes(rule) === true) {
                    return true;
                }
            }

            return false;
        },
        /**
         * Event handler <tt>beforeRouteEnter</tt> untuk memeriksa apakah aktif USER
         * memiliki Access Rule tertentu terhadap route path yang dituju.
         *
         * @param {Object} route       The target Route Object being navigated to
         * @param {String|Array} rules Rule hak akses
         * @return {boolean}           TRUE jika memiliki hak akses
         */
        verifyRouteEnter(route, rules) {
            const result = this[FIND_ACL](route.path);

            if (!Helper.isEmpty(result)) {
                const found = result.accessRules.find(name => {
                    if (Helper.isArray(rules)) {
                        return rules.includes(name);
                    } else {
                        return name === rules
                    }
                });

                if (!Helper.isEmpty(found)) {
                    return true;
                }
            }

            return false;
        },
        /**
         * Verifikasi Route URL SMK/Poltek berdasarkan kategori institusi.
         *
         * @param {Object} route  The target Route Object being navigated to
         * @param {Function} next Function resolve Hook
         * @return {void}
         */
        verifyRouteUrlSmkPoltek(route, next) {
            const kategori = route.params.kategori;

            if (kategori !== 'smk' && kategori !== 'poltek') {
                this[SET_ACTIVE_YEAR](null);
                this[SET_ACTIVE_PROGRAM](null);
                this[SET_ACTIVE_PRGNAME](null);
                this[SET_ACTIVE_PRGCSR](null);
                this[SET_ACTIVE_PRGSTATE](null);

                if (this.hasAccess) {
                    this.$router.push('/kegiatan/penerima-manfaat');
                } else {
                    this.$router.push('/access-denied');
                }
            } else {
                next();
            }
        }
    }
}
