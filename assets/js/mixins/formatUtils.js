import { Helper } from "vue-bs4";
import moment from "moment";
import DatePickerConst from "vue-bs4/src/components/BsPicker/utils/DatePickerConst";
import FormatConst from "./formatConst";

export default {
    data: () => ({
        numberFormat: null,
        smallNumberFormat: null,
        formatConst: FormatConst
    }),
    beforeDestroy() {
        this.numberFormat = null;
        this.smallNumberFormat = null;
    },
    methods: {
        /**
         * Format Nilai akreditas institusi.
         *
         * @param {boolean} akreditas      Sudah terakreditasi atau belum
         * @param {string}  nilaiAkreditas Nilai akreditasi
         *
         * @return {string} Nilai akreditasi
         */
        akreditasInstansi(akreditas, nilaiAkreditas) {
            return akreditas === true ? nilaiAkreditas : "Belum";
        },
        /**
         * Format alamat penerima manfaat.
         *
         * @param {Object} item Data model
         * @return {string} Alamat penerima manfaat
         */
        alamatBeneficiary(item) {
            let alamat = item.alamat;

            if (alamat !== '' && alamat != null && item.kelurahan) {
                alamat += ', ' + item.kelurahan.namaKelurahan;
            } else if (item.kelurahan) {
                alamat = 'Kel. ' + item.kelurahan.namaKelurahan;
            }
            if (alamat !== '' && alamat != null && item.kecamatan) {
                alamat += ', Kec. ' + item.kecamatan.namaKecamatan;
            } else if (item.kecamatan) {
                alamat = 'Kec. ' + item.kecamatan.namaKecamatan;
            }

            return alamat;
        },
        /**
         * Format alamat peserta Beasiswa.
         *
         * @param {Object} item Data model
         * @return {string} Alamat peserta beasiswa
         */
        alamatBeneficiaryBeasiswa(item) {
            return item.biodata.alamat + ', ' + item.biodata.kelurahan.namaKelurahan +
                ', Kec. ' + item.biodata.kecamatan.namaKecamatan;
        },
        /**
         * Format date or datetime value.
         *
         * @param {Date|string} value  The input date/datetime
         * @param {string}      format The output format to be used
         * @return {string} The formatted date or datetime
         */
        dateFormat(value, format) {
            moment.locale(this.getLocale);

            try {
                return moment(value).format(format);
            } catch (e) {
                return moment(value, [
                    format, DatePickerConst.shortISO, DatePickerConst.dateTimeISO,
                    DatePickerConst.yearMonthISO, DatePickerConst.yearISO
                ]).format(format);
            }
        },
        /**
         * Format the given date into timeago or relative time.
         *
         * @param {Moment|Date|String} value The input date
         * @return {string} Timeago formatted string
         */
        dateFormatTimeAgo(value) {
            if (!value) {
                return '';
            }

            moment.locale(this.getLocale);

            try {
                return moment(value).fromNow();
            } catch (e) {
                return 'Invalid date';
            }
        },
        /**
         * Format numeric value based on current Locale.
         *
         * @param {Number|String} value A numeric value or string representing a number in fixed-point notation
         * @return {String} Number formatted
         */
        decimalFormat(value) {
            if (value === null) {
                return '';
            }
            if (this.numberFormat === null) {
                this.numberFormat = new Intl.NumberFormat(this.getLocale, {maximumFractionDigits: 12})
            }

            return this.numberFormat.format(value);
        },
        /**
         * Format numeric value based on current Locale.
         *
         * @param {Number|String} value A numeric value or string representing a number in fixed-point notation
         * @return {Number|String} Number formatted
         */
        percentTarget(value) {
            if (!Helper.isEmpty(value)) {
                if (this.smallNumberFormat === null) {
                    this.smallNumberFormat = new Intl.NumberFormat(this.getLocale, {maximumFractionDigits: 2})
                }
                return value = value ? this.smallNumberFormat.format(value) : 0;
            }

            return 0;
        },
        /**
         * Fix alamat URL.
         *
         * @param {string} url Alamat URL
         * @return {string|null} Alamat URL
         */
        fixUrl(url) {
            if (!Helper.isEmpty(url) && (url.startsWith("http://") || url.startsWith("https://"))) {
                return url;
            } else if (!Helper.isEmpty(url)) {
                return "http://" + url;
            } else {
                return null;
            }
        },
        /**
         * Returns nama kategori Penerima Manfaat Individu.
         *
         * @param {integer} kategori Kategori penerima manfaat
         * @return {string} Nama kategori penerima manfaat
         */
        fmtKategoriIndividu(kategori) {
            switch (kategori) {
                case 1:
                    return 'Santri';
                case 2:
                    return 'Siswa';
                case 3:
                    return 'Mahasiswa';
                case 4:
                    return 'Ustadz';
                case 5:
                    return 'Guru';
                case 6:
                    return 'Dosen';
                case 7:
                    return 'Manajemen';
                case 8:
                    return 'Ustadz/Instruktur';
                default:
                    return ''
            }
        },
        /**
         * Returns nama kategori Penerima Manfaat Institusi.
         *
         * @param {int} kategori Kategori penerima manfaat
         * @return {string} Nama kategori penerima manfaat
         */
        fmtKategoriInstitusi(kategori) {
            switch (kategori) {
                case 1:
                    return 'PAUD';
                case 2:
                    return 'SMK';
                case 3:
                    return 'POLTEK';
                case 4:
                    return 'Pesantren';
                case 5:
                    return 'Lembaga Keagamaan';
                default:
                    return ''
            }
        },
        /**
         * Returns formatted text of Jenis Institusi.
         *
         * @param {boolean} swasta Institusi bertipe swasta atau bukan
         * @return {string} Nama jenis institusi
         */
        jenisInstitusi(swasta) {
            return swasta === false ? 'Negeri' : 'Swasta';
        },
        /**
         * Returns formatted text of Jenis Kelamin.
         *
         * @param {string} jns Kode jenis kelamin
         * @return {string} Jenis kelamin
         */
        jenisKelamin(jns) {
            switch (jns) {
                case 'L' :
                    return 'Laki-laki';
                case 'P' :
                    return 'Perempuan';
                default:
                    return ''
            }
        },
        /**
         * Create mail URL from email address.
         *
         * @param {string} email The email address
         * @return {string} Email Url
         */
        mailto(email) {
            return "mailto:" + email;
        },
        /**
         * Mask alamat email dengan karakter tertentu.
         *
         * @param {string} email Alamat email yang akan di mask
         * @return {string} Alamat email yang telah dimask. Example: <code>ah******@domain.com</code>
         */
        maskEmail(email) {
            if (!Helper.isEmpty(email)) {
                const splits = email.split('@');
                const name   = splits[0].substr(0, 2);

                return name.padEnd(12, '*') + '@' + splits[1];
            }

            return '';
        },
        /**
         * Format numeric value based on current Locale.
         *
         * @param {number} value The value to format with current locale
         * @return {string} Number formatted
         */
        moneyFormat(value) {
            if (this.smallNumberFormat === null) {
                this.smallNumberFormat = new Intl.NumberFormat(this.getLocale, {maximumFractionDigits: 2})
            }

            return this.smallNumberFormat.format(value);
        },
        /**
         * Format numeric value based on current locale and display as percentage.
         *
         * @param {Number|String} value The value to format with current locale
         * @return {String} The formatted number
         */
        percentFormat(value) {
            if (this.smallNumberFormat === null) {
                this.smallNumberFormat = new Intl.NumberFormat(this.getLocale, {maximumFractionDigits: 2})
            }
            if (!Helper.isEmpty(value)) {
                return this.smallNumberFormat.format(value) + '%';
            }

            return '';
        },
        /**
         * Make the given string Title Case.
         *
         * @param {string} str The string to format
         * @return {string} The formatted string
         */
        titleCase(str) {
            return str.toLowerCase().split(' ').map(word => {
                return (word.charAt(0).toUpperCase() + word.slice(1));
            }).join(' ');
        },
        /**
         * Converts all the alphabetic characters in a string to uppercase.
         *
         * @param {string} str The string to UpperCase
         * @return {string} The formatted string
         */
        upperCase(str) {
            return str == null ? '' : str.toUpperCase();
        }
    }
}
