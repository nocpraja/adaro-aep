export default {
    data: () => ({
        // follow Bootstrap4 MediaQuery
        mobile: window.matchMedia('(max-width: 991.98px)'),
        screenLg: window.matchMedia('(min-width: 992px)'),
        screenXL: window.matchMedia('(min-width: 1200px)')
    })
}
