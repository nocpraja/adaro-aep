const formatConst = {
    dateTimeISO: 'YYYY-MM-DD HH:mm:ss',
    shortISO: 'YYYY-MM-DD',
    shortDate: 'DD-MM-YYYY',
    fullDate: 'DD MMMM YYYY',
    simpleDate: 'DD MMM YYYY',
    simpleDateTime: 'DD MMM YYYY HH:mm',
    simpleFormatTime: 'DD/MM/YYYY HH:mm',
    dateTime: 'DD-MM-YYYY HH:mm:ss',
    dateTimeCustom: 'DD MMM YYYY HH:mm',
};

export default formatConst;
