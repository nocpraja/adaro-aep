import {Helper} from "vue-bs4";
import {mapGetters, mapMutations} from "vuex";
import {
    GET_ACTIVE_INSTITUSI,
    GET_ACTIVE_PRGCSR,
    GET_ACTIVE_PRGNAME,
    GET_ACTIVE_PRGSTATE,
    GET_ACTIVE_PROGRAM,
    GET_ACTIVE_YEAR,
    SET_ACTIVE_INSTITUSI,
    SET_ACTIVE_PRGCSR,
    SET_ACTIVE_PRGNAME,
    SET_ACTIVE_PRGSTATE,
    SET_ACTIVE_PROGRAM,
    SET_ACTIVE_YEAR
} from "../../store/modules/Kegiatan/mutation-types";
import {CLOSED, STEP_APPROVAL, STEP_REJECT, STEP_UPDATE, STEP_VERIFY} from "../../store/modules/mutation-types";
import DanaBatchModel from "../../models/Pendanaan/DanaBatchModel";

export default {
    data: () => ({
        enablePaging: false,
        programModel: new DanaBatchModel(),
        showLoader: false,
        showModalDelete: false,
        showModalProcess: false,
        approval: {
            action: '',
            reason: ''
        }
    }),
    beforeDestroy() {
        this.programModel.destroy();
        this.programModel = null;
        this.approval = null;
    },
    computed: {
        ...mapGetters('Kegiatan/Beneficiary', [
            GET_ACTIVE_PROGRAM, GET_ACTIVE_PRGNAME, GET_ACTIVE_PRGCSR, GET_ACTIVE_PRGSTATE,
            GET_ACTIVE_YEAR, GET_ACTIVE_INSTITUSI
        ]),
        /**
         * Calculate column Tindakan cell alignment.
         *
         * @return {string|null} Cell alignment
         */
        cellTindakanAlignment() {
            return (this.hasEditorAccessRights || this.hasDeleteAccessRights || this.hasVerificatorAccessRights || this.hasApprovalAccessRights)
                ? null : 'center';
        },
        /**
         * Calculate column Tindakan cell width.
         *
         * @return {number} Cell width
         */
        cellTindakanWidth() {
            let width = 45;

            if (this.hasEditorAccessRights) {
                width += 25
            }
            if (this.hasDeleteAccessRights) {
                width += 25
            }
            if (this.hasVerificatorAccessRights) {
                width += 25
            }
            if (this.hasApprovalAccessRights) {
                width += 25
            }
            return this.hasAdminRole ? 115 : width < 90 ? 90 : width;
        },
        /**
         * Check status button <b>TAMBAH BARU</b>, apakah enabled atau disabled.
         *
         * @return {boolean} TRUE if disabled otherwise FALSE
         */
        disableAddButton() {
            return this[GET_ACTIVE_PRGSTATE] === CLOSED;
        },
    },
    methods: {
        ...mapMutations('Kegiatan/Beneficiary', [
            SET_ACTIVE_PROGRAM, SET_ACTIVE_PRGNAME, SET_ACTIVE_PRGCSR, SET_ACTIVE_PRGSTATE,
            SET_ACTIVE_YEAR, SET_ACTIVE_INSTITUSI
        ]),
        /**
         * Fetch data program Dana Batch tahunan.
         *
         * @param {int|string} id    Program ID
         * @param {boolean} mainPage Laman sumber yang melakukan fetch data
         * @return {void}
         */
        fetchDpt(id, mainPage = false) {
            if (!Helper.isEmpty(id)) {
                this.programModel.fetch(id).then(() => {
                    this[SET_ACTIVE_YEAR](this.programModel.tahun);
                    this[SET_ACTIVE_PROGRAM](this.programModel.id);
                    this[SET_ACTIVE_PRGNAME](this.programModel.namaProgram);
                    this[SET_ACTIVE_PRGSTATE](this.programModel.status);
                    this[SET_ACTIVE_PRGCSR](this.programModel.programCsrId);
                }).catch(() => {
                    if (mainPage === false) {
                        this.$notification.error('DPT yang dipilih tidak benar.');
                        this.$router.push('/kegiatan/penerima-manfaat');
                    }
                });
            }
        },
        /**
         * Event handler triggered after record filtered.
         *
         * @return {void}
         */
        onAfterSearch() {
            this.enablePaging = true;
        },
        /**
         * Periksa apakah data grid ditampilkan atau tidak.
         *
         * @return {boolean} TRUE jika ditampilkan, dan FALSE jika sebaliknya
         */
        showGrid() {
            if (this.dataSource) {
                return this.dataSource.dataItems.length > 0;
            }

            return false;
        },
        /**
         * @param {TechBaseModel|BeasiswaModel|InstitusiModel} item Data model
         * @return {boolean} TRUE jika item data dapat disunting
         */
        canEdit(item) {
            return (this.hasEditorAccessRights && this.workflow.canXor(item, [STEP_UPDATE, STEP_VERIFY, STEP_REJECT]));
        },
        /**
         * @param {TechBaseModel|BeasiswaModel} item Data model
         * @return {boolean} TRUE jika item data dapat dihapus
         */
        canDelete(item) {
            return (item.biodata.statusVerifikasi === 0) &&
                (this[GET_ACTIVE_PRGSTATE] !== CLOSED) &&
                (this.hasDeleteAccessRights && this.workflow.canXor(item, [STEP_UPDATE, STEP_VERIFY, STEP_REJECT]));
        },
        /**
         * @param {TechBaseModel|BeasiswaModel} item Data model
         * @return {boolean} TRUE jika item belum diverifikasi
         */
        canVerify(item) {
            return (item.biodata.statusVerifikasi === 0) && (this.hasVerificatorAccessRights && this.workflow.can(item, STEP_VERIFY));
        },
        /**
         * @param {TechBaseModel|BeasiswaModel} item Data model
         * @return {boolean} TRUE jika item belum diverifikasi
         */
        canApprove(item) {
            return (item.biodata.statusVerifikasi === 1) && (this.hasApprovalAccessRights && this.workflow.can(item, STEP_APPROVAL));
        },

        /**
         * @param {InstitusiModel|IndividuModel} item Data model
         * @return {boolean} TRUE jika item data dapat dihapus
         */
        insDelete(item) {
            return (item.statusVerifikasi === 0) &&
                (this[GET_ACTIVE_PRGSTATE] !== CLOSED) &&
                (this.hasDeleteAccessRights && this.workflow.canXor(item, [STEP_UPDATE, STEP_VERIFY, STEP_REJECT]));
        },
        /**
         * @param {InstitusiModel|IndividuModel} item Data model
         * @return {boolean} TRUE jika item belum diverifikasi
         */
        insVerify(item) {
            return (item.statusVerifikasi === 0) && (this.hasVerificatorAccessRights && this.workflow.can(item, STEP_VERIFY));
        },
        /**
         * @param {InstitusiModel|IndividuModel} item Data model
         * @return {boolean} TRUE jika item belum diverifikasi
         */
        insApprove(item) {
            return (item.statusVerifikasi === 1) && (this.hasApprovalAccessRights && this.workflow.can(item, STEP_APPROVAL));
        },
    }
}
