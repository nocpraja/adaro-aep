import mapStoreKegiatanBeneficiary from "./mapStoreKegiatanBeneficiary";

export default {
    mixins: [mapStoreKegiatanBeneficiary],
    methods: {
        /**
         * Format nama institusi SMK atupun Poltek.
         *
         * @param {string} name Nama institusi SMK/Poltek
         * @return {string} Nama institusi
         */
        fmtNamaInstitusi(name) {
            if (name != null && name.toLowerCase().startsWith(this.getActiveCategory().toLowerCase())) {
                return name;
            }

            const tipe = this.enumIdJurusan() === 2 ? this.titleCase(this.getActiveCategory()) : 'SMK';

            return `${tipe} ${name}`;
        },
        /**
         * Format nama lokasi SMK ataupun Poltek.
         *
         * @param {Object} model Data model
         * @return {string} Lokasi SMK ataupun Poltek
         */
        fmtLokasiInstitusi(model) {
            if (model && model.kecamatan) {
                const kabupaten = model.kecamatan.kabupaten.namaKabupaten;
                const provinsi = model.kecamatan.kabupaten.provinsi.namaProvinsi;

                return `${kabupaten}, ${provinsi}`;
            }

            return '';
        },
        /**
         * Get field label.
         *
         * @param {string} label Tipe label
         * @return {string} Nama Label
         **/
        generateLabel(label) {
            const kategori  = this.getActiveCategory();
            const listLabel = {
                title: kategori === 'poltek'
                    ? !this.isEditMode ? 'Poltek Baru' : 'Sunting Poltek'
                    : !this.isEditMode ? 'SMK Baru' : 'Sunting SMK',
                namaInstitusi: kategori === 'poltek' ? 'Nama Institusi' : 'Nama Sekolah',
                namaPimpinan: kategori === 'poltek' ? 'Nama Rektor' : 'Nama Kepala Sekolah',
                kepalaJurusan: kategori === 'poltek' ? 'Dekan' : 'Kepala Jurusan',
                tipeJurusan: kategori === 'poltek' ? 'Program Studi' : 'Program Keahlian',
                tipePendidik: kategori === 'poltek' ? 'Dosen' : 'Guru',
                tipeSiswa: kategori === 'poltek' ? 'mahasiswa' : 'siswa',
                jmlPendidik: kategori === 'poltek' ? 'Jml. Dosen' : 'Jml. Guru',
                jmlSiswa: kategori === 'poltek' ? 'Jml. Mahasiswa' : 'Jml. Siswa',
                alamat: kategori === 'poltek' ? 'Alamat Institusi' : 'Alamat Sekolah'
            };

            return listLabel[label];
        },
        /**
         * Get kategori institusi dari Route URL.
         *
         * @return {string} Jenis kategori institusi
         **/
        getActiveCategory() {
            return this.$route.params.kategori;
        },
        /**
         * Check apakah nama institusi diawali dengan SMK ataupun Poltek.
         *
         * @param {string} name Nama institusi SMK/Poltek
         * @return {boolean} TRUE jika diawali dengan SMK ataupun Poltek
         */
        nameStartWithTipe(name) {
            return name != null ? name.toLowerCase().startsWith(this.getActiveCategory().toLowerCase()) : false;
        },
        /**
         * Get enum ID dari <b>Program Keahlian</b> ataupun <b>Program Studi</b>.
         *
         * @return {int} The enumeration ID
         */
        enumIdJurusan() {
            return this.getActiveCategory() === 'poltek' ? 2 : 1;
        }
    }
}
