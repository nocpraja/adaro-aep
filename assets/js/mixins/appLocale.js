import { mapMutations } from "vuex";
import { GET_LOCALE, SET_LOCALE } from "../store/modules/mutation-types";

export default {
    computed: {
        /**
         * Get default LOCALE to be used by the application.
         *
         * @return {string} Kode locale
         */
        getLocale() {
            return this.$store.getters[GET_LOCALE];
        }
    },
    methods: {
        ...mapMutations({
            /**
             * Set default LOCALE to be used by the application.
             *
             * @param {string} locale
             */
            'setLocale': SET_LOCALE
        })
    }
}
