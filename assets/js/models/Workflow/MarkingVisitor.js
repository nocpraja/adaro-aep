import { Helper } from "vue-bs4";
import Marking from "./Marking";

/**
 * MarkingVisitor class.
 *
 * @author Ahmad Fajar
 * @since  28/04/2019 modified: 08/05/2019 1:41
 */
export default class MarkingVisitor {

    /**
     * Class constructor.
     *
     * @param {String} property Subject field property
     */
    constructor(property = 'marking') {
        this._property = property;
    }

    /**
     * Get a Marking from a given subject.
     *
     * @param {Object} subject An object to get the marking state
     * @return {Marking} A marking for the given subject
     */
    getMarking(subject) {
        if (!Helper.isObject(subject)) {
            throw new Error('Parameter "subject" must be an object.');
        }

        const method = 'get' + this._property;

        if (!subject.hasOwnProperty(this._property) && !Helper.isFunction(subject[method])) {
            const name = subject.constructor.name;
            throw new Error(`Property or method "${name}::${this._property}" does not exists.`);
        }

        const place = subject.hasOwnProperty(this._property) ? subject[this._property] : subject[method]();

        if (!place) {
            return new Marking();
        } else {
            return new Marking([place]);
        }
    }

    /**
     * Set a Marking to a given subject.
     *
     * @param {Object} subject An object to marked
     * @param {Marking} marking A marking to add
     * @return {void}
     */
    setMarking(subject, marking) {
        if (!Helper.isObject(subject)) {
            throw new Error('Parameter "subject" must be an object.');
        }
        if (!(marking instanceof Marking)) {
            throw new Error('Parameter "marking" must be instance of "Marking".');
        }

        const method = 'set' + this._property;

        if (!subject.hasOwnProperty(this._property) && !Helper.isFunction(subject[method])) {
            const name = subject.constructor.name;
            throw new Error(`Property or method "${name}::${this._property}" does not exists.`);
        }

        const place = marking.isEmpty() ? null : marking.places[0];

        if (Helper.isFunction(subject[method])) {
            subject[method](place);
        } else {
            subject[this._property] = place;
        }
    }

}
