/**
 * Transition class.
 *
 * @author Ahmad Fajar
 * @since  28/04/2019 modified: 29/04/2019 4:16
 */
export default class Transition {

    /**
     * Class constructor.
     *
     * @param {String[]} froms  Any valid start marking places
     * @param {String} to       Next marking place
     * @param {String} name     Workflow transition name
     */
    constructor(froms, to, name) {
        this._froms = froms;
        this._to    = to;
        this._name  = name;
    }

    /**
     * Destroy and removes all registered transition's places.
     *
     * @return {void}
     */
    destroy() {
        this._froms = null;
    }

    /**
     * Transition start marking places.
     *
     * @type {String[]}
     */
    get fromPlaces() {
        return this._froms;
    }

    /**
     * Transition next marking place.
     *
     * @type {String}
     */
    get toPlace() {
        return this._to;
    }

    /**
     * Transition name.
     *
     * @type {String}
     */
    get name() {
        return this._name;
    }

}
