import Workflow from "./Workflow";
import MarkingVisitor from "./MarkingVisitor";

/**
 * BaseConfigurer class.
 *
 * @author Ahmad Fajar
 * @since  29/04/2019 modified: 01/05/2019 23:56
 */
export default class BaseConfigurer {

    /**
     * Class constructor.
     *
     * @param {string} name         A workflow name
     * @param {string} property     A property name for marking state
     * @param {string} startPlace   A marking initial place
     */
    constructor(name, property, startPlace) {
        this._name        = name;
        this._property    = property;
        this._startplace  = startPlace;
        this._createPlaces();
        this._createTransitions();
        this._createWorkflow();
    }

    /**
     * Finally create a workflow.
     *
     * @return {void}
     * @private
     */
    _createWorkflow() {
        this._workflow = new Workflow(
            new MarkingVisitor(this._property), this._transitions,
            this._places, this._startplace, this._name
        );
    }

    /**
     * Destroy and removes all registered transition's places.
     *
     * @return {void}
     */
    destroy() {
        this._workflow.destroy();
        this._workflow = null;
        this._places   = null;

        for (const transition of this._transitions) {
            transition.destroy();
        }
        this._transitions = null;
    }

    /**
     * Get a workflow.
     *
     * @type {Workflow}
     */
    get workflow() {
        return this._workflow;
    }

}
