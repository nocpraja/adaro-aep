/**
 * Marking class.
 *
 * @author Ahmad Fajar
 * @since  28/04/2019 modified: 29/04/2019 4:15
 */
export default class Marking {

    /**
     * Class constructor.
     *
     * @param {String[]} places Array of place names
     */
    constructor(places = []) {
        this._places = places;
    }

    /**
     * Destroy and removes all registered places.
     *
     * @return {void}
     */
    destroy() {
        this._places = null;
    }

    /**
     * Check if Marking object contains a place or not.
     *
     * @param {string} place A place to search
     * @return {boolean} TRUE if Marking has the place, otherwise FALSE
     */
    hasPlace(place) {
        return this.places.includes(place);
    }

    /**
     * Check whether Marking does not contains any place or not.
     *
     * @return {boolean} TRUE if marking is empty otherwise FALSE
     */
    isEmpty() {
        return this._places.length === 0;
    }

    /**
     * Add a place to Marking object instance.
     *
     * @param {String} place A place to add
     * @return {void}
     */
    mark(place) {
        this._places.push(place);
    }

    /**
     * Remove a place from Marking object instance.
     *
     * @param {String} place A place to remove
     * @return {void}
     */
    unmark(place) {
        const idx = this._places.indexOf(place);

        if (idx > -1) {
            this._places.splice(idx, 1);
        }
    }

    /**
     * Get all available marking places.
     *
     * @type {String[]}
     */
    get places() {
        return this._places;
    }

}
