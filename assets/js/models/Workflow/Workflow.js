import { Helper } from "vue-bs4";
import MarkingVisitor from "./MarkingVisitor";

/**
 * Workflow class.
 *
 * @author Ahmad Fajar
 * @since  29/04/2019 modified: 29/04/2019 4:22
 */
export default class Workflow {

    /**
     * Class constructor.
     *
     * @param {MarkingVisitor} checker       Object marking checker
     * @param {Transition[]} transitions     Array of workflow transitions
     * @param {String[]} validPlaces         Array of valid places
     * @param {String|String[]} startPlaces  Initial place before workflow begin
     * @param {String} name                  The workflow name
     */
    constructor(checker, transitions, validPlaces, startPlaces, name) {
        if (!(checker instanceof MarkingVisitor)) {
            throw new Error('Parameter "checker" must be instance of "MarkingVisitor".');
        }

        this._checker      = checker;
        this._workflowName = name;
        this._transitions  = Helper.isArray(transitions) ? transitions : [transitions];
        this._places       = Helper.isArray(validPlaces) ? validPlaces : [validPlaces];
        this._startPlaces  = Helper.isArray(startPlaces) ? startPlaces : [startPlaces];
    }

    /**
     * Check if the transition is enabled or not.
     *
     * @param {Object} subject A subject to check
     * @param {String} name    A transition name
     * @return {boolean} TRUE if the transition is enabled otherwise FALSE
     */
    can(subject, name) {
        if (!Helper.isObject(subject)) {
            throw new Error('Parameter "subject" must be an object.')
        }

        try {
            const marking = this.getMarking(subject);

            for (const transition of this._transitions) {
                if (transition.name !== name) {
                    continue;
                }

                const blockerList = Workflow.buildBlockerListForTransition(marking, transition);

                if (blockerList.length === 0) {
                    return true;
                }
            }
        } catch (e) {
            return false;
        }

        return false;
    }

    /**
     * Check if one or more of the given transition is enabled or not.
     *
     * @param {Object} subject A subject to check
     * @param {String[]} names Array of transition name to check
     * @return {boolean} TRUE if there is at least one transition enabled otherwise FALSE
     */
    canXor(subject, names) {
        for (const name of names) {
            if (this.can(subject, name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Destroy and removes all registered transition's places.
     *
     * @return {void}
     */
    destroy() {
        this._checker     = null;
        this._places      = null;
        this._startPlaces = null;

        for (const transition of this._transitions) {
            transition.destroy();
        }
        this._transitions = null;
    }

    /**
     * Get the Object's marking.
     *
     * @param {Object} subject An object to check
     * @return {Marking} Object's marking
     */
    getMarking(subject) {
        const marking = this._checker.getMarking(subject);

        if (marking.isEmpty()) {
            for (const place of this._startPlaces) {
                marking.mark(place);
            }
        }

        for (const place of marking.places) {
            if (!this._places.includes(place)) {
                throw new Error(`Place "${place}" is not valid for workflow "${this._workflowName}".`);
            }
        }

        return marking;
    }

    /**
     * Get a MarkingVisitor used to check a marking.
     *
     * @return {MarkingVisitor} A MarkingVisitor
     */
    get markingVisitor() {
        return this._checker;
    }

    /**
     * Get all available transitions in the workflow instance.
     *
     * @return {Transition[]} Array of transitions
     */
    get transitions() {
        return this._transitions;
    }

    /**
     * Get the workflow name.
     *
     * @return {String} Workflow name
     */
    get workflowName() {
        return this._workflowName;
    }

    /**
     * Build a blocker untuk memeriksa validitas suatu transition.
     *
     * @param {Marking} marking        Current marking place
     * @param {Transition} transition  The transition to check
     * @return {Error[]} Error objects
     */
    static buildBlockerListForTransition(marking, transition) {
        let found = false;

        for (const place of transition.fromPlaces) {
            if (marking.hasPlace(place)) {
                found = true;
                break;
            }
        }

        if (!found) {
            return [new Error('Blocked by Marking')];
        }

        return [];
    }

}
