import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class UserAccountModel.
 * Schema model UserAccount.
 *
 * @author Suhendra
 * @since  25/07/2018, modified: 30/04/2019 3:58
 */
export class UserAccountModel extends BsModel {
    /**
     * @property {int} uid ID pengguna
     */

    /**
     * @property {string} username Akun pengguna
     */

    /**
     * @property {string} fullName Nama lengkap pengguna
     */

    /**
     * @property {string} userPassword Password akun pengguna
     */

    /**
     * @property {string} email Alamat email akun pengguna
     */

    /**
     * @property {boolean} enabled Apakah akun pengguna aktif atau tidak?
     */

    /**
     * @property {int} groupId ID group pengguna
     */


    /**
     * Class constructor.
     *
     * @param {Object} [schema] The data model schema
     */
    constructor(schema = {
        uid: null,
        username: null,
        fullName: null,
        userPassword: null,
        email: null,
        enabled: true,
        group: null,
        mitraCompany: null,
    }) {
        super(schema, 'uid');
        Vue.set(this, 'groupId', null);
        Vue.set(this, 'mitraId', null);
        Vue.set(this, 'roles', []);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'roles', data.group ? data.group.roles : null);
        Vue.set(this, 'groupId', data.group ? data.group.groupId : null);
        Vue.set(this, 'mitraId', data.mitraCompany ? data.mitraCompany.mitraId : null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object} The REST URL configuration
     */
    get restUrl() {
        return {
            'save': '/security/user/create',
            'delete': '/security/user/delete/{id}',
            'fetch': '/security/user/view/{id}',
            'update': '/security/user/update/{id}'
        }
    }
}
