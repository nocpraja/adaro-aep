import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class UserGroupModel
 *
 * @author  Tri N
 * @since   29/07/20018, modified: 18/05/2019 13:43
 */
export class UserGroupModel extends BsModel {
    /**
     * @property {int} groupId ID group pengguna
     */

    /**
     * @property {string} groupName Nama group pengguna
     */

    /**
     * @property {string} description Keterangan tentang group pengguna
     */

    /**
     * @property {string[]} roles Roles untuk diapply pada group pengguna
     */

    /**
     * @property {boolean} enabled Apakah group pengguna aktif atau tidak?
     */

    /**
     * @property {boolean} protected Apakah group pengguna terproteksi atau tidak?
     */


    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model properties
     */
    constructor(schema = {
        groupId: null,
        groupName: null,
        description: null,
        roles: null,
        enabled: true
    }) {
        super(schema, 'groupId');
        Vue.set(this, 'protected', false);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'protected', data.protected);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/security/group/create',
            'delete': '/security/group/delete/{id}',
            'fetch': '/security/group/view/{id}',
            'update': '/security/group/update/{id}'
        }
    }
}