/**
 * Function UserGroupValidator
 *
 * @author  Tri N
 * @since   29/07/20018, modified: 02/08/2018 11:06
 */
import { maxLength, minLength, required } from 'vuelidate/lib/validators';

/**
 * Schema UserGroup's Validator
 *
 * @return {Object}
 */
export const userGroupValidator = {
    groupName: {
        required,
        minLength: minLength(5),
        maxLength: maxLength(200)
    },
    description: {
        minLength: minLength(20),
        maxLength: maxLength(500)
    }
};

/**
 * UserGroup's Validation Handler.
 *
 * @param {string} field Data property name
 * @return {Object} Validation handler
 */
export function handleUserGroupValidator(field) {
    return {
        computed: {
            groupNameValidation() {
                return {
                    hasError: this.$v[field].groupName.$error,
                    messages: {
                        required: 'Field nama grup wajib diisi',
                        minLength: 'Field nama grup setidaknya harus memiliki 5 karakter',
                        maxLength: 'Field nama grup hanya bisa memiliki paling banyak 200 karakter'
                    },
                    validators: {
                        required: this.$v[field].groupName.required,
                        minLength: this.$v[field].groupName.minLength,
                        maxLength: this.$v[field].groupName.maxLength
                    }
                }
            },
            descriptionValidation() {
                return {
                    hasError: this.$v[field].description.$error,
                    messages: {
                        minLength: 'Field Deskripsi setidaknya harus memiliki 20 karakter',
                        maxLength: 'Field Deskripsi hanya bisa memiliki paling banyak 500 karakter'
                    },
                    validators: {
                        minLength: this.$v[field].description.minLength,
                        maxLength: this.$v[field].description.maxLength
                    }
                }
            }
        }
    }
}
