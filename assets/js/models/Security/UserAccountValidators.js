/**
 * Function UserAccountValidator
 *
 * @author  Suhendra
 * @since   26/07/20018, modified: 30/04/2019 3:59
 */
import { required, requiredUnless, requiredIf, email, minLength, maxLength } from "vuelidate/lib/validators";

/**
 * Schema UserAccount's Validator
 *
 * @return {Object}
 */
export const userAccountValidator = {
    username: {
        required,
        minLength: minLength(4),
        maxLength: maxLength(50)
    },
    fullName: {
        required,
        minLength: minLength(6),
        maxLength: maxLength(250)
    },
    userPassword: {
        required: requiredUnless(function () {
            return this.isFormUserEdit
        }),
        minLength: minLength(5),
        maxLength: maxLength(50)
    },
    email: {
        required,
        email,
        maxLength: maxLength(100)
    },
    groupId: {
        required
    },
    mitraId: {
        required: requiredIf(function () {
            return this.user.roles.includes('ROLE_MITRA');
        })
    }
};

/**
 * UserAccount's Validation Handler
 *
 * @param {string} field Field property name
 * @return {Object} User account validation handler
 */
export function userAccountValidators(field) {
    return {
        computed: {
            usernameValidator() {
                return {
                    hasError: this.$v[field].username.$error,
                    messages: {
                        required: 'Field akun pengguna wajib diisi',
                        minLength: 'Field akun pengguna setidaknya harus memiliki 4 karakter',
                        maxLength: 'Field akun pengguna hanya bisa memiliki paling banyak 50 karakter'
                    },
                    validators: {
                        required: this.$v[field].username.required,
                        minLength: this.$v[field].username.minLength,
                        maxLength: this.$v[field].username.maxLength
                    }
                }
            },
            fullNameValidator() {
                return {
                    hasError: this.$v[field].fullName.$error,
                    messages: {
                        required: 'Field nama lengkap wajib diisi',
                        minLength: 'Field nama lengkap setidaknya harus memiliki 6 karakter',
                        maxLength: 'Field nama lengkap hanya bisa memiliki paling banyak 250 karakter'
                    },
                    validators: {
                        required: this.$v[field].fullName.required,
                        minLength: this.$v[field].fullName.minLength,
                        maxLength: this.$v[field].fullName.maxLength
                    }
                }
            },
            userPasswordValidator() {
                return {
                    hasError: this.$v[field].userPassword.$error,
                    messages: {
                        required: 'Field password wajib diisi',
                        minLength: 'Field password setidaknya harus memiliki 5 karakter',
                        maxLength: 'Field password hanya bisa memiliki paling banyak 50 karakter'
                    },
                    validators: {
                        required: this.$v[field].userPassword.required,
                        minLength: this.$v[field].userPassword.minLength,
                        maxLength: this.$v[field].userPassword.maxLength
                    }
                }
            },
            emailValidator() {
                return {
                    hasError: this.$v[field].email.$error,
                    messages: {
                        required: 'Field alamat email wajib diisi',
                        email: 'Format alamat email tidak benar',
                        maxLength: 'Field alamat email hanya bisa memiliki paling banyak 100 karakter'
                    },
                    validators: {
                        required: this.$v[field].email.required,
                        email: this.$v[field].email.email,
                        maxLength: this.$v[field].email.maxLength
                    }
                }
            },
            groupValidator() {
                return {
                    hasError: this.$v[field].groupId.$error,
                    messages: {
                        required: 'Field grup pengguna wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].groupId.required,
                    }
                }
            },
            mitraIdValidator() {
                return {
                    hasError: this.$v[field].mitraId.$error,
                    messages: {
                        required: 'Field Nama Mitra wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].mitraId.required,
                    }
                }
            },
        }
    }
}
