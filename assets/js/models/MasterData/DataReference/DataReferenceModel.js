import Vue from "vue";
import {BsModel} from "vue-bs4";

/**
 * Class DataReferenceModel
 *
 * @author Tri N
 * @since  07/02/2019, modified: 07/02/2019 09:16
 */
export class DataReferenceModel extends BsModel
{
    constructor(schema = {
        id:null,
        protected:null,
        code:null,
        name:null,
        params:[{property: '', value:'', type: 'STRING'}],
        description:null,
        parent:null
    }) {
        super(schema, 'id');
        Vue.set(this, 'parentId', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/master-data/data-reference/create',
            'fetch': '/master-data/data-reference/{id}',
            'delete': '/master-data/data-reference/delete/{id}',
            'update': '/master-data/data-reference/update/{id}'
        }
    }
}