/**
 * Validators untuk data model DataReference
 *
 * @author Tri N
 * @since  07/02/2019, modified: 07/02/2019 10:06
 */
import {required} from "vuelidate/lib/validators";

/**
 * DataReference Validation's model
 *
 * @type {Object}
 */
export const dataReferenceValidator = {
    code:{required},
    name:{required}
};

/**
 * Validation Handler
 *
 * @param field
 *
 * @returns {object}
 */
export function dataReferenceValidationHandler(field) {
    return {
        computed: {
            codeValidator() {
                return {
                    hasError: this.$v[field].code.$error,
                    messages: {
                        required: 'Field kode referensi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].code.required
                    }
                };
            },
            nameValidator() {
                return {
                    hasError: this.$v[field].name.$error,
                    messages: {
                        required: 'Field nama referensi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].name.required
                    }
                };
            },
        }
    }
}