import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class data model Batch Pembinaan
 *
 * @author Ahmad Fajar
 * @since  07/11/2018, modified: 13/02/2019 21:02
 */
export class BatchModel extends BsModel {
    /**
     * Class constructor.
     *
     * @param {Object} [schema] Schema data model
     */
    constructor(schema = {
        batchId: null,
        namaBatch: null,
        tahunAwal: null,
        tahunAkhir: null,
        managementFee: 5,
        programCsr: null,
        mitra: null
    }) {
        super(schema, 'batchId');
        Vue.set(this, 'programId', null);
        Vue.set(this, 'programName', null);
        Vue.set(this, 'mitraId', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'fetch': '/master-data/batch/{id}',
            'save': '/master-data/batch/create',
            'delete': '/master-data/batch/delete/{id}',
            'update': '/master-data/batch/update/{id}'
        }
    }
}
