/**
 * Validator untuk data model Master Batch
 *
 * @author  Ahmad Fajar
 * @since   08/11/2018, modified: 04/12/2018 06:21
 */
import { integer, maxLength, required } from "vuelidate/lib/validators";

/**
 * Master Batch validation model.
 *
 * @type {Object}
 */
export const masterBatchValidator = {
    namaBatch: {required, maxLength: maxLength(100)},
    tahunAwal: {required, integer},
    tahunAkhir: {required, integer},
    managementFee: {required},
    mitraId: {required},
    programId: {required}
};

/**
 * Master Batch validation's handler.
 *
 * @param {string} field Data property name
 * @return {Object} Validation handler
 */
export function masterBatchValidationHandler(field) {
    return {
        computed: {
            mitraIdValidator() {
                return {
                    hasError: this.$v[field].mitraId.$error,
                    messages: {
                        required: 'Field Mitra Kerjasama wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].mitraId.required
                    }
                }
            },
            programIdValidator() {
                return {
                    hasError: this.$v[field].programId.$error,
                    messages: {
                        required: 'Field Program CSR wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].programId.required,
                    }
                }
            },
            namaBatchValidator() {
                return {
                    hasError: this.$v[field].namaBatch.$error,
                    messages: {
                        required: 'Field Nama Batch wajib diisi',
                        maxLength: 'Field Nama Batch hanya bisa memiliki paling banyak 100 karakter'
                    },
                    validators: {
                        required: this.$v[field].namaBatch.required,
                        maxLength: this.$v[field].namaBatch.maxLength
                    }
                }
            },
            managementFeeValidator() {
                return {
                    hasError: this.$v[field].managementFee.$error,
                    messages: {
                        required: 'Field Management Fee wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].managementFee.required,
                    }
                }
            },
            tahunAwalValidator() {
                return {
                    hasError: this.$v[field].tahunAwal.$error,
                    messages: {
                        required: 'Field Tahun Dimulai wajib diisi',
                        integer: 'Field Tahun Dimulai haruslah bertipe integer'
                    },
                    validators: {
                        required: this.$v[field].tahunAwal.required,
                        integer: this.$v[field].tahunAwal.integer
                    }
                }
            },
            tahunAkhirValidator() {
                return {
                    hasError: this.$v[field].tahunAkhir.$error,
                    messages: {
                        required: 'Field Tahun Berakhir wajib diisi',
                        integer: 'Field Tahun Berakhir haruslah bertipe integer'
                    },
                    validators: {
                        required: this.$v[field].tahunAkhir.required,
                        integer: this.$v[field].tahunAkhir.integer
                    }
                }
            }
        }
    }
}
