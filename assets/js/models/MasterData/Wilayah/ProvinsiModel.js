import {BsModel} from "vue-bs4";

/**
 * Class ProvinsiModel
 *
 * @author Ahmad Fajar
 * @since  04/09/2018, modified: 04/09/2018 17:56
 */
export class ProvinsiModel extends BsModel {
    /**
     * Class constructor.
     *
     * @param {Object} [schema]
     */
    constructor(schema = {
        provinsiId: null,
        kodeProvinsi: null,
        namaProvinsi: null
    }) {
        super(schema, 'provinsiId');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'fetch': '/master-data/wilayah/provinsi/{id}',
            'save': '/master-data/wilayah/provinsi/create',
            'delete': '/master-data/wilayah/provinsi/delete/{id}',
            'update': '/master-data/wilayah/provinsi/update/{id}'
        }
    }
}