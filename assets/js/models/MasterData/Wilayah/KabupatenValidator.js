/**
 * Validator untuk data model Kabupaten
 *
 * @author  Tri N
 * @since   25/09/2018, modified: 25/09/2018 11:06
 */
import {maxLength, minLength, required} from "vuelidate/lib/validators";

/**
 * Kabupaten validation model.
 *
 * @type {Object}
 */
export const kabupatenValidator = {
    provinsiId: {required},
    kodeKabupaten: {
        required,
        minLength: minLength(5),
        maxLength: maxLength(5)
    },
    namaKabupaten: {
        required,
        minLength: minLength(4)
    }
};

/**
 * Kabupaten validation's handler.
 *
 * @param {string} field
 * @return {Object}
 */
export function kabupatenValidationHandler(field) {
    return {
        computed: {
            provinsiIdValidator() {
                return {
                    hasError: this.$v[field].provinsiId.$error,
                    messages: {
                        required: 'Field Provinsi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].provinsiId.required
                    }
                }
            },
            kodeKabupatenValidator() {
                return {
                    hasError: this.$v[field].kodeKabupaten.$error,
                    messages: {
                        required: 'Field kode kabupaten wajib diisi',
                        minLength: 'Field kode kabupaten setidaknya harus memiliki 5 karakter',
                        maxLength: 'Field kode kabupaten hanya bisa memiliki paling banyak 5 karakter'
                    },
                    validators: {
                        required: this.$v[field].kodeKabupaten.required,
                        minLength: this.$v[field].kodeKabupaten.minLength,
                        maxLength: this.$v[field].kodeKabupaten.maxLength
                    }
                }
            },
            namaKabupatenValidator() {
                return {
                    hasError: this.$v[field].namaKabupaten.$error,
                    messages: {
                        required: 'Field nama kabupaten wajib diisi',
                        minLength: 'Field nama kabupaten setidaknya harus memiliki 4 karakter'
                    },
                    validators: {
                        required: this.$v[field].namaKabupaten.required,
                        minLength: this.$v[field].namaKabupaten.minLength
                    }
                }
            }
        }
    }
}
