/**
 * Validator untuk data model Kecamatan
 *
 * @author  Ahmad Fajar
 * @since   07/09/2018, modified: 07/09/2018 01:08
 */

import {maxLength, minLength, required} from "vuelidate/lib/validators";

/**
 * Provinsi validation model.
 *
 * @type {Object}
 */
export const kecamatanValidator = {
    provinsiId: {required},
    kabupatenId: {required},
    kodeKecamatan: {
        required,
        minLength: minLength(8),
        maxLength: maxLength(8)
    },
    namaKecamatan: {
        required,
        minLength: minLength(4)
    }
};

/**
 * Kecamatan validation's handler.
 *
 * @param {string} field
 * @return {Object}
 */
export function kecamatanValidationHandler(field) {
    return {
        computed: {
            provinsiIdValidator() {
                return {
                    hasError: this.$v[field].provinsiId.$error,
                    messages: {
                        required: 'Field Provinsi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].provinsiId.required
                    }
                }
            },
            kabupatenIdValidator() {
                return {
                    hasError: this.$v[field].kabupatenId.$error,
                    messages: {
                        required: 'Field Kabupaten wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kabupatenId.required
                    }
                }
            },
            kodeKecamatanValidator() {
                return {
                    hasError: this.$v[field].kodeKecamatan.$error,
                    messages: {
                        required: 'Field kode kecamatan wajib diisi',
                        minLength: 'Field kode kecamatan setidaknya harus memiliki 8 karakter',
                        maxLength: 'Field kode kecamatan hanya bisa memiliki paling banyak 8 karakter'
                    },
                    validators: {
                        required: this.$v[field].kodeKecamatan.required,
                        minLength: this.$v[field].kodeKecamatan.minLength,
                        maxLength: this.$v[field].kodeKecamatan.maxLength
                    }
                }
            },
            namaKecamatanValidator() {
                return {
                    hasError: this.$v[field].namaKecamatan.$error,
                    messages: {
                        required: 'Field nama kecamatan wajib diisi',
                        minLength: 'Field nama kecamatan setidaknya harus memiliki 4 karakter'
                    },
                    validators: {
                        required: this.$v[field].namaKecamatan.required,
                        minLength: this.$v[field].namaKecamatan.minLength
                    }
                }
            }
        }
    }
}