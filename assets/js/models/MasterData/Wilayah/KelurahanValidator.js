/**
 * Validator untuk data model Kecamatan
 *
 * @author  Mark Melvin
 * @since   12/09/2018
 */

import {maxLength, minLength, required} from "vuelidate/lib/validators";

/**
 * Provinsi validation model.
 *
 * @type {Object}
 */
export const kelurahanValidator = {
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kodeKelurahan: {
        required,
        minLength: minLength(13),
        maxLength: maxLength(13)
    },
    namaKelurahan: {
        required,
        minLength: minLength(4)
    }
};

/**
 * Kelurahan validation's handler.
 *
 * @param {string} field
 * @return {Object}
 */
export function kelurahanValidationHandler(field) {
    return {
        computed: {
            provinsiIdValidator() {
                return {
                    hasError: this.$v[field].provinsiId.$error,
                    messages: {
                        required: 'Field Provinsi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].provinsiId.required
                    }
                }
            },
            kabupatenIdValidator() {
                return {
                    hasError: this.$v[field].kabupatenId.$error,
                    messages: {
                        required: 'Field Kabupaten wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kabupatenId.required
                    }
                }
            },
            kecamatanIdValidator() {
                return {
                    hasError: this.$v[field].kecamatanId.$error,
                    messages: {
                        required: 'Field Kecamatan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kecamatanId.required
                    }
                }
            },
            kodeKelurahanValidator() {
                return {
                    hasError: this.$v[field].kodeKelurahan.$error,
                    messages: {
                        required: 'Field kode kelurahan wajib diisi',
                        minLength: 'Field kode kelurahan setidaknya harus memiliki 13 karakter',
                        maxLength: 'Field kode kelurahan hanya bisa memiliki paling banyak 13 karakter'
                    },
                    validators: {
                        required: this.$v[field].kodeKelurahan.required,
                        minLength: this.$v[field].kodeKelurahan.minLength,
                        maxLength: this.$v[field].kodeKelurahan.maxLength
                    }
                }
            },
            namaKelurahanValidator() {
                return {
                    hasError: this.$v[field].namaKelurahan.$error,
                    messages: {
                        required: 'Field nama kelurahan wajib diisi',
                        minLength: 'Field nama kelurahan setidaknya harus memiliki 4 karakter'
                    },
                    validators: {
                        required: this.$v[field].namaKelurahan.required,
                        minLength: this.$v[field].namaKelurahan.minLength
                    }
                }
            }
        }
    }
}