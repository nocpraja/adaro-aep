import Vue from "vue";
import {BsModel} from "vue-bs4";

/**
 * Class KelurahanModel
 *
 * @author Ahmad Fajar
 * @since  04/09/2018, modified: 04/09/2018 18:31
 */
export class KelurahanModel extends BsModel {
    /**
     * Class constructor.
     *
     * @param {Object} [schema]
     */
    constructor(schema = {
        kelurahanId: null,
        kodeKelurahan: null,
        namaKelurahan: null,
        kecamatan: null
    }) {
        super(schema, 'kelurahanId');
        Vue.set(this, 'kabupatenId', null);
        Vue.set(this, 'provinsiId', null);
        Vue.set(this, 'kecamatanId', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'fetch': '/master-data/wilayah/kelurahan/{id}',
            'save': '/master-data/wilayah/kelurahan/create',
            'delete': '/master-data/wilayah/kelurahan/delete/{id}',
            'update': '/master-data/wilayah/kelurahan/update/{id}'
        }
    }

}