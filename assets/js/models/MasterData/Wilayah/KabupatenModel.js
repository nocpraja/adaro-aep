import Vue from "vue";
import {BsModel} from "vue-bs4";

/**
 * Class KabupatenModel
 *
 * @author Ahmad Fajar
 * @since  04/09/2018, modified: 25/09/2018 11:07
 */
export class KabupatenModel extends BsModel {
    /**
     * Class constructor.
     *
     * @param {Object} [schema]
     */
    constructor(schema = {
        kabupatenId: null,
        kodeKabupaten: null,
        namaKabupaten: null,
        provinsi: null
    }) {
        super(schema, 'kabupatenId');
        Vue.set(this, 'provinsiId', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'fetch': '/master-data/wilayah/kabupaten/{id}',
            'save': '/master-data/wilayah/kabupaten/create',
            'delete': '/master-data/wilayah/kabupaten/delete/{id}',
            'update': '/master-data/wilayah/kabupaten/update/{id}'
        }
    }

}
