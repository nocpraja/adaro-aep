import Vue from "vue";
import {BsModel} from "vue-bs4";

/**
 * Class KecamatanModel
 *
 * @author Ahmad Fajar
 * @since  04/09/2018, modified: 07/09/2018 01:01
 */
export class KecamatanModel extends BsModel {
    /**
     * Class constructor.
     *
     * @param {Object} [schema]
     */
    constructor(schema = {
        kecamatanId: null,
        kodeKecamatan: null,
        namaKecamatan: null,
        kabupaten: null
    }) {
        super(schema, 'kecamatanId');
        Vue.set(this, 'kabupatenId', null);
        Vue.set(this, 'provinsiId', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'fetch': '/master-data/wilayah/kecamatan/{id}',
            'save': '/master-data/wilayah/kecamatan/create',
            'delete': '/master-data/wilayah/kecamatan/delete/{id}',
            'update': '/master-data/wilayah/kecamatan/update/{id}'
        }
    }

}