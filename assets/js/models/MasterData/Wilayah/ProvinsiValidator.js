/**
 * Validator untuk data model Provinsi
 *
 * @author  Ahmad Fajar
 * @since   06/09/2018, modified: 06/09/2018 09:54
 */

import {maxLength, minLength, required} from "vuelidate/lib/validators";

/**
 * Provinsi validation model.
 *
 * @type {Object}
 */
export const provinsiValidator = {
    kodeProvinsi: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(2)
    },
    namaProvinsi: {
        required,
        minLength: minLength(4)
    }
};

/**
 * Provinsi validation's handler.
 *
 * @param {string} field
 * @return {Object}
 */
export function provinsiValidationHandler(field) {
    return {
        computed: {
            kodeProvinsiValidator() {
                return {
                    hasError: this.$v[field].kodeProvinsi.$error,
                    messages: {
                        required: 'Field kode provinsi wajib diisi',
                        minLength: 'Field kode provinsi setidaknya harus memiliki 2 karakter',
                        maxLength: 'Field kode provinsi hanya bisa memiliki paling banyak 2 karakter'
                    },
                    validators: {
                        required: this.$v[field].kodeProvinsi.required,
                        minLength: this.$v[field].kodeProvinsi.minLength,
                        maxLength: this.$v[field].kodeProvinsi.maxLength
                    }
                }
            },
            namaProvinsiValidator() {
                return {
                    hasError: this.$v[field].namaProvinsi.$error,
                    messages: {
                        required: 'Field nama provinsi wajib diisi',
                        minLength: 'Field nama provinsi setidaknya harus memiliki 4 karakter'
                    },
                    validators: {
                        required: this.$v[field].namaProvinsi.required,
                        minLength: this.$v[field].namaProvinsi.minLength
                    }
                }
            }
        }
    }
}