import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class data model Mitra.
 *
 * @author Tri N
 * @since  24/01/2019, modified: 11/02/2019 2:55
 */
export class MitraModel extends BsModel {
    /**
     * @property {int|String} mitraId ID mitra
     */

    /**
     * @property {string} jenisLembaga Jenis lembaga
     */

    /**
     * @property {string} namaLembaga Nama lembaga mitra
     */

    /**
     * @property {string} kodeLembaga Kode mitra
     */

    /**
     * @property {int} msid ID program CSR
     */

    /**
     * @property {int} provinsiId ID provinsi
     */

    /**
     * @property {int} kabupatenId ID kabupaten
     */

    /**
     * @property {int} kecamatanId ID kecamatan
     */

    /**
     * @property {*} kecamatan Data model kecamatan
     */

    /**
     * @property {*} kelurahan Data model kelurahan
     */


    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        mitraId: null,
        jenisLembaga: null,
        namaLembaga: null,
        kodeLembaga: null,
        aktePendirian: null,
        namaPimpinan: null,
        picProgram: null,
        pimpinanPhone: null,
        picPhone: null,
        officePhone: null,
        email: null,
        website: null,
        latitude: null,
        longitude: null,
        alamat: null,
        rtRw: null,
        kodePos: null,
        kecamatan: null,
        kelurahan: null,
        programCsr: null
    }) {
        super(schema, "mitraId");
        Vue.set(this, 'msid', null);
        Vue.set(this, 'provinsiId', null);
        Vue.set(this, 'kabupatenId', null);
        Vue.set(this, 'kecamatanId', null);
        Vue.set(this, 'kelurahanId', null);
        Vue.set(this, 'postedBy', null);
        Vue.set(this, 'postedDate', null);
        Vue.set(this, 'updatedBy', null);
        Vue.set(this, 'lastUpdated', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/master-data/mitra/create',
            'fetch': '/master-data/mitra/{id}',
            'delete': '/master-data/mitra/delete/{id}',
            'update': '/master-data/mitra/update/{id}'
        }
    }
}
