/**
 * Validators untuk data model Mitra
 *
 * @author Tri N
 * @since  28/01/2019, modified: 24/05/2019 23:30
 */
import { email, required } from "vuelidate/lib/validators";
import { validRtRw } from "../../Beneficiary/validators";

/**
 * Mitra Validation's model
 *
 * @type {Object}
 */
export const mitraValidator = {
    kodeLembaga: {required},
    namaLembaga: {required},
    jenisLembaga: {required},
    aktePendirian: {required},
    email: {email},
    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * Mitra validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function mitraValidationHandler(field) {
    return {
        computed: {
            kodeLembagaValidator() {
                return {
                    hasError: this.$v[field].kodeLembaga.$error,
                    messages: {
                        required: 'Field kode mitra wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kodeLembaga.required
                    }
                };
            },
            namaLembagaValidator() {
                return {
                    hasError: this.$v[field].namaLembaga.$error,
                    messages: {
                        required: 'Field nama mitra wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaLembaga.required
                    }
                };
            },
            jenisLembagaValidator() {
                return {
                    hasError: this.$v[field].jenisLembaga.$error,
                    messages: {
                        required: 'Field kategori mitra wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].jenisLembaga.required
                    }
                };
            },
            emailValidator() {
                return {
                    hasError: this.$v[field].email.$error,
                    messages: {
                        required: 'Field email harus dalam format email yang valid',
                    },
                    validators: {
                        required: this.$v[field].email.email
                    }
                };
            },
            alamatMitraValidator() {
                return {
                    hasError: this.$v[field].alamat.$error,
                    messages: {
                        required: 'Field alamat mitra wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].alamat.required
                    }
                };
            },
            aktePendirianValidator() {
                return {
                    hasError: this.$v[field].aktePendirian.$error,
                    messages: {
                        required: 'Field akte pendirian wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].aktePendirian.required
                    }
                };
            }
        }
    }
}
