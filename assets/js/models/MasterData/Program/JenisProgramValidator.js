/**
 * Validator untuk data model Bidang CSR
 *
 * @author  Mark Melvin
 * @since  9/27/2018, modified: 02/10/2018 03:27
 */
import { maxLength, minLength, required } from "vuelidate/lib/validators";

/**
 * Bidang CSR validation model.
 *
 * @type {Object}
 */
export const jenisProgramValidator = {
    title: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(150)
    }
};

/**
 * Bidang CSR validation's handler.
 *
 * @param {string} field Data property name
 * @return {Object} Validation handler
 */
export function jenisProgramValidationHandler(field) {
    return {
        computed: {
            titleValidator() {
                return {
                    hasError: this.$v[field].title.$error,
                    messages: {
                        required: 'Field Nama Bidang wajib diisi',
                        minLength: 'Field Nama Bidang setidaknya harus memiliki 2 karakter',
                        maxLength: 'Field Nama Bidang hanya bisa memiliki paling banyak 150 karakter'
                    },
                    validators: {
                        required: this.$v[field].title.required,
                        minLength: this.$v[field].title.minLength,
                        maxLength: this.$v[field].title.maxLength
                    }
                }
            }
        }
    }
}
