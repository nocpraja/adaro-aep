import { BsModel } from "vue-bs4";

/**
 * Class JenisProgramModel
 *
 * @author Mark Melvin
 * @since  9/27/2018, modified: 27/04/2019 23:58
 */
export class JenisProgramModel extends BsModel {
    /**
     * @property {int} id
     * ID Bidang CSR
     */

    /**
     * @property {string} title
     * Nama Bidang CSR
     */

    /**
     * @property {string} description
     * Keterangan Bidang CSR
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        id: null,
        title: null,
        description: null
    }) {
        super(schema, 'id');
    }

    /**
     * Define REST URL configuration in the form <code>{key: url}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update', etc.
     *
     * @type {Object} REST URL configuration
     */
    get restUrl() {
        return {
            'fetch': '/master-data/bidang-csr/{id}',
            'save': '/master-data/bidang-csr/create',
            'delete': '/master-data/bidang-csr/delete/{id}',
            'update': '/master-data/bidang-csr/update/{id}'
        }
    }
}
