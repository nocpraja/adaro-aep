/**
 * Validator untuk data model Program CSR
 *
 * @author  Mark Melvin
 * @since  9/27/2018
 */
import { maxLength, minLength, required } from "vuelidate/lib/validators";

/**
 * Program CSR validation model.
 *
 * @type {Object}
 */
export const programValidator = {
    jenisId: {required},
    name: {
        required,
        minLength: minLength(2),
        maxLength: maxLength(200)
    },
    templateName: {
        maxLength: maxLength(250)
    }
};

/**
 * Program CSR validation's handler.
 *
 * @param {string} field Data property name
 * @return {Object} Validation handler
 */
export function programValidationHandler(field) {
    return {
        computed: {
            jenisIdValidator() {
                return {
                    hasError: this.$v[field].jenisId.$error,
                    messages: {
                        required: 'Field Bidang CSR wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].jenisId.required
                    }
                }
            },
            nameValidator() {
                return {
                    hasError: this.$v[field].name.$error,
                    messages: {
                        required: 'Field Program CSR wajib diisi',
                        minLength: 'Field Program CSR setidaknya harus memiliki 2 karakter',
                        maxLength: 'Field Program CSR hanya bisa memiliki paling banyak 200 karakter'
                    },
                    validators: {
                        required: this.$v[field].name.required,
                        minLength: this.$v[field].name.minLength,
                        maxLength: this.$v[field].name.maxLength
                    }
                }
            },
            templateNameValidator() {
                return {
                    hasError: this.$v[field].templateName.$error,
                    messages: {
                        maxLength: 'Field Template Nama Program hanya bisa memiliki paling banyak 250 karakter'
                    },
                    validators: {
                        maxLength: this.$v[field].templateName.maxLength
                    }
                }
            }
        }
    }
}
