import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class ProgramModel
 *
 * @author Mark Melvin
 * @since  9/27/2018, modified: 28/04/2019 0:00
 */
export class ProgramModel extends BsModel {
    /**
     * @property {int} msid
     * ID Program CSR
     */

    /**
     * @property {string} name
     * Nama Program CSR
     */

    /**
     * @property {string} templateName
     * String template untuk pembuatan nama Dana Batch
     */

    /**
     * @property {string} description
     * Keterangan Program CSR
     */

    /**
     * @property {int} jenisId
     * ID Bidang CSR
     */


    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        msid: null,
        name: null,
        templateName: null,
        description: null,
        jenis: null
    }) {
        super(schema, 'msid');
        Vue.set(this, 'jenisId', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: url}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update', etc.
     *
     * @type {Object} REST URL configuration
     */
    get restUrl() {
        return {
            'fetch': '/master-data/program-csr/{id}',
            'save': '/master-data/program-csr/create',
            'delete': '/master-data/program-csr/delete/{id}',
            'update': '/master-data/program-csr/update/{id}'
        }
    }
}
