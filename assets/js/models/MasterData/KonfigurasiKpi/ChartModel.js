import { BsModel } from "vue-bs4";

/**
 * Class ChartModel.
 */
export class ChartModel extends BsModel {

    /**
     * Class constructor.
     *
     * @param {Object} [schema] The data model schema
     */
    constructor(schema = {
        chartId: null,
        chartName: null,
        chartType: null,
        dataSource: null,
        dataConf: null,
        axisConf: null,
        chartDesc: null
    }) {
        super(schema, 'chartId');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/master-data/konfigurasi-kpi/chart/create',
            'fetch': '/master-data/konfigurasi-kpi/chart/{id}',
            'delete': '/master-data/konfigurasi-kpi/chart/delete/{id}',
            'update': '/master-data/konfigurasi-kpi/chart/update/{id}'
        }
    }
}
