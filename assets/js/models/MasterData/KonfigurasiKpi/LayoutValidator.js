import {required} from "vuelidate/lib/validators";

/**
 * @type {Object}
 */
export const LayoutValidator = {
    layoutName: {required},
    templateEditor: {required}
};

/**
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function LayoutValidationHandler(field) {
    return {
        computed: {
            layoutNameValidator() {
                return {
                    hasError: this.$v[field].layoutName.$error,
                    messages: {
                        required: 'Field nama layout wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].layoutName.required
                    }
                };
            },
            templateEditorValidator() {
                return {
                    hasError: this.$v[field].templateEditor.$error,
                    messages: {
                        required: 'Field template editor wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].templateEditor.required
                    }
                };
            }
        }
    };
}
