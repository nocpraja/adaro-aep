import {required} from "vuelidate/lib/validators";

/**
 * @type {Object}
 */
export const ChartValidator = {
    chartName: {required},
    chartType: {required},
    dataSource: {required},
    dataConf: {required},
    axisConf: {required}
};

/**
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function ChartValidationHandler(field) {
    return {
        computed: {
            chartNameValidator() {
                return {
                    hasError: this.$v[field].chartName.$error,
                    messages: {
                        required: 'Field nama chart wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].chartName.required
                    }
                };
            },
            chartTypeValidator() {
                return {
                    hasError: this.$v[field].chartType.$error,
                    messages: {
                        required: 'Field tipe chart wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].chartType.required
                    }
                };
            },
            dataSourceValidator() {
                return {
                    hasError: this.$v[field].dataSource.$error,
                    messages: {
                        required: 'Field data source wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].dataSource.required
                    }
                };
            },
            dataConfValidator() {
                return {
                    hasError: this.$v[field].dataConf.$error,
                    messages: {
                        required: 'Field data configuration wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].dataConf.required
                    }
                };
            },
            axisConfValidator() {
                return {
                    hasError: this.$v[field].axisConf.$error,
                    messages: {
                        required: 'Field axis configuration wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].axisConf.required
                    }
                };
            }
        }
    };
}
