import {required} from "vuelidate/lib/validators";

/**
 * @type {Object}
 */
export const KpiDataSourceValidator = {
    sourceName: {required},
    sourceType: {required},
    sourceQuery: {required}
};

/**
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function KpiDataSourceValidationHandler(field) {
    return {
        computed: {
            sourceNameValidator() {
                return {
                    hasError: this.$v[field].sourceName.$error,
                    messages: {
                        required: 'Field nama data source wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].sourceName.required
                    }
                };
            },
            sourceTypeValidator() {
                return {
                    hasError: this.$v[field].sourceType.$error,
                    messages: {
                        required: 'Field tipe wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].sourceType.required
                    }
                };
            },
            sourceQueryValidator() {
                return {
                    hasError: this.$v[field].sourceQuery.$error,
                    messages: {
                        required: 'Field query wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].sourceQuery.required
                    }
                };
            }
        }
    };
}
