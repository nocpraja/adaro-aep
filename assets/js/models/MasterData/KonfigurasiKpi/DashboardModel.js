import { BsModel } from "vue-bs4";

/**
 * Class DashboardModel.
 */
export class DashboardModel extends BsModel {

    /**
     * Class constructor.
     *
     * @param {Object} [schema] The data model schema
     */
    constructor(schema = {
        dashboardId: null,
        dashboardName: null,
        templateLayout: null,
        displayType: null,
        displaySource: null,
        dashboardComp: null,
        dashboardDesc: null
    }) {
        super(schema, 'dashboardId');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/master-data/konfigurasi-kpi/dashboard/create',
            'fetch': '/master-data/konfigurasi-kpi/dashboard/{id}',
            'delete': '/master-data/konfigurasi-kpi/dashboard/delete/{id}',
            'update': '/master-data/konfigurasi-kpi/dashboard/update/{id}'
        }
    }
}
