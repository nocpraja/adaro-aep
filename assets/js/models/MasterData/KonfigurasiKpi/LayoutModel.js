import { BsModel } from "vue-bs4";

/**
 * Class LayoutModel.
 */
export class LayoutModel extends BsModel {

    /**
     * Class constructor.
     *
     * @param {Object} [schema] The data model schema
     */
    constructor(schema = {
        layoutId: null,
        layoutName: null,
        templateEditor: null,
        layoutDesc: null
    }) {
        super(schema, 'layoutId');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/master-data/konfigurasi-kpi/layout/create',
            'fetch': '/master-data/konfigurasi-kpi/layout/{id}',
            'delete': '/master-data/konfigurasi-kpi/layout/delete/{id}',
            'update': '/master-data/konfigurasi-kpi/layout/update/{id}'
        }
    }
}
