import { BsModel } from "vue-bs4";

/**
 * Class ReportModel.
 */
export class ReportModel extends BsModel {

    /**
     * Class constructor.
     *
     * @param {Object} [schema] The data model schema
     */
    constructor(schema = {
        reportId: null,
        reportName: null,
        reportTitle: null,
        dataSource: null,
        reportFilter: null,
        reportSort: null
    }) {
        super(schema, 'reportId');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/master-data/konfigurasi-kpi/report/create',
            'fetch': '/master-data/konfigurasi-kpi/report/{id}',
            'delete': '/master-data/konfigurasi-kpi/report/delete/{id}',
            'update': '/master-data/konfigurasi-kpi/report/update/{id}'
        }
    }
}
