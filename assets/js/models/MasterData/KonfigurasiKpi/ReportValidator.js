import {required} from "vuelidate/lib/validators";

/**
 * @type {Object}
 */
export const ReportValidator = {
    reportName: {required},
    reportTitle: {required},
    dataSource: {required},
    reportFilter: {required},
    reportSort: {required}
};

/**
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function ReportValidationHandler(field) {
    return {
        computed: {
            reportNameValidator() {
                return {
                    hasError: this.$v[field].reportName.$error,
                    messages: {
                        required: 'Field nama report wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].reportName.required
                    }
                };
            },
            reportTitleValidator() {
                return {
                    hasError: this.$v[field].reportTitle.$error,
                    messages: {
                        required: 'Field title wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].reportName.required
                    }
                };
            },
            dataSourceValidator() {
                return {
                    hasError: this.$v[field].dataSource.$error,
                    messages: {
                        required: 'Field data source wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].dataSource.required
                    }
                };
            },
            reportFilterValidator() {
                return {
                    hasError: this.$v[field].reportFilter.$error,
                    messages: {
                        required: 'Field data filter wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].reportFilter.required
                    }
                };
            },
            reportSortValidator() {
                return {
                    hasError: this.$v[field].reportSort.$error,
                    messages: {
                        required: 'Field data sorting wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].reportSort.required
                    }
                };
            }
        }
    };
}
