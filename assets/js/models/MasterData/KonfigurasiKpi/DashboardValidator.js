import {required} from "vuelidate/lib/validators";

/**
 * @type {Object}
 */
export const DashboardValidator = {
    dashboardName: {required},
    templateLayout: {required},
    displayType: {required},
    dashboardComp: {required}
};

/**
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function DashboardValidationHandler(field) {
    return {
        computed: {
            dashboardNameValidator() {
                return {
                    hasError: this.$v[field].dashboardName.$error,
                    messages: {
                        required: 'Field nama dashboard wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].dashboardName.required
                    }
                };
            },
            templateLayoutValidator() {
                return {
                    hasError: this.$v[field].templateLayout.$error,
                    messages: {
                        required: 'Field layout wajib dipilih',
                    },
                    validators: {
                        required: this.$v[field].templateLayout.required
                    }
                };
            },
            displayTypeValidator() {
                return {
                    hasError: this.$v[field].displayType.$error,
                    messages: {
                        required: 'Field display type wajib dipilih',
                    },
                    validators: {
                        required: this.$v[field].displayType.required
                    }
                };
            },
            dashboardCompValidator() {
                return {
                    hasError: this.$v[field].dashboardComp.$error,
                    messages: {
                        required: 'Field data component wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].dashboardComp.required
                    }
                };
            }
        }
    };
}
