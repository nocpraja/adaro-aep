import { BsModel } from "vue-bs4";

/**
 * Class DataSourceModel.
 */
export class KpiDataSourceModel extends BsModel {

    /**
     * Class constructor.
     *
     * @param {Object} [schema] The data model schema
     */
    constructor(schema = {
        sourceId: null,
        sourceName: null,
        sourceType: null,
        sourceQuery: null,
        sourceDesc: null
    }) {
        super(schema, 'sourceId');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/master-data/konfigurasi-kpi/data-source/create',
            'fetch': '/master-data/konfigurasi-kpi/data-source/{id}',
            'delete': '/master-data/konfigurasi-kpi/data-source/delete/{id}',
            'update': '/master-data/konfigurasi-kpi/data-source/update/{id}',
            'query': '/master-data/konfigurasi-kpi/data-source/query/{id}',
        }
    }
}
