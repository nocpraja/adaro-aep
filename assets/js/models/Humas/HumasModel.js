import Vue from "vue";
import {BsModel} from "vue-bs4";

/**
 * Class HumasModel.
 *
 * @author Alex Gaspersz
 * @since  25/02/2020, modified: 25/02/2020 03:26
 */
export class HumasModel extends BsModel {

    /**
     * @property {int} bid
     */

    /**
     * @property {string} category
     */

    /**
     * @property {string} title
     */

    /**
     * @property {string} intro
     */

    /**
     * @property {string} content
     */

    /**
     * @property {string} status
     */

    /**
     * @property {string|null} filePath
     */

    /**
     * @property {string|null} fileName
     */

    /**
     * @property {string|null} fileExt
     */

    /**
     * @property {int|null} fileSize
     */

    /**
     * @property {string|null} fileUrl
     */

    /**
     * @property {string|null} fileThumbnail
     */

    /**
     * @property {boolean} hasAttachment
     */

    /**
     * @property {boolean} hasAnalyzed
     */

    /**
     * @property {UserAccountModel} verifiedBy
     */

    /**
     * @property {String|Date} tanggalVerified
     */

    /**
     * @property {UserAccountModel} approvedBy
     */

    /**
     * @property {String|Date} tanggalApproved
     */

    /**
     * @property {string|null} analisa
     */

    /**
     * @property {int|null} score
     */

    /**
     * @property {UserAccountModel} analyzeBy
     */

    /**
     * @property {String|Date} tanggalAnalyze
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model properties
     */
    constructor(schema = {
        bid: null,
        category: null,
        title: null,
        intro: null,
        content: null,
        filePath: null,
        fileName: null,
        fileExt: null,
        fileSize: null,
        fileUrl: null,
        fileThumbnail: null,
        hasAttachment: null,
        hasAnalyzed: null,
        analisa: null,
        score: null,
        status: null
    }) {
        super(schema, 'bid');
        Vue.set(this, 'status', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'verifiedBy', data.verifiedBy);
        Vue.set(this, 'tanggalVerified', data.tanggalVerified);
        Vue.set(this, 'approvedBy', data.approvedBy);
        Vue.set(this, 'tanggalApproved', data.tanggalApproved);
        Vue.set(this, 'analyzeBy', data.analyzeBy);
        Vue.set(this, 'tanggalAnalyze', data.tanggalAnalyze);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/humas/create',
            'fetch': '/humas/{id}',
            'delete': '/humas/delete/{id}',
            'update': '/humas/update/{id}',
            'stepflow': '/humas/stepflow/{id}',
        }
    }
}
