/**
 * Function UserProfileValidators
 *
 * @author  Ahmad Fajar
 * @since   2019-04-16, modified: 2019-04-17 03:06
 */
import { required, requiredUnless, email, minLength, maxLength, sameAs } from "vuelidate/lib/validators";

/**
 * Schema UserProfile's Validator
 *
 * @return {Object}
 */
export const userProfileValidator = {
    fullName: {
        required,
        minLength: minLength(6),
        maxLength: maxLength(250)
    },
    newEmail: {
        required,
        email,
        maxLength: maxLength(100)
    },
    userPassword: {
        required: requiredUnless(function () {
            return !this.isPasswordChange
        }),
        minLength: minLength(5),
        maxLength: maxLength(50)
    },
    newPassword: {
        required: requiredUnless(function () {
            return !this.isPasswordChange
        }),
        minLength: minLength(5),
        maxLength: maxLength(50)
    },
    confirmPassword: {
        required: requiredUnless(function () {
            return !this.isPasswordChange
        }),
        sameAs: sameAs('newPassword')
    },
};

/**
 * UserProfile's Validation Handler
 *
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function userProfileValidationHandler(field) {
    return {
        computed: {
            fullNameValidator() {
                return {
                    hasError: this.$v[field].fullName.$error,
                    messages: {
                        required: 'Field nama lengkap wajib diisi',
                        minLength: 'Field nama lengkap setidaknya harus memiliki 6 karakter',
                        maxLength: 'Field nama lengkap hanya bisa memiliki paling banyak 250 karakter'
                    },
                    validators: {
                        required: this.$v[field].fullName.required,
                        minLength: this.$v[field].fullName.minLength,
                        maxLength: this.$v[field].fullName.maxLength
                    }
                }
            },
            emailValidator() {
                return {
                    hasError: this.$v[field].newEmail.$error,
                    messages: {
                        required: 'Field alamat email wajib diisi',
                        email: 'Format alamat email tidak benar',
                        maxLength: 'Field alamat email hanya bisa memiliki paling banyak 100 karakter'
                    },
                    validators: {
                        required: this.$v[field].newEmail.required,
                        email: this.$v[field].newEmail.email,
                        maxLength: this.$v[field].newEmail.maxLength
                    }
                }
            },
            userPasswordValidator() {
                return {
                    hasError: this.$v[field].userPassword.$error,
                    messages: {
                        required: 'Field sandi lama wajib diisi',
                        minLength: 'Field sandi lama setidaknya harus memiliki 5 karakter',
                        maxLength: 'Field sandi lama hanya bisa memiliki paling banyak 50 karakter'
                    },
                    validators: {
                        required: this.$v[field].userPassword.required,
                        minLength: this.$v[field].userPassword.minLength,
                        maxLength: this.$v[field].userPassword.maxLength
                    }
                }
            },
            newPassworddValidator() {
                return {
                    hasError: this.$v[field].newPassword.$error,
                    messages: {
                        required: 'Field sandi baru wajib diisi',
                        minLength: 'Field sandi baru setidaknya harus memiliki 5 karakter',
                        maxLength: 'Field sandi baru hanya bisa memiliki paling banyak 50 karakter'
                    },
                    validators: {
                        required: this.$v[field].newPassword.required,
                        minLength: this.$v[field].newPassword.minLength,
                        maxLength: this.$v[field].newPassword.maxLength
                    }
                }
            },
            confirmPasswordValidator() {
                return {
                    hasError: this.$v[field].confirmPassword.$error,
                    messages: {
                        required: 'Field konfirmasi sandi baru wajib diisi',
                        sameAs: 'Field konfirmasi sandi baru harus sama dengan field sandi baru'
                    },
                    validators: {
                        required: this.$v[field].confirmPassword.required,
                        sameAs: this.$v[field].confirmPassword.sameAs
                    }
                }
            },
        }
    }
}
