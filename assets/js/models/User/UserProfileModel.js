import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class UserProfileModel.
 * Schema model UserProfile.
 *
 * @author Ahmad Fajar
 * @since  2019-04-16, modified: 2019-04-17 00:11
 */
export default class UserProfileModel extends BsModel {

    /**
     * @property {String} username Akun pengguna
     */

    /**
     * @property {String} fullName Nama lengkap pengguna
     */

    /**
     * @property {String} email Alamat email akun pengguna
     */

    /**
     * @property {String} newEmail Alamat email baru untuk request change email address
     */

    /**
     * @property {String} userPassword Password akun pengguna
     */

    /**
     * @property {String} newPassword Password baru akun pengguna
     */

    /**
     * @property {String} groupName Nama group akun pengguna
     */

    /**
     * @property {String} lastVisited DateTime terakhir membuka aplikasi
     */


    /**
     * Class constructor.
     *
     * @param {Object} [schema] The data model schema
     */
    constructor(schema = {
        fullName: null,
        newEmail: null,
        userPassword: null,
        newPassword: null
    }) {
        super(schema, 'uid');
        Vue.set(this, 'email', null);
        Vue.set(this, 'username', null);
        Vue.set(this, 'groupName', null);
        Vue.set(this, 'lastVisited', null);
        Vue.set(this, 'confirmPassword', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'fetch', 'update'.
     *
     * @return {Object} The REST URL configuration
     */
    get restUrl() {
        return {
            'fetch': '/user/profile',
            'update': '/user/profile/update'
        }
    }

}
