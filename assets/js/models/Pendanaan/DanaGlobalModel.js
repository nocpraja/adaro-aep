import Vue from "vue";
import { BsModel } from "vue-bs4";
import { VERIFIED } from "../../store/modules/mutation-types";

/**
 * Class DanaGlobalModel.
 *
 * @author Mark Melvin
 * @since  29/10/2018, modified: 20/05/2019 5:34
 */
export default class DanaGlobalModel extends BsModel {
    /**
     * @property {int|String} id
     * ID Dana Global.
     */

    /**
     * @property {String} title
     * Nama transaksi Dana Global.
     */

    /**
     * @property {String} kode
     * Kode transaksi.
     */

    /**
     * @property {String} status
     * Status transaksi.
     */

    /**
     * @property {int} tahunAwal
     * Periode anggaran Dana Global.
     */

    /**
     * @property {int} tahunAkhir
     * Periode anggaran Dana Global.
     */

    /**
     * @property {Number} anggaran
     * Nilai anggaran (dalam rupiah).
     */

    /**
     * @property {Number} newValue
     * Transient field nilai anggaran (dalam rupiah).
     */

    /**
     * @property {String} postedBy
     * Data creator yang membuat Dana Global.
     */

    /**
     * @property {String|Date} postedDate
     * Tanggal pembuatan Dana Global.
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        id: null,
        title: null,
        tahunAwal: null,
        tahunAkhir: null,
        newValue: null
    }) {
        super(schema, 'id');
        Vue.set(this, 'kode', null);
        Vue.set(this, 'anggaran', null);
        Vue.set(this, 'status', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'kode', data.kode);
        Vue.set(this, 'status', data.status);
        Vue.set(this, 'anggaran', data.anggaran);
        Vue.set(this, 'postedBy', data.postedBy);
        Vue.set(this, 'postedDate', data.postedDate);

        if (data.status !== VERIFIED) {
            Vue.set(this, 'newValue', Math.abs(data.anggaran));
        }
    }

    /**
     * Define REST URL configuration in the form <code>{key: url}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object} REST URL configuration
     */
    get restUrl() {
        return {
            'save': '/pendanaan/global/create',
            'info': '/pendanaan/global/lastinfo',
            'fetch': '/pendanaan/global/{id}',
            'update': '/pendanaan/global/update/{id}',
            'delete': '/pendanaan/global/delete/{id}',
            'stepflow': '/pendanaan/global/stepflow/{id}',
        }
    }

}

