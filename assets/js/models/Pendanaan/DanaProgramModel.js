import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class DanaProgramModel.
 *
 * @author Ahmad Fajar
 * @since  29/04/2019, modified: 20/05/2019 6:19
 */
export default class DanaProgramModel extends BsModel{
    /**
     * @property {int|String} id
     * ID Dana Program.
     */

    /**
     * @property {String} title
     * Nama transaksi Dana Program.
     */

    /**
     * @property {String} kode
     * Kode transaksi.
     */

    /**
     * @property {String} status
     * Status transaksi.
     */

    /**
     * @property {int} tahunAwal
     * Periode anggaran Dana Program.
     */

    /**
     * @property {int} tahunAkhir
     * Periode anggaran Dana Program.
     */

    /**
     * @property {Number} anggaran
     * Nilai anggaran (dalam rupiah).
     */

    /**
     * @property {Number} newValue
     * Transient field nilai anggaran (dalam rupiah).
     */

    /**
     * @property {Number} alokasi
     * Nilai alokasi dana anggaran (dalam rupiah).
     */

    /**
     * @property {Number} realisasi
     * Realisasi pemakaian anggaran (dalam rupiah).
     */

    /**
     * @property {Number} danaTersedia
     * Nilai anggaran yang tersedia setelah dikurangi alokasi dana (dalam rupiah).
     */

    /**
     * @property {Number} sisaDana
     * Sisa dana yang tersedia setelah dikurangi dengan realisasi dana dan
     * yang akan dikembalikan ke Dana Bidang (dalam rupiah).
     */

    /**
     * @property {String} postedBy
     * Data creator yang membuat Dana Program.
     */

    /**
     * @property {String|Date} postedDate
     * Tanggal pembuatan Dana Program.
     */

    /**
     * @property {Number|DanaBidangModel} danaBidang
     * Program CSR.
     */

    /**
     * @property {Number|ProgramModel} programCsr
     * Program CSR.
     */

    /**
     * @property {Number} danaBidangId
     * ID Dana Bidang.
     */

    /**
     * @property {Number} programCsrId
     * ID Program CSR.
     */

    /**
     * @property {Number} bidangCsrId
     * ID Bidang CSR.
     */


    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        id: null,
        title: null,
        tahunAwal: null,
        tahunAkhir: null,
        newValue: null,
        danaBidang: null,
        programCsr: null
    }) {
        super(schema, 'id');
        Vue.set(this, 'kode', null);
        Vue.set(this, 'anggaran', null);
        Vue.set(this, 'status', null);
        Vue.set(this, 'danaBidangId', null);
        Vue.set(this, 'bidangCsrId', null);
        Vue.set(this, 'programCsrId', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'kode', data.kode);
        Vue.set(this, 'status', data.status);
        Vue.set(this, 'anggaran', data.anggaran);
        Vue.set(this, 'alokasi', data.alokasi);
        Vue.set(this, 'realisasi', data.realisasi);
        Vue.set(this, 'danaTersedia', data.danaTersedia);
        Vue.set(this, 'sisaDana', data.sisaDana);
        Vue.set(this, 'postedBy', data.postedBy);
        Vue.set(this, 'postedDate', data.postedDate);
        Vue.set(this, 'programCsrId', data.programCsr ? data.programCsr.msid : null);

        if (data.danaBidang) {
            Vue.set(this, 'danaBidangId', data.danaBidang.id);
            Vue.set(this, 'bidangCsrId', data.danaBidang.bidangCsr ? data.danaBidang.bidangCsr.id : null);
        }
    }

    /**
     * Define REST URL configuration in the form <code>{key: url}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update', etc.
     *
     * @type {Object} REST URL configuration
     */
    get restUrl() {
        return {
            'save': '/pendanaan/program/create',
            'fetch': '/pendanaan/program/{id}',
            'update': '/pendanaan/program/update/{id}',
            'delete': '/pendanaan/program/delete/{id}',
            'revisi': '/pendanaan/program/revisi/{id}',
            'stepflow': '/pendanaan/program/stepflow/{id}',
        }
    }

}
