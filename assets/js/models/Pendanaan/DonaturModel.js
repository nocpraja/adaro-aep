import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class DonaturModel.
 *
 * @author Ahmad Fajar
 * @since  29/04/2019, modified: 30/04/2019 2:13
 */
export default class DonaturModel extends BsModel {
    /**
     * @property {int|String} ids
     * ID Donatur.
     */

    /**
     * @property {Number} jumlah
     * Jumlah dana yang telah diberikan (dalam rupiah)
     */

    /**
     * @property {Number|Object} company
     * Data reference berisi informasi perusahaan penyandang dana
     */

    /**
     * @property {int} companyId
     * ID data reference
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        jumlah: null,
        company: null
    }) {
        super(schema, 'ids');
        Vue.set(this, 'companyId', null);
    }

}
