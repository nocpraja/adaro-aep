/**
 * Validator untuk data model Pendanaan Dana Bidang
 *
 * @author Ahmad Fajar
 * @since  28/04/2019, modified: 28/04/2019 12:02
 */
import {required, integer} from "vuelidate/lib/validators";

/**
 * Dana Bidang validation's model.
 *
 * @type {Object}
 */
export const danaBidangValidator = {
    tahunAwal: {required, integer},
    tahunAkhir: {required, integer},
    newValue: {required, integer},
    bidangCsrId: {required},
    title: {required}
};

/**
 * Dana Bidang Validation's Handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function danaBidangValidationHandler(field) {
    return {
        computed: {
            bidangCsrValidator() {
                return {
                    hasError: this.$v[field].bidangCsrId.$error,
                    messages: {
                        required: 'Field Bidang CSR wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].bidangCsrId.required,
                    }
                }
            },
            bidangTitleValidator() {
                return {
                    hasError: this.$v[field].title.$error,
                    messages: {
                        required: 'Field Nama Dana Bidang wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].title.required,
                    }
                }
            },
        }
    }
}
