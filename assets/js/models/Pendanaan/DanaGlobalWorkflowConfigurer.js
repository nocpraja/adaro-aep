import {
    NEW,
    REJECTED,
    STATE_INVALID,
    STEP_REJECT,
    STEP_UPDATE,
    STEP_VERIFY,
    UPDATED,
    VERIFIED
} from "../../store/modules/mutation-types";
import BaseConfigurer from "../Workflow/BaseConfigurer";
import Transition from "../Workflow/Transition";

/**
 * DanaGlobalWorkflowConfigurer class.
 *
 * @author Ahmad Fajar
 * @since  03/05/2019 modified: 03/05/2019 19:36
 */
export default class DanaGlobalWorkflowConfigurer extends BaseConfigurer {
    /**
     * Class constructor.
     *
     * @param {string} name A workflow name
     */
    constructor(name) {
        super(name, 'status', NEW);
    }

    /**
     * Get major place from object's marking.
     *
     * @param {Object} subject The object to check
     * @return {String} A major place name
     */
    majorPlaceMark(subject) {
        try {
            const marking = this.workflow.getMarking(subject);

            if (marking.isEmpty()) {
                return '';
            }

            const place = marking.places[0];

            if (place === REJECTED) {
                return REJECTED;
            } else if (place === UPDATED) {
                return UPDATED;
            } else if (place === VERIFIED) {
                return VERIFIED;
            } else if (place === NEW) {
                return NEW;
            }
        } catch (e) {
            return STATE_INVALID;
        }

        return '';
    }

    /**
     * Create places configurations.
     *
     * @return {void}
     * @private
     */
    _createPlaces() {
        this._places = [
            NEW, UPDATED, VERIFIED, REJECTED
        ];
    }

    /**
     * Create transition configurations.
     *
     * @return {void}
     * @private
     */
    _createTransitions() {
        this._transitions = [
            new Transition([VERIFIED, REJECTED], UPDATED, STEP_UPDATE),
            new Transition([NEW, UPDATED], VERIFIED, STEP_VERIFY),
            new Transition([NEW, UPDATED], REJECTED, STEP_REJECT),
        ];
    }

    /**
     * Get color name for badge.
     * NEW, UPDATED, VERIFIED, REJECTED
     * @param {Object} subject The subject to check
     * @return {string} Color name
     */
    badgeColor(subject) {
        const status = this.majorPlaceMark(subject);

        switch (status) {
            case NEW :
                return 'info';
            case UPDATED :
                return 'primary';
            case VERIFIED :
                return 'success-color-dark';
            case REJECTED :
                return 'danger';
            default :
                return 'dark';
        }
    }
}
