import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class DanaBatchModel.
 *
 * @author Ahmad Fajar
 * @since  29/04/2019, modified: 14/05/2019 2:04
 */
export default class DanaBatchModel extends BsModel {
    /**
     * @property {int|String} id
     * ID Dana Batch.
     */

    /**
     * @property {String} namaProgram
     * Nama program Dana Batch.
     */

    /**
     * @property {String} kode
     * Kode transaksi.
     */

    /**
     * @property {String} status
     * Status transaksi.
     */

    /**
     * @property {int|string} tahun
     * Periode anggaran Dana Batch.
     */

    /**
     * @property {int} batchId
     * ID Batch Pembinaan
     */

    /**
     * @property {string} batchName
     * Nama Batch Pembinaan
     */

    /**
     * @property {int} danaProgramId
     * ID Dana Program
     */

    /**
     * @property {String} programCsrName
     * Nama Program CSR
     */

    /**
     * @property {String} templateName
     * Template untuk pembuatan nama Dana Batch
     */

    /**
     * @property {DonaturModel[]|Array} donaturs
     * Collection of Donatur penyandang dana.
     */

    /**
     * @property {Number} anggaran
     * Nilai anggaran (dalam rupiah).
     */

    /**
     * @property {Number} alokasi
     * Nilai alokasi dana anggaran (dalam rupiah).
     */

    /**
     * @property {Number} realisasi
     * Realisasi pemakaian anggaran (dalam rupiah).
     */

    /**
     * @property {Number} danaTersedia
     * Nilai anggaran yang tersedia setelah dikurangi alokasi dana (dalam rupiah).
     */

    /**
     * @property {Number} sisaDana
     * Sisa dana yang tersedia setelah dikurangi dengan realisasi dana dan
     * yang akan dikembalikan ke Dana Program (dalam rupiah).
     */

    /**
     * @property {Object[]} revisiAnggarans
     * Collection of data revisi anggaran Dana Batch.
     */

    /**
     * @property {Object} tags
     * Embedded tags within Dana Batch.
     */

    /**
     * @property {Object[]} requests
     * Transient collection of data pengelolaan revisi anggaran Dana Batch.
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        id: null,
        namaProgram: null,
        tahun: null,
        newValue: null,
        danaProgram: null,
        batch: null,
        donaturs: []
    }) {
        super(schema, 'id');
        Vue.set(this, 'kode', null);
        Vue.set(this, 'anggaran', null);
        Vue.set(this, 'status', null);
        Vue.set(this, 'batchId', null);
        Vue.set(this, 'batchName', null);
        Vue.set(this, 'danaProgramId', null);
        Vue.set(this, 'programCsrId', null);
        Vue.set(this, 'programCsrName', null);
        Vue.set(this, 'templateName', null);
        Vue.set(this, 'title', null);
        Vue.set(this, 'requests', []);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'kode', data.kode);
        Vue.set(this, 'status', data.status);
        Vue.set(this, 'title', data.namaProgram);
        Vue.set(this, 'anggaran', data.anggaran);
        Vue.set(this, 'alokasi', data.alokasi);
        Vue.set(this, 'realisasi', data.realisasi);
        Vue.set(this, 'danaTersedia', data.danaTersedia);
        Vue.set(this, 'sisaDana', data.sisaDana);
        Vue.set(this, 'postedBy', data.postedBy);
        Vue.set(this, 'postedDate', data.postedDate);
        Vue.set(this, 'newValue', Math.abs(data.anggaran));
        Vue.set(this, 'batchId', data.batch.batchId);
        Vue.set(this, 'batchName', data.batch.namaBatch);
        Vue.set(this, 'danaProgramId', data.danaProgram.id);
        Vue.set(this, 'programCsrId', data.danaProgram.programCsr.msid);
        Vue.set(this, 'programCsrName', data.danaProgram.programCsr.name);
        Vue.set(this, 'templateName', data.danaProgram.programCsr.templateName);
        Vue.set(this, 'revisiAnggarans', data.revisiAnggarans);
        Vue.set(this, 'tags', data.tags);

        if (data.donaturs && data.donaturs.length > 0) {
            data.donaturs.forEach(item => {
                Vue.set(item, 'companyId', item.company.id);
            });
        }
    }

    /**
     * Define REST URL configuration in the form <code>{key: url}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update', etc.
     *
     * @type {Object} REST URL configuration
     */
    get restUrl() {
        return {
            'save': '/pendanaan/batch/create',
            'fetch': '/pendanaan/batch/{id}',
            'update': '/pendanaan/batch/update/{id}',
            'delete': '/pendanaan/batch/delete/{id}',
            'revisi': '/pendanaan/batch/revisi/{id}',
            'stepflow': '/pendanaan/batch/stepflow/{id}',
        }
    }

}
