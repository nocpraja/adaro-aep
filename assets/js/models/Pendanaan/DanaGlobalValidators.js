/**
 * Validator untuk data model Pendanaan Dana Global
 *
 * @author Mark Melvin
 * @since  29/10/2018, modified: 30/04/2019 1:05
 */
import {required, integer} from "vuelidate/lib/validators";

/**
 * DanaGlobal validation's model.
 *
 * @type {Object}
 */
export const danaGlobalValidator = {
    tahunAwal: {required, integer},
    tahunAkhir: {required, integer},
    newValue: {required, integer},
};

/**
 * DanaGlobal Validation's Handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function danaGlobalValidationHandler(field) {
    return {
        computed: {
            anggaranValidator() {
                return {
                    hasError: this.$v[field].newValue.$error,
                    messages: {
                        required: 'Field nilai anggaran wajib diisi',
                        integer: this.invalidNumericErrorMsg
                    },
                    validators: {
                        required: this.$v[field].newValue.required,
                        integer: this.$v[field].newValue.integer
                    }
                }
            },
            tahunAwalValidator() {
                return {
                    hasError: this.$v[field].tahunAwal.$error,
                    messages: {
                        required: 'Field periode awal wajib diisi',
                        integer: this.invalidNumericErrorMsg
                    },
                    validators: {
                        required: this.$v[field].tahunAwal.required,
                        integer: this.$v[field].tahunAwal.integer
                    }
                }
            },
            tahunAkhirValidator() {
                return {
                    hasError: this.$v[field].tahunAkhir.$error,
                    messages: {
                        required: 'Field periode akhir wajib diisi',
                        integer: this.invalidNumericErrorMsg
                    },
                    validators: {
                        required: this.$v[field].tahunAkhir.required,
                        integer: this.$v[field].tahunAkhir.integer
                    }
                }
            }
        }
    }
}
