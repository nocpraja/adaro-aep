import RiwayatTransaksiModel from "./RiwayatTransaksiModel";

/**
 * Class RiwayatDanaGlobal.
 *
 * @author Ahmad Fajar
 * @since  24/04/2019, modified: 24/04/2019 11:02
 */
export default class RiwayatDanaGlobal extends RiwayatTransaksiModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object} REST URL configuration
     */
    get restUrl() {
        return {
            'fetch': '/pendanaan/global/riwayat/lastinfo'
        }
    }

}
