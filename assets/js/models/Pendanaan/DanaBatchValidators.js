/**
 * Validator untuk data model Pendanaan Dana Batch
 *
 * @author Ahmad Fajar
 * @since  30/04/2019, modified: 11/05/2019 4:32
 */
import {required, integer } from "vuelidate/lib/validators";

/**
 * Dana Batch validation's model.
 *
 * @type {Object}
 */
export const danaBatchValidator = {
    batchId: {required},
    danaProgramId: {required},
    namaProgram: {required},
    newValue: {required, integer},
    programCsrName: {required},
    tahun: {required, integer},
};

/**
 * Revisi Dana Batch validation's model.
 *
 * @type {Object}
 */
export const danaRevisiValidator = {
    refcode: {required},
    jumlah: {
        minValue: function (value) {
            return value > 0;
        },
        lessThan: function (value, ref) {
            return ref.saldo > value;
        }
    },
};

/**
 * Dana Batch Validation's Handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function danaBatchValidationHandler(field) {
    return {
        computed: {
            anggaranValidator() {
                return {
                    hasError: this.$v[field].newValue.$error,
                    messages: {
                        required: 'Field nilai anggaran wajib diisi',
                        integer: this.invalidNumericErrorMsg
                    },
                    validators: {
                        required: this.$v[field].newValue.required,
                        integer: this.$v[field].newValue.integer
                    }
                }
            },
            batchIdValidator() {
                return {
                    hasError: this.$v[field].batchId.$error,
                    messages: {
                        required: 'Field Periode Batch wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].batchId.required,
                    }
                }
            },
            danaProgramIdValidator() {
                return {
                    hasError: this.$v[field].danaProgramId.$error,
                    messages: {
                        required: 'Field Dana Program wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].danaProgramId.required,
                    }
                }
            },
            namaProgramValidator() {
                return {
                    hasError: this.$v[field].namaProgram.$error,
                    messages: {
                        required: 'Field Nama Dana Batch wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaProgram.required,
                    }
                }
            },
            programCsrNameValidator() {
                return {
                    hasError: this.$v[field].programCsrName.$error,
                    messages: {
                        required: 'Field Nama Program CSR wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].programCsrName.required,
                    }
                }
            },
            tahunValidator() {
                return {
                    hasError: this.$v[field].tahun.$error,
                    messages: {
                        required: 'Field tahun wajib diisi',
                        integer: this.invalidNumericErrorMsg
                    },
                    validators: {
                        required: this.$v[field].tahun.required,
                        integer: this.$v[field].tahun.integer
                    }
                }
            },
        }
    }
}

export const danaRevisiValidationHandler = {
    methods: {
        refcodeValidator(element) {
            return {
                hasError: element.refcode.$error,
                messages: {
                    required: 'Wajib diisi',
                },
                validators: {
                    required: element.refcode.required,
                }
            };
        },
        jumlahValidator(element) {
            return {
                hasError: element.jumlah.$error,
                messages: {
                    minValue: 'Wajib diisi',
                    lessThan: 'Harus lebih kecil dari dana tersedia',
                },
                validators: {
                    minValue: element.jumlah.minValue,
                    lessThan: element.jumlah.lessThan,
                }
            };
        },
    },
};
