/**
 * Validator untuk data model Pendanaan Dana Program
 *
 * @author Ahmad Fajar
 * @since  29/04/2019, modified: 29/04/2019 6:05
 */
import {required, integer} from "vuelidate/lib/validators";

/**
 * Dana Bidang validation's model.
 *
 * @type {Object}
 */
export const danaProgramValidator = {
    tahunAwal: {required, integer},
    tahunAkhir: {required, integer},
    newValue: {required, integer},
    danaBidangId: {required},
    programCsrId: {required},
    title: {required}
};

/**
 * Dana Program Validation's Handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function danaProgramValidationHandler(field) {
    return {
        computed: {
            programTitleValidator() {
                return {
                    hasError: this.$v[field].title.$error,
                    messages: {
                        required: 'Field Nama Dana Program wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].title.required,
                    }
                }
            },
            programCsrValidator() {
                return {
                    hasError: this.$v[field].programCsrId.$error,
                    messages: {
                        required: 'Field Program CSR wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].programCsrId.required,
                    }
                }
            },
            bidangValidator() {
                return {
                    hasError: this.$v[field].danaBidangId.$error,
                    messages: {
                        required: 'Field Dana Bidang wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].danaBidangId.required,
                    }
                }
            },
        }
    }
}
