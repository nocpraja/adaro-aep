import {
    APPROVED_1,
    APPROVED_2,
    APPROVED_3,
    CLOSED,
    NEW,
    ONGOING,
    PLANNED,
    REJECTED,
    REVISED,
    REVISI,
    REVISI_REJECTED,
    STATE_INVALID,
    STEP_AKTIVASI,
    STEP_CLOSING,
    STEP_REJECT,
    STEP_REJECT_REQUEST,
    STEP_REQUEST,
    STEP_UPDATE,
    STEP_VERIFY,
    STEP_VERIFY_REQUEST,
    UPDATED,
    VERIFIED
} from "../../store/modules/mutation-types";
import Transition from "../Workflow/Transition";
import BaseConfigurer from "../Workflow/BaseConfigurer";

/**
 * ClosableWorkflowConfigurer class.
 *
 * @author Ahmad Fajar
 * @since  29/04/2019 modified: 04/05/2019 8:51
 */
export default class ClosableWorkflowConfigurer extends BaseConfigurer {

    /**
     * Class constructor.
     *
     * @param {string} name A workflow name
     */
    constructor(name) {
        super(name, 'status', NEW);
    }

    /**
     * Get approval marking place from object's marking.
     *
     * @param {Object} subject The object to check
     * @return {string} Approval place
     */
    approvalPlaceMark(subject) {
        try {
            const marking = this.workflow.getMarking(subject);

            if (marking.isEmpty()) {
                return '';
            }
            const place = marking.places[0];

            if ([APPROVED_1, APPROVED_2, APPROVED_3].includes(place)) {
                return place.replace('_', '-');
            } else if ([PLANNED, REVISED].includes(place)) {
                return VERIFIED;
            } else if ([REVISI_REJECTED].includes(place)) {
                return REJECTED;
            }
        } catch (e) {
            return '';
        }

        return '';
    }

    /**
     * Get major place from object's marking.
     *
     * @param {Object} subject The object to check
     * @return {String} A major place name
     */
    majorPlaceMark(subject) {
        try {
            const marking = this.workflow.getMarking(subject);

            if (marking.isEmpty()) {
                return '';
            }

            const place = marking.places[0];

            if (place === REJECTED) {
                return REJECTED;
            } else if (this.workflow.canXor(subject, [STEP_UPDATE, STEP_VERIFY])) {
                return NEW;
            } else if (this.workflow.can(subject, STEP_AKTIVASI)) {
                return PLANNED;
            } else if (this.workflow.canXor(subject, [STEP_VERIFY_REQUEST, STEP_REJECT_REQUEST, STEP_CLOSING])) {
                return ONGOING;
            } else if (place === CLOSED) {
                return CLOSED;
            }
        } catch (e) {
            return STATE_INVALID;
        }

        return '';
    }

    /**
     * Get color name for badge.
     *
     * @param {Object} subject The subject to check
     * @return {string} Color name
     */
    badgeColor(subject) {
        const status = this.majorPlaceMark(subject);

        switch (status) {
            case NEW :
                return 'info';
            case PLANNED :
                return 'primary';
            case ONGOING :
                return 'success-color-dark';
            case STATE_INVALID :
            case REJECTED :
                return 'danger';
            default :
                return 'dark';
        }
    }

    /**
     * Create places configurations.
     *
     * @return {void}
     * @private
     */
    _createPlaces() {
        this._places = [
            NEW, UPDATED, PLANNED, /* APPROVED_1, APPROVED_2, APPROVED_3, */ REJECTED,
            ONGOING, REVISI, REVISED, REVISI_REJECTED, CLOSED
        ];
    }

    /**
     * Create transition configurations.
     *
     * @return {void}
     * @private
     */
    _createTransitions() {
        this._transitions = [
            new Transition([NEW, UPDATED, REJECTED], UPDATED, STEP_UPDATE),
            new Transition([NEW, UPDATED], PLANNED, STEP_VERIFY),
            // new Transition([PLANNED, REVISED], APPROVED_1, STEP_APPROVAL_1),
            // new Transition([APPROVED_1], APPROVED_2, STEP_APPROVAL_2),
            // new Transition([APPROVED_2], APPROVED_3, STEP_APPROVAL_3),
            new Transition([NEW, UPDATED/* , PLANNED, REVISED, APPROVED_1, APPROVED_2 */], REJECTED, STEP_REJECT),
            new Transition([PLANNED], ONGOING, STEP_AKTIVASI),
            new Transition([ONGOING, REVISED, REVISI_REJECTED], REVISI, STEP_REQUEST),
            new Transition([REVISI], REVISED, STEP_VERIFY_REQUEST),
            new Transition([REVISI], REVISI_REJECTED, STEP_REJECT_REQUEST),
            new Transition([ONGOING, REVISED, REVISI_REJECTED], CLOSED, STEP_CLOSING)
        ];
    }

}

