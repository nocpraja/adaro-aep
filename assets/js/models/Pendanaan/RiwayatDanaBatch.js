import RiwayatTransaksiModel from "./RiwayatTransaksiModel";

/**
 * Class RiwayatDanaBatch.
 *
 * @author Mark Melvin
 * @since  25/06/2019, modified: 08/07/2019 23:15
 */
export default class RiwayatDanaBatch extends RiwayatTransaksiModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object} REST URL configuration
     */
    get restUrl() {
        return {
            'fetch': '/pendanaan/batch/riwayat/lastinfo/'
        }
    }

}
