/**
 * Validator untuk data model SumberDana
 *
 * @author  Ahmad Fajar
 * @since   27/09/2018, modified: 27/09/2018 22:43
 */
import {required} from "vuelidate/lib/validators";

/**
 * SumberDana validation model.
 *
 * @return {Object}
 */
export const sumberDanaValidator = {
    refId: {required},
    jumlah: {required}
};

/**
 * SumberDana Validation's Handler
 *
 * @return {Object}
 */
export function sumberDanaValidationHandler(field) {
    return {
        computed: {
            donaturValidator() {
                return {
                    hasError: this.$v[field].refId.$error,
                    messages: {required: this.requiredErrorMsg},
                    validators: {required: this.$v[field].refId.required}
                }
            },
            jumlahValidators() {
                return {
                    hasError: this.$v[field].jumlah.$error,
                    messages: {required: this.requiredErrorMsg},
                    validators: {required: this.$v[field].jumlah.required}
                }
            }
        }
    }
}
