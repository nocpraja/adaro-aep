import { BsModel } from "vue-bs4";

/**
 * Class RiwayatTransaksiModel.
 *
 * @author Ahmad Fajar
 * @since  24/04/2019, modified: 24/04/2019 11:02
 */
export default class RiwayatTransaksiModel extends BsModel {
    /**
     * @property {int} tid
     * ID riwayat transaksi pendanaan.
     */

    /**
     * @property {String} refcode
     * Kode referensi transaksi pendanaan.
     */

    /**
     * @property {String} title
     * Nama transaksi pendanaan.
     */

    /**
     * @property {int|String} tahunAwal
     * Periode anggaran Dana Global.
     */

    /**
     * @property {int|String} tahunAkhir
     * Periode anggaran Dana Global.
     */

    /**
     * @property {Number} jumlah
     * Nilai transaksi (dalam rupiah).
     */

    /**
     * @property {Number} saldo
     * Dana anggaran yang masih tersedia (dalam rupiah).
     */

    /**
     * @property {UserAccountModel} postedBy
     * User account yang melakukan transaksi pendanaan.
     */

    /**
     * @property {String|Date} postedDate
     * Tanggal pencatatan transaksi pendanaan.
     */

    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        tid: null,
        refcode: null,
        title: null,
        tahunAwal: null,
        tahunAkhir: null,
        jumlah: null,
        saldo: null,
        postedBy: null,
        postedDate: null
    }) {
        super(schema, 'tid');
    }

}
