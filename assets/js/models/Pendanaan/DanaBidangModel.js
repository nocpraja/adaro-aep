import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class DanaBidangModel.
 *
 * @author Ahmad Fajar
 * @since  27/04/2019, modified: 03/05/2019 22:55
 */
export default class DanaBidangModel extends BsModel {
    /**
     * @property {int|String} id
     * ID Dana Bidang.
     */

    /**
     * @property {String} title
     * Nama transaksi Dana Bidang.
     */

    /**
     * @property {String} kode
     * Kode transaksi.
     */

    /**
     * @property {String} status
     * Status transaksi.
     */

    /**
     * @property {int} tahunAwal
     * Periode anggaran Dana Bidang.
     */

    /**
     * @property {int} tahunAkhir
     * Periode anggaran Dana Bidang.
     */

    /**
     * @property {Number} anggaran
     * Nilai anggaran (dalam rupiah).
     */

    /**
     * @property {Number} newValue
     * Transient field nilai anggaran (dalam rupiah).
     */

    /**
     * @property {Number} alokasi
     * Nilai alokasi dana anggaran (dalam rupiah).
     */

    /**
     * @property {Number} realisasi
     * Realisasi pemakaian anggaran (dalam rupiah).
     */

    /**
     * @property {Number} danaTersedia
     * Nilai anggaran yang tersedia setelah dikurangi alokasi dana (dalam rupiah).
     */

    /**
     * @property {Number} sisaDana
     * Sisa dana yang tersedia setelah dikurangi dengan realisasi dana dan
     * yang akan dikembalikan ke Dana Global (dalam rupiah).
     */

    /**
     * @property {String} postedBy
     * Data creator yang membuat Dana Bidang.
     */

    /**
     * @property {String|Date} postedDate
     * Tanggal pembuatan Dana Bidang.
     */

    /**
     * @property {Number|JenisProgramModel} bidangCsr
     * Bidang CSR.
     */

    /**
     * @property {Number} bidangCsrId
     * ID Bidang CSR.
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        id: null,
        title: null,
        tahunAwal: null,
        tahunAkhir: null,
        newValue: null,
        bidangCsr: null
    }) {
        super(schema, 'id');
        Vue.set(this, 'kode', null);
        Vue.set(this, 'anggaran', null);
        Vue.set(this, 'status', null);
        Vue.set(this, 'newValue', 0);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'kode', data.kode);
        Vue.set(this, 'status', data.status);
        Vue.set(this, 'anggaran', data.anggaran);
        Vue.set(this, 'alokasi', data.alokasi);
        Vue.set(this, 'realisasi', data.realisasi);
        Vue.set(this, 'danaTersedia', data.danaTersedia);
        Vue.set(this, 'sisaDana', data.sisaDana);
        Vue.set(this, 'postedBy', data.postedBy);
        Vue.set(this, 'postedDate', data.postedDate);
        Vue.set(this, 'bidangCsrId', data.bidangCsr.id);
    }

    /**
     * Define REST URL configuration in the form <code>{key: url}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update', etc.
     *
     * @type {Object} REST URL configuration
     */
    get restUrl() {
        return {
            'save': '/pendanaan/bidang/create',
            'fetch': '/pendanaan/bidang/{id}',
            'update': '/pendanaan/bidang/update/{id}',
            'delete': '/pendanaan/bidang/delete/{id}',
            'revisi': '/pendanaan/bidang/revisi/{id}',
            'stepflow': '/pendanaan/bidang/stepflow/{id}',
        }
    }

}
