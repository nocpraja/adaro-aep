import Vue from "vue";
import {BsModel} from "vue-bs4";


/**
 * Class SumberDanaExtModel.
 *
 * @author Suhendra
 * @since  03/08/2018, modified: 28/09/2018 11:58
 */
export class SumberDanaExtModel extends BsModel {
    /**
     * Class constructor.
     *
     * @param {Object} [schema]
     */
    constructor(schema = {
        ids: null,
        name: null,
        /**
         * @type {number} Program EntityID
         */
        sumberId: null,
        jumlah: null
    }) {
        super(schema, 'ids');
        Vue.set(this, 'tersedia', null);
        Vue.set(this, 'protected', null);
    }
}