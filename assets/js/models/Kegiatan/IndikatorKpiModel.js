import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class IndikatorKpiModel
 *
 * @author Alex Gaspersz
 * @since  21/03/2020, modified: 21/03/2020 20:28
 */
export class IndikatorKpiModel extends BsModel {
    /**
     * @property {int} idk ID Indikator Kpi
     */

    /**
     * @property {ProgramModel|int} programCsr
     */

    /**
     * @property {BatchModel|int} periodeBatch
     */

    /**
     * @property {BeasiswaModel|int} abfl
     */

    /**
     * @property {DataReferenceModel|int} dataRef
     */

    /**
     * @property {TechBaseModel|int} tbe
     */

    /**
     * @property {PaudModel|int} institusi
     */

    /**
     * @property {PaudGuruModel|PaudSiswaModel|int} individu
     */

    /**
     * @property {array} abflParams
     */

    /**
     * @property {array} tbeParams
     */

    /**
     * @property {array} paudParams
     */

    /**
     * @property {array} vokasiPelatihanParams
     */

    /**
     * @property {array} vokasiPendidikanParams
     */

    /**
     * @property {int} tahun
     */

    /**
     * @property {int} semester
     */

    /**
     * @property {int} programId
     * ID account RAB.
     */

    /**
     * @property {int} batchId
     * ID item RAB parent.
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model properties
     */
    constructor(schema = {
        idk: null,
        programCsr: null,
        periodeBatch: null,
        abfl: null,
        tbe: null,
        institusi: null,
        individu: null,
        tahun: null,
        semester: null,
        dataRef: null,
        abflParams: {},
        tbeParams: {},
        paudParams: {},
        // vokasiPelatihanParams: {},
        // vokasiPendidikanParams: {}
        // abflParams: {
        //     ipk: null,
        //     ipkRate:null,
        //     magang: null,
        //     magangRate: null,
        //     internalKampus: null,
        //     externalKampus: null,
        //     kampusRate: null,
        //     internalCsr: null,
        //     externalCsr: null,
        //     csrRate: null,
        //     internalKom: null,
        //     externalKom: null,
        //     komRate: null,
        //     nilaiTotal: null,
        //     level: null
        // },
        // tbeParams: {
        //     subsJumlah: null,
        //     subsAspekMax:null,
        //     subsAspekMin: null,
        //     subsSkorMax: null,
        //     subsSkorMin: null,
        //     augmentJumlah: null,
        //     augmentAspekMax:null,
        //     augmentAspekMin: null,
        //     augmentSkorMax: null,
        //     augmentSkorMin: null,
        //     modifJumlah: null,
        //     modifAspekMax:null,
        //     modifAspekMin: null,
        //     modifSkorMax: null,
        //     modifSkorMin: null,
        //     redefJumlah: null,
        //     redefAspekMax:null,
        //     redefAspekMin: null,
        //     redefSkorMax: null,
        //     redefSkorMin: null
        // },
        // paudParams: {
        //     rutinitasJumlah: null,
        //     rutinitasMax: null,
        //     rutinitasMin: null,
        //     rutinitasSkorMax: null,
        //     rutinitasSkorMin: null,
        //     barisJumlah: null,
        //     barisMax: null,
        //     barisMin: null,
        //     barisSkorMax: null,
        //     barisSkorMin: null,
        //     circleJumlah: null,
        //     circleMax: null,
        //     circleMin: null,
        //     circleSkorMax: null,
        //     circleSkorMin: null,
        //     jurnalJumlah: null,
        //     jurnalMax: null,
        //     jurnalMin: null,
        //     jurnalSkorMax: null,
        //     jurnalSkorMin: null,
        //     displayJumlah: null,
        //     displayMax: null,
        //     displayMin: null,
        //     displaySkorMax: null,
        //     displaySkorMin: null,
        //     karakterJumlah: null,
        //     karakterMax: null,
        //     karakterMin: null,
        //     karakterSkorMax: null,
        //     karakterSkorMin: null,
        //     komunikasiJumlah: null,
        //     komunikasiMax: null,
        //     komunikasiMin: null,
        //     komunikasiSkorMax: null,
        //     komunikasiSkorMin: null,
        //     sentraJumlah: null,
        //     sentraMax: null,
        //     sentraMin: null,
        //     sentraSkorMax: null,
        //     sentraSkorMin: null,
        //     literasiJumlah: null,
        //     literasiMax: null,
        //     literasiMin: null,
        //     literasiSkorMax: null,
        //     literasiSkorMin: null,
        //     evaluasiJumlah: null,
        //     evaluasiMax: null,
        //     evaluasiMin: null,
        //     evaluasiSkorMax: null,
        //     evaluasiSkorMin: null,
        //     admJumlah: null,
        //     admMax: null,
        //     admMin: null,
        //     admSkorMax: null,
        //     admSkorMin: null,
        //     mnjJumlah: null,
        //     mnjMax: null,
        //     mnjMin: null,
        //     mnjSkorMax: null,
        //     mnjSkorMin: null,
        //     rekanJumlah: null,
        //     rekanMax: null,
        //     rekanMin: null,
        //     rekanSkorMax: null,
        //     rekanSkorMin: null,
        //     ortuJumlah: null,
        //     ortuMax: null,
        //     ortuMin: null,
        //     ortuSkorMax: null,
        //     ortuSkorMin: null
        // },
        vokasiPelatihanParams: {
            profit: null,
            wirausaha: {
                modul: null,
                demplot: null,
                kalender: null,
                praktik: null
            },
            bpup: {
                bentuk: null,
                skp: null,
                program: null,
                akte: null,
            },
        },
        vokasiPendidikanParams: {
            tefa: {
                target: null,
                capaian: null,
            },
            unit: {
                target: null,
                capaian: null,
            },
            profit: {
                target: null,
                capaian: null,
            }
        }
    }) {
        super(schema, 'idk');
        Vue.set(this, 'programId', null);
        Vue.set(this, 'batchId', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object} Rest URL configuration
     */
    get restUrl() {
        return {
            'fetch': '/kegiatan/indikator-kpi/{id}',
            'save': '/kegiatan/indikator-kpi/create',
            'delete': '/kegiatan/indikator-kpi/delete/{id}',
            'update': '/kegiatan/indikator-kpi/update/{id}'
        }
    }
}
