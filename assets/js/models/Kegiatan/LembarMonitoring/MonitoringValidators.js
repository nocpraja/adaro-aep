/**
 * Validator untuk data model Lembar Monitoring
 *
 * @author Mark Melvin
 * @since  17/11/2018, modified: 18/11/2018 23:37
 */
import {numeric, required} from "vuelidate/lib/validators";

/**
 * Monitoring validation model.
 *
 * @return {Object}
 */
export const monitoringValidator = {
    evalScore: {required, numeric},
    tanggalInput: {required},
};

/**
 * Monitoring Validation's Handler
 *
 * @return {Object}
 */
export function monitoringValidationHandler(field) {
    return {
        computed: {
            evalScoreValidator() {
                return {
                    hasError: this.$v[field].evalScore.$error,
                    messages: {required: this.requiredErrorMsg},
                    validators: {required: this.$v[field].evalScore.required}
                }
            },
            tanggalValidator() {
                return {
                    hasError: this.$v[field].tanggalInput.$error,
                    messages: {required: this.requiredErrorMsg},
                    validators: {required: this.$v[field].tanggalInput.required}
                }
            },
        }
    }
}