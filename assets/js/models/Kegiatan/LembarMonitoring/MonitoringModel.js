import {BsModel} from "vue-bs4";

/**
 * Class MonitoringkModel.
 *
 * @author Mark Melvin
 * @since 16/11/2018
 */
export class MonitoringModel extends BsModel {
    /**
     * Class constructor.
     *
     * @param {Object} [schema]
     */
    constructor(schema = {
        id: null,
        type: null,
        beneficiaryId: null,
        tanggalInput: null,
        evalScore: null,
        namaFile: null,
    }) {
        super(schema, 'id');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/lembar-monitoring/create',
            'fetch': '/kegiatan/lembar-monitoring/{id}',
            'delete': '/kegiatan/lembar-monitoring/delete/{id}',
        }
    }
}
