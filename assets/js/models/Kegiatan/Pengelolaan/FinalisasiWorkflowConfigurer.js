/**
 * FinalisasiWorkflowConfigurer class.
 *
 * @author Mark Melvin
 * @since  27/06/2020 modified: 27/06/2020
 */
export default class FinalisasiWorkflowConfigurer {
	/*
	Because in my opinion it it pointless to have workflow in frontend,
	here's the minimal useful part.
	 */

	majorPlaceMark(subject) {
		if (subject.status) {
			return subject.status;
		}
		return '';
	}

	badgeColor(subject) {
		const status = this.majorPlaceMark(subject);

		switch (status) {
			case 'NEW' :
				return 'primary';
			case 'PRESUBMITTED' :
				return 'warning';
			case 'SUBMITTED' :
				return 'info';
			case 'VERIFIED1' :
				return 'warning';
			case 'VERIFIED2' :
				return 'success';
			case 'APPROVED1' :
				return 'primary';
			case 'APPROVED2':
				return 'green';
			case 'APPROVED3':
				return 'success';
			default :
				return 'black';
		}
	}
}
