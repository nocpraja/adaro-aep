/**
 * Validators untuk data model Item Realisasi RAB
 *
 * @author Alex
 * @since  19/11/2018, modified: 11/02/2019 2:09
 */

import { required } from "vuelidate/lib/validators";

/**
 * Item RAB Validator Model
 *
 * @type {Object}
 */
export const itemRealisasiValidator = {
    realisasi: {required},
    realisasiDate: {required}
};

/**
 * Item realisasi RAB validation's handler.
 *
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function itemRealisasiValidationHandler(field) {
    return {
        computed: {
            realisasiValidator() {
                return {
                    hasError: this.$v[field].realisasi.$error,
                    messages: {
                        required: 'Field nilai realisasi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].realisasi.required
                    }
                };
            },
            realisasiDateValidator() {
                return {
                    hasError: this.$v[field].realisasiDate.$error,
                    messages: {
                        required: 'Field tanggal realisasi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].realisasiDate.required
                    }
                };
            }
        }
    };
}
