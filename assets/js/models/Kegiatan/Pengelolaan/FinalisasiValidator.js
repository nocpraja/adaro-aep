import { required } from "vuelidate/lib/validators";

/**
 * Finalisasi Validator Model
 *
 * @type {Object}
 */
export const FinalisasiValidator = {
    judul: {required},
    kegiatan: {required},
};

/**
 * Finalisasi validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function finalisasiValidationHandler(field) {
    return {
        computed: {
            judulValidator() {
                return {
                    hasError: this.$v[field].judul.$error,
                    messages: {
                        required: 'Field Judul Laporan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].judul.required
                    }
                };
            },
            kegiatanValidator() {
                return {
                    hasError: this.$v[field].kegiatan.$error,
                    messages: {
                        required: 'Field kegiatan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kegiatan.required
                    }
                };
            },
        }
    };
}
