import Vue from "vue";
import {BsModel} from "vue-bs4";

/**
 * Class SubKegiatanModel.
 *
 * @author Ahmad Fajar
 * @since  16/11/2018, modified: 16/05/2019 2:00
 */
export class SubKegiatanModel extends BsModel {
    /**
     * @property {int} sid
     * ID subkegiatan.
     */

    /**
     * @property {string} title
     * Nama subkegiatan.
     */

    /**
     * @property {string} tanggalMulai
     * Tanggal mulai jadwal pelaksanaan.
     */

    /**
     * @property {string} tanggalAkhir
     * Tanggal berakhir jadawal pelaksanaan.
     */

    /**
     * @property {number} totalSubRealisasi
     */

    /**
     * @property {boolean} isRealisasi
     */

    /**
     * @property {number} totalDokumentasi
     */

    /**
     * @property {number} totalFoto
     */

    /**
     * @property {number} totalDokumen
     */

    /**
     * @property {number} verifiedDokumen
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        sid: null,
        title: null,
        tanggalMulai: null,
        tanggalAkhir: null,
        kegiatan: null,
        provincies: null,
        kabupatens: null
    }) {
        super(schema, 'sid');
        Vue.set(this, 'tahun', null);
        Vue.set(this, 'programId', null);
        Vue.set(this, 'kegiatanId', null);
        Vue.set(this, 'namaProgram', null);
        Vue.set(this, 'namaKegiatan', null);
        Vue.set(this, 'provinsi', []);
        Vue.set(this, 'kabupaten', []);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        if (data.totalKegiatanDokumentasi) {
            Vue.set(this, 'totalDokumentasi', data.totalDokumentasi);
            Vue.set(this, 'latestDocument', data.latestDocument);
            Vue.set(this, 'latestGallery', data.latestGallery);
            Vue.set(this, 'totalFoto', data.totalFoto);
            Vue.set(this, 'totalDokumen', data.totalDokumen);
            Vue.set(this, 'verifiedDokumen', data.verifiedDokumen);
        }
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/pengelolaan/subkegiatan/create',
            'fetch': '/kegiatan/pengelolaan/subkegiatan/{id}',
            'delete': '/kegiatan/pengelolaan/subkegiatan/delete/{id}',
            'update': '/kegiatan/pengelolaan/subkegiatan/update/{id}'
        }
    }
}
