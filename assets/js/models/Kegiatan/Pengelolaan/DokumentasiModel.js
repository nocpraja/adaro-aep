import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class DokumentasiModel.
 *
 * @author Alex Gaspersz
 * @since  23/01/2020, modified: 18/02/2020 02:09
 */
export class DokumentasiModel extends BsModel {
    /**
     * @property {int} docId
     * ID item dokumen.
     */

    /**
     * @property {SubKegiatanModel|int} parentDoc
     * Item RAB sub kegiatan.
     */

    /**
     * @property {string} docCategory
     * Nama kategori dokumen.
     */

    /**
     * @property {string} docTitle
     * Judul dokumen.
     */

    /**
     * @property {string} baseFolder
     * Folder penyimpanan.
     */

    /**
     * @property {string} basePath
     * Path full dokumen.
     */

    /**
     * @property {string} mime
     * Tipe mime file.
     */

    /**
     * @property {string} extension
     * Tipe ekstensi file.
     */

    /**
     * @property {int} docSize
     * Ukuran file.
     */

    /**
     * @property {UserAccountModel} verifiedBy
     * Data model User yang melakukan verifikasi pengelolaan kegiatan.
     */

    /**
     * @property {String|Date} tanggalVerified
     * Tanggal anggaran Kegiatan diverifikasi oleh Adaro.
     */

    /**
     * @property {int} parentId
     * ID item sub kegiatan.
     */

    /**
     * @property {string} status
     * Status dokumen.
     */

    /**
     * @property {string} url
     * full path file.
     */

    /**
     * @property {string} thumbnail
     * full path thumbnail image.
     */

    /**
     * @property {boolean} isverified
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model properties
     */
    constructor(schema = {
        docId: null,
        kegiatanDoc: null,
        parentDoc: null,
        subDoc: null,
        docCategory: null,
        docTitle: null,
        baseFolder: null,
        basePath: null,
        mime: null,
        extension: null,
        docSize: null,
        url: null,
        thumbnail: null,
        status: null
    }) {
        super(schema, 'docId');
        Vue.set(this, 'kegiatanId', null);
        Vue.set(this, 'parentId', null);
        Vue.set(this, 'status', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'verifiedBy', data.verifiedBy);
        Vue.set(this, 'tanggalVerified', data.tanggalVerified);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update', 'verify'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/dokumentasi/create',
            'fetch': '/kegiatan/dokumentasi/{id}',
            'delete': '/kegiatan/dokumentasi/delete/{id}',
            'update': '/kegiatan/dokumentasi/update/{id}'
        }
    }
}
