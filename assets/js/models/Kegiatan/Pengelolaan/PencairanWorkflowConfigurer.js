/**
 * PencairanWorkflowConfigurer class.
 *
 * @author Mark Melvin
 * @since  27/06/2020 modified: 27/06/2020
 */
export default class PencairanWorkflowConfigurer {
	/*
	Because in my opinion it it pointless to have workflow in frontend,
	here's the minimal useful part.
	 */

	majorPlaceMark(subject) {
		if (subject.status) {
			return subject.status;
		}
		return '';
	}

	badgeColor(subject) {
		const status = this.majorPlaceMark(subject);

		switch (status) {
			case 'NEW' :
			case 'PRINTED' :
				return 'info';
			case 'SENT' :
				return 'success-color-dark';
			case 'RECEIVED' :
				return 'primary';
			case 'REJECTED' :
				return 'red';
			case 'VERIFIED':
			case 'APPROVED':
				return 'green';
			case 'PAID':
				return 'success';
			case 'REVISED':
				return 'green';
			default :
				return 'white';
		}
	}
}
