import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class data model ItemRealisasiModel.
 *
 * @author seegithub
 * @since  26/09/2019, modified: 07/10/2019 04:22
 */
export class ItemRealisasiModel extends BsModel {
    /**
     * @property {int} id
     * ID item RAB.
     */

    /**
     * @property {ItemRabModel|int} parentRab
     * Item RAB parentRab.
     */

    /**
     * @property {number} realisasi
     * Nilai realisasi anggaran.
     */

    /**
     * @property {string} status
     * Tahapan approval yang telah dilalui.
     */

    /**
     * @property {boolean} isSubmitted
     * Flag menyatakan realisasi sudah disubmit atau belum.
     */

    /**
     * @property {boolean} isVerified
     * Flag menyatakan realisasi sudah diverifikasi atau belum.
     */

    /**
     * @property {boolean} isRejected
     * Flag menyatakan realisasi direject.
     */

    /**
     * @property {UserAccountModel} postedBy
     * User account yang melakukan input data realisasi.
     */

    /**
     * @property {String|Date} realisasiDate
     * Tanggal realisasi.
     */

    /**
     * @property {int} kegiatanId
     * ID item kegiatan.
     */

    /**
     * @property {int} subId
     * ID item sub kegiatan.
     */

    /**
     * @property {int} parentId
     * ID item RAB parent.
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        id: null,
        kegiatanRab: null,
        subRab: null,
        parentRab: null,
        realisasi: null,
        postedBy: null,
        realisasiDate: null
    }) {
        super(schema, 'id');
        Vue.set(this, 'kegiatanId', null);
        Vue.set(this, 'subId', null);
        Vue.set(this, 'parentId', null);
        Vue.set(this, 'status', null);
        Vue.set(this, 'isSubmitted', null);
        Vue.set(this, 'isVerified', null);
        Vue.set(this, 'isRejected', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/pengelolaan/item-rab/realisasi/create'
        }
    }
}
