import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class KegiatanModel.
 *
 * @author Mark Melvin
 * @since  27/10/2018, modified: 19/05/2019 15:56
 */
export class KegiatanModel extends BsModel {
    /**
     * @property {int} kegiatanId
     * ID kegiatan.
     */

    /**
     * @property {string} namaKegiatan
     * Nama kegiatan.
     */

    /**
     * @property {string} kode
     * Kode transaksi.
     */

    /**
     * @property {string} status
     * Tahapan approval yang telah dilalui.
     */

    /**
     * @property {int|null} tahun
     * Tahun anggaran kegiatan.
     */

    /**
     * @property {Number} anggaran
     * Nilai anggaran kegiatan.
     */

    /**
     * @property {Number} realisasi
     * Nilai realisasi anggaran kegiatan.
     */

    /**
     * @property {int} submitted
     * Flag menyatakan kegiatan sudah disubmit ke Adaro atau belum.
     */

    /**
     * @property {{awal: string, akhir: string}} jadwalKegiatan
     * Jadwal pelaksanaan kegiatan.
     */

    /**
     * @property {SubKegiatanModel[]} children
     * Data model Subkegiatan.
     */

    /**
     * @property {KendaliKegiatanModel|null} kendaliKegiatan
     * Data model KendaliKegiatan.
     */

    /**
     * @property {UserAccountModel} submittedBy
     * Data model User yang melakukan submit pengelolaan kegiatan.
     */

    /**
     * @property {UserAccountModel} verifiedBy
     * Data model User yang melakukan verifikasi pengelolaan kegiatan.
     */

    /**
     * @property {UserAccountModel} postedRealisasiBy
     * Data model User yang melakukan update realisasi anggaran kegiatan.
     */

    /**
     * @property {UserAccountModel} closedBy
     * Data model User yang melakukan penutupan/closing anggaran kegiatan.
     */

    /**
     * @property {String|Date} tanggalSubmitted
     * Tanggal anggaran Kegiatan disubmit oleh Mitra.
     */

    /**
     * @property {String|Date} tanggalVerified
     * Tanggal anggaran Kegiatan diverifikasi oleh Adaro.
     */

    /**
     * @property {String|Date} tanggalPostedRealisasi
     * Tanggal realisasi anggaran Kegiatan disubmit oleh Mitra.
     */

    /**
     * @property {String|Date} tanggalClosed
     * Tanggal anggaran Kegiatan ditutup.
     */

    /**
     * @property {number} totalKegiatanRealisasi
     */

    /**
     * @property {number} totalKegiatanDokumentasi
     */

    /**
     * @property {string} latestDocument
     */

    /**
     * @property {string} latestGallery
     */

    /**
     * @property {number} verifiedDokumen
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] The data model schema
     */
    constructor(schema = {
        kegiatanId: null,
        namaKegiatan: null,
        periodeBatch: null,
        danaBatch: null
    }) {
        super(schema, 'kegiatanId');
        Vue.set(this, 'kode', null);
        Vue.set(this, 'status', null);
        Vue.set(this, 'tahun', null);
        Vue.set(this, 'programId', null);
        Vue.set(this, 'namaProgram', null);
        Vue.set(this, 'batchId', null);
        Vue.set(this, 'namaBatch', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'kode', data.kode);
        Vue.set(this, 'status', data.status);
        Vue.set(this, 'anggaran', data.anggaran);
        Vue.set(this, 'realisasi', data.realisasi);
        Vue.set(this, 'submittedBy', data.submittedBy);
        Vue.set(this, 'tanggalSubmitted', data.tanggalSubmitted);
        Vue.set(this, 'verifiedBy', data.verifiedBy);
        Vue.set(this, 'tanggalVerified', data.tanggalVerified);
        Vue.set(this, 'postedRealisasiBy', data.postedRealisasiBy);
        Vue.set(this, 'tanggalPostedRealisasi', data.tanggalPostedRealisasi);
        Vue.set(this, 'closedBy', data.closedBy);
        Vue.set(this, 'tanggalClosed', data.tanggalClosed);

        if (data.pencairan) {
            Vue.set(this, 'pencairan', data.pencairan);
            Vue.set(this, 'pencairan_sisa', data.pencairan_sisa);
        }

        if(data.totalKegiatanRealisasi){
            Vue.set(this, 'totalKegiatanRealisasi', data.totalKegiatanRealisasi);
        }
        if (data.totalKegiatanDokumentasi) {
            Vue.set(this, 'totalKegiatanDokumentasi', data.totalKegiatanDokumentasi);
            Vue.set(this, 'latestDocument', data.latestDocument);
            Vue.set(this, 'latestGallery', data.latestGallery);
            Vue.set(this, 'verifiedDokumen', data.verifiedDokumen);
        }
        if (data.danaBatch) {
            Vue.set(this, 'tahun', data.danaBatch.tahun);
            Vue.set(this, 'programId', data.danaBatch.id);
            Vue.set(this, 'namaProgram', data.danaBatch.namaProgram);
        }
        if (data.periodeBatch) {
            Vue.set(this, 'batchId', data.periodeBatch.batchId);
            Vue.set(this, 'namaBatch', data.periodeBatch.namaBatch);
            Vue.set(this, 'programCsrId', data.periodeBatch.programCsr.msid);
            Vue.set(this, 'programCsrName', data.periodeBatch.programCsr.name);
        }
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/pengelolaan/create',
            'fetch': '/kegiatan/pengelolaan/{id}',
            'delete': '/kegiatan/pengelolaan/delete/{id}',
            'update': '/kegiatan/pengelolaan/update/{id}',
            'stepflow': '/kegiatan/pengelolaan/stepflow/{id}',
        }
    }
}

