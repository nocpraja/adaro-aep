/**
 * Validators untuk data model Kegiatan
 *
 * @author Ahmad Fajar
 * @since  15/11/2018, modified: 15/05/2019 20:44
 */
import {required} from "vuelidate/lib/validators";

/**
 * Kegiatan Validator Model
 *
 * @type {Object}
 */
export const kegiatanValidator = {
    namaKegiatan: {required},
    programId: {required},
    tahun: {required}
};

/**
 * Kegiatan validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function kegiatanValidationHandler(field) {
    return {
        computed: {
            namaKegiatanValidator() {
                return {
                    hasError: this.$v[field].namaKegiatan.$error,
                    messages: {
                        required: 'Field nama kegiatan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaKegiatan.required
                    }
                };
            },
            programIdValidator() {
                return {
                    hasError: this.$v[field].programId.$error,
                    messages: {
                        required: 'Field nama program wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].programId.required
                    }
                };
            },
            tahunValidator() {
                return {
                    hasError: this.$v[field].tahun.$error,
                    messages: {
                        required: 'Field tahun anggaran wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].tahun.required
                    }
                };
            }
        }
    };
}
