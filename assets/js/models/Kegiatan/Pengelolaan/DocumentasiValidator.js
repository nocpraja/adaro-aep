/**
 * Validators untuk data model dokumentasi
 *
 * @author Alex Gaspersz
 * @since  06/02/2020, modified: 18/02/2020 02:09
 */

import {required} from "vuelidate/lib/validators";

/**
 * Dokumentasi Validator Model
 *
 * @type {Object}
 */
export const DokumentasiValidator = {
    subDoc: {required},
    docTitle: {required}
};

/**
 * Dokumentasi validation's handler.
 *
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function DokumentasiValidationHandler(field) {
    return {
        computed: {
            subDocValidator() {
                return {
                    hasError: this.$v[field].subDoc.$error,
                    messages: {
                        required: 'Field sub kegiatan wajib dipilih',
                    },
                    validators: {
                        required: this.$v[field].subDoc.required
                    }
                };
            },
            docTitleValidator() {
                return {
                    hasError: this.$v[field].docTitle.$error,
                    messages: {
                        required: 'Field nama dokumen wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].docTitle.required
                    }
                };
            },
        }
    };
}
