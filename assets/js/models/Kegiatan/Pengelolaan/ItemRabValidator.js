/**
 * Validators untuk data model Item RAB
 *
 * @author Ahmad Fajar
 * @since  19/11/2018, modified: 11/02/2019 2:09
 */

import { required, numeric } from "vuelidate/lib/validators";

/**
 * Item RAB Validator Model
 *
 * @type {Object}
 */
export const itemRabValidator = {
    title: {required},
    parentId: {required},
    accountId: {required},
    fixedCost: {required},
    jumlahQty1: {required, numeric},
    jumlahQty2: {required, numeric},
    frequency: {required, numeric},
    unitPrice: {required},
    unitQty1: {required},
    unitQty2: {required},
    unitFrequency: {required}
};

/**
 * Item RAB validation's handler.
 *
 * @param {string} field Field property name
 * @return {Object} Validation handler
 */
export function itemRabValidationHandler(field) {
    return {
        computed: {
            titleValidator() {
                return {
                    hasError: this.$v[field].title.$error,
                    messages: {
                        required: 'Field deskripsi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].title.required
                    }
                };
            },
            parentIdValidator() {
                return {
                    hasError: this.$v[field].parentId.$error,
                    messages: {
                        required: 'Field nama sub-kegiatan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].parentId.required
                    }
                };
            },
            accountIdValidator() {
                return {
                    hasError: this.$v[field].accountId.$error,
                    messages: {
                        required: 'Field nama account wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].accountId.required
                    }
                };
            },
            fixedCostValidator() {
                return {
                    hasError: this.$v[field].fixedCost.$error,
                    messages: {
                        required: 'Field metode perhitungan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].fixedCost.required
                    }
                };
            },
            jumlahQty1Validator() {
                return {
                    hasError: this.$v[field].jumlahQty1.$error,
                    messages: {
                        required: 'Field Amount Qty-1 wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].jumlahQty1.required
                    }
                };
            },
            jumlahQty2Validator() {
                return {
                    hasError: this.$v[field].jumlahQty2.$error,
                    messages: {
                        required: 'Field Amount Qty-2 wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].jumlahQty2.required
                    }
                };
            },
            frequencyValidator() {
                return {
                    hasError: this.$v[field].frequency.$error,
                    messages: {
                        required: 'Field frekuensi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].frequency.required
                    }
                };
            },
            unitPriceValidator() {
                return {
                    hasError: this.$v[field].unitPrice.$error,
                    messages: {
                        required: 'Field Amount per-Unit wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].unitPrice.required
                    }
                };
            },
            unitQty1Validator() {
                return {
                    hasError: this.$v[field].unitQty1.$error,
                    messages: {
                        required: 'Field unit satuan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].unitQty1.required
                    }
                };
            },
            unitQty2Validator() {
                return {
                    hasError: this.$v[field].unitQty2.$error,
                    messages: {
                        required: 'Field unit satuan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].unitQty2.required
                    }
                };
            },
            unitFrequencyValidator() {
                return {
                    hasError: this.$v[field].unitFrequency.$error,
                    messages: {
                        required: 'Field unit satuan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].unitFrequency.required
                    }
                };
            },
        }
    };
}
