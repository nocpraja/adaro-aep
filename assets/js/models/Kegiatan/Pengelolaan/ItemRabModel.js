import Vue from "vue";
import { BsModel } from "vue-bs4";
import {ItemRealisasiModel} from "./ItemRealisasiModel";

/**
 * Class data model ItemRabModel.
 *
 * @author Ahmad Fajar
 * @since  17/11/2018, modified: 19/05/2019 23:46
 */
export class ItemRabModel extends BsModel {
    /**
     * @property {int} id
     * ID item RAB.
     */

    /**
     * @property {string} title
     * Item name.
     */

    /**
     * @property {Object} account
     * Account RAB.
     */

    /**
     * @property {SubKegiatanModel|int} parent
     * Item RAB parent.
     */

    /**
     * @property {boolean} fixedCost
     * Perhitungan menggunakan metode FixedCost atau tidak.
     */

    /**
     * @property {Number|String} mgtFeeConst
     * Management Fee (dalam %).
     */

    /**
     * @property {number} jumlahQty1
     * Jumlah item Qty-1.
     */

    /**
     * @property {number} jumlahQty2
     * Jumlah item Qty-2.
     */

    /**
     * @property {number} frequency
     * Jumlah Frekuensi kegiatan.
     */

    /**
     * @property {number} unitPrice
     * Harga satuan.
     */

    /**
     * @property {string} unitQty1
     * Unit satuan item Qty-1.
     */

    /**
     * @property {string} unitQty2
     * Unit satuan item Qty-2.
     */

    /**
     * @property {string} unitFrequency
     * Unit satuan frekuensi kegiatan.
     */

    /**
     * @property {int} accountId
     * ID account RAB.
     */

    /**
     * @property {int} parentId
     * ID item RAB parent.
     */

    /**
     * @property {number} totalRealisasi
     */

    /**
     * @property {number} verifiedRealisasi
     */

    /**
     * @property {string} lastRealisasi
     */

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        id: null,
        title: null,
        remark: null,
        account: null,
        parent: null,
        fixedCost: true,
        jumlahQty1: null,
        jumlahQty2: null,
        frequency: null,
        unitPrice: null,
        unitQty1: null,
        unitQty2: null,
        unitFrequency: null,
        realisasiRabs: null
    }) {
        super(schema, 'id');
        Vue.set(this, 'accountId', null);
        Vue.set(this, 'parentId', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/pengelolaan/item-rab/create',
            'fetch': '/kegiatan/pengelolaan/item-rab/{id}',
            'delete': '/kegiatan/pengelolaan/item-rab/delete/{id}',
            'update': '/kegiatan/pengelolaan/item-rab/update/{id}',
            'realisasi': '/kegiatan/pengelolaan/item-rab/realisasi-rab/create'
        }
    }
}

