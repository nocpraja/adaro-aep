import {
    NEW,
    REJECTED,
    STATE_INVALID,
    STEP_REJECT,
    STEP_UPDATE,
    STEP_VERIFY,
    UPDATED,
    VERIFIED
} from "../../../store/modules/mutation-types";
import BaseConfigurer from "../../Workflow/BaseConfigurer";
import Transition from "../../Workflow/Transition";

/**
 * DokumentasiWorkflowConfigurer class.
 * NEW, VERIFIED, UNPUBLISHED, REJECTED
 * @author Alex Gaspersz
 * @since  17/02/2020 modified: 17/02/2020 22:13
 */
export default class DokumentasiWorkflowConfigurer extends BaseConfigurer {
    /**
     * Class constructor.
     *
     * @param {string} name A workflow name
     */
    constructor(name) {
        super(name, 'status', NEW);
    }

    /**
     * Get major place from object's marking.
     *
     * @param {Object} subject The object to check
     * @return {String} A major place name
     */
    majorPlaceMark(subject) {
        try {
            const marking = this.workflow.getMarking(subject);

            if (marking.isEmpty()) {
                return '';
            }

            const place = marking.places[0];

            if (place === REJECTED) {
                return REJECTED;
            } else if (place === VERIFIED) {
                return VERIFIED;
            } else if (this.workflow.canXor(subject, [STEP_UPDATE, STEP_REJECT])) {
                return NEW;
            }
        } catch (e) {
            return STATE_INVALID;
        }

        return '';
    }

    /**
     * Create places configurations.
     *
     * @return {void}
     * @private
     */
    _createPlaces() {
        this._places = [
            NEW, UPDATED, VERIFIED, REJECTED
        ];
    }

    /**
     * Create transition configurations.
     *
     * @return {void}
     * @private
     */
    _createTransitions() {
        this._transitions = [
            new Transition([NEW, UPDATED], VERIFIED, STEP_VERIFY),
            new Transition([NEW, UPDATED], REJECTED, STEP_REJECT),
        ];
    }

    /**
     * Get color name for badge.
     * NEW, UNPUBLISHED, VERIFIED, REJECTED
     * @param {Object} subject The subject to check
     * @return {string} Color name
     */
    badgeColor(subject) {
        const status = this.majorPlaceMark(subject);

        switch (status) {
            case NEW :
            case UPDATED :
                return 'info';
            case VERIFIED :
                return 'success-color-dark';
            case REJECTED :
                return 'danger';
            default :
                return 'dark';
        }
    }
}
