import {
    CLOSED,
    NEW,
    REALISASI,
    REALISASI_REJECTED,
    REALISASI_SUBMITTED,
    REALISASI_VERIFIED,
    REJECTED,
    STATE_INVALID,
    STEP_CLOSING,
    STEP_SUBMIT_REALISASI,
    STEP_REJECT,
    STEP_REJECT_REALISASI,
    STEP_SUBMIT,
    STEP_UPDATE,
    STEP_VERIFY,
    STEP_VERIFY_REALISASI,
    SUBMITTED,
    UPDATED,
    VERIFIED
} from "../../../store/modules/mutation-types";
import BaseConfigurer from "../../Workflow/BaseConfigurer";
import Transition from "../../Workflow/Transition";

/**
 * KegiatanWorkflowConfigurer class.
 *
 * @author Ahmad Fajar
 * @since  15/05/2019 modified: 18/05/2019 1:53
 */
export default class KegiatanWorkflowConfigurer extends BaseConfigurer {

    /**
     * Class constructor.
     *
     * @param {string} name A workflow name
     */
    constructor(name) {
        super(name, 'status', NEW);
    }

    /**
     * Get major place from object's marking.
     *
     * @param {Object} subject The object to check
     * @return {String} A major place name
     */
    majorPlaceMark(subject) {
        try {
            const marking = this.workflow.getMarking(subject);

            if (marking.isEmpty()) {
                return '';
            }

            const place = marking.places[0];

            if (place === REJECTED) {
                return REJECTED;
            } else if (place === REALISASI_VERIFIED) {
                return REALISASI_VERIFIED.replace('_', ' ');
            } else if (place === REALISASI_REJECTED) {
                return REALISASI_REJECTED.replace('_', ' ');
            } else if (place === CLOSED) {
                return CLOSED;
            } else if (this.workflow.canXor(subject, [STEP_UPDATE, STEP_SUBMIT])) {
                return NEW;
            } else if (this.workflow.can(subject, STEP_VERIFY)) {
                return SUBMITTED;
            } else if (this.workflow.canXor(subject, [STEP_SUBMIT_REALISASI, STEP_CLOSING])) {
                return VERIFIED;
            } else if (this.workflow.can(subject, STEP_VERIFY_REALISASI)) {
                return REALISASI;
            }
        } catch (e) {
            return STATE_INVALID;
        }

        return '';
    }

    /**
     * Get color name for badge.
     *
     * @param {Object} subject The subject to check
     * @return {string} Color name
     */
    badgeColor(subject) {
        const status = this.majorPlaceMark(subject);

        switch (status) {
            case NEW :
                return 'info';
            case SUBMITTED :
            case REALISASI :
                return 'primary';
            case VERIFIED :
                return 'success-color-dark';
            case STATE_INVALID :
            case REJECTED :
            case REALISASI_REJECTED.replace('_', ' ') :
                return 'danger';
            case REALISASI_VERIFIED.replace('_', ' ') :
                return 'success-color-dark';
            default :
                return 'dark';
        }
    }

    /**
     * Create places configurations.
     *
     * @return {void}
     * @private
     */
    _createPlaces() {
        this._places = [
            NEW, UPDATED, SUBMITTED, REJECTED, VERIFIED, CLOSED,
            REALISASI_SUBMITTED, REALISASI_REJECTED, REALISASI_VERIFIED
        ];
    }

    /**
     * Create transition configurations.
     *
     * @return {void}
     * @private
     */
    _createTransitions() {
        this._transitions = [
            new Transition([NEW, UPDATED, REJECTED], UPDATED, STEP_UPDATE),
            new Transition([NEW, UPDATED], SUBMITTED, STEP_SUBMIT),
            new Transition([SUBMITTED], REJECTED, STEP_REJECT),
            new Transition([SUBMITTED], VERIFIED, STEP_VERIFY),
            new Transition([VERIFIED, REALISASI_REJECTED], REALISASI_SUBMITTED, STEP_SUBMIT_REALISASI),
            new Transition([REALISASI_SUBMITTED], REALISASI_REJECTED, STEP_REJECT_REALISASI),
            new Transition([REALISASI_SUBMITTED], REALISASI_VERIFIED, STEP_VERIFY_REALISASI),
            new Transition([REALISASI_VERIFIED], CLOSED, STEP_CLOSING)
        ];
    }

}
