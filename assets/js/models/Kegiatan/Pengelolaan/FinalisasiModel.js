import Vue from "vue";
import {BsModel} from "vue-bs4";

export class FinalisasiModel extends BsModel {
    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model schema
     */
    constructor(schema = {
        id: null,
        kegiatan: null,
        mitra: null,
        judul: null,
        latarbelakang: null,
        tujuanprogram: null,
        outcome: null,
        ruanglingkup: null,
        pemangkukepentingan: null,
        status: 'NEW'
    }) {
        super(schema, 'id');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/finalisasi/create',
            'fetch': '/kegiatan/finalisasi/{id}',
            'delete': '/kegiatan/finalisasi/delete/{id}',
            'update': '/kegiatan/finalisasi/update/{id}'
        }
    }
}