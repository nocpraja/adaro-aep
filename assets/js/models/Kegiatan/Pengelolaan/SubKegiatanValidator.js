/**
 * Validators untuk data model Subkegiatan
 *
 * @author Ahmad Fajar
 * @since  19/11/2018, modified: 19/11/2018 04:18
 */
import { required } from "vuelidate/lib/validators";

/**
 * SubKegiatan Validator Model
 *
 * @type {Object}
 */
export const subkegiatanValidator = {
    title: {required},
    tanggalMulai: {required},
    provinsi: {required},
    kabupaten: {required}
};

/**
 * SubKegiatan validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function subkegiatanValidationHandler(field) {
    return {
        computed: {
            titleValidator() {
                return {
                    hasError: this.$v[field].title.$error,
                    messages: {
                        required: 'Field nama Subkegiatan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].title.required
                    }
                };
            },
            tanggalMulaiValidator() {
                return {
                    hasError: this.$v[field].tanggalMulai.$error,
                    messages: {
                        required: 'Field jadwal pelaksanaan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].tanggalMulai.required
                    }
                };
            },
            provinsiIdValidator() {
                return {
                    hasError: this.$v[field].provinsi.$error,
                    messages: {
                        required: 'Field lokasi kegiatan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].provinsi.required
                    }
                };
            },
            kabupatenIdValidator() {
                return {
                    hasError: this.$v[field].kabupaten.$error,
                    messages: {
                        required: 'Field lokasi kegiatan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kabupaten.required
                    }
                };
            }
        }
    };
}
