import { BsModel } from "vue-bs4";

/**
 * Class CapaianKpi
 *
 * @author Mico
 * @since  2/13/2019, modified: 03/02/2019 13:38
 */
export class CapaianKpiModel extends BsModel {
    /**
     * @property {int} id ID Capaian Kpi
     */

    /**
     * @property {int} msid Nama Bidang CSR
     */

    /**
     * @property {int} batch_id Capaian Kpi batch
     */

    /**
     * @property {int} data_ref Capaian Kpi Data Ref
     */

    /**
     * @property {int} posted_by Capaian Kpi posted by
     */

    /**
     * @property {int} updated_by Capaian Kpi updated by
     */

    /**
     * @property {int} tahun Capaian Kpi Tahun
     */

    /**
     * @property {bool} dashboard Capaian Kpi Dashboard
     */

    /**
     * @property {bool} kuantitas Capaian Kpi kuantitas
     */


    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model properties
     */
    constructor(schema = {
        id: null,
        msid: null,
        batch_id: null,
        data_ref: null,
        posted_by: null,
        update_by: null,
        tahun: null,
        dashboard: null,
        kuantitas: null,
    }) {
        super('id', schema);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object} Rest URL configuration
     */
    get restUrl() {
        return {
            'fetch': '/kegiatan/capaian-kpi/{id}',
            'save': '//kegiatan/capaian-kpi/create',
            'delete': '/kegiatan/capaian-kpi/delete/{id}',
            'update': '/kegiatan/capaian-kpi/update/{id}'
        }
    }
}