import { BsModel } from "vue-bs4";

/**
 * Class KendaliKegiatanModel
 *
 * @author Mark Melvin
 * @since  17/07/2019, modified: 17/07/2019 13:27
 */
export class KendaliKegiatanModel extends BsModel {

    /**
     * Class constructor.
     *
     * @param {Object} [schema] Data model properties
     */
    constructor(schema = {
        kegiatanId: null,
        kegiatan: null,
        bobot: null,
        rencana1: null,
        rencana2: null,
        rencana3: null,
        rencana4: null,
        rencana5: null,
        rencana6: null,
        rencana7: null,
        rencana8: null,
        rencana9: null,
        rencana10: null,
        rencana11: null,
        rencana12: null,
        realisasi1: null,
        realisasi2: null,
        realisasi3: null,
        realisasi4: null,
        realisasi5: null,
        realisasi6: null,
        realisasi7: null,
        realisasi8: null,
        realisasi9: null,
        realisasi10: null,
        realisasi11: null,
        realisasi12: null
    }) {
        super(schema, 'kegiatanId');
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object} Rest URL configuration
     */
    get restUrl() {
        return {
            'fetch': '/kegiatan/kendaliproyek/kendalikegiatan/{id}'
        }
    }
}
