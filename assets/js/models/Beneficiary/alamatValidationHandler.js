/**
 * Common validation's handler untuk data alamat.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export default function alamatValidationHandler(field) {
    return {
        computed: {
            alamatValidator() {
                return {
                    hasError: this.$v[field].alamat.$error,
                    messages: {
                        required: 'Field alamat tinggal wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].alamat.required
                    }
                };
            },
            provinsiIdValidator() {
                return {
                    hasError: this.$v[field].provinsiId.$error,
                    messages: {
                        required: 'Field provinsi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].provinsiId.required
                    }
                };
            },
            kabupatenIdValidator() {
                return {
                    hasError: this.$v[field].kabupatenId.$error,
                    messages: {
                        required: 'Field kabupaten wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kabupatenId.required
                    }
                };
            },
            kecamatanIdValidator() {
                return {
                    hasError: this.$v[field].kecamatanId.$error,
                    messages: {
                        required: 'Field kecamatan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kecamatanId.required
                    }
                };
            },
            kelurahanIdValidator() {
                return {
                    hasError: this.$v[field].kelurahanId.$error,
                    messages: {
                        required: 'Field kelurahan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kelurahanId.required
                    }
                };
            },
            rtRwValidator() {
                return {
                    hasError: this.$v[field].rtRw.$error,
                    messages: {
                        validRtRw: 'Format nilai RT/RW tidak benar',
                    },
                    validators: {
                        validRtRw: this.$v[field].rtRw.validRtRw
                    }
                };
            }
        }
    };
}
