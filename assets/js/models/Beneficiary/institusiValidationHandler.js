/**
 * Validation's handler untuk data utama penerima manfaat Institusi.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export default function institusiValidationHandler(field) {
    return {
        computed: {
            nomorAkteValidator() {
                return {
                    hasError: this.$v[field].nomorAkte.$error,
                    messages: {
                        required: 'Field nomor akte pendirian wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].nomorAkte.required
                    }
                };
            },
            tahunBerdiriValidator() {
                return {
                    hasError: this.$v[field].tahunBerdiri.$error,
                    messages: {
                        required: 'Field berdiri tahun wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].tahunBerdiri.required
                    }
                };
            },
            akreditasiValidator() {
                return {
                    hasError: this.$v[field].akreditasi.$error,
                    messages: {
                        required: 'Field akreditasi wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].akreditasi.required
                    }
                };
            },
            nilaiAkreditasiValidator() {
                return {
                    hasError: this.$v[field].nilaiAkreditasi.$error,
                    messages: {
                        requiredIf: 'Field nilai akreditasi wajib diisi',
                        customValue: 'Nilai akreditasi yang diperbolehkan hanya A, B atau C'
                    },
                    validators: {
                        requiredIf: this.$v[field].nilaiAkreditasi.requiredIf,
                        customValue: this.$v[field].nilaiAkreditasi.customValue
                    }
                };
            },
            emailValidator() {
                return {
                    hasError: this.$v[field].email.$error,
                    messages: {
                        email: 'Alamat email tidak benar',
                    },
                    validators: {
                        email: this.$v[field].email.email
                    }
                };
            }
        }
    }
}
