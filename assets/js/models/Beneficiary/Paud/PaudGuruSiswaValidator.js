/**
 * Validators untuk data model Penerima Manfaat Individu (Guru/Siswa PAUD)
 *
 * @author Tri N
 * @since  2/11/2018, modified: 24/05/2019 23:25
 */
import { email, required } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * Guru Paud Validation's model
 *
 * @type {Object}
 */
export const paudGuruValidator = {
    institusiId: {required},
    statusJabatan: {required},

    nik: {required},
    namaLengkap: {required},
    tempatLahir: {required},
    tanggalLahir: {required},
    jenisKelamin: {required},
    email: {email},

    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * Siswa Paud Validation's Model
 *
 * @type {Object}
 */
export const paudSiswaValidator = {
    institusiId: {required},

    nik: {required},
    namaLengkap: {required},
    tempatLahir: {required},
    tanggalLahir: {required},
    jenisKelamin: {required},
    email: {email}
};

/**
 * Guru/Siswa Paud validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function paudGuruSiswaValidationHandler(field) {
    return {
        computed: {
            institusiIdValidator() {
                return {
                    hasError: this.$v[field].institusiId.$error,
                    messages: {
                        required: 'Field institusi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].institusiId.required
                    }
                };
            },
            statusJabatanValidator() {
                return {
                    hasError: this.$v[field].statusJabatan.$error,
                    messages: {
                        required: 'Field status wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].statusJabatan.required
                    }
                };
            }
        }
    };
}
