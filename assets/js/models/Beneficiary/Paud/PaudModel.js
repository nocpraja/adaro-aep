import {InstitusiModel} from "../InstitusiModel";

/**
 * Class PaudModel.
 *
 * @author Tri N
 * @since  25/10/2018, modified: 5/11/2018 11:46
 */
export class PaudModel extends InstitusiModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/paud/create',
            'fetch': '/kegiatan/penerima-manfaat/paud/{id}',
            'delete': '/kegiatan/penerima-manfaat/paud/delete/{id}',
            'monitoring': '/kegiatan/penerima-manfaat/paud/monitoring/{id}',
            'update': '/kegiatan/penerima-manfaat/paud/update/{id}'
        }
    }
}