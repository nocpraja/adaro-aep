/**
 * Validators untuk data model PAUD
 *
 * @author Tri N
 * @since  25/10/2018, modified: 24/05/2019 23:25
 */
import { email, required, requiredIf } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * PAUD Validation's model
 *
 * @type {Object}
 */
export const paudValidator = {
    kodeInstitusi: {required},
    namaInstitusi: {required},
    jenisPaud: {required},
    swasta: {required},

    email: {email},
    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},

    nomorAkte: {required},
    tahunBerdiri: {required},
    akreditasi: {required},
    nilaiAkreditasi: {
        requiredIf: requiredIf(function () {
            return this.dtpaud.akreditasi === true;
        }),
        customValue: function () {
            if (this.dtpaud.akreditasi !== true) {
                return true;
            }
            return ['A', 'B', 'C', 'a', 'b', 'c'].includes(this.dtpaud.nilaiAkreditasi);
        }
    }
};

/**
 * PAUD validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function paudValidationHandler(field) {
    return {
        computed: {
            kodeInstitusiValidator() {
                return {
                    hasError: this.$v[field].kodeInstitusi.$error,
                    messages: {
                        required: 'Field kode sekolah wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kodeInstitusi.required
                    }
                };
            },
            namaInstitusiValidator() {
                return {
                    hasError: this.$v[field].namaInstitusi.$error,
                    messages: {
                        required: 'Field nama sekolah wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaInstitusi.required
                    }
                };
            },
            jenisPaudValidator() {
                return {
                    hasError: this.$v[field].jenisPaud.$error,
                    messages: {
                        required: 'Field kategori sekolah wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].jenisPaud.required
                    }
                };
            },
            statusSwastaValidator() {
                return {
                    hasError: this.$v[field].swasta.$error,
                    messages: {
                        required: 'Field status sekolah wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].swasta.required
                    }
                };
            },
            alamatValidator() {
                return {
                    hasError: this.$v[field].alamat.$error,
                    messages: {
                        required: 'Field alamat sekolah wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].alamat.required
                    }
                };
            }
        }
    };
}
