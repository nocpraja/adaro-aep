import {IndividuModel} from "../IndividuModel";

/**
 * Class PaudSiswaModel.
 *
 * @author  Tri N
 * @since   1/11/2018, modified: 5/11/2018 12:41
 */
export class PaudSiswaModel extends IndividuModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/paud/siswa/create',
            'fetch': '/kegiatan/penerima-manfaat/paud/siswa/{id}',
            'update': '/kegiatan/penerima-manfaat/paud/siswa/update/{id}',
            'delete': '/kegiatan/penerima-manfaat/paud/siswa/delete/{id}',
            'monitoring': '/kegiatan/penerima-manfaat/paud/siswa/monitoring/{id}'
        }
    }
}