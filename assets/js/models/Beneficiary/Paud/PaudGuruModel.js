import {IndividuModel} from "../IndividuModel";

/**
 * Class PaudGuruModel.
 *
 * @author  Tri N
 * @since   1/11/2018, modified: 1/11/2018 17:43
 */
export class PaudGuruModel extends IndividuModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/paud/guru/create',
            'fetch': '/kegiatan/penerima-manfaat/paud/guru/{id}',
            'update': '/kegiatan/penerima-manfaat/paud/guru/update/{id}',
            'delete': '/kegiatan/penerima-manfaat/paud/guru/delete/{id}',
            'monitoring': '/kegiatan/penerima-manfaat/paud/guru/monitoring/{id}'
        }
    }
}