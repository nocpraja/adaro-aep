/**
 * Validation's handler untuk data utama penerima manfaat individu.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export default function personValidationHandler(field) {
    return {
        computed: {
            nikValidator() {
                return {
                    hasError: this.$v[field].nik.$error,
                    messages: {
                        required: 'Field NIK wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].nik.required
                    }
                };
            },
            namaLengkapValidator() {
                return {
                    hasError: this.$v[field].namaLengkap.$error,
                    messages: {
                        required: 'Field nama lengkap wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaLengkap.required
                    }
                };
            },
            emailValidator() {
                return {
                    hasError: this.$v[field].email.$error,
                    messages: {
                        email: 'Alamat email tidak benar',
                    },
                    validators: {
                        email: this.$v[field].email.email
                    }
                };
            },
            tempatLahirValidator() {
                return {
                    hasError: this.$v[field].tempatLahir.$error,
                    messages: {
                        required: 'Field tempat lahir wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].tempatLahir.required
                    }
                };
            },
            tanggalLahirValidator() {
                return {
                    hasError: this.$v[field].tanggalLahir.$error,
                    messages: {
                        required: 'Field tanggal lahir wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].tanggalLahir.required
                    }
                };
            },
            jenisKelaminValidator() {
                return {
                    hasError: this.$v[field].jenisKelamin.$error,
                    messages: {
                        required: 'Field jenis kelamin wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].jenisKelamin.required
                    }
                };
            }
        }
    };
}
