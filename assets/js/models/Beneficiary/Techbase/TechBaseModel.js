import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class TechBaseModel.
 *
 * @author  Tri N
 * @since   28/11/2018, modified: 24/05/2019 0:51
 */
export class TechBaseModel extends BsModel {

    /**
     * @property {int} tbeId
     */

    /**
     * @property {IndividuModel} biodata
     */

    /**
     * @property {IndikatorKpiModel} kpi
     */

    /**
     * @property {string} status
     */

    /**
     * @property {UserAccountModel} verifiedBy
     */

    /**
     * @property {String|Date} tanggalVerified
     */

    /**
     * @property {UserAccountModel} approvedBy
     */

    /**
     * @property {String|Date} tanggalApproved
     */

    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        tbeId: null,
        namaInstitusi: null,
        alamatInstitusi: null,
        jenisInstitusi: null,
        nip: null,
        lamaMengajar: null,
        sertifikasi: null,
        mapel: [{text: ''}],
        prestasi: [{nama: '', tingkat:'', tahun: null}],
        pelatihan: [{nama: '', tahun: null}],
        biodata: {},
        kpi: null,
        status: null
    }) {
        super(schema, 'tbeId');
        Vue.set(this, 'biodataId', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'verifiedBy', data.verifiedBy);
        Vue.set(this, 'tanggalVerified', data.tanggalVerified);
        Vue.set(this, 'approvedBy', data.approvedBy);
        Vue.set(this, 'tanggalApproved', data.tanggalApproved);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/tech-base/create',
            'fetch': '/kegiatan/penerima-manfaat/tech-base/{id}',
            'update': '/kegiatan/penerima-manfaat/tech-base/update/{id}',
            'delete': '/kegiatan/penerima-manfaat/tech-base/delete/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/tech-base/stepflow/{id}',
        }
    }
}
