/**
 * Validators untuk data model TechBase Form Data Umum (Step 1)
 *
 * @author Tri N
 * @since  04/12/2018, modified: 24/05/2019 23:28
 */
import { email, required } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * Validation model untuk data utama peserta techBase.
 *
 * @type {Object}
 */
export const techBaseStep1Validator = {
    nip: {required}
};

/**
 * Validation model untuk biodata peserta techBase.
 *
 * @type {Object}
 */
export const techBaseBiodataValidator = {
    nik: {required},
    namaLengkap: {required},
    statusJabatan: {required},
    email: {email},
    tempatLahir: {required},
    tanggalLahir: {required},
    jenisKelamin: {required},
    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * Data utama peserta TechBase validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function techBaseStep1ValidationHandler(field) {
    return {
        computed: {
            nipValidator() {
                return {
                    hasError: this.$v[field].nip.$error,
                    messages: {
                        required: 'Field NIP wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].nip.required
                    }
                };
            }
        }
    };
}

/**
 * Biodata peserta TechBase validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function techBaseBiodataValidationHandler(field) {
    return {
        computed: {
            statusJabatanValidator() {
                return {
                    hasError: this.$v[field].statusJabatan.$error,
                    messages: {
                        required: 'Field status wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].statusJabatan.required
                    }
                };
            }
        }
    };
}
