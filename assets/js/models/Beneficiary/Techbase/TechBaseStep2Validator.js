/**
 * Validators untuk data model TechBase Form Data Lainnya (Step 2)
 *
 * @author Tri N
 * @since  04/12/2018, modified: 04/12/2018 14:20
 */
import { required } from "vuelidate/lib/validators";

/**
 * Validation model untuk data lainnya peserta techBase.
 *
 * @type {Object}
 */
export const techBaseStep2Validator = {
    namaInstitusi: {required},
    alamatInstitusi: {required},
    jenisInstitusi: {required},
    sertifikasi: {required},
};

/**
 * Data lainnya peserta TechBase validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function techBaseStep2ValidationHandler(field) {
    return {
        computed: {
            namaInstitusiValidator() {
                return {
                    hasError: this.$v[field].namaInstitusi.$error,
                    messages: {
                        required: 'Field nama institusi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaInstitusi.required
                    }
                };
            },
            alamatInstitusiValidator() {
                return {
                    hasError: this.$v[field].alamatInstitusi.$error,
                    messages: {
                        required: 'Field alamat institusi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].alamatInstitusi.required
                    }
                };
            },
            jenisInstitusiValidator() {
                return {
                    hasError: this.$v[field].jenisInstitusi.$error,
                    messages: {
                        required: 'Field jenis institusi wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].jenisInstitusi.required
                    }
                };
            },
            sertifikasiValidator() {
                return {
                    hasError: this.$v[field].sertifikasi.$error,
                    messages: {
                        required: 'Field sertifikasi wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].sertifikasi.required
                    }
                };
            }
        }
    };
}
