/**
 * Validators untuk data model Beasiswa Form Data Umum
 *
 * @author Ahmad Fajar
 * @since  23/11/2018, modified: 24/05/2019 23:26
 */
import { email, required } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * Validation model untuk data utama peserta beasiswa.
 *
 * @type {Object}
 */
export const beasiswaStep1Validator = {
    nim: {required},
    fakultasId: {required},
    jurusanId: {required}
};

/**
 * Validation model untuk biodata peserta beasiswa.
 *
 * @type {Object}
 */
export const beasiswaBiodataValidator = {
    nik: {required},
    namaLengkap: {required},
    email: {email},
    tempatLahir: {required},
    tanggalLahir: {required},
    jenisKelamin: {required},
    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * Data utama peserta Beasiswa validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function beasiswaStep1ValidationHandler(field) {
    return {
        computed: {
            nimValidator() {
                return {
                    hasError: this.$v[field].nim.$error,
                    messages: {
                        required: 'Field NIM wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].nim.required
                    }
                };
            },
            fakultasIdValidator() {
                return {
                    hasError: this.$v[field].fakultasId.$error,
                    messages: {
                        required: 'Field nama Fakultas wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].fakultasId.required
                    }
                };
            },
            jurusanIdValidator() {
                return {
                    hasError: this.$v[field].jurusanId.$error,
                    messages: {
                        required: 'Field nama Program Studi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].jurusanId.required
                    }
                };
            }
        }
    };
}
