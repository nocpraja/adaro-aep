import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class OrangTuaModel.
 *
 * @author  Tri N
 * @since   14/11/2018, modified: 24/05/2019 22:33
 */
export class OrangTuaModel extends BsModel
{
    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        namaAyah: null,
        namaIbu: null,
        pekerjaanAyah: null,
        pekerjaanIbu: null,
        penghasilan: null,
        alamat: null,
        rtRw: null,
        kodePos: null,
        kecamatan: null,
        kelurahan: null
    }) {
        super(schema, '');
        Vue.set(this, 'beasiswaId', null);
        Vue.set(this, 'provinsiId', null);
        Vue.set(this, 'kabupatenId', null);
        Vue.set(this, 'kecamatanId', null);
        Vue.set(this, 'kelurahanId', null);
    }
}
