import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class BeasiswaModel.
 *
 * @author  Tri N
 * @since   13/11/2018, modified: 24/05/2019 22:33
 */
export class BeasiswaModel extends BsModel
{
    /**
     * @property {int} beasiswaId
     */

    /**
     * @property {IndividuModel} biodata
     */

    /**
     * @property {IndikatorKpiModel} kpi
     */

    /**
     * @property {string} status
     */

    /**
     * @property {UserAccountModel} verifiedBy
     */

    /**
     * @property {String|Date} tanggalVerified
     */

    /**
     * @property {UserAccountModel} approvedBy
     */

    /**
     * @property {String|Date} tanggalApproved
     */

    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        beasiswaId: null,
        nim: null,
        tahunMasuk: null,
        jalurMasuk: null,
        nomorRekening: null,
        nomorCif: null,
        namaBank: null,
        kartuSehat: null,
        golonganDarah: null,
        sekolahAsal: null,
        alamatSekolah: null,
        prestasi: [{text: ''}],
        fakultas: null,
        jurusan: null,
        biodata: {},
        kpi: null,
        orangTua: null,
        status: null
    }) {
        super(schema, 'beasiswaId');
        Vue.set(this, 'biodataId', null);
        Vue.set(this, 'fakultasId', null);
        Vue.set(this, 'jurusanId', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'verifiedBy', data.verifiedBy);
        Vue.set(this, 'tanggalVerified', data.tanggalVerified);
        Vue.set(this, 'approvedBy', data.approvedBy);
        Vue.set(this, 'tanggalApproved', data.tanggalApproved);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/beasiswa/create',
            'fetch': '/kegiatan/penerima-manfaat/beasiswa/{id}',
            'update': '/kegiatan/penerima-manfaat/beasiswa/update/{id}',
            'delete': '/kegiatan/penerima-manfaat/beasiswa/delete/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/beasiswa/stepflow/{id}',
        }
    }
}
