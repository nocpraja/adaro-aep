/**
 * Validators untuk data model Beasiswa Form Data Lainnya
 *
 * @author Ahmad Fajar
 * @since  23/11/2018, modified: 24/05/2019 23:27
 */
import { required } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * Validation Model peserta beasiswa untuk data lainnya.
 *
 * @type {Object}
 */
export const beasiswaStep2Validator = {
    namaBank: {required},
    nomorRekening: {required}
};

/**
 * Validation Model untuk data orang tua peserta beasiswa.
 *
 * @type {Object}
 */
export const orangTuaValidator = {
    namaAyah: {required},
    namaIbu: {required},
    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * Validation's handler peserta beasiswa untuk data lainnya.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function beasiswaStep2ValidationHandler(field) {
    return {
        computed: {
            namaBankValidator() {
                return {
                    hasError: this.$v[field].namaBank.$error,
                    messages: {
                        required: 'Field nama Bank wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaBank.required
                    }
                };
            },
            nomorRekeningValidator() {
                return {
                    hasError: this.$v[field].nomorRekening.$error,
                    messages: {
                        required: 'Field nomor rekening Bank wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].nomorRekening.required
                    }
                };
            }
        }
    };
}

/**
 * Validation's handler untuk data orang tua peserta beasiswa.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function orangTuaValidationHandler(field) {
    return {
        computed: {
            namaAyahValidator() {
                return {
                    hasError: this.$v[field].namaAyah.$error,
                    messages: {
                        required: 'Field nama Ayah wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaAyah.required
                    }
                };
            },
            namaIbuValidator() {
                return {
                    hasError: this.$v[field].namaIbu.$error,
                    messages: {
                        required: 'Field nama Ibu wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaIbu.required
                    }
                };
            },
            alamatValidator() {
                return {
                    hasError: this.$v[field].alamat.$error,
                    messages: {
                        required: 'Field alamat tinggal orang tua wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].alamat.required
                    }
                };
            }
        }
    };
}
