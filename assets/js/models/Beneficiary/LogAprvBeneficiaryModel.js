import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class LogAprvBeneficiaryModel.
 */
export class LogAprvBeneficiaryModel extends BsModel {
    /**
     * @property {int|string} logId
     */

    /**
     * @property {int} kategori
     */

    /**
     * @property {string} logAction
     */

    /**
     * @property {string} logTitle
     */

    /**
     * @property {string} fromStatus
     */

    /**
     * @property {string} toStatus
     */

    /**
     * @property {string|null} keterangan
     */

    /**
     * @property {UserAccountModel} postedBy
     */

    /**
     * @property {String|Date} postedDate
     */

    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        logId: null,
        kategori: null,
        logAction: null,
        logTitle: null,
        fromStatus: null,
        toStatus: null,
        keterangan: null
    }) {
        super(schema, 'logId');
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'postedBy', data.postedBy);
        Vue.set(this, 'postedDate', data.postedDate);
    }
}
