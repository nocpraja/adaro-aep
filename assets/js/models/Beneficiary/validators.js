export function validRtRw(value) {
    if (typeof value === 'undefined' || value == null || value === '') {
        return true;
    }

    return new RegExp(/(^[0-9]{3}\/[0-9]{3})$/).test(value);
}
