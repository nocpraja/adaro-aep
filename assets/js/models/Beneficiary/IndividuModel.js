import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class IndividuModel.
 *
 * @author  Tri N
 * @since   3/11/2018, modified: 04/03/2020 01:10
 */
export class IndividuModel extends BsModel {
    /**
     * @property {int|string} individuId
     * ID individu penerima manfaat.
     */

    /**
     * @property {int} statusVerifikasi
     * Status verifikasi data.
     */

    /**
     * @property {string} namaLengkap
     * Nama lengkap penerima manfaat.
     */

    /**
     * @property {string} status
     */

    /**
     * @property {IndikatorKpiModel} kpi
     */

    /**
     * @property {UserAccountModel} verifiedBy
     */

    /**
     * @property {String|Date} tanggalVerified
     */

    /**
     * @property {UserAccountModel} approvedBy
     */

    /**
     * @property {String|Date} tanggalApproved
     */

    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        individuId: null,
        kategori: null,
        namaLengkap: null,
        nik: null,
        jenisKelamin: null,
        tempatLahir: null,
        tanggalLahir: null,
        phone: null,
        email: null,
        facebook: null,
        instagram: null,
        pendidikan: null,
        jabatan: null,
        statusJabatan: null,
        kompetensiWirausaha: [{text: null}],
        alamat: null,
        rtRw: null,
        kodePos: null,
        kecamatan: null,
        kelurahan: null,
        institusi: null,
        programs: null,
        kpi: null,
        status: null,
        statusVerifikasi: null,
    }) {
        super(schema, 'individuId');
        Vue.set(this, 'provinsiId', null);
        Vue.set(this, 'kabupatenId', null);
        Vue.set(this, 'kecamatanId', null);
        Vue.set(this, 'kelurahanId', null);
        Vue.set(this, 'institusiId', null);
        Vue.set(this, 'postedBy', null);
        Vue.set(this, 'postedDate', null);
        Vue.set(this, 'updatedBy', null);
        Vue.set(this, 'lastUpdated', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'verifiedBy', data.verifiedBy);
        Vue.set(this, 'tanggalVerified', data.tanggalVerified);
        Vue.set(this, 'approvedBy', data.approvedBy);
        Vue.set(this, 'tanggalApproved', data.tanggalApproved);
    }
}
