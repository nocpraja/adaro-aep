/**
 * Validators untuk data model Mitra Dudi SMK/Poltek
 *
 * @author Ahmad Fajar
 * @since  26/03/2019, modified: 26/03/2019 15:50
 */

import { required, minLength } from "vuelidate/lib/validators";

/**
 * Mitra DUDI validation's model.
 *
 * @type {Object}
 */
export const mitraDudiValidator = {
    namaUsaha: {required, minLength: minLength(4)},
    jenisUsaha: {required},
    namaPimpinan: {required, minLength: minLength(4)},
    contactPhone: {required},
    alamat: {required, minLength: minLength(8)}
};

/**
 * Mitra DUDI validation's handler.
 *
 * @return {Object} Validation handler
 */
export const mitraDudiValidationHandler = {
    methods: {
        namaUsahaValidator(element) {
            return {
                hasError: element.namaUsaha.$error,
                messages: {
                    required: 'Field nama usaha wajib diisi',
                    minLength: 'Field nama usaha setidaknya harus memiliki 4 karakter'
                },
                validators: {
                    required: element.namaUsaha.required,
                    minLength: element.namaUsaha.minLength
                }
            };
        },
        jenisUsahaValidator(element) {
            return {
                hasError: element.jenisUsaha.$error,
                messages: {
                    required: 'Field jenis usaha wajib diisi'
                },
                validators: {
                    required: element.jenisUsaha.required
                }
            };
        },
        namaPimpinanValidator(element) {
            return {
                hasError: element.namaPimpinan.$error,
                messages: {
                    required: 'Field nama pimpinan wajib diisi',
                    minLength: 'Field nama pimpinan setidaknya harus memiliki 4 karakter'
                },
                validators: {
                    required: element.namaPimpinan.required,
                    minLength: element.namaPimpinan.minLength
                }
            };
        },
        contactPhoneValidator(element) {
            return {
                hasError: element.contactPhone.$error,
                messages: {
                    required: 'Field nomor kontak wajib diisi'
                },
                validators: {
                    required: element.contactPhone.required
                }
            };
        },
        alamatValidator(element) {
            return {
                hasError: element.alamat.$error,
                messages: {
                    required: 'Field alamat wajib diisi',
                    minLength: 'Field alamat setidaknya harus memiliki 8 karakter'
                },
                validators: {
                    required: element.alamat.required,
                    minLength: element.alamat.minLength
                }
            };
        }
    }
};
