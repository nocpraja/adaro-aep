import Vue from 'vue';
import { BsModel } from "vue-bs4";

/**
 * Class ProgramKeahlianModel.
 *
 * @author Mark Melvin
 * @since  21/02/2019, modified: 24/05/2019 22:25
 */
export class ProgramKeahlianModel extends BsModel {
    /**
     * @property {Number} jurusanId
     * ID Program Keahlian/Studi.
     */

    /**
     * @property {Number} jenis
     * Tipe jurusan, jika bernilai:
     * 1 => Program Keahlian,
     * 2 => Program Studi
     */

    /**
     * @property {string} namaJurusan
     * Nama Program Keahlian/Studi.
     */

    /**
     * @property {Object[]} pesertaDidik
     * Jumlah peserta didik pertahun.
     */

    /**
     * @property {Number} dosenMkdu
     * Jumlah dosen pengajar MKDU.
     */

    /**
     * @property {Number} dosenProdi
     * Jumlah dosen pengajar mata kuliah jurusan.
     */

    /**
     * @property {Number} guruProduktif
     * Jumlah guru produktif.
     */

    /**
     * @property {Number} guruAdaptif
     * Jumlah guru adaptif.
     */

    /**
     * @property {Number} guruNormatif
     * Jumlah guru normatif.
     */


    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        jurusanId: null,
        jenis: null,
        namaJurusan: null,
        nilaiAkreditasi: null,
        contactPhone: null,
        kepalaJurusan: null,
        dosenMkdu: null,
        dosenProdi: null,
        guruProduktif: null,
        guruAdaptif: null,
        guruNormatif: null,
        rombelX: null,
        rombelXi: null,
        rombelXii: null,
        jumlahSiswa: null,
        pesertaDidik: [{tahun: null, jumlah: null}],
        unitProduksi: null,
        mitraDudis: [],
        institusi: null
    }) {
        super(schema, "jurusanId");
        Vue.set(this, "institusiId", null);
        Vue.set(this, 'postedBy', null);
        Vue.set(this, 'postedDate', null);
        Vue.set(this, 'updatedBy', null);
        Vue.set(this, 'lastUpdated', null);
    }

    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pendidikan/program-keahlian/create',
            'fetch': '/kegiatan/penerima-manfaat/pendidikan/program-keahlian/{id}',
            'delete': '/kegiatan/penerima-manfaat/pendidikan/program-keahlian/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pendidikan/program-keahlian/update/{id}'
        }
    }
}
