/**
 * Validators untuk data model Penerima Manfaat Individu
 *
 * @author Mark Melvin
 * @since  2/11/2018, modified: 24/05/2019 23:21
 */
import { email, required } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * Ustadz & Manajemen Validation's model
 *
 * @type {Object}
 */
export const personilValidator = {
    institusiId: {required},
    statusJabatan: {required},
    kategori: {required},

    nik: {required},
    namaLengkap: {required},
    tempatLahir: {required},
    tanggalLahir: {required},
    jenisKelamin: {required},
    email: {email},

    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * Siswa Validation's Model
 *
 * @type {Object}
 */
export const siswaValidator = {
    institusiId: {required},

    nik: {required},
    namaLengkap: {required},
    tempatLahir: {required},
    tanggalLahir: {required},
    jenisKelamin: {required},
    email: {email},
    kategori: {required},

    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * personil Pelatihan validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function pendidikanValidationHandler(field) {
    return {
        computed: {
            institusiIdValidator() {
                return {
                    hasError: this.$v[field].institusiId.$error,
                    messages: {
                        required: 'Field institusi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].institusiId.required
                    }
                };
            },
            statusJabatanValidator() {
                return {
                    hasError: this.$v[field].statusJabatan.$error,
                    messages: {
                        required: 'Field status wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].statusJabatan.required
                    }
                };
            },
            kategoriSiswaValidator() {
                return {
                    hasError: this.$v[field].kategori.$error,
                    messages: {
                        required: 'Field kategori wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].kategori.required
                    }
                };
            },
            kategoriGuruValidator() {
                return {
                    hasError: this.$v[field].kategori.$error,
                    messages: {
                        required: 'Field kategori wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].kategori.required
                    }
                };
            }

        }
    };

}
