/**
 * Validators untuk data model Unit Produksi dari Program Keahlian ataupun Program Studi
 *
 * @author Mark Melvin
 * @since  04/03/2019, modified: 26/03/2019 15:54
 */
import { required } from "vuelidate/lib/validators";

/**
 * Unit produksi validation's model.
 *
 * @type {Object}
 */
export const unitProduksiValidator = {
    namaUnit: {required},
    jenis: {required}
};

/**
 * Unit usaha Progli/Prodi validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function unitProduksiValidationHandler(field) {
    return {
        computed: {
            namaUnitValidator() {
                return {
                    hasError: this.$v[field].namaUnit.$error,
                    messages: {
                        required: 'Field nama Unit Produksi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaUnit.required
                    }
                };
            },
            jenisValidator() {
                return {
                    hasError: this.$v[field].jenis.$error,
                    messages: {
                        required: 'Field Jenis Usaha wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].jenis.required
                    }
                };
            }
        }
    };
}
