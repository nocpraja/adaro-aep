import {IndividuModel} from "../IndividuModel";

/**
 * Class ManajemenModel.
 *
 * @author Mark Melvin
 * @since  30/11/2018, modified: 04/03/2020 23:25
 */
export class ManajemenModel extends IndividuModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pendidikan/manajemen/create',
            'fetch': '/kegiatan/penerima-manfaat/pendidikan/manajemen/{id}',
            'delete': '/kegiatan/penerima-manfaat/pendidikan/manajemen/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pendidikan/manajemen/update/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/pendidikan/manajemen/stepflow/{id}',
        }
    }
}
