import { BsModel } from "vue-bs4";

/**
 * Class MitraDudiModel.
 *
 * @author Mark Melvin
 * @since  21/02/2019, modified: 27/03/2019 3:24
 */
export class MitraDudiModel extends BsModel {
    /**
     * @property {int} dudiId
     * ID Mitra DUDI.
     */

    /**
     * @property {int} namaUsaha
     * Nama usaha/industri Mitra DUDI.
     */

    /**
     * @property {int} jenisUsaha
     * Jenis usaha Mitra DUDI.
     */

    /**
     * @property {int} namaPimpinan
     * Nama pimpinan Mitra DUDI.
     */

    /**
     * @property {int} contactPhone
     * Nomor kontak (HP) pimpinan Mitra DUDI.
     */

    /**
     * @property {string} alamat
     * Alamat Mitra DUDI.
     */


    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        namaUsaha: null,
        jenisUsaha: null,
        namaPimpinan: null,
        contactPhone: null,
        alamat: null
    }) {
        super(schema, "dudiId");
    }

}
