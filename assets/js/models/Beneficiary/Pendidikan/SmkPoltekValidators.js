/**
 * Validators untuk data model institusi SMK/Poltek
 *
 * @author Hendra
 * @since  25/10/2018, modified: 24/05/2019 23:22
 */
import { email, required, requiredIf } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * SMK/Poltek Validation's model.
 *
 * @type {Object}
 */
export const smkPoltekValidator = {
    kodeInstitusi: {required},
    namaInstitusi: {required},
    swasta: {required},

    email: {email},
    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},

    nomorAkte: {required},
    tahunBerdiri: {required},
    akreditasi: {required},
    nilaiAkreditasi: {
        requiredIf: requiredIf(function () {
            return (this.dtpendidikan.akreditasi === true);
        }),
        customValue: function () {
            if (this.dtpendidikan.akreditasi !== true) {
                return true;
            }
            return ['A', 'B', 'C', 'a', 'b', 'c'].includes(this.dtpendidikan.nilaiAkreditasi);
        }
    }
};

/**
 * SMK/Poltek validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function smkPoltekValidationHandler(field) {
    return {
        computed: {
            nomorIndukValidator() {
                return {
                    hasError: this.$v[field].kodeInstitusi.$error,
                    messages: {
                        required: 'Field nomor induk wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kodeInstitusi.required
                    }
                };
            },
            namaSmkPoltekValidator() {
                return {
                    hasError: this.$v[field].namaInstitusi.$error,
                    messages: {
                        required: 'Field nama institusi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaInstitusi.required
                    }
                };
            },
            swastaValidator() {
                return {
                    hasError: this.$v[field].swasta.$error,
                    messages: {
                        required: 'Field status wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].swasta.required
                    }
                };
            },
            alamatValidator() {
                return {
                    hasError: this.$v[field].alamat.$error,
                    messages: {
                        required: 'Field alamat institusi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].alamat.required
                    }
                };
            }
        }
    };
}
