import { InstitusiModel } from "../InstitusiModel";

/**
 * Class SmkPoltekModel.
 *
 * @author Hendra
 * @since  29/10/2018, modified: 04/03/2020 23:26
 */
export class SmkPoltekModel extends InstitusiModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pendidikan/smk-poltek/create',
            'fetch': '/kegiatan/penerima-manfaat/pendidikan/smk-poltek/{id}',
            'delete': '/kegiatan/penerima-manfaat/pendidikan/smk-poltek/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pendidikan/smk-poltek/update/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/pendidikan/smk-poltek/stepflow/{id}'
        }
    }
}
