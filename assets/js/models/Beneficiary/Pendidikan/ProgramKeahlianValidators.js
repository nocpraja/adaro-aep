/**
 * Validators untuk data model Program Keahlian SMK/Poltek
 *
 * @author Mark Melvin
 * @since  04/03/2019, modified: 26/03/2019 13:12
 */
import { required, minLength } from "vuelidate/lib/validators";

/**
 * Program Keahlian/Studi validation's model.
 *
 * @type {Object}
 */
export const programKeahlianValidator = {
    namaJurusan: {required, minLength: minLength(3) },
    kepalaJurusan: {required, minLength: minLength(4)},
    dosenProdi: {
        minimalSalahSatu: function () {
            if (this.progli.jenis === 2) {
                return (this.progli.dosenMkdu !== null && !isNaN(this.progli.dosenMkdu)) ||
                    (this.progli.dosenProdi !== null && !isNaN(this.progli.dosenProdi));
            }

            return true;
        }
    },
    guruNormatif: {
        minimalSalahSatu: function () {
            if (this.progli.jenis === 1) {
                return (this.progli.guruProduktif !== null && !isNaN(this.progli.guruProduktif)) ||
                    (this.progli.guruAdaptif !== null && !isNaN(this.progli.guruAdaptif)) ||
                    (this.progli.guruNormatif !== null && !isNaN(this.progli.guruNormatif));
            }

            return true;
        }
    },
    rombelXii: {
        minimalSalahSatu: function () {
            if (this.progli.jenis === 1) {
                return (this.progli.rombelX !== null && !isNaN(this.progli.rombelX)) ||
                    (this.progli.rombelXi !== null && !isNaN(this.progli.rombelXi)) ||
                    (this.progli.rombelXii !== null && !isNaN(this.progli.rombelXii));
            }

            return true;
        }
    }
};

/**
 * Program Keahlian/Studi validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function programKeahlianValidationHandler(field) {
    return {
        computed: {
            namaJurusanValidator() {
                return {
                    hasError: this.$v[field].namaJurusan.$error,
                    messages: {
                        required: 'Field nama ' + (this[field].jenis === 2 ? 'Program Studi' : 'Program Keahlian') +
                            ' wajib diisi',
                        minLength: 'Field nama ' + (this[field].jenis === 2 ? 'Program Studi' : 'Program Keahlian') +
                            ' setidaknya harus memiliki 3 karakter'
                    },
                    validators: {
                        required: this.$v[field].namaJurusan.required,
                        minLength: this.$v[field].namaJurusan.minLength
                    }
                };
            },
            kepalaJurusanValidator() {
                return {
                    hasError: this.$v[field].kepalaJurusan.$error,
                    messages: {
                        required: 'Field ' + (this[field].jenis === 2 ? 'Nama Dekan' : 'Kepala Jurusan') +
                            ' wajib diisi',
                        minLength: 'Field ' + (this[field].jenis === 2 ? 'Nama Dekan' : 'Kepala Jurusan') +
                            ' setidaknya harus memiliki 4 karakter'
                    },
                    validators: {
                        required: this.$v[field].kepalaJurusan.required,
                        minLength: this.$v[field].kepalaJurusan.minLength
                    }
                };
            },
            jumlahDosenValidator() {
                return {
                    hasError: this.$v[field].dosenProdi.$error,
                    messages: {
                        minimalSalahSatu: 'Field jumlah Dosen harus diisi minimal salah satu'
                    },
                    validators: {
                        minimalSalahSatu: this.$v[field].dosenProdi.minimalSalahSatu
                    }
                };
            },
            jumlahGuruValidator() {
                return {
                    hasError: this.$v[field].guruNormatif.$error,
                    messages: {
                        minimalSalahSatu: 'Field jumlah Guru harus diisi minimal salah satu'
                    },
                    validators: {
                        minimalSalahSatu: this.$v[field].guruNormatif.minimalSalahSatu
                    }
                };
            },
            rombelValidator() {
                return {
                    hasError: this.$v[field].rombelXii.$error,
                    messages: {
                        minimalSalahSatu: 'Field jumlah rombel harus diisi minimal salah satu'
                    },
                    validators: {
                        minimalSalahSatu: this.$v[field].rombelXii.minimalSalahSatu
                    }
                };
            }
        }
    };
}
