import {IndividuModel} from "../IndividuModel";

/**
 * Class UstadzModel.
 *
 * @author Mark Melvin
 * @since  30/11/2018, modified: 04/03/2020 23:25
 */
export class GuruModel extends IndividuModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pendidikan/guru/create',
            'fetch': '/kegiatan/penerima-manfaat/pendidikan/guru/{id}',
            'delete': '/kegiatan/penerima-manfaat/pendidikan/guru/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pendidikan/guru/update/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/pendidikan/guru/stepflow/{id}',
        }
    }
}
