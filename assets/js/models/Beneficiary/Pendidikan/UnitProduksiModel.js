import { BsModel } from "vue-bs4";

/**
 * Class UnitProduksiModel.
 *
 * @author Mark Melvin
 * @since  21/02/2019, modified: 26/03/2019 12:21
 */
export class UnitProduksiModel extends BsModel {

    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        namaUnit: null,
        jenis: null,
        omset: null
    }) {
        super(schema, "produksiId");
    }

}
