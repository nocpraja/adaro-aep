import {IndividuModel} from "../IndividuModel";

/**
 * Class SantriModel.
 *
 * @author Mark Melvin
 * @since  30/11/2018, modified: 04/03/2020 23:26
 */
export class SiswaModel extends IndividuModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pendidikan/siswa/create',
            'fetch': '/kegiatan/penerima-manfaat/pendidikan/siswa/{id}',
            'delete': '/kegiatan/penerima-manfaat/pendidikan/siswa/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pendidikan/siswa/update/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/pendidikan/siswa/stepflow/{id}',
        }
    }
}
