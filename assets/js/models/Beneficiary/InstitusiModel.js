import Vue from "vue";
import { BsModel } from "vue-bs4";

/**
 * Class InstitusiModel.
 *
 * @author Hendra
 * @since  29/10/2018, modified: 13/02/2019 1:08
 */
export class InstitusiModel extends BsModel {
    /**
     * @property {int|string} institusiId
     * ID institusi penerima manfaat.
     */
    /**
     * @property {string} namaInstitusi
     * Nama institusi penerima manfaat.
     */
    /**
     * @property {int} kategori
     * Kategori institusi penerima manfaat.
     */

    /**
     * @property {int} statusVerifikasi
     * Status verifikasi data.
     */

    /**
     * @property {string} status
     */

    /**
     * @property {IndikatorKpiModel} kpi
     */

    /**
     * @property {UserAccountModel} verifiedBy
     */

    /**
     * @property {String|Date} tanggalVerified
     */

    /**
     * @property {UserAccountModel} approvedBy
     */

    /**
     * @property {String|Date} tanggalApproved
     */

    /**
     * Constructor Class
     *
     * @param {Object} [schema] Data model
     */
    constructor(schema = {
        institusiId: null,
        kodeInstitusi: null,
        namaInstitusi: null,
        kategori: null,
        swasta: null,
        nomorAkte: null,
        tahunBerdiri: null,
        akreditasi: false,
        nilaiAkreditasi: null,
        jenisPaud: null,
        email: null,
        website: null,
        officePhone: null,
        contactPhone: null,
        namaPimpinan: null,
        latitude: null,
        longitude: null,
        alamat: null,
        rtRw: null,
        kodePos: null,
        kecamatan: null,
        kelurahan: null,
        programs: null,
        kpi: null,
        status: null,
        statusVerifikasi: null,
    }) {
        super(schema, "institusiId");
        Vue.set(this, 'provinsiId', null);
        Vue.set(this, 'kabupatenId', null);
        Vue.set(this, 'kecamatanId', null);
        Vue.set(this, 'kelurahanId', null);
        Vue.set(this, 'postedBy', null);
        Vue.set(this, 'postedDate', null);
        Vue.set(this, 'updatedBy', null);
        Vue.set(this, 'lastUpdated', null);
    }

    /**
     * Action trigger after data was fetched from the remote server.
     *
     * @param {Object} data The response data
     * @return {void}
     */
    onAfterFetch(data) {
        Vue.set(this, 'verifiedBy', data.verifiedBy);
        Vue.set(this, 'tanggalVerified', data.tanggalVerified);
        Vue.set(this, 'approvedBy', data.approvedBy);
        Vue.set(this, 'tanggalApproved', data.tanggalApproved);
    }
}
