/**
 * Validators untuk data model Pesantren/NonPesantren
 *
 * @author Mark Melin
 * @since  29/11/2018, modified: 24/05/2019 22:53
 */
import { between, email, integer, required } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * Pesantren/NonPesantren Validation's model
 *
 * @type {Object}
 */
export const pesantrenValidator = {
    kodeInstitusi: {required},
    namaInstitusi: {required},
    swasta: {required},
    kategori: {required, integer, between: between(4, 5)},

    email: {email},
    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},

    nomorAkte: {required},
    tahunBerdiri: {required},
};

/**
 * Pesantren/NonPesantren validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function pesantrenValidationHandler(field) {
    return {
        computed: {
            kodeInstitusiValidator() {
                return {
                    hasError: this.$v[field].kodeInstitusi.$error,
                    messages: {
                        required: 'Field kode lembaga wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].kodeInstitusi.required
                    }
                };
            },
            namaInstitusiValidator() {
                return {
                    hasError: this.$v[field].namaInstitusi.$error,
                    messages: {
                        required: 'Field nama lembaga wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].namaInstitusi.required
                    }
                };
            },
            kategoriPesantrenValidator() {
                return {
                    hasError: this.$v[field].kategori.$error,
                    messages: {
                        required: 'Field kategori lembaga wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].kategori.required,
                        integer: this.$v[field].kategori.integer,
                        between: this.$v[field].kategori.between
                    }
                };
            },
            statusSwastaValidator() {
                return {
                    hasError: this.$v[field].swasta.$error,
                    messages: {
                        required: 'Field status lembaga wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].swasta.required
                    }
                };
            },
            alamatValidator() {
                return {
                    hasError: this.$v[field].alamat.$error,
                    messages: {
                        required: 'Field alamat lembaga wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].alamat.required
                    }
                };
            }
        }
    };
}