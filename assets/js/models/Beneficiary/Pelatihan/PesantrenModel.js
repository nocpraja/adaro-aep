import {InstitusiModel} from "../InstitusiModel";

/**
 * Class PesantrenModel.
 *
 * @author Mark Melvin
 * @since  30/11/2018, modified: 03/03/2020 15:25
 */
export class PesantrenModel extends InstitusiModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pelatihan/institusi/create',
            'fetch': '/kegiatan/penerima-manfaat/pelatihan/institusi/{id}',
            'delete': '/kegiatan/penerima-manfaat/pelatihan/institusi/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pelatihan/institusi/update/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/pelatihan/institusi/stepflow/{id}',
        }
    }
}
