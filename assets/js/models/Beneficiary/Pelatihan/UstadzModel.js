import {IndividuModel} from "../IndividuModel";

/**
 * Class UstadzModel.
 *
 * @author Mark Melvin
 * @since  30/11/2018, modified: 03/03/2020 15:24
 */
export class UstadzModel extends IndividuModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pelatihan/ustadz/create',
            'fetch': '/kegiatan/penerima-manfaat/pelatihan/ustadz/{id}',
            'delete': '/kegiatan/penerima-manfaat/pelatihan/ustadz/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pelatihan/ustadz/update/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/pelatihan/ustadz/stepflow/{id}',
        }
    }
}
