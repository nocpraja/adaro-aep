import {IndividuModel} from "../IndividuModel";

/**
 * Class ManajemenModel.
 *
 * @author Mark Melvin
 * @since  30/11/2018, modified: 03/03/2020 15:24
 */
export class ManajemenModel extends IndividuModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @return {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pelatihan/manajemen/create',
            'fetch': '/kegiatan/penerima-manfaat/pelatihan/manajemen/{id}',
            'delete': '/kegiatan/penerima-manfaat/pelatihan/manajemen/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pelatihan/manajemen/update/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/pelatihan/manajemen/stepflow/{id}',
        }
    }
}
