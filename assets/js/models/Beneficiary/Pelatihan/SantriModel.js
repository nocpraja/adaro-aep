import { IndividuModel } from "../IndividuModel";

/**
 * Class SantriModel.
 *
 * @author Mark Melvin
 * @since  30/11/2018, modified: 03/03/2020 15:24
 */
export class SantriModel extends IndividuModel {
    /**
     * Define REST URL configuration in the form <code>{key: name}</code>, where the keys are:
     * 'save', 'fetch', 'delete', 'update'.
     *
     * @type {Object}
     */
    get restUrl() {
        return {
            'save': '/kegiatan/penerima-manfaat/pelatihan/santri/create',
            'fetch': '/kegiatan/penerima-manfaat/pelatihan/santri/{id}',
            'delete': '/kegiatan/penerima-manfaat/pelatihan/santri/delete/{id}',
            'update': '/kegiatan/penerima-manfaat/pelatihan/santri/update/{id}',
            'stepflow': '/kegiatan/penerima-manfaat/pelatihan/santri/stepflow/{id}',
        }
    }
}
