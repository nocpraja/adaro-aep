/**
 * Validators untuk data model Penerima Manfaat Individu
 *
 * @author Mark Melvin
 * @since  2/11/2018, modified: 24/05/2019 22:53
 */
import { email, required } from "vuelidate/lib/validators";
import { validRtRw } from "../validators";

/**
 * Ustadz & Manajemen Validation's model
 *
 * @type {Object}
 */
export const personilValidator = {
    institusiId: {required},
    statusJabatan: {required},
    kategori: {required},

    nik: {required},
    namaLengkap: {required},
    tempatLahir: {required},
    tanggalLahir: {required},
    jenisKelamin: {required},
    email: {email},

    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * Siswa/Santri Validation's Model
 *
 * @type {Object}
 */
export const santriValidator = {
    institusiId: {required},

    nik: {required},
    namaLengkap: {required},
    tempatLahir: {required},
    tanggalLahir: {required},
    jenisKelamin: {required},
    email: {email},
    kategori: {required},
    pendidikan: {required},

    alamat: {required},
    provinsiId: {required},
    kabupatenId: {required},
    kecamatanId: {required},
    kelurahanId: {required},
    rtRw: {validRtRw},
};

/**
 * Personil Pelatihan validation's handler.
 *
 * @param {string} field Vue component's property to bind
 * @return {Object} Validation handler
 */
export function pelatihanValidationHandler(field) {
    return {
        computed: {
            institusiIdValidator() {
                return {
                    hasError: this.$v[field].institusiId.$error,
                    messages: {
                        required: 'Field institusi wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].institusiId.required
                    }
                };
            },
            statusJabatanValidator() {
                return {
                    hasError: this.$v[field].statusJabatan.$error,
                    messages: {
                        required: 'Field status wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].statusJabatan.required
                    }
                };
            },
            kategoriSiswaValidator() {
                return {
                    hasError: this.$v[field].kategori.$error,
                    messages: {
                        required: 'Field kategori wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].kategori.required
                    }
                };
            },
            kategoriGuruValidator() {
                return {
                    hasError: this.$v[field].kategori.$error,
                    messages: {
                        required: 'Field kategori wajib dipilih salah satu',
                    },
                    validators: {
                        required: this.$v[field].kategori.required
                    }
                };
            },
            pendidikanSantriValidator() {
                return {
                    hasError: this.$v[field].pendidikan.$error,
                    messages: {
                        required: 'Field Tingkat Pendidikan wajib diisi',
                    },
                    validators: {
                        required: this.$v[field].pendidikan.required
                    }
                };
            }

        }
    };
}
