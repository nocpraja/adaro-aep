// eslint-disable-next-line no-undef
const viewResolver = (name) => require('../views/' + name).default;

const routes = [
    // SECTION STARTPAGE
    {
        path: '/',
        component: viewResolver('StartPage')
    }, {
        path: '/welcome',
        component: viewResolver('Welcome')
    },

    // SECTION UTK MENU DASHBOARD
    {
        path: '/dashboard',
        component: viewResolver('Dashboard/Overview'),
        name: 'dashboard',
        meta: {title: 'Dashboard'},
    },

    // SECTION UTK MENU PENDANAAN
    // PENDANAAN -> DANA GLOBAL
    {
        path: '/pendanaan/global',
        component: viewResolver('Pendanaan/Grids/GridDanaGlobal'),
        meta: {title: 'Dana Global'}
    }, {
        path: '/pendanaan/global/adjust',
        component: viewResolver('Pendanaan/Forms/FormDanaGlobal'),
        meta: {title: 'Sunting Dana Global', previous: '/pendanaan/global'}
    }, {
        path: '/pendanaan/global/adjust/history',
        component: viewResolver('Pendanaan/Grids/GridHistoryDanaGlobal'),
        meta: {title: 'Riwayat Penambahan Dana Global', previous: '..'}
    }, {
        path: '/pendanaan/global/adjust/history/verification/:id',
        component: viewResolver('Pendanaan/Grids/GridVerifyDanaGlobal'),
        meta: {title: 'Riwayat Verifikasi Dana Global', previous: '..'}
    },
    // PENDANAAN -> DANA BIDANG
    {
        path: '/pendanaan/bidang',
        component: viewResolver('Pendanaan/Grids/GridDanaBidang'),
        meta: {title: 'Dana Bidang'}
    }, {
        path: '/pendanaan/bidang/create',
        component: viewResolver('Pendanaan/Forms/FormDanaBidang'),
        meta: {title: 'Dana Bidang Baru', previous: '/pendanaan/bidang'}
    }, {
        path: '/pendanaan/bidang/edit/:id',
        component: viewResolver('Pendanaan/Forms/FormDanaBidang'),
        meta: {title: 'Sunting Dana Bidang', previous: '/pendanaan/bidang'}
    }, {
        path: '/pendanaan/bidang/revisi/:id',
        component: viewResolver('Pendanaan/Forms/FormDanaBidang'),
        meta: {title: 'Revisi Dana Bidang', previous: '/pendanaan/bidang'}
    }, {
        path: '/pendanaan/bidang/history/:id',
        component: viewResolver('Pendanaan/Grids/GridHistoryDanaBidang'),
        meta: {title: 'Riwayat Dana Bidang', previous: '..'}
    }, {
        path: '/pendanaan/bidang/history/verification/:id',
        component: viewResolver('Pendanaan/Grids/GridVerifyDanaBidang'),
        meta: {title: 'Riwayat Verifikasi Dana Bidang', previous: '../..'}
    },
    // PENDANAAN -> DANA PROGRAM
    {
        path: '/pendanaan/program',
        component: viewResolver('Pendanaan/Grids/GridDanaProgram'),
        meta: {title: 'Dana Program'}
    }, {
        path: '/pendanaan/program/create',
        component: viewResolver('Pendanaan/Forms/FormDanaProgram'),
        meta: {title: 'Dana Program Baru', previous: '/pendanaan/program'}
    }, {
        path: '/pendanaan/program/edit/:id',
        component: viewResolver('Pendanaan/Forms/FormDanaProgram'),
        meta: {title: 'Sunting Dana Program', previous: '/pendanaan/program'}
    }, {
        path: '/pendanaan/program/revisi/:id',
        component: viewResolver('Pendanaan/Forms/FormDanaProgram'),
        meta: {title: 'Revisi Dana Program', previous: '/pendanaan/program'}
    }, {
        path: '/pendanaan/program/history/:id',
        component: viewResolver('Pendanaan/Grids/GridHistoryDanaProgram'),
        meta: {title: 'Riwayat Dana Program', previous: '..'}
    }, {
        path: '/pendanaan/program/history/verification/:id',
        component: viewResolver('Pendanaan/Grids/GridVerifyDanaProgram'),
        meta: {title: 'Riwayat Verifikasi Dana Program', previous: '../..'}
    },
    // PENDANAAN -> DANA BATCH
    {
        path: '/pendanaan/batch',
        component: viewResolver('Pendanaan/Grids/GridDanaBatch'),
        meta: {title: 'Dana Batch'}
    }, {
        path: '/pendanaan/batch/create',
        component: viewResolver('Pendanaan/Forms/FormDanaBatch'),
        meta: {title: 'Dana Batch Baru', previous: '/pendanaan/batch'}
    }, {
        path: '/pendanaan/batch/edit/:id',
        component: viewResolver('Pendanaan/Forms/FormDanaBatch'),
        meta: {title: 'Sunting Dana Batch', previous: '/pendanaan/batch'}
    }, {
        path: '/pendanaan/batch/revisi/:id',
        component: viewResolver('Pendanaan/Forms/FormDanaBatchRevisi'),
        meta: {title: 'Revisi Dana Batch', previous: '/pendanaan/batch'}
    }, {
        path: '/pendanaan/batch/revisi/verify/:id',
        component: viewResolver('Pendanaan/Forms/FormDanaBatchVerifyRevisi'),
        meta: {title: 'Verifikasi Revisi Dana Batch', previous: '/pendanaan/batch'}
    }, {
        path: '/pendanaan/batch/history/:id',
        component: viewResolver('Pendanaan/Grids/GridHistoryDanaBatch'),
        meta: {title: 'Riwayat Dana Batch', previous: '..'}
    }, {
        path: '/pendanaan/batch/history/verification/:id',
        component: viewResolver('Pendanaan/Grids/GridVerifyDanaBatch'),
        meta: {title: 'Riwayat Verifikasi Dana Batch', previous: '../..'}
    },

    // SECTION UTK MENU KEGIATAN
    // KEGIATAN -> PENGELOLAAN KEGIATAN
    {
        path: '/kegiatan/pengelolaan',
        component: viewResolver('Kegiatan/Pengelolaan/Grids/GridPengelolaanKegiatan'),
        meta: {title: 'Pengelolaan Kegiatan'}
    }, {
        path: '/kegiatan/pengelolaan/create',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormKegiatan'),
        meta: {title: 'Kegiatan Baru', previous: '/kegiatan/pengelolaan'}
    }, {
        path: '/kegiatan/pengelolaan/edit/:id',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormKegiatan'),
        meta: {title: 'Sunting Kegiatan', previous: '/kegiatan/pengelolaan'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab',
        component: viewResolver('Kegiatan/Pengelolaan/Grids/GridPengelolaanRab'),
        meta: {title: 'Detail Kegiatan dan RAB', previous: '/kegiatan/pengelolaan'}
    }, {
        path: '/kegiatan/pengelolaan/:id/subkegiatan',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormSubKegiatan'),
        meta: {title: 'Subkegiatan Baru', previous: './rab'}
    }, {
        path: '/kegiatan/pengelolaan/:id/subkegiatan/:sid',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormSubKegiatan'),
        meta: {title: 'Sunting Subkegiatan', previous: '../rab'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/create',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormItemRab'),
        meta: {title: 'Entri RAB Baru', previous: '../rab'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/:rabId',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormItemRab'),
        meta: {title: 'Sunting Entri RAB', previous: '../rab'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/realisasi/create',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormItemRabRealisasi'),
        meta: {title: 'Entri Akun Biaya Baru', previous: '..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/realisasi/bulanan',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormItemRealisasiBulanan'),
        meta: {title: 'Entri Realisasi Bulanan', previous: '..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/realisasi/bulanan/:bultah?',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormItemRealisasiBulanan'),
        meta: {title: 'Update Realisasi Bulanan', previous: '../..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/realisasi/bulanan/:bultah?/:rejected?',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormItemRealisasiBulanan'),
        meta: {title: 'Rejected Realisasi Bulanan', previous: '../../..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/:rabId/realisasi',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormRealisasiRab'),
        meta: {title: 'Entri Realisasi RAB', previous: '..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/:rabId/realisasi/history',
        component: viewResolver('Kegiatan/Pengelolaan/Grids/GridRealisasiRab'),
        meta: {title: 'Riwayat Realisasi RAB', previous: '../..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/rab/:periode/realisasi/verify',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormVerifikasiRealisasiBulanan'),
        meta: {title: 'Verifikasi Realisasi RAB', previous: '../..'}
    },{
        path: '/kegiatan/pengelolaan/:id/galeri/create',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormGallery'),
        meta: {title: 'Upload Foto/Video Kegiatan', previous: '../rab'}
    },{
        path: '/kegiatan/pengelolaan/:id/dokumentasi/:sid/update/galeri',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormGallery'),
        meta: {title: 'Upload Foto/Video Kegiatan', previous: '..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/dokumen/create',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormDocuments'),
        meta: {title: 'Upload Dokumen Pendukung', previous: '../rab'}
    }, {
        path: '/kegiatan/pengelolaan/:id/dokumentasi/:sid/update/dosir',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormDocuments'),
        meta: {title: 'Upload Dokumen Pendukung', previous: '..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/dokumentasi/:sid/update/:docId',
        component: viewResolver('Kegiatan/Pengelolaan/Forms/FormEditFiles'),
        meta: {title: 'Edit Data Dokumentasi Kegiatan', previous: '..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/dokumentasi',
        component: viewResolver('Kegiatan/Pengelolaan/Grids/GridsDokumenKegiatan'),
        meta: {title: 'Dokumentasi Kegiatan', previous: '..'}
    }, {
        path: '/kegiatan/pengelolaan/:id/dokumentasi/:sid',
        redirect: '/kegiatan/pengelolaan/:id/dokumentasi/:sid/foto',
        component: viewResolver('Kegiatan/Pengelolaan/Grids/DokumentasiView'),
        children: [{
            path: 'foto',
            component: viewResolver('Kegiatan/Pengelolaan/Grids/GridGallery'),
            meta: {title: 'Dokumentasi Kegiatan', hasPageNav: true, previous: '..'}
        }, {
            path: 'dokumen',
            component: viewResolver('Kegiatan/Pengelolaan/Grids/GridDocuments'),
            meta: {title: 'Dokumentasi Kegiatan', hasPageNav: true, previous: '..'}
        }]
    },
    // KEGIATAN -> PENERIMA MANFAAT -> DATAGRID
    {
        path: '/kegiatan/penerima-manfaat',
        component: viewResolver('Kegiatan/PenerimaManfaat/MainPage'),
        meta: {title: 'Penerima Manfaat'},
        children: [{
            path: ':idprogram/paud',
            redirect: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi',
            component: viewResolver('Kegiatan/PenerimaManfaat/Paud'),
            meta: {title: 'Penerima Manfaat'},
            children: [{
                path: 'institusi',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Paud/GridPaud'),
                meta: {title: 'Penerima Manfaat'}
            }, {
                path: 'guru',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Paud/GridPaudGuru'),
                meta: {title: 'Penerima Manfaat'}
            }, {
                path: 'siswa',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Paud/GridPaudSiswa'),
                meta: {title: 'Penerima Manfaat'}
            }]
        }, {
            path: ':idprogram/pendidikan',
            redirect: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori',
            component: viewResolver('Kegiatan/PenerimaManfaat/PendidikanVokasi'),
            meta: {title: 'Penerima Manfaat'},
            children: [
                {
                path: 'institusi/:kategori',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pendidikan/GridSmkPoltek'),
                meta: {title: 'Penerima Manfaat'}
            }, {
                path: 'manajemen',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pendidikan/GridManajemen'),
                meta: {title: 'Penerima Manfaat'}
            }, {
                path: 'guru',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pendidikan/GridGuru'),
                meta: {title: 'Penerima Manfaat'}
            }, {
                path: 'siswa',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pendidikan/GridSiswa'),
                meta: {title: 'Penerima Manfaat'}
            }]
        }, {
            path: ':idprogram/pelatihan',
            redirect: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi',
            component: viewResolver('Kegiatan/PenerimaManfaat/PelatihanVokasi'),
            meta: {title: 'Penerima Manfaat'},
            children: [{
                path: 'institusi',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pelatihan/GridPesantren'),
                meta: {title: 'Penerima Manfaat'}
            }, {
                path: 'manajemen',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pelatihan/GridManajemen'),
                meta: {title: 'Penerima Manfaat'}
            }, {
                path: 'guru',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pelatihan/GridUstadz'),
                meta: {title: 'Penerima Manfaat'}
            }, {
                path: 'siswa',
                component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pelatihan/GridSantri'),
                meta: {title: 'Penerima Manfaat'}
            }]
        }, {
            path: ':idprogram/beasiswa',
            component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Beasiswa/GridBeasiswa'),
            meta: {title: 'Penerima Manfaat'}
        }, {
            path: ':idprogram/tech-base',
            component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Techbase/GridTechBase'),
            meta: {title: 'Penerima Manfaat'}
        }]
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INSTITUSI PAUD
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Paud/GridSearchPaud'),
        meta: {title: 'PAUD Baru', previous: '../institusi'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormPaud'),
        meta: {title: 'PAUD Baru', previous: '../institusi'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormPaud'),
        meta: {title: 'Sunting PAUD', previous: '../../institusi'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Paud/ViewPaudTabs'),
        redirect: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/view/:id/profil',
        children: [{
            path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/view/:id/profil',
            component: viewResolver('Kegiatan/PenerimaManfaat/Views/Paud/ViewPaudProfil'),
            meta: {title: 'Detail PAUD', previous: '../../../institusi', hasPageNav: true},
        }, {
            path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/view/:id/foto',
            component: viewResolver('Blank'),
            meta: {title: 'Detail PAUD', previous: '../../../institusi', hasPageNav: true},
        }, {
            path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/view/:id/video',
            component: viewResolver('Blank'),
            meta: {title: 'Detail PAUD', previous: '../../../institusi', hasPageNav: true},
        }]
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/view/:uid/profil/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormKpiPaud'),
        meta: {title: 'Entri KPI PAUD', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/institusi/view/:uid/profil/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormKpiPaud'),
        meta: {title: 'Sunting KPI PAUD', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INDIVIDU GURU PAUD
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/guru/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Paud/GridSearchPaudGuru'),
        meta: {title: 'Guru PAUD Baru', previous: '../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/guru/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormPaudGuru'),
        meta: {title: 'Guru PAUD Baru', previous: '../guru'},
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/guru/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormPaudGuru'),
        meta: {title: 'Sunting Guru PAUD', previous: '../../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/guru/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Paud/ViewPaudGuru'),
        meta: {title: 'Detail Guru PAUD', previous: '../../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/guru/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormKpiPaudGuru'),
        meta: {title: 'Entri KPI Guru PAUD', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/guru/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormKpiPaudGuru'),
        meta: {title: 'Sunting KPI Guru PAUD', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INDIVIDU SISWA PAUD
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/siswa/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Paud/GridSearchPaudSiswa'),
        meta: {title: 'Siswa PAUD Baru', previous: '../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/siswa/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormPaudSiswa'),
        meta: {title: 'Siswa PAUD Baru', previous: '../siswa'},
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/siswa/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormPaudSiswa'),
        meta: {title: 'Sunting Siswa PAUD', previous: '../../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/siswa/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Paud/ViewPaudSiswa'),
        meta: {title: 'Detail Siswa PAUD', previous: '../../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/siswa/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormKpiPaudSiswa'),
        meta: {title: 'Entri KPI Siswa PAUD', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/paud/siswa/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Paud/FormKpiPaudSiswa'),
        meta: {title: 'Sunting KPI Siswa PAUD', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> PENDIDIKAN VOKASI SMK & POLTEK
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Pendidikan/GridSearchSmkPoltek')
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormSmkPoltek'),
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormSmkPoltek'),
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pendidikan/ViewSmkPoltekTabs'),
        redirect: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/view/:id/profil',
        children: [{
            path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/view/:id/profil',
            component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pendidikan/ViewSmkPoltek'),
            meta: {previous: '../..', hasPageNav: true}
        }, {
            path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/view/:id/foto',
            component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pendidikan/ViewSmkPoltekFoto'),
            meta: {previous: '../..', hasPageNav: true}
        }, {
            path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/view/:id/video',
            component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pendidikan/ViewSmkPoltekVideo'),
            meta: {previous: '../..', hasPageNav: true}
        }]
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/view/:uid/profil/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/KpiFormSmkPoltek'),
        meta: {title: 'Entri KPI Pendidikan Vokasi', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/view/:uid/profil/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/KpiFormSmkPoltek'),
        meta: {title: 'Sunting KPI Pendidikan Vokasi', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> SMK & Poltek -> Program Keahlian & Studi
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/:idInstitusi/jurusan',
        component: viewResolver('Kegiatan/PenerimaManfaat/Grids/Pendidikan/GridProgramKeahlian'),
        meta: {title: 'Program Keahlian', rawTitle: true, previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/:idInstitusi/jurusan/add',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormProgramKeahlian'),
        meta: {title: 'Program Keahlian Baru', previous: '.'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/:idInstitusi/jurusan/edit/:idJurusan',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormProgramKeahlian'),
        meta: {title: 'Sunting Program Keahlian', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/institusi/:kategori/:idInstitusi/jurusan/view/:idJurusan',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pendidikan/ViewProgramKeahlian'),
        meta: {title: 'Profil Program Keahlian Binaan', previous: '..'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INDIVIDU MANAJEMEN SMK/Poltek
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/manajemen/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Pendidikan/GridSearchManajemen'),
        meta: {title: 'Manajemen Baru', previous: '../manajemen'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/manajemen/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormManajemen'),
        meta: {title: 'Manajemen Baru', previous: '../manajemen'},
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/manajemen/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormManajemen'),
        meta: {title: 'Sunting Manajemen', previous: '../../manajemen'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/manajemen/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pendidikan/ViewManajemen'),
        meta: {title: 'Detail Manajemen', previous: '../../manajemen'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/manajemen/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/KpiFormManajemen'),
        meta: {title: 'Entri KPI Pendidikan Vokasi Manajemen', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/manajemen/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormManajemen'),
        meta: {title: 'Sunting KPI Pendidikan Vokasi Manajemen', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INDIVIDU GURU/DOSEN SMK/Poltek
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/guru/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Pendidikan/GridSearchGuru'),
        meta: {title: 'Guru/Dosen Baru', previous: '../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/guru/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormGuru'),
        meta: {title: 'Guru/Dosen Baru', previous: '../guru'},
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/guru/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormGuru'),
        meta: {title: 'Sunting Guru/Dosen', previous: '../../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/guru/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pendidikan/ViewGuru'),
        meta: {title: 'Detail', previous: '../../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/guru/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/KpiFormGuru'),
        meta: {title: 'Entri KPI Pendidikan Vokasi Guru', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/guru/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/KpiFormGuru'),
        meta: {title: 'Sunting KPI Pendidikan Vokasi Guru', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INDIVIDU SISWA SMK/Poltek
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/siswa/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Pendidikan/GridSearchSiswa'),
        meta: {title: 'Siswa/Mahasiswa Baru', previous: '../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/siswa/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormSiswa'),
        meta: {title: 'Siswa/Mahasiswa Baru', previous: '../siswa'},
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/siswa/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/FormSiswa'),
        meta: {title: 'Sunting Siswa/Mahasiswa', previous: '../../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/siswa/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pendidikan/ViewSiswa'),
        meta: {title: 'Detail', previous: '../../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/siswa/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/KpiFormSiswa'),
        meta: {title: 'Entri KPI Pendidikan Vokasi Siswa', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pendidikan/siswa/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pendidikan/KpiFormSiswa'),
        meta: {title: 'Sunting KPI Pendidikan Vokasi Siswa', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> BEASISWA
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/beasiswa/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Beasiswa/GridSearchBeasiswa'),
        meta: {title: 'Peserta Beasiswa Baru', previous: '../beasiswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/beasiswa/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Beasiswa/MainForm'),
        meta: {title: 'Peserta Beasiswa Baru', previous: '../beasiswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/beasiswa/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Beasiswa/MainForm'),
        meta: {title: 'Sunting Peserta Beasiswa', previous: '../../beasiswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/beasiswa/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Beasiswa/ViewBeasiswa'),
        meta: {title: 'Detail Peserta Beasiswa', previous: '../../beasiswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/beasiswa/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Beasiswa/FormKpi'),
        meta: {title: 'Entri KPI ABFL', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/beasiswa/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Beasiswa/FormKpi'),
        meta: {title: 'Sunting KPI ABFL', previous: '../../'}
    }, {
        path: '/kegiatan/lembarmonitoring/:idprogram/:kind/:id',
        component: viewResolver('Kegiatan/LembarMonitoring/Forms/FormMonitoring'),
        meta: {title: 'Lembar Monitoring', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> TECH BASED EDUCATION
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/tech-base/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Techbase/GridSearchTechBase'),
        meta: {title: 'Peserta TBE Baru', previous: '../tech-base'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/tech-base/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Techbase/MainForm'),
        meta: {title: 'Peserta TBE Baru', previous: '../tech-base'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/tech-base/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Techbase/MainForm'),
        meta: {title: 'Sunting Peserta TBE', previous: '../../tech-base'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/tech-base/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Techbase/ViewTechBase'),
        meta: {title: 'Detail Peserta TBE', previous: '../../tech-base'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/tech-base/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Techbase/FormKpi'),
        meta: {title: 'Entri KPI TBE', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/tech-base/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Techbase/FormKpi'),
        meta: {title: 'Sunting KPI TBE', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INSTITUSI Pesantren/Lembaga Keagamaan
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Pelatihan/GridSearchPesantren'),
        meta: {title: 'Pesantren/Lembaga Keagamaan Baru', previous: '../institusi'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/FormPesantren'),
        meta: {title: 'Pesantren/Lembaga Keagamaan Baru', previous: '../institusi'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/FormPesantren'),
        meta: {title: 'Sunting Pesantren/Lembaga Keagamaan', previous: '../../institusi'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pelatihan/ViewPesantrenTabs'),
        redirect: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/view/:id/profil',
        children: [{
            path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/view/:id/profil',
            component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pelatihan/ViewPesantren'),
            meta: {title: 'Detail', previous: '../../../institusi', hasPageNav: true},
        }, {
            path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/view/:id/foto',
            component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pelatihan/ViewPesantrenFoto'),
            meta: {title: 'Detail', previous: '../../../institusi', hasPageNav: true},
        }, {
            path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/view/:id/video',
            component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pelatihan/ViewPesantrenVideo'),
            meta: {title: 'Detail', previous: '../../../institusi', hasPageNav: true},
        }]
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/view/:uid/profil/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormPesantren'),
        meta: {title: 'Entri KPI Pelatihan Vokasi Pesantren', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/institusi/view/:uid/profil/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormPesantren'),
        meta: {title: 'Sunting KPI Pelatihan Vokasi Pesantren', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INDIVIDU USTADZ/GURU
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/guru/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Pelatihan/GridSearchUstadz'),
        meta: {title: 'Ustadz/Instruktur Baru', previous: '../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/guru/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/FormUstadz'),
        meta: {title: 'Ustadz/Instruktur Baru', previous: '../guru'},
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/guru/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/FormUstadz'),
        meta: {title: 'Sunting Ustadz/Instruktur', previous: '../../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/guru/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pelatihan/ViewUstadz'),
        meta: {title: 'Detail', previous: '../../guru'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/guru/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormUstadz'),
        meta: {title: 'Entri KPI Pelatihan Vokasi Ustadz', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/guru/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormUstadz'),
        meta: {title: 'Sunting KPI Pelatihan Vokasi Ustadz', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INDIVIDU SISWA
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/siswa/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Pelatihan/GridSearchSantri'),
        meta: {title: 'Santri/Siswa Baru', previous: '../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/siswa/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/FormSantri'),
        meta: {title: 'Santri/Siswa Baru', previous: '../siswa'},
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/siswa/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/FormSantri'),
        meta: {title: 'Sunting Santri/Siswa', previous: '../../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/siswa/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pelatihan/ViewSantri'),
        meta: {title: 'Detail', previous: '../../siswa'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/siswa/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormSantri'),
        meta: {title: 'Entri KPI Pelatihan Vokasi Santri', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/siswa/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormSantri'),
        meta: {title: 'Sunting KPI Pelatihan Vokasi Santri', previous: '../../'}
    },
    // KEGIATAN -> PENERIMA MANFAAT -> INDIVIDU MANAJEMEN Pesantren/Lembaga Keagamaan
    {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/manajemen/search',
        component: viewResolver('Kegiatan/PenerimaManfaat/GridsSearch/Pelatihan/GridSearchManajemen'),
        meta: {title: 'Manajemen Baru', previous: '../manajemen'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/manajemen/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/FormManajemen'),
        meta: {title: 'Manajemen Baru', previous: '../manajemen'},
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/manajemen/edit/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/FormManajemen'),
        meta: {title: 'Sunting Manajemen', previous: '../../manajemen'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/manajemen/view/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Views/Pelatihan/ViewManajemen'),
        meta: {title: 'Detail Manajemen', previous: '../../manajemen'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/manajemen/view/:uid/kpi/create',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormManajemen'),
        meta: {title: 'Entri KPI Pelatihan Vokasi Manajemen', previous: '..'}
    }, {
        path: '/kegiatan/penerima-manfaat/:idprogram/pelatihan/manajemen/view/:uid/kpi/update/:id',
        component: viewResolver('Kegiatan/PenerimaManfaat/Forms/Pelatihan/KpiFormManajemen'),
        meta: {title: 'Sunting KPI Pelatihan Vokasi Manajemen', previous: '../../'}
    },

    // KEGIATAN -> CAPAIAN KPI
    {
        path: '/kegiatan/capaian-kpi',
        component: viewResolver('Kegiatan/Kpi/DashboardCapaianKpi'),
        meta: {title: 'Pencapaian KPI'},
    }, {
        path: '/kegiatan/capaian-kpi/:idBatch/:tahun',
        component: viewResolver('Kegiatan/Kpi/FormCapaianKpi'),
        meta: {title: 'Detail Capaian KPI', previous: '/kegiatan/capaian-kpi'},
    }, {
        path: '/kegiatan/capaian-kpi/:idBatch/:tahun/edit',
        component: viewResolver('Kegiatan/Kpi/FormCapaianKpi'),
        meta: {title: 'Perbarui Capaian KPI', previous: '/kegiatan/capaian-kpi'},
    },

    // KEGIATAN => KENDALI PROJECT
    {
        path: '/kegiatan/kendali-proyek',
        component: viewResolver('Kegiatan/KendaliProyek/KendaliProyek'),
        meta: {title: 'Kendali Proyek'},
        redirect: '/kegiatan/kendali-proyek/rencana',
        children: [{
            path: 'rencana',
            component: viewResolver('Kegiatan/KendaliProyek/Rencana'),
            meta: {title: 'Rencana Kendali Kegiatan'},
        }, {
            path: 'realisasi',
            component: viewResolver('Kegiatan/KendaliProyek/Realisasi'),
            meta: {title: 'Realisasi Kendali Kegiatan'},
        }, {
            path: 'pembobotan-bidang',
            component: viewResolver('Kegiatan/KendaliProyek/PembobotanBidang'),
            meta: {title: 'Pembobotan Bidang CSR'},
        }, {
            path: 'pembobotan-program',
            component: viewResolver('Kegiatan/KendaliProyek/PembobotanProgram'),
            meta: {title: 'Pembobotan Program CSR'},
        },
        ]
    },


    // SECTION UTK MENU KEGIATAN
    // KEGIATAN -> PENCAIRAN KEGIATAN
    {
        path: '/kegiatan/pencairan',
        component: viewResolver('Kegiatan/Pencairan/Grids/GridPencairanKegiatan'),
        meta: {title: 'Pencairan Kegiatan'}
    }, {
        path: '/kegiatan/pencairan/edit/:id',
        component: viewResolver('Kegiatan/Pencairan/Forms/FormPencairanKegiatan'),
        meta: {title: 'Sunting Pencairan Kegiatan', previous:'/kegiatan/pencairan'}
    }, {
        path: '/kegiatan/pencairan/view/:id',
        component: viewResolver('Kegiatan/Pencairan/Views/ViewPencairanKegiatan'),
        meta: {title: 'Data Pencairan Kegiatan', previous:'/kegiatan/pencairan'}
    }, {
        path: '/kegiatan/pencairan/upload/:id',
        component: viewResolver('Kegiatan/Pencairan/Forms/FormPencairanKegiatanUpload'),
        meta: {title: 'Berkas Pencairan Kegiatan', previous:'/kegiatan/pencairan'}
    }, {
        path: '/kegiatan/pencairan/voucher/:id',
        component: viewResolver('Kegiatan/Pencairan/Forms/FormVoucher'),
        meta: {title: 'Informasi Transfer Pencairan Kegiatan', previous:'/kegiatan/pencairan'}
    }, {
        path: '/kegiatan/pencairan/rincian/:id',
        component: viewResolver('Kegiatan/Pencairan/Views/ViewPencairanRincianBiaya'),
        meta: {title: 'Rincian Pencairan Kegiatan', previous:'/kegiatan/pencairan'}
    },
    // KEGIATAN -> FINALISASI
    {
        path: '/kegiatan/finalisasi',
        component: viewResolver('Kegiatan/Finalisasi/Grids/GridFinalisasi'),
        meta: {title: 'Finalisasi Kegiatan'}
    }, {
        path: '/kegiatan/finalisasi/create',
        component: viewResolver('Kegiatan/Finalisasi/Forms/FormFinalisasi'),
        meta: {title: 'Buat Finalisasi Kegiatan Baru', previous:'/kegiatan/finalisasi'}
    }, {
        path: '/kegiatan/finalisasi/edit/:id',
        component: viewResolver('Kegiatan/Finalisasi/Forms/FormFinalisasi'),
        meta: {title: 'Sunting Finalisasi Kegiatan', previous:'/kegiatan/finalisasi'}
    }, {
        path: '/kegiatan/finalisasi/view/:id',
        component: viewResolver('Kegiatan/Finalisasi/Views/ViewFinalisasi'),
        meta: {title: 'Data Finalisasi Kegiatan', previous:'/kegiatan/finalisasi'}
    },

    // SECTION UTK MENU LAPORAN
    // LAPORAN -> MONITORING ANGGARAN KEGIATAN
    {
        path: '/laporan/monitoring',
        component: viewResolver('Laporan/MonitoringAnggaran'),
        meta: {title: 'Monitoring Anggaran', previous: '../..'},
        children: [{
            path: ':idprogram/:awal/:akhir',
            component: viewResolver('Laporan/Grids/GridMonitoringAnggaran'),
            meta: {title: 'Monitoring Anggaran'},
        }],
    },
    // LAPORAN -> REKAP ANGGARAN KEGIATAN
    {
        path: '/laporan/rekap',
        component: viewResolver('Laporan/Rekap'),
        meta: {title: 'Rekap Monitoring Anggaran', previous: '../..'},
        children: [{
            path: ':tahun/:bidang',
            component: viewResolver('Laporan/Grids/GridRekap'),
            meta: {title: 'Rekap Monitoring Anggaran'},
        }],
    },
    // LAPORAN -> RIWAYAT PEMBAYARAN
    {
        path: '/laporan/pembayaran',
        component: viewResolver('Laporan/RiwayatPembayaran'),
        meta: {title: 'Riwayat Pembayaran', previous: '../..'},
        children: [{
            path: ':idprogram/:tahun',
            component: viewResolver('Laporan/Grids/GridRiwayatPembayaran'),
            meta: {title: 'RiwayatPembayaran'},
        }],
    },
    // SECTION UTK MENU MASTER DATA
    // MASTER DATA -> WILAYAH
    {
        path: '/master-data/wilayah',
        redirect: '/master-data/wilayah/provinsi',
        component: viewResolver('MasterData/Wilayah/DataWilayah'),
        children: [{
            path: 'provinsi',
            component: viewResolver('MasterData/Wilayah/Grids/GridProvinsi'),
            meta: {title: 'Master Data Wilayah', hasPageNav: true}
        }, {
            path: 'kabupaten',
            component: viewResolver('MasterData/Wilayah/Grids/GridKabupaten'),
            meta: {title: 'Master Data Wilayah', hasPageNav: true}
        }, {
            path: 'kecamatan',
            component: viewResolver('MasterData/Wilayah/Grids/GridKecamatan'),
            meta: {title: 'Master Data Wilayah', hasPageNav: true}
        }, {
            path: 'kelurahan',
            component: viewResolver('MasterData/Wilayah/Grids/GridKelurahan'),
            meta: {title: 'Master Data Wilayah', hasPageNav: true}
        }]
    }, {
        path: '/master-data/wilayah/provinsi/create',
        component: viewResolver('MasterData/Wilayah/Forms/FormProvinsi'),
        meta: {title: 'Provinsi Baru', previous: '/master-data/wilayah/provinsi'}
    }, {
        path: '/master-data/wilayah/provinsi/edit/:id',
        component: viewResolver('MasterData/Wilayah/Forms/FormProvinsi'),
        meta: {title: 'Sunting Provinsi', previous: '/master-data/wilayah/provinsi'}
    }, {
        path: '/master-data/wilayah/kabupaten/create',
        component: viewResolver('MasterData/Wilayah/Forms/FormKabupaten'),
        meta: {title: 'Kabupaten Baru', previous: '/master-data/wilayah/kabupaten'}
    }, {
        path: '/master-data/wilayah/kabupaten/edit/:id',
        component: viewResolver('MasterData/Wilayah/Forms/FormKabupaten'),
        meta: {title: 'Sunting Kabupaten', previous: '/master-data/wilayah/kabupaten'}
    }, {
        path: '/master-data/wilayah/kecamatan/create',
        component: viewResolver('MasterData/Wilayah/Forms/FormKecamatan'),
        meta: {title: 'Kecamatan Baru', previous: '/master-data/wilayah/kecamatan'}
    }, {
        path: '/master-data/wilayah/kecamatan/edit/:id',
        component: viewResolver('MasterData/Wilayah/Forms/FormKecamatan'),
        meta: {title: 'Sunting Kecamatan', previous: '/master-data/wilayah/kecamatan'}
    }, {
        path: '/master-data/wilayah/kelurahan/create',
        component: viewResolver('MasterData/Wilayah/Forms/FormKelurahan'),
        meta: {title: 'Kelurahan Baru', previous: '/master-data/wilayah/kelurahan'}
    }, {
        path: '/master-data/wilayah/kelurahan/edit/:id',
        component: viewResolver('MasterData/Wilayah/Forms/FormKelurahan'),
        meta: {title: 'Sunting Kelurahan', previous: '/master-data/wilayah/kelurahan'}
    },
    // MASTER DATA -> HIRARKI PROGRAM
    {
        path: '/master-data/program',
        redirect: '/master-data/program/bidang-csr',
        component: viewResolver('MasterData/Program/DataProgramCSR'),
        children: [{
            path: 'bidang-csr',
            component: viewResolver('MasterData/Program/Grids/GridJenisProgram'),
            meta: {title: 'Hierarki Program', hasPageNav: true}
        }, {
            path: 'program-csr',
            component: viewResolver('MasterData/Program/Grids/GridProgram'),
            meta: {title: 'Hierarki Program', hasPageNav: true}
        }]
    }, {
        path: '/master-data/program/bidang-csr/create',
        component: viewResolver('MasterData/Program/Forms/FormJenisProgram'),
        meta: {title: 'Bidang CSR Baru', previous: '/master-data/program/bidang-csr'}
    }, {
        path: '/master-data/program/bidang-csr/edit/:id',
        component: viewResolver('MasterData/Program/Forms/FormJenisProgram'),
        meta: {title: 'Sunting Bidang CSR', previous: '/master-data/program/bidang-csr'}
    }, {
        path: '/master-data/program/program-csr/create',
        component: viewResolver('MasterData/Program/Forms/FormProgram'),
        meta: {title: 'Program CSR Baru', previous: '/master-data/program/program-csr'}
    }, {
        path: '/master-data/program/program-csr/edit/:id',
        component: viewResolver('MasterData/Program/Forms/FormProgram'),
        meta: {title: 'Sunting Program CSR', previous: '/master-data/program/program-csr'}
    },
    // MASTER DATA -> BATCH PEMBINAAN
    {
        path: '/master-data/batch',
        component: viewResolver('MasterData/Batch/GridBatch'),
        meta: {title: 'Batch Pembinaan'}
    }, {
        path: '/master-data/batch/create',
        component: viewResolver('MasterData/Batch/FormBatch'),
        meta: {title: 'Batch Baru', previous: '/master-data/batch'}
    }, {
        path: '/master-data/batch/edit/:id',
        component: viewResolver('MasterData/Batch/FormBatch'),
        meta: {title: 'Sunting Batch', previous: '/master-data/batch'}
    },
    // MASTER DATA -> TARGET KPI
    {
        path: '/master-data/target-kpi',
        component: viewResolver('MasterData/Kpi/TargetKpi'),
        meta: {title: 'Target KPI'}
    },
    //Data Source KPI
    {
        path: '/master-data/konfigurasi-kpi/data-source',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Grids/GridDataSource'),
        meta: {title: 'Konfigurasi Data Source KPI'}
    },
    {
        path: '/master-data/konfigurasi-kpi/data-source/create',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormDataSource'),
        meta: {title: 'Tambah Konfigurasi Data Source KPI', previous: '.'}
    },
    {
        path: '/master-data/konfigurasi-kpi/data-source/edit/:id',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormDataSource'),
        meta: {title: 'Sunting Konfigurasi Data Source KPI', previous: '..'}
    },
    //Chart KPI
    {
        path: '/master-data/konfigurasi-kpi/chart',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Grids/GridChart'),
        meta: {title: 'Konfigurasi Chart KPI'}
    },
    {
        path: '/master-data/konfigurasi-kpi/chart/create',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormChart'),
        meta: {title: 'Tambah Konfigurasi Chart KPI', previous: '.'}
    },
    {
        path: '/master-data/konfigurasi-kpi/chart/edit/:id',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormChart'),
        meta: {title: 'Sunting Konfigurasi Chart KPI', previous: '..'}
    },
    //Report KPI
    {
        path: '/master-data/konfigurasi-kpi/report',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Grids/GridReport'),
        meta: {title: 'Konfigurasi Report KPI'}
    },
    {
        path: '/master-data/konfigurasi-kpi/report/create',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormReport'),
        meta: {title: 'Tambah Konfigurasi Report KPI', previous: '.'}
    },
    {
        path: '/master-data/konfigurasi-kpi/report/edit/:id',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormReport'),
        meta: {title: 'Sunting Konfigurasi Report KPI', previous: '..'}
    },
    //Layout Dashboard KPI
    {
        path: '/master-data/konfigurasi-kpi/layout',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Grids/GridLayout'),
        meta: {title: 'Konfigurasi Layout Dashboard KPI'}
    },
    {
        path: '/master-data/konfigurasi-kpi/layout/create',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormLayout'),
        meta: {title: 'Tambah Konfigurasi Layout Dashboard KPI', previous: '.'}
    },
    {
        path: '/master-data/konfigurasi-kpi/layout/edit/:id',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormLayout'),
        meta: {title: 'Sunting Konfigurasi Layout Dashboard KPI', previous: '..'}
    },
    //Dashboard KPI
    {
        path: '/master-data/konfigurasi-kpi/dashboard',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Grids/GridDashboard'),
        meta: {title: 'Konfigurasi Dashboard KPI'}
    },
    {
        path: '/master-data/konfigurasi-kpi/dashboard/create',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormDashboard'),
        meta: {title: 'Tambah Konfigurasi Dashboard KPI', previous: '.'}
    },
    {
        path: '/master-data/konfigurasi-kpi/dashboard/edit/:id',
        component: viewResolver('MasterData/Kpi/Konfigurasi/Forms/FormDashboard'),
        meta: {title: 'Sunting Konfigurasi Dashboard KPI', previous: '..'}
    },
    // MASTER DATA -> MITRA
    {
        path: '/master-data/mitra',
        component: viewResolver('MasterData/Mitra/Grids/GridMitra'),
        meta: {title: 'Mitra'}
    }, {
        path: '/master-data/mitra/create',
        component: viewResolver('MasterData/Mitra/Forms/FormMitra'),
        meta: {title: 'Mitra Baru', previous: '/master-data/mitra'}
    }, {
        path: '/master-data/mitra/edit/:id',
        component: viewResolver('MasterData/Mitra/Forms/FormMitra'),
        meta: {title: 'Sunting Profil Mitra', previous: '/master-data/mitra'}
    }, {
        path: '/master-data/mitra/view/:id',
        component: viewResolver('MasterData/Mitra/Views/ViewMitraProfile'),
        meta: {title: 'Detail Profil Mitra', previous: '/master-data/mitra'}
    },
    // MASTER DATA -> DATA REFERENCE
    {
        path: '/master-data/data-reference',
        component: viewResolver('MasterData/DataReference/Grids/GridDataReference'),
        meta: {title: 'Data Referensi'}
    }, {
        path: '/master-data/data-reference/create',
        component: viewResolver('MasterData/DataReference/Forms/FormDataReference'),
        meta: {title: 'Data Referensi Baru', previous: '/master-data/data-reference'}
    }, {
        path: '/master-data/data-reference/edit/:id',
        component: viewResolver('MasterData/DataReference/Forms/FormDataReference'),
        meta: {title: 'Sunting Data Referensi', previous: '/master-data/data-reference'}
    }, {
        path: '/master-data/data-reference/view/:id',
        component: viewResolver('MasterData/DataReference/Views/ViewDataReference'),
        meta: {title: 'Detail Data Referensi', previous: '/master-data/data-reference'}
    },

    // SECTION UTK MENU SETTING
    // SETTING -> KELOLA PENGGUNA
    {
        path: '/security/members',
        redirect: '/security/members/user',
        component: viewResolver('Security/Members'),
        children: [{
            path: 'user',
            component: viewResolver('Security/Grids/GridUserAccount'),
            meta: {title: 'Kelola Pengguna', hasPageNav: true}
        }, {
            path: 'group',
            component: viewResolver('Security/Grids/GridUserGroup'),
            meta: {title: 'Kelola Pengguna', hasPageNav: true}
        }]
    }, {
        path: '/security/members/user/create',
        component: viewResolver('Security/Forms/FormUserAccount'),
        meta: {title: 'Pengguna Baru', previous: '/security/members/user'}
    }, {
        path: '/security/members/user/edit/:id',
        component: viewResolver('Security/Forms/FormUserAccount'),
        meta: {title: 'Sunting Pengguna', previous: '/security/members/user'}
    }, {
        path: '/security/members/group/create',
        component: viewResolver('Security/Forms/FormUserGroup'),
        meta: {title: 'Grup Pengguna Baru', previous: '/security/members/group'}
    }, {
        path: '/security/members/group/edit/:id',
        component: viewResolver('Security/Forms/FormUserGroup'),
        meta: {title: 'Sunting Grup Pengguna', previous: '/security/members/group'}
    },
    // SETTING -> HAK AKSES
    {
        path: '/security/access-rights',
        component: viewResolver('Security/ManageAccessRights'),
        meta: {title: 'Kelola Hak Akses'}
    },
    // SETTING -> APP SETTING
    {
        path: '/security/appconfig',
        component: viewResolver('Security/Grids/GridAppSetting'),
        meta: {title: 'Konfigurasi'}
    }, {
        path: '/security/appconfig/:section/:attr',
        component: viewResolver('Security/Forms/FormAppSetting'),
        meta: {title: 'Sunting Konfigurasi', previous: '/security/appconfig'}
    },
    // SETTING -> AUDIT TRAIL
    {
        path: '/security/auditlog',
        component: viewResolver('Security/Grids/GridAuditLog'),
        meta: {title: 'Audit Log'}
    },
    // USER PROFILE
    {
        path: '/user/profile',
        component: viewResolver('User/Forms/FormUserProfile'),
        meta: {title: 'Profil Saya'}
    },
    // =============================================
    // ADARO-AEP V2
    // =============================================
    // PETA SEBARAN
    {
        path: '/peta/sebaran/penerima-manfaat',
        component: viewResolver('Peta/Sebaran/PenerimaManfaat.vue'),
        meta: {title: 'Peta Sebaran Penerima Manfaat'}
    },
    // DIAGRAM KURVA-S
    {
        path: '/diagram/progress',
        component: viewResolver('Diagram/Progress/KurvaS.vue'),
        meta: {title: 'Diagram Akumulasi Progres Kegiatan'}
    },
    {
        path: '/diagram/progress/:id',
        component: viewResolver('Diagram/Progress/KurvaS.vue'),
        meta: {title: 'Diagram Progres Kegiatan Program CSR'}
    },
    {
        path: '/diagram/rekap',
        component: viewResolver('Diagram/Progress/Rekap.vue'),
        meta: {title: 'Diagram Overview Progress Kegiatan'}
    },
    // SECTION UTK MENU FUNGSI PENUNJANG
    // GALLERY
    {
        path: '/dokumentasi/gallery',
        component: viewResolver('Dokumentasi/Gallery/AlbumKegiatan'),
        meta: {title: 'Galeri Kegiatan'}
    }, {
        path: '/dokumentasi/gallery/:id',
        component: viewResolver('Dokumentasi/Gallery/AlbumSubKegiatan'),
        meta: {title: 'Dokumentasi Galeri Kegiatan', previous: '.'}
    }, {
        path: '/dokumentasi/gallery/:id/:sid',
        component: viewResolver('Dokumentasi/Gallery/AlbumSubKegiatanAll'),
        meta: {title: 'Dokumentasi Galeri Sub Kegiatan', previous: '.'}
    }, {
        path: '/dokumentasi/dosir',
        component: viewResolver('Dokumentasi/Dosir/DosirKegiatan'),
        meta: {title: 'Dosir Kegiatan'}
    }, {
        path: '/dokumentasi/dosir/:id',
        component: viewResolver('Dokumentasi/Dosir/DosirSubKegiatan'),
        meta: {title: 'Dokumentasi Dosir Kegiatan', previous: '.'}
    }, {
        path: '/dokumentasi/dosir/:id/:sid',
        component: viewResolver('Dokumentasi/Dosir/DosirSubKegiatanAll'),
        meta: {title: 'Dokumentasi Dosir Sub Kegiatan', previous: '.'}
    },
    // SECTION UTK MENU HUMAS
    // MEDIA
    {
        path: '/publikasi/media',
        component: viewResolver('Humas/Media/GridMedia'),
        meta: {title: 'Dampak Media'}
    }, {
        path: '/publikasi/media/create',
        component: viewResolver('Humas/Media/FormMedia'),
        meta: {title: 'Dampak Media Baru', previous: '.'}
    }, {
        path: '/publikasi/media/edit/:id',
        component: viewResolver('Humas/Media/FormMedia'),
        meta: {title: 'Sunting Dampak Media', previous: '..'}
    }, {
        path: '/publikasi/media/view/:id',
        component: viewResolver('Humas/ViewContent'),
        meta: {title: 'Dampak Media', previous: '..'}
    },
    // CSR
    {
        path: '/publikasi/csr',
        component: viewResolver('Humas/Csr/GridCsr'),
        meta: {title: 'Publikasi CSR'}
    }, {
        path: '/publikasi/csr/create',
        component: viewResolver('Humas/Csr/FormCsr'),
        meta: {title: 'Publikasi CSR Baru', previous: '.'}
    }, {
        path: '/publikasi/csr/edit/:id',
        component: viewResolver('Humas/Csr/FormCsr'),
        meta: {title: 'Sunting Publikasi CSR', previous: '..'}
    }, {
        path: '/publikasi/csr/view/:id',
        component: viewResolver('Humas/ViewContent'),
        meta: {title: 'Publikasi CSR', previous: '..'}
    },

    // CATCH EVERYTHING HERE
    {
        path: '/access-denied',
        component: viewResolver('AccessDenied'),
        meta: {title: ''}
    }, {
        path: '/not-found',
        component: viewResolver('NotFound'),
        meta: {title: 'Page Not Found'}
    }, {
        path: '*',
        component: viewResolver('NotFound'),
        meta: {title: 'Page Not Found'}
    }
];

export default routes;
