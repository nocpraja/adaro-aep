<template>
  <bs-card shadow v-if="mode !='none'">
    <bs-card-header tag="h6" class="font-weight-light bg-white">
      <div style="height:2em">{{ data.program.name }}</div>
    </bs-card-header>
    <bs-card-body class="table-responsive-lg">
      <kpi-display-kuantitas :kpi-kuantitas="getCapaianKuantitas"
                             kpi-title="PESANTREN & LEMBAGA KEAGAMAAN"
                             :mode="mode"
      />
      <kpi-chart-kualitas :color-data="data.colorData"
                          :data-options="getCapaianKualitas"
                          append-sign="%"
                          kpi-prefix="pelatihan"
                          :only-show="getOnlyShow"
                          @kpiPopup="doPopup" />
      <kpi-executive-summary :narasi="data.executiveSummary"/>
    </bs-card-body>
    <bs-card-footer class="text-right">
      <bs-button size="sm"
                 color="teal"
                 outlined
                 @click="doMaximize"
                 v-if="mode === 'normal'">
        Selengkapnya
      </bs-button>
      <bs-button size="sm"
                 color="blue"
                 outlined
                 @click="doRestore"
                 v-if="mode === 'full'">
        Kembali
      </bs-button>
    </bs-card-footer>
  </bs-card>
</template>

<script>
import KpiDisplayKuantitas from './Parts/KpiDisplayKuantitas';
import KpiChartKualitas from './Parts/KpiChartKualitas';
import KpiExecutiveSummary from './Parts/KpiExecutiveSummary';

export default {
    name: "DashboardKpiPelatihanVokasi",
    components: {KpiDisplayKuantitas, KpiChartKualitas, KpiExecutiveSummary},
    props: {
        data: {
            type: Object,
            default: undefined,
            required: true
        },
        mode: {
            type: String,
            default: 'normal'
        }
    },
    computed: {
        getCapaianKuantitas() {
            if (this.$props.data['overviewBatches']) {
                let batches = this.$props.data['overviewBatches'];
                let selectedBatch = null;
                for (let batchId in batches) {
                    if (batches.hasOwnProperty(batchId)) {
                        let batch = batches[batchId];
                        if (batch.hasOwnProperty('capaianKpiKuantitas')) {
                            if (batch['periode'] != 0) {  // pilih batch yang sudah terisi
                                selectedBatch = batch;
                            }
                        }
                    }

                }
                if (selectedBatch) {
                    return selectedBatch['capaianKpiKuantitas'];
                }
            }
            return [];
        },
        getCapaianKualitas() {
            if (this.$props.data['overviewBatches']) {
                return this.$props.data['overviewBatches'];
            }
            return null;
        },
        getOnlyShow() {
            if (this.$props.mode === "full") {
                return '';
            }
            let batches = this.$props.data['overviewBatches'];
            let selectedBatchId = null;
            let lastBatchId = 0;
            for (let batchId in batches){
                if (batches.hasOwnProperty(batchId)) {
                    let batch = batches[batchId];
                    if (batch.hasOwnProperty('capaianKpiKualitas')) {
                        if (batch['periode'] !== 0) {  // pilih batch yang sudah terisi
                            selectedBatchId = batchId;
                        }
                    }
                    lastBatchId = batchId;
                }
            }
            if (selectedBatchId) {
                return selectedBatchId;
            }
            return lastBatchId;
        }
    },
    methods: {
        /**
         * Fire event <tt>maximize</tt> .
         * @return {void}
         */
        doMaximize() {
            this.$emit('maximize', this.$props.data.program.msid);
        },
        /**
         * Fire event <tt>restore</tt> .
         * @return {void}
         */
        doRestore() {
            this.$emit('restore');
        },
        /**
         * Fire event <tt>open-kpi-popup</tt> .
         * @return {void}
         */
        doPopup(batchId, tahun) {
            this.$emit('open-kpi-popup', batchId, tahun);
        },
    }
}
</script>

<style scoped>

</style>
