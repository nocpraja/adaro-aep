import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueBootstrap from 'vue-bs4';
import VueSwal  from "vue-swal";
import AppNavbar from '../components/AppNavbar';
import router from '../router';
import store from '../store';
import '../../scss/AdminPage.scss';

import Highcharts from 'highcharts';
import highchartsMore from 'highcharts/highcharts-more';
import solidGauge from 'highcharts/modules/solid-gauge';

Vue.use(VueBootstrap);
Vue.use(VueSwal);
library.add(faCheckCircle);

highchartsMore(Highcharts);
solidGauge(Highcharts);

new Vue({
    el: '#app',
    store,
    router,
    components: {FontAwesomeIcon, AppNavbar},
    data: () => ({
        miniSidebar: false,
        openSidebar: true
    }),
    methods: {
        toggleMini() {
            this.miniSidebar = !this.miniSidebar;
        },
        toggleOpenSidebar(value) {
            this.miniSidebar = false;
            this.openSidebar = value;
        },
        toggleSidebar(value) {
            if (value === 'mini') {
                this.miniSidebar = true;
            } else if (value === 'expand') {
                this.miniSidebar = false;
            } else if (value === 'close') {
                this.miniSidebar = false;
                this.openSidebar = false;
            } else if (value === 'open') {
                this.miniSidebar = false;
                this.openSidebar = true;
            }
        }
    }
});
