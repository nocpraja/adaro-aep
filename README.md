Web Aplikasi Adaro Education Program
=======================================

### Requirement
1. PostgreSQL 11.3.x (latest version) ataupun 10.8.x (latest version)
2. Postgis, Spatial and Geographic objects for PostgreSQL
3. PHP versi 7.3.5 (latest version)
4. PHP extensions: php-pdo, php-pgsql, php-intl, php-mbstring, php-gd, php-imagick, php-tidy,
   php-curl, php-sodium, php-apcu (5.1.x or above)
5. Apache 2.4.x, sebaiknya dijalankan dalam mode `mpm-worker` ataupun `mpm-event`

### Instalasi
Diasumsikan PostgreSQL, Apache dan PHP sudah terinstalasi dengan baik dan sempurna.
Jika belum, baca panduan pada website berikut ini: 

- [PostgreSQL Database](http://www.postgresql.net)
- [Postgis](https://postgis.net/)
- [Apache HTTP Server](http://httpd.apache.org)
- [PHP Manual](http://www.php.net/manual)

#### Prosedure Instalasi
1. Download dan install `NodeJs`, see [Node JS website](https://nodejs.org/en/) untuk petunjuk instalasi.
2. Download dan install `Yarn`, see [Yarn website](https://yarnpkg.com/en/) untuk petunjuk instalasi.
3. Download dan install `composer`, see [composer website](http://www.getcomposer.org) untuk petunjuk instalasi.
4. Install ataupun copy project source code pada webroot direktori.

Setelah semua prosedur diatas berjalan sempurna, kemudian jalankan sintaks berikut dari terminal dalam project direktori:

**On Windows**
```shell
# install application dependencies
  [PHP-INSTALL-DIR]/php.exe [COMPOSER-INSTALL-DIR]/composer.phar install   
```

**On Linux/Mac**
```shell
# install application dependencies
  [COMPOSER-INSTALL-DIR]/composer install
```

#### Pengaturan Konfigurasi
Kemudian buka kembali PHP editor/studio dan lakukan langkah berikut:

1. Create file `.env.local` pada project direktori.
2. Copy baris `23` dari file `.env` ke dalam file `.env.local` dan lakukan penyesuaian konfigurasi.
3. Create database pada postgres dengan nama database dan owner database sesuai dengan konfigurasi pada file `.env.local`.
3. Execute query: `CREATE EXTENSION postgis;` pada database yang baru dibuat.
4. Atur parameter aplikasi pada: `config/services.yaml` pada section `parameters`, attribute `asset_url_prefix`.
   Sesuaikan dengan url instalasi aplikasi jika diakses melalui web browser.
5. Atur agar **http-server** memiliki hak `read` dan `write` terhadap direktori: `var/cache`, `var/log` 
   dan `public/uploads`.
6. Atur konfigurasi `webpack` pada file `webpack.config.js`, lihat pada baris: `5, 7, 8`.
7. Kemudian dari terminal dalam project direktori jalankan sintaks berikut:

**On Windows**
```shell
# create database struktur dan data awal
  [PHP-INSTALL-DIR]/php.exe [PROJECT-DIR]/bin/console doctrine:migrations:migrate

# install javascript dependencies
  yarn install
  cd assets\package
  yarn link
  cd ..\..
  yarn link vue-bs4
   
# build and compile javascript 
  # (development version)
  yarn run encore dev
  # atau (production version)
  yarn run encore production
   
# remove development cache files
  rmdir var\cache\dev /s
  del var\logs\dev.log
```

**On Linux/Mac**
```shell
# create database struktur (on Linux/Mac)
  [PROJECT-DIR]/bin/console doctrine:migrations:migrate
  
# install javascript dependencies
  yarn install
  cd assets/package
  yarn link
  cd ../..
  yarn link vue-bs4
  
# build and compile javascript
  # (development version)
  yarn run encore dev
  # atau (production version)
  yarn run encore production
   
# remove development cache files
  rm -rfv var/cache/dev
  rm -rfv var/log/dev.log
```

### Catatan Untuk Beberapa Direktori

#### Direktori: assets\js\views

Direktori ini dipergunakan untuk menempatkan template halaman web aplikasi. Konsepnya
hampir sama dengan view template pada PHP-MVC, seperti *Laravel* dan *Symfony*.

Direktori sebaiknya disusun dalam beberapa sub-direktori dan disesuaikan dengan topik bisnis process,
ataupun menu utama.

Contoh struktur direktori:
```text
-- views
   +--- Dashboard
   +--- Kegiatan
        |--- Forms (view template untuk Forms)
        |--- Grids (view template untuk DataGrid)
   +--- MasterData
        |--- Forms (view template untuk Forms)
        |--- Grids (view template untuk DataGrid)
   +--- ModuleName
        |--- Forms (view template untuk Forms)
        |--- Grids (view template untuk DataGrid)
```

#### Direktori: assets\js\models

Direktori ini dipergunakan untuk DataModel yang merefleksikan struktur tabel pada database.

Direktori sebaiknya disusun dalam beberapa sub-direktori dan disesuaikan dengan topik bisnis process,
dan sedapat mungkin konsisten dengan struktur folder pada: `src\Entity` dan `src\Repository`.

Contoh struktur direktori:
```text
-- models
   |--- Kegiatan
   |--- MasterData
   |--- ModuleName
```

#### Direktori: assets\js\store\modules

Direktori ini dipergunakan untuk menempatkan DataStore yang bersifat *singleton* dan *reusable*.
Artinya DataStore yang dibuat akan selalu reside pada memory dan tidak pernah di-dispose sepanjang
aplikasi berjalan. DataStore tersebut dapat direusable dengan mudah oleh view templates yang ada.

Direktori sebaiknya disusun dalam beberapa sub-direktori dan disesuaikan dengan topik bisnis process.

Contoh struktur direktori:
```text
-- modules
   |--- Dashboard
   |--- Kegiatan
   |--- ModuleName
```

#### Direktori: src\Entity

Direktori ini dipergunakan untuk Entity object yang merefleksikan struktur tabel pada database.

Direktori sebaiknya disusun dalam beberapa sub-direktori dan disesuaikan dengan topik bisnis process,
dan sedapat mungkin konsisten dengan struktur folder pada: `assets\js\models` dan `src\Repository`.

Contoh struktur direktori:
```text
-- Entity
   |--- Security
   |--- Kegiatan
   |--- MasterData
   |--- ModuleName
```

#### Direktori: src\Models

Direktori ini dipergunakan untuk script PHP yang berhubungan dengan bisnis process.

Semua script PHP yang berkaitan erat dengan bisnis process ditempatkan dalam direktori ini.
Dan sebaiknya direktori disusun dalam beberapa sub-direktori dan disesuaikan dengan topik 
bisnis processnya.

Sedangkan pada controller tidak diperkenankan melakukan bisnis process. Melainkan hanya bertindak sebagai
gateway ataupun intermediate yang memanggil fungsi pada `EntityRepository` classes ataupun `Model` classes.

Contoh struktur direktori:
```text
-- Models
   |--- Kegiatan
   |--- MasterData
   |--- ModuleName
```

#### Direktori: src\Controller

Direktori ini dipergunakan untuk script PHP **Application Controller**. Baik itu untuk *REST-API Controller* 
ataupun *Normal Controller*. 

Direktori sebaiknya disusun dalam beberapa sub-direktori dan disesuaikan dengan topik bisnis processnya.

Pada controller tidak diperkenankan melakukan bisnis process. Melainkan hanya bertindak sebagai
gateway ataupun intermediate yang memanggil fungsi pada `EntityRepository` classes ataupun `Model` classes.

Contoh struktur direktori:
```text
-- Controller
   |--- Dashboard
   |--- Kegiatan
   |--- MasterData
   |--- ModuleName
```

#### Direktori: src\Service

Direktori ini dipergunakan untuk script PHP yang berhubungan dengan *Service/Dependency Injection*.

Direktori sebaiknya disusun dalam beberapa sub-direktori dan disesuaikan dengan topik 
task process yang dilakukan oleh service tersebut.

Contoh struktur direktori:
```text
-- Service
   |--- Security
   |--- Uploadable
   |--- Notification
```
