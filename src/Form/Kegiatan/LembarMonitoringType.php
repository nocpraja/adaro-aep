<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Kegiatan;

use App\Entity\Kegiatan\LembarMonitoring;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class ProvinsiType
 *
 * @package App\Form\Kegiatan
 * @author  Mark Melvin
 * @since   06/09/2018, modified: 06/09/2018 10:13
 */
class LembarMonitoringType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', TextType::class)
            ->add('beneficiaryId', IntegerType::class)
            ->add('tanggalInput', DateType::class, [
                'widget' => 'single_text',
                'input' => 'string'
            ])
            ->add('namaFile', TextType::class)
            ->add('evalScore', NumberType::class);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LembarMonitoring::class,
        ]);
    }
}
