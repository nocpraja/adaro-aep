<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Kegiatan;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Kegiatan\Kegiatan;
use App\Repository\MasterData\BatchCsrRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class KegiatanType
 *
 * @package App\Form\Kegiatan
 * @author  Ahmad Fajar
 * @since   15/11/2018, modified: 15/05/2019 7:56
 */
class KegiatanType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('namaKegiatan', TextType::class)
                ->add('danaBatch', IntegerType::class)
                ->add('periodeBatch', IntegerType::class);

        /** @var DanaBatchRepository $repo1 */
        $repo1 = $options['program_repo'];
        $builder->get('danaBatch')->addModelTransformer(new IdToEntityTransformer($repo1, 'id', false));

        /** @var BatchCsrRepository $repo2 */
        $repo2 = $options['batch_repo'];
        $builder->get('periodeBatch')->addModelTransformer(new IdToEntityTransformer($repo2, 'batchId', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Kegiatan::class])
                 ->setRequired('batch_repo')
                 ->setRequired('program_repo')
                 ->setAllowedTypes('batch_repo', 'Doctrine\Common\Persistence\ObjectRepository')
                 ->setAllowedTypes('program_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}