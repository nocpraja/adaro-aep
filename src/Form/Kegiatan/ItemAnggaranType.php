<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Kegiatan;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Kegiatan\ItemAnggaran;
use App\Entity\Kegiatan\ItemRealisasiRab;
use App\Repository\Kegiatan\SubkegiatanRepository;
use App\Repository\MasterData\DataReferenceRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ItemAnggaranType
 *
 * @package App\Form\Kegiatan
 * @author  Ahmad Fajar
 * @since   18/11/2018, modified: 15/05/2019 7:57
 */
class ItemAnggaranType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('title', TextType::class)
                ->add('remark', TextType::class)
                ->add('unitQty1', TextType::class)
                ->add('unitQty2', TextType::class)
                ->add('unitFrequency', TextType::class)
                ->add('unitPrice', NumberType::class)
                ->add('jumlahQty1', NumberType::class)
                ->add('jumlahQty2', NumberType::class)
                ->add('frequency', NumberType::class)
                ->add('fixedCost')
                ->add('account', IntegerType::class)
                ->add('parent', IntegerType::class)
                ->add('realisasiRabs', CollectionType::class);

        /** @var SubkegiatanRepository $repo1 */
        $repo1 = $options['subkegiatan_repo'];
        $builder->get('parent')->addModelTransformer(new IdToEntityTransformer($repo1, 'sid', false));

        /** @var DataReferenceRepository $repo2 */
        $repo2 = $options['dataref_repo'];
        $builder->get('account')->addModelTransformer(new IdToEntityTransformer($repo2, 'id', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => ItemAnggaran::class])
                 ->setRequired('subkegiatan_repo')
                 ->setRequired('dataref_repo')
                 ->setAllowedTypes('subkegiatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
                 ->setAllowedTypes('dataref_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}
