<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Kegiatan;


use App\Component\Form\DataTransformer\IdsToEntitiesTransformer;
use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Kegiatan\Subkegiatan;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\MasterData\KabupatenRepository;
use App\Repository\MasterData\ProvinsiRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubKegiatanType
 *
 * @package App\Form\Kegiatan
 * @author  Ahmad Fajar
 * @since   17/11/2018, modified: 15/05/2019 7:56
 */
class SubkegiatanType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('title', TextType::class)
                ->add('tanggalMulai', DateType::class, [
                    'widget' => 'single_text',
                    'format' => DateType::HTML5_FORMAT
                ])
                ->add('tanggalAkhir', DateType::class, [
                    'widget' => 'single_text',
                    'format' => DateType::HTML5_FORMAT
                ])
                ->add('kegiatan', IntegerType::class)
                ->add('provincies', CollectionType::class, [
                    'entry_type'   => IntegerType::class,
                    'allow_add'    => true,
                    'allow_delete' => true
                ])
                ->add('kabupatens', CollectionType::class, [
                    'entry_type'   => IntegerType::class,
                    'allow_add'    => true,
                    'allow_delete' => true
                ]);

        /** @var KegiatanRepository $repo1 */
        $repo1 = $options['kegiatan_repo'];
        $builder->get('kegiatan')->addModelTransformer(new IdToEntityTransformer($repo1, 'kegiatanId', false));

        /** @var ProvinsiRepository $repo2 */
        $repo2 = $options['provinsi_repo'];
        $builder->get('provincies')->addModelTransformer(new IdsToEntitiesTransformer($repo2, 'provinsiId',
                                                                                      'namaProvinsi',
                                                                                      false));

        /** @var KabupatenRepository $repo3 */
        $repo3 = $options['kabupaten_repo'];
        $builder->get('kabupatens')->addModelTransformer(new IdsToEntitiesTransformer($repo3, 'kabupatenId',
                                                                                      'namaKabupaten',
                                                                                      false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Subkegiatan::class])
                 ->setRequired('kegiatan_repo')
                 ->setRequired('provinsi_repo')
                 ->setRequired('kabupaten_repo')
                 ->setAllowedTypes('kegiatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
                 ->setAllowedTypes('provinsi_repo', 'Doctrine\Common\Persistence\ObjectRepository')
                 ->setAllowedTypes('kabupaten_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}