<?php


namespace App\Form\Kegiatan;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Finalisasi\Finalisasi;
use App\Entity\Kegiatan\Kegiatan;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\MasterData\MitraRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FinalisasiType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('judul', TextType::class)
            ->add('kegiatan', IntegerType::class)
            ->add('mitra', IntegerType::class)
            ->add('status')
            ->add('latarbelakang', TextType::class)
            ->add('tujuanprogram', TextType::class)
            ->add('outcome', TextType::class)
            ->add('ruanglingkup', TextType::class)
            ->add('pemangkukepentingan', TextType::class)
        ;

        /** @var KegiatanRepository $repo1 */
        $repo1 = $options['kegiatan_repo'];
        $builder->get('kegiatan')->addModelTransformer(new IdToEntityTransformer($repo1, 'kegiatanId', false));

        /** @var MitraRepository $repo2 */
        $repo2 = $options['mitra_repo'];
        $builder->get('mitra')->addModelTransformer(new IdToEntityTransformer($repo2, 'mitraId', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Finalisasi::class])
            ->setRequired('kegiatan_repo')
            ->setRequired('mitra_repo')
            ->setAllowedTypes('kegiatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('mitra_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}