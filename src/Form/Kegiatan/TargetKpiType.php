<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Kegiatan;


use App\Entity\Kegiatan\CapaianKpi;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TargetKpiType
 *
 * @package App\Form\MasterData
 * @author  Mark Melvin
 * @since   03/02/2019, modified: 04/02/2019 02:13
 */
class TargetKpiType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id')
            ->add('programCsr', IntegerType::class)
            ->add('periodeBatch', IntegerType::class)
            ->add('tahun', IntegerType::class)
            ->add('dataRef', IntegerType::class)
            ->add('showInDashboard', CheckboxType::class)
            ->add('kuantitas', CheckboxType::class)
            ->add('label', TextType::class)
            ->add('target', NumberType::class)
            ->add('p1', NumberType::class)
            ->add('p2', NumberType::class)
            ->add('p3', NumberType::class)
            ->add('p4', NumberType::class)
            ->add('nilai', NumberType::class)
            ->add('nilaiAkhir', NumberType::class)
            ->add('p1Verified', NumberType::class)
            ->add('p2Verified', NumberType::class)
            ->add('p3Verified', NumberType::class)
            ->add('p4Verified', NumberType::class);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => CapaianKpi::class]);
    }

}