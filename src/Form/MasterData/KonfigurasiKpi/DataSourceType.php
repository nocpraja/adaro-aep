<?php
/**
 * Description: DataSourceType.php generated by PhpStorm.
 * @project     adaro-aep
 * @package     App\Form\MasterData\KonfigurasiKpi
 * @author      alex
 * @created     2020-05-18, modified: 2020-05-18 01:49
 * @copyright   Copyright (c) 2020
 */

namespace App\Form\MasterData\KonfigurasiKpi;


use App\Entity\MasterData\KonfigurasiKpi\DataSourceEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DataSourceType extends AbstractType
{
    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('sourceName')
            ->add('sourceType')
            ->add('sourceQuery')
            ->add('sourceDesc');
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DataSourceEntity::class,
        ]);
    }
}
