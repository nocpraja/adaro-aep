<?php
/**
 * Description: DashboardType.php generated by PhpStorm.
 * @project     adaro-aep
 * @package     App\Form\MasterData\KonfigurasiKpi
 * @author      alex
 * @created     2020-05-18, modified: 2020-05-18 06:27
 * @copyright   Copyright (c) 2020
 */

namespace App\Form\MasterData\KonfigurasiKpi;

use App\Entity\MasterData\KonfigurasiKpi\DashboardEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DashboardType extends AbstractType
{
    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dashboardName')
            ->add('templateLayout')
            ->add('displayType')
            ->add('displaySource')
            ->add('dashboardComp')
            ->add('dashboardDesc');
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DashboardEntity::class,
        ]);
    }
}
