<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\MasterData;


use App\Entity\MasterData\ProgramCSR;
use App\Repository\MasterData\JenisProgramCSRRepository;
use App\Component\Form\DataTransformer\IdToEntityTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProgramCSRType
 *
 * @package App\Form\MasterData
 * @author  Mark Melvin
 * @since   27/09/2018, modified: 02/10/2018 02:13
 */
class ProgramCSRType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('templateName')
            ->add('description')
            ->add('jenis', IntegerType::class);

        /** @var JenisProgramCSRRepository $repo */
        $repo = $options['object_repo'];
        $builder->get('jenis')->addModelTransformer(new IdToEntityTransformer($repo, 'id', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => ProgramCSR::class])
            ->setRequired('object_repo')
            ->setAllowedTypes('object_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}