<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\MasterData;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Component\Form\Extension\ParamsType;
use App\Entity\MasterData\DataReference;
use App\Repository\MasterData\DataReferenceRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataReferenceType
 *
 * @package App\Form\MasterData
 * @author  Tri N
 * @since   07/02/2019, modified: 07/02/2019 10:52
 */
class DataReferenceType extends AbstractType
{
    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('protected')
            ->add('code', TextType::class)
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('parent', IntegerType::class)
            ->add('params', CollectionType::class, [
                'entry_type'   => ParamsType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ]);

        /** @var DataReferenceRepository $repo */
        $repo = $options['dataReference_repo'];
        $builder->get('parent')->addModelTransformer(new IdToEntityTransformer($repo, 'id', true));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => DataReference::class])
            ->setRequired('dataReference_repo')
            ->setAllowedTypes('dataReference_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}