<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\MasterData;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\MasterData\Kelurahan;
use App\Repository\MasterData\KecamatanRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class KelurahanType
 *
 * @package App\Form\MasterData
 * @author  Mark Melvin
 * @since   14/09/2018, modified: 20/09/2018 22:49
 */
class KelurahanType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('kodeKelurahan', TextType::class)
            ->add('namaKelurahan', TextType::class)
            ->add('kecamatan', IntegerType::class);

        /** @var KecamatanRepository $repo */
        $repo = $options['object_repo'];
        $builder->get('kecamatan')->addModelTransformer(new IdToEntityTransformer($repo, 'kecamatanId', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Kelurahan::class])
                 ->setRequired('object_repo')
                 ->setAllowedTypes('object_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}
