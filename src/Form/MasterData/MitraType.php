<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\MasterData;

use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\MasterData\Mitra;
use App\Form\Beneficiary\AlamatTypeTrait;
use App\Repository\MasterData\ProgramCSRRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MitraType
 *
 * @package App\Form\MasterData
 * @author  Tri N
 * @since   24/01/2019, modified: 24/01/2019 14:19
 */
class MitraType extends AbstractType
{
    use AlamatTypeTrait;

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jenisLembaga', TextType::class)
            ->add('namaLembaga',TextType::class)
            ->add('kodeLembaga',TextType::class)
            ->add('aktePendirian',TextType::class)
            ->add('namaPimpinan',TextType::class)
            ->add('picProgram',TextType::class)
            ->add('pimpinanPhone',TextType::class)
            ->add('picPhone',TextType::class)
            ->add('officePhone', TextType::class)
            ->add('programCsr', IntegerType::class)
            ->add('email', EmailType::class)
            ->add('website',TextType::class)
            ->add('latitude', NumberType::class)
            ->add('longitude', NumberType::class);

        $this->addAlamat($builder, $options);

        /** @var ProgramCSRRepository $repo */
        $repo = $options['programCsr_repo'];
        $builder->get('programCsr')->addModelTransformer(new IdToEntityTransformer($repo, 'msid', true));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => Mitra::class])
            ->setRequired('programCsr_repo')
            ->setRequired('kecamatan_repo')
            ->setRequired('kelurahan_repo')
            ->setAllowedTypes('programCsr_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kecamatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kelurahan_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}