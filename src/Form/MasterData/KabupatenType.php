<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\MasterData;

use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\MasterData\Kabupaten;
use App\Repository\MasterData\ProvinsiRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class KabupatenType
 *
 * @package App\Form\MasterData
 * @author  Tri N
 * @since   25/09/2018, modified: 25/09/2018 11:15
 */
class KabupatenType extends AbstractType
{
    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('kodeKabupaten', TextType::class)
            ->add('namaKabupaten', TextType::class)
            ->add('provinsi', IntegerType::class);

        /** @var ProvinsiRepository $repo */
        $repo = $options['object_repo'];
        $builder->get('provinsi')->addModelTransformer(new IdToEntityTransformer($repo, 'provinsiId', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => Kabupaten::class])
            ->setRequired('object_repo')
            ->setAllowedTypes('object_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}
