<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\MasterData;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\MasterData\BatchCsr;
use App\Repository\MasterData\MitraRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BatchCsrType
 *
 * @package App\Form\MasterData
 * @author  Ahmad Fajar
 * @since   08/11/2018, modified: 04/12/2018 08:38
 */
class BatchCsrType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('mitra', IntegerType::class)
            ->add('programCsr', IntegerType::class)
            ->add('namaBatch', TextType::class)
            ->add('managementFee', NumberType::class)
            ->add('tahunAwal', IntegerType::class)
            ->add('tahunAkhir', IntegerType::class);

        /** @var MitraRepository $mitraRepo */
        $mitraRepo = $options['mitra_repo'];
        $builder->get('mitra')->addModelTransformer(new IdToEntityTransformer($mitraRepo, 'mitraId', false));

        $programRepo = $options['program_repo'];
        $builder->get('programCsr')->addModelTransformer(new IdToEntityTransformer($programRepo, 'msid', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => BatchCsr::class])
            ->setRequired('mitra_repo')
            ->setRequired('program_repo')
            ->setAllowedTypes('mitra_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('program_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}