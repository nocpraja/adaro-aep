<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\MasterData;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\MasterData\Kecamatan;
use App\Repository\MasterData\KabupatenRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class KecamatanType
 *
 * @package App\Form\MasterData
 * @author  Ahmad Fajar
 * @since   06/09/2018, modified: 07/09/2018 00:27
 */
class KecamatanType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('kodeKecamatan', TextType::class)
            ->add('namaKecamatan', TextType::class)
            ->add('kabupaten', IntegerType::class);

        /** @var KabupatenRepository $repo */
        $repo = $options['object_repo'];
        $builder->get('kabupaten')->addModelTransformer(new IdToEntityTransformer($repo, 'kabupatenId', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Kecamatan::class])
                 ->setRequired('object_repo')
                 ->setAllowedTypes('object_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}
