<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Form\Pendanaan;


use App\Entity\Pendanaan\DanaBatchRevisiAnggaran;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DanaBatchRevisiAnggaranType
 *
 * @package App\Form\Pendanaan
 * @author  Ahmad Fajar
 * @since   11/05/2019, modified: 12/05/2019 3:20
 */
class DanaBatchRevisiAnggaranType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('refcode', TextType::class)
                ->add('jumlah', NumberType::class);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => DanaBatchRevisiAnggaran::class]);
    }

}