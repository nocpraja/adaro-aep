<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Form\Pendanaan;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Pendanaan\DanaBatchDonatur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DanaBatchDonaturType
 *
 * @package App\Form\Pendanaan
 * @author  Ahmad Fajar
 * @since   30/04/2019, modified: 12/05/2019 3:15
 */
class DanaBatchDonaturType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jumlah', NumberType::class)
            ->add('company', IntegerType::class);

        $repo = $options['repo'];
        $builder->get('company')->addModelTransformer(new IdToEntityTransformer($repo, 'id', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => DanaBatchDonatur::class])
                 ->setRequired('repo')
                 ->setAllowedTypes('repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}