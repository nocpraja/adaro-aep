<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Form\Pendanaan;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Pendanaan\DanaProgram;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DanaProgramType
 *
 * @package App\Form\Pendanaan
 * @author  Ahmad Fajar
 * @since   29/04/2019, modified: 29/04/2019 6:33
 */
class DanaProgramType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('tahunAwal', IntegerType::class)
            ->add('tahunAkhir', IntegerType::class)
            ->add('newValue', NumberType::class)
            ->add('danaBidang', IntegerType::class)
            ->add('programCsr', IntegerType::class);

        $danaRepo = $options['dana_repo'];
        $programRepo = $options['program_repo'];
        $builder->get('danaBidang')->addModelTransformer(new IdToEntityTransformer($danaRepo, 'id', false));
        $builder->get('programCsr')->addModelTransformer(new IdToEntityTransformer($programRepo, 'msid', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => DanaProgram::class])
                 ->setRequired('dana_repo')
                 ->setRequired('program_repo')
                 ->setAllowedTypes('dana_repo', 'Doctrine\Common\Persistence\ObjectRepository')
                 ->setAllowedTypes('program_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}