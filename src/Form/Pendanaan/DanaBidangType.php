<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Form\Pendanaan;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Pendanaan\DanaBidang;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DanaBidangType
 *
 * @package App\Form\Pendanaan
 * @author  Ahmad Fajar
 * @since   28/04/2019, modified: 28/04/2019 12:46
 */
class DanaBidangType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('tahunAwal', IntegerType::class)
            ->add('tahunAkhir', IntegerType::class)
            ->add('newValue', NumberType::class)
            ->add('bidangCsr', IntegerType::class);

        $repo = $options['repo'];
        $builder->get('bidangCsr')->addModelTransformer(new IdToEntityTransformer($repo, 'id', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => DanaBidang::class])
                 ->setRequired('repo')
                 ->setAllowedTypes('repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}