<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Form\Pendanaan;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Pendanaan\DanaBatch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DanaBatchType
 *
 * @package App\Form\Pendanaan
 * @author  Ahmad Fajar
 * @since   30/04/2019, modified: 12/05/2019 3:15
 */
class DanaBatchType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('namaProgram', TextType::class)
            ->add('tahun', IntegerType::class)
            ->add('newValue', NumberType::class)
            ->add('danaProgram', IntegerType::class)
            ->add('batch', IntegerType::class)
            ->add('donaturs', CollectionType::class, [
                'entry_type'    => DanaBatchDonaturType::class,
                'entry_options' => ['repo' => $options['dataref_repo']],
                'allow_add'     => true,
                'allow_delete'  => true
            ]);

        $danaRepo = $options['dana_repo'];
        $batchRepo = $options['batch_repo'];
        $builder->get('danaProgram')->addModelTransformer(new IdToEntityTransformer($danaRepo, 'id', false));
        $builder->get('batch')->addModelTransformer(new IdToEntityTransformer($batchRepo, 'batchId', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => DanaBatch::class])
                 ->setRequired('dana_repo')
                 ->setRequired('batch_repo')
                 ->setRequired('dataref_repo')
                 ->setAllowedTypes('dana_repo', 'Doctrine\Common\Persistence\ObjectRepository')
                 ->setAllowedTypes('batch_repo', 'Doctrine\Common\Persistence\ObjectRepository')
                 ->setAllowedTypes('dataref_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}