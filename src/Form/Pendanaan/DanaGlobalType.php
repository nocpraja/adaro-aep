<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Pendanaan;


use App\Entity\Pendanaan\DanaGlobal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DanaGlobalType
 *
 * @package App\Form\Pendanaan
 * @author  Ahmad Fajar
 * @since   03/11/2018, modified: 24/04/2019 6:50
 */
class DanaGlobalType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class)
            ->add('tahunAwal', IntegerType::class)
            ->add('tahunAkhir', IntegerType::class)
            ->add('newValue', NumberType::class);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => DanaGlobal::class]);
    }
}