<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Form\Pendanaan;


use App\Entity\Pendanaan\DanaBatch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DanaBatchRevisiType
 *
 * @package App\Form\Pendanaan
 * @author  Ahmad Fajar
 * @since   11/05/2019, modified: 13/05/2019 2:16
 */
class DanaBatchRevisiType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('requests', CollectionType::class, [
            'entry_type'   => DanaBatchRevisiAnggaranType::class,
            'allow_add'    => true,
            'allow_delete' => true
        ]);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => DanaBatch::class]);
    }

}