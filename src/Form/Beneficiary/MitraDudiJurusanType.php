<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;


use App\Entity\Beneficiary\MitraDudiJurusan;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MitraDudiJurusanType
 *
 * @package App\Form\Beneficiary
 * @author  Mark Melvin
 * @since   28/02/2019, modified: 26/03/2019 22:21
 *
 */
class MitraDudiJurusanType extends AbstractType
{
    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('namaUsaha', TextType::class)
            ->add('jenisUsaha', TextType::class)
            ->add('namaPimpinan', TextType::class)
            ->add('contactPhone', TextType::class)
            ->add('alamat', TextType::class);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => MitraDudiJurusan::class]);
    }

}
