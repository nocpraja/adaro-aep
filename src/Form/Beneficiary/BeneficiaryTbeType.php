<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;


use App\Component\Form\Extension\PelatihanType;
use App\Component\Form\Extension\PrestasiType;
use App\Component\Form\Extension\TextTagType;
use App\Entity\Beneficiary\BeneficiaryTbe;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BeneficiaryTbeType
 *
 * @package App\Form\Beneficiary
 * @author  Tri N
 * @since   03/12/2018, modified: 2018-12-17 19:43
 */
class BeneficiaryTbeType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nip', TextType::class)
            ->add('jenisInstitusi', IntegerType::class)
            ->add('namaInstitusi', TextType::class)
            ->add('alamatInstitusi', TextType::class)
            ->add('lamaMengajar', TextType::class)
            ->add('sertifikasi')
            ->add('mapel', CollectionType::class, [
                'entry_type'   => TextTagType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ])
            ->add('prestasi', CollectionType::class, [
                'entry_type'   => PrestasiType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ])
            ->add('pelatihan', CollectionType::class, [
                'entry_type'   => PelatihanType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ])
            ->add('biodata', BeneficiaryIndividuType::class, [
                'institusi_repo' => $options['institusi_repo'],
                'program_repo'   => $options['program_repo'],
                'kecamatan_repo' => $options['kecamatan_repo'],
                'kelurahan_repo' => $options['kelurahan_repo']
            ]);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => BeneficiaryTbe::class])
            ->setRequired('institusi_repo')
            ->setRequired('program_repo')
            ->setRequired('kecamatan_repo')
            ->setRequired('kelurahan_repo')
            ->setAllowedTypes('institusi_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('program_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kecamatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kelurahan_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}