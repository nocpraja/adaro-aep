<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;


use App\Component\Form\DataTransformer\IdsToEntitiesTransformer;
use App\Entity\Beneficiary\BeneficiaryInstitusi;
use App\Repository\Pendanaan\ProgramRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BeneficiaryInstitusiType
 *
 * @package App\Form\Beneficiary
 * @author  Tri N
 * @since   30/09/2018, modified: 30/04/2019 4:55
 */
class BeneficiaryInstitusiType extends AbstractType
{

    use AlamatTypeTrait;

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kategori', IntegerType::class)
            ->add('swasta')
            ->add('namaInstitusi')
            ->add('kodeInstitusi')
            ->add('nomorAkte')
            ->add('tahunBerdiri', IntegerType::class)
            ->add('akreditasi')
            ->add('nilaiAkreditasi')
            ->add('jenisPaud')
            ->add('email', EmailType::class)
            ->add('website')
            ->add('officePhone')
            ->add('contactPhone')
            ->add('namaPimpinan')
            ->add('latitude', NumberType::class)
            ->add('longitude', NumberType::class)
            ->add('status')
            ->add('statusVerifikasi')
            ->add('programs', CollectionType::class, [
                'entry_type'   => IntegerType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ]);

        $this->addAlamat($builder, $options);

        /** @var ProgramRepository $repo */
        $repo = $options['program_repo'];
        $builder->get('programs')->addModelTransformer(new IdsToEntitiesTransformer($repo, 'id',
                                                                                    'namaProgram',
                                                                                    false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => BeneficiaryInstitusi::class])
            ->setRequired('program_repo')
            ->setRequired('kecamatan_repo')
            ->setRequired('kelurahan_repo')
            ->setAllowedTypes('program_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kecamatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kelurahan_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}