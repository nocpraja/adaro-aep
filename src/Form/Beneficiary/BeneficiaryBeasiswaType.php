<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Component\Form\Extension\TextTagType;
use App\Entity\Beneficiary\BeneficiaryBeasiswa;
use App\Repository\MasterData\DataReferenceRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BeneficiaryBeasiswaType
 *
 * @package App\Form\Beneficiary
 * @author  Tri N
 * @since   13/11/2018, modified: 23/11/2018 22:59
 */
class BeneficiaryBeasiswaType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nim', TextType::class)
            ->add('tahunMasuk', IntegerType::class)
            ->add('jalurMasuk', TextType::class)
            ->add('nomorRekening', TextType::class)
            ->add('nomorCif', TextType::class)
            ->add('namaBank', TextType::class)
            ->add('kartuSehat', TextType::class)
            ->add('golonganDarah', TextType::class)
            ->add('sekolahAsal', TextType::class)
            ->add('alamatSekolah', TextType::class)
            ->add('prestasi', CollectionType::class, [
                'entry_type'   => TextTagType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ])
            ->add('fakultas', IntegerType::class)
            ->add('jurusan', IntegerType::class)
            ->add('biodata', BeneficiaryIndividuType::class, [
                'institusi_repo' => $options['institusi_repo'],
                'program_repo'   => $options['program_repo'],
                'kecamatan_repo' => $options['kecamatan_repo'],
                'kelurahan_repo' => $options['kelurahan_repo']
            ])
            ->add('orangTua', OrangTuaType::class, [
                'kecamatan_repo' => $options['kecamatan_repo'],
                'kelurahan_repo' => $options['kelurahan_repo']
            ]);

        /** @var DataReferenceRepository $repo */
        $repo = $options['reference_repo'];
        $builder->get('fakultas')->addModelTransformer(new IdToEntityTransformer($repo, 'id', false));
        $builder->get('jurusan')->addModelTransformer(new IdToEntityTransformer($repo, 'id', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => BeneficiaryBeasiswa::class])
            ->setRequired('reference_repo')
            ->setRequired('institusi_repo')
            ->setRequired('program_repo')
            ->setRequired('kecamatan_repo')
            ->setRequired('kelurahan_repo')
            ->setAllowedTypes('reference_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('institusi_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('program_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kecamatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kelurahan_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}