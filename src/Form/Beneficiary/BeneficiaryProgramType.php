<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Repository\Pendanaan\DanaBatchRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class BeneficiaryProgramType
 *
 * @package App\Form\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/11/2018, modified: 30/04/2019 4:55
 */
class BeneficiaryProgramType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('beneficiary', IntegerType::class, ['constraints' => new NotNull()])
                ->add('program', IntegerType::class, ['constraints' => new NotNull()]);

        $repo1 = $options['entry_repo'];
        $builder->get('beneficiary')->addModelTransformer(new IdToEntityTransformer($repo1, $options['entry_key'], false));

        /** @var DanaBatchRepository $repo2 */
        $repo2 = $options['program_repo'];
        $builder->get('program')->addModelTransformer(new IdToEntityTransformer($repo2, 'id', false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('entry_key')
            ->setRequired('entry_repo')
            ->setRequired('program_repo')
            ->setAllowedTypes('entry_key', 'string')
            ->setAllowedTypes('entry_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('program_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}