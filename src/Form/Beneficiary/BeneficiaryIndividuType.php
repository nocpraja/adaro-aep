<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;


use App\Component\Form\DataTransformer\IdsToEntitiesTransformer;
use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Component\Form\Extension\TextTagType;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Repository\Beneficiary\BeneficiaryInstitusiRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BeneficiaryIndividuType
 *
 * @package App\Form\Beneficiary
 * @author  Tri N
 * @since   1/11/2018, modified: 24/05/2019 0:46
 */
class BeneficiaryIndividuType extends AbstractType
{

    use AlamatTypeTrait;

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('kategori', IntegerType::class)
            ->add('namaLengkap')
            ->add('nik')
            ->add('jenisKelamin')
            ->add('tempatLahir')
            ->add('tanggalLahir', DateType::class, [
                'widget' => 'single_text',
                'format' => DateType::HTML5_FORMAT
            ])
            ->add('phone')
            ->add('facebook')
            ->add('instagram')
            ->add('pendidikan')
            ->add('jabatan')
            ->add('statusJabatan')
            ->add('email', EmailType::class)
            ->add('institusi', IntegerType::class)
            ->add('status')
            ->add('statusVerifikasi')
            ->add('programs', CollectionType::class, [
                'entry_type'   => IntegerType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ])
            ->add('kompetensiWirausaha', CollectionType::class, [
                'entry_type' => TextTagType::class,
                'allow_add' => true,
                'allow_delete' => true
            ]);

        $this->addAlamat($builder, $options);

        /** @var BeneficiaryInstitusiRepository $institusi_repo */
        $institusi_repo = $options['institusi_repo'];
        $builder->get('institusi')->addModelTransformer(new IdToEntityTransformer($institusi_repo,
                                                                                  'institusiId', true));
        /** @var DanaBatchRepository $program_repo */
        $program_repo = $options['program_repo'];
        $builder->get('programs')->addModelTransformer(new IdsToEntitiesTransformer($program_repo,
                                                                                    'id',
                                                                                    'namaProgram',
                                                                                    false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => BeneficiaryIndividu::class])
            ->setRequired('institusi_repo')
            ->setRequired('program_repo')
            ->setRequired('kecamatan_repo')
            ->setRequired('kelurahan_repo')
            ->setAllowedTypes('institusi_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('program_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kecamatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kelurahan_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}