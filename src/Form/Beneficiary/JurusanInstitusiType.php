<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Component\Form\Extension\PesertaDidikType;
use App\Entity\Beneficiary\JurusanInstitusi;
use App\Repository\Beneficiary\BeneficiaryInstitusiRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class ProgramKeahlianType
 *
 * @package App\Form\Beneficiary
 * @author  Mark Melvin
 * @since   28/02/2019, modified: 26/03/2019 22:18
 */
class JurusanInstitusiType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('jenis', IntegerType::class)
            ->add('namaJurusan', TextType::class)
            ->add('nilaiAkreditasi', TextType::class)
            ->add('kepalaJurusan', TextType::class)
            ->add('contactPhone', TextType::class)
            ->add('dosenMkdu', IntegerType::class)
            ->add('dosenProdi', IntegerType::class)
            ->add('guruProduktif', IntegerType::class)
            ->add('guruAdaptif', IntegerType::class)
            ->add('guruNormatif', IntegerType::class)
            ->add('rombelX', IntegerType::class)
            ->add('rombelXi', IntegerType::class)
            ->add('rombelXii', IntegerType::class)
            ->add('rombelXiii', IntegerType::class)
            ->add('jumlahSiswa', IntegerType::class)
            ->add('pesertaDidik', CollectionType::class, [
                'entry_type'   => PesertaDidikType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ])
            ->add('unitProduksi', UnitProduksiType::class)
            ->add('mitraDudis', CollectionType::class, [
                'entry_type'   => MitraDudiJurusanType::class,
                'allow_add'    => true,
                'allow_delete' => true
            ])
            ->add('institusi', IntegerType::class);

        /** @var BeneficiaryInstitusiRepository $repo */
        $repo = $options['institusi_repo'];
        $builder->get('institusi')->addModelTransformer(new IdToEntityTransformer($repo, 'institusiId', false));

    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => JurusanInstitusi::class])
                 ->setRequired('institusi_repo')
                 ->setAllowedTypes('institusi_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}