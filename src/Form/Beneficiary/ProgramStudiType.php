<?php
/**
 * Created by PhpStorm.
 * User: Mark Melvin
 * Date: 3/20/2019
 * Time: 3:25 PM
 */

namespace App\Form\Beneficiary;

use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Component\Form\Extension\PesertaDidikType;
use App\Entity\Beneficiary\JurusanInstitusi;
use App\Repository\Beneficiary\JurusanInstitusiRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ProgramStudiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('namaJurusan', TextType::class)
            ->add('jenis', IntegerType::class)
            ->add('nilaiAkreditasi', TextType::class)
            ->add('kepalaJurusan', TextType::class)
            ->add('contactPhone', TextType::class)
            ->add('dosenMkdu', NumberType::class)
            ->add('dosenProdi', NumberType::class)
            ->add('jumlahSiswa', NumberType::class)
            ->add('pesertaDidik', CollectionType::class, [
                'entry_type' => PesertaDidikType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('unitProduksi', UnitProduksiType::class)
            ->add('mitraDudis', CollectionType::class, [
                'entry_type' => MitraDudiJurusanType::class,
                //'entry_options' => ['object_repo' => $options['object_repo1']],
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('institusi', IntegerType::class);

        /** @var JurusanInstitusiRepository $repo */
        $repo = $options['object_repo'];
        $builder->get('institusi')->addModelTransformer(new IdToEntityTransformer($repo, 'institusiId', false));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => JurusanInstitusi::class])
            ->setRequired('object_repo')
            //->setRequired('object_repo1')
            //->setRequired('object_repo2')
            ->setAllowedTypes('object_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            //->setAllowedTypes('object_repo1', 'Doctrine\Common\Persistence\ObjectRepository')
            //->setAllowedTypes('object_repo2', 'Doctrine\Common\Persistence\ObjectRepository')
        ;
    }
}