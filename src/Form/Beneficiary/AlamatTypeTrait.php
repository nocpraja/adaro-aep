<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;

use App\Component\Form\DataTransformer\IdToEntityTransformer;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Trait AlamatTypeTrait
 *
 * @package App\Form\Beneficiary
 * @author  Tri N
 * @since   30/09/2018, modified: 2018-12-17 07:26
 */
trait AlamatTypeTrait
{

    protected function addAlamat(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alamat')
            ->add('rtRw')
            ->add('kodePos')
            ->add('kecamatan')
            ->add('kelurahan');

        $kecamatanRepo = $options['kecamatan_repo'];
        $builder->get('kecamatan')->addModelTransformer(new IdToEntityTransformer($kecamatanRepo, 'kecamatanId', true));
        $kelurahanRepo = $options['kelurahan_repo'];
        $builder->get('kelurahan')->addModelTransformer(new IdToEntityTransformer($kelurahanRepo, 'kelurahanId', true));
    }
}