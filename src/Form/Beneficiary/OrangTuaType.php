<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Beneficiary;


use App\Entity\Beneficiary\OrangTua;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrangTuaType
 *
 * @package App\Form\Beneficiary
 * @author  Tri N
 * @since   14/11/2018, modified: 23/11/2018 22:58
 */
class OrangTuaType extends AbstractType
{

    use AlamatTypeTrait;

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('namaAyah', TextType::class)
            ->add('namaIbu', TextType::class)
            ->add('pekerjaanAyah', TextType::class)
            ->add('pekerjaanIbu', TextType::class)
            ->add('penghasilan', NumberType::class);

        $this->addAlamat($builder, $options);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => OrangTua::class])
            ->setRequired('kecamatan_repo')
            ->setRequired('kelurahan_repo')
            ->setAllowedTypes('kecamatan_repo', 'Doctrine\Common\Persistence\ObjectRepository')
            ->setAllowedTypes('kelurahan_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}