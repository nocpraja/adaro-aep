<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Security;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Component\Form\Extension\ParamsType;
use App\Entity\Security\AppSetting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AppSettingType
 *
 * @package App\Form\Security
 * @author  Mark Melvin
 * @since   23/02/2020, modified: 23/03/2020 2:00
 */
class AppSettingType extends AbstractType
{
    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('section', TextType::class)
            ->add('attribute', TextType::class)
            ->add('attributeType', TextType::class)
            ->add('attributeValue', TextType::class)
            ->add('hidden')
            ->add('position', IntegerType::class)
            ->add('label', TextType::class)
            ->add('description', TextType::class);

    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => AppSetting::class]);
    }
}