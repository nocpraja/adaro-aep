<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Security;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use App\Entity\Security\UserAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserAccountType
 *
 * @package App\Form\Security
 * @author  Tri N
 * @since   10/08/2018, modified: 24/04/2019 20:49
 */
class UserAccountType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class)
            ->add('fullName', TextType::class)
            ->add('userPassword')
            ->add('email', EmailType::class)
            ->add('enabled')
            ->add('group', IntegerType::class)
            ->add('mitraCompany', IntegerType::class);

        $repo = $options['group_repo'];
        $mitraRepo = $options['mitra_repo'];
        $builder->get('group')->addModelTransformer(new IdToEntityTransformer($repo, 'groupId', false));
        $builder->get('mitraCompany')->addModelTransformer(new IdToEntityTransformer($mitraRepo, 'mitraId', true));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => UserAccount::class])
                 ->setRequired('group_repo')
                 ->setRequired('mitra_repo')
                 ->setAllowedTypes('group_repo', 'Doctrine\Common\Persistence\ObjectRepository')
                 ->setAllowedTypes('mitra_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }
}
