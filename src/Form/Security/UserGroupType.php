<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Form\Security;


use App\Entity\Security\UserGroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserGroupType
 *
 * @package App\Form\Security
 * @author  Ahmad Fajar
 * @since   03/08/2018, modified: 18/05/2019 13:39
 */
class UserGroupType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('groupName', TextType::class)
            ->add('description', TextareaType::class, ['required' => false])
            ->add('enabled')
            ->add('roles', CollectionType::class, [
                'allow_add'    => true,
                'allow_delete' => true
            ]);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => UserGroup::class]);
    }
}
