<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Form\User;


use App\Component\DataObject\ChangePasswordDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubmitNewPasswordType
 *
 * @package App\Form\User
 * @author  Tri N
 * @since   04/04/2019, modified: 2019-04-16 00:27
 */
class ChangePasswordType extends AbstractType
{

    const CSRF_NAME = 'user.forgot_password_confirm';

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('password', PasswordType::class, [
                'required' => true,
                'label'    => 'ForgotPassword.LabelNewPassword'
            ])
            ->add('confirmPassword', PasswordType::class, [
                'required' => true,
                'label'    => 'ForgotPassword.LabelConfirmNewPassword'
            ]);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class'      => ChangePasswordDto::class,
                                'csrf_protection' => true,
                                'csrf_token_id'   => self::CSRF_NAME,
                                'csrf_field_name' => '_csrf'
                               ]);
    }

}