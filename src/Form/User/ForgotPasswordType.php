<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Form\User;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class SubmitEmailType
 *
 * @package App\Form\User
 * @author  Tri N
 * @since   29/03/2019, modified: 2019-04-14 22:47
 */
class ForgotPasswordType extends AbstractType
{

    const CSRF_NAME = 'user.forgot_password_submit_email';

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('email', EmailType::class, [
            'required'    => true,
            'label'       => 'ForgotPassword.LabelEmail',
            'constraints' => [new NotBlank(), new Email()],
            'attr'        => [
                'placeholder'   => 'my-email@example.com',
                'autocomplete'  => 'off',
                'aria-required' => true,
                'autofocus'     => true
            ]
        ]);
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['csrf_protection' => true,
                                'csrf_token_id'   => self::CSRF_NAME,
                                'csrf_field_name' => '_csrf'
                               ]);
    }

}