<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Event\Pendanaan;


use Symfony\Component\EventDispatcher\Event;

/**
 * Class TransactionEvent
 *
 * @package App\Event\Pendanaan
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 16:49
 */
final class TransactionEvent extends Event
{

    /**
     * @var string
     */
    private $refcode;

    /**
     * @var string
     */
    private $title;

    /**
     * @var float
     */
    private $jumlah;

    /**
     * @var int
     */
    private $tahunAwal;

    /**
     * @var int
     */
    private $tahunAkhir;


    /**
     * TransactionEvent constructor.
     *
     * @param string $refcode
     * @param float  $jumlah
     * @param string $title
     * @param int    $tahunAwal
     * @param int    $tahunAkhir
     */
    public function __construct(string $refcode, float $jumlah, ?string $title, ?int $tahunAwal, ?int $tahunAkhir)
    {
        $this->refcode = $refcode;
        $this->title = $title;
        $this->jumlah = $jumlah;
        $this->tahunAwal = $tahunAwal;
        $this->tahunAkhir = $tahunAkhir;
    }

    /**
     * Jumlah dana dalam rupiah.
     *
     * @return float
     */
    public function getJumlah(): float
    {
        return $this->jumlah;
    }

    /**
     * Kode referensi transaksi pendanaan.
     *
     * @return string
     */
    public function getRefcode(): string
    {
        return $this->refcode;
    }

    /**
     * Periode tahun akhir.
     *
     * @return int
     */
    public function getTahunAkhir(): ?int
    {
        return $this->tahunAkhir;
    }

    /**
     * Periode tahun awal.
     *
     * @return int
     */
    public function getTahunAwal(): ?int
    {
        return $this->tahunAwal;
    }

    /**
     * Nama transaksi pendanaan.
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

}