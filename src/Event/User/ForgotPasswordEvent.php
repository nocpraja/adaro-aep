<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Event\User;


use App\Entity\Security\UserAccount;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ForgotPasswordEvent
 *
 * @package App\Event
 * @author  Ahmad Fajar
 * @since   2019-04-14, modified: 2019-04-18 13:11
 */
final class ForgotPasswordEvent extends Event
{

    const NAME = 'user.forgot_password';

    /**
     * @var UserAccount
     */
    private $userAccount;


    /**
     * ForgotPasswordEvent constructor.
     *
     * @param UserAccount $userAccount Akun pengguna yang melakukan request RESET PASSWORD
     */
    public function __construct(UserAccount $userAccount)
    {
        $this->userAccount = $userAccount;
    }

    /**
     * Get UserAccount yang melakukan request <b>Reset Password</b>.
     *
     * @return UserAccount
     */
    public function getUserAccount(): ?UserAccount
    {
        return $this->userAccount;
    }

}
