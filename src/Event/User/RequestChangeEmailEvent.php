<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Event\User;


use App\Entity\Security\UserAccount;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class RequestChangeEmailEvent
 *
 * @package App\Event\User
 * @author  Ahmad Fajar
 * @since   2019-04-16, modified: 2019-04-18 13:11
 */
final class RequestChangeEmailEvent extends Event
{

    const REQUEST = 'user.request_change_email';
    const CONFIRM = 'user.confirm_new_email';

    /**
     * @var UserAccount
     */
    private $userAccount;


    /**
     * RequestChangeEmailEvent constructor.
     *
     * @param UserAccount $userAccount Akun pengguna yang melakukan request CHANGE EMAIL ADDRESS
     */
    public function __construct(UserAccount $userAccount)
    {
        $this->userAccount = $userAccount;
    }

    /**
     * Get UserAccount yang melakukan request <b>Forgot Password</b>.
     *
     * @return UserAccount
     */
    public function getUserAccount(): ?UserAccount
    {
        return $this->userAccount;
    }

}