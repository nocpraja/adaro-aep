<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow;

/**
 * Class Transition is used to define workflow steps.
 *
 * @package App\Component\Workflow
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 26/04/2019 16:12
 */
class Transition
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string[]
     */
    private $froms;

    /**
     * @var string
     */
    private $to;


    /**
     * Transition constructor.
     *
     * @param string[] $froms Any valid start marking places
     * @param string   $to    Next marking place
     * @param string   $name  Workflow transition name
     */
    public function __construct(array $froms, string $to, string $name)
    {
        $this->name = $name;
        $this->froms = $froms;
        $this->to = $to;
    }

    /**
     * Get workflow transition name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get any valid start marking places for current transition.
     *
     * @return string[]
     */
    public function getFroms(): array
    {
        return $this->froms;
    }

    /**
     * Get next marking place.
     *
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

}