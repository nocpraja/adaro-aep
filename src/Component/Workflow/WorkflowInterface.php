<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow;


use App\Component\Workflow\Blocker\TransitionBlockerList;
use App\Component\Workflow\DataObject\Marking;
use App\Component\Workflow\Exception\NotEnabledTransitionException;
use App\Component\Workflow\Exception\UndefinedTransitionException;
use App\Component\Workflow\Visitor\MarkingVisitorInterface;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;

/**
 * Interface WorkflowInterface
 *
 * @package App\Component\Workflow
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 02/05/2019 1:42
 */
interface WorkflowInterface
{
    /**
     * Fire a transition.
     *
     * @param object                      $subject        A subject
     * @param string                      $transitionName A transition
     * @param array                       $context        Additional context to apply
     * @param EntityManagerInterface|null $entityManager  Doctrine entity manager
     *
     * @return Marking The new Marking
     * @throws UndefinedTransitionException
     * @throws NotEnabledTransitionException
     */
    public function apply(object $subject, string $transitionName, array $context = [],
                          EntityManagerInterface $entityManager = null): Marking;

    /**
     * Builds a TransitionBlockerList to know why a transition is blocked.
     *
     * @param object $subject A subject
     * @param string $transitionName
     *
     * @return TransitionBlockerList
     */
    public function buildTransitionBlockerList(object $subject, string $transitionName): TransitionBlockerList;

    /**
     * Returns true if the transition is enabled.
     *
     * @param object $subject        A subject
     * @param string $transitionName A transition
     *
     * @return bool true if the transition is enabled
     */
    public function can(object $subject, string $transitionName): bool;

    /**
     * Get workflow configurations.
     *
     * @return Definition
     */
    public function getDefinition(): Definition;

    /**
     * Returns all enabled transitions.
     *
     * @param object $subject A subject
     *
     * @return Transition[] All enabled transitions
     */
    public function getEnabledTransitions(object $subject): array;

    /**
     * Returns the object's Marking.
     *
     * @param object $subject A subject
     *
     * @return Marking The Marking
     *
     * @throws LogicException
     */
    public function getMarking(object $subject): Marking;

    /**
     * Workflow step marking handler.
     *
     * @return MarkingVisitorInterface
     */
    public function getMarkingVisitor(): MarkingVisitorInterface;

    /**
     * Get workflow name.
     *
     * @return string
     */
    public function getName(): string;

}