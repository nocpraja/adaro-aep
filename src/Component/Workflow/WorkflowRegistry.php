<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow;


use App\Component\Workflow\DataObject\SupportedObjectInterface;
use InvalidArgumentException;

/**
 * Class WorkflowRegistry
 *
 * @package App\Component\Workflow
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 26/04/2019 4:10
 */
class WorkflowRegistry
{
    /**
     * @var WorkflowInterface[]
     */
    private $workflows = [];


    /**
     * Register a workflow instance to the workflow registry manager.
     *
     * @param WorkflowInterface        $workflow        Workflow service instance
     * @param SupportedObjectInterface $supportStrategy Supported object checker
     */
    public function addWorkflow(WorkflowInterface $workflow, SupportedObjectInterface $supportStrategy): void
    {
        $this->workflows[] = [$workflow, $supportStrategy];
    }

    /**
     * Get a workflow instance for the given subject.
     *
     * @param object      $subject      An object to check
     * @param string|null $workflowName The workflow name
     *
     * @return WorkflowInterface|null
     */
    public function get(object $subject, string $workflowName = null): ?WorkflowInterface
    {
        $matched = null;

        /** @var WorkflowInterface $workflow */
        /** @var SupportedObjectInterface $supportStrategy */
        foreach ($this->workflows as list($workflow, $supportStrategy)) {
            if ($this->supports($workflow, $supportStrategy, $subject, $workflowName)) {
                if ($matched) {
                    throw new InvalidArgumentException('At least two workflows match this subject. ' .
                                                       'Set a different name on each and use the second (name) ' .
                                                       'argument of this method.');
                }

                $matched = $workflow;
            }
        }

        if (!$matched) {
            throw new InvalidArgumentException(sprintf('Unable to find a workflow for class "%s".',
                                                       get_class($subject)));
        }

        return $matched;
    }

    /**
     * Get all matched workflows for the given subject.
     *
     * @param object $subject An object to check
     *
     * @return WorkflowInterface[]
     */
    public function all(object $subject): array
    {
        $matched = [];

        /** @var WorkflowInterface $workflow */
        /** @var SupportedObjectInterface $supportStrategy */
        foreach ($this->workflows as list($workflow, $supportStrategy)) {
            if ($supportStrategy->supports($workflow, $subject)) {
                $matched[] = $workflow;
            }
        }

        return $matched;
    }

    private function supports(WorkflowInterface $workflow, SupportedObjectInterface $supportStrategy, object $subject,
                              ?string $workflowName): bool
    {
        if (null !== $workflowName && $workflowName !== $workflow->getName()) {
            return false;
        }

        return $supportStrategy->supports($workflow, $subject);
    }

}