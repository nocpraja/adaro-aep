<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow;


use App\Component\Workflow\Blocker\TransitionBlocker;
use App\Component\Workflow\Blocker\TransitionBlockerList;
use App\Component\Workflow\DataObject\Marking;
use App\Component\Workflow\Event\CompletedEvent;
use App\Component\Workflow\Event\EnteredEvent;
use App\Component\Workflow\Event\EnterEvent;
use App\Component\Workflow\Event\GuardEvent;
use App\Component\Workflow\Event\LeaveEvent;
use App\Component\Workflow\Exception\NotEnabledTransitionException;
use App\Component\Workflow\Exception\UndefinedTransitionException;
use App\Component\Workflow\Visitor\MarkingVisitorInterface;
use App\Component\Workflow\Visitor\MarkingVisitorImpl;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Workflow Implementation
 *
 * @package App\Component\Workflow
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 02/05/2019 1:41
 */
class WorkflowImpl implements WorkflowInterface
{

    /**
     * @var Definition
     */
    private $definition;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var MarkingVisitorInterface
     */
    private $markingStep;

    /**
     * @var string
     * Workflow name
     */
    private $name;


    /**
     * WorkflowImpl constructor.
     *
     * @param Definition               $definition
     * @param MarkingVisitorInterface  $markingStep
     * @param EventDispatcherInterface $dispatcher
     * @param string                   $name
     */
    public function __construct(Definition $definition, MarkingVisitorInterface $markingStep = null,
                                EventDispatcherInterface $dispatcher = null, string $name = 'noname')
    {
        $this->definition = $definition;
        $this->markingStep = $markingStep ?: new MarkingVisitorImpl();
        $this->dispatcher = $dispatcher ?: new EventDispatcher();
        $this->name = $name;
    }

    /**
     * Fire a transition.
     *
     * @param object                      $subject        A subject
     * @param string                      $transitionName A transition
     * @param array                       $context        Additional information or context to apply
     * @param EntityManagerInterface|null $entityManager  Doctrine entity manager
     *
     * @return Marking The new Marking
     * @throws UndefinedTransitionException
     * @throws NotEnabledTransitionException
     */
    public function apply(object $subject, string $transitionName, array $context = [],
                          EntityManagerInterface $entityManager = null): Marking
    {
        $applied = false;
        $transitionBlockerList = null;
        $approvedTransitionQueue = [];
        $marking = $this->getMarking($subject);

        foreach ($this->definition->getTransitions() as $transition) {
            if ($transition->getName() !== $transitionName) {
                continue;
            }

            $transitionBlockerList = $this->buildTransitionBlockerListForTransition($subject, $marking, $transition);

            if (!$transitionBlockerList->isEmpty()) {
                continue;
            }

            $approvedTransitionQueue[] = $transition;
        }

        foreach ($approvedTransitionQueue as $transition) {
            $applied = true;
            $this->leave($subject, $transition, $marking);
//            $context = $this->transition($subject, $transition, $marking, $context);
            $this->enter($subject, $transition, $marking, $context);
            $this->markingStep->setMarking($subject, $marking, $context);
            $this->entered($subject, $marking, $transition, $entityManager, $context);
            $this->completed($subject, $transition, $marking, $context);
//            $this->announce($subject, $transition, $marking);
        }

        if (!$transitionBlockerList) {
            throw new UndefinedTransitionException($subject, $transitionName, $this);
        }
        if (!$applied) {
            throw new NotEnabledTransitionException($subject, $transitionName, $this, $transitionBlockerList);
        }

        return $marking;
    }

    /**
     * Builds a TransitionBlockerList to know why a transition is blocked.
     *
     * @param object $subject A subject
     * @param string $transitionName
     *
     * @return TransitionBlockerList
     */
    public function buildTransitionBlockerList(object $subject, string $transitionName): TransitionBlockerList
    {
        $transitions = $this->definition->getTransitions();
        $marking = $this->getMarking($subject);
        $transitionBlockerList = null;

        foreach ($transitions as $transition) {
            if ($transition->getName() !== $transitionName) {
                continue;
            }

            $transitionBlockerList = $this->buildTransitionBlockerListForTransition($subject, $marking, $transition);

            if ($transitionBlockerList->isEmpty()) {
                return $transitionBlockerList;
            }

            // We prefer to return transitions blocker by something else than
            // marking. Because it means the marking was OK. Transitions are
            // deterministic: it's not possible to have many transitions enabled
            // at the same time that match the same marking with the same name
            if (!$transitionBlockerList->has(TransitionBlocker::BLOCKED_BY_MARKING)) {
                return $transitionBlockerList;
            }
        }

        if (!$transitionBlockerList) {
            throw new UndefinedTransitionException($subject, $transitionName, $this);
        }

        return $transitionBlockerList;
    }

    /**
     * Returns true if the transition is enabled.
     *
     * @param object $subject        A subject
     * @param string $transitionName A transition
     *
     * @return bool true if the transition is enabled
     */
    public function can(object $subject, string $transitionName): bool
    {
        $transitions = $this->definition->getTransitions();
        $marking = $this->getMarking($subject);

        foreach ($transitions as $transition) {
            if ($transition->getName() !== $transitionName) {
                continue;
            }

            $transitionBlockerList = $this->buildTransitionBlockerListForTransition($subject, $marking, $transition);

            if ($transitionBlockerList->isEmpty()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get workflow configurations.
     *
     * @return Definition
     */
    public function getDefinition(): Definition
    {
        return $this->definition;
    }

    /**
     * Returns all enabled transitions.
     *
     * @param object $subject A subject
     *
     * @return Transition[] All enabled transitions
     */
    public function getEnabledTransitions(object $subject): array
    {
        $enabledTransitions = [];
        $marking = $this->getMarking($subject);

        foreach ($this->definition->getTransitions() as $transition) {
            $transitionBlockerList = $this->buildTransitionBlockerListForTransition($subject, $marking, $transition);

            if ($transitionBlockerList->isEmpty()) {
                $enabledTransitions[] = $transition;
            }
        }

        return $enabledTransitions;
    }

    /**
     * Returns the object's Marking.
     *
     * @param object $subject A subject
     *
     * @return Marking The Marking
     *
     * @throws LogicException
     */
    public function getMarking(object $subject): Marking
    {
        $marking = $this->markingStep->getMarking($subject);

        if (!$marking instanceof Marking) {
            throw new LogicException(
                sprintf('The value returned by the MarkingStep is not an instance of "%s" for workflow "%s".',
                        Marking::class, $this->name)
            );
        }

        // check if the subject is already in the workflow
        if (empty($marking->getPlaces())) {
            if (empty($this->definition->getInitialPlaces())) {
                throw new LogicException(
                    sprintf('The Marking is empty and there is no initial place for workflow "%s".',
                            $this->name)
                );
            }
            foreach ($this->definition->getInitialPlaces() as $place) {
                $marking->mark($place);
            }

            // update the subject with the new marking
            $this->markingStep->setMarking($subject, $marking);
            $this->entered($subject, $marking);
        }

        // check that the subject has a known place
        $places = $this->definition->getPlaces();

        foreach ($marking->getPlaces() as $placeName => $nbToken) {
            if (!isset($places[$placeName])) {
                $message = sprintf('Place "%s" is not valid for workflow "%s".', $placeName, $this->name);

                if (!$places) {
                    $message .= ' It seems you forgot to add places to the current workflow.';
                }

                throw new LogicException($message);
            }
        }

        return $marking;
    }

    /**
     * Workflow step marking handler.
     *
     * @return MarkingVisitorInterface
     */
    public function getMarkingVisitor(): MarkingVisitorInterface
    {
        return $this->markingStep;
    }

    /**
     * Get workflow name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    private function buildTransitionBlockerListForTransition(object $subject, Marking $marking,
                                                             Transition $transition): TransitionBlockerList
    {
        $found = false;
        foreach ($transition->getFroms() as $place) {
            if ($marking->has($place)) {
                $found = true;
                break;
            }
        }
        if ($found === false) {
            return new TransitionBlockerList([TransitionBlocker::createBlockedByMarking($marking)]);
        }

        if (null === $this->dispatcher) {
            return new TransitionBlockerList();
        }

        $event = $this->guardTransition($subject, $marking, $transition);
        if ($event->isBlocked()) {
            return $event->getTransitionBlockerList();
        }

        return new TransitionBlockerList();
    }

    private function completed($subject, Transition $transition, Marking $marking, array $context = []): void
    {
        if (null === $this->dispatcher) {
            return;
        }

        $event = new CompletedEvent($subject, $marking, $transition, $this, $context);
        $this->dispatcher->dispatch(sprintf('%s.%s', WorkflowEvents::NAME,
                                            WorkflowEvents::COMPLETED), $event);
        $this->dispatcher->dispatch(sprintf('%s.%s.%s', WorkflowEvents::NAME, $this->name,
                                            WorkflowEvents::COMPLETED), $event);
        $this->dispatcher->dispatch(sprintf('%s.%s.%s.%s', WorkflowEvents::NAME, $this->name,
                                            WorkflowEvents::COMPLETED, $transition->getName()), $event);
    }

    private function enter($subject, Transition $transition, Marking $marking, array $context = []): void
    {
        $place = $transition->getTo();

        if (null !== $this->dispatcher) {
            $event = new EnterEvent($subject, $marking, $transition, $this, $context);
            $this->dispatcher->dispatch(sprintf('%s.%s', WorkflowEvents::NAME,
                                                WorkflowEvents::ENTER), $event);
            $this->dispatcher->dispatch(sprintf('%s.%s.%s', WorkflowEvents::NAME,
                                                $this->name, WorkflowEvents::ENTER), $event);
            $this->dispatcher->dispatch(sprintf('%s.%s.%s.%s', WorkflowEvents::NAME,
                                                $this->name, WorkflowEvents::ENTER, $place), $event);
        }

        $marking->mark($place);
    }

    private function entered(object $subject, Marking $marking, Transition $transition = null,
                             EntityManagerInterface $entityManager = null, array $context = []): void
    {
        if (!is_null($entityManager) && $transition) {
            $entityManager->persist($subject);
            $entityManager->flush();
        }
        if (is_null($this->dispatcher)) {
            return;
        }

        $event = new EnteredEvent($subject, $marking, $transition, $this, $context);
        $this->dispatcher->dispatch(sprintf('%s.%s', WorkflowEvents::NAME,
                                            WorkflowEvents::ENTERED), $event);
        $this->dispatcher->dispatch(sprintf('%s.%s.%s', WorkflowEvents::NAME, $this->name,
                                            WorkflowEvents::ENTERED), $event);

        if ($transition) {
            $place = $transition->getTo();
            $this->dispatcher->dispatch(sprintf('%s.%s.%s.%s', WorkflowEvents::NAME,
                                                $this->name, WorkflowEvents::ENTERED, $place), $event);
        }
    }

    private function guardTransition(object $subject, Marking $marking, Transition $transition): ?GuardEvent
    {
        if (null === $this->dispatcher) {
            return null;
        }

        $event = new GuardEvent($subject, $marking, $transition, $this);
        $this->dispatcher->dispatch(sprintf('%s.%s', WorkflowEvents::NAME,
                                            WorkflowEvents::GUARD), $event);
        $this->dispatcher->dispatch(sprintf('%s.%s.%s', WorkflowEvents::NAME, $this->name,
                                            WorkflowEvents::GUARD), $event);
        $this->dispatcher->dispatch(sprintf('%s.%s.%s.%s', WorkflowEvents::NAME, $this->name,
                                            WorkflowEvents::GUARD, $transition->getName()), $event);

        return $event;
    }

    private function leave(object $subject, Transition $transition, Marking $marking): void
    {
        $places = $transition->getFroms();

        if (null !== $this->dispatcher) {
            $event = new LeaveEvent($subject, $marking, $transition, $this);
            $this->dispatcher->dispatch(sprintf('%s.%s', WorkflowEvents::NAME,
                                                WorkflowEvents::LEAVE), $event);
            $this->dispatcher->dispatch(sprintf('%s.%s.%s', WorkflowEvents::NAME, $this->name,
                                                WorkflowEvents::LEAVE), $event);

            foreach ($places as $place) {
                $this->dispatcher->dispatch(sprintf('%s.%s.%s.%s', WorkflowEvents::NAME, $this->name,
                                                    WorkflowEvents::LEAVE, $place), $event);
            }
        }

        foreach ($places as $place) {
            $marking->unmark($place);
        }
    }

}