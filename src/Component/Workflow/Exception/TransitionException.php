<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Exception;


use App\Component\Workflow\WorkflowInterface;
use LogicException;

/**
 * Class TransitionException
 *
 * @package App\Component\Workflow\Exception
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 2:14
 */
class TransitionException extends LogicException
{

    /**
     * @var object
     */
    private $subject;

    /**
     * @var string
     */
    private $transitionName;

    /**
     * @var WorkflowInterface
     */
    private $workflow;


    /**
     * TransitionException constructor.
     *
     * @param object            $subject
     * @param string            $transitionName
     * @param WorkflowInterface $workflow
     * @param string            $message
     */
    public function __construct(object $subject, string $transitionName, WorkflowInterface $workflow, string $message)
    {
        parent::__construct($message);
        $this->subject = $subject;
        $this->transitionName = $transitionName;
        $this->workflow = $workflow;
    }

    /**
     * @return object
     */
    public function getSubject(): object
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getTransitionName(): string
    {
        return $this->transitionName;
    }

    /**
     * @return WorkflowInterface
     */
    public function getWorkflow(): WorkflowInterface
    {
        return $this->workflow;
    }

}