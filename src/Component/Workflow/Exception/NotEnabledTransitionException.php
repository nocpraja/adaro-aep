<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Exception;


use App\Component\Workflow\Blocker\TransitionBlockerList;
use App\Component\Workflow\WorkflowInterface;

/**
 * Class NotEnabledTransitionException
 *
 * @package App\Component\Workflow\Exception
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 2:40
 */
class NotEnabledTransitionException extends TransitionException
{

    /**
     * @var TransitionBlockerList
     */
    private $transitionBlockerList;


    /**
     * NotEnabledTransitionException constructor.
     *
     * @param object                $subject
     * @param string                $transitionName
     * @param WorkflowInterface     $workflow
     * @param TransitionBlockerList $transitionBlockerList
     */
    public function __construct(object $subject, string $transitionName, WorkflowInterface $workflow,
                                TransitionBlockerList $transitionBlockerList)
    {
        parent::__construct($subject, $transitionName, $workflow,
                            sprintf('Transition "%s" is not enabled for workflow "%s".',
                                    $transitionName, $workflow->getName())
        );

        $this->transitionBlockerList = $transitionBlockerList;
    }

    public function getTransitionBlockerList(): TransitionBlockerList
    {
        return $this->transitionBlockerList;
    }

}