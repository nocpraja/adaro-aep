<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Exception;


use App\Component\Workflow\WorkflowInterface;

/**
 * Class UndefinedTransitionException
 *
 * @package App\Component\Workflow\Exception
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 2:14
 */
class UndefinedTransitionException extends TransitionException
{

    /**
     * UndefinedTransitionException constructor.
     *
     * @param object            $subject
     * @param string            $transitionName
     * @param WorkflowInterface $workflow
     */
    public function __construct(object $subject, string $transitionName, WorkflowInterface $workflow)
    {
        parent::__construct($subject, $transitionName, $workflow,
                            sprintf('Transition "%s" is not defined for workflow "%s".',
                                    $transitionName, $workflow->getName())
        );
    }

}