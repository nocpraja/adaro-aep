<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Visitor;


use App\Component\Workflow\DataObject\Marking;
use BadMethodCallException;

/**
 * Class MarkingVisitorImpl
 *
 * @package App\Component\Workflow\Visitor
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 02/05/2019 1:42
 */
final class MarkingVisitorImpl implements MarkingVisitorInterface
{

    /***
     * @var string
     */
    private $property;


    /**
     * MarkObjectState constructor.
     *
     * @param string $property
     */
    public function __construct(string $property = 'marking')
    {
        $this->property = $property;
    }

    /**
     * @inheritDoc
     *
     * @throws BadMethodCallException If getter method for marking fieldname doesn't exist
     */
    public function getMarking(object $subject): Marking
    {
        $method = sprintf("get%s", ucfirst($this->property));

        if (!method_exists($subject, $method)) {
            throw new BadMethodCallException(sprintf('The method "%s::%s()" does not exists.',
                                                     get_class($subject), $method));
        }

        $marking = $subject->{$method}();

        if (!$marking) {
            return new Marking();
        }

        return new Marking([$marking => 1]);
    }

    /**
     * @inheritDoc
     *
     * @throws BadMethodCallException If setter method for marking fieldname doesn't exist
     */
    public function setMarking(object $subject, Marking $marking, array $context = []): void
    {
        $place = key($marking->getPlaces());
        $method = sprintf("set%s", ucfirst($this->property));

        if (!method_exists($subject, $method)) {
            throw new BadMethodCallException(sprintf('The method "%s::%s()" does not exists.',
                                                     get_class($subject), $method));
        }

        $subject->{$method}($place, $context);
    }

}