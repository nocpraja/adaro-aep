<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Visitor;


use App\Component\Workflow\DataObject\Marking;
use BadMethodCallException;

/**
 * Interface MarkingVisitorInterface
 *
 * @package App\Component\Workflow\Visitor
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 02/05/2019 1:41
 */
interface MarkingVisitorInterface
{
    /**
     * Get a Marking from a given subject.
     *
     * @param object $subject An object to get the marking state
     *
     * @return Marking
     * @throws BadMethodCallException If getter method for marking fieldname doesn't exist
     */
    public function getMarking(object $subject): Marking;

    /**
     * Set a Marking to a given subject.
     *
     * @param object  $subject An object to marked
     * @param Marking $marking The marking state
     * @param array   $context Additional context to apply
     *
     * @throws BadMethodCallException If setter method for marking fieldname doesn't exist
     */
    public function setMarking(object $subject, Marking $marking, array $context = []): void;

}