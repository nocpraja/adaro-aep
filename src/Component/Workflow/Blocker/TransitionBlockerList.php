<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Blocker;


use ArrayIterator;
use Countable;
use IteratorAggregate;

/**
 * Class TransitionBlockerList
 *
 * @package App\Component\Workflow\Blocker
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 1:35
 */
final class TransitionBlockerList implements IteratorAggregate, Countable
{

    /**
     * @var TransitionBlocker[]
     */
    private $blockers;


    /**
     * TransitionBlockerList constructor.
     *
     * @param TransitionBlocker[] $blockers
     */
    public function __construct(array $blockers = [])
    {
        $this->blockers = [];

        foreach ($blockers as $blocker) {
            $this->add($blocker);
        }
    }

    public function add(TransitionBlocker $blocker): void
    {
        $this->blockers[] = $blocker;
    }

    public function has(string $code): bool
    {
        foreach ($this->blockers as $blocker) {
            if ($code === $blocker->getCode()) {
                return true;
            }
        }

        return false;
    }

    public function clear(): void
    {
        $this->blockers = [];
    }

    public function isEmpty(): bool
    {
        return !$this->blockers;
    }

    /**
     * @inheritDoc
     *
     * @return ArrayIterator|TransitionBlocker[]
     */
    public function getIterator()
    {
        return new ArrayIterator($this->blockers);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->blockers);
    }

}