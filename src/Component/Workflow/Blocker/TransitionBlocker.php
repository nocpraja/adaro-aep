<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Blocker;


use App\Component\Workflow\DataObject\Marking;

/**
 * Class TransitionBlocker
 *
 * @package App\Component\Workflow\Blocker
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 1:35
 */
final class TransitionBlocker
{

    const BLOCKED_BY_MARKING = '19beefc8-6b1e-4716-9d07-a39bd6d16e34';
    const BLOCKED_BY_EXPRESSION_GUARD_LISTENER = '326a1e9c-0c12-11e8-ba89-0ed5f89f718b';
    const UNKNOWN = 'e8b5bbb9-5913-4b98-bfa6-65dbd228a82a';

    private $message;
    private $code;
    private $context;


    /**
     * TransitionBlocker constructor.
     *
     * @param string $message    Blocked message
     * @param string $code       Code is a machine-readable string, usually an UUID
     * @param array  $context    This is useful if you would like to pass around the condition values, that
     *                           blocked the transition. E.g. for a condition "distance must be larger than
     *                           5 miles", you might want to pass around the value of 5.
     */
    public function __construct(string $message, string $code, array $context = [])
    {
        $this->message = $message;
        $this->code = $code;
        $this->context = $context;
    }

    /**
     * Create a blocker that says the transition cannot be made because it is
     * not enabled.
     *
     * It means the subject is in wrong place (i.e. status):
     * * If the workflow is a state machine: the subject is not in the previous place of the transition.
     * * If the workflow is a workflow: the subject is not in all previous places of the transition.
     *
     * @param Marking $marking
     *
     * @return TransitionBlocker
     */
    public static function createBlockedByMarking(Marking $marking): self
    {
        return new static('The marking does not enable the transition.',
                          self::BLOCKED_BY_MARKING, ['marking' => $marking]);
    }

    /**
     * Creates a blocker that says the transition cannot be made because it has
     * been blocked by the expression guard listener.
     *
     * @param string $expression
     *
     * @return TransitionBlocker
     */
    public static function createBlockedByExpressionGuardListener(string $expression): self
    {
        return new static('The expression blocks the transition.',
                          self::BLOCKED_BY_EXPRESSION_GUARD_LISTENER, ['expression' => $expression]);
    }

    /**
     * Creates a blocker that says the transition cannot be made because of an
     * unknown reason.
     *
     * This blocker code is chiefly for preserving backwards compatibility.
     */
    public static function createUnknown(): self
    {
        return new static('Unknown reason.', self::UNKNOWN);
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getContext(): array
    {
        return $this->context;
    }

}