<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Event;


use App\Component\Workflow\DataObject\Marking;
use App\Component\Workflow\Transition;
use App\Component\Workflow\WorkflowInterface;
use Symfony\Component\EventDispatcher\Event as BaseEvent;

/**
 * Class Event
 *
 * @package App\Component\Workflow\Event
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 27/04/2019 16:22
 */
class Event extends BaseEvent
{

    /**
     * @var object
     */
    private $subject;

    /**
     * @var Marking
     */
    private $marking;

    /**
     * @var Transition
     */
    private $transition;

    /**
     * @var WorkflowInterface
     */
    private $workflow;

    /**
     * @var array
     */
    private $context = [];


    /**
     * Event constructor.
     *
     * @param object            $subject
     * @param Marking           $marking
     * @param Transition        $transition
     * @param WorkflowInterface $workflow
     * @param array             $context
     */
    public function __construct(object $subject, Marking $marking, Transition $transition,
                                WorkflowInterface $workflow, array $context = [])
    {
        $this->subject = $subject;
        $this->marking = $marking;
        $this->transition = $transition;
        $this->workflow = $workflow;
        $this->context = $context;
    }

    /**
     * @return Marking
     */
    public function getMarking(): Marking
    {
        return $this->marking;
    }

    /**
     * @return object
     */
    public function getSubject(): object
    {
        return $this->subject;
    }

    /**
     * Get current transition step.
     *
     * @return Transition
     */
    public function getTransition(): Transition
    {
        return $this->transition;
    }

    /**
     * Get current workflow.
     *
     * @return WorkflowInterface
     */
    public function getWorkflow(): WorkflowInterface
    {
        return $this->workflow;
    }

    /**
     * Get the workflow name.
     *
     * @return string
     */
    public function getWorkflowName(): string
    {
        return $this->workflow->getName();
    }

    /**
     * Get additional information or context.
     *
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

}