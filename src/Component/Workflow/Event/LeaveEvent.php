<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Event;


/**
 * Class LeaveEvent
 *
 * @package App\Component\Workflow\Event
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 20:17
 */
final class LeaveEvent extends Event
{
}