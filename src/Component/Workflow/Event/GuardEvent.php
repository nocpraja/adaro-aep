<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Event;


use App\Component\Workflow\Blocker\TransitionBlocker;
use App\Component\Workflow\Blocker\TransitionBlockerList;
use App\Component\Workflow\DataObject\Marking;
use App\Component\Workflow\Transition;
use App\Component\Workflow\WorkflowInterface;

/**
 * Class GuardEvent
 *
 * @package App\Component\Workflow\Event
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 2:00
 */
final class GuardEvent extends Event
{

    /**
     * @var TransitionBlockerList
     */
    private $transitionBlockerList;


    /**
     * GuardEvent constructor.
     *
     * @param object            $subject
     * @param Marking           $marking
     * @param Transition        $transition
     * @param WorkflowInterface $workflow
     */
    public function __construct(object $subject, Marking $marking, Transition $transition, WorkflowInterface $workflow)
    {
        parent::__construct($subject, $marking, $transition, $workflow);
        $this->transitionBlockerList = new TransitionBlockerList();
    }

    public function isBlocked(): bool
    {
        return !$this->transitionBlockerList->isEmpty();
    }

    public function setBlocked(bool $blocked): void
    {
        if (!$blocked) {
            $this->transitionBlockerList->clear();
            return;
        }

        $this->transitionBlockerList->add(TransitionBlocker::createUnknown());
    }

    public function getTransitionBlockerList(): TransitionBlockerList
    {
        return $this->transitionBlockerList;
    }

    public function addTransitionBlocker(TransitionBlocker $transitionBlocker): void
    {
        $this->transitionBlockerList->add($transitionBlocker);
    }

}