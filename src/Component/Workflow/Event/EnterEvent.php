<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Event;


/**
 * Class EnterEvent
 *
 * @package App\Component\Workflow\Event
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 3:03
 */
final class EnterEvent extends Event
{
}