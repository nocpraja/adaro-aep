<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\Event;


/**
 * Class TransitionEvent
 *
 * @package App\Component\Workflow\Event
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 2:56
 */
final class TransitionEvent extends Event
{
    private $context;

    public function setContext(array $context)
    {
        $this->context = $context;
    }

    public function getContext(): array
    {
        return $this->context;
    }

}