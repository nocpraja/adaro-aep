<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow;


/**
 * Class DefinitionBuilder
 *
 * @package App\Component\Workflow
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 4:26
 */
final class DefinitionBuilder
{

    /**
     * @var string[]
     */
    private $places = [];

    /**
     * @var Transition[]
     */
    private $transitions = [];

    /**
     * @var string
     */
    private $initialPlace;


    /**
     * DefinitionBuilder constructor.
     *
     * @param string[]     $places      collection of workflow state
     * @param Transition[] $transitions Collection of workflow transition step
     */
    public function __construct(array $places = [], array $transitions = [])
    {
        $this->addPlaces($places);
        $this->addTransitions($transitions);
    }

    /**
     * Register workflow state.
     *
     * @param string $place Workflow state
     *
     * @return DefinitionBuilder
     */
    public function addPlace(string $place): self
    {
        if (!count($this->places)) {
            $this->initialPlace = $place;
        }

        $this->places[$place] = $place;

        return $this;
    }

    /**
     * Register collection of workflow state.
     *
     * @param string[] $places Workflow state
     *
     * @return DefinitionBuilder
     */
    public function addPlaces(array $places): self
    {
        foreach ($places as $place) {
            $this->addPlace($place);
        }

        return $this;
    }

    /**
     * Register a workflow transition step.
     *
     * @param Transition $transition Transition step
     *
     * @return DefinitionBuilder
     */
    public function addTransition(Transition $transition): self
    {
        $this->transitions[] = $transition;

        return $this;
    }

    /**
     * Register collection of workflow transition step.
     *
     * @param Transition[] $transitions Workflow transition steps
     *
     * @return DefinitionBuilder
     */
    public function addTransitions(array $transitions): self
    {
        foreach ($transitions as $transition) {
            $this->addTransition($transition);
        }

        return $this;
    }

    /**
     * Build workflow configurations.
     *
     * @return Definition
     */
    public function build(): Definition
    {
        return new Definition($this->places, $this->transitions, $this->initialPlace);
    }

    /**
     * Clear all data in the builder.
     *
     * @return DefinitionBuilder
     */
    public function clear(): self
    {
        $this->places = [];
        $this->transitions = [];
        $this->initialPlace = null;

        return $this;
    }

    /**
     * Register workflow initial state.
     *
     * @param string $place Any valid state
     *
     * @return DefinitionBuilder
     */
    public function setInitialPlace(string $place): self
    {
        $this->initialPlace = $place;

        return $this;
    }

}