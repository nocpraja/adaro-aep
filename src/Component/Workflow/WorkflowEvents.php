<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow;

/**
 * Class WorkflowEvents
 *
 * @package App\Component\Workflow
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 26/04/2019 16:25
 */
final class WorkflowEvents
{
    const NAME = 'app.workflow';
    const COMPLETED = 'completed';
    const ENTER = 'enter';
    const ENTERED = 'entered';
    const GUARD = 'guard';
    const LEAVE = 'leave';


    private function __construct()
    {
    }

    private function __clone()
    {
    }

}