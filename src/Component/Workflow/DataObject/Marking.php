<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\DataObject;

/**
 * Class Marking is used to store places of every tokens.
 *
 * @package App\Component\Workflow\DataObject
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 25/04/2019 21:59
 */
final class Marking
{

    private $places = [];

    /**
     * Marking constructor.
     *
     * @param string[] $presentations Array of place names
     */
    public function __construct(array $presentations = [])
    {
        foreach ($presentations as $place => $token) {
            $this->mark($place);
        }
    }

    public function mark(string $place): void
    {
        $this->places[$place] = 1;
    }

    public function unmark(string $place): void
    {
        unset($this->places[$place]);
    }

    public function has(string $place): bool
    {
        return isset($this->places[$place]);
    }

    public function getPlaces(): array
    {
        return $this->places;
    }

}