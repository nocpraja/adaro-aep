<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\DataObject;


use App\Component\Workflow\WorkflowInterface;

/**
 * Interface SupportedObjectInterface
 *
 * @package App\Component\Workflow\DataObject
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 26/04/2019 3:58
 */
interface SupportedObjectInterface
{
    /**
     * Check if data object class is supported or not.
     *
     * @param WorkflowInterface $workflow The workflow instance
     * @param object            $subject  Data object classname
     *
     * @return bool
     */
    public function supports(WorkflowInterface $workflow, object $subject): bool;

}