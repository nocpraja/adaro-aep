<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow\DataObject;

use App\Component\Workflow\WorkflowInterface;

/**
 * Class SupportedObject
 *
 * @package App\Component\Workflow\DataObject
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 26/04/2019 3:59
 */
final class SupportedObject implements SupportedObjectInterface
{

    /**
     * @var string
     */
    private $className;


    /**
     * SupportedObject constructor.
     *
     * @param string $classname Data object classname
     */
    public function __construct(string $classname)
    {
        $this->className = $classname;
    }

    /**
     * Get data object classname.
     *
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * Check if data object class is supported or not.
     *
     * @param WorkflowInterface $workflow The workflow instance
     * @param object            $subject  Data object classname
     *
     * @return bool
     */
    public function supports(WorkflowInterface $workflow, object $subject): bool
    {
        return $subject instanceof $this->className;
    }

}