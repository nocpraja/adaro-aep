<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Workflow;


use LogicException;

/**
 * Class Definition
 *
 * @package App\Component\Workflow
 * @author  Ahmad Fajar
 * @since   25/04/2019, modified: 26/04/2019 1:01
 */
final class Definition
{

    /**
     * @var string[]
     */
    private $places = [];

    /**
     * @var Transition[]
     */
    private $transitions = [];

    /**
     * @var string[]
     */
    private $initialPlaces = [];


    /**
     * Definition constructor.
     *
     * @param string[]        $places
     * @param Transition[]    $transitions
     * @param string|string[] $initialPlaces
     */
    public function __construct(array $places, array $transitions, $initialPlaces = null)
    {
        foreach ($places as $place) {
            $this->addPlace($place);
        }
        foreach ($transitions as $transition) {
            $this->addTransition($transition);
        }

        $this->setInitialPlaces($initialPlaces);
    }

    /**
     * Get collection of valid places.
     *
     * @return string[]
     */
    public function getPlaces(): array
    {
        return $this->places;
    }

    /**
     * Get collection of workflow transitions.
     *
     * @return Transition[]
     */
    public function getTransitions(): array
    {
        return $this->transitions;
    }

    /**
     * Get collection of valid initial places.
     *
     * @return string[]
     */
    public function getInitialPlaces(): array
    {
        return $this->initialPlaces;
    }

    private function addPlace(string $place): void
    {
        if (!count($this->places)) {
            $this->initialPlaces = [$place];
        }

        $this->places[$place] = $place;
    }

    private function addTransition(Transition $transition): void
    {
        $name = $transition->getName();

        foreach ($transition->getFroms() as $from) {
            if (!isset($this->places[$from])) {
                throw new LogicException(sprintf('Place "%s" referenced in transition "%s" does not exist.',
                                                 $from, $name));
            }
        }
        if (!isset($this->places[$transition->getTo()])) {
            throw new LogicException(sprintf('Place "%s" referenced in transition "%s" does not exist.',
                                             $transition->getTo(), $name));
        }

        $this->transitions[] = $transition;
    }

    /**
     * @param string|string[] $places
     */
    private function setInitialPlaces($places = null): void
    {
        if (!$places) {
            return;
        }

        $places = (array)$places;

        foreach ($places as $place) {
            if (!isset($this->places[$place])) {
                throw new LogicException(sprintf('Place "%s" cannot be the initial place as it does not exist.',
                                                 $place));
            }
        }

        $this->initialPlaces = $places;
    }

}