<?php

/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Util;


use App\Component\Uuid\Generator\RandomGeneratorFactory;
use Symfony\Component\Security\Csrf\CsrfToken;

/**
 * Class TokenGenerator digunakan untuk membuat secure Token. Berbeda dengan Symfony CSRF Token generator,
 * karena token yang dibuat oleh Class ini tidak disimpan di memory ataupun session. Sehingga setiap kali
 * melakukan generate token akan mendapatkan hasil yang berbeda.
 *
 * @package App\Component\Util
 * @author  Ahmad Fajar
 * @since   2019-04-14, modified: 2019-04-15 11:35
 */
final class TokenGenerator
{

    /**
     * @var RandomGeneratorFactory
     */
    private $generator;

    /**
     * Hash algorithm to be used.
     *
     * @var string
     * @see https://www.php.net/manual/en/function.hash.php hash function
     */
    private $algorithm = 'whirlpool';

    /**
     * @var int
     */
    private $entropy;


    /**
     * TokenGenerator constructor.
     *
     * @param int $entropy The amount of entropy collected for each token (in bits)
     */
    public function __construct(int $entropy = 256)
    {
        $this->entropy = $entropy;
        $this->generator = RandomGeneratorFactory::create();
    }

    /**
     * Generates token.
     *
     * @param string $subject Token subject used to increase security
     *
     * @return CsrfToken
     */
    public function generateToken(string $subject): CsrfToken
    {
        $bytes = $this->generator->generate($this->entropy / 8);
        $token = $this->uriSafe($bytes);
        $value = $this->generateHash($subject, $token);

        return new CsrfToken($token, $value);
    }

    /**
     * Gets token Hash algorithm to be used.
     *
     * @return string Hash algorithm
     */
    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    /**
     * Sets Hash algorithm to be used.
     *
     * @param string $algorithm
     *
     * @return TokenGenerator
     * @see https://www.php.net/manual/en/function.hash.php hash function
     */
    public function setAlgorithm(string $algorithm): self
    {
        $this->algorithm = $algorithm;

        return $this;
    }

    /**
     * Returns whether the given token is valid or not.
     *
     * @param string    $subject Token subject used when making tokens
     * @param CsrfToken $token   The token to check
     *
     * @return bool TRUE if the token is valid, FALSE otherwise
     */
    public function isTokenValid(string $subject, CsrfToken $token): bool
    {
        $value1 = $token->getValue();
        $value2 = $this->generateHash($subject, $token->getId());

        return hash_equals($value1, $value2);
    }

    /**
     * Generate token hash value.
     *
     * @param string $subject Token subject used to increase security
     * @param string $key     The token ID
     *
     * @return string
     */
    private function generateHash(string $subject, string $key): string
    {
        $salted = $subject . '::' . $key;
        $digest = hash($this->algorithm, $salted, true);

        // "stretch" the hash to increase security
        for ($i = 1; $i < 200; $i++) {
            $digest = hash($this->algorithm, $digest . $salted, true);
        }

        return $this->uriSafe($digest);
    }

    private function uriSafe(string $bytes): string
    {
        return rtrim(strtr(base64_encode($bytes), '+/', '-_'), '=');
    }

}