<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Util;

/**
 * Class LonLatUtil
 *
 * @package App\Component\Util
 * @author  Ahmad Fajar
 * @since   31/10/2018, modified: 29/01/2019 2:57
 */
final class LonLatUtil
{

    /**
     * Create SQL expression from a <var>LonLat</var> value into a data with <var>Geometry::POINT</var> type.
     *
     * @param float  $longitude The longitude value
     * @param float  $latitude  The latitude value
     * @param string $srid      The SRID value to be used
     *
     * @return string The SQL expression
     */
    public static function lonlatToPoint($longitude, $latitude, $srid = '4326'): string
    {
        return sprintf('ST_SetSRID(ST_Point(%s, %s), %s)', $longitude, $latitude, $srid);
    }

    /**
     * Create WKT formatted text from a <var>LonLat</var> value.
     *
     * @param float  $longitude The longitude value
     * @param float  $latitude  The latitude value
     * @param string $srid      The SRID value to be used
     *
     * @return string The WKT formatted text
     */
    public static function lonlatToWkt($longitude, $latitude, $srid = '4326'): string
    {
        return sprintf('SRID=%s;POINT(%s %s)', $srid, $longitude, $latitude);
    }

}