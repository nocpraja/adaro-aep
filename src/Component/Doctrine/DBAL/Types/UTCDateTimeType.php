<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Doctrine\DBAL\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeType;

/**
 * Class UTCDateTimeType
 *
 * @package App\Component\Doctrine\DBAL\Types
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 24/07/2018 11:34
 */
class UTCDateTimeType extends DateTimeType
{
    private static $utc;

    /**
     * Converts a value from its PHP representation to its database representation of this type.
     *
     * @param \DateTimeInterface|null $value    The value to convert
     * @param AbstractPlatform        $platform The currently used database platform
     *
     * @return null|string            The database representation of the value
     * @throws \Doctrine\DBAL\Types\ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (is_null(self::$utc)) {
            self::$utc = new \DateTimeZone('UTC');
        }
        if ($value instanceof \DateTime) {
            $value->setTimezone(self::$utc);
        }

        return parent::convertToDatabaseValue($value, $platform);
    }

    /**
     * Converts a value from its database representation to its PHP representation of this type.
     *
     * @param mixed|null       $value    The value to convert
     * @param AbstractPlatform $platform The currently used database platform
     *
     * @return \DateTimeInterface|null   The PHP representation of the value
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?\DateTimeInterface
    {
        if (null === $value || $value instanceof \DateTimeInterface) {
            return $value;
        }

        if (is_null(self::$utc)) {
            self::$utc = new \DateTimeZone('UTC');
        }

        $converted = \DateTime::createFromFormat($platform->getDateTimeFormatString(), $value, self::$utc);

        if (!$converted) {
            $converted = date_create($value, self::$utc);
        }
        if (!$converted) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(),
                                                              $platform->getDateTimeFormatString());
        }

        return $converted;
    }
}