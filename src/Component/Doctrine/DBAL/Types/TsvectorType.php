<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Doctrine\DBAL\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class TsvectorType
 *
 * @package App\Component\Doctrine\DBAL\Types
 * @author  Ahmad Fajar
 * @since   17/08/2018, modified: 17/08/2018 04:33
 */
class TsvectorType extends Type
{
    /**
     * @inheritdoc
     */
    public function canRequireSQLConversion(): bool
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        # tsvector doesn't store the exact content,
        # here we strip down the un-needed characters from the content
        if (is_array($value)) {
            $result = '';

            foreach ($value as $item) {
                if (is_array($item)) {
                    $tmp = implode(' ', $item);
                    $result .= ' ' . $tmp;
                } else {
                    $result .= (!is_null($item) ? ' ' . $item : '');
                }
            }

            return str_replace(["\r", "\n", "\r\n", "\f", "\0", "\x0B"], "", $result);
        }

        return str_replace(["\r", "\n", "\r\n", "\f", "\0", "\x0B"], "", $value);
    }

    /**
     * @inheritdoc
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform): string
    {
        return sprintf('to_tsvector(%s)', $sqlExpr);
    }

    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return 'tsvector';
    }

    /**
     * @inheritdoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'TSVECTOR';
    }
}