<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Doctrine\ORM\Functions;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\InputParameter;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class DatePart is custom Doctrine ORM function that implement sql DATE_PART function.
 *
 * ```
 * DatePartFunction ::= DATE_PART("pattern", datetime)
 * ```
 *
 * @package App\Component\Doctrine\ORM\Functions
 * @author  Ahmad Fajar
 * @since   31/10/2018, modified: 29/01/2019 2:54
 */
final class DatePartFunction extends FunctionNode
{

    /**
     * @var InputParameter
     */
    private $dateExpression = null;

    /**
     * @var InputParameter
     */
    private $patternExpression = null;


    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     * @throws \Doctrine\ORM\Query\AST\ASTException
     */
    public function getSql(SqlWalker $sqlWalker): string
    {
        return
            'DATE_PART(' . $sqlWalker->walkStringPrimary($this->patternExpression) .
            ', ' . $this->dateExpression->dispatch($sqlWalker) . ')';
    }

    /**
     * @param Parser $parser
     *
     * @return void
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->patternExpression = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->dateExpression = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}