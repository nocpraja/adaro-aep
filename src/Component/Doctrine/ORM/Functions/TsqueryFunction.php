<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Doctrine\ORM\Functions;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\InputParameter;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class TsqueryFunction is custom Doctrine ORM function that implement postgresql fulltext search function.
 *
 * ```
 * TsqueryFunction ::= TSQUERY(fieldname, :keywords)
 * ```
 *
 * @package App\Component\Doctrine\ORM\Functions
 * @author  Ahmad Fajar
 * @since   31/10/2018, modified: 29/01/2019 2:55
 */
final class TsqueryFunction extends FunctionNode
{
    /**
     * @var InputParameter
     */
    private $config = null;

    /**
     * @var InputParameter
     */
    private $fieldName = null;

    /**
     * @var InputParameter
     */
    private $queryString = null;


    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     * @throws \Doctrine\ORM\Query\AST\ASTException
     */
    public function getSql(SqlWalker $sqlWalker): string
    {
        if ($this->config) {
            $stmt = $this->fieldName->dispatch($sqlWalker)
                . ' @@ to_tsquery(' . $sqlWalker->walkStringPrimary($this->config)
                . ', ' . $this->queryString->dispatch($sqlWalker) . ')';
        } else {
            $stmt = $this->fieldName->dispatch($sqlWalker) .
                ' @@ to_tsquery(' . $this->queryString->dispatch($sqlWalker) . ')';
        }

        return $stmt;
    }

    /**
     * @param Parser $parser
     *
     * @return void
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->fieldName = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->queryString = $parser->StringPrimary();
        $lexer = $parser->getLexer();

        if ($lexer->isNextToken(Lexer::T_COMMA)) {
            $parser->match(Lexer::T_COMMA);
            $this->config = $parser->StringPrimary();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

}