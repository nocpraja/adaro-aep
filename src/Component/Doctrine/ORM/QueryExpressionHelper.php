<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Doctrine\ORM;


use App\Component\DataObject\SortOrFilter;
use App\Component\Uuid\Generator\RandomGeneratorFactory;
use App\Component\Uuid\Generator\RandomGeneratorInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;


/**
 * Class QueryExpressionHelper
 *
 * @package App\Component\Doctrine\ORM
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 01/04/2019 15:46
 */
final class QueryExpressionHelper
{

    /**
     * @var RandomGeneratorInterface|null
     */
    private static $generator = null;


    /**
     * QueryExpressionHelper class constructor.
     */
    private function __construct()
    {
    }

    /**
     * Builds and add 'OR' condition to the query expression.
     *
     * @param QueryBuilder $qb       The doctrine query builder
     * @param Orx          $orX      The OrX reference
     * @param SortOrFilter $criteria Filter criteria
     *
     * @return Parameter|null
     */
    public static function addOrWhere(QueryBuilder $qb, Orx $orX, SortOrFilter $criteria): ?Parameter
    {
        $operator = strtolower($criteria->getOperator());
        $field = $criteria->getProperty();
        $value = $criteria->getValue();
        $property = strrchr($field, '.');
        $shortField = $property === false ? $field : ltrim($property, "\0. ");
        $name = 'fld' . self::createRandom($shortField);

        if ($operator == 'in' && !empty($value)) {
            if (is_array($value)) {
                $orX->add($qb->expr()->in($field, $value));
            } else {
                $_array = explode(',', $value);
                $orX->add($qb->expr()->in($field, $_array));
            }
        } elseif ($operator == 'notin' && !empty($value)) {
            if (is_array($value)) {
                $orX->add($qb->expr()->notIn($field, $value));
            } else {
                $_binder = explode(',', $value);
                $orX->add($qb->expr()->notIn($field, $_binder));
            }
        } elseif ($operator == 'startwith' && !is_null($value) && $value != '') {
            $orX->add($qb->expr()->like($qb->expr()->lower($field), ':' . $name));
            return new Parameter($name, strtolower($value) . '%');
        } elseif ($operator == 'endwith' && !is_null($value) && $value != '') {
            $orX->add($qb->expr()->like($qb->expr()->lower($field), ':' . $name));
            return new Parameter($name, '%' . strtolower($value));
        } elseif (($operator == 'like' || $operator == 'contains') && !is_null($value) && $value != '') {
            $orX->add($qb->expr()->like($qb->expr()->lower($field), ':' . $name));
            return new Parameter($name, '%' . strtolower($value) . '%');
        } elseif ($operator == '>' || $operator == 'gt') {
            $orX->add($qb->expr()->gt($field, ':' . $name));
            return new Parameter($name, $value);
        } elseif ($operator == '<' || $operator == 'lt') {
            $orX->add($qb->expr()->lt($field, ':' . $name));
            return new Parameter($name, $value);
        } elseif ($operator == '>=' || $operator == 'gte') {
            $orX->add($qb->expr()->gte($field, ':' . $name));
            return new Parameter($name, $value);
        } elseif ($operator == '<=' || $operator == 'lte') {
            $orX->add($qb->expr()->lte($field, ':' . $name));
            return new Parameter($name, $value);
        } elseif ($operator == '=' || $operator == '==' || $operator == 'eq') {
            if ($value === null || $value === 'null') {
                $orX->add($qb->expr()->isNull($field));
            } else {
                $orX->add($qb->expr()->eq($field, ':' . $name));
                return new Parameter($name, $value);
            }
        } elseif ($operator == '!=' || $operator == 'neq') {
            if ($value === null || $value === 'null') {
                $orX->add($qb->expr()->isNotNull($field));
            } else {
                $orX->add($qb->expr()->neq($field, ':' . $name));
                return new Parameter($name, $value);
            }
        } elseif ($operator == 'tsquery' || $operator == 'fts') {
            $tsquery = new Func('plainto_tsquery', [$field, ':' . $name]);
            $orX->add($qb->expr()->eq($tsquery, 'true'));
            return new Parameter($name, $value);
        }

        return null;
    }

    /**
     * Builds and add 'AND' condition to the query expression.
     *
     * @param QueryBuilder $qb   The doctrine query builder
     * @param Andx         $andX The AndX reference
     * @param SortOrFilter $criteria
     *
     * @return Parameter|null
     */
    public static function addWhere(QueryBuilder $qb, Andx $andX, SortOrFilter $criteria): ?Parameter
    {
        $operator = strtolower($criteria->getOperator());
        $field = $criteria->getProperty();
        $value = $criteria->getValue();
        $property = strrchr($field, '.');
        $shortField = $property === false ? $field : ltrim($property, "\0. ");
        $name = 'fld' . self::createRandom($shortField);

        if ($operator == 'in' && !empty($value)) {
            if (is_array($value)) {
                $andX->add($qb->expr()->in($field, $value));
            } else {
                $_binder = explode(',', $value);
                $andX->add($qb->expr()->in($field, $_binder));
            }
        } elseif ($operator == 'notin' && !empty($value)) {
            if (is_array($value)) {
                $andX->add($qb->expr()->notIn($field, $value));
            } else {
                $_binder = explode(',', $value);
                $andX->add($qb->expr()->notIn($field, $_binder));
            }
        } elseif ($operator == 'startwith' && !is_null($value) && $value != '') {
            $andX->add($qb->expr()->like($qb->expr()->lower($field), ':' . $name));
            return new Parameter($name, strtolower($value) . '%');
        } elseif ($operator == 'endwith' && !is_null($value) && $value != '') {
            $andX->add($qb->expr()->like($qb->expr()->lower($field), ':' . $name));
            return new Parameter($name, '%' . strtolower($value));
        } elseif (($operator == 'like' || $operator == 'contains') && !is_null($value) && $value != '') {
            $andX->add($qb->expr()->like($qb->expr()->lower($field), ':' . $name));
            return new Parameter($name, '%' . strtolower($value) . '%');
        } elseif ($operator == '>' || $operator == 'gt') {
            $andX->add($qb->expr()->gt($field, ':' . $name));
            return new Parameter($name, $value);
        } elseif ($operator == '<' || $operator == 'lt') {
            $andX->add($qb->expr()->lt($field, ':' . $name));
            return new Parameter($name, $value);
        } elseif ($operator == '>=' || $operator == 'gte') {
            $andX->add($qb->expr()->gte($field, ':' . $name));
            return new Parameter($name, $value);
        } elseif ($operator == '<=' || $operator == 'lte') {
            $andX->add($qb->expr()->lte($field, ':' . $name));
            return new Parameter($name, $value);
        } elseif ($operator == '=' || $operator == '==' || $operator == 'eq') {
            if ($value === null || $value === 'null') {
                $andX->add($qb->expr()->isNull($field));
            } else {
                $andX->add($qb->expr()->eq($field, ':' . $name));
                return new Parameter($name, $value);
            }
        } elseif ($operator == '!=' || $operator == 'neq') {
            if ($value === null || $value === 'null') {
                $andX->add($qb->expr()->isNotNull($field));
            } else {
                $andX->add($qb->expr()->neq($field, ':' . $name));
                return new Parameter($name, $value);
            }
        } elseif ($operator == 'tsquery' || $operator == 'fts') {
            $tsquery = new Func('plainto_tsquery', [$field, ':' . $name]);
            $andX->add($qb->expr()->eq($tsquery, 'true'));
            return new Parameter($name, $value);
        }

        return null;
    }

    /**
     * Build a simple filter or sorter criteria class.
     * The result can be used as input params in repository class.
     *
     * @param array $criteria Array with key-value pairs
     *
     * @return SortOrFilter The filter or sorter criteria class
     */
    public static function createCriteria(array $criteria): SortOrFilter
    {
        if (isset($criteria['direction'])) {
            $filter = new SortOrFilter($criteria['property'] ?? $criteria['field'], $criteria['direction']);
        } elseif (isset($criteria['operator'])) {
            $filter = new SortOrFilter($criteria['property'] ?? $criteria['field'], null, $criteria['value']);
            $filter->setOperator($criteria['operator']);
        } elseif (isset($criteria['property']) || isset($criteria['field'])) {
            $filter = new SortOrFilter($criteria['property'] ?? $criteria['field'], null, $criteria['value']);
            $filter->setOperator('eq');
        } else {
            $filter = new SortOrFilter(key($criteria), null, current($criteria));
            $filter->setOperator('eq');
        }

        return $filter;
    }

    /**
     * Build a collection of filter or sorter criteria class.
     * The result can be used as input params in repository class.
     *
     * @param array $criterias A collection of key-value pairs array
     *
     * @return SortOrFilter[] A collection of filter or sorter criteria class
     */
    public static function createCriterias(array $criterias): array
    {
        $result = array();

        foreach ($criterias as $key => $values) {
            if (is_string($values) && $values[0] === '{') {
                $values = json_decode($values, true);
            }
            if (is_array($values)) {
                $result[] = static::createCriteria($values);
            } elseif ($values instanceof SortOrFilter) {
                $result[] = $values;
            } elseif (is_string($key)) {
                $result[] = static::createCriteria([$key => $values]);
            }
        }

        return $result;
    }

    /**
     * Build <var>ORDER BY</var> query expression from a simple select statement.
     * It only support from a single table.
     *
     * @param QueryBuilder   $qb             The doctrine query builder
     * @param string         $tableAlias     The table alias
     * @param array          $defaultOrderBy The default OrderBy if requestOrderBy is empty,
     *                                       in format: [field => direction, field => direction]
     * @param SortOrFilter[] $requestOrderBy The requestOrderBy, see ExtJs query string format
     */
    public static function createSimpleOrderBy(QueryBuilder $qb, $tableAlias,
                                               array $defaultOrderBy, array $requestOrderBy = []): void
    {
        if (!empty($requestOrderBy)) {
            foreach ($requestOrderBy as $sort) {
                if ($sort instanceof SortOrFilter) {
                    $sortX = new OrderBy(sprintf('%s.%s', $tableAlias, $sort->getProperty()),
                                         $sort->getDirection());
                } else {
                    $_sort = static::createCriteria($sort);
                    $sortX = new OrderBy(sprintf('%s.%s', $tableAlias, $_sort->getProperty()),
                                         $_sort->getDirection());
                }

                $qb->addOrderBy($sortX);
            }
        } else {
            foreach ($defaultOrderBy as $field => $direction) {
                $sortX = new OrderBy(sprintf('%s.%s', $tableAlias, $field), $direction);
                $qb->addOrderBy($sortX);
            }
        }
    }

    /**
     * Build <var>WHERE</var> query expression from a simple select statement.
     * It only support from a single table and the <var>WHERE</var> condition
     * supports <var>AND</var> condition only.
     *
     * @param QueryBuilder   $qb         The doctrine query builder
     * @param string         $tableAlias The table alias
     * @param SortOrFilter[] $filters    The filter criteria, see ExtJs query string
     */
    public static function createSimpleWhereAndX(QueryBuilder $qb, string $tableAlias, array $filters = []): void
    {
        if (!empty($filters)) {
            $parameters = new ArrayCollection();
            $andX = $qb->expr()->andX();

            foreach ($filters as $filter) {
                if ($filter instanceof SortOrFilter) {
                    $field = sprintf('%s.%s', $tableAlias, $filter->getProperty());
                    $filter->setProperty($field);
                    $parameter = static::addWhere($qb, $andX, $filter);
                } else {
                    $_filter = static::createCriteria($filter);
                    $field = sprintf('%s.%s', $tableAlias, $_filter->getProperty());
                    $_filter->setProperty($field);
                    $parameter = static::addWhere($qb, $andX, $_filter);
                }

                if ($parameter !== null) {
                    $parameters->add($parameter);
                }
            }

            $qb->where($andX);
            if ($parameters->count() > 0) {
                $qb->setParameters($parameters);
            }
        }
    }

    /**
     * Build <var>WHERE</var> query expression from a simple select statement.
     * It only support from a single table and the <var>WHERE</var> condition
     * supports <var>OR</var> condition only.
     *
     * @param QueryBuilder   $qb         The doctrine query builder
     * @param string         $tableAlias The table alias
     * @param SortOrFilter[] $filters    The filter criteria, see ExtJs query string
     */
    public static function createSimpleWhereOrX(QueryBuilder $qb, $tableAlias, array $filters = []): void
    {
        if (!empty($filters)) {
            $parameters = new ArrayCollection();
            $orX = $qb->expr()->orX();

            foreach ($filters as $filter) {
                if ($filter instanceof SortOrFilter) {
                    $field = sprintf('%s.%s', $tableAlias, $filter->getProperty());
                    $filter->setProperty($field);
                    $parameter = static::addOrWhere($qb, $orX, $filter);
                } else {
                    $_filter = static::createCriteria($filter);
                    $field = sprintf('%s.%s', $tableAlias, $_filter->getProperty());
                    $_filter->setProperty($field);
                    $parameter = static::addOrWhere($qb, $orX, $_filter);
                }

                if ($parameter !== null) {
                    $parameters->add($parameter);
                }
            }

            $qb->where($orX);
            if ($parameters->count() > 0) {
                $qb->setParameters($parameters);
            }
        }
    }

    /**
     * Disable object cloning support
     */
    private function __clone()
    {
    }

    /**
     * Create random string generator.
     *
     * @param string $prefix
     *
     * @return string
     */
    private static function createRandom(string $prefix = ''): string
    {
        if (is_null(self::$generator)) {
            self::$generator = RandomGeneratorFactory::create();
        }

        return !is_null(self::$generator)
            ? $prefix . bin2hex(self::$generator->generate(10))
            : md5(uniqid($prefix, true));
    }

}