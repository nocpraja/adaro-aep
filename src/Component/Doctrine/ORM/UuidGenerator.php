<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Doctrine\ORM;


use App\Component\Uuid\UuidFactory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;


/**
 * Class UuidGenerator
 *
 * @package App\Component\Doctrine\ORM
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 24/07/2018 11:08
 */
final class UuidGenerator extends AbstractIdGenerator
{

    /**
     * Generates an identifier for an entity.
     *
     * @param EntityManager                $em
     * @param \Doctrine\ORM\Mapping\Entity $entity
     * @return string
     */
    public function generate(EntityManager $em, $entity): string
    {
        return UuidFactory::createV4()->toString();
    }

}