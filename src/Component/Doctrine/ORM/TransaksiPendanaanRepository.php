<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Doctrine\ORM;


use App\Component\DataObject\SortOrFilter;
use App\Entity\Pendanaan\TransaksiDanaBatch;
use App\Entity\Pendanaan\TransaksiDanaBidang;
use App\Entity\Pendanaan\TransaksiDanaGlobal;
use App\Entity\Pendanaan\TransaksiDanaProgram;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;

/**
 * Class TransaksiPendanaanRepository
 *
 * @package App\Component\Doctrine\ORM
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 15/05/2019 1:21
 */
class TransaksiPendanaanRepository extends BaseEntityRepository
{

    /**
     * Menampilkan data transaksi pendanaan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause Logic operator untuk WHERE clause
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan sejumlah N transaksi terakhir.
     *
     * @param int            $limit   Jumlah record untuk ditampilkan
     * @param SortOrFilter[] $filters Filter kriteria
     * @param SortOrFilter[] $sorts   Sort method
     *
     * @return TransaksiDanaGlobal[]|TransaksiDanaBidang[]|TransaksiDanaProgram[]|TransaksiDanaBatch[]
     */
    public function getLastNRecords(int $limit, array $filters = [], array $sorts = []): array
    {
        return $this->findAllByCriteria($filters, $sorts, $limit)
                    ->getResult();
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    protected function createSelectQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('transaksi');
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'transaksi.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('transaksi.postedDate', 'desc');
        }
    }

}