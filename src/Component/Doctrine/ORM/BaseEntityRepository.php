<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Doctrine\ORM;


use App\Component\DataObject\SortOrFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * Class BaseEntityRepository.
 *
 * Semua EntityRepository pada aplikasi harus extends dari Class ini.
 * Untuk mempermudah automasi dan Dependency Injection.
 *
 * @package App\Component\Doctrine\ORM
 * @author  Ahmad Fajar
 * @since   25/07/2018, modified: 27/04/2019 17:10
 */
class BaseEntityRepository extends ServiceEntityRepository
{

    const CACHE_TTL = 1800;       // 30 menit
    const CACHE_TTL_LOWEST = 900; // 15 menit

    /**
     * Refreshes the persistent state of an entity from the database,
     * overriding any local changes that have not yet been persisted.
     *
     * @param object $entity The entity to refresh
     *
     * @throws ORMException
     */
    public function refresh(object $entity): void
    {
        $this->_em->refresh($entity);
    }

    /**
     * Removes an object instance.
     *
     * A removed object will be removed from the database as a result of the flush operation.
     *
     * @param object $entity The entity instance to remove
     *
     * @throws ORMException
     */
    final public function remove(object $entity): void
    {
        $this->_em->remove($entity);
    }

    /**
     * Tells the EntityManager to make an instance managed and persistent.
     *
     * @param object $entity The entity instance to make managed and persistent.
     *
     * @throws ORMException
     */
    final public function save(object $entity): void
    {
        $this->_em->persist($entity);
    }

    /**
     * Save and flushes all changes of persistent object to the database.
     *
     * Only managed entity object can be used as parameter. Trying to pass new entity object
     * or detached entity object will thrown ORMException.
     *
     * @param null|object|object[] $entity The persistent entity object instance
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    final public function saveAndFlush($entity = null): void
    {
        $this->_em->flush($entity);
    }

    /**
     * Build generic query result.
     *
     * @param QueryBuilder $qb     The query builder instance
     * @param int          $limit  Jumlah record untuk ditampilkan
     * @param int          $offset Posisi record awal
     *
     * @return Query
     */
    final protected function buildQueryResult(QueryBuilder $qb, int $limit = 0, int $offset = 0): Query
    {
        if (!empty($limit)) {
            $qb->setMaxResults($limit);
        }
        if (!empty($offset)) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()
                  ->setCacheable(true)
                  ->useQueryCache(true)
                  ->setQueryCacheLifetime(self::CACHE_TTL);
    }

    /**
     * Build <var>WHERE</var> query expression.
     *
     * @param QueryBuilder   $qb      The doctrine's QueryBuilder object
     * @param SortOrFilter[] $filters Record filtering
     */
    final protected function createOrWhereCriteria(QueryBuilder $qb, array $filters = []): void
    {
        if (!empty($filters)) {
            $parameters = new ArrayCollection();
            $orX = $qb->expr()->orX();

            foreach ($filters as $filter) {
                $field = $this->parseField($filter);
                $filter->setProperty($field);
                $parameter = QueryExpressionHelper::addOrWhere($qb, $orX, $filter);
                if ($parameter !== null) {
                    $parameters->add($parameter);
                }
            }

            $qb->where($orX);
            if ($parameters->count() > 0) {
                $qb->setParameters($parameters);
            }
        }
    }

    /**
     * Build <var>WHERE</var> query expression.
     *
     * @param QueryBuilder   $qb      The doctrine's QueryBuilder object
     * @param SortOrFilter[] $filters Record filtering
     */
    final protected function createWhereCriteria(QueryBuilder $qb, array $filters = []): void
    {
        if (!empty($filters) && is_array($filters)) {
            $parameters = new ArrayCollection();
            $andX = $qb->expr()->andX();

            foreach ($filters as $filter) {
                $field = $this->parseField($filter);
                $filter->setProperty($field);
                $parameter = QueryExpressionHelper::addWhere($qb, $andX, $filter);
                if ($parameter !== null) {
                    $parameters->add($parameter);
                }
            }

            $qb->where($andX);
            if ($parameters->count() > 0) {
                $qb->setParameters($parameters);
            }
        }
    }

    /**
     * Get the shortname of fieldname.
     *
     * @param string $fqname The field name
     *
     * @return string
     */
    final protected function getShortFieldname(string $fqname): string
    {
        $property = strrchr($fqname, '.');

        return $property === false ? $fqname : ltrim($property, "\0. ");
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     * EntityReposity class dapat melakukan override function ini.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        return $filter->getProperty();
    }
}