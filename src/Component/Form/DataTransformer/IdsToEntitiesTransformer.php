<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Form\DataTransformer;


use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class IdsToEntitiesTransformer is used to transform a collection of ID value to
 * a collection of Entity objects or gets collection of ID value from a collection of Entity object.
 *
 * @package App\Component\Form\DataTransformer
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 24/07/2018 14:12
 */
class IdsToEntitiesTransformer implements DataTransformerInterface
{

    /**
     * @var ObjectRepository
     */
    private $repository;
    /**
     * @var string
     */
    private $idProperty;
    /**
     * @var string
     */
    private $labelProperty;
    /**
     * @var boolean
     */
    private $nullable;
    /**
     * @var string
     */
    private $emptyMsg = 'The collection of Entity value can not be empty.';


    /**
     * IdsToEntitiesTransformer constructor.
     *
     * @param ObjectRepository $repository    The entity repository object
     * @param string           $idProperty    The key to construct field value
     * @param string           $labelProperty The key to construct label or description value
     * @param bool             $nullable      Whether the field value can be empty or not
     */
    public function __construct(ObjectRepository $repository, string $idProperty, string $labelProperty, bool $nullable)
    {
        $this->repository = $repository;
        $this->idProperty = $idProperty;
        $this->labelProperty = $labelProperty;
        $this->nullable = $nullable;
    }

    /**
     * @inheritdoc
     *
     * @param mixed $value The value in the original representation
     *
     * @return mixed The value in the transformed representation
     * @throws TransformationFailedException when the transformation fails
     */
    public function transform($value)
    {
        if (null == $value) {
            return null;
        }

        $result = array();
        $methodV = 'get' . ucfirst($this->idProperty);
        $methodL = 'get' . ucfirst($this->labelProperty);

        if (is_array($value) || ($value instanceof Collection)) {
            foreach ($value as $item) {
                $fvalue = call_user_func_array([$item, $methodV], []);
                $flabel = call_user_func_array([$item, $methodL], []);
                $result[$flabel] = $fvalue;
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     *
     * @param mixed $value The value in the transformed representation
     *
     * @return mixed The value in the original representation
     * @throws TransformationFailedException when the transformation fails
     */
    public function reverseTransform($value)
    {
        if (empty($value)) {
            if ($this->nullable) {
                return null;
            } else {
                throw new TransformationFailedException($this->emptyMsg);
            }
        }

        $result = array();
        if (is_array($value) || ($value instanceof Collection)) {
            foreach ($value as $id) {
                if ((is_array($id) ? !empty($id[$this->idProperty]) : !empty($id))) {
                    $entity = $this->repository->find(is_array($id) ? $id[$this->idProperty] : $id);
                    if (is_null($entity)) {
                        throw new TransformationFailedException(sprintf('Entity "%s" with ID "%s" does not exists.',
                                                                        $this->repository->getClassName(), $value));
                    }
                    $result[] = $entity;
                } elseif ($this->nullable === false && (is_array($id) ? empty($id[$this->idProperty]) : empty($id))) {
                    throw new TransformationFailedException('The entity ID can not be empty!');
                }
            }
        }

        return $result;
    }
}