<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Form\DataTransformer;


use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class IdToEntityTransformer is used to transform ID value to an Entity object or
 * gets ID value from an Entity object.
 *
 * @package App\Component\Form\DataTransformer
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 24/07/2018 13:35
 */
class IdToEntityTransformer implements DataTransformerInterface
{

    /**
     * @var ObjectRepository
     */
    private $repository;
    /**
     * @var string
     */
    private $idProperty;
    /**
     * @var boolean
     */
    private $nullable;
    /**
     * @var string
     */
    private $emptyMsg = 'The entity value can not be empty.';


    /**
     * IdToEntityTransformer constructor.
     *
     * @param ObjectRepository $repository The entity repository object
     * @param string           $idProperty The key to construct field value
     * @param bool             $nullable   Whether the field value can be empty or not
     */
    public function __construct(ObjectRepository $repository, string $idProperty, bool $nullable)
    {
        $this->repository = $repository;
        $this->idProperty = $idProperty;
        $this->nullable = $nullable;
    }

    /**
     * @inheritdoc
     *
     * @param mixed $value The value in the original representation
     *
     * @return mixed The value in the transformed representation
     * @throws TransformationFailedException when the transformation fails
     */
    public function transform($value)
    {
        if (null == $value) {
            return null;
        }
        $method = 'get' . ucfirst($this->idProperty);

        return call_user_func_array([$value, $method], []);
    }

    /**
     * @inheritdoc
     *
     * @param mixed $value The value in the transformed representation
     *
     * @return mixed The value in the original representation
     * @throws TransformationFailedException when the transformation fails
     */
    public function reverseTransform($value)
    {
        if (empty($value)) {
            if ($this->nullable) {
                return null;
            } else {
                throw new TransformationFailedException($this->emptyMsg);
            }
        }

        $entity = $this->repository->find($value);
        if (is_null($entity)) {
            throw new TransformationFailedException(sprintf('Entity "%s" with ID "%s" does not exists.',
                                                            $this->repository->getClassName(), $value));
        }

        return $entity;
    }
}