<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Form\Extension;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PesertaDidikType
 *
 * @package App\Component\Form\Extension
 * @author  Mark Melvin
 * @since   07/02/2019, modified: 26/03/2019 22:25
 */
class PesertaDidikType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('tahun', IntegerType::class)
                ->add('jumlah', IntegerType::class);
    }

}