<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Form\Extension;


use App\Component\Form\DataTransformer\IdToEntityTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class EntityIDType is used to create generic Form of an entity from the given field.
 *
 * @package App\Component\Form\Extension
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 25/01/2019 17:11
 */
class EntityIDType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $_options = ['constraints' => new NotNull()];
        if (isset($options['entry_options']) && !empty($options['entry_options'])) {
            $_options = array_merge(['constraints' => new NotNull()], $options['entry_options']);
        }

        $repo = $options['entry_repo'];
        $builder->add($options['entry_key'], $options['entry_type'], $_options);
        $builder->get($options['entry_key'])
                ->addModelTransformer(new IdToEntityTransformer($repo, $options['entry_key'], false));
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired('entry_key')
            ->setRequired('entry_type')
            ->setRequired('entry_repo')
            ->setDefined('entry_options')
            ->setAllowedTypes('entry_key', 'string')
            ->setAllowedTypes('entry_type', 'string')
            ->setAllowedTypes('entry_options', 'array')
            ->setAllowedTypes('entry_repo', 'Doctrine\Common\Persistence\ObjectRepository');
    }

}