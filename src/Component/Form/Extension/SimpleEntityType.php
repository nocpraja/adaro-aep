<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Form\Extension;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SimpleEntityType is used to create a Form from simple entity.
 *
 * @package App\Component\Form\Extension
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 24/07/2018 12:49
 */
class SimpleEntityType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $fields = $options['entry_fields'];
        foreach ($fields as $field) {
            if (is_array($field)) {
                $builder->add($field['property'], $field['ftype'], isset($field['foptions']) ? $field['foptions'] : []);
                if (!empty($options['html_purifier'])) {
                    $builder->get($field['property'])->addModelTransformer($options['html_purifier']);
                }
            } else {
                $builder->add($field->property, $field->ftype, isset($field->foptions) ? $field->foptions : []);
                if (!empty($options['html_purifier'])) {
                    $builder->get($field->property)->addModelTransformer($options['html_purifier']);
                }
            }
        }
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('data_class')
                 ->setRequired('entry_fields')
                 ->setDefined('html_purifier')
                 ->setAllowedTypes('entry_fields', 'array')
                 ->setAllowedTypes('html_purifier', 'Exercise\HTMLPurifierBundle\Form\HTMLPurifierTransformer');
    }

}