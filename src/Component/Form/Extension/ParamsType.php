<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Form\Extension;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ParamsType
 *
 * @package App\Component\Form\Extension
 * @author  Tri N
 * @since   07/02/2019, modified:07/02/2019 11:03
 */
class ParamsType extends AbstractType
{

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('property', TextType::class)
            ->add('value', TextType::class)
            ->add('type', TextType::class);
    }

}