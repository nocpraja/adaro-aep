<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\DataObject;


/**
 * Class ActionObject
 * @package App\Component\DataObject
 * @author  Ahmad Fajar
 * @since   02/05/2019, modified: 02/05/2019 1:46
 */
final class ActionObject
{

    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $message;


    /**
     * @return string
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param string $action
     *
     * @return ActionObject
     */
    public function setAction(?string $action): self
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return ActionObject
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

}