<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\DataObject;


use App\Entity\Security\MenuItem;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class MenuItemDto
 *
 * @package App\Component\DataObject
 * @author  Ahmad Fajar
 * @since   31/03/2019, modified: 04/04/2019 12:11
 */
final class MenuItemDto
{
    /**
     * @var int
     *
     * @Groups({"without_rel"})
     */
    private $id;

    /**
     * @var string
     *
     * @Groups({"without_rel"})
     */
    private $menuLabel;

    /**
     * @var string
     *
     * @Groups({"without_rel"})
     */
    private $routeUrl;

    /**
     * @var string
     *
     * @Groups({"without_rel"})
     */
    private $routeAlias;

    /**
     * @var boolean
     *
     * @Groups({"without_rel"})
     */
    private $enabled = true;

    /**
     * @var int
     *
     * @Groups({"without_rel"})
     */
    private $position = 0;

    /**
     * @var int
     *
     * @Groups({"without_rel"})
     */
    private $urlType = 0;

    /**
     * @var string
     *
     * @Groups({"without_rel"})
     */
    private $icon;

    /**
     * @var string
     *
     * @Groups({"without_rel"})
     */
    private $cssClass;

    /**
     * @var bool
     *
     * @Groups({"without_rel"})
     */
    private $leaf = false;

    /**
     * @var string[]
     *
     * @Groups({"without_rel"})
     */
    private $accessRules = [];

    /**
     * @var MenuItemDto
     */
    private $parent;

    /**
     * @var MenuItemDto[]
     *
     * @Groups({"without_rel"})
     */
    private $children = [];


    /**
     * MenuItemDto constructor.
     *
     * @param int      $id          Menu ID
     * @param string   $label       Menu label
     * @param bool     $enabled     Enabled or disabled
     * @param string[] $accessRules Access rules
     */
    public function __construct(int $id = 0, string $label = '', bool $enabled = true, array $accessRules = [])
    {
        $this->id = $id;
        $this->menuLabel = $label;
        $this->enabled = $enabled;
        $this->accessRules = $accessRules;
    }

    /**
     * Create DTO from the given entity Object.
     *
     * @param MenuItem $menuItem Persistent entity object
     *
     * @return MenuItemDto
     */
    public static function create(MenuItem $menuItem): MenuItemDto
    {
        $dto = new self($menuItem->getMenuId(), $menuItem->getMenuLabel(), $menuItem->getEnabled());
        $dto->setRouteUrl($menuItem->getRouteUrl())
            ->setRouteAlias($menuItem->getRouteAlias())
            ->setPosition($menuItem->getPosition())
            ->setUrlType($menuItem->getUrlType())
            ->setIcon($menuItem->getIcon())
            ->setCssClass($menuItem->getCssClass());

        return $dto;
    }

    /**
     * Add a child menu from the given entity object.
     *
     * @param MenuItem $menuItem Persistent entity object
     */
    public function addSubMenu(MenuItem $menuItem): void
    {
        $dto = static::create($menuItem);
        $dto->setParent($this);
        $this->children[] = $dto;
        $this->leaf = false;
    }

    /**
     * Gets the access rules for this menu item.
     *
     * @return string[]
     */
    public function getAccessRules(): ?array
    {
        return $this->accessRules;
    }

    /**
     * Sets the access rules for this menu item.
     *
     * @param string[] $accessRules
     *
     * @return MenuItemDto
     */
    public function setAccessRules(?array $accessRules = []): self
    {
        $this->accessRules = $accessRules;

        return $this;
    }

    /**
     * Gets collection of child menu items.
     *
     * @return MenuItemDto[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * Assign child menu items.
     *
     * @param MenuItemDto[]|MenuItem[] $menuItems
     *
     * @return MenuItemDto
     */
    public function setChildren(?array $menuItems): self
    {
        $this->children = [];

        if (empty($menuItems)) {
            $this->leaf = true;
        } elseif (!empty($menuItems)) {
            $this->leaf = false;

            foreach ($menuItems as $child) {
                if ($child instanceof MenuItem) {
                    $this->addSubMenu($child);
                } elseif ($child instanceof MenuItemDto) {
                    $child->setParent($this);
                    $this->children[] = $child;
                }
            }
        }

        return $this;
    }

    /**
     * Gets optional css classes that will be appended to the menu when it is rendered.
     *
     * @return string The css classes to append
     */
    public function getCssClass(): ?string
    {
        return $this->cssClass;
    }

    /**
     * Sets optional css classes that will be appended to the menu when it is rendered.
     *
     * @param string $cssClass The css classes to append
     *
     * @return MenuItemDto
     */
    public function setCssClass(?string $cssClass): self
    {
        $this->cssClass = $cssClass;

        return $this;
    }

    /**
     * Gets menu item's icon.
     *
     * @return string Icon name
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * Sets menu item's icon.
     *
     * @param string $icon Icon name
     *
     * @return MenuItemDto
     */
    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Gets the menu ID.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Sets the menu ID.
     *
     * @param int $id Menu ID
     *
     * @return MenuItemDto
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets menu label.
     *
     * @return string
     */
    public function getMenuLabel(): string
    {
        return $this->menuLabel;
    }

    /**
     * Sets menu label.
     *
     * @param string $menuLabel The menu label
     *
     * @return MenuItemDto
     */
    public function setMenuLabel(string $menuLabel): self
    {
        $this->menuLabel = $menuLabel;

        return $this;
    }

    /**
     * Gets parent menu.
     *
     * @return MenuItemDto
     */
    public function getParent(): ?MenuItemDto
    {
        return $this->parent;
    }

    /**
     * Sets parent menu.
     *
     * @param MenuItemDto $parent
     *
     * @return MenuItemDto
     */
    public function setParent(?MenuItemDto $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Gets menu order/position relative to its parent.
     *
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * Sets menu order/position relative to its parent.
     *
     * @param int $position Menu position
     *
     * @return MenuItemDto
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Gets back-end route URL or Symfony route name.
     *
     * @return string Route URL or Symfony route name
     */
    public function getRouteAlias(): ?string
    {
        return $this->routeAlias;
    }

    /**
     * Sets back-end route URL or Symfony route name.
     *
     * @param string $routeAlias Route URL or Symfony route name
     *
     * @return MenuItemDto
     */
    public function setRouteAlias(?string $routeAlias): self
    {
        $this->routeAlias = $routeAlias;

        return $this;
    }

    /**
     * Gets front-end or VueJs route URL.
     *
     * @return string Route URL
     */
    public function getRouteUrl(): ?string
    {
        return $this->routeUrl;
    }

    /**
     * Sets front-end or VueJS route URL.
     *
     * @param string $routeUrl Route URL
     *
     * @return MenuItemDto
     */
    public function setRouteUrl(?string $routeUrl): self
    {
        $this->routeUrl = $routeUrl;

        return $this;
    }

    /**
     * Gets the menu's route URL type.
     *
     * @return int Salah satu dari nilai berikut:<br>
     * 0 = Route URL internal dan tampil pada MainMenu,<br>
     * 1 = Route URL external dan tampil pada MainMenu,<br>
     * 2 = Route URL internal dan tidak tampil pada MainMenu<br>
     * 3 = Menu parent, tampil pada MainMenu dan tanpa route URL<br>
     * 4 = Menu separator, tanpa route URL
     */
    public function getUrlType(): int
    {
        return $this->urlType;
    }

    /**
     * Sets the menu's route URL type.
     *
     * @param int $urlType Salah satu dari nilai berikut:<br>
     *                     0 = Route URL internal dan tampil pada MainMenu,<br>
     *                     1 = Route URL external dan tampil pada MainMenu,<br>
     *                     2 = Route URL internal dan tidak tampil pada MainMenu,<br>
     *                     3 = Menu parent, tampil pada MainMenu dan tanpa route URL,<br>
     *                     4 = Menu separator, no route URL
     *
     * @return MenuItemDto
     */
    public function setUrlType(int $urlType): self
    {
        $this->urlType = $urlType;

        return $this;
    }

    /**
     * Checks whether a menu is enabled or disabled.
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Enable or disable a menu.
     *
     * @param bool $enabled
     *
     * @return MenuItemDto
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Check whether has children or not.
     *
     * @return bool TRUE if this menu doesn't have any child otherwise FALSE
     */
    public function isLeaf(): bool
    {
        return $this->leaf;
    }

    /**
     * Convert this instance to Entity object.
     *
     * @return MenuItem Unmanaged entity object
     */
    public function toEntity()
    {
        $entity = new MenuItem($this->getId());
        $entity->setMenuLabel($this->getMenuLabel())
               ->setRouteUrl($this->getRouteUrl())
               ->setRouteAlias($this->getRouteAlias())
               ->setEnabled($this->isEnabled())
               ->setIcon($this->getIcon())
               ->setCssClass($this->getCssClass())
               ->setPosition($this->getPosition())
               ->setUrlType($this->getUrlType());

        return $entity;
    }

}
