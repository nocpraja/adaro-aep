<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\DataObject;


use App\Component\Doctrine\ORM\QueryExpressionHelper;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DataQuery
 *
 * @package App\Component\DataObject
 * @author  Ahmad Fajar
 * @since   03/11/2018, modified: 24/02/2019 14:06
 */
final class DataQuery
{

    /**
     * @var SortOrFilter[]
     */
    private $filters = [];

    /**
     * @var SortOrFilter[]
     */
    private $sorts = [];

    /**
     * @var integer
     */
    private $offset = 0;

    /**
     * @var integer
     */
    private $page = 1;

    /**
     * @var integer
     */
    private $pageSize = 0;

    /**
     * @var string
     */
    private $condition = 'AND';


    /**
     * DataQuery constructor.
     *
     * @param int $page     Current page number
     * @param int $pageSize Maksimum jumlah record yang dapat ditampilkan
     */
    public function __construct(int $page = 1, int $pageSize = 0)
    {
        $this->page = $page;
        $this->pageSize = $pageSize;
        $this->offset = $page > 0 ? (($page - 1) * $pageSize) : 0;
    }

    /**
     * Get SQL query condition to be used.
     *
     * @return string Salah satu dari nilai berikut: AND, OR
     */
    public function getCondition(): string
    {
        return $this->condition;
    }

    /**
     * Set SQL query condition to be used.
     *
     * @param string $condition Valid values: AND, OR
     *
     * @return DataQuery
     */
    public function setCondition(string $condition): self
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * Get data filtering conditions.
     *
     * @return SortOrFilter[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * Set data filtering conditions.
     *
     * @param SortOrFilter[] $filters
     *
     * @return DataQuery
     */
    public function setFilters(array $filters): self
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * Get absolute record position to be displayed.
     *
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * Set absolute record position to be displayed.
     *
     * @param int $offset Absolute record position
     *
     * @return DataQuery
     */
    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * Get current page number.
     *
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * Set current page number.
     *
     * @param int $page Page number
     *
     * @return DataQuery
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get maksimum jumlah record yang dapat ditampilkan dalam satu Page.
     *
     * Jika bernilai `0` berarti tidak ada batasan/limit.
     *
     * @return int Maksimum jumlah record
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * Set maksimum jumlah record yang dapat ditampilkan dalam satu Page.
     *
     * Jika bernilai `0` berarti tidak ada batasan/limit.
     *
     * @param int $pageSize Jumlah record maksimum
     *
     * @return DataQuery
     */
    public function setPageSize(int $pageSize): self
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * Get the sort method.
     *
     * @return SortOrFilter[]
     */
    public function getSorts(): array
    {
        return $this->sorts;
    }

    /**
     * Set the sort method.
     *
     * @param SortOrFilter[] $sorts
     *
     * @return DataQuery
     */
    public function setSorts(array $sorts): self
    {
        $this->sorts = $sorts;

        return $this;
    }

    /**
     * Parse http request query parameters and build query object.
     *
     * @param Request $request
     *
     * @return DataQuery Data query
     */
    public static function parseRequest(Request $request): DataQuery
    {
        $page = $request->query->getInt('page', 0);
        $limit = $request->query->getInt('limit', 0);
        $logic = $request->query->getAlpha('logic', 'AND');
        $filters = static::parseSortOrFilter($request, 'filters');
        $sorts = static::parseSortOrFilter($request, 'sorts');

        $result = new self($page, $limit);
        $result->setFilters($filters)
               ->setSorts($sorts)
               ->setCondition($logic);

        return $result;
    }

    /**
     * Parse request query parameters and find spesific parameter.
     *
     * @param Request        $request HTTP request object
     * @param string         $search  The parameter to search
     * @param SortOrFilter[] $default Default sorts or filters criteria
     *
     * @return SortOrFilter[]
     */
    public static function parseSortOrFilter(Request $request, string $search, array $default = []): array
    {
        $req = $request->query->get($search);

        if (!empty($req) && is_array($req)) {
            return QueryExpressionHelper::createCriterias($req);
        } else {
            return $default;
        }
    }

}