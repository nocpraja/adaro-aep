<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\DataObject;


/**
 * Class SortFilter
 *
 * @package App\Component\DataObject
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 29/01/2019 2:50
 */
final class SortOrFilter
{

    /**
     * @var string
     */
    private $property;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var string
     */
    private $operator = 'eq';

    /**
     * @var string
     */
    private $direction;

    /**
     * @var bool
     */
    private $expression = false;


    /**
     * SortOrFilter constructor.
     *
     * @param string $property  Filter or Sort field property
     * @param string $direction Sort direction
     * @param mixed  $value     Filter value
     */
    public function __construct(string $property, string $direction = null, $value = null)
    {
        $this->property = $property;
        $this->direction = is_null($direction) ? null : strtolower(trim($direction));
        $this->value = $value;
        if (!is_null($direction)) {
            $this->operator = null;
        }
    }

    /**
     * Gets Sort direction.
     *
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }

    /**
     * Sets Sort direction.
     *
     * @param string $direction
     *
     * @return SortOrFilter
     */
    public function setDirection(string $direction): self
    {
        $this->direction = is_null($direction) ? null : strtolower(trim($direction));

        return $this;
    }

    /**
     * Gets Filter operator.
     *
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * Sets Filter operator.
     *
     * @param string $operator
     *
     * @return SortOrFilter
     */
    public function setOperator(string $operator): self
    {
        $this->operator = is_null($operator) ? null : trim($operator);

        return $this;
    }

    /**
     * Gets Sort or Filter field property.
     *
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * Sets Sort or Filter field property.
     *
     * @param string $property
     *
     * @return SortOrFilter
     */
    public function setProperty(string $property): self
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Gets Filter value.
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets Filter value.
     *
     * @param mixed $value
     *
     * @return SortOrFilter
     */
    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Checks if Filter criteria has already in query expression format or not.
     *
     * @return bool
     */
    public function isExpression(): bool
    {
        return $this->expression;
    }

    /**
     * Marks that Filter criteria is in query expression format.
     *
     * @param bool $expression
     *
     * @return SortOrFilter
     */
    public function setExpression(bool $expression): self
    {
        $this->expression = $expression;

        return $this;
    }

}