<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\DataObject;


use App\Entity\Kegiatan\CapaianKpi;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class CapaianKpiDto
 *
 * @package App\Component\DataObject
 * @author  Ahmad Fajar
 * @since   17/02/2019, modified: 17/02/2019 15:48
 */
final class CapaianKpiDto
{

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $year;

    /**
     * @var CapaianKpi[]
     * @Groups({"without_rel"})
     */
    private $parameters = [];


    /**
     * CapaianKpiDto constructor.
     *
     * @param int               $year
     * @param CapaianKpi[]|null $capaianKpis
     */
    public function __construct(int $year, array $capaianKpis = null)
    {
        $this->year = $year;
        if (!empty($capaianKpis)) {
            $this->parameters = $capaianKpis;
        }
    }

    /**
     * Add parameter capaian KPI to the collection of parameters.
     *
     * @param CapaianKpi $capaianKpi Persistent entity object
     */
    public function addParameter(CapaianKpi $capaianKpi): void
    {
        $this->parameters[] = $capaianKpi;
    }

    /**
     * Get parameters pencapaian KPI.
     *
     * @return CapaianKpi[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Assign parameters pencapaian KPI.
     *
     * @param CapaianKpi[] $parameters
     *
     * @return CapaianKpiDto
     */
    public function setParameters(array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Get tahun anggaran pelaksanaan.
     *
     * @return int Tahun anggaran
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * Define tahun anggaran pelaksanaan.
     *
     * @param int $year Tahun anggaran
     *
     * @return CapaianKpiDto
     */
    public function setYear(int $year): self
    {
        $this->year = $year;
        return $this;
    }

}