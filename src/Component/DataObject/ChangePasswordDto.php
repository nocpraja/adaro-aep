<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\DataObject;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChangePasswordDto
 *
 * @package App\Component\DataObject
 * @author  Ahmad Fajar
 * @since   2019-04-16, modified: 2019-04-16 00:49
 */
final class ChangePasswordDto
{

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="6")
     */
    private $password;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="6")
     * @Assert\EqualTo(propertyPath="password", message="ForgotPassword.FieldConfirmNotEqual")
     */
    private $confirmPassword;


    /**
     * @return string
     */
    public function getConfirmPassword(): ?string
    {
        return $this->confirmPassword;
    }

    /**
     * @param mixed $confirmPassword
     *
     * @return ChangePasswordDto
     */
    public function setConfirmPassword(string $confirmPassword): self
    {
        $this->confirmPassword = $confirmPassword;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return ChangePasswordDto
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

}