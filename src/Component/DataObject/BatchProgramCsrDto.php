<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\DataObject;


use App\Entity\MasterData\BatchCsr;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class BatchProgramCsrDto
 *
 * @package App\Component\DataObject
 * @author  Ahmad Fajar
 * @since   17/02/2019, modified: 2019-02-21 02:17
 */
final class BatchProgramCsrDto
{

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $batchId;

    /**
     * @var string
     * @Groups({"without_rel"})
     */
    private $namaBatch;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $tahunAwal;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $tahunAkhir;

    /**
     * @var CapaianKpiDto[]
     * @Groups({"without_rel"})
     */
    private $yearlyCapaian = [];


    /**
     * BatchProgramCsrDto constructor.
     *
     * @param int    $batchId   ID Batch program CSR
     * @param string $namaBatch Nama batch
     */
    public function __construct(?int $batchId, ?string $namaBatch)
    {
        if (!empty($batchId)) {
            $this->batchId = $batchId;
        }
        if (!empty($namaBatch)) {
            $this->namaBatch = $namaBatch;
        }
    }

    /**
     * Get ID batch program CSR.
     *
     * @return int
     */
    public function getBatchId(): ?int
    {
        return $this->batchId;
    }

    /**
     * Define ID batch progrma CSR.
     *
     * @param int $batchId ID Batch
     *
     * @return BatchProgramCsrDto
     */
    public function setBatchId(int $batchId): self
    {
        $this->batchId = $batchId;

        return $this;
    }

    /**
     * Get nama batch program CSR.
     *
     * @return string Nama batch
     */
    public function getNamaBatch(): ?string
    {
        return $this->namaBatch;
    }

    /**
     * Define nama batch program CSR.
     *
     * @param string $namaBatch Nama batch
     *
     * @return BatchProgramCsrDto
     */
    public function setNamaBatch(string $namaBatch): self
    {
        $this->namaBatch = $namaBatch;

        return $this;
    }

    /**
     * Get tahun akhir pelaksanaan.
     *
     * @return int
     */
    public function getTahunAkhir(): ?int
    {
        return $this->tahunAkhir;
    }

    /**
     * Define tahun akhir pelaksanaan.
     *
     * @param int $tahunAkhir Tahun akhir pelaksanaan
     *
     * @return BatchProgramCsrDto
     */
    public function setTahunAkhir(int $tahunAkhir): self
    {
        $this->tahunAkhir = $tahunAkhir;
        $this->setupYearlyCapaianKpi();

        return $this;
    }

    /**
     * Get tahun awal pelaksanaan.
     *
     * @return int
     */
    public function getTahunAwal(): ?int
    {
        return $this->tahunAwal;
    }

    /**
     * Define tahun awal pelaksanaan.
     *
     * @param int $tahunAwal Tahun awal pelaksanaan
     *
     * @return BatchProgramCsrDto
     */
    public function setTahunAwal(int $tahunAwal): self
    {
        $this->tahunAwal = $tahunAwal;
        $this->setupYearlyCapaianKpi();

        return $this;
    }

    /**
     * Get yearly capaian KPI's for current batch.
     *
     * @return CapaianKpiDto[]
     */
    public function getYearlyCapaian(): array
    {
        return $this->yearlyCapaian;
    }

    /**
     * Assign yearly capaian KPI's for current batch.
     *
     * @param CapaianKpiDto[] $yearlyCapaian
     *
     * @return BatchProgramCsrDto
     */
    public function setYearlyCapaian(array $yearlyCapaian): self
    {
        $this->yearlyCapaian = $yearlyCapaian;

        return $this;
    }

    private function setupYearlyCapaianKpi()
    {
        if (!empty($this->tahunAwal) && !empty($this->tahunAkhir)) {
            $this->yearlyCapaian = [];
            for ($year = $this->tahunAwal; $year < $this->tahunAkhir + 1; $year++) {
                $this->yearlyCapaian[] = new CapaianKpiDto($year);
            }
        }
    }

    /**
     * Create Data Transfer Object from persistent entity object.
     *
     * @param BatchCsr $entity Persistent entity object
     *
     * @return BatchProgramCsrDto
     */
    public static function createFromEntity(BatchCsr $entity): BatchProgramCsrDto
    {
        $batch = new self($entity->getBatchId(), $entity->getNamaBatch());
        $batch->setTahunAwal($entity->getTahunAwal())
              ->setTahunAkhir($entity->getTahunAkhir());

        return $batch;
    }

}