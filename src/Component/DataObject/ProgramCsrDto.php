<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\DataObject;


use App\Entity\MasterData\BatchCsr;
use App\Entity\MasterData\ProgramCSR;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ProgramCsrDto
 *
 * @package App\Component\DataObject
 * @author  Ahmad Fajar
 * @since   17/02/2019, modified: 2019-02-21 02:17
 */
final class ProgramCsrDto
{

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $msid;

    /**
     * @var string
     * @Groups({"without_rel"})
     */
    private $title;

    /**
     * @var BatchProgramCsrDto[]
     * @Groups({"without_rel"})
     */
    private $batches = [];


    /**
     * ProgramCsrDto constructor.
     *
     * @param int    $msid  ID program CSR
     * @param string $title Nama program CSR
     */
    public function __construct(?int $msid, ?string $title)
    {
        if (!empty($msid)) {
            $this->msid = $msid;
        }
        if (!empty($title)) {
            $this->title = $title;
        }
    }

    /**
     * Add a Batch CSR to the batch collection.
     *
     * @param BatchProgramCsrDto $batch Item to add
     */
    public function addBatch(BatchProgramCsrDto $batch): void
    {
        $this->batches[] = $batch;
    }

    /**
     * Get collection of Batch for current program CSR.
     *
     * @return BatchProgramCsrDto[]
     */
    public function getBatches(): array
    {
        return $this->batches;
    }

    /**
     * Assign collection of Batch for current program CSR.
     *
     * @param BatchProgramCsrDto[] $batches Collection of Batch
     *
     * @return ProgramCsrDto
     */
    public function setBatches(array $batches): self
    {
        $this->batches = $batches;

        return $this;
    }

    /**
     * Get ID program CSR.
     *
     * @return int
     */
    public function getMsid(): ?int
    {
        return $this->msid;
    }

    /**
     * Define ID program CSR.
     *
     * @param int $msid ID program CSR
     *
     * @return ProgramCsrDto
     */
    public function setMsid(int $msid): self
    {
        $this->msid = $msid;

        return $this;
    }

    /**
     * Get nama program CSR.
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Define nama program CSR.
     *
     * @param string $title Nama program CSR
     *
     * @return ProgramCsrDto
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Create collection of ProgramCSR DTO from collection of entity object BatchCsr.
     *
     * @param BatchCsr[] $batches Collection of persistent entity object
     *
     * @return ProgramCsrDto[]
     */
    public static function createFromEntities(array $batches): array
    {
        $programs = [];

        if (!empty($batches) && $batches[0] instanceof BatchCsr) {
            foreach ($batches as $batch) {
                $csr = $batch->getProgramCsr();
                $idx = static::programInArray($programs, $csr);

                if ($idx > -1) {
                    /** @var ProgramCsrDto $program */
                    $program = $programs[$idx];
                    $program->addBatch(BatchProgramCsrDto::createFromEntity($batch));
                } else {
                    $program = new self($csr->getMsid(), $csr->getName());
                    $program->setBatches([BatchProgramCsrDto::createFromEntity($batch)]);
                    $programs[] = $program;
                }
            }
        }

        return $programs;
    }

    /**
     * Check if a ProgramCsr already exists in the collection.
     *
     * @param ProgramCsrDto[] $sources The source array
     * @param ProgramCSR      $search  Item to search
     *
     * @return int Position index
     */
    private static function programInArray(array $sources, ProgramCsr $search): int
    {
        if (!empty($sources) && $sources[0] instanceof ProgramCsrDto) {
            for ($i = 0; $i < count($sources); $i++) {
                if ($sources[$i]->getMsid() == $search->getMsid()) {
                    return $i;
                }
            }
        }

        return -1;
    }

}