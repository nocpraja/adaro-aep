<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid\Generator;


/**
 * Class MtRandGenerator provides functionality to generate strings of random
 * binary data using the `mt_rand()` PHP function.
 *
 * @package App\Component\Uuid\Generator
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 02/06/2018 13:09
 */
final class MtRandGenerator implements RandomGeneratorInterface
{

    /**
     * @inheritdoc
     */
    public function generate($length): string
    {
        $bytes = '';

        for ($i = 0; $i < $length; $i++) {
            $bytes = chr(mt_rand(0, 255)) . $bytes;
        }

        return $bytes;
    }
}