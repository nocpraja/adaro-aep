<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid\Generator;


/**
 * Interface NodeGeneratorInterface provides functionality to generate strings of binary data
 * for UUID version 1.
 *
 * @package App\Component\Uuid\Generator
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 02/06/2018 13:05
 */
interface NodeGeneratorInterface
{
    /**
     * Generate a binary string for UUID version 1 from a host ID, sequence number, and the current time.
     *
     * If $node is not given, we will attempt to obtain the local hardware
     * address. If $clockSeq is given, it is used as the sequence number;
     * otherwise a random 14-bit sequence number is chosen.
     *
     * @param integer|string $node     A 48-bit number representing the hardware address.
     *                                 This number may be represented as an integer or
     *                                 a hexadecimal string.
     * @param integer        $clockSeq A 14-bit number used to help avoid duplicates that
     *                                 could arise when the clock is set backwards in time
     *                                 or if the node ID changes.
     *
     * @return string A binary string
     */
    public function generate($node = null, $clockSeq = null): string;

}