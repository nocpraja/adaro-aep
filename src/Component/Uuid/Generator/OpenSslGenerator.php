<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid\Generator;


/**
 * Class OpenSslGenerator provides functionality to generate strings of random
 * binary data using the `openssl_random_pseudo_bytes()` PHP function.
 *
 * @package App\Component\Uuid\Generator
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 2019-04-14 23:02
 */
final class OpenSslGenerator implements RandomGeneratorInterface
{

    /**
     * @inheritdoc
     */
    public function generate($length): string
    {
        return openssl_random_pseudo_bytes($length, $strong);
    }
}