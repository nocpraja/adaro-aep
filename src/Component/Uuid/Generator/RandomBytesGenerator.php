<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid\Generator;


/**
 * Class RandomBytesGenerator provides functionality to generate strings of random
 * binary data using `random_bytes()` function in PHP 7+
 *
 * @package App\Component\Uuid\Generator
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 24/07/2018 21:27
 */
final class RandomBytesGenerator implements RandomGeneratorInterface
{
    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function generate($length): string
    {
        return random_bytes($length);
    }
}