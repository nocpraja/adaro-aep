<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid\Generator;


/**
 * RandomGeneratorFactory is factory class for retrieving a random generator based on the environment.
 *
 * @package App\Component\Uuid\Generator
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 02/06/2018 13:11
 */
final class RandomGeneratorFactory
{
    /**
     * Returns a default random generator, based on the current environment.
     *
     * @return RandomGeneratorInterface
     */
    public static function create(): RandomGeneratorInterface
    {
        if (function_exists('random_bytes')) {
            return new RandomBytesGenerator();
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            return new OpenSslGenerator();
        } elseif (extension_loaded('mcrypt')) {
            return new McryptGenerator();
        } else {
            return new MtRandGenerator();
        }
    }
}