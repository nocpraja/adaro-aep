<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid\Generator;


use App\Component\Uuid\UuidFactory;

/**
 * Class DefaultNodeGenerator
 *
 * @package App\Component\Uuid\Generator
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 02/06/2018 13:05
 */
final class DefaultNodeGenerator implements NodeGeneratorInterface
{
    /**
     * Uses the provided seconds and micro-seconds to calculate the time_low,
     * time_mid, and time_high fields used by RFC 4122 version 1 UUIDs
     *
     * @param integer $seconds
     * @param integer $microSeconds
     *
     * @return string[] An array containing `low`, `mid`, and `high` keys
     * @link http://tools.ietf.org/html/rfc4122#section-4.2.2
     */
    public static function calculateTime($seconds, $microSeconds): array
    {
        // 0x01b21dd213814000 is the number of 100-ns intervals between the
        // UUID epoch 1582-10-15 00:00:00 and the Unix epoch 1970-01-01 00:00:00.
        $uuidTime = ($seconds * 10000000) + ($microSeconds * 10) + 0x01b21dd213814000;

        return [
            'low' => sprintf('%08x', $uuidTime & 0xffffffff),
            'mid' => sprintf('%04x', ($uuidTime >> 32) & 0xffff),
            'hi'  => sprintf('%04x', ($uuidTime >> 48) & 0x0fff),
        ];
    }

    /**
     * @inheritdoc
     */
    public function generate($node = null, $clockSeq = null): string
    {
        $hostNode = $this->getValidNode($node);
        if (is_null($clockSeq)) {
            // Not using "stable storage"; see RFC 4122, Section 4.2.1.1
            $clockSeq = mt_rand(0, 1 << 14);
        }

        // Create a 60-bit time value as a count of 100-nanosecond intervals
        $timeOfDay = gettimeofday();
        $uuidTime = static::calculateTime($timeOfDay['sec'], $timeOfDay['usec']);
        $timeHi = UuidFactory::applyVersion($uuidTime['hi'], 1);
        $clockSeqHi = UuidFactory::applyVariant($clockSeq >> 8);
        $hex = vsprintf(
            '%08s%04s%04s%02s%02s%012s',
            [
                $uuidTime['low'],
                $uuidTime['mid'],
                sprintf('%04x', $timeHi),
                sprintf('%02x', $clockSeqHi),
                sprintf('%02x', $clockSeq & 0xff),
                $hostNode,
            ]
        );

        return hex2bin($hex);
    }

    /**
     * Returns the system MAC address.
     *
     * @return string|false Host MAC address in hexadecimal string or FALSE on failure
     */
    private function getHostNode()
    {
        static $node = null;

        if ($node !== null) {
            return $node;
        }

        $pattern = '/[^:]([0-9A-Fa-f]{2}([:-])[0-9A-Fa-f]{2}(\2[0-9A-Fa-f]{2}){4})[^:]/';
        $matches = array();

        // Search the ifconfig output for all MAC addresses and return
        // the first one found
        $node = false;
        if (preg_match_all($pattern, $this->getMacAddress(), $matches, PREG_PATTERN_ORDER)) {
            $node = $matches[1][0];
            $node = str_replace([':', '-'], '', $node);
        }

        return $node;
    }

    /**
     * Returns the network interface configuration for the system.
     *
     * @return string
     */
    private function getMacAddress(): string
    {
        ob_start();
        switch (strtoupper(substr(php_uname('a'), 0, 3))) {
            case 'WIN':
                passthru('ipconfig /all 2>&1');
                break;
            case 'DAR':
                passthru('ifconfig 2>&1');
                break;
            case 'LIN':
            default:
                passthru('netstat -ie 2>&1');
                break;
        }

        return ob_get_clean();
    }

    /**
     * Generate random hex string.
     *
     * @return string
     */
    private function getRandomNode(): string
    {
        return sprintf('%06x%06x', mt_rand(0, 0xffffff), mt_rand(0, 0xffffff));
    }

    /**
     * Get valid node ID, it is usually a MAC address.
     *
     * @param string|integer $node The node value
     *
     * @return string Hexadecimal string representation of the node ID
     */
    private function getValidNode($node): string
    {
        if (is_null($node)) {
            if (false === ($result = $this->getHostNode())) {
                $result = $this->getRandomNode();
            }

            $node = $result;
        }
        $node = str_replace([':', '-'], '', $node);

        // Convert the node to hex, if it is still an integer
        if (is_int($node)) {
            $node = sprintf('%012x', $node);
        }

        if (!ctype_xdigit($node) || strlen($node) > 12) {
            throw new \InvalidArgumentException('Invalid node value');
        }

        return strtolower(sprintf('%012s', $node));
    }

}