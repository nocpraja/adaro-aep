<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid\Generator;


/**
 * Interface RandomGeneratorInterface provides functionality to generate strings of random binary data.
 *
 * @package App\Component\Uuid\Generator
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 02/06/2018 13:05
 */
interface RandomGeneratorInterface
{
    /**
     * Generates a string of random binary data of the specified length
     *
     * @param integer $length The number of bytes of random binary data to generate
     *
     * @return string A binary string on success or false on failure
     */
    public function generate($length): string;

}