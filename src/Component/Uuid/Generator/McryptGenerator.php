<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid\Generator;


/**
 * Class McryptGenerator provides functionality to generate strings of random
 * binary data using the PHP mcrypt extension.
 *
 * @package App\Component\Uuid\Generator
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 02/06/2018 13:09
 */
final class McryptGenerator implements RandomGeneratorInterface
{

    /**
     * @inheritdoc
     */
    public function generate($length): string
    {
        if (is_numeric($length)) {
            $length += 0;
        }
        if (is_float($length) && $length > ~PHP_INT_MAX && $length < PHP_INT_MAX) {
            $length = (int)$length;
        }

        return @mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
    }
}