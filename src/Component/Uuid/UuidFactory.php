<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid;


use App\Component\Uuid\Generator\DefaultNodeGenerator;
use App\Component\Uuid\Generator\NodeGeneratorInterface;
use App\Component\Uuid\Generator\RandomGeneratorFactory;
use App\Component\Uuid\Generator\RandomGeneratorInterface;

/**
 * Class UuidFactory provides functionality to generate UUID version 1, 3, 4 or 5 based on RFC 4122.
 *
 * @package App\Component\Uuid
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 02/06/2018 13:14
 */
final class UuidFactory
{
    /**
     * The nil UUID is special form of UUID that is specified to have all 128 bits set to zero.
     * @link http://tools.ietf.org/html/rfc4122#section-4.1.7
     */
    const NIL = '00000000-0000-0000-0000-000000000000';

    /**
     * Regular expression pattern for matching a valid UUID of any variant.
     */
    const VALID_PATTERN = '^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$';

    /**
     * @var UuidFactory
     */
    private static $factory = null;

    /**
     * @var RandomGeneratorInterface
     */
    private $randomGenerator;

    /**
     * @var NodeGeneratorInterface
     */
    private $nodeGenerator;


    /**
     * UuidFactory constructor.
     *
     * @param NodeGeneratorInterface $nodeGenerator
     */
    private function __construct(NodeGeneratorInterface $nodeGenerator = null)
    {
        $this->randomGenerator = RandomGeneratorFactory::create();

        if (is_null($nodeGenerator)) {
            $this->nodeGenerator = new DefaultNodeGenerator();
        } else {
            $this->nodeGenerator = $nodeGenerator;
        }
    }

    /**
     * Applies the RFC 4122 variant field to the `clock_seq_hi` field
     *
     * @param $clockSeqHi
     *
     * @return int The high field of the clock sequence multiplexed with the variant
     * @link http://tools.ietf.org/html/rfc4122#section-4.1.1
     */
    public static function applyVariant($clockSeqHi): int
    {
        // Set the variant to RFC 4122
        $clockSeqHi = $clockSeqHi & 0x3f;
        $clockSeqHi &= ~(0xc0);
        $clockSeqHi |= 0x80;

        return $clockSeqHi;
    }

    /**
     * Applies the RFC 4122 version number to the `time_hi_and_version` field
     *
     * @param string  $timeHi
     * @param integer $version
     *
     * @return int The high field of the timestamp multiplexed with the version number
     * @link http://tools.ietf.org/html/rfc4122#section-4.1.3
     */
    public static function applyVersion($timeHi, $version): int
    {
        $timeHi = hexdec($timeHi) & 0x0fff;
        $timeHi &= ~(0xf000);
        $timeHi |= $version << 12;

        return $timeHi;
    }

    /**
     * Generate an UUID version 1 from a host ID, and the current time.
     *
     * @param string  $macAddress A 48-bit number representing the hardware mac address
     * @param integer $clockSeq   A 14-bit number used to help avoid duplicates that could arise
     *                            when the clock is set backwards in time
     *
     * @return Uuid
     */
    public static function createV1($macAddress = null, $clockSeq = null): Uuid
    {
        self::create();

        $bytes = self::$factory->nodeGenerator->generate($macAddress, $clockSeq);
        // When converting the bytes to hex, it turns into a 32-character
        // hexadecimal string that looks a lot like an MD5 hash, so at this
        // point, we can just pass it to uuidFromHashedName.
        $hex = bin2hex($bytes);

        return self::$factory->uuidFromHashedName($hex, 1);
    }

    /**
     * Generate an UUID version 3 based on the MD5 hash of a namespace identifier and a domain name.
     *
     * @param string $nsUuid The namespace UUID
     * @param string $domain The domain name
     *
     * @return Uuid
     */
    public static function createV3($nsUuid, $domain): Uuid
    {
        self::create();

        return self::$factory->uuidFromNsAndName($nsUuid, $domain, 3, 'md5');
    }

    /**
     * Generate an UUID version 4 based on RFC 4122.
     *
     * @return Uuid
     */
    public static function createV4(): Uuid
    {
        self::create();

        $bytes = self::$factory->randomGenerator->generate(16);
        // When converting the bytes to hex, it turns into a 32-character
        // hexadecimal string that looks a lot like an MD5 hash, so at this
        // point, we can just pass it to uuidFromHashedName.
        $hex = bin2hex($bytes);

        return self::$factory->uuidFromHashedName($hex, 4);
    }

    /**
     * Generate an UUID version 5 based on the SHA-1 hash of a namespace identifier and a domain name.
     *
     * @param string $nsUuid The namespace UUID
     * @param string $domain The domain name
     *
     * @return Uuid
     */
    public static function createV5($nsUuid, $domain): Uuid
    {
        self::create();

        return self::$factory->uuidFromNsAndName($nsUuid, $domain, 5, 'sha1');
    }

    /**
     * Check if a string is a valid UUID.
     *
     * @param string $uuid The string UUID to test
     *
     * @return boolean
     */
    public static function isValid($uuid): bool
    {
        $uuid = str_replace(['urn:', 'uuid:', '{', '}'], '', $uuid);

        if ($uuid == self::NIL) {
            return true;
        }

        if (!preg_match('/' . self::VALID_PATTERN . '/', $uuid)) {
            return false;
        }

        return true;
    }

    /**
     * Returns a <var>Uuid</var> created from `$hash` with the version field set to `$version`
     * and the variant field set for RFC 4122.
     *
     * @param string  $hash    The hash to use when creating the UUID
     * @param integer $version The UUID version to set for this hash (1, 3, 4, or 5)
     *
     * @return Uuid
     */
    public function uuidFromHashedName($hash, $version): Uuid
    {
        $timeHi = static::applyVersion(substr($hash, 12, 4), $version);
        $clockSeqHi = static::applyVariant(hexdec(substr($hash, 16, 2)));

        $fields = array(
            'time_low'            => substr($hash, 0, 8),
            'time_mid'            => substr($hash, 8, 4),
            'time_hi_and_version' => sprintf('%04x', $timeHi),
            'clock_seq_hi'        => sprintf('%02x', $clockSeqHi),
            'clock_seq_low'       => substr($hash, 18, 2),
            'node'                => substr($hash, 20, 12)
        );

        return new Uuid($fields, $version);
    }

    /**
     * Returns a version 3 or 5 namespaced `Uuid`
     *
     * @param string  $nsUuid       The UUID namespace to use
     * @param string  $domain       The string to hash together with the namespace
     * @param integer $version      The version of UUID to create (3 or 5)
     * @param string  $hashFunction The hash function to use when hashing together
     *                              the namespace and name
     *
     * @return Uuid
     */
    public function uuidFromNsAndName($nsUuid, $domain, $version, $hashFunction): Uuid
    {
        $uuid = str_replace(['urn:', 'uuid:', '{', '}',], '', $nsUuid);
        if (!static::isValid($uuid)) {
            throw new \InvalidArgumentException('Invalid UUID string: ' . $nsUuid);
        }

        $hash = call_user_func($hashFunction, (hex2bin(str_replace('-', '', $uuid)) . $domain));

        return $this->uuidFromHashedName($hash, $version);
    }

    /**
     * Create the UUID factory instance.
     *
     * @return UuidFactory
     */
    private static function create(): UuidFactory
    {
        if (!self::$factory) {
            self::$factory = new UuidFactory();
        }

        return self::$factory;
    }

}