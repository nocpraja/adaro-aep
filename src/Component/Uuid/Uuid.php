<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Uuid;


/**
 * Class Uuid
 *
 * @package App\Component\Uuid
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 24/07/2018 12:25
 */
final class Uuid implements \JsonSerializable
{
    /**
     * When this namespace is specified, the name string is a fully-qualified domain name.
     * @link http://tools.ietf.org/html/rfc4122#appendix-C
     */
    const NAMESPACE_DNS = '6ba7b810-9dad-11d1-80b4-00c04fd430c8';

    /**
     * When this namespace is specified, the name string is a URL.
     * @link http://tools.ietf.org/html/rfc4122#appendix-C
     */
    const NAMESPACE_URL = '6ba7b811-9dad-11d1-80b4-00c04fd430c8';

    /**
     * When this namespace is specified, the name string is an ISO OID.
     * @link http://tools.ietf.org/html/rfc4122#appendix-C
     */
    const NAMESPACE_OID = '6ba7b812-9dad-11d1-80b4-00c04fd430c8';

    /**
     * When this namespace is specified, the name string is an X.500 DN in DER or a text output format.
     * @link http://tools.ietf.org/html/rfc4122#appendix-C
     */
    const NAMESPACE_X500 = '6ba7b814-9dad-11d1-80b4-00c04fd430c8';

    /**
     * The UUID parts.
     *
     * @var array
     */
    private $fields = array(
        'time_low'            => '00000000',
        'time_mid'            => '0000',
        'time_hi_and_version' => '0000',
        'clock_seq_hi'        => '00',
        'clock_seq_low'       => '00',
        'node'                => '000000000000',
    );

    /**
     * The UUID version.
     *
     * @var integer
     */
    private $version;


    /**
     * Uuid constructor.
     *
     * @param array   $fields
     * @param integer $version
     */
    public function __construct(array $fields, int $version)
    {
        if (!$this->isValid($fields)) {
            throw new \InvalidArgumentException('Invalid UUID fields!');
        }

        $this->fields = $fields;
        $this->version = $version;
    }

    /**
     * Converts this UUID object to a string when the object is used in any string context.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * Get the UUID version.
     *
     * @return integer
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * Converts this UUID object to a string when the object is serialized with `json_encode()`.
     *
     * @return string
     * @link http://php.net/manual/en/class.jsonserializable.php
     */
    public function jsonSerialize(): string
    {
        return $this->toString();
    }

    /**
     * Converts this UUID object to a string when the object is serialized with `serialize()`.
     *
     * @return string
     * @link http://php.net/manual/en/class.serializable.php
     */
    public function serialize(): string
    {
        return $this->toString();
    }

    /**
     * Converts this UUID object to a string.
     *
     * @return string
     */
    public function toString(): string
    {
        return vsprintf('%08s-%04s-%04s-%02s%02s-%012s', array_values($this->fields));
    }

    /**
     * Dirty check whether the given fields is valid or not.
     *
     * @param array $fields
     *
     * @return boolean
     */
    private function isValid(array $fields): bool
    {
        return (isset($fields['time_low']) && isset($fields['time_mid']) && isset($fields['time_hi_and_version'])
            && isset($fields['clock_seq_hi']) && isset($fields['clock_seq_low']) && isset($fields['node']));
    }

}