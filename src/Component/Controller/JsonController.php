<?php
/*
 * Web aplikasi Adaro Education Program.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Controller;


use App\Component\DataObject\DataQuery;
use App\Component\DataObject\SortOrFilter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Traversable;

/**
 * Class JsonController.
 * Semua API controller pada web aplikasi harus extends dari Class ini.
 * @package App\Component\Controller
 * @author  Ahmad Fajar
 * @since   01/08/2018, modified: 04/03/2020 22:36
 */
abstract class JsonController extends AbstractController
{
    const CONTEXT_NON_REL = 'without_rel';
    const CONTEXT_WITH_REL = 'with_rel';
    const CONTEXT_WITH_PARENT = 'with_parent';
    const CONTEXT_WITH_CHILD = 'with_child';
    const CONTEXT_WITH_POSTDATE = 'with_postdate';
    const CONTEXT_WITH_APPROVAL = 'with_approval';

    /**
     * Normalizer annotation context options for entities without relation.
     *
     * @var array
     */
    protected $context_norels = ['groups' => ['without_rel']];

    /**
     * Normalizer annotation context options for entities with relation.
     *
     * @var array
     */
    protected $context_withrels = ['groups' => ['with_rel']];

    /**
     * Normalizer annotation context options for entities with parent relation.
     *
     * @var array
     */
    protected $context_parentrels = ['groups' => ['without_rel', 'with_parent']];

    /**
     * Normalizer annotation context options for entities with child relation, auditor and publication date.
     *
     * @var array
     */
    protected $context_customrels = ['groups' => ['with_rel', 'with_child', 'with_postdate']];

    /**
     * Normalizer annotation context options for entities with auditor, publication date and approval.
     *
     * @var array
     */
    protected $context_approvalrels = ['groups' => ['with_rel', 'with_postdate', 'with_approval']];

    /**
     * Returns the annotation context groups to be used with the normalizer metadata context options.
     *
     * @param string[] $context
     *
     * @return array
     */
    final protected function createNormalizerContext(array $context = []): array
    {
        return (empty($context) ? [] : ['groups' => $context]);
    }

    /**
     * Get the encoders to be used with the Serializer.
     *
     * @return array
     */
    final protected function getEncoders(): array
    {
        return [new JsonEncoder(), new XmlEncoder()];
    }

    /**
     * Returns a json response error message.
     *
     * @param string $message The error message.
     * @param int    $status  HTTP response status code
     *
     * @return JsonResponse
     */
    final protected function jsonError(string $message, int $status = JsonResponse::HTTP_OK): JsonResponse
    {
        return $this->json(['success' => false, 'message' => $message], $status);
    }

    /**
     * Returns a JsonResponse that uses the serializer component if enabled, or json_encode.
     *
     * @param array|object|Traversable $data    The given data records
     * @param int                      $total   Total records or number of elements in the <var>$data</var>
     * @param array                    $context The serializer context to be used
     * @param array                    $headers Custom HTTP Headers
     * @param int                      $status  HTTP response status code
     *
     * @return JsonResponse
     */
    final protected function jsonResponse($data, int $total = -1, array $context = [],
                                          array $headers = [], int $status = JsonResponse::HTTP_OK): JsonResponse
    {
        $_data = $this->wrapResult($data, $total);
        $_context = $context;

        if (!empty($context)) {
            if (!isset($context['groups'])) {
                $_context = $this->createNormalizerContext($context);
            }
        }

        return $this->json($_data, $status, $headers, array_merge(['enable_max_depth' => true], $_context));
    }

    /**
     * Returns a json response success message.
     *
     * @param string $message The success message.
     * @param int    $status  HTTP response status code
     *
     * @return JsonResponse
     */
    final protected function jsonSuccess(string $message = '', int $status = JsonResponse::HTTP_OK): JsonResponse
    {
        return $this->json(['success' => true, 'message' => $message], $status);
    }

    /**
     * Parse request query parameters and find spesific parameter.
     *
     * @param Request        $request HTTP request object
     * @param string         $search  The parameter to search
     * @param SortOrFilter[] $default Default sorts or filters criteria
     *
     * @return array|SortOrFilter[]
     *
     * @deprecated Use {@see DataQuery::parseSortOrFilter}
     */
    final protected function parseSortOrFilter(Request $request, string $search, array $default = []): array
    {
        return DataQuery::parseSortOrFilter($request, $search, $default);
    }

    /**
     * Wrap or decorate <var>$data</var> with something useful.
     * The returns result can be used for creating JsonResponse.
     * Here an example how to use it.
     *
     * <code>
     * $ret = $this->wrapResult($data, 100);
     * return $this->json($ret);
     * </code>
     *
     * @param array|Traversable $data  The data to decorate.
     * @param int               $total Total records or number of elements in the <var>$data</var>
     *
     * @return array
     */
    final protected function wrapResult($data, int $total = -1): array
    {
        $ret = ['success' => true, 'data' => $data];
        if ($total > -1) {
            $ret['total'] = $total;
        }

        return $ret;
    }


}
