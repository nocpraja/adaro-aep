<?php
/*
 * Web aplikasi Adaro Education Program.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Controller;


use App\Component\DataObject\ActionObject;
use App\Entity\Pendanaan\EntityDanaInterface;
use App\Models\Pendanaan\PendanaanDomainModel;
use Exception;
use LogicException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class PendanaanController digunakan untuk menyederhanakan dan menyeragamkan pola
 * API pada modul pendanaan Bidang, Program dan Batch.
 *
 * @package App\Component\Controller
 * @author  Ahmad Fajar
 * @since   05/05/2019, modified: 05/05/2019 14:20
 */
abstract class PendanaanController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var PendanaanDomainModel
     */
    protected $model;

    /**
     * Validate the submitted form data and create record pendanaan.
     *
     * @param FormInterface       $form    Symfony form instance
     * @param EntityDanaInterface $entity  Unmanaged entity object
     * @param array               $context Array of normalizer annotation context to be used
     *
     * @return JsonResponse
     */
    protected function invokeCreate(FormInterface $form, EntityDanaInterface $entity,
                                    array $context = [self::CONTEXT_NON_REL]): JsonResponse
    {
        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->create($entity);
            return $this->jsonResponse($entity, -1, $context);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Validate the persistent object whether it is NULL or not. If it is NOT NULL then remove it from database.
     *
     * @param string                   $kategori Kategori pendanaan
     * @param EntityDanaInterface|null $entity   Persistent entity object
     *
     * @return JsonResponse
     */
    protected function invokeDelete(string $kategori, ?EntityDanaInterface $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError(sprintf("Data %s tidak ditemukan.", $kategori),
                                    JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->model->delete($entity);
            return $this->jsonSuccess(sprintf("Data %s telah berhasil dihapus dari database", $kategori));
        } catch (LogicException $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError(sprintf("Data %s tidak boleh dihapus, masih ada record yang terkait dengannya.",
                                            $kategori), JsonResponse::HTTP_FORBIDDEN);
        }
    }

    /**
     * Apply some process to the record Pendanaan after loading it from database then returns as response object.
     *
     * @param string                   $kategori Kategori pendanaan
     * @param EntityDanaInterface|null $entity   Persistent entity object
     * @param array                    $context  Array of normalizer annotation context to be used
     *
     * @return JsonResponse
     */
    protected function invokeFetch(string $kategori, ?EntityDanaInterface $entity,
                                   array $context = [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError(sprintf("Data %s tidak ditemukan.", $kategori),
                                    JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $context);
    }

    /**
     * Validate the submitted form data dan simpan request revisi anggaran Pendanaan.
     *
     * @param FormInterface       $form    Symfony form interface
     * @param EntityDanaInterface $entity  Persistent entity object
     * @param array               $context Array of normalizer annotation context to be used
     *
     * @return JsonResponse
     */
    protected function invokeRevisi(FormInterface $form, EntityDanaInterface $entity,
                                    array $context = [self::CONTEXT_NON_REL]): JsonResponse
    {
        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->revisi($entity);
            return $this->jsonResponse($entity, -1, $context);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Validate the submitted form data dan simpan perubahan record Pendanaan.
     *
     * @param FormInterface       $form    Symfony form interface
     * @param EntityDanaInterface $entity  Persistent entity object
     * @param array               $context Array of normalizer annotation context to be used
     *
     * @return JsonResponse
     */
    protected function invokeUpdate(FormInterface $form, EntityDanaInterface $entity,
                                    array $context = [self::CONTEXT_NON_REL]): JsonResponse
    {
        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->update($entity);
            return $this->jsonResponse($entity, -1, $context);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Validate the submitted form data and apply workflow transition pada record Pendanaan.
     *
     * @param FormInterface       $form    Symfony form interface
     * @param EntityDanaInterface $entity  Persistent entity object
     * @param ActionObject        $data    The form data
     * @param array               $context Array of normalizer annotation context to be used
     *
     * @return JsonResponse
     */
    protected function invokeWorkflowTransition(FormInterface $form, EntityDanaInterface $entity, ActionObject $data,
                                                array $context = [self::CONTEXT_NON_REL]): JsonResponse
    {
        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            return $this->jsonResponse($entity, -1, $context);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}