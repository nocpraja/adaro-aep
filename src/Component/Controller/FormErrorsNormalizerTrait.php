<?php
/*
 * Web aplikasi Adaro Education Program.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Component\Controller;

use Symfony\Component\Form\FormInterface;

/**
 * Trait FormErrorsNormalizerTrait
 *
 * @package App\Component\Controller
 * @author  Mark Melvin
 * @since   5/23/2019, modified: 25/06/2019 3:37
 */
trait FormErrorsNormalizerTrait
{
    /**
     * Normalize pesan error setelah form divalidasi.
     *
     * @param FormInterface $form Form yang telah divalidasi
     *
     * @return string Pesan error yang telah dinormalisasi
     */
    protected function normalizeFormErrors(FormInterface $form): string
    {
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                foreach ($childForm->getErrors() as $error) {
                    // Langsung mengembalikan error pertama yang ditemukan
                    return $this->formatErrorMessage($childForm->getName(), $error->getMessage());
                }

                foreach ($childForm->all() as $grandchildForm) {
                    if ($grandchildForm instanceof FormInterface) {
                        foreach ($grandchildForm->getErrors() as $childError) {
                            // Apabila tidak ditemukan di level pertama, ambil di form di bawahnya.
                            // Cukup sampai dua level saja tidak rekursif, sesuai form yang ada di aplikasi
                            return $this->formatErrorMessage($childForm->getName() . ':' . $grandchildForm->getName(),
                                                             $childError->getMessage());
                        }
                    }
                }
            }
        }
        // Apabila tidak tertangkap di dua level tersebut, berikan pesan error default.
        return 'Ada kesalahan dalam pengisian data';
    }

    /**
     * Format pesan error.
     *
     * @param string $namaField Nama Field yang terdapat kesalahan
     * @param string $pesan     Pesan kesalahan yang terjadi
     *
     * @return string
     */
    private function formatErrorMessage($namaField, $pesan): string
    {
        return 'ERROR ' . $namaField . ': ' . $pesan;
    }

}