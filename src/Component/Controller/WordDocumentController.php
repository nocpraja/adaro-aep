<?php

/*
 * Web aplikasi Adaro Education Program.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Controller;

use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class WordDocumentController.
 *
 * Controller untuk generate dan donload Word document
 *
 * @package App\Component\Controller
 * @author  Mark Melvin
 * @since   12/11/2019, modified: 12/11/2019 15:05
 */

abstract class WordDocumentController extends AbstractController
{
    // di mana mencari template file excel
    private $templatePrefix = "../templates/word/";

    private $mimetype = 'application/vnd.ms-word';

    /**
     * load template untuk laporan
     * @param string $filename Nama file template
     * @return TemplateProcessor
     */

    final protected function loadTemplate ($filename) : TemplateProcessor {
        $fullname = $this->templatePrefix.$filename;

        try {
            $template = new TemplateProcessor($fullname);
        } catch (CopyFileException $e) {
            return null;
        } catch (CreateTemporaryFileException $e) {
            return null;
        }

        return $template;
    }


    /**
     * Membentuk response untuk download file laporan yang disusun
     *
     * @param TemplateProcessor $processor
     * @param string $filename
     *
     * @return Response
     *
     */
    final protected function streamResponse(TemplateProcessor $processor, string $filename) : Response
    {
        try {
            $response = new StreamedResponse(
                function () use ($processor) {
                    $processor->saveAs('php://output');
                }
            );
            $response->headers->set('Content-Type', $this->mimetype);
            $response->headers->set('Content-Disposition',
                'attachment;filename="' . $filename .  '"');
            $response->headers->set('Cache-Control', 'max-age=0');
            return $response;
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Returns a json response error message.
     *
     * @param string $message The error message.
     * @param int    $status  HTTP response status code
     *
     * @return Response
     */
    final protected function jsonError(string $message, int $status = Response::HTTP_OK): Response
    {
        return $this->json(['success' => false, 'message' => $message], $status);
    }


}