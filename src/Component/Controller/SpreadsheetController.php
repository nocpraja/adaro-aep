<?php
/*
 * Web aplikasi Adaro Education Program.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Component\Controller;

use App\Component\DataObject\DataQuery;
use App\Component\DataObject\SortOrFilter;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class SpreadsheetController.
 *
 * Controller untuk generate report dan donload spreadsheet
 *
 * @package App\Component\Controller
 * @author  Mark Melvin
 * @since   12/11/2019, modified: 12/11/2019 15:05
 */
abstract class SpreadsheetController extends AbstractController
{
    // di mana mencari template file excel
    private $templatePrefix = "../templates/spreadsheet/";

    // format code untuk jumlah uang
    const FORMAT_ACCOUNTING_RP = '_("Rp"* #,##0.00_);_("Rp"* \(#,##0.00\);_("Rp"* "-"??_);_(@_)';

    /**
     * load template untuk laporan
     * @param string $filename Nama file template
     * @return Spreadsheet
     */

    final protected function load ($filename) : Spreadsheet {
        $fullname = $this->templatePrefix.$filename;

        try {
            $spreadsheet = IOFactory::load($fullname);
        }
        catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $ex) {
            return new Spreadsheet();
        }

        return $spreadsheet;
    }

    /**
     * Tentukan extension menurut file type
     * @param string $filetype
     * @return string
     * @throws
     */
    private function selectExtension(string $filetype) : string
    {
        if (strtolower($filetype) === "xlsx") {
            return '.xlsx';
        }
        else if (strtolower($filetype) === "xls") {
            return '.xls';
        }
        else  if (strtolower($filetype) === "pdf") {
            return '.pdf';
        }
        else  {
            throw new \InvalidArgumentException("Jenis file spreadsheet tidak didukung.");
        }
    }

    /**
     * Tentukan extension menurut file type
     * @param string $filetype
     * @return string
     * @throws
     */
    private function selectMime(string $filetype) : string
    {
        if (strtolower($filetype) === "xls") {
            return 'application/vnd.ms-excel';
        }
        else if (strtolower($filetype) === "xlsx") {
            return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        }
        else  if (strtolower($filetype) === "pdf")  {
            return 'application/pdf';
        }
        else  {
            throw new \InvalidArgumentException("Jenis file spreadsheet tidak didukung.");
        }
    }



    /**
     * Membentuk response untuk download file laporan yang disusun
     *
     * @param Spreadsheet $spreadsheet
     * @param string $filename
     * @param string $filetype
     *
     * @return Response
     *
     */
    final protected function streamResponse(Spreadsheet $spreadsheet, string $filename, string $filetype) : Response
    {
        try {
            $extension = $this->selectExtension($filetype);
            if ($extension === '.pdf') {
                IOFactory::registerWriter('pdf', \PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf::class);
            }
            $writer = IOFactory::createWriter($spreadsheet, $filetype);
            $response = new StreamedResponse(
                function () use ($writer) {
                    $writer->save('php://output');
                }
            );
            $mimetype = $this->selectMime($filetype);
            $response->headers->set('Content-Type', $mimetype);
            $response->headers->set('Content-Disposition',
                'attachment;filename="' . $filename . $extension . '"');
            $response->headers->set('Cache-Control', 'max-age=0');
            return $response;
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Parse request query parameters and find spesific parameter.
     *
     * @param Request        $request HTTP request object
     * @param string         $search  The parameter to search
     * @param SortOrFilter[] $default Default sorts or filters criteria
     *
     * @return array|SortOrFilter[]
     *
     * @deprecated Use {@see DataQuery::parseSortOrFilter}
     */
    final protected function parseSortOrFilter(Request $request, string $search, array $default = []): array
    {
        return DataQuery::parseSortOrFilter($request, $search, $default);
    }

    /**
     * Returns a json response error message.
     *
     * @param string $message The error message.
     * @param int    $status  HTTP response status code
     *
     * @return Response
     */
    final protected function jsonError(string $message, int $status = Response::HTTP_OK): Response
    {
        return $this->json(['success' => false, 'message' => $message], $status);
    }



}