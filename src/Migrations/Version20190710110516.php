<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190710110516 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $menus = [
            [8, 'Kendali Proyek', '', '', 'Kendali Proyek', 't', 3, 3, NULL, NULL],
            [24, 'Rencana', '/kegiatan/kendali-proyek/rencana', 'app_kegiatan_kendaliproyek_rencana', 'Rencana', 't', 4, 0, NULL, NULL],
            [24, 'Realisasi', '/kegiatan/kendali-proyek/realisasi', 'app_kegiatan_kendaliproyek_realisasi', 'Realisasi', 't', 5, 0, NULL, NULL],
            [8, 'Pencairan', '/kegiatan/pencairan', 'app_kegiatan_pencairan', 'Pendataan capaian KPI Kegiatan', 't', 3, 0, NULL, NULL],
        ];
        $sql = 'INSERT INTO menu_item (parent_id, menu_label, route_url, route_alias, description, enabled, position, url_type, icon, css_class) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        foreach ($menus as $params) {
            $this->addSql($sql, $params);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
