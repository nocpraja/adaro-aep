<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200517112448 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        $menu1 = [46, 12, 'Konfigurasi KPI', NULL, NULL, 'Pengaturan konfigurasi kpi', NULL, true, 7, 3];
        $this->addSql($sql, $menu1);

        $menu2 = [47, 46, 'Data Source', '/master-data/konfigurasi-kpi/data-source', 'master_data_konfigurasi_kpi_data_source', 'Pengaturan Data Source KPI',NULL, true, 1, 0];
        $this->addSql($sql, $menu2);

        $menu3 = [48, 46, 'Chart', '/master-data/konfigurasi-kpi/chart', 'master_data_konfigurasi_kpi_chart', 'Pengaturan Chart KPI',NULL, true, 2, 0];
        $this->addSql($sql, $menu3);

        $menu4 = [49, 46, 'Report', '/master-data/konfigurasi-kpi/report', 'master_data_konfigurasi_kpi_report', 'Pengaturan Report KPI',NULL, true, 3, 0];
        $this->addSql($sql, $menu4);

        $menu5 = [50, 46, 'Layout', '/master-data/konfigurasi-kpi/layout', 'master_data_konfigurasi_kpi_layout', 'Pengaturan Layout KPI',NULL, true, 4, 0];
        $this->addSql($sql, $menu5);

        $menu6 = [51, 46, 'Dashboard', '/master-data/konfigurasi-kpi/dashboard', 'master_data_konfigurasi_kpi_dashboard', 'Pengaturan Dashboard KPI',NULL, true, 5, 0];
        $this->addSql($sql, $menu6);

        $this->addSql('CREATE TABLE kpi_conf_datasource (
                            id BIGSERIAL NOT NULL, 
                            name VARCHAR(150) NOT NULL, 
                            tipe VARCHAR(150) NOT NULL, 
                            query TEXT NOT NULL, 
                            keterangan TEXT DEFAULT NULL, 
                            PRIMARY KEY(id))');

        $this->addSql('CREATE TABLE kpi_conf_chart_type (
                            id BIGSERIAL NOT NULL, 
                            name VARCHAR(150) NOT NULL,
                            config TEXT DEFAULT NULL,
                            PRIMARY KEY(id))');

        $this->addSql('CREATE TABLE kpi_conf_chart (
                            id BIGSERIAL NOT NULL, 
                            name VARCHAR(150) NOT NULL,
                            tipe VARCHAR(150) DEFAULT NULL,
                            type_id BIGINT DEFAULT NULL,
                            datasource_id BIGINT NOT NULL,
                            data_conf JSONB DEFAULT NULL,
                            axis_conf JSONB DEFAULT NULL,
                            keterangan TEXT DEFAULT NULL, 
                            PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_chart ON kpi_conf_chart (datasource_id)');
        $this->addSql('ALTER TABLE kpi_conf_chart ADD CONSTRAINT FK_CHARTDATASOURCE FOREIGN KEY (datasource_id) REFERENCES kpi_conf_datasource (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('CREATE TABLE kpi_conf_report (
                            id BIGSERIAL NOT NULL, 
                            name VARCHAR(150) NOT NULL,
                            title VARCHAR(150) DEFAULT NULL,
                            datasource_id BIGINT NOT NULL,
                            filtering JSONB DEFAULT NULL,
                            sorting JSONB DEFAULT NULL,
                            PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_report ON kpi_conf_report (datasource_id)');
        $this->addSql('ALTER TABLE kpi_conf_report ADD CONSTRAINT FK_REPORTDATASOURCE FOREIGN KEY (datasource_id) REFERENCES kpi_conf_datasource (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('CREATE TABLE kpi_conf_layout (
                            id BIGSERIAL NOT NULL, 
                            name VARCHAR(150) NOT NULL,
                            template_editor TEXT DEFAULT NULL,
                            keterangan TEXT DEFAULT NULL, 
                            PRIMARY KEY(id))');

        $this->addSql('CREATE TABLE kpi_conf_dashboard (
                            id BIGSERIAL NOT NULL, 
                            name VARCHAR(150) NOT NULL,
                            template_id BIGINT NOT NULL,
                            display_type VARCHAR(50) DEFAULT NULL,
                            display_source BIGINT DEFAULT NULL,
                            component JSONB DEFAULT NULL,
                            keterangan TEXT DEFAULT NULL,
                            PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_dashboard ON kpi_conf_dashboard (template_id)');
        $this->addSql('ALTER TABLE kpi_conf_dashboard ADD CONSTRAINT FK_LAYOUTDASHBOARD FOREIGN KEY (template_id) REFERENCES kpi_conf_layout (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('DROP SEQUENCE kpi_conf_dashboard_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE kpi_conf_layout_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE kpi_conf_report_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE kpi_conf_chart_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE kpi_conf_datasource_id_seq CASCADE');
        $this->addSql('DROP INDEX idx_dashboard');
        $this->addSql('DROP INDEX idx_report');
        $this->addSql('DROP INDEX idx_chart');
        $this->addSql('ALTER TABLE kpi_conf_dashboard DROP CONSTRAINT FK_LAYOUTDASHBOARD');
        $this->addSql('ALTER TABLE kpi_conf_report DROP CONSTRAINT FK_REPORTDATASOURCE');
        $this->addSql('ALTER TABLE kpi_conf_chart DROP CONSTRAINT FK_CHARTDATASOURCE');
        $this->addSql('DROP TABLE kpi_conf_dashboard');
        $this->addSql('DROP TABLE kpi_conf_layout');
        $this->addSql('DROP TABLE kpi_conf_report');
        $this->addSql('DROP TABLE kpi_conf_chart');
        $this->addSql('DROP TABLE kpi_conf_datasource');
    }
}
