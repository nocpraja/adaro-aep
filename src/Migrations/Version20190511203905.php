<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190511203905 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE dana_batch_donatur (ids BIGSERIAL NOT NULL, bp_id BIGINT NOT NULL, ref_id INT NOT NULL, jumlah DOUBLE PRECISION NOT NULL, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX dana_batch_donatur_x1 ON dana_batch_donatur (ref_id)');
        $this->addSql('CREATE INDEX dana_batch_donatur_x2 ON dana_batch_donatur (bp_id)');
        $this->addSql('ALTER TABLE dana_batch_donatur ADD CONSTRAINT FK_B21414A0D7EF6F8E FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_batch_donatur ADD CONSTRAINT FK_B21414A021B741A9 FOREIGN KEY (ref_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('INSERT INTO dana_batch_donatur SELECT * FROM donatur_sumber_dana');
        $this->addSql('SELECT SETVAL(\'dana_batch_donatur_ids_seq\', (SELECT MAX(ids) FROM donatur_sumber_dana), true)');
        $this->addSql('COMMENT ON TABLE dana_batch_donatur IS \'Tabel jumlah dana yang diberikan oleh Perusahaan Donatur\'');

        $this->addSql('CREATE TABLE dana_batch_revisi (ids BIGSERIAL NOT NULL, bp_id BIGINT NOT NULL, refcode VARCHAR(25) NOT NULL, title VARCHAR(255) NOT NULL, jumlah DOUBLE PRECISION NOT NULL, posted_by VARCHAR(50), posted_date TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX dana_batch_revisi_x1 ON dana_batch_revisi (bp_id)');
        $this->addSql('CREATE INDEX dana_batch_revisi_x2 ON dana_batch_revisi (refcode)');
        $this->addSql('ALTER TABLE dana_batch_revisi ADD CONSTRAINT FK_F9B4E58AD7EF6F8E FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('INSERT INTO dana_batch_revisi SELECT * FROM anggaran_revisi_dana_batch');
        $this->addSql('SELECT SETVAL(\'dana_batch_revisi_ids_seq\', (SELECT MAX(ids) FROM anggaran_revisi_dana_batch), true)');
        $this->addSql('COMMENT ON TABLE dana_batch_revisi IS \'Data revisi anggaran untuk request revisi Dana Batch\'');

        $this->addSql('DROP SEQUENCE anggaran_revisi_dana_batch_ids_seq CASCADE');
        $this->addSql('DROP SEQUENCE donatur_sumber_dana_ids_seq CASCADE');
        $this->addSql('DROP TABLE anggaran_revisi_dana_batch');
        $this->addSql('DROP TABLE donatur_sumber_dana');

        $this->dropTriggers();
        $this->createTriggers();
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE anggaran_revisi_dana_batch (ids BIGSERIAL NOT NULL, bp_id BIGINT NOT NULL, refcode VARCHAR(25) NOT NULL, title VARCHAR(255) NOT NULL, jumlah DOUBLE PRECISION NOT NULL, posted_by VARCHAR(50), posted_date TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX anggaran_revisi_dana_batch_x2 ON anggaran_revisi_dana_batch (refcode)');
        $this->addSql('CREATE INDEX anggaran_revisi_dana_batch_x1 ON anggaran_revisi_dana_batch (bp_id)');
        $this->addSql('ALTER TABLE anggaran_revisi_dana_batch ADD CONSTRAINT fk_8de1feddd7ef6f8e FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('INSERT INTO anggaran_revisi_dana_batch SELECT * FROM dana_batch_revisi');
        $this->addSql('SELECT SETVAL(\'anggaran_revisi_dana_batch_ids_seq\', (SELECT MAX(ids) FROM dana_batch_revisi), true)');
        $this->addSql('COMMENT ON TABLE anggaran_revisi_dana_batch IS \'Data revisi anggaran untuk request revisi Dana Batch\'');

        $this->addSql('CREATE TABLE donatur_sumber_dana (ids BIGSERIAL NOT NULL, bp_id BIGINT NOT NULL, ref_id INT NOT NULL, jumlah DOUBLE PRECISION NOT NULL, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX donatur_sumber_dana_x1 ON donatur_sumber_dana (ref_id)');
        $this->addSql('CREATE INDEX donatur_sumber_dana_x2 ON donatur_sumber_dana (bp_id)');
        $this->addSql('ALTER TABLE donatur_sumber_dana ADD CONSTRAINT fk_cbe4157fd7ef6f8e FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE donatur_sumber_dana ADD CONSTRAINT fk_cbe4157f21b741a9 FOREIGN KEY (ref_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('INSERT INTO donatur_sumber_dana SELECT * FROM dana_batch_donatur');
        $this->addSql('SELECT SETVAL(\'donatur_sumber_dana_ids_seq\', (SELECT MAX(ids) FROM dana_batch_donatur), true)');
        $this->addSql('COMMENT ON TABLE donatur_sumber_dana IS \'Tabel jumlah dana yang diberikan oleh Perusahaan Donatur\'');

        $this->addSql('DROP TABLE dana_batch_donatur');
        $this->addSql('DROP TABLE dana_batch_revisi');

        $this->dropTriggers();
        $this->restoreOldTriggers();
    }

    private function dropTriggers(): void
    {
        $this->addSql('DROP TRIGGER trg_dana_batch ON dana_batch');
        $this->addSql('DROP TRIGGER trg_dana_program ON dana_program');
        $this->addSql('DROP FUNCTION tp_update_dana_batch()');
        $this->addSql('DROP FUNCTION tp_update_dana_program()');
    }

    private function createTriggers(): void
    {
        $this->addSql('CREATE OR REPLACE FUNCTION tp_update_dana_batch() RETURNS trigger AS $$
BEGIN
    IF (TG_OP = \'UPDATE\') THEN
        UPDATE dana_batch_revisi SET title = NEW.nama_program WHERE refcode = OLD.kode;
    END IF;
    RETURN NULL;
END;
$$ language \'plpgsql\'');

        $this->addSql('CREATE OR REPLACE FUNCTION tp_update_dana_program() RETURNS trigger AS $$
BEGIN
    IF (TG_OP = \'UPDATE\') THEN
        UPDATE dana_batch_revisi SET title = NEW.title WHERE refcode = OLD.kode;
    END IF;
    RETURN NULL;
END;
$$ language \'plpgsql\'');

        $this->addSql('CREATE TRIGGER trg_dana_batch AFTER UPDATE OF nama_program ON dana_batch FOR EACH ROW EXECUTE PROCEDURE tp_update_dana_batch()');
        $this->addSql('CREATE TRIGGER trg_dana_program AFTER UPDATE OF title ON dana_program FOR EACH ROW EXECUTE PROCEDURE tp_update_dana_program()');
    }

    private function restoreOldTriggers(): void
    {
        $this->addSql('CREATE OR REPLACE FUNCTION tp_update_dana_batch() RETURNS trigger AS $$
BEGIN
    IF (TG_OP = \'UPDATE\') THEN
        UPDATE anggaran_revisi_dana_batch SET title = NEW.nama_program WHERE refcode = OLD.kode;
    END IF;
    RETURN NULL;
END;
$$ language \'plpgsql\'');

        $this->addSql('CREATE OR REPLACE FUNCTION tp_update_dana_program() RETURNS trigger AS $$
BEGIN
    IF (TG_OP = \'UPDATE\') THEN
        UPDATE anggaran_revisi_dana_batch SET title = NEW.title WHERE refcode = OLD.kode;
    END IF;
    RETURN NULL;
END;
$$ language \'plpgsql\'');

        $this->addSql('CREATE TRIGGER trg_dana_batch AFTER UPDATE OF nama_program ON dana_batch FOR EACH ROW EXECUTE PROCEDURE tp_update_dana_batch()');
        $this->addSql('CREATE TRIGGER trg_dana_program AFTER UPDATE OF title ON dana_program FOR EACH ROW EXECUTE PROCEDURE tp_update_dana_program()');
    }

}
