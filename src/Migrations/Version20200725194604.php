<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200725194604 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE pencairan_realisasi (itempencairan_id BIGINT NOT NULL, realisasi_id BIGINT NOT NULL, PRIMARY KEY(itempencairan_id, realisasi_id))');
        $this->addSql('CREATE INDEX IDX_F97BB55EC827D9BD ON pencairan_realisasi (itempencairan_id)');
        $this->addSql('CREATE INDEX IDX_F97BB55EA6856DED ON pencairan_realisasi (realisasi_id)');
        $this->addSql('ALTER TABLE pencairan_realisasi ADD CONSTRAINT FK_F97BB55EC827D9BD FOREIGN KEY (itempencairan_id) REFERENCES pencairan_items (itempencairan_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan_realisasi ADD CONSTRAINT FK_F97BB55EA6856DED FOREIGN KEY (realisasi_id) REFERENCES item_realisasi (id_realisasi) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan_items DROP CONSTRAINT fk_df9eee7a02a47e6');
        $this->addSql('DROP INDEX idx_df9eee7a02a47e6');
        $this->addSql('ALTER TABLE pencairan_items DROP itemrealisasi_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE pencairan_realisasi');
        $this->addSql('ALTER TABLE pencairan_items ADD itemrealisasi_id BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE pencairan_items ADD CONSTRAINT fk_df9eee7a02a47e6 FOREIGN KEY (itemrealisasi_id) REFERENCES item_realisasi (id_realisasi) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_df9eee7a02a47e6 ON pencairan_items (itemrealisasi_id)');
    }
}
