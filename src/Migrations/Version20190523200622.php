<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190523200622 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE beneficiary_individu ADD kompetensi_wirausaha JSONB');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.kompetensi_wirausaha IS \'Keahlian/Kompetensi Kewirausahaan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.kategori IS \'Valid values:
            1 = SANTRI
            2 = SISWA
            3 = MAHASISWA
            4 = USTADZ
            5 = GURU
            6 = DOSEN
            7 = MANAJEMEN
            8 = USTADZ & INSTRUKTUR\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE beneficiary_individu DROP kompetensi_wirausaha');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.kategori IS \'Valid values:
                    1 = SANTRI
                    2 = SISWA
                    3 = MAHASISWA
                    4 = USTADZ
                    5 = GURU
                    6 = DOSEN
                    7 = MANAJEMEN\'');
    }

}
