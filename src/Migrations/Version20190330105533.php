<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190330105533 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE menu_access_rule (rule_id BIGSERIAL NOT NULL, group_id INT NOT NULL, menu_id INT NOT NULL, access JSONB, PRIMARY KEY(rule_id))');
        $this->addSql('CREATE INDEX menu_access_x1 ON menu_access_rule (group_id)');
        $this->addSql('CREATE INDEX menu_access_x2 ON menu_access_rule (menu_id)');
        $this->addSql('COMMENT ON COLUMN menu_access_rule.access IS \'Menu access rule\'');
        $this->addSql('CREATE TABLE menu_item (menu_id SERIAL NOT NULL, parent_id INT DEFAULT NULL, menu_label VARCHAR(250) NOT NULL, route_url VARCHAR(255), route_alias VARCHAR(255), description TEXT, enabled BOOLEAN DEFAULT \'true\' NOT NULL, position INT DEFAULT 0 NOT NULL, url_type SMALLINT DEFAULT 0 NOT NULL, icon VARCHAR(50), css_class VARCHAR(100), PRIMARY KEY(menu_id))');
        $this->addSql('CREATE INDEX menu_item_x1 ON menu_item (parent_id)');
        $this->addSql('CREATE INDEX menu_item_x2 ON menu_item (route_url)');
        $this->addSql('CREATE INDEX menu_item_x3 ON menu_item (route_alias)');
        $this->addSql('CREATE INDEX menu_item_x4 ON menu_item (enabled)');
        $this->addSql('COMMENT ON COLUMN menu_item.route_url IS \'Frontend ataupun VueJs route URL\'');
        $this->addSql('COMMENT ON COLUMN menu_item.route_alias IS \'Symfony route name ataupun Backend API route URL\'');
        $this->addSql('COMMENT ON COLUMN menu_item.url_type IS \'Valid values:
            0 = Route URL internal dan tampil pada MainMenu
            1 = Route URL eksternal dan tampil pada MainMenu
            2 = Route URL internal dan tidak tampil pada MainMenu
            3 = Menu parent, tampil pada MainMenu dan tanpa route URL
            4 = Menu separator, tanpa route URL\'');
        $this->addSql('ALTER TABLE menu_access_rule ADD CONSTRAINT FK_D8FDD486FE54D947 FOREIGN KEY (group_id) REFERENCES user_group (group_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE menu_access_rule ADD CONSTRAINT FK_D8FDD486CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu_item (menu_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE menu_item ADD CONSTRAINT FK_D754D550727ACA70 FOREIGN KEY (parent_id) REFERENCES menu_item (menu_id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE user_account ADD new_email VARCHAR(100) DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN user_account.new_email IS \'Temporary email untuk menyimpan perubahan alamat email sebelum diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN user_account.token_key IS \'Token key untuk reset password atau perubahan alamat email\'');
        $this->addSql('COMMENT ON COLUMN user_account.token_expiry IS \'Token key expiration DateTime\'');

        $this->registerMenuItems();
        $this->registerRoles();
    }

    private function registerMenuItems(): void
    {
        $menus = [
            [1, null, 'Dashboard', '/dashboard', 'default_homepage', 'Landing Page', 'desktop', 'true', 1, 0],

            [2, null, 'Pendanaan', null, null, 'Group menu Pendanaan', 'money-bill', 'true', 2, 3],
            [3, 2, 'Dana Global', '/pendanaan/dana-global', 'app_pendanaan_global', 'Pengelolaan Dana Global', null, 'true', 1, 0],
            [4, 2, 'Dana Bidang', '/pendanaan/bidang', 'app_pendanaan_bidang', 'Pengelolaan Dana Bidang', null, 'false', 2, 0],
            [5, 2, 'Dana Program', '/pendanaan/program', 'app_pendanaan_program', 'Pengelolaan Dana Program CSR', null, 'false', 3, 0],
            [6, 2, 'Dana Batch', '/pendanaan/batch', 'app_pendanaan_batch', 'Pengelolaan Dana Batch CSR', null, 'false', 4, 0],
            [7, 2, 'Dana Program Tahunan', '/pendanaan/program', null, null, null, 'true', 5, 0],

            [8, null, 'Kegiatan', null, null, 'Group menu Kegiatan', 'calendar-check', 'true', 3, 3],
            [9, 8, 'Pengelolaan Kegiatan', '/kegiatan/pengelolaan', 'app_kegiatan_pengelolaan', 'Pengelolaan pengajuan RAB Kegiatan', null, 'true', 1, 0],
            [10, 8, 'Capaian KPI', '/kegiatan/capaian-kpi', 'app_kegiatan_kpi', 'Pendataan capaian KPI Kegiatan', null, 'true', 2, 0],

            [11, null, 'Penerima Manfaat', '/kegiatan/penerima-manfaat', 'app_kegiatan_penerimamanfaat', 'Pengelolaan data Penerima Manfaat', 'book-open', 'true', 4, 0],

            [12, null, 'Master Data', null, null, 'Group menu Master Data', 'database', 'true', 5, 3],
            [13, 12, 'Target KPI', '/master-data/target-kpi', 'app_kegiatan_kpi', 'Pendataan target KPI Kegiatan', null, 'true', 1, 0],
            [14, 12, 'Batch Pembinaan', '/master-data/batch', 'master_data_batchcsr', 'Pengelolaan data Batch Pembinaan', null, 'true', 2, 0],
            [15, 12, 'Hierarki Program', '/master-data/program', 'master_data_csr', 'Pengelolaan data Hirarki Program CSR', null, 'true', 3, 0],
            [16, 12, 'Mitra', '/master-data/mitra', 'master_data_mitra', 'Pengelolaan data Mitra', null, 'true', 4, 0],
            [17, 12, 'Data Referensi', '/master-data/data-reference', 'master_data_reference', 'Pengelolaan data Reference', null, 'true', 5, 0],
            [18, 12, 'Wilayah', '/master-data/wilayah', 'master_data_wilayah', 'Pengelolaan data Wilayah Administratif', null, 'true', 6, 0],

            [19, null, 'Setting', null, null, 'Group menu Konfigurasi dan Security', 'cogs', 'true', 6, 3],
            [20, 19, 'Kelola Pengguna', '/security/members', null, 'Pengelolaan data Pengguna dan Grup Pengguna', null, 'true', 1, 0],
            [21, 20, 'Pengguna', '/security/members/user', 'app_security_user', 'Pengelolaan data Pengguna', null, 'true', 1, 2],
            [22, 20, 'Grup Pengguna', '/security/members/group', 'app_security_group', 'Pengelolaan data Grup Pengguna', null, 'true', 2, 2],
            [23, 19, 'Kelola Hak Akses', '/security/access-rights', 'app_security_acl', 'Pengelolaan hak akses Grup Pengguna', null, 'true', 2, 0],
        ];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        foreach ($menus as $params) {
            $this->addSql($sql, $params);
        }
    }

    private function registerRoles(): void
    {
        $this->addSql('TRUNCATE app_role');
        $this->addSql('INSERT INTO app_role values (1, \'ROLE_USER\', \'ROLE USER\')');
        $this->addSql('INSERT INTO app_role values (2, \'ROLE_MITRA\', \'ROLE MITRA\')');
        $this->addSql('INSERT INTO app_role values (3, \'ROLE_FINANCE\', \'ROLE FINANCE\')');
        $this->addSql('INSERT INTO app_role values (4, \'ROLE_VERIFICATOR\', \'ROLE VERIFICATOR\')');
        $this->addSql('INSERT INTO app_role values (5, \'ROLE_APPROVAL\', \'ROLE APPROVAL\')');
        $this->addSql('INSERT INTO app_role values (6, \'ROLE_SUPERVISOR\', \'ROLE SUPERVISOR\')');
        $this->addSql('INSERT INTO app_role values (7, \'ROLE_ADMIN\', \'ROLE ADMIN\')');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE menu_access_rule DROP CONSTRAINT FK_D8FDD486CCD7E912');
        $this->addSql('ALTER TABLE menu_item DROP CONSTRAINT FK_D754D550727ACA70');
        $this->addSql('DROP TABLE menu_access_rule');
        $this->addSql('DROP TABLE menu_item');
        $this->addSql('ALTER TABLE user_account DROP new_email');
    }
}
