<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200811200352 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D739D91309');
        $this->addSql('ALTER TABLE finalisasi ADD created_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD presubmitted_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD submitted_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD verified1_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD verified2_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD approved1_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD approved2_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD approved3_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD created_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD presubmitted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD submitted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD verified1_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD verified2_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD approved1_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD approved2_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD approved3_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE finalisasi ALTER mitra_id DROP NOT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D7DE12AB56 FOREIGN KEY (created_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D7D699E9C3 FOREIGN KEY (presubmitted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D7641EE842 FOREIGN KEY (submitted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D7FE1963C1 FOREIGN KEY (verified1_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D7ECACCC2F FOREIGN KEY (verified2_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D7FD81F4A6 FOREIGN KEY (approved1_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D7EF345B48 FOREIGN KEY (approved2_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D757883C2D FOREIGN KEY (approved3_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D739D91309 FOREIGN KEY (mitra_id) REFERENCES ms_mitra (mitra_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_CCE459D7DE12AB56 ON finalisasi (created_by)');
        $this->addSql('CREATE INDEX IDX_CCE459D7D699E9C3 ON finalisasi (presubmitted_by)');
        $this->addSql('CREATE INDEX IDX_CCE459D7641EE842 ON finalisasi (submitted_by)');
        $this->addSql('CREATE INDEX IDX_CCE459D7FE1963C1 ON finalisasi (verified1_by)');
        $this->addSql('CREATE INDEX IDX_CCE459D7ECACCC2F ON finalisasi (verified2_by)');
        $this->addSql('CREATE INDEX IDX_CCE459D7FD81F4A6 ON finalisasi (approved1_by)');
        $this->addSql('CREATE INDEX IDX_CCE459D7EF345B48 ON finalisasi (approved2_by)');
        $this->addSql('CREATE INDEX IDX_CCE459D757883C2D ON finalisasi (approved3_by)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D7DE12AB56');
        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D7D699E9C3');
        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D7641EE842');
        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D7FE1963C1');
        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D7ECACCC2F');
        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D7FD81F4A6');
        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D7EF345B48');
        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT FK_CCE459D757883C2D');
        $this->addSql('ALTER TABLE finalisasi DROP CONSTRAINT fk_cce459d739d91309');
        $this->addSql('DROP INDEX IDX_CCE459D7DE12AB56');
        $this->addSql('DROP INDEX IDX_CCE459D7D699E9C3');
        $this->addSql('DROP INDEX IDX_CCE459D7641EE842');
        $this->addSql('DROP INDEX IDX_CCE459D7FE1963C1');
        $this->addSql('DROP INDEX IDX_CCE459D7ECACCC2F');
        $this->addSql('DROP INDEX IDX_CCE459D7FD81F4A6');
        $this->addSql('DROP INDEX IDX_CCE459D7EF345B48');
        $this->addSql('DROP INDEX IDX_CCE459D757883C2D');
        $this->addSql('ALTER TABLE finalisasi DROP created_by');
        $this->addSql('ALTER TABLE finalisasi DROP presubmitted_by');
        $this->addSql('ALTER TABLE finalisasi DROP submitted_by');
        $this->addSql('ALTER TABLE finalisasi DROP verified1_by');
        $this->addSql('ALTER TABLE finalisasi DROP verified2_by');
        $this->addSql('ALTER TABLE finalisasi DROP approved1_by');
        $this->addSql('ALTER TABLE finalisasi DROP approved2_by');
        $this->addSql('ALTER TABLE finalisasi DROP approved3_by');
        $this->addSql('ALTER TABLE finalisasi DROP created_date');
        $this->addSql('ALTER TABLE finalisasi DROP presubmitted_date');
        $this->addSql('ALTER TABLE finalisasi DROP submitted_date');
        $this->addSql('ALTER TABLE finalisasi DROP verified1_date');
        $this->addSql('ALTER TABLE finalisasi DROP verified2_date');
        $this->addSql('ALTER TABLE finalisasi DROP approved1_date');
        $this->addSql('ALTER TABLE finalisasi DROP approved2_date');
        $this->addSql('ALTER TABLE finalisasi DROP approved3_date');
        $this->addSql('ALTER TABLE finalisasi ALTER mitra_id SET NOT NULL');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT fk_cce459d739d91309 FOREIGN KEY (mitra_id) REFERENCES ms_mitra (mitra_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
