<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181113030852 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE item_anggaran (ids BIGSERIAL NOT NULL, parent_id BIGINT NOT NULL, account_id INT NOT NULL, posted_by INT DEFAULT NULL, deskripsi TEXT NOT NULL, fixed_cost BOOLEAN DEFAULT \'false\' NOT NULL, jml_qty1 DOUBLE PRECISION DEFAULT \'0\', jml_qty2 DOUBLE PRECISION DEFAULT \'0\', frequency DOUBLE PRECISION DEFAULT \'0\', unit_price DOUBLE PRECISION DEFAULT \'0\', anggaran DOUBLE PRECISION DEFAULT \'0\', realisasi DOUBLE PRECISION DEFAULT \'0\', unit_qty1 VARCHAR(100) DEFAULT NULL, unit_qty2 VARCHAR(100) DEFAULT NULL, unit_freq VARCHAR(100) DEFAULT NULL, remark TEXT DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX item_anggaran_x1 ON item_anggaran (parent_id)');
        $this->addSql('CREATE INDEX item_anggaran_x2 ON item_anggaran (account_id)');
        $this->addSql('CREATE INDEX item_anggaran_x3 ON item_anggaran (posted_by)');
        $this->addSql('CREATE TABLE kegiatan (kegiatan_id BIGSERIAL NOT NULL, program_id INT NOT NULL, batch_id INT NOT NULL, payment_method INT DEFAULT NULL, posted_by INT DEFAULT NULL, nama_kegiatan VARCHAR(250) NOT NULL, submitted SMALLINT DEFAULT 0 NOT NULL, billed BOOLEAN DEFAULT \'false\' NOT NULL, closed BOOLEAN DEFAULT \'false\' NOT NULL, approval_step SMALLINT DEFAULT 0 NOT NULL, approval_status VARCHAR(25) DEFAULT NULL, anggaran DOUBLE PRECISION DEFAULT \'0\', realisasi DOUBLE PRECISION DEFAULT \'0\', posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(kegiatan_id))');
        $this->addSql('CREATE INDEX kegiatan_x1 ON kegiatan (program_id)');
        $this->addSql('CREATE INDEX kegiatan_x2 ON kegiatan (batch_id)');
        $this->addSql('CREATE INDEX kegiatan_x3 ON kegiatan (submitted, approval_step)');
        $this->addSql('CREATE INDEX kegiatan_x4 ON kegiatan (closed)');
        $this->addSql('CREATE INDEX kegiatan_x5 ON kegiatan (payment_method)');
        $this->addSql('CREATE INDEX kegiatan_x6 ON kegiatan (posted_by)');
        $this->addSql('COMMENT ON COLUMN kegiatan.submitted IS \'Sudah di submit ke Adaro atau belum. Keterangan:
            1 = Submitted for Review
            2 = Submitted for Approval\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.billed IS \'Sudah melakukan penagihan ke Adaro atau belum\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_step IS \'Tahapan approval yang sudah dilalui. Keterangan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_status IS \'Valid values: REJECTED, APPROVED\'');
        $this->addSql('CREATE TABLE log_aprv_kegiatan (log_id BIGSERIAL NOT NULL, kegiatan_id BIGINT NOT NULL, posted_by INT DEFAULT NULL, title VARCHAR(250) NOT NULL, status VARCHAR(25) NOT NULL, approval_step SMALLINT DEFAULT 0 NOT NULL, keterangan TEXT DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(log_id))');
        $this->addSql('CREATE INDEX log_aprv_kegiatan_x1 ON log_aprv_kegiatan (kegiatan_id)');
        $this->addSql('CREATE INDEX log_aprv_kegiatan_x2 ON log_aprv_kegiatan (posted_by)');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.status IS \'Valid values: REJECTED, APPROVED\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.approval_step IS \'Tahapan approval, keterangan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');
        $this->addSql('CREATE TABLE sub_kegiatan (sub_id BIGSERIAL NOT NULL, kegiatan_id BIGINT NOT NULL, posted_by INT DEFAULT NULL, title TEXT NOT NULL, tgl_mulai DATE NOT NULL, tgl_akhir DATE DEFAULT NULL, anggaran DOUBLE PRECISION DEFAULT \'0\', realisasi DOUBLE PRECISION DEFAULT \'0\', closed BOOLEAN DEFAULT \'false\' NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(sub_id))');
        $this->addSql('CREATE INDEX sub_kegiatan_x1 ON sub_kegiatan (kegiatan_id)');
        $this->addSql('CREATE INDEX sub_kegiatan_x2 ON sub_kegiatan (posted_by)');
        $this->addSql('CREATE TABLE sub_kegiatan_provinsi (sub_id BIGINT NOT NULL, prov_id INT NOT NULL, PRIMARY KEY(sub_id, prov_id))');
        $this->addSql('CREATE INDEX IDX_9E0F949656992D9 ON sub_kegiatan_provinsi (sub_id)');
        $this->addSql('CREATE INDEX IDX_9E0F9496FBD8A061 ON sub_kegiatan_provinsi (prov_id)');
        $this->addSql('CREATE TABLE sub_kegiatan_kabupaten (sub_id BIGINT NOT NULL, kab_id INT NOT NULL, PRIMARY KEY(sub_id, kab_id))');
        $this->addSql('CREATE INDEX IDX_C671DF1156992D9 ON sub_kegiatan_kabupaten (sub_id)');
        $this->addSql('CREATE INDEX IDX_C671DF117F8C236D ON sub_kegiatan_kabupaten (kab_id)');
        $this->addSql('ALTER TABLE item_anggaran ADD CONSTRAINT FK_F2072E61727ACA70 FOREIGN KEY (parent_id) REFERENCES sub_kegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE item_anggaran ADD CONSTRAINT FK_F2072E619B6B5FBA FOREIGN KEY (account_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE item_anggaran ADD CONSTRAINT FK_F2072E61AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4B3EB8070A FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4BF39EBE7A FOREIGN KEY (batch_id) REFERENCES ms_batch_csr (batch_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4B7B61A1F6 FOREIGN KEY (payment_method) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4BAE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD CONSTRAINT FK_E496B08A83C3F230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD CONSTRAINT FK_E496B08AAE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD CONSTRAINT FK_CDF821283C3F230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD CONSTRAINT FK_CDF8212AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan_provinsi ADD CONSTRAINT FK_9E0F949656992D9 FOREIGN KEY (sub_id) REFERENCES sub_kegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan_provinsi ADD CONSTRAINT FK_9E0F9496FBD8A061 FOREIGN KEY (prov_id) REFERENCES provinsi (prov_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan_kabupaten ADD CONSTRAINT FK_C671DF1156992D9 FOREIGN KEY (sub_id) REFERENCES sub_kegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan_kabupaten ADD CONSTRAINT FK_C671DF117F8C236D FOREIGN KEY (kab_id) REFERENCES kabupaten (kab_id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('INSERT INTO ms_dataref (protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'ACCOUNT-RAB\', \'Master Account RAB\', \'Master Account untuk RAB\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'PAYMENT-TPL\', \'Template Payment Method\', \'Template metode pencairan dana\', NOW())');
        $this->addSql('UPDATE ms_dataref SET ref_code = \'SATUAN-RAB\' WHERE ref_code = \'UNIT-RAB\'');

        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-001\', \'Program Supplies\', 
        \'Modul, jasa laporan, ATK program, bantuan modal\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-002\', \'Infrastructure support expenses\', 
        \'Bantuan dalam bentuk infrastruktur\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-003\', \'Professional and consultant fees\', 
        \'Honor narasumber, lumpsum, perdiem\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-004\', \'Transportation and travel expenses\', 
        \'Tiket pesawat, transport peserta, transport lokal narsum, hotel\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-005\', \'Salaries and benefits\', 
        \'Gaji operasional dan pendukungnya\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-006\', \'Meal and drinks\', 
        \'Makan dan minum narasumber, peserta\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-007\', \'Rent, utilities and repair expenses\', 
        \'Sewa tempat, komputer, sarana kerja\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-008\', \'Advertising and sponsorship\', 
        \'Iklan,branding\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-009\', \'Office supplies\', 
        \'Telepon, perlengkapan kantor, foto copy, ATK Kantor\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'ACCOUNT-RAB\'), true, \'100-010\', \'Others\', 
        \'Transport tenaga kebersihan\', NOW())');

        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-01\', \'Paket\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-02\', \'Orang\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-03\', \'Eksemplar\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-04\', \'Set\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-05\', \'Kali\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-06\', \'Unit\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-07\', \'Lumpsum\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-08\', \'Hari\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-09\', \'Minggu\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-10\', \'Bulan\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-11\', \'Tim\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-12\', \'Lokasi\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-13\', \'Bis\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-14\', \'Trip\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-15\', \'Kamar\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-16\', \'Mobil\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-17\', \'Sekolah\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'SATUAN-RAB\'), true, \'SATUAN-18\', \'Gugus\', NOW())');

        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'PAYMENT-TPL\'), true, \'PAYMENT-01\', \'2 Termin (60% + 40%)\', 
        \'[{"property":"Termin-1", "value":"60"},{"property":"Termin-2", "value":"40"}]\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'PAYMENT-TPL\'), true, \'PAYMENT-02\', \'1 Termin (100%)\', 
        \'[{"property":"Termin-1", "value":"100"}]\', NOW())');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE log_aprv_kegiatan DROP CONSTRAINT FK_E496B08A83C3F230');
        $this->addSql('ALTER TABLE sub_kegiatan DROP CONSTRAINT FK_CDF821283C3F230');
        $this->addSql('ALTER TABLE item_anggaran DROP CONSTRAINT FK_F2072E61727ACA70');
        $this->addSql('ALTER TABLE sub_kegiatan_provinsi DROP CONSTRAINT FK_9E0F949656992D9');
        $this->addSql('ALTER TABLE sub_kegiatan_kabupaten DROP CONSTRAINT FK_C671DF1156992D9');
        $this->addSql('DROP TABLE item_anggaran');
        $this->addSql('DROP TABLE kegiatan');
        $this->addSql('DROP TABLE log_aprv_kegiatan');
        $this->addSql('DROP TABLE sub_kegiatan');
        $this->addSql('DROP TABLE sub_kegiatan_provinsi');
        $this->addSql('DROP TABLE sub_kegiatan_kabupaten');
    }

}
