<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200808211533 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE kegiatan ADD jenis_pembayaran VARCHAR(20) DEFAULT \'\'');
        $this->addSql('ALTER TABLE kegiatan ADD termin1 BOOLEAN DEFAULT \'false\'');
        $this->addSql('ALTER TABLE kegiatan ADD termin1_percent INT DEFAULT 0');
        $this->addSql('ALTER TABLE kegiatan ADD termin_pelunasan BOOLEAN DEFAULT \'false\'');
        $this->addSql('ALTER TABLE pencairan_items ADD budget DOUBLE PRECISION DEFAULT \'0\'');
        $this->addSql('ALTER TABLE pencairan ADD is_pelunasan BOOLEAN DEFAULT \'false\' NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE pencairan DROP is_pelunasan');
        $this->addSql('ALTER TABLE pencairan_items DROP budget');
        $this->addSql('ALTER TABLE kegiatan DROP jenis_pembayaran');
        $this->addSql('ALTER TABLE kegiatan DROP termin1');
        $this->addSql('ALTER TABLE kegiatan DROP termin1_percent');
        $this->addSql('ALTER TABLE kegiatan DROP termin_pelunasan');
    }
}
