<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200704134337 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE pencairan_items (itempencairan_id BIGSERIAL NOT NULL, pencairan_id BIGINT NOT NULL, kegiatan_id BIGINT DEFAULT NULL, subkegiatan_id BIGINT DEFAULT NULL, title VARCHAR(255) DEFAULT \'0\', amount DOUBLE PRECISION DEFAULT \'0\', PRIMARY KEY(itempencairan_id))');
        $this->addSql('CREATE INDEX itempencairan_x1 ON pencairan_items (pencairan_id)');
        $this->addSql('CREATE INDEX itempencairan_x2 ON pencairan_items (kegiatan_id)');
        $this->addSql('CREATE INDEX itempencairan_x3 ON pencairan_items (subkegiatan_id)');
        $this->addSql('ALTER TABLE pencairan_items ADD CONSTRAINT FK_DF9EEE776F23375 FOREIGN KEY (pencairan_id) REFERENCES pencairan (pencairan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan_items ADD CONSTRAINT FK_DF9EEE783C3F230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan_items ADD CONSTRAINT FK_DF9EEE7CC439185 FOREIGN KEY (subkegiatan_id) REFERENCES subkegiatan (sub_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan DROP CONSTRAINT fk_b8ad7f1b83c3f230');
        $this->addSql('DROP INDEX idx_b8ad7f1b83c3f230');
        $this->addSql('ALTER TABLE pencairan DROP kegiatan_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE pencairan_items');
        $this->addSql('ALTER TABLE pencairan ADD kegiatan_id BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT fk_b8ad7f1b83c3f230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_b8ad7f1b83c3f230 ON pencairan (kegiatan_id)');
    }
}
