<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200115092559 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $menu = [33, NULL, 'Diagram', NULL, NULL, 'Diagram', 'chart-line', 'true', 3, 3];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);
        //$menu = [32, 8, 'Monitoring Anggaran', '/laporan/monitoring', 'laporan_monitoring_anggaran', 'Monitoring Anggaran', NULL, 'true', 4, 0];
        $sql = 'UPDATE menu_item set parent_id = 33, icon=NULL, position=1 where route_url=?';
        $this->addSql($sql, ['/diagram/progress']);
        $menu = [34, 33, 'Overview', '/diagram/rekap', 'diagram_rekap', 'Overview', NULL, 'true', 2, 0];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

    }
}
