<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190512185500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE dana_batch_revisi ADD revnum INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE dana_batch_revisi ADD status VARCHAR(50) DEFAULT \'NEW\' NOT NULL');
        $this->addSql('ALTER TABLE dana_batch_revisi ADD last_updated TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE kegiatan ADD kode VARCHAR(25) NOT NULL');
        $this->addSql('COMMENT ON COLUMN kegiatan.kode IS \'Kode untuk referensi Riwayat Transaksi Kegiatan. Format: PK-yy.0000\'');

        $this->addSql('COMMENT ON COLUMN dana_batch.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN dana_bidang.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN dana_program.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE dana_batch_revisi DROP revnum');
        $this->addSql('ALTER TABLE dana_batch_revisi DROP status');
        $this->addSql('ALTER TABLE dana_batch_revisi DROP last_updated');
        $this->addSql('ALTER TABLE kegiatan DROP kode');
    }

}
