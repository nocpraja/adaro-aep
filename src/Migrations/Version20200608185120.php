<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608185120 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE pencairan (pencairan_id BIGSERIAL NOT NULL, mitra_id INT NOT NULL, kegiatan_id BIGINT DEFAULT NULL, created_by INT DEFAULT NULL, printed_by INT DEFAULT NULL, sent_by INT DEFAULT NULL, received_by INT DEFAULT NULL, approved_by INT DEFAULT NULL, rejected_by INT DEFAULT NULL, paid_by INT DEFAULT NULL, tahun INT NOT NULL, urutan INT NOT NULL, nomor_surat TEXT NOT NULL, tipe_termin VARCHAR(80) NOT NULL, persen DOUBLE PRECISION DEFAULT \'0\', is_expense BOOLEAN DEFAULT \'false\' NOT NULL, url_file_tagihan VARCHAR(255) DEFAULT NULL, att_path VARCHAR(200) DEFAULT NULL, att_file VARCHAR(100) DEFAULT NULL, att_type VARCHAR(10) DEFAULT NULL, att_size INT DEFAULT NULL, status VARCHAR(50) NOT NULL, created_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, printed_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, sent_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, received_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, approved_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, rejected_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, rejected_reason TEXT DEFAULT NULL, paid_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, total_budget DOUBLE PRECISION DEFAULT \'0\', total_pencairan DOUBLE PRECISION DEFAULT \'0\', PRIMARY KEY(pencairan_id))');
        $this->addSql('CREATE INDEX IDX_B8AD7F1B83C3F230 ON pencairan (kegiatan_id)');
        $this->addSql('CREATE INDEX pencairan_x1 ON pencairan (mitra_id)');
        $this->addSql('CREATE INDEX pencairan_x2 ON pencairan (created_by)');
        $this->addSql('CREATE INDEX pencairan_x3 ON pencairan (printed_by)');
        $this->addSql('CREATE INDEX pencairan_x4 ON pencairan (sent_by)');
        $this->addSql('CREATE INDEX pencairan_x5 ON pencairan (received_by)');
        $this->addSql('CREATE INDEX pencairan_x6 ON pencairan (approved_by)');
        $this->addSql('CREATE INDEX pencairan_x7 ON pencairan (rejected_by)');
        $this->addSql('CREATE INDEX pencairan_x8 ON pencairan (paid_by)');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1B39D91309 FOREIGN KEY (mitra_id) REFERENCES ms_mitra (mitra_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1B83C3F230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1BDE12AB56 FOREIGN KEY (created_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1BB36CF41B FOREIGN KEY (printed_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1BC378DCF6 FOREIGN KEY (sent_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1B38D350E7 FOREIGN KEY (received_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1B4EA3CB3D FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1B7DC818F9 FOREIGN KEY (rejected_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD CONSTRAINT FK_B8AD7F1B8B380FF2 FOREIGN KEY (paid_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE pencairan');
    }
}
