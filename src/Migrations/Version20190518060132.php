<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190518060132 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE kegiatan ALTER realisasi DROP DEFAULT');
        $this->addSql('ALTER TABLE subkegiatan ALTER realisasi DROP DEFAULT');
        $this->addSql('ALTER TABLE item_anggaran ALTER realisasi DROP DEFAULT');
        $this->addSql('UPDATE kegiatan SET realisasi = NULL WHERE realisasi = 0');
        $this->addSql('UPDATE subkegiatan SET realisasi = NULL WHERE realisasi = 0');
        $this->addSql('UPDATE item_anggaran SET realisasi = NULL WHERE realisasi = 0');

        $params = [
          'GENERAL', 'Mailer.From', 'JSON',
          json_encode(['property' => 'Admin Adaro CSRMS', 'value' => 'adarofoundation@adaro.com']),
          'false', 1, 'Mailer From', null
        ];
        $this->addSql('INSERT INTO app_setting (section, attribute, attribute_type, attribute_value, hidden, position, label, description) ' .
                      'values (?, ?, ?, ?, ?, ?, ?, ?)', $params);

        $this->addSql('COMMENT ON COLUMN dana_batch.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN dana_bidang.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN dana_program.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE kegiatan ALTER realisasi SET DEFAULT \'0\'');
        $this->addSql('ALTER TABLE subkegiatan ALTER realisasi SET DEFAULT \'0\'');
        $this->addSql('ALTER TABLE item_anggaran ALTER realisasi SET DEFAULT \'0\'');
        $this->addSql('UPDATE kegiatan SET realisasi = 0 WHERE realisasi = NULL');
        $this->addSql('UPDATE subkegiatan SET realisasi = 0 WHERE realisasi = NULL');
        $this->addSql('UPDATE item_anggaran SET realisasi = 0 WHERE realisasi = NULL');
        $this->addSql('TRUNCATE app_setting');

        $this->addSql('COMMENT ON COLUMN kegiatan.status IS \'Valid values: NEW, UPDATED, SUBMITTED, CLOSED, VERIFIED, REJECTED, APPROVED_1, APPROVED_2, APPROVED_3\'');
        $this->addSql('COMMENT ON COLUMN dana_program.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_batch.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REVISI_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_bidang.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REVISI_REJECTED\'');
    }
}
