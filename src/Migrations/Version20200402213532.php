<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200402213532 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        //MENU HUMAS
        $menu = [41, 19, 'Konfigurasi', '/security/appconfig', 'setting_appconfig', 'Konfigurasi Aplikasi', NULL, 'true', 3, 0];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);
        $menu = [42, 19, 'Audit Log', '/security/auditlog', 'setting_auditlog', 'Audit Log', NULL, 'true', 4, 0];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);
    }

        public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
