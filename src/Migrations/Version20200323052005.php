<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323052005 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('TRUNCATE TABLE app_setting');

        $this->addSql('INSERT INTO app_setting (section, attribute, attribute_type, attribute_value, hidden, position, label, description) ' .
            'values (?, ?, ?, ?, ?, ?, ?, ?)', [
            'MAILER', 'From', 'STRING',
            'adarofoundation@adaro.com',
            'false', 1, 'Mailer From',
            null
        ]);
        $this->addSql('INSERT INTO app_setting (section, attribute, attribute_type, attribute_value, hidden, position, label, description) ' .
            'values (?, ?, ?, ?, ?, ?, ?, ?)', [
            'MAILER', 'FromName', 'STRING',
            'Admin Adaro CSRMS',
            'false', 2, 'Mailer From Name',
            null
        ]);

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('TRUNCATE TABLE app_setting');

        $params = [
            'GENERAL', 'Mailer.From', 'JSON',
            json_encode(['property' => 'Admin Adaro CSRMS', 'value' => 'adarofoundation@adaro.com']),
            'false', 1, 'Mailer From', null
        ];
        $this->addSql('INSERT INTO app_setting (section, attribute, attribute_type, attribute_value, hidden, position, label, description) ' .
            'values (?, ?, ?, ?, ?, ?, ?, ?)', $params);

    }
}
