<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181026062940 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE beneficiary_beasiswa (beasiswa_id BIGSERIAL NOT NULL, individu_id BIGINT NOT NULL, fakultas_id INT NOT NULL, jurusan_id INT NOT NULL, tahun_masuk INT NOT NULL, nim VARCHAR(50) NOT NULL, jalur_masuk VARCHAR(150) DEFAULT NULL, norek VARCHAR(100) DEFAULT NULL, nama_bank VARCHAR(250) DEFAULT NULL, kartu_sehat VARCHAR(100) DEFAULT NULL, golongan_darah VARCHAR(5) DEFAULT NULL, sekolah_asal VARCHAR(250) NOT NULL, alamat_sekolah TEXT DEFAULT NULL, prestasi JSONB DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(beasiswa_id))');
        $this->addSql('CREATE UNIQUE INDEX beneficiary_beasiswa_x1 ON beneficiary_beasiswa (individu_id)');
        $this->addSql('CREATE INDEX beneficiary_beasiswa_x2 ON beneficiary_beasiswa (fakultas_id)');
        $this->addSql('CREATE INDEX beneficiary_beasiswa_x3 ON beneficiary_beasiswa (jurusan_id)');
        $this->addSql('CREATE INDEX beneficiary_beasiswa_x4 ON beneficiary_beasiswa (nim)');
        $this->addSql('COMMENT ON COLUMN beneficiary_beasiswa.norek IS \'Nomor rekening Bank\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_beasiswa.kartu_sehat IS \'Nomor Kartu Indonesia Sehat/BPJS\'');
        $this->addSql('CREATE TABLE beneficiary_individu (individu_id BIGSERIAL NOT NULL, institusi_id BIGINT DEFAULT NULL, kec_id INT NOT NULL, kel_id INT DEFAULT NULL, kategori SMALLINT NOT NULL, nama_lengkap VARCHAR(250) NOT NULL, nik VARCHAR(50) NOT NULL, jenis_kelamin VARCHAR(2) NOT NULL, tempat_lahir VARCHAR(250) NOT NULL, tgl_lahir DATE NOT NULL, phone VARCHAR(15) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, facebook VARCHAR(100) DEFAULT NULL, instagram VARCHAR(100) DEFAULT NULL, pendidikan VARCHAR(250) DEFAULT NULL, jabatan VARCHAR(250) DEFAULT NULL, status_jabatan VARCHAR(15) DEFAULT NULL, status_verifikasi SMALLINT DEFAULT 0 NOT NULL, alamat VARCHAR(255) NOT NULL, rt_rw VARCHAR(11) DEFAULT NULL, kode_pos VARCHAR(15) DEFAULT NULL, posted_by VARCHAR(50) DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, search_fts TSVECTOR DEFAULT NULL, PRIMARY KEY(individu_id))');
        $this->addSql('CREATE INDEX beneficiary_individu_x1 ON beneficiary_individu (kategori)');
        $this->addSql('CREATE INDEX beneficiary_individu_x3 ON beneficiary_individu (kec_id)');
        $this->addSql('CREATE INDEX beneficiary_individu_x4 ON beneficiary_individu (kel_id)');
        $this->addSql('CREATE INDEX beneficiary_individu_x5 ON beneficiary_individu (institusi_id)');
        $this->addSql('CREATE INDEX beneficiary_individu_x6 ON beneficiary_individu USING gin (search_fts)');
        $this->addSql('CREATE UNIQUE INDEX beneficiary_individu_x2 ON beneficiary_individu (nik)');
        $this->addSql('COMMENT ON TABLE beneficiary_individu is \'Data utama penerima manfaat: Siswa, Mahasiswa, Santri, Guru, Dosen, Manajemen\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.kategori IS \'Valid values:
            1 = SANTRI
            2 = SISWA
            3 = MAHASISWA
            4 = USTADZ
            5 = GURU
            6 = DOSEN
            7 = MANAJEMEN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.jenis_kelamin IS \'Valid values:
             L = Laki-Laki, P = Perempuan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.pendidikan IS \'Pendidikan terakhir\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.jabatan IS \'Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_jabatan IS \'Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen.
             Valid values: PNS, DTT, DTY, GTT, GTY\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.search_fts IS \'(DC2Type:tsvector)\'');
        $this->addSql('CREATE TABLE individu_in_program (individu_id BIGINT NOT NULL, program_id INT NOT NULL, PRIMARY KEY(individu_id, program_id))');
        $this->addSql('CREATE INDEX IDX_D0EACA2A480B6028 ON individu_in_program (individu_id)');
        $this->addSql('CREATE INDEX IDX_D0EACA2A3EB8070A ON individu_in_program (program_id)');
        $this->addSql('CREATE TABLE beneficiary_institusi (institusi_id BIGSERIAL NOT NULL, kec_id INT NOT NULL, kel_id INT DEFAULT NULL, kategori SMALLINT NOT NULL, swasta BOOLEAN DEFAULT \'true\' NOT NULL, nama_institusi VARCHAR(250) NOT NULL, kode VARCHAR(25) DEFAULT NULL, nomor_akte VARCHAR(150) DEFAULT NULL, tahun_berdiri INT DEFAULT NULL, akreditasi BOOLEAN DEFAULT \'false\' NOT NULL, nilai_akreditasi VARCHAR(15) DEFAULT NULL, jenis_paud VARCHAR(25) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, website VARCHAR(200) DEFAULT NULL, office_phone VARCHAR(25) DEFAULT NULL, contact_phone VARCHAR(25) DEFAULT NULL, nama_pimpinan VARCHAR(250) DEFAULT NULL, status_verifikasi SMALLINT DEFAULT 0 NOT NULL, status_pembinaan SMALLINT DEFAULT 0 NOT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, alamat VARCHAR(255) NOT NULL, rt_rw VARCHAR(11) DEFAULT NULL, kode_pos VARCHAR(15) DEFAULT NULL, posted_by VARCHAR(50) DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, geom geometry(POINT, 4326) DEFAULT NULL, search_fts TSVECTOR DEFAULT NULL, PRIMARY KEY(institusi_id))');
        $this->addSql('CREATE INDEX beneficiary_institusi_x1 ON beneficiary_institusi (kategori)');
        $this->addSql('CREATE INDEX beneficiary_institusi_x2 ON beneficiary_institusi (kec_id)');
        $this->addSql('CREATE INDEX beneficiary_institusi_x3 ON beneficiary_institusi (kel_id)');
        $this->addSql('CREATE INDEX beneficiary_institusi_x5 ON beneficiary_institusi (nomor_akte)');
        $this->addSql('CREATE INDEX beneficiary_institusi_x6 ON beneficiary_institusi USING gin (search_fts)');
        $this->addSql('CREATE UNIQUE INDEX beneficiary_institusi_x4 ON beneficiary_institusi (kode)');
        $this->addSql('COMMENT ON TABLE beneficiary_institusi is \'Data utama penerima manfaat: PAUD, SMK, POLTEK, Pesantren, NonPesantren\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.kategori IS \'Valid values:
            1 = PAUD
            2 = SMK
            3 = POLTEK
            4 = PESANTREN
            5 = NON-PESANTREN/YAYASAN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.kode IS \'Nomor Induk, Kode Institusi, ataupun kode penomoran lainnya\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.jenis_paud IS \'Khusus untuk PAUD, valid values: [INTI, IMBAS]\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_pembinaan IS \'Status pembinaan penerima manfaat, valid values:
            0 = Baru diikutsertakan
            1 = Pemantauan atau monitoring
            2 = Selesai pembinaan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.search_fts IS \'(DC2Type:tsvector)\'');
        $this->addSql('CREATE TABLE institusi_in_program (institusi_id BIGINT NOT NULL, program_id INT NOT NULL, PRIMARY KEY(institusi_id, program_id))');
        $this->addSql('CREATE INDEX IDX_764F18CB918D6CE4 ON institusi_in_program (institusi_id)');
        $this->addSql('CREATE INDEX IDX_764F18CB3EB8070A ON institusi_in_program (program_id)');
        $this->addSql('CREATE TABLE orang_tua (beasiswa_id BIGINT NOT NULL, kec_id INT NOT NULL, kel_id INT DEFAULT NULL, nama_ayah VARCHAR(250) NOT NULL, nama_ibu VARCHAR(250) NOT NULL, pekerjaan_ayah VARCHAR(250) DEFAULT NULL, pekerjaan_ibu VARCHAR(250) DEFAULT NULL, penghasilan DOUBLE PRECISION DEFAULT NULL, alamat VARCHAR(255) NOT NULL, rt_rw VARCHAR(11) DEFAULT NULL, kode_pos VARCHAR(15) DEFAULT NULL, PRIMARY KEY(beasiswa_id))');
        $this->addSql('CREATE INDEX orang_tua_x1 ON orang_tua (kec_id)');
        $this->addSql('CREATE INDEX orang_tua_x2 ON orang_tua (kel_id)');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD CONSTRAINT FK_2F341ED1480B6028 FOREIGN KEY (individu_id) REFERENCES beneficiary_individu (individu_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD CONSTRAINT FK_2F341ED14F3826BF FOREIGN KEY (fakultas_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD CONSTRAINT FK_2F341ED1A443AEB4 FOREIGN KEY (jurusan_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_individu ADD CONSTRAINT FK_683ABC6D918D6CE4 FOREIGN KEY (institusi_id) REFERENCES beneficiary_institusi (institusi_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_individu ADD CONSTRAINT FK_683ABC6D32B0E2C8 FOREIGN KEY (kec_id) REFERENCES kecamatan (kec_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_individu ADD CONSTRAINT FK_683ABC6D6AD3F29E FOREIGN KEY (kel_id) REFERENCES kelurahan (kel_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE individu_in_program ADD CONSTRAINT FK_D0EACA2A480B6028 FOREIGN KEY (individu_id) REFERENCES beneficiary_individu (individu_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE individu_in_program ADD CONSTRAINT FK_D0EACA2A3EB8070A FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD CONSTRAINT FK_C34B43FD32B0E2C8 FOREIGN KEY (kec_id) REFERENCES kecamatan (kec_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD CONSTRAINT FK_C34B43FD6AD3F29E FOREIGN KEY (kel_id) REFERENCES kelurahan (kel_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE institusi_in_program ADD CONSTRAINT FK_764F18CB918D6CE4 FOREIGN KEY (institusi_id) REFERENCES beneficiary_institusi (institusi_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE institusi_in_program ADD CONSTRAINT FK_764F18CB3EB8070A FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orang_tua ADD CONSTRAINT FK_17E04D15FFC69477 FOREIGN KEY (beasiswa_id) REFERENCES beneficiary_beasiswa (beasiswa_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orang_tua ADD CONSTRAINT FK_17E04D1532B0E2C8 FOREIGN KEY (kec_id) REFERENCES kecamatan (kec_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orang_tua ADD CONSTRAINT FK_17E04D156AD3F29E FOREIGN KEY (kel_id) REFERENCES kelurahan (kel_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orang_tua DROP CONSTRAINT FK_17E04D15FFC69477');
        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP CONSTRAINT FK_2F341ED1480B6028');
        $this->addSql('ALTER TABLE individu_in_program DROP CONSTRAINT FK_D0EACA2A480B6028');
        $this->addSql('ALTER TABLE beneficiary_individu DROP CONSTRAINT FK_683ABC6D918D6CE4');
        $this->addSql('ALTER TABLE institusi_in_program DROP CONSTRAINT FK_764F18CB918D6CE4');
        $this->addSql('DROP TABLE beneficiary_beasiswa');
        $this->addSql('DROP TABLE beneficiary_individu');
        $this->addSql('DROP TABLE individu_in_program');
        $this->addSql('DROP TABLE beneficiary_institusi');
        $this->addSql('DROP TABLE institusi_in_program');
        $this->addSql('DROP TABLE orang_tua');
    }
}
