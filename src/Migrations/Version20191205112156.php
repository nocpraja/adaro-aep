<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191205112156 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE ms_batch_csr_tahun_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE ms_batch_csr_tahun (id INT NOT NULL, batch_id INT NOT NULL, tahun INT NOT NULL, narasi_kpi VARCHAR(500) NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX ms_batch_tahun_x1 ON ms_batch_csr_tahun(batch_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE batch_csr_tahun_id_seq CASCADE');
        $this->addSql('DROP TABLE batch_csr_tahun');
    }
}
