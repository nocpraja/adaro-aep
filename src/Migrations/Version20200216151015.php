<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200216151015 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE menu_item set position=2 where menu_id=30'); //Peta Sebaran
        $this->addSql('UPDATE menu_item set position=3 where menu_id=2'); //Pendanaan
        $this->addSql('UPDATE menu_item set position=4 where menu_id=33'); //Diagram
        $this->addSql('UPDATE menu_item set position=5 where menu_id=8'); //Kegiatan
        $this->addSql('UPDATE menu_item set position=8 where menu_id=11'); //Penerima Manfaat
        $this->addSql('UPDATE menu_item set position=9 where menu_id=12'); //Master Data
        $this->addSql('UPDATE menu_item set position=10 where menu_id=19'); //Setting

        //MENU DOKUMENTASI KEGIATAN
        $menu = [35, NULL, 'Fungsi Penunjang', NULL, NULL, 'Dokumentasi Kegiatan', 'folder-open', 'true', 6, 3];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);
        $menu = [36, 35, 'Galeri Kegiatan', '/dokumentasi/gallery', 'dokumentasi_gallery', 'Dokumentasi Gallery Kegiatan', NULL, 'true', 1, 0];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);
        $menu = [37, 35, 'Dosir Kegiatan', '/dokumentasi/dosir', 'dokumentasi_dosir', 'Dokumen Pendukung Kegiatan', NULL, 'true', 2, 0];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
