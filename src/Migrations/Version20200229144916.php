<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200229144916 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE beneficiary_log_aprv_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE beneficiary_log_aprv (log_id BIGSERIAL NOT NULL, beasiswa_id BIGINT, tbe_id BIGINT, institusi_id BIGINT, individu_id BIGINT, kategori SMALLINT NOT NULL, log_title VARCHAR(250) NOT NULL, keterangan TEXT DEFAULT NULL, log_action VARCHAR(50) NOT NULL, from_status VARCHAR(50) NOT NULL, to_status VARCHAR(50) NOT NULL, posted_by INT NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(log_id))');
        $this->addSql('COMMENT ON COLUMN beneficiary_log_aprv.beasiswa_id IS \'FK table beneficiary_beasiswa\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_log_aprv.individu_id IS \'FK table beneficiary_individu\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_log_aprv.institusi_id IS \'FK table beneficiary_institusi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_log_aprv.tbe_id IS \'FK table beneficiary_tbe\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_log_aprv.kategori IS \'Kategori log beneficiary. Valid values:
        1 = Beasiswa
        2 = TBE
        3 = Institusi
        4 = Individu\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_log_aprv.log_action IS \'Tindakan yang dilakukan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_log_aprv.from_status IS \'Status sebelum tindakan verifikasi ataupun approval\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_log_aprv.to_status IS \'Status setelah tindakan verifikasi ataupun approval\'');
        $this->addSql('CREATE INDEX log_beneficiary_idx1 ON beneficiary_log_aprv (beasiswa_id)');
        $this->addSql('CREATE INDEX log_beneficiary_idx2 ON beneficiary_log_aprv (institusi_id)');
        $this->addSql('CREATE INDEX log_beneficiary_idx3 ON beneficiary_log_aprv (individu_id)');
        $this->addSql('CREATE INDEX log_beneficiary_idx4 ON beneficiary_log_aprv (tbe_id)');
        $this->addSql('CREATE INDEX log_beneficiary_idx5 ON beneficiary_log_aprv (posted_by)');
        $this->addSql('ALTER TABLE beneficiary_log_aprv ADD CONSTRAINT FK1_LOGBENEFICIARY_BEASISWA FOREIGN KEY (beasiswa_id) REFERENCES beneficiary_beasiswa (beasiswa_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE beneficiary_log_aprv ADD CONSTRAINT FK2_LOGBENEFICIARY_INDIVIDU FOREIGN KEY (individu_id) REFERENCES beneficiary_individu (individu_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE beneficiary_log_aprv ADD CONSTRAINT FK3_LOGBENEFICIARY_INSTITUSI FOREIGN KEY (institusi_id) REFERENCES beneficiary_institusi (institusi_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE beneficiary_log_aprv ADD CONSTRAINT FK4_LOGBENEFICIARY_TBE FOREIGN KEY (tbe_id) REFERENCES beneficiary_tbe (tbe_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE beneficiary_log_aprv ADD CONSTRAINT FK5_LOGBENEFICIARY_POSTED FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');

        /**
         * MODIFIED TABLE beneficiary_beasiswa
         */
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD status VARCHAR(20) DEFAULT \'NEW\'');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD verified_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD approved_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD CONSTRAINT FK_BEASISWA_VERIFIED FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD CONSTRAINT FK_BEASISWA_APPROVED FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX beasiswa_aprv_idx1 ON beneficiary_beasiswa (verified_by)');
        $this->addSql('CREATE INDEX beasiswa_aprv_idx2 ON beneficiary_beasiswa (approved_by)');
        $this->addSql('COMMENT ON COLUMN beneficiary_beasiswa.status IS \'Valid values: NEW, UPDATED, VERIFIED, APPROVED, REJECTED\'');

        /**
         * MODIFIED TABLE beneficiary_individu
         */
        $this->addSql('ALTER TABLE beneficiary_individu ADD status VARCHAR(20) DEFAULT \'NEW\'');
        $this->addSql('ALTER TABLE beneficiary_individu ADD verified_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_individu ADD tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_individu ADD approved_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_individu ADD tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_individu ADD CONSTRAINT FK_INDIVIDU_VERIFIED FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_individu ADD CONSTRAINT FK_INDIVIDU_APPROVED FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX individu_aprv_idx1 ON beneficiary_individu (verified_by)');
        $this->addSql('CREATE INDEX individu_aprv_idx2 ON beneficiary_individu (approved_by)');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status IS \'Valid values: NEW, UPDATED, VERIFIED, APPROVED, REJECTED\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_verifikasi IS \'Valid values:
        0: NEW
        1: VERIFIED
        2: APPROVED
        3: REJECTED\'');

        /**
         * MODIFIED TABLE beneficiary_institusi
         */
        $this->addSql('ALTER TABLE beneficiary_institusi ADD status VARCHAR(20) DEFAULT \'NEW\'');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD verified_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD approved_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD CONSTRAINT FK_INSTITUSI_VERIFIED FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD CONSTRAINT FK_INSTITUSI_APPROVED FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX institusi_aprv_idx1 ON beneficiary_institusi (verified_by)');
        $this->addSql('CREATE INDEX institusi_aprv_idx2 ON beneficiary_institusi (approved_by)');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status IS \'Valid values: NEW, UPDATED, VERIFIED, APPROVED, REJECTED\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_verifikasi IS \'Valid values:
        0: NEW
        1: VERIFIED
        2: APPROVED
        3: REJECTED\'');

        /**
         * MODIFIED TABLE beneficiary_tbe
         */
        $this->addSql('ALTER TABLE beneficiary_tbe ADD status VARCHAR(20) DEFAULT \'NEW\'');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD verified_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD approved_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD CONSTRAINT FK_TBE_VERIFIED FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD CONSTRAINT FK_TBE_APPROVED FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX tbe_aprv_idx1 ON beneficiary_tbe (verified_by)');
        $this->addSql('CREATE INDEX tbe_aprv_idx2 ON beneficiary_tbe (approved_by)');
        $this->addSql('COMMENT ON COLUMN beneficiary_tbe.status IS \'Valid values: NEW, UPDATED, VERIFIED, APPROVED, REJECTED\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('DROP SEQUENCE beneficiary_log_aprv_id_seq CASCADE');
        $this->addSql('DROP INDEX log_beneficiary_idx1');
        $this->addSql('DROP INDEX log_beneficiary_idx2');
        $this->addSql('DROP INDEX log_beneficiary_idx3');
        $this->addSql('DROP INDEX log_beneficiary_idx4');
        $this->addSql('DROP INDEX log_beneficiary_idx5');
        $this->addSql('ALTER TABLE beneficiary_log_aprv DROP CONSTRAINT FK1_LOGBENEFICIARY_BEASISWA');
        $this->addSql('ALTER TABLE beneficiary_log_aprv DROP CONSTRAINT FK2_LOGBENEFICIARY_INDIVIDU');
        $this->addSql('ALTER TABLE beneficiary_log_aprv DROP CONSTRAINT FK3_LOGBENEFICIARY_INSTITUSI');
        $this->addSql('ALTER TABLE beneficiary_log_aprv DROP CONSTRAINT FK4_LOGBENEFICIARY_TBE');
        $this->addSql('ALTER TABLE beneficiary_log_aprv DROP CONSTRAINT FK5_LOGBENEFICIARY_POSTED');
        $this->addSql('DROP TABLE beneficiary_log_aprv');

        $this->addSql('DROP INDEX beasiswa_aprv_idx1');
        $this->addSql('DROP INDEX beasiswa_aprv_idx2');
        $this->addSql('DROP INDEX individu_aprv_idx1');
        $this->addSql('DROP INDEX individu_aprv_idx2');
        $this->addSql('DROP INDEX institusi_aprv_idx1');
        $this->addSql('DROP INDEX institusi_aprv_idx2');
        $this->addSql('DROP INDEX tbe_aprv_idx1');
        $this->addSql('DROP INDEX tbe_aprv_idx2');
        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP CONSTRAINT FK_BEASISWA_VERIFIED');
        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP CONSTRAINT FK_BEASISWA_APPROVED');
        $this->addSql('ALTER TABLE beneficiary_individu DROP CONSTRAINT FK_INDIVIDU_VERIFIED');
        $this->addSql('ALTER TABLE beneficiary_individu DROP CONSTRAINT FK_INDIVIDU_APPROVED');
        $this->addSql('ALTER TABLE beneficiary_institusi DROP CONSTRAINT FK_INSTITUSI_VERIFIED');
        $this->addSql('ALTER TABLE beneficiary_institusi DROP CONSTRAINT FK_INSTITUSI_APPROVED');
        $this->addSql('ALTER TABLE beneficiary_tbe DROP CONSTRAINT FK_TBE_VERIFIED');
        $this->addSql('ALTER TABLE beneficiary_tbe DROP CONSTRAINT FK_TBE_APPROVED');

        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP COLUMN status, DROP COLUMN verified_by, DROP COLUMN tgl_verified, DROP COLUMN approved_by, DROP COLUMN tgl_approved');
        $this->addSql('ALTER TABLE beneficiary_individu DROP COLUMN status, DROP COLUMN verified_by, DROP COLUMN tgl_verified, DROP COLUMN approved_by, DROP COLUMN tgl_approved');
        $this->addSql('ALTER TABLE beneficiary_institusi DROP COLUMN status, DROP COLUMN verified_by, DROP COLUMN tgl_verified, DROP COLUMN approved_by, DROP COLUMN tgl_approved');
        $this->addSql('ALTER TABLE beneficiary_tbe DROP COLUMN status, DROP COLUMN verified_by, DROP COLUMN tgl_verified, DROP COLUMN approved_by, DROP COLUMN tgl_approved');
    }
}
