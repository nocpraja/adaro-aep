<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200724170121 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE item_realisasi DROP CONSTRAINT fk1_realisasikegiatan');
        $this->addSql('ALTER TABLE item_realisasi DROP CONSTRAINT fk2_realisasisub');
        $this->addSql('ALTER TABLE item_realisasi ADD realisasi_paid DOUBLE PRECISION DEFAULT \'0\'');
        $this->addSql('ALTER TABLE item_realisasi ALTER tgl_realisasi DROP NOT NULL');
        $this->addSql('ALTER TABLE item_realisasi ADD CONSTRAINT FK_6ABB873216FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6ABB873216FE72E1 ON item_realisasi (updated_by)');
        $this->addSql('ALTER TABLE kegiatan ADD pencairan DOUBLE PRECISION DEFAULT \'0\'');
        $this->addSql('ALTER TABLE pencairan_items ADD itemrealisasi_id BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE pencairan_items ADD CONSTRAINT FK_DF9EEE7A02A47E6 FOREIGN KEY (itemrealisasi_id) REFERENCES item_realisasi (id_realisasi) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DF9EEE7A02A47E6 ON pencairan_items (itemrealisasi_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE item_realisasi DROP CONSTRAINT FK_6ABB873216FE72E1');
        $this->addSql('DROP INDEX IDX_6ABB873216FE72E1');
        $this->addSql('ALTER TABLE item_realisasi DROP realisasi_paid');
        $this->addSql('ALTER TABLE item_realisasi ALTER tgl_realisasi SET NOT NULL');
        $this->addSql('COMMENT ON COLUMN item_realisasi.status IS NULL');
        $this->addSql('ALTER TABLE item_realisasi ADD CONSTRAINT fk1_realisasikegiatan FOREIGN KEY (id_kegiatan) REFERENCES kegiatan (kegiatan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE item_realisasi ADD CONSTRAINT fk2_realisasisub FOREIGN KEY (id_subkegiatan) REFERENCES subkegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan_items DROP CONSTRAINT FK_DF9EEE7A02A47E6');
        $this->addSql('DROP INDEX IDX_DF9EEE7A02A47E6');
        $this->addSql('ALTER TABLE pencairan_items DROP itemrealisasi_id');
        $this->addSql('ALTER TABLE kegiatan DROP pencairan');
    }
}
