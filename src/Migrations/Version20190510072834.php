<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190510072834 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE log_aprv_program DROP CONSTRAINT fk_1cd380373eb8070a');
        $this->addSql('ALTER TABLE ts_revisi_program DROP CONSTRAINT fk_64e6eabd3eb8070a');
        $this->addSql('ALTER TABLE ts_pendanaan_program DROP CONSTRAINT fk_80fccef83eb8070a');
        $this->addSql('ALTER TABLE item_anggaran DROP CONSTRAINT fk_f2072e61727aca70');
        $this->addSql('ALTER TABLE sub_kegiatan_kabupaten DROP CONSTRAINT fk_c671df1156992d9');
        $this->addSql('ALTER TABLE sub_kegiatan_provinsi DROP CONSTRAINT fk_9e0f949656992d9');
        $this->addSql('DROP SEQUENCE ts_revisi_program_ts_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE log_aprv_program_log_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE pendanaan_program_program_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE ts_pendanaan_global_tsid_seq CASCADE');
        $this->addSql('DROP SEQUENCE ts_pendanaan_program_ts_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sub_kegiatan_sub_id_seq CASCADE');

        $this->addSql('CREATE TABLE subkegiatan (sub_id BIGSERIAL NOT NULL, kegiatan_id BIGINT NOT NULL, posted_realisasi_by INT, closed_by INT, posted_by INT, updated_by INT, title TEXT NOT NULL, tgl_mulai DATE NOT NULL, tgl_akhir DATE, anggaran DOUBLE PRECISION DEFAULT \'0\', realisasi DOUBLE PRECISION DEFAULT \'0\', closed BOOLEAN DEFAULT \'false\' NOT NULL, tgl_posted_realisasi TIMESTAMP(0) WITHOUT TIME ZONE, tgl_closed TIMESTAMP(0) WITHOUT TIME ZONE, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, last_updated TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(sub_id))');
        $this->addSql('CREATE INDEX subkegiatan_x1 ON subkegiatan (kegiatan_id)');
        $this->addSql('CREATE INDEX subkegiatan_x2 ON subkegiatan (closed_by)');
        $this->addSql('CREATE INDEX subkegiatan_x3 ON subkegiatan (posted_realisasi_by)');
        $this->addSql('CREATE INDEX subkegiatan_x4 ON subkegiatan (posted_by)');
        $this->addSql('CREATE INDEX subkegiatan_x5 ON subkegiatan (updated_by)');
        $this->addSql('ALTER TABLE subkegiatan ADD CONSTRAINT FK_BDE0D1C283C3F230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subkegiatan ADD CONSTRAINT FK_BDE0D1C2189B6986 FOREIGN KEY (posted_realisasi_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subkegiatan ADD CONSTRAINT FK_BDE0D1C288F6E01 FOREIGN KEY (closed_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subkegiatan ADD CONSTRAINT FK_BDE0D1C2AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subkegiatan ADD CONSTRAINT FK_BDE0D1C216FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('CREATE TABLE subkegiatan_provinsi (sub_id BIGINT NOT NULL, prov_id INT NOT NULL, PRIMARY KEY(sub_id, prov_id))');
        $this->addSql('CREATE INDEX IDX_DE1C34E156992D9 ON subkegiatan_provinsi (sub_id)');
        $this->addSql('CREATE INDEX IDX_DE1C34E1FBD8A061 ON subkegiatan_provinsi (prov_id)');
        $this->addSql('ALTER TABLE subkegiatan_provinsi ADD CONSTRAINT FK_DE1C34E156992D9 FOREIGN KEY (sub_id) REFERENCES subkegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subkegiatan_provinsi ADD CONSTRAINT FK_DE1C34E1FBD8A061 FOREIGN KEY (prov_id) REFERENCES provinsi (prov_id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('CREATE TABLE subkegiatan_kabupaten (sub_id BIGINT NOT NULL, kab_id INT NOT NULL, PRIMARY KEY(sub_id, kab_id))');
        $this->addSql('CREATE INDEX IDX_850282E56992D9 ON subkegiatan_kabupaten (sub_id)');
        $this->addSql('CREATE INDEX IDX_850282E7F8C236D ON subkegiatan_kabupaten (kab_id)');
        $this->addSql('ALTER TABLE subkegiatan_kabupaten ADD CONSTRAINT FK_850282E56992D9 FOREIGN KEY (sub_id) REFERENCES subkegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subkegiatan_kabupaten ADD CONSTRAINT FK_850282E7F8C236D FOREIGN KEY (kab_id) REFERENCES kabupaten (kab_id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('CREATE TABLE anggaran_revisi_dana_batch (ids BIGSERIAL NOT NULL, bp_id BIGINT NOT NULL, refcode VARCHAR(25) NOT NULL, title VARCHAR(255) NOT NULL, jumlah DOUBLE PRECISION NOT NULL, posted_by VARCHAR(50), posted_date TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX anggaran_revisi_dana_batch_x1 ON anggaran_revisi_dana_batch (bp_id)');
        $this->addSql('CREATE INDEX anggaran_revisi_dana_batch_x2 ON anggaran_revisi_dana_batch (refcode)');
        $this->addSql('ALTER TABLE anggaran_revisi_dana_batch ADD CONSTRAINT FK_8DE1FEDDD7EF6F8E FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('COMMENT ON TABLE anggaran_revisi_dana_batch IS \'Data sumber dana untuk request revisi Dana Batch\'');

        $this->addSql('DROP TABLE log_aprv_program');
        $this->addSql('DROP TABLE ts_pendanaan_global');
        $this->addSql('DROP TABLE ts_revisi_program');
        $this->addSql('DROP TABLE ts_pendanaan_program');
        $this->addSql('DROP TABLE pendanaan_program');
        $this->addSql('DROP TABLE sub_kegiatan_kabupaten');
        $this->addSql('DROP TABLE sub_kegiatan_provinsi');
        $this->addSql('DROP TABLE sub_kegiatan');
        $this->addSql('TRUNCATE item_anggaran, kegiatan RESTART IDENTITY CASCADE');

        $this->addSql('ALTER TABLE item_anggaran ALTER fixed_cost SET DEFAULT \'true\'');
        $this->addSql('ALTER TABLE item_anggaran ADD CONSTRAINT FK_F2072E61727ACA70 FOREIGN KEY (parent_id) REFERENCES subkegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('DROP INDEX kegiatan_x10');
        $this->addSql('DROP INDEX kegiatan_x9');
        $this->addSql('DROP INDEX kegiatan_x6');
        $this->addSql('DROP INDEX kegiatan_x7');
        $this->addSql('DROP INDEX kegiatan_x4');
        $this->addSql('DROP INDEX kegiatan_x3');
        $this->addSql('DROP INDEX kegiatan_x5');
        $this->addSql('DROP INDEX kegiatan_x8');
        $this->addSql('ALTER TABLE kegiatan DROP submitted');
        $this->addSql('ALTER TABLE kegiatan DROP approval_step');
        $this->addSql('ALTER TABLE kegiatan DROP approval_status');
        $this->addSql('ALTER TABLE kegiatan ADD updated_by INT ');
        $this->addSql('ALTER TABLE kegiatan ADD status VARCHAR(50) DEFAULT \'NEW\' NOT NULL');
        $this->addSql('ALTER TABLE kegiatan add submitted BOOLEAN DEFAULT false NOT NULL');
        $this->addSql('COMMENT ON COLUMN kegiatan.status IS \'Valid values: NEW, UPDATED, SUBMITTED, CLOSED, VERIFIED, REJECTED, APPROVED_1, APPROVED_2, APPROVED_3\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.submitted IS \'Sudah di submit ke Adaro atau belum\'');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4B16FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX kegiatan_x3 ON kegiatan (payment_method)');
        $this->addSql('CREATE INDEX kegiatan_x4 ON kegiatan (submitted_by)');
        $this->addSql('CREATE INDEX kegiatan_x5 ON kegiatan (posted_realisasi_by)');
        $this->addSql('CREATE INDEX kegiatan_x6 ON kegiatan (verified_by)');
        $this->addSql('CREATE INDEX kegiatan_x7 ON kegiatan (approved_by)');
        $this->addSql('CREATE INDEX kegiatan_x8 ON kegiatan (closed_by)');
        $this->addSql('CREATE INDEX kegiatan_x9 ON kegiatan (posted_by)');
        $this->addSql('CREATE INDEX kegiatan_x10 ON kegiatan (updated_by)');

        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD from_status VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD to_status VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE log_aprv_kegiatan DROP approval_step');
        $this->addSql('ALTER TABLE log_aprv_kegiatan ALTER log_action TYPE VARCHAR(50)');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.from_status IS \'Status kegiatan sebelum tindakan verifikasi ataupun approval\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.to_status IS \'Status kegiatan setelah tindakan verifikasi ataupun approval\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.log_action IS \'Tindakan yang dilakukan\'');

        $this->addSql('DROP INDEX log_aprv_pendanaan_x7');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x6');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x3');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x2');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x4');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x5');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x1');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x1 ON log_aprv_pendanaan (dg_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x2 ON log_aprv_pendanaan (db_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x3 ON log_aprv_pendanaan (dp_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x4 ON log_aprv_pendanaan (bp_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x5 ON log_aprv_pendanaan (posted_by)');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.kategori IS \'Kategori log pendanaan. Valid values:
            1 = Dana Global
            2 = Dana Bidang
            3 = Dana Program
            4 = Dana Batch\'');

        $this->addSql('COMMENT ON COLUMN dana_batch.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REVISI_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_batch.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN dana_bidang.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REVISI_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_bidang.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN dana_program.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_program.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * PB-yy.0000 => untuk transaksi Dana Batch\'');

        $this->createTriggers();
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE item_anggaran DROP CONSTRAINT FK_F2072E61727ACA70');
        $this->addSql('ALTER TABLE subkegiatan_provinsi DROP CONSTRAINT FK_DE1C34E156992D9');
        $this->addSql('ALTER TABLE subkegiatan_kabupaten DROP CONSTRAINT FK_850282E56992D9');

        $this->addSql('CREATE TABLE sub_kegiatan_kabupaten (sub_id BIGINT NOT NULL, kab_id INT NOT NULL, PRIMARY KEY(sub_id, kab_id))');
        $this->addSql('CREATE INDEX idx_c671df1156992d9 ON sub_kegiatan_kabupaten (sub_id)');
        $this->addSql('CREATE INDEX idx_c671df117f8c236d ON sub_kegiatan_kabupaten (kab_id)');
        $this->addSql('CREATE TABLE sub_kegiatan_provinsi (sub_id BIGINT NOT NULL, prov_id INT NOT NULL, PRIMARY KEY(sub_id, prov_id))');
        $this->addSql('CREATE INDEX idx_9e0f9496fbd8a061 ON sub_kegiatan_provinsi (prov_id)');
        $this->addSql('CREATE INDEX idx_9e0f949656992d9 ON sub_kegiatan_provinsi (sub_id)');

        $this->addSql('CREATE TABLE sub_kegiatan (sub_id BIGSERIAL NOT NULL, kegiatan_id BIGINT NOT NULL, posted_by INT, posted_realisasi_by INT, closed_by INT, title TEXT NOT NULL, tgl_mulai DATE NOT NULL, tgl_akhir DATE, anggaran DOUBLE PRECISION DEFAULT \'0\', realisasi DOUBLE PRECISION DEFAULT \'0\', closed BOOLEAN DEFAULT \'false\' NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, last_updated TIMESTAMP(0) WITHOUT TIME ZONE, tgl_posted_realisasi TIMESTAMP(0) WITHOUT TIME ZONE, tgl_closed TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(sub_id))');
        $this->addSql('CREATE INDEX sub_kegiatan_x1 ON sub_kegiatan (kegiatan_id)');
        $this->addSql('CREATE INDEX sub_kegiatan_x2 ON sub_kegiatan (closed_by)');
        $this->addSql('CREATE INDEX sub_kegiatan_x3 ON sub_kegiatan (posted_realisasi_by)');
        $this->addSql('CREATE INDEX sub_kegiatan_x4 ON sub_kegiatan (posted_by)');

        $this->addSql('ALTER TABLE sub_kegiatan_kabupaten ADD CONSTRAINT fk_c671df1156992d9 FOREIGN KEY (sub_id) REFERENCES sub_kegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan_kabupaten ADD CONSTRAINT fk_c671df117f8c236d FOREIGN KEY (kab_id) REFERENCES kabupaten (kab_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan_provinsi ADD CONSTRAINT fk_9e0f949656992d9 FOREIGN KEY (sub_id) REFERENCES sub_kegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan_provinsi ADD CONSTRAINT fk_9e0f9496fbd8a061 FOREIGN KEY (prov_id) REFERENCES provinsi (prov_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD CONSTRAINT fk_cdf821283c3f230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD CONSTRAINT fk_cdf8212ae36d154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD CONSTRAINT fk_cdf8212189b6986 FOREIGN KEY (posted_realisasi_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD CONSTRAINT fk_cdf821288f6e01 FOREIGN KEY (closed_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('DROP TABLE subkegiatan');
        $this->addSql('DROP TABLE subkegiatan_provinsi');
        $this->addSql('DROP TABLE subkegiatan_kabupaten');
        $this->addSql('DROP TABLE anggaran_revisi_dana_batch');

        $this->addSql('ALTER TABLE item_anggaran ALTER fixed_cost SET DEFAULT \'false\'');
        $this->addSql('ALTER TABLE item_anggaran ADD CONSTRAINT fk_f2072e61727aca70 FOREIGN KEY (parent_id) REFERENCES sub_kegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('DROP INDEX log_aprv_pendanaan_x1');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x2');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x3');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x4');
        $this->addSql('DROP INDEX log_aprv_pendanaan_x5');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x7 ON log_aprv_pendanaan (posted_by)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x6 ON log_aprv_pendanaan (bp_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x1 ON log_aprv_pendanaan (kategori)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x2 ON log_aprv_pendanaan (refcode)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x3 ON log_aprv_pendanaan (dg_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x4 ON log_aprv_pendanaan (db_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x5 ON log_aprv_pendanaan (dp_id)');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.kategori IS \'Kategori log pendanaan. Valid values:
                    1 = Dana Global
                    2 = Dana Bidang
                    3 = Dana Program
                    4 = Dana Batch\'');

        $this->addSql('ALTER TABLE kegiatan DROP CONSTRAINT FK_B6706C4B16FE72E1');
        $this->addSql('DROP INDEX kegiatan_x3');
        $this->addSql('DROP INDEX kegiatan_x4');
        $this->addSql('DROP INDEX kegiatan_x5');
        $this->addSql('DROP INDEX kegiatan_x6');
        $this->addSql('DROP INDEX kegiatan_x7');
        $this->addSql('DROP INDEX kegiatan_x8');
        $this->addSql('DROP INDEX kegiatan_x9');
        $this->addSql('DROP INDEX kegiatan_x10');
        $this->addSql('ALTER TABLE kegiatan DROP updated_by');
        $this->addSql('ALTER TABLE kegiatan DROP status');
        $this->addSql('ALTER TABLE kegiatan DROP submitted');
        $this->addSql('ALTER TABLE kegiatan ADD approval_step SMALLINT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE kegiatan ADD approval_status VARCHAR(25) ');
        $this->addSql('ALTER TABLE kegiatan ADD submitted SMALLINT DEFAULT 0 NOT NULL');
        $this->addSql('CREATE INDEX kegiatan_x3 ON kegiatan (closed)');
        $this->addSql('CREATE INDEX kegiatan_x4 ON kegiatan (payment_method)');
        $this->addSql('CREATE INDEX kegiatan_x5 ON kegiatan (submitted_by)');
        $this->addSql('CREATE INDEX kegiatan_x6 ON kegiatan (posted_realisasi_by)');
        $this->addSql('CREATE INDEX kegiatan_x7 ON kegiatan (verified_by)');
        $this->addSql('CREATE INDEX kegiatan_x8 ON kegiatan (approved_by)');
        $this->addSql('CREATE INDEX kegiatan_x9 ON kegiatan (closed_by)');
        $this->addSql('CREATE INDEX kegiatan_x10 ON kegiatan (posted_by)');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_step IS \'Tahapan approval yang sudah dilalui. Penjelasan:
                    1 = Verified Tahap-1
                    2 = Verified Tahap-2
                    3 = Approved Tahap-1
                    4 = Approved Tahap-2
                    5 = Approved Tahap-3\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_status IS \'Valid values: REJECTED, VERIFIED, APPROVED\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.submitted IS \'Sudah di submit ke Adaro atau belum. Penjelasan:
                    1 = Submitted for Review
                    2 = Submitted for Approval\'');

        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD approval_step SMALLINT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE log_aprv_kegiatan DROP from_status');
        $this->addSql('ALTER TABLE log_aprv_kegiatan DROP to_status');
        $this->addSql('ALTER TABLE log_aprv_kegiatan ALTER log_action TYPE VARCHAR(25)');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.approval_step IS \'Tahapan approval, keterangan:
                            1 = Verified Tahap-1
                            2 = Verified Tahap-2
                            3 = Approved Tahap-1
                            4 = Approved Tahap-2
                            5 = Approved Tahap-3\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.log_action IS \'Valid values: REJECTED, VERIFIED, APPROVED\'');
        $this->dropTriggers();
    }

    private function createTriggers(): void
    {
        $this->addSql('CREATE OR REPLACE FUNCTION tp_update_dana_batch() RETURNS trigger AS $$
BEGIN
    IF (TG_OP = \'UPDATE\') THEN
        UPDATE anggaran_revisi_dana_batch SET title = NEW.nama_program WHERE refcode = OLD.kode;
    END IF;
    RETURN NULL;
END;
$$ language \'plpgsql\'');

        $this->addSql('CREATE OR REPLACE FUNCTION tp_update_dana_program() RETURNS trigger AS $$
BEGIN
    IF (TG_OP = \'UPDATE\') THEN
        UPDATE anggaran_revisi_dana_batch SET title = NEW.title WHERE refcode = OLD.kode;
    END IF;
    RETURN NULL;
END;
$$ language \'plpgsql\'');

        $this->addSql('CREATE TRIGGER trg_dana_batch AFTER UPDATE OF nama_program ON dana_batch FOR EACH ROW EXECUTE PROCEDURE tp_update_dana_batch()');
        $this->addSql('CREATE TRIGGER trg_dana_program AFTER UPDATE OF title ON dana_program FOR EACH ROW EXECUTE PROCEDURE tp_update_dana_program()');
    }

    private function dropTriggers(): void
    {
        $this->addSql('DROP TRIGGER trg_dana_batch ON dana_batch');
        $this->addSql('DROP TRIGGER trg_dana_program ON dana_program');
        $this->addSql('DROP FUNCTION tp_update_dana_batch()');
        $this->addSql('DROP FUNCTION tp_update_dana_program()');
    }
}
