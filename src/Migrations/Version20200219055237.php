<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200219055237 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE pembobotan_bidang (id SERIAL8 NOT NULL, bidang_id INT NOT NULL, posted_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, bobot DOUBLE PRECISION DEFAULT \'0\', posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B3A7DF18FE9AA8A1 ON pembobotan_bidang (bidang_id)');
        $this->addSql('CREATE INDEX IDX_B3A7DF18AE36D154 ON pembobotan_bidang (posted_by)');
        $this->addSql('CREATE INDEX IDX_B3A7DF1816FE72E1 ON pembobotan_bidang (updated_by)');
        $this->addSql('CREATE INDEX pembobotan_bidang_x1 ON pembobotan_bidang (bidang_id)');
        $this->addSql('CREATE TABLE pembobotan_program (id SERIAL8 NOT NULL, bidang_id INT NOT NULL, program_id INT NOT NULL, posted_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, bobot DOUBLE PRECISION DEFAULT \'0\', posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7A5321013EB8070A ON pembobotan_program (program_id)');
        $this->addSql('CREATE INDEX IDX_7A532101AE36D154 ON pembobotan_program (posted_by)');
        $this->addSql('CREATE INDEX IDX_7A53210116FE72E1 ON pembobotan_program (updated_by)');
        $this->addSql('CREATE INDEX pembobotan_program_x1 ON pembobotan_program (bidang_id)');
        $this->addSql('CREATE INDEX pembobotan_program_x2 ON pembobotan_program (program_id)');
        $this->addSql('ALTER TABLE pembobotan_bidang ADD CONSTRAINT FK_B3A7DF18FE9AA8A1 FOREIGN KEY (bidang_id) REFERENCES jenis_program_csr (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pembobotan_bidang ADD CONSTRAINT FK_B3A7DF18AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pembobotan_bidang ADD CONSTRAINT FK_B3A7DF1816FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pembobotan_program ADD CONSTRAINT FK_7A532101FE9AA8A1 FOREIGN KEY (bidang_id) REFERENCES jenis_program_csr (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pembobotan_program ADD CONSTRAINT FK_7A5321013EB8070A FOREIGN KEY (program_id) REFERENCES ms_program_csr (msid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pembobotan_program ADD CONSTRAINT FK_7A532101AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pembobotan_program ADD CONSTRAINT FK_7A53210116FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE pembobotan_bidang');
        $this->addSql('DROP TABLE pembobotan_program');
    }
}
