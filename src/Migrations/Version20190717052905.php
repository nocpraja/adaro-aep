<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190717052905 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create Table Kegiatan_realisasi';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE kegiatan_realisasi (kegiatan_id BIGINT NOT NULL, bobot DOUBLE PRECISION DEFAULT NULL, rencana1 DOUBLE PRECISION DEFAULT NULL, rencana2 DOUBLE PRECISION DEFAULT NULL, rencana3 DOUBLE PRECISION DEFAULT NULL, rencana4 DOUBLE PRECISION DEFAULT NULL, rencana5 DOUBLE PRECISION DEFAULT NULL, rencana6 DOUBLE PRECISION DEFAULT NULL, rencana7 DOUBLE PRECISION DEFAULT NULL, rencana8 DOUBLE PRECISION DEFAULT NULL, rencana9 DOUBLE PRECISION DEFAULT NULL, rencana10 DOUBLE PRECISION DEFAULT NULL, rencana11 DOUBLE PRECISION DEFAULT NULL, rencana12 DOUBLE PRECISION DEFAULT NULL, realisasi1 DOUBLE PRECISION DEFAULT NULL, realisasi2 DOUBLE PRECISION DEFAULT NULL, realisasi3 DOUBLE PRECISION DEFAULT NULL, realisasi4 DOUBLE PRECISION DEFAULT NULL, realisasi5 DOUBLE PRECISION DEFAULT NULL, realisasi6 DOUBLE PRECISION DEFAULT NULL, realisasi7 DOUBLE PRECISION DEFAULT NULL, realisasi8 DOUBLE PRECISION DEFAULT NULL, realisasi9 DOUBLE PRECISION DEFAULT NULL, realisasi10 DOUBLE PRECISION DEFAULT NULL, realisasi11 DOUBLE PRECISION DEFAULT NULL, realisasi12 DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(kegiatan_id))');
        $this->addSql('CREATE UNIQUE INDEX ix_kegiatan ON kegiatan_realisasi (kegiatan_id)');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.bobot IS \'Bobot kegiatan dalam persen untuk perhitungan batch\'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana1 IS \'Rencana progress kegiatan untuk bulan 1 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana2 IS \'Rencana progress kegiatan untuk bulan 2 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana3 IS \'Rencana progress kegiatan untuk bulan 3 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana4 IS \'Rencana progress kegiatan untuk bulan 4 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana5 IS \'Rencana progress kegiatan untuk bulan 5 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana6 IS \'Rencana progress kegiatan untuk bulan 6 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana7 IS \'Rencana progress kegiatan untuk bulan 7 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana8 IS \'Rencana progress kegiatan untuk bulan 8 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana9 IS \'Rencana progress kegiatan untuk bulan 9 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana10 IS \'Rencana progress kegiatan untuk bulan 10 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana11 IS \'Rencana progress kegiatan untuk bulan 11 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.rencana12 IS \'Rencana progress kegiatan untuk bulan 12 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi1 IS \'Realisasi progress kegiatan untuk bulan 1 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi2 IS \'Realisasi progress kegiatan untuk bulan 2 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi3 IS \'Realisasi progress kegiatan untuk bulan 3 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi4 IS \'Realisasi progress kegiatan untuk bulan 4 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi5 IS \'Realisasi progress kegiatan untuk bulan 5 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi6 IS \'Realisasi progress kegiatan untuk bulan 6 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi7 IS \'Realisasi progress kegiatan untuk bulan 7 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi8 IS \'Realisasi progress kegiatan untuk bulan 8 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi9 IS \'Realisasi progress kegiatan untuk bulan 9 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi10 IS \'Realisasi progress kegiatan untuk bulan 10 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi11 IS \'Realisasi progress kegiatan untuk bulan 11 \'');
        $this->addSql('COMMENT ON COLUMN kegiatan_realisasi.realisasi12 IS \'Realisasi progress kegiatan untuk bulan 12 \'');
        $this->addSql('ALTER TABLE kegiatan_realisasi ADD CONSTRAINT FK_1805553283C3F230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE kegiatan_realisasi');
    }
}
