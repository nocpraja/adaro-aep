<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200224160015 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE humas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE humas (bid BIGSERIAL NOT NULL, category VARCHAR(25) NOT NULL, title VARCHAR(250) NOT NULL, intro TEXT NOT NULL, content TEXT DEFAULT NULL, att_path VARCHAR(200) DEFAULT NULL, att_file VARCHAR(100) DEFAULT NULL, att_type VARCHAR(10) DEFAULT NULL, att_size  INT DEFAULT NULL, status VARCHAR(20) DEFAULT \'NEW\' NOT NULL, verified_by INT DEFAULT NULL, tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, approved_by INT DEFAULT NULL, tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, posted_by INT DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_by INT DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(bid))');
        $this->addSql('COMMENT ON COLUMN humas.category IS \'Valid values: CSR, MEDIA\'');
        $this->addSql('COMMENT ON COLUMN humas.status IS \'Valid values: NEW, UPDATED, VERIFIED, REJECTED, APPROVED\'');
        $this->addSql('COMMENT ON COLUMN humas.verified_by IS \'User yang memverifikasi data humas\'');
        $this->addSql('COMMENT ON COLUMN humas.approved_by IS \'User yang mempublish ke web adaro\'');
        $this->addSql('CREATE INDEX humas_x1 ON humas (posted_by)');
        $this->addSql('CREATE INDEX humas_x2 ON humas (verified_by)');
        $this->addSql('CREATE INDEX humas_x3 ON humas (approved_by)');
        $this->addSql('ALTER TABLE humas ADD CONSTRAINT FK1_HUMAS_AUTHOR FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE humas ADD CONSTRAINT FK2_HUMAS_VERIFY FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE humas ADD CONSTRAINT FK3_HUMAS_PUBLISH FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('CREATE SEQUENCE humas_log_aprv_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE humas_log_aprv (log_id BIGSERIAL NOT NULL, humas_id BIGINT NOT NULL, log_title VARCHAR(250) NOT NULL, keterangan TEXT DEFAULT NULL, log_action VARCHAR(50) NOT NULL, from_status VARCHAR(50) NOT NULL, to_status VARCHAR(50) NOT NULL, posted_by INT DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(log_id))');
        $this->addSql('CREATE INDEX humas_log_aprv_x1 ON humas_log_aprv (humas_id)');
        $this->addSql('CREATE INDEX humas_log_aprv_x2 ON humas_log_aprv (posted_by)');
        $this->addSql('ALTER TABLE humas_log_aprv ADD CONSTRAINT FK1_HUMAS_ID FOREIGN KEY (humas_id) REFERENCES humas (bid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE humas_log_aprv ADD CONSTRAINT FK2_ACT_AUTHOR FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('COMMENT ON COLUMN humas_log_aprv.log_action IS \'Tindakan yang dilakukan\'');
        $this->addSql('COMMENT ON COLUMN humas_log_aprv.from_status IS \'Status sebelum tindakan\'');
        $this->addSql('COMMENT ON COLUMN humas_log_aprv.to_status IS \'Status setelah tindakan\'');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('DROP SEQUENCE humas_id_seq CASCADE');
        $this->addSql('DROP INDEX humas_x1');
        $this->addSql('DROP INDEX humas_x2');
        $this->addSql('DROP INDEX humas_x3');
        $this->addSql('ALTER TABLE humas DROP CONSTRAINT FK1_HUMAS_AUTHOR');
        $this->addSql('ALTER TABLE humas DROP CONSTRAINT FK2_HUMAS_VERIFY');
        $this->addSql('ALTER TABLE humas DROP CONSTRAINT FK3_HUMAS_PUBLISH');
        $this->addSql('DROP TABLE humas');

        $this->addSql('DROP SEQUENCE humas_log_aprv_id_seq CASCADE');
        $this->addSql('DROP INDEX humas_log_aprv_x1');
        $this->addSql('DROP INDEX humas_log_aprv_x2');
        $this->addSql('ALTER TABLE humas_log_aprv DROP CONSTRAINT FK1_HUMAS_ID');
        $this->addSql('ALTER TABLE humas_log_aprv DROP CONSTRAINT FK2_ACT_AUTHOR');
        $this->addSql('DROP TABLE humas_log_aprv');
    }
}
