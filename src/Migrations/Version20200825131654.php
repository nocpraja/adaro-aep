<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200825131654 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('CREATE VIEW vw_indikator AS  SELECT kpi_indikator.idk,
    kpi_indikator.msid,
    kpi_indikator.batch_id,
    kpi_indikator.tahun,
    kpi_indikator.beasiswa_id,
    kpi_indikator.tbe_id,
    kpi_indikator.institusi_id,
    kpi_indikator.individu_id,
    kpi_indikator.semester AS period,
    kpi_indikator.data_ref,
    ms_dataref.ref_name,
    ms_dataref.ref_code,
    beneficiary_individu.jenis_kelamin
   FROM ((((kpi_indikator
     LEFT JOIN beneficiary_beasiswa ON ((kpi_indikator.beasiswa_id = beneficiary_beasiswa.beasiswa_id)))
     LEFT JOIN beneficiary_tbe ON ((kpi_indikator.tbe_id = beneficiary_tbe.tbe_id)))
     LEFT JOIN beneficiary_individu ON ((beneficiary_beasiswa.individu_id = beneficiary_individu.individu_id)))
     LEFT JOIN ms_dataref ON ((kpi_indikator.data_ref = ms_dataref.id)))
  ORDER BY kpi_indikator.msid, kpi_indikator.batch_id;');

        $this->addSql('CREATE VIEW vw_beneficiary_gender AS  SELECT kpi_indikator.msid,
    kpi_indikator.batch_id,
    kpi_indikator.tahun,
    kpi_indikator.semester AS period,
    beneficiary_individu.jenis_kelamin,
    count(beneficiary_individu.jenis_kelamin) AS total
   FROM ((kpi_indikator
     JOIN beneficiary_beasiswa ON ((kpi_indikator.beasiswa_id = beneficiary_beasiswa.beasiswa_id)))
     JOIN beneficiary_individu ON ((beneficiary_beasiswa.individu_id = beneficiary_individu.individu_id)))
  GROUP BY kpi_indikator.msid, kpi_indikator.batch_id, kpi_indikator.tahun, kpi_indikator.semester, beneficiary_individu.jenis_kelamin
  ORDER BY beneficiary_individu.jenis_kelamin;');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP VIEW vw_indikator');
        $this->addSql('DROP VIEW vw_beneficiary_gender');
    }
}
