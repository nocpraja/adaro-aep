<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190428080328 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('COMMENT ON COLUMN dana_bidang.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1,
            APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_bidang.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * BP-yy.0000 => untuk transaksi Dana Batch\'');

        $this->addSql('COMMENT ON COLUMN dana_batch.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1,
            APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_batch.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * BP-yy.0000 => untuk transaksi Dana Batch\'');

        $this->addSql('COMMENT ON COLUMN dana_program.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1,
            APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_program.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * BP-yy.0000 => untuk transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.kategori IS \'Kategori log pendanaan. Valid values:
            1 = Dana Global
            2 = Dana Bidang
            3 = Dana Program
            4 = Dana Batch\'');

        $this->addSql('DROP INDEX ts_dana_batch_x1');
        $this->addSql('ALTER TABLE ts_dana_batch ADD bp_id BIGINT');
        $this->addSql('ALTER TABLE ts_dana_batch ADD CONSTRAINT FK_75EEF1A8D7EF6F8E FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX ts_dana_batch_x1 ON ts_dana_batch (bp_id)');
        $this->addSql('CREATE INDEX ts_dana_batch_x2 ON ts_dana_batch (posted_by)');

        $this->addSql('DROP INDEX ts_dana_bidang_x1');
        $this->addSql('ALTER TABLE ts_dana_bidang ADD db_id BIGINT');
        $this->addSql('ALTER TABLE ts_dana_bidang ADD CONSTRAINT FK_EC1BB1EBA2BF053A FOREIGN KEY (db_id) REFERENCES dana_bidang (db_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX ts_dana_bidang_x1 ON ts_dana_bidang (db_id)');
        $this->addSql('CREATE INDEX ts_dana_bidang_x2 ON ts_dana_bidang (posted_by)');

        $this->addSql('DROP INDEX ts_dana_program_x1');
        $this->addSql('ALTER TABLE ts_dana_program ADD dp_id BIGINT');
        $this->addSql('ALTER TABLE ts_dana_program ADD CONSTRAINT FK_5EB83EC958AF9A2E FOREIGN KEY (dp_id) REFERENCES dana_program (dp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX ts_dana_program_x1 ON ts_dana_program (dp_id)');
        $this->addSql('CREATE INDEX ts_dana_program_x2 ON ts_dana_program (posted_by)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('COMMENT ON COLUMN dana_program.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_program.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
                     * DG-yy.0000 => untuk transaksi Dana Global
                     * DB-yy.0000 => untuk transaksi Dana Bidang
                     * DP-yy.0000 => untuk transaksi Dana Program
                     * BP-yy.0000 => untuk transaksi Dana Batch\'');

        $this->addSql('COMMENT ON COLUMN dana_batch.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_batch.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
                     * DG-yy.0000 => untuk transaksi Dana Global
                     * DB-yy.0000 => untuk transaksi Dana Bidang
                     * DP-yy.0000 => untuk transaksi Dana Program
                     * BP-yy.0000 => untuk transaksi Dana Batch\'');

        $this->addSql('COMMENT ON COLUMN dana_bidang.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_bidang.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
                     * DG-yy.0000 => untuk transaksi Dana Global
                     * DB-yy.0000 => untuk transaksi Dana Bidang
                     * DP-yy.0000 => untuk transaksi Dana Program
                     * BP-yy.0000 => untuk transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.kategori IS \'Kategori log pendanaan. Valid values:
                    1 = Dana Global
                    2 = Dana Bidang
                    3 = Dana Program
                    4 = Dana Batch\'');

        $this->addSql('ALTER TABLE ts_dana_batch DROP CONSTRAINT FK_75EEF1A8D7EF6F8E');
        $this->addSql('DROP INDEX ts_dana_batch_x2');
        $this->addSql('DROP INDEX ts_dana_batch_x1');
        $this->addSql('ALTER TABLE ts_dana_batch DROP bp_id');
        $this->addSql('CREATE INDEX ts_dana_batch_x1 ON ts_dana_batch (posted_by)');

        $this->addSql('ALTER TABLE ts_dana_bidang DROP CONSTRAINT FK_EC1BB1EBA2BF053A');
        $this->addSql('DROP INDEX ts_dana_bidang_x2');
        $this->addSql('DROP INDEX ts_dana_bidang_x1');
        $this->addSql('ALTER TABLE ts_dana_bidang DROP db_id');
        $this->addSql('CREATE INDEX ts_dana_bidang_x1 ON ts_dana_bidang (posted_by)');

        $this->addSql('ALTER TABLE ts_dana_program DROP CONSTRAINT FK_5EB83EC958AF9A2E');
        $this->addSql('DROP INDEX ts_dana_program_x2');
        $this->addSql('DROP INDEX ts_dana_program_x1');
        $this->addSql('ALTER TABLE ts_dana_program DROP dp_id');
        $this->addSql('CREATE INDEX ts_dana_program_x1 ON ts_dana_program (posted_by)');
    }

}
