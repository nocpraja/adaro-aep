<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190227053617 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE jurusan_institusi (jurusan_id BIGSERIAL NOT NULL, institusi_id BIGINT NOT NULL, posted_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, jenis SMALLINT DEFAULT 1 NOT NULL, nama_jurusan VARCHAR(250) NOT NULL, nilai_akreditasi VARCHAR(15) DEFAULT NULL, kepala_jurusan VARCHAR(250) DEFAULT NULL, contact_phone VARCHAR(25) DEFAULT NULL, guru_produktif INT DEFAULT NULL, guru_adaptif INT DEFAULT NULL, guru_normatif INT DEFAULT NULL, dosen_mkdu INT DEFAULT NULL, dosen_prodi INT DEFAULT NULL, rombel_x INT DEFAULT NULL, rombel_xi INT DEFAULT NULL, rombel_xii INT DEFAULT NULL, rombel_xiii INT DEFAULT NULL, jumlah_siswa INT DEFAULT NULL, peserta_didik JSONB DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, search_fts TSVECTOR DEFAULT NULL, PRIMARY KEY(jurusan_id))');
        $this->addSql('CREATE INDEX jurusan_institusi_x1 ON jurusan_institusi (institusi_id)');
        $this->addSql('CREATE INDEX jurusan_institusi_x2 ON jurusan_institusi (jenis)');
        $this->addSql('CREATE INDEX jurusan_institusi_x3 ON jurusan_institusi (posted_by)');
        $this->addSql('CREATE INDEX jurusan_institusi_x4 ON jurusan_institusi (updated_by)');
        $this->addSql('CREATE INDEX jurusan_institusi_x5 ON jurusan_institusi USING gin (search_fts)');
        $this->addSql('COMMENT ON table jurusan_institusi IS \'Data utama Program Keahlian/Studi untuk SMK dan POLTEK\'');
        $this->addSql('COMMENT ON COLUMN jurusan_institusi.jenis IS \'Valid values:
             1 = Program Keahlian,
             2 = Program Studi\'');
        $this->addSql('COMMENT ON COLUMN jurusan_institusi.dosen_mkdu IS \'Hanya diisi jika JENIS = 2 (Program Studi)\'');
        $this->addSql('COMMENT ON COLUMN jurusan_institusi.dosen_prodi IS \'Hanya diisi jika JENIS = 2 (Program Studi)\'');
        $this->addSql('COMMENT ON COLUMN jurusan_institusi.jumlah_siswa IS \'Total jumlah siswa/mahasiswa terkini\'');
        $this->addSql('COMMENT ON COLUMN jurusan_institusi.peserta_didik IS \'Histori jumlah siswa/mahasiswa per tahun.
             Format: [{tahun: integer, jumlah: integer}]\'');
        $this->addSql('COMMENT ON COLUMN jurusan_institusi.search_fts IS \'(DC2Type:tsvector)\'');

        $this->addSql('CREATE TABLE mitra_dudi_jurusan (dudi_id BIGSERIAL NOT NULL, jurusan_id BIGINT NOT NULL, nama_usaha VARCHAR(250) NOT NULL, jenis_usaha VARCHAR(150) NOT NULL, nama_pimpinan VARCHAR(250) NOT NULL, contact_phone VARCHAR(25) DEFAULT NULL, alamat TEXT DEFAULT NULL, posted_by VARCHAR(50) DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, search_fts TSVECTOR DEFAULT NULL, PRIMARY KEY(dudi_id))');
        $this->addSql('CREATE INDEX mitra_dudi_x1 ON mitra_dudi_jurusan (jurusan_id)');
        $this->addSql('CREATE INDEX mitra_dudi_x2 ON mitra_dudi_jurusan (jenis_usaha)');
        $this->addSql('CREATE INDEX mitra_dudi_x3 ON mitra_dudi_jurusan USING gin (search_fts)');
        $this->addSql('COMMENT ON table mitra_dudi_jurusan IS \'Data utama Mitra DUDI untuk Program Keahlian/Studi SMK dan POLTEK\'');
        $this->addSql('COMMENT ON COLUMN mitra_dudi_jurusan.search_fts IS \'(DC2Type:tsvector)\'');

        $this->addSql('CREATE TABLE unit_produksi (produksi_id BIGSERIAL NOT NULL, jurusan_id BIGINT NOT NULL, nama_unit VARCHAR(250) NOT NULL, jenis VARCHAR(100) NOT NULL, omset DOUBLE PRECISION DEFAULT NULL, posted_by VARCHAR(50) DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(produksi_id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7AB48ECDA443AEB4 ON unit_produksi (jurusan_id)');
        $this->addSql('CREATE INDEX unit_produksi_x1 ON unit_produksi (jurusan_id)');
        $this->addSql('COMMENT ON table unit_produksi IS \'Data utama unit produksi untuk Program Keahlian/Studi SMK dan POLTEK\'');
        $this->addSql('COMMENT ON COLUMN unit_produksi.jenis IS \'Jenis unit produksi\'');
        $this->addSql('COMMENT ON COLUMN unit_produksi.omset IS \'Omset per tahun, dalam Rupiah\'');

        $this->addSql('ALTER TABLE jurusan_institusi ADD CONSTRAINT FK_79D08AB4918D6CE4 FOREIGN KEY (institusi_id) REFERENCES beneficiary_institusi (institusi_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jurusan_institusi ADD CONSTRAINT FK_79D08AB4AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jurusan_institusi ADD CONSTRAINT FK_79D08AB416FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mitra_dudi_jurusan ADD CONSTRAINT FK_D0E774ACA443AEB4 FOREIGN KEY (jurusan_id) REFERENCES jurusan_institusi (jurusan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_produksi ADD CONSTRAINT FK_7AB48ECDA443AEB4 FOREIGN KEY (jurusan_id) REFERENCES jurusan_institusi (jurusan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_account ADD token_key VARCHAR(250) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_account ADD token_expiry TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');

        // Update dataref Pembinaan PAUD
        $jmlPaud = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah PAUD', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlPaud) . '\' WHERE ref_code = \'KPI-01.001\'');

        $jmlGuru = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah GURU', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlGuru) . '\' WHERE ref_code = \'KPI-01.002\'');

        $jmlSiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah SISWA', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlSiswa) . '\' WHERE ref_code = \'KPI-01.003\'');

        // Update dataref SMK
        $jmlSMK = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah SMK', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlSMK) . '\' WHERE ref_code = \'KPI-02.001\'');
        $jmlProgli = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah PROGLI', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlProgli) . '\' WHERE ref_code = \'KPI-02.002\'');
        $jmlManajemen = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah MANAJEMEN', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlManajemen) . '\' WHERE ref_code = \'KPI-02.003\'');
        $jmlGuru = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah GURU', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlGuru) . '\' WHERE ref_code = \'KPI-02.004\'');
        $jmlSiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah SISWA', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlSiswa) . '\' WHERE ref_code = \'KPI-02.005\'');

        // Update dataref POLTEK
        $jmlPoltek = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah POLTEK', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlPoltek) . '\' WHERE ref_code = \'KPI-02.006\'');
        $jmlProgli = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah PRODI', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlProgli) . '\' WHERE ref_code = \'KPI-02.007\'');
        $jmlManajemen = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah MANAJEMEN', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlManajemen) . '\' WHERE ref_code = \'KPI-02.008\'');
        $jmlDosen = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah DOSEN', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlDosen) . '\' WHERE ref_code = \'KPI-02.009\'');
        $jmlMahasiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah MAHASISWA', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlMahasiswa) . '\' WHERE ref_code = \'KPI-02.010\'');

        // Update dataref Pesantren
        $jmlPesantren = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah INSTITUSI', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlPesantren) . '\' WHERE ref_code = \'KPI-03.001\'');
        $jmlManajemen = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah MANAJEMEN', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlManajemen) . '\' WHERE ref_code = \'KPI-03.002\'');
        $jmlUstadz = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah USTADZ/INSTRUKTUR', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlUstadz) . '\' WHERE ref_code = \'KPI-03.003\'');
        $jmlSantri = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah SANTRI/SISWA', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlSantri) . '\' WHERE ref_code = \'KPI-03.004\'');

        // Update dataref Beasiswa
        $jmlMahasiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah TOTAL', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlMahasiswa) . '\' WHERE ref_code = \'KPI-04.001\'');
        $jmlPria = [
            ['property' => 'targeted', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah LAKI-LAKI', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlPria) . '\' WHERE ref_code = \'KPI-04.002\'');
        $jmlWanita = [
            ['property' => 'targeted', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah PEREMPUAN', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlWanita) . '\' WHERE ref_code = \'KPI-04.003\'');

        // Update dataref TBE
        $jmlGuruSD = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah GURU SD', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlGuruSD) . '\' WHERE ref_code = \'KPI-05.001\'');
        $jmlGuruSMP = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'Jumlah GURU SMP', 'type' => 'STRING']
        ];
        $this->addSql('UPDATE ms_dataref set ref_value = \''. json_encode($jmlGuruSMP) . '\' WHERE ref_code = \'KPI-05.002\'');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE mitra_dudi_jurusan DROP CONSTRAINT FK_D0E774ACA443AEB4');
        $this->addSql('ALTER TABLE unit_produksi DROP CONSTRAINT FK_7AB48ECDA443AEB4');
        $this->addSql('DROP TABLE jurusan_institusi');
        $this->addSql('DROP TABLE mitra_dudi_jurusan');
        $this->addSql('DROP TABLE unit_produksi');
        $this->addSql('ALTER TABLE user_account DROP token_key');
        $this->addSql('ALTER TABLE user_account DROP token_expiry');
    }

}
