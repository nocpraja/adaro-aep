<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181129155520 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE subyek_monitoring (id BIGINT NOT NULL, nama TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE beneficiary_tbe (tbe_id BIGSERIAL NOT NULL, individu_id BIGINT NOT NULL, nama_institusi VARCHAR(250) NOT NULL, alamat_institusi TEXT DEFAULT NULL, nip VARCHAR(50) DEFAULT NULL, lama_mengajar VARCHAR(150) DEFAULT NULL, sertifikasi BOOLEAN DEFAULT \'false\' NOT NULL, mapel JSONB DEFAULT NULL, prestasi JSONB DEFAULT NULL, pelatihan JSONB DEFAULT NULL, posted_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(tbe_id))');
        $this->addSql('CREATE UNIQUE INDEX beneficiary_tbe_x1 ON beneficiary_tbe (individu_id)');
        $this->addSql('CREATE INDEX beneficiary_tbe_x2 ON beneficiary_tbe (nip)');
        $this->addSql('CREATE INDEX beneficiary_tbe_x3 ON beneficiary_tbe (posted_by)');
        $this->addSql('CREATE INDEX beneficiary_tbe_x4 ON beneficiary_tbe (updated_by)');
        $this->addSql('COMMENT ON COLUMN beneficiary_tbe.mapel IS \'Mata pelajaran yang diajarkan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_tbe.prestasi IS \'Sertifikat kompetensi/prestasi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_tbe.pelatihan IS \'Pelatihan terkait pembelajaran teknologi yang pernah diikuti\'');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD CONSTRAINT FK_7B4484B8480B6028 FOREIGN KEY (individu_id) REFERENCES beneficiary_individu (individu_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD CONSTRAINT FK_7B4484B8AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD CONSTRAINT FK_7B4484B816FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD posted_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD updated_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD CONSTRAINT FK_2F341ED1AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD CONSTRAINT FK_2F341ED116FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX beneficiary_beasiswa_x5 ON beneficiary_beasiswa (posted_by)');
        $this->addSql('CREATE INDEX beneficiary_beasiswa_x6 ON beneficiary_beasiswa (updated_by)');

        $this->addSql('ALTER TABLE beneficiary_institusi DROP posted_by');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD posted_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD updated_by INT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.kategori IS \'Valid values:
            1 = PAUD
            2 = SMK
            3 = POLTEK
            4 = PESANTREN
            5 = NON-PESANTREN/YAYASAN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_pembinaan IS \'Status pembinaan penerima manfaat, valid values:
            0 = Baru diikutsertakan
            1 = Pemantauan atau monitoring
            2 = Selesai pembinaan\'');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD CONSTRAINT FK_C34B43FDAE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_institusi ADD CONSTRAINT FK_C34B43FD16FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX beneficiary_institusi_x7 ON beneficiary_institusi (posted_by)');
        $this->addSql('CREATE INDEX beneficiary_institusi_x8 ON beneficiary_institusi (updated_by)');

        $this->addSql('ALTER TABLE beneficiary_individu DROP posted_by');
        $this->addSql('ALTER TABLE beneficiary_individu ADD posted_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_individu ADD updated_by INT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.kategori IS \'Valid values:
            1 = SANTRI
            2 = SISWA
            3 = MAHASISWA
            4 = USTADZ
            5 = GURU
            6 = DOSEN
            7 = MANAJEMEN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.jenis_kelamin IS \'Valid values:
            L = Laki-Laki, P = Perempuan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_jabatan IS \'Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen.
            Valid values: PNS, DTT, DTY, GTT, GTY\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('ALTER TABLE beneficiary_individu ADD CONSTRAINT FK_683ABC6DAE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE beneficiary_individu ADD CONSTRAINT FK_683ABC6D16FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX beneficiary_individu_x7 ON beneficiary_individu (posted_by)');
        $this->addSql('CREATE INDEX beneficiary_individu_x8 ON beneficiary_individu (updated_by)');
        $this->addSql('COMMENT ON COLUMN kegiatan.submitted IS \'Sudah di submit ke Adaro atau belum. Keterangan:
            1 = Submitted for Review
            2 = Submitted for Approval\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_step IS \'Tahapan approval yang sudah dilalui. Keterangan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');

        $this->addSql('ALTER TABLE ms_batch_csr ADD mgt_fee DOUBLE PRECISION DEFAULT \'5\'');
        $this->addSql('COMMENT ON COLUMN ms_batch_csr.mgt_fee IS \'Manajemen Fee dalam persen\'');

        $this->addSql('ALTER TABLE pendanaan_program ADD batch_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pendanaan_program ADD posted_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pendanaan_program ADD updated_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pendanaan_program ADD CONSTRAINT FK_7E20BB26F39EBE7A FOREIGN KEY (batch_id) REFERENCES ms_batch_csr (batch_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pendanaan_program ADD CONSTRAINT FK_7E20BB26AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pendanaan_program ADD CONSTRAINT FK_7E20BB2616FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7E20BB26F39EBE7A ON pendanaan_program (batch_id)');
        $this->addSql('CREATE INDEX program_x4 ON pendanaan_program (posted_by)');
        $this->addSql('CREATE INDEX program_x5 ON pendanaan_program (updated_by)');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.approval_step IS \'Tahapan approval, keterangan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE beneficiary_tbe');
        $this->addSql('DROP TABLE subyek_monitoring');
        $this->addSql('ALTER TABLE beneficiary_individu DROP CONSTRAINT FK_683ABC6DAE36D154');
        $this->addSql('ALTER TABLE beneficiary_individu DROP CONSTRAINT FK_683ABC6D16FE72E1');
        $this->addSql('DROP INDEX beneficiary_individu_x7');
        $this->addSql('DROP INDEX beneficiary_individu_x8');
        $this->addSql('ALTER TABLE beneficiary_individu DROP updated_by');
        $this->addSql('ALTER TABLE beneficiary_individu ALTER posted_by TYPE VARCHAR(50)');
        $this->addSql('ALTER TABLE beneficiary_individu ALTER posted_by DROP DEFAULT');
        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP CONSTRAINT FK_2F341ED1AE36D154');
        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP CONSTRAINT FK_2F341ED116FE72E1');
        $this->addSql('DROP INDEX beneficiary_beasiswa_x5');
        $this->addSql('DROP INDEX beneficiary_beasiswa_x6');
        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP posted_by');
        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP updated_by');
        $this->addSql('ALTER TABLE beneficiary_institusi DROP CONSTRAINT FK_C34B43FDAE36D154');
        $this->addSql('ALTER TABLE beneficiary_institusi DROP CONSTRAINT FK_C34B43FD16FE72E1');
        $this->addSql('DROP INDEX beneficiary_institusi_x7');
        $this->addSql('DROP INDEX beneficiary_institusi_x8');
        $this->addSql('ALTER TABLE beneficiary_institusi DROP updated_by');
        $this->addSql('ALTER TABLE beneficiary_institusi ALTER posted_by TYPE VARCHAR(50)');
        $this->addSql('ALTER TABLE beneficiary_institusi ALTER posted_by DROP DEFAULT');
        $this->addSql('ALTER TABLE ms_batch_csr DROP mgt_fee');
        $this->addSql('ALTER TABLE pendanaan_program DROP CONSTRAINT FK_7E20BB26F39EBE7A');
        $this->addSql('ALTER TABLE pendanaan_program DROP CONSTRAINT FK_7E20BB26AE36D154');
        $this->addSql('ALTER TABLE pendanaan_program DROP CONSTRAINT FK_7E20BB2616FE72E1');
        $this->addSql('DROP INDEX IDX_7E20BB26F39EBE7A');
        $this->addSql('DROP INDEX program_x4');
        $this->addSql('DROP INDEX program_x5');
        $this->addSql('ALTER TABLE pendanaan_program DROP batch_id');
        $this->addSql('ALTER TABLE pendanaan_program DROP posted_by');
        $this->addSql('ALTER TABLE pendanaan_program DROP updated_by');
    }

}
