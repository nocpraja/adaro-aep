<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200723071346 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE pencairan_items DROP CONSTRAINT FK_DF9EEE776F23375');
        $this->addSql('ALTER TABLE pencairan_items ALTER title DROP DEFAULT');
        $this->addSql('ALTER TABLE pencairan_items ADD CONSTRAINT FK_DF9EEE776F23375 FOREIGN KEY (pencairan_id) REFERENCES pencairan (pencairan_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pencairan ADD transfer_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE pencairan ADD nomor_voucher VARCHAR(80) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE pencairan DROP transfer_date');
        $this->addSql('ALTER TABLE pencairan DROP nomor_voucher');
        $this->addSql('ALTER TABLE pencairan_items DROP CONSTRAINT fk_df9eee776f23375');
        $this->addSql('ALTER TABLE pencairan_items ALTER title SET DEFAULT \'0\'');
        $this->addSql('ALTER TABLE pencairan_items ADD CONSTRAINT fk_df9eee776f23375 FOREIGN KEY (pencairan_id) REFERENCES pencairan (pencairan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
