<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190128191830 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE ms_mitra SET posted_by = NULL');
        $this->addSql('ALTER TABLE ms_mitra ADD updated_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ms_mitra ALTER COLUMN posted_by DROP DEFAULT');
        $this->addSql('ALTER TABLE ms_mitra ALTER COLUMN posted_by TYPE INTEGER USING posted_by::integer');
        $this->addSql('ALTER TABLE ms_mitra ALTER COLUMN posted_by TYPE INT');
        $this->addSql('ALTER TABLE ms_mitra ADD CONSTRAINT FK_5D5C9BD4AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ms_mitra ADD CONSTRAINT FK_5D5C9BD416FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX ms_mitra_x5 ON ms_mitra (posted_by)');
        $this->addSql('CREATE INDEX ms_mitra_x6 ON ms_mitra (updated_by)');
        $this->addSql('UPDATE ms_mitra SET posted_by = (SELECT uid FROM user_account WHERE username = \'admin-aep\')');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE ms_mitra DROP CONSTRAINT FK_5D5C9BD4AE36D154');
        $this->addSql('ALTER TABLE ms_mitra DROP CONSTRAINT FK_5D5C9BD416FE72E1');
        $this->addSql('DROP INDEX ms_mitra_x5');
        $this->addSql('DROP INDEX ms_mitra_x6');
        $this->addSql('ALTER TABLE ms_mitra DROP updated_by');
        $this->addSql('ALTER TABLE ms_mitra ALTER posted_by TYPE VARCHAR(50)');
        $this->addSql('ALTER TABLE ms_mitra ALTER posted_by DROP DEFAULT');
    }
}
