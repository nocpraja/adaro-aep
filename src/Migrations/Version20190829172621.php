<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190829172621 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Tabel untuk menyimpan data realisasi anggaran kegiatan';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE item_realisasi_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE item_realisasi (id_realisasi BIGSERIAL NOT NULL, id_kegiatan BIGINT NOT NULL, id_subkegiatan BIGINT NOT NULL, id_rab BIGINT NOT NULL, realisasi DOUBLE PRECISION DEFAULT \'0\', tgl_realisasi DATE NOT NULL, keterangan TEXT DEFAULT NULL, status VARCHAR(50) DEFAULT \'REALISASI_CREATED\' NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, posted_by INT DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_by INT DEFAULT NULL, PRIMARY KEY(id_realisasi))');
        $this->addSql('CREATE INDEX item_realisasi_x1 ON item_realisasi (id_kegiatan)');
        $this->addSql('CREATE INDEX item_realisasi_x2 ON item_realisasi (id_subkegiatan)');
        $this->addSql('CREATE INDEX item_realisasi_x3 ON item_realisasi (id_rab)');
        $this->addSql('CREATE INDEX item_realisasi_x4 ON item_realisasi (posted_by)');
        $this->addSql('ALTER TABLE item_realisasi ADD CONSTRAINT FK1_REALISASIKEGIATAN FOREIGN KEY (id_kegiatan) REFERENCES kegiatan (kegiatan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE item_realisasi ADD CONSTRAINT FK2_REALISASISUB FOREIGN KEY (id_subkegiatan) REFERENCES subkegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE item_realisasi ADD CONSTRAINT FK3_REALISASIRAB FOREIGN KEY (id_rab) REFERENCES item_anggaran (ids) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE item_realisasi ADD CONSTRAINT FK4_REALISASIUID FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE item_realisasi_id_seq CASCADE');
        $this->addSql('ALTER TABLE item_realisasi DROP CONSTRAINT FK1_REALISASIKEGIATAN');
        $this->addSql('ALTER TABLE item_realisasi DROP CONSTRAINT FK2_REALISASISUB');
        $this->addSql('ALTER TABLE item_realisasi DROP CONSTRAINT FK3_REALISASIRAB');
        $this->addSql('ALTER TABLE item_realisasi DROP CONSTRAINT FK4_REALISASIUID');
        $this->addSql('DROP TABLE item_realisasi');
    }
}
