<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180819074935 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DELETE FROM user_account WHERE group_id = 1');
        $this->addSql('DELETE FROM user_account WHERE uid = 1');
        $this->addSql('DELETE FROM user_group WHERE group_id = 1');
        $this->addSql('INSERT INTO user_group (group_id, group_name, enabled, protected, description, roles) VALUES(1, \'Sistem Admin\', true, true, \'Grup untuk Sistem Administrator\', \'["ROLE_USER", "ROLE_ADMIN"]\')');
        $this->addSql('INSERT INTO user_account (uid, group_id, username, full_name, user_password, enabled, posted_date) VALUES(1, 1, \'admin-aep\', \'Administrator\', \'$argon2i$v=19$m=16384,t=2,p=4$dGx1SzNtdTIwOHRNYXNvNg$1uR+yOQ5cqcCQsei2qtDoulPDvrbSGev1bfCwc3yYjw\', true, now())');
//        $this->addSql('CREATE TABLE kabupaten (kab_id SERIAL NOT NULL, prov_id INT NOT NULL, kode_kabupaten VARCHAR(10) NOT NULL, nama_kabupaten VARCHAR(250) NOT NULL, geom geometry(MULTIPOLYGON, 4326) DEFAULT NULL, PRIMARY KEY(kab_id))');
//        $this->addSql('CREATE INDEX kabupaten_x1 ON kabupaten (prov_id)');
//        $this->addSql('CREATE UNIQUE INDEX ak_x1_kabupaten ON kabupaten (kode_kabupaten)');
//        $this->addSql('COMMENT ON COLUMN kabupaten.kode_kabupaten IS \'Lihat kode kabupaten dari Depdagri\'');
//        $this->addSql('CREATE TABLE kecamatan (kec_id SERIAL NOT NULL, kab_id INT NOT NULL, kode_kecamatan VARCHAR(15) NOT NULL, nama_kecamatan VARCHAR(250) NOT NULL, geom geometry(MULTIPOLYGON, 4326) DEFAULT NULL, PRIMARY KEY(kec_id))');
//        $this->addSql('CREATE INDEX kecamatan_x1 ON kecamatan (kab_id)');
//        $this->addSql('CREATE UNIQUE INDEX ak_x1_kecamatan ON kecamatan (kode_kecamatan)');
//        $this->addSql('COMMENT ON COLUMN kecamatan.kode_kecamatan IS \'Lihat kode kecamatan dari Depdagri\'');
//        $this->addSql('CREATE TABLE kelurahan (kel_id SERIAL NOT NULL, kec_id INT NOT NULL, kode_kelurahan VARCHAR(25) DEFAULT NULL, nama_kelurahan VARCHAR(250) NOT NULL, geom geometry(MULTIPOLYGON, 4326) DEFAULT NULL, PRIMARY KEY(kel_id))');
//        $this->addSql('CREATE INDEX kelurahan_x1 ON kelurahan (kec_id)');
//        $this->addSql('CREATE INDEX kelurahan_x2 ON kelurahan (kode_kelurahan)');
//        $this->addSql('COMMENT ON COLUMN kelurahan.kode_kelurahan IS \'Lihat kode kelurahan dari Depdagri\'');
//        $this->addSql('CREATE TABLE provinsi (prov_id SERIAL NOT NULL, kode_provinsi VARCHAR(4) NOT NULL, nama_provinsi VARCHAR(250) NOT NULL, geom geometry(MULTIPOLYGON, 4326) DEFAULT NULL, PRIMARY KEY(prov_id))');
//        $this->addSql('CREATE INDEX provinsi_x1 ON provinsi (nama_provinsi)');
//        $this->addSql('CREATE UNIQUE INDEX ak_x1_provinsi ON provinsi (kode_provinsi)');
//        $this->addSql('COMMENT ON COLUMN provinsi.kode_provinsi IS \'Lihat kode provinsi dari Depdagri\'');
        $this->addSql('CREATE TABLE audit_log (id BIGSERIAL NOT NULL, posted_by VARCHAR(50) DEFAULT NULL, log_level VARCHAR(25) NOT NULL, log_message TEXT NOT NULL, entity_name VARCHAR(200) DEFAULT NULL, ip_address VARCHAR(50) DEFAULT NULL, user_agent VARCHAR(500) DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX audit_log_x1 ON audit_log (log_message)');
        $this->addSql('CREATE INDEX audit_log_x2 ON audit_log (posted_by)');
        $this->addSql('CREATE UNIQUE INDEX ak_x1_user_account ON user_account (username)');
//        $this->addSql('ALTER TABLE kabupaten ADD CONSTRAINT FK_F88809D4FBD8A061 FOREIGN KEY (prov_id) REFERENCES provinsi (prov_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
//        $this->addSql('ALTER TABLE kecamatan ADD CONSTRAINT FK_BEEEDA207F8C236D FOREIGN KEY (kab_id) REFERENCES kabupaten (kab_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
//        $this->addSql('ALTER TABLE kelurahan ADD CONSTRAINT FK_276DB00332B0E2C8 FOREIGN KEY (kec_id) REFERENCES kecamatan (kec_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE audit_log ADD CONSTRAINT FK_F6E1C0F5AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (username) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE kecamatan DROP CONSTRAINT FK_BEEEDA207F8C236D');
        $this->addSql('ALTER TABLE kelurahan DROP CONSTRAINT FK_276DB00332B0E2C8');
        $this->addSql('ALTER TABLE kabupaten DROP CONSTRAINT FK_F88809D4FBD8A061');
        $this->addSql('DROP TABLE kabupaten');
        $this->addSql('DROP TABLE kecamatan');
        $this->addSql('DROP TABLE kelurahan');
        $this->addSql('DROP TABLE provinsi');
        $this->addSql('DROP TABLE audit_log');
        $this->addSql('DROP INDEX ak_x1_user_account');
    }
}
