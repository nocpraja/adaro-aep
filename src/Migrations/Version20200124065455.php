<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200124065455 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX ms_batch_tahun_x1');
        $this->addSql('ALTER TABLE ms_batch_csr_tahun ALTER tahun SET NOT NULL');
        $this->addSql('CREATE INDEX ms_batch_tahun_x1 ON ms_batch_csr_tahun (batch_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX ms_batch_tahun_x1');
        $this->addSql('ALTER TABLE ms_batch_csr_tahun ALTER tahun DROP NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX ms_batch_tahun_x1 ON ms_batch_csr_tahun (batch_id)');
    }
}
