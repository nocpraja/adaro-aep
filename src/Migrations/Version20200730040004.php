<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200730040004 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE humas ADD analisa TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE humas ADD score SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE humas ADD analyze_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE humas ADD analyze_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN humas.analyze_by IS \'User yang menganalisa dampak media\'');
        $this->addSql('CREATE INDEX humas_x4 ON humas (analyze_by)');
        $this->addSql('ALTER TABLE humas ADD CONSTRAINT FK4_HUMAS_ANALYZE FOREIGN KEY (analyze_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX humas_x4');
        $this->addSql('ALTER TABLE humas DROP CONSTRAINT FK4_HUMAS_ANALYZE');

        $this->addSql('ALTER TABLE humas DROP analisa');
        $this->addSql('ALTER TABLE humas DROP score');
        $this->addSql('ALTER TABLE humas DROP analyze_by');
        $this->addSql('ALTER TABLE humas DROP analyze_date');
    }
}
