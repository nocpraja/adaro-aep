<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181107030659 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE log_aprv_program (log_id BIGSERIAL NOT NULL, program_id INT NOT NULL, posted_by INT DEFAULT NULL, title VARCHAR(250) NOT NULL, status VARCHAR(25) NOT NULL, jumlah DOUBLE PRECISION DEFAULT \'0\' NOT NULL, keterangan TEXT DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(log_id))');
        $this->addSql('CREATE INDEX log_aprv_program_x1 ON log_aprv_program (program_id)');
        $this->addSql('CREATE INDEX log_aprv_program_x2 ON log_aprv_program (posted_by)');
        $this->addSql('CREATE TABLE ts_pendanaan_program (ts_id BIGSERIAL NOT NULL, program_id INT NOT NULL, posted_by INT DEFAULT NULL, ts_name VARCHAR(255) NOT NULL, flag VARCHAR(25) NOT NULL, jumlah DOUBLE PRECISION DEFAULT \'0\' NOT NULL, saldo DOUBLE PRECISION DEFAULT \'0\' NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(ts_id))');
        $this->addSql('CREATE INDEX ts_pendanaan_program_x1 ON ts_pendanaan_program (program_id)');
        $this->addSql('CREATE INDEX ts_pendanaan_program_x2 ON ts_pendanaan_program (posted_by)');
        $this->addSql('CREATE TABLE ts_revisi_program (ts_id BIGSERIAL NOT NULL, program_id INT NOT NULL, posted_by INT DEFAULT NULL, ts_name VARCHAR(255) NOT NULL, status VARCHAR(25) NOT NULL, jumlah DOUBLE PRECISION DEFAULT \'0\' NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(ts_id))');
        $this->addSql('CREATE INDEX ts_revisi_program_x1 ON ts_revisi_program (program_id)');
        $this->addSql('CREATE INDEX ts_revisi_program_x2 ON ts_revisi_program (posted_by)');
        $this->addSql('ALTER TABLE log_aprv_program ADD CONSTRAINT FK_1CD380373EB8070A FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_aprv_program ADD CONSTRAINT FK_1CD38037AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_pendanaan_program ADD CONSTRAINT FK_80FCCEF83EB8070A FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_pendanaan_program ADD CONSTRAINT FK_80FCCEF8AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_revisi_program ADD CONSTRAINT FK_64E6EABD3EB8070A FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_revisi_program ADD CONSTRAINT FK_64E6EABDAE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pendanaan_program RENAME COLUMN anggaran TO alokasi_dana');
        $this->addSql('ALTER TABLE pendanaan_program ADD alokasi_rab DOUBLE PRECISION DEFAULT \'0\'');
        $this->addSql('ALTER TABLE pendanaan_program ADD has_request BOOLEAN');
        $this->addSql('ALTER TABLE pendanaan_program ADD progres_status VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE pendanaan_program ALTER status DROP DEFAULT');
        $this->addSql('ALTER TABLE pendanaan_program ALTER status TYPE VARCHAR(25) USING status::varchar');
        $this->addSql('ALTER TABLE pendanaan_program ALTER status TYPE VARCHAR(25)');
        $this->addSql('ALTER TABLE pendanaan_program ALTER realisasi SET DEFAULT \'0\'');

        $this->addSql('UPDATE pendanaan_program SET status = \'NEW\' WHERE status = \'0\'');
        $this->addSql('UPDATE pendanaan_program SET status = \'NEW\' WHERE status = \'1\'');
        $this->addSql('UPDATE pendanaan_program SET has_request = false');
        $this->addSql('ALTER TABLE pendanaan_program ALTER COLUMN has_request SET NOT NULL');
        $this->addSql('CREATE INDEX program_x3 ON pendanaan_program (status)');

        $this->addSql('COMMENT ON COLUMN log_aprv_program.status IS \'Valid values: REJECTED, APPROVED\'');
        $this->addSql('COMMENT ON COLUMN ts_revisi_program.status IS \'Valid values: REQUEST, APPROVED, REJECTED\'');
        $this->addSql('COMMENT ON COLUMN ts_pendanaan_program.flag IS \'Valid values: SETUP, SOURCE, RECLASS, REQUEST, CLOSING\'');
        $this->addSql('COMMENT ON COLUMN pendanaan_program.progres_status IS \'Valid values: REJECTED, APPROVED\'');
        $this->addSql('COMMENT ON COLUMN pendanaan_program.status IS \'Valid values: NEW, PLANNED, ONGOING, CLOSED\'');
        $this->addSql('COMMENT ON TABLE pendanaan_program IS \'Tabel untuk menyimpan data Pendanaan Program Tahunan\'');
        $this->addSql('COMMENT ON TABLE log_aprv_program IS \'Tabel untuk menyimpan data log approval DPT\'');
        $this->addSql('COMMENT ON TABLE ts_pendanaan_program IS \'Tabel untuk menyimpan transaksi DPT\'');
        $this->addSql('COMMENT ON TABLE ts_revisi_program IS \'Tabel untuk menyimpan transaksi request perubahan DPT\'');

        $this->addSql('INSERT INTO ms_dataref(protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'FAKULTAS\', \'Nama Fakultas\', \'Grouping data nama fakultas\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'JURUSAN\', \'Nama Jurusan\', \'Grouping data nama jurusan\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'UNIT-RAB\', \'Unit Satuan RAB\', \'Grouping data unit satuan untuk RAB\', NOW())');

        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-01\', \'EKONOMI DAN BISNIS\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-02\', \'HUKUM\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-03\', \'ILMU SOSIAL DAN ILMU POLITIK\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-04\', \'KEDOKTERAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-05\', \'KEGURUAN DAN ILMU PENDIDIKAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-06\', \'KEHUTANAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-07\', \'MATEMATIKA DAN IPA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-08\', \'PERIKANAN DAN KELAUTAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-09\', \'PERTANIAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'FAKULTAS\'), false, \'FAKULTAS-10\', \'TEKNIK\', NOW())');

        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-01\', \'AGRIBISNIS\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-02\', \'AGROBISNIS PERIKANAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-03\', \'AGROEKOTEKNOLOGI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-04\', \'AGRONOMI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-05\', \'AKUNTANSI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-06\', \'ARSITEKTUR\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-07\', \'BIMBINGAN KONSELING\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-08\', \'BUDIDAYA PERAIRAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-09\', \'D3 AKUNTANSI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-10\', \'D3 ANALIS FARMASI DAN MAKANAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-11\', \'D3 PERPAJAKAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-12\', \'FARMASI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-13\', \'ILMU ADMINISTRASI BISNIS\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-14\', \'ILMU ADMINISTRASI PUBLIK\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-15\', \'ILMU EKONOMI & STUDI PEMBANGUNAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-16\', \'ILMU HUKUM\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-17\', \'ILMU KOMPUTER\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-18\', \'ILMU PEMERINTAHAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-19\', \'ILMU TANAH\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-20\', \'KEHUTANAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-21\', \'KIMIA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-22\', \'MANAJEMEN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-23\', \'MATEMATIKA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-24\', \'PEMANFAATAN SUMBER DAYA PERIKANAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-25\', \'PENDIDIKAN BAHASA DAN SASTRA INDONESIA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-26\', \'PENDIDIKAN BIOLOGI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-27\', \'PENDIDIKAN EKONOMI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-28\', \'PENDIDIKAN FISIKA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-29\', \'PENDIDIKAN GEOGRAFI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-30\', \'PENDIDIKAN GURU - PENDIDIKAN ANAK USIA DINI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-31\', \'PENDIDIKAN GURU SEKOLAH DASAR\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-32\', \'PENDIDIKAN ILMU KOMPUTER\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-33\', \'PENDIDIKAN IPA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-34\', \'PENDIDIKAN IPS\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-35\', \'PENDIDIKAN JASMANI, KESEHATAN & REKREASI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-36\', \'PENDIDIKAN KIMIA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-37\', \'PENDIDIKAN LUAR BIASA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-38\', \'PENDIDIKAN MATEMATIKA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-39\', \'PENDIDIKAN PANCASILA DAN KEWARGANEGARAAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-40\', \'PENDIDIKAN SEJARAH\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-41\', \'PENDIDIKAN SENI, DRAMA, TARI DAN MUSIK\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-42\', \'PENDIDIKAN SOSIOLOGI DAN ANTROPOLOGI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-43\', \'PROTEKSI TANAMAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-44\', \'PSIKOLOGI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-45\', \'SOSIOLOGI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-46\', \'TEKNIK KIMIA\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-47\', \'TEKNIK SIPIL\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-48\', \'TEKNOLOGI HASIL PERIKANAN\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'JURUSAN\'), false, \'JURUSAN-49\', \'TEKNOLOGI PENDIDIKAN\', NOW())');

        $this->addSql('ALTER TABLE ms_mitra ADD kode_lembaga VARCHAR(25)');
        $this->addSql('UPDATE ms_mitra SET kode_lembaga = \'IHF\' WHERE mitra_id = 1');
        $this->addSql('UPDATE ms_mitra SET kode_lembaga = \'ATMI\' WHERE mitra_id = 2');
        $this->addSql('UPDATE ms_mitra SET kode_lembaga = \'BSK\' WHERE mitra_id = 3');
        $this->addSql('UPDATE ms_mitra SET kode_lembaga = \'ULM\' WHERE mitra_id = 4');
        $this->addSql('UPDATE ms_mitra SET kode_lembaga = \'ITELL\' WHERE mitra_id = 5');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE log_aprv_program');
        $this->addSql('DROP TABLE ts_pendanaan_program');
        $this->addSql('DROP TABLE ts_revisi_program');
        $this->addSql('DROP INDEX program_x3');
        $this->addSql('ALTER TABLE pendanaan_program RENAME COLUMN alokasi_dana TO anggaran');
        $this->addSql('ALTER TABLE pendanaan_program DROP alokasi_rab');
        $this->addSql('ALTER TABLE pendanaan_program DROP has_request');
        $this->addSql('ALTER TABLE pendanaan_program DROP progres_status');
        $this->addSql('ALTER TABLE pendanaan_program ALTER realisasi DROP DEFAULT');

        $this->addSql('UPDATE pendanaan_program SET status = \'1\' WHERE status = \'NEW\'');
        $this->addSql('UPDATE pendanaan_program SET status = \'2\' WHERE status = \'PLANNED\'');
        $this->addSql('UPDATE pendanaan_program SET status = \'3\' WHERE status = \'ONGOING\'');
        $this->addSql('UPDATE pendanaan_program SET status = \'9\' WHERE status = \'CLOSED\'');

        $this->addSql('ALTER TABLE pendanaan_program ALTER status DROP DEFAULT');
        $this->addSql('ALTER TABLE pendanaan_program ALTER status TYPE SMALLINT USING status::smallint');
        $this->addSql('ALTER TABLE pendanaan_program ALTER status TYPE SMALLINT');
        $this->addSql('COMMENT ON COLUMN pendanaan_program.status IS NULL');
    }

}
