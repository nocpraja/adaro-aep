<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180927085215 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE pendanaan_program (program_id SERIAL NOT NULL, msid INT NOT NULL, tahun INT NOT NULL, nama_program VARCHAR(250) NOT NULL, status SMALLINT NOT NULL, anggaran DOUBLE PRECISION NOT NULL, realisasi DOUBLE PRECISION DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(program_id))');
        $this->addSql('CREATE INDEX program_x1 ON pendanaan_program (msid)');
        $this->addSql('CREATE INDEX program_x2 ON pendanaan_program (tahun)');
        $this->addSql('CREATE TABLE sumber_dana (ids SERIAL NOT NULL, program_id INT NOT NULL, ref_id INT NOT NULL, jumlah DOUBLE PRECISION NOT NULL, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX sumber_dana_x1 ON sumber_dana (ref_id)');
        $this->addSql('CREATE INDEX sumber_dana_x2 ON sumber_dana (program_id)');
        $this->addSql('ALTER TABLE pendanaan_program ADD CONSTRAINT FK_7E20BB26405F5364 FOREIGN KEY (msid) REFERENCES ms_program_csr (msid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sumber_dana ADD CONSTRAINT FK_3A784BC13EB8070A FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sumber_dana ADD CONSTRAINT FK_3A784BC121B741A9 FOREIGN KEY (ref_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('INSERT INTO ms_dataref(protected, ref_code, ref_name, description, posted_date) VALUES (true, \'COMPANY\', \'Adaro Company\', \'Perusahaan dalam organisasi Adaro\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(protected, ref_code, ref_name, description, posted_date) VALUES (true, \'MITRA-CSR\', \'Mitra CSR Adaro\', \'Mitra Kerja CSR Adaro\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) VALUES (1, false, \'COMPANY-AE\', \'PT. Adaro Energy Tbk (AE)\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) VALUES (1, false, \'COMPANY-AI\', \'PT. Adaro Indonesia (AI)\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) VALUES (1, false, \'COMPANY-MIP\', \'PT. Mustika Indah Permai (MIP)\', NOW())');
        $this->addSql('INSERT INTO ms_dataref(parent_id, protected, ref_code, ref_name, posted_date) VALUES (1, false, \'COMPANY-BEE\', \'PT. Bukit Enim Energi (BEE)\', NOW())');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE sumber_dana DROP CONSTRAINT FK_3A784BC13EB8070A');
        $this->addSql('DROP TABLE pendanaan_program');
        $this->addSql('DROP TABLE sumber_dana');
    }
}
