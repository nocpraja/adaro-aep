<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200321080849 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('CREATE SEQUENCE kpi_indikator_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE kpi_indikator (
idk BIGSERIAL NOT NULL, 
msid BIGINT NOT NULL, 
batch_id BIGINT NOT NULL, 
beasiswa_id BIGINT DEFAULT NULL, 
tbe_id BIGINT DEFAULT NULL, 
institusi_id BIGINT DEFAULT NULL, 
individu_id BIGINT DEFAULT NULL, 
tahun INT NOT NULL,
abfl_params JSONB DEFAULT NULL,
tbe_params JSONB DEFAULT NULL,
paud_params JSONB DEFAULT NULL,
vokasi_params JSONB DEFAULT NULL,
semester VARCHAR(2) DEFAULT NULL, 
ipk DOUBLE PRECISION DEFAULT NULL,
ipk_rate DOUBLE PRECISION DEFAULT NULL,
internal_kmp SMALLINT DEFAULT NULL, 
external_kmp SMALLINT DEFAULT NULL,
kmp_rate DOUBLE PRECISION DEFAULT NULL,
internal_csr SMALLINT DEFAULT NULL,
external_csr SMALLINT DEFAULT NULL,
csr_rate DOUBLE PRECISION DEFAULT NULL,
internal_kom SMALLINT DEFAULT NULL,
external_kom SMALLINT DEFAULT NULL,
kom_rate DOUBLE PRECISION DEFAULT NULL,
magang SMALLINT DEFAULT NULL,
magang_rate DOUBLE PRECISION DEFAULT NULL,
status VARCHAR(20) DEFAULT \'NEW\' NOT NULL,
verified_by INT DEFAULT NULL, 
tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, 
approved_by INT DEFAULT NULL, 
tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
posted_by INT DEFAULT NULL, 
posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, 
updated_by INT DEFAULT NULL, 
last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, 
PRIMARY KEY(idk))');
        $this->addSql('CREATE INDEX kpi_indikator_x1 ON kpi_indikator (msid)');
        $this->addSql('CREATE INDEX kpi_indikator_x2 ON kpi_indikator (batch_id)');
        $this->addSql('CREATE INDEX kpi_indikator_x3 ON kpi_indikator (beasiswa_id)');
        $this->addSql('CREATE INDEX kpi_indikator_x4 ON kpi_indikator (tbe_id)');
        $this->addSql('CREATE INDEX kpi_indikator_x5 ON kpi_indikator (institusi_id)');
        $this->addSql('CREATE INDEX kpi_indikator_x6 ON kpi_indikator (individu_id)');
        $this->addSql('CREATE INDEX kpi_indikator_x7 ON kpi_indikator (posted_by)');
        $this->addSql('CREATE INDEX kpi_indikator_x8 ON kpi_indikator (updated_by)');
        $this->addSql('COMMENT ON table kpi_indikator IS \'Table untuk data Indikator Penilaian KPI\'');
        $this->addSql('COMMENT ON COLUMN kpi_indikator.ipk_rate IS \'Rata-rata indikator penilaian IPK\'');
        $this->addSql('COMMENT ON COLUMN kpi_indikator.kmp_rate IS \'Rata-rata indikator penilaian Keaktifan Organisasi\'');
        $this->addSql('COMMENT ON COLUMN kpi_indikator.csr_rate IS \'Rata-rata indikator penilaian Volunteerism\'');
        $this->addSql('COMMENT ON COLUMN kpi_indikator.kom_rate IS \'Rata-rata indikator penilaian Prestasi Non Akademik\'');
        $this->addSql('COMMENT ON COLUMN kpi_indikator.magang_rate IS \'Rata-rata indikator penilaian Magang\'');
        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_PROGRAM FOREIGN KEY (msid) REFERENCES ms_program_csr (msid) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_BATCH FOREIGN KEY (batch_id) REFERENCES ms_batch_csr (batch_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_ABFL FOREIGN KEY (beasiswa_id) REFERENCES beneficiary_beasiswa (beasiswa_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_TBE FOREIGN KEY (tbe_id) REFERENCES beneficiary_tbe (tbe_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_INST FOREIGN KEY (institusi_id) REFERENCES beneficiary_institusi (institusi_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_INDV FOREIGN KEY (individu_id) REFERENCES beneficiary_individu (individu_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_POSTED FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_UPDATED FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('DROP SEQUENCE kpi_indikator_id_seq CASCADE');
        $this->addSql('DROP INDEX kpi_indikator_x1');
        $this->addSql('DROP INDEX kpi_indikator_x2');
        $this->addSql('DROP INDEX kpi_indikator_x3');
        $this->addSql('DROP INDEX kpi_indikator_x4');
        $this->addSql('DROP INDEX kpi_indikator_x5');
        $this->addSql('DROP INDEX kpi_indikator_x6');
        $this->addSql('DROP INDEX kpi_indikator_x7');
        $this->addSql('DROP INDEX kpi_indikator_x8');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_PROGRAM');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_BATCH');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_ABFL');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_TBE');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_INST');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_INDV');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_POSTED');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_UPDATED');
        $this->addSql('DROP TABLE kpi_indikator');
    }
}
