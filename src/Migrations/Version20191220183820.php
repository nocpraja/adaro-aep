<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191220183820 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE beneficiary_individu ADD latitude float8 default NULL');
        $this->addSql('ALTER TABLE beneficiary_individu ADD longitude float8 default NULL');
        $this->addSql('ALTER TABLE ms_batch_csr_tahun ADD CONSTRAINT FK_8E55B70FF39EBE7A FOREIGN KEY (batch_id) REFERENCES ms_batch_csr (batch_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE beneficiary_individu DROP latitude');
        $this->addSql('ALTER TABLE beneficiary_individu DROP longitude');
        $this->addSql('ALTER TABLE ms_batch_csr_tahun DROP CONSTRAINT FK_8E55B70FF39EBE7A');
        $this->addSql('DROP INDEX ms_batch_tahun_x1');

    }
}
