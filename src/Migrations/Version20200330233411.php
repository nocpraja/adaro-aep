<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200330233411 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE provinsi DROP geom');
        $this->addSql('ALTER TABLE kabupaten DROP geom');
        $this->addSql('ALTER TABLE kecamatan DROP geom');
        $this->addSql('ALTER TABLE kelurahan DROP geom');
        $this->addSql('ALTER TABLE ms_mitra DROP geom');
        $this->addSql('ALTER TABLE beneficiary_institusi DROP geom');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->throwIrreversibleMigrationException('Untuk Migrasi ini hanya bisa di rollback dengan backup ke keadaan sebelum penghapusan Postgis');
    }
}
