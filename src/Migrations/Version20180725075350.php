<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180725075350 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE user_account (uid SERIAL NOT NULL, group_id INT NOT NULL, username VARCHAR(50) NOT NULL, full_name VARCHAR(250) NOT NULL, user_password VARCHAR(250) NOT NULL, avatar VARCHAR(255) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, enabled BOOLEAN DEFAULT \'true\' NOT NULL, expiry TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_visited TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(uid))');
        $this->addSql('CREATE INDEX user_account_x1 ON user_account (full_name)');
        $this->addSql('CREATE INDEX user_account_x2 ON user_account (group_id)');
        $this->addSql('CREATE TABLE user_group (group_id SERIAL NOT NULL, group_name VARCHAR(200) NOT NULL, enabled BOOLEAN DEFAULT \'true\' NOT NULL, protected BOOLEAN DEFAULT \'false\' NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(group_id))');
        $this->addSql('CREATE UNIQUE INDEX ak_x1_user_group ON user_group (group_name)');
        $this->addSql('ALTER TABLE user_account ADD CONSTRAINT FK_253B48AEFE54D947 FOREIGN KEY (group_id) REFERENCES user_group (group_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_account DROP CONSTRAINT FK_253B48AEFE54D947');
        $this->addSql('DROP TABLE user_account');
        $this->addSql('DROP TABLE user_group');
    }
}
