<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200522165903 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $menu = [43, null, 'Reporting', null, null, 'Pelaporan monitoring kegiatan dan anggaran', 'print', 'true', 4, 3];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);

        $menu = [44, 43, 'Rekap', '/laporan/rekap', 'laporan_rekap', 'Rekap pemakaian anggaran', null, 'true', 3, 0];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);

        $sql = "UPDATE menu_item SET parent_id=43, position=1 WHERE route_url='/laporan/monitoring'";
        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

    }
}
