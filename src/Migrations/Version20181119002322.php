<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181119002322 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE IF EXISTS monitoring_riwayat');
        $this->addSql('CREATE TABLE monitoring_riwayat (riwayat_id BIGSERIAL NOT NULL, type TEXT NOT NULL, beneficiary_id BIGINT DEFAULT NULL, tanggal_input VARCHAR(255) DEFAULT NULL, nama_file VARCHAR(250) DEFAULT NULL, eval_score DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(riwayat_id))');
        $this->addSql('CREATE INDEX ix_bid ON monitoring_riwayat (beneficiary_id)');
        $this->addSql('CREATE INDEX ix_type ON monitoring_riwayat (type)');
        $this->addSql('ALTER TABLE kegiatan ADD jadwal_kegiatan JSONB DEFAULT NULL');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_closing DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_submitted DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_update_realisasi DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE sub_kegiatan ADD tgl_update_realisasi DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE sub_kegiatan ADD tgl_closing DATE DEFAULT NULL');

        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.kategori IS \'Valid values:
            1 = PAUD
            2 = SMK
            3 = POLTEK
            4 = PESANTREN
            5 = NON-PESANTREN/YAYASAN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_pembinaan IS \'Status pembinaan penerima manfaat, valid values:
            0 = Baru diikutsertakan
            1 = Pemantauan atau monitoring
            2 = Selesai pembinaan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.kategori IS \'Valid values:
            1 = SANTRI
            2 = SISWA
            3 = MAHASISWA
            4 = USTADZ
            5 = GURU
            6 = DOSEN
            7 = MANAJEMEN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.jenis_kelamin IS \'Valid values:
            L = Laki-Laki, P = Perempuan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_jabatan IS \'Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen.
            Valid values: PNS, DTT, DTY, GTT, GTY\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.submitted IS \'Sudah di submit ke Adaro atau belum. Keterangan:
            1 = Submitted for Review
            2 = Submitted for Approval\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_step IS \'Tahapan approval yang sudah dilalui. Keterangan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.approval_step IS \'Tahapan approval, keterangan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE monitoring_riwayat');
        $this->addSql('ALTER TABLE kegiatan DROP jadwal_kegiatan');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_closing');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_submitted');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_update_realisasi');
        $this->addSql('ALTER TABLE sub_kegiatan DROP tgl_update_realisasi');
        $this->addSql('ALTER TABLE sub_kegiatan DROP tgl_closing');
    }
}
