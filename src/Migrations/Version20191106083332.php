<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191106083332 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        // this up() migration is auto-generated, please modify it to your needs
        $menu = [30, null, 'Peta Sebaran', '/peta/sebaran/penerima-manfaat', 'peta_sebaran_penerima_manfaat', 'Peta yang menampilkan semua penerima manfaat', 'map', 'true', 2, 0];
        $sql = 'INSERT INTO menu_item (menu_id, parent_id, menu_label, route_url, route_alias, description, icon, enabled, position, url_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $this->addSql($sql, $menu);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
