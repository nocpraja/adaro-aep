<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180921030551 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE ms_dataref (id SERIAL NOT NULL, parent_id INT DEFAULT NULL, protected BOOLEAN DEFAULT \'false\' NOT NULL, ref_code VARCHAR(25) NOT NULL, ref_name VARCHAR(200) NOT NULL, ref_value JSONB DEFAULT NULL, description TEXT DEFAULT NULL, posted_by VARCHAR(50) DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX ms_dataref_x1 ON ms_dataref (parent_id)');
        $this->addSql('CREATE UNIQUE INDEX ak_x1_ms_dataref ON ms_dataref (ref_code)');
        $this->addSql('CREATE TABLE jenis_program_csr (id SERIAL NOT NULL, title VARCHAR(150) NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ms_program_csr (msid SERIAL NOT NULL, jenis_id INT NOT NULL, program VARCHAR(200) NOT NULL, template_name VARCHAR(250) DEFAULT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(msid))');
        $this->addSql('CREATE INDEX ms_program_csr_x1 ON ms_program_csr (jenis_id)');
        $this->addSql('ALTER TABLE ms_dataref ADD CONSTRAINT FK_5A0C7C1D727ACA70 FOREIGN KEY (parent_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ms_program_csr ADD CONSTRAINT FK_4C3C4DAABBD1681E FOREIGN KEY (jenis_id) REFERENCES jenis_program_csr (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE audit_log DROP CONSTRAINT FK_F6E1C0F5AE36D154');
        $this->addSql('ALTER TABLE audit_log ALTER COLUMN posted_by DROP DEFAULT');
        $this->addSql('ALTER TABLE audit_log ALTER COLUMN posted_by TYPE INTEGER USING posted_by::integer');
        $this->addSql('ALTER TABLE audit_log ALTER COLUMN posted_by TYPE INT');
        $this->addSql('ALTER TABLE audit_log ADD CONSTRAINT FK_F6E1C0F5AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('INSERT INTO jenis_program_csr(title, description) VALUES (\'EKONOMI\', \'Adaro Nyalakan Sejahtera\')');
        $this->addSql('INSERT INTO jenis_program_csr(title, description) VALUES (\'PENDIDIKAN\', \'Adaro Nyalakan Ilmu\')');
        $this->addSql('INSERT INTO jenis_program_csr(title, description) VALUES (\'KESEHATAN\', \'Adaro Nyalakan Raga\')');
        $this->addSql('INSERT INTO jenis_program_csr(title, description) VALUES (\'SOSIAL BUDAYA\', \'Adaro Nyalakan Warna\')');
        $this->addSql('INSERT INTO jenis_program_csr(title, description) VALUES (\'LINGKUNGAN\', \'Adaro Nyalakan Lestari\')');
        $this->addSql('INSERT INTO ms_program_csr(jenis_id, program, template_name) VALUES (2, \'Pembinaan PAUD\', \'Pembinaan PAUD Tahun {0}\')');
        $this->addSql('INSERT INTO ms_program_csr(jenis_id, program, template_name) VALUES (2, \'Pendidikan Vokasi\', \'Pendidikan Vokasi Tahun {0}\')');
        $this->addSql('INSERT INTO ms_program_csr(jenis_id, program, template_name) VALUES (2, \'Pelatihan Vokasi\', \'Pelatihan Vokasi Tahun {0}\')');
        $this->addSql('INSERT INTO ms_program_csr(jenis_id, program, template_name) VALUES (2, \'Beasiswa\', \'Pemberian Beasiswa Tahun {0}\')');
        $this->addSql('INSERT INTO ms_program_csr(jenis_id, program, template_name) VALUES (2, \'Tech-Based Education\', \'Tech-Based Education Tahun {0}\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE ms_dataref DROP CONSTRAINT FK_5A0C7C1D727ACA70');
        $this->addSql('ALTER TABLE ms_program_csr DROP CONSTRAINT FK_4C3C4DAABBD1681E');
        $this->addSql('DROP TABLE ms_dataref');
        $this->addSql('DROP TABLE jenis_program_csr');
        $this->addSql('DROP TABLE ms_program_csr');
        $this->addSql('ALTER TABLE audit_log DROP CONSTRAINT fk_f6e1c0f5ae36d154');
        $this->addSql('ALTER TABLE audit_log ALTER posted_by DROP DEFAULT');
        $this->addSql('ALTER TABLE audit_log ALTER posted_by TYPE VARCHAR(50)');
        $this->addSql('ALTER TABLE audit_log ADD CONSTRAINT fk_f6e1c0f5ae36d154 FOREIGN KEY (posted_by) REFERENCES user_account (username) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
