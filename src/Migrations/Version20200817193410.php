<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200817193410 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE kpi_indikator ADD data_ref INT DEFAULT NULL');
        $this->addSql('CREATE INDEX kpi_indikator_x9 ON kpi_indikator (data_ref)');

        $this->addSql('ALTER TABLE kpi_indikator ADD CONSTRAINT FK_INDIKATOR_REF FOREIGN KEY (data_ref) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX kpi_indikator_x9');
        $this->addSql('ALTER TABLE kpi_indikator DROP CONSTRAINT FK_INDIKATOR_REF');
        $this->addSql('ALTER TABLE kpi_indikator DROP data_ref');
    }
}
