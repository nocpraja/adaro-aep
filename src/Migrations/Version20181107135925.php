<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181107135925 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE beneficiary_beasiswa ADD nomor_cif VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ALTER tahun_masuk DROP NOT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ALTER sekolah_asal DROP NOT NULL');
        $this->addSql('ALTER TABLE ms_mitra ALTER kode_lembaga SET NOT NULL');
        $this->addSql('ALTER TABLE pendanaan_program ALTER alokasi_dana SET DEFAULT \'0\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_beasiswa.nomor_cif IS \'Nomor CIF Rekening Bank\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE pendanaan_program ALTER alokasi_dana DROP DEFAULT');
        $this->addSql('ALTER TABLE beneficiary_beasiswa DROP nomor_cif');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ALTER tahun_masuk SET NOT NULL');
        $this->addSql('ALTER TABLE beneficiary_beasiswa ALTER sekolah_asal SET NOT NULL');
        $this->addSql('ALTER TABLE ms_mitra ALTER kode_lembaga DROP NOT NULL');
    }
}
