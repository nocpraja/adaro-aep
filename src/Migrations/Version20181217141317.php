<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181217141317 extends AbstractMigration
{

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE beneficiary_individu ALTER kec_id DROP NOT NULL');
        $this->addSql('ALTER TABLE beneficiary_individu ALTER alamat DROP NOT NULL');
        $this->addSql('ALTER TABLE beneficiary_tbe ADD jenis_institusi SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('COMMENT ON COLUMN beneficiary_tbe.jenis_institusi IS \'Valid values:
            1 = SD, 2 = SMP, 3 = SMA\'');

        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.kategori IS \'Valid values:
            1 = PAUD
            2 = SMK
            3 = POLTEK
            4 = PESANTREN
            5 = NON KEAGAMAAN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_pembinaan IS \'Status pembinaan penerima manfaat, valid values:
            0 = Baru diikutsertakan
            1 = Pemantauan atau monitoring
            2 = Selesai pembinaan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.kategori IS \'Valid values:
            1 = SANTRI
            2 = SISWA
            3 = MAHASISWA
            4 = USTADZ
            5 = GURU
            6 = DOSEN
            7 = MANAJEMEN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.jenis_kelamin IS \'Valid values:
            L = Laki-Laki, P = Perempuan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_jabatan IS \'Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen.
            Valid values: PNS, DTT, DTY, GTT, GTY\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.submitted IS \'Sudah di submit ke Adaro atau belum. Keterangan:
            1 = Submitted for Review
            2 = Submitted for Approval\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_step IS \'Tahapan approval yang sudah dilalui. Keterangan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.approval_step IS \'Tahapan approval, keterangan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE beneficiary_tbe DROP jenis_institusi');
        $this->addSql('ALTER TABLE beneficiary_individu ALTER kec_id SET NOT NULL');
        $this->addSql('ALTER TABLE beneficiary_individu ALTER alamat SET NOT NULL');
    }
}
