<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200113155322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE kegiatan_docs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE kegiatan_docs (doc_id BIGSERIAL NOT NULL, kegiatan_id BIGINT NOT NULL, sub_id BIGINT NOT NULL, category VARCHAR(25) NOT NULL, title VARCHAR(250) NOT NULL, base_folder VARCHAR(250) NOT NULL, base_path VARCHAR(250) NOT NULL, mime_type VARCHAR(250) DEFAULT NULL, extension VARCHAR(10) DEFAULT NULL, docsize INT DEFAULT NULL, status VARCHAR(20) DEFAULT \'NEW\' NOT NULL, verified_by INT DEFAULT NULL, tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, posted_by INT DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_by INT DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(doc_id))');
        $this->addSql('COMMENT ON COLUMN kegiatan_docs.status IS \'Valid values: NEW, VERIFIED, UNPUBLISHED, REJECTED\'');
        $this->addSql('CREATE INDEX kegiatan_docs_x1 ON kegiatan_docs (kegiatan_id)');
        $this->addSql('CREATE INDEX kegiatan_docs_x2 ON kegiatan_docs (sub_id)');
        $this->addSql('CREATE INDEX kegiatan_docs_x3 ON kegiatan_docs (posted_by)');
        $this->addSql('CREATE INDEX kegiatan_docs_x4 ON kegiatan_docs (verified_by)');
        $this->addSql('ALTER TABLE kegiatan_docs ADD CONSTRAINT FK1_DOCS_KEGIATAN FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan_docs ADD CONSTRAINT FK2_DOCS_SUBKEGIATAN FOREIGN KEY (sub_id) REFERENCES subkegiatan (sub_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan_docs ADD CONSTRAINT FK3_DOCS_AUTHOR FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan_docs ADD CONSTRAINT FK4_DOCS_VERIFY FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('DROP SEQUENCE kegiatan_docs_id_seq CASCADE');
        $this->addSql('DROP INDEX kegiatan_docs_x1');
        $this->addSql('DROP INDEX kegiatan_docs_x2');
        $this->addSql('DROP INDEX kegiatan_docs_x3');
        $this->addSql('DROP INDEX kegiatan_docs_x4');
        $this->addSql('ALTER TABLE kegiatan_docs DROP CONSTRAINT FK1_DOCS_KEGIATAN');
        $this->addSql('ALTER TABLE kegiatan_docs DROP CONSTRAINT FK2_DOCS_SUBKEGIATAN');
        $this->addSql('ALTER TABLE kegiatan_docs DROP CONSTRAINT FK3_DOCS_AUTHOR');
        $this->addSql('ALTER TABLE kegiatan_docs DROP CONSTRAINT FK4_DOCS_VERIFY');
        $this->addSql('DROP TABLE kegiatan_docs');
    }
}
