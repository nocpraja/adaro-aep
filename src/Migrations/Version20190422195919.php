<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190422195919 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE sumber_dana_ids_seq CASCADE');
        $this->addSql('DROP TABLE sumber_dana');

        $this->addSql('CREATE TABLE ms_numbering (category VARCHAR(250) NOT NULL, param1 VARCHAR(25) NOT NULL, param2 VARCHAR(25), value INT NOT NULL, PRIMARY KEY(category, param1))');
        $this->addSql('CREATE TABLE app_setting (section VARCHAR(150) NOT NULL, attribute VARCHAR(250) NOT NULL, attribute_type VARCHAR(25) NOT NULL, attribute_value TEXT NOT NULL, hidden BOOLEAN DEFAULT \'false\' NOT NULL, position INT DEFAULT 0 NOT NULL, label VARCHAR(200), description TEXT, PRIMARY KEY(section, attribute))');
        $this->addSql('COMMENT ON TABLE app_setting IS \'Data konfigurasi aplikasi\'');
        $this->addSql('COMMENT ON COLUMN app_setting.attribute_type IS \'Valid value: [Boolean, String, Json, Integer, Float]\'');

        $this->addSql('CREATE TABLE dana_batch (bp_id BIGSERIAL NOT NULL, dp_id BIGINT NOT NULL, batch_id INT NOT NULL, verified_by INT, approved_by INT, closed_by INT, posted_by INT, updated_by INT, nama_program VARCHAR(255) NOT NULL, tahun INT NOT NULL, status VARCHAR(50) DEFAULT \'NEW\' NOT NULL, kode VARCHAR(25) NOT NULL, jumlah DOUBLE PRECISION NOT NULL, alokasi DOUBLE PRECISION DEFAULT \'0\', realisasi DOUBLE PRECISION DEFAULT \'0\', tags JSONB, tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE, tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE, tgl_closed TIMESTAMP(0) WITHOUT TIME ZONE, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, last_updated TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(bp_id))');
        $this->addSql('CREATE INDEX dana_batch_x1 ON dana_batch (dp_id)');
        $this->addSql('CREATE INDEX dana_batch_x2 ON dana_batch (batch_id)');
        $this->addSql('CREATE INDEX dana_batch_x3 ON dana_batch (kode)');
        $this->addSql('CREATE INDEX dana_batch_x4 ON dana_batch (verified_by)');
        $this->addSql('CREATE INDEX dana_batch_x5 ON dana_batch (approved_by)');
        $this->addSql('CREATE INDEX dana_batch_x6 ON dana_batch (closed_by)');
        $this->addSql('CREATE INDEX dana_batch_x7 ON dana_batch (posted_by)');
        $this->addSql('CREATE INDEX dana_batch_x8 ON dana_batch (updated_by)');
        $this->addSql('COMMENT ON COLUMN dana_batch.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_batch.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * BP-yy.0000 => untuk transaksi Dana Batch\'');

        $this->addSql('CREATE TABLE dana_bidang (db_id BIGSERIAL NOT NULL, bidang_id INT NOT NULL, verified_by INT, approved_by INT, closed_by INT, posted_by INT, updated_by INT, status VARCHAR(50) DEFAULT \'NEW\' NOT NULL, kode VARCHAR(25) NOT NULL, jumlah DOUBLE PRECISION NOT NULL, alokasi DOUBLE PRECISION DEFAULT \'0\', realisasi DOUBLE PRECISION DEFAULT \'0\', tags JSONB, title VARCHAR(255) NOT NULL, tahun_awal INT NOT NULL, tahun_akhir INT NOT NULL, tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE, tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE, tgl_closed TIMESTAMP(0) WITHOUT TIME ZONE, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, last_updated TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(db_id))');
        $this->addSql('CREATE INDEX dana_bidang_x1 ON dana_bidang (bidang_id)');
        $this->addSql('CREATE INDEX dana_bidang_x2 ON dana_bidang (kode)');
        $this->addSql('CREATE INDEX dana_bidang_x3 ON dana_bidang (verified_by)');
        $this->addSql('CREATE INDEX dana_bidang_x4 ON dana_bidang (approved_by)');
        $this->addSql('CREATE INDEX dana_bidang_x5 ON dana_bidang (closed_by)');
        $this->addSql('CREATE INDEX dana_bidang_x6 ON dana_bidang (posted_by)');
        $this->addSql('CREATE INDEX dana_bidang_x7 ON dana_bidang (updated_by)');
        $this->addSql('COMMENT ON COLUMN dana_bidang.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_bidang.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * BP-yy.0000 => untuk transaksi Dana Batch\'');

        $this->addSql('CREATE TABLE dana_global (dg_id BIGSERIAL NOT NULL, verified_by INT, posted_by INT, updated_by INT, status VARCHAR(50) DEFAULT \'NEW\' NOT NULL, kode VARCHAR(25) NOT NULL, jumlah DOUBLE PRECISION NOT NULL, title VARCHAR(255) NOT NULL, tahun_awal INT NOT NULL, tahun_akhir INT NOT NULL, tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, last_updated TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(dg_id))');
        $this->addSql('CREATE INDEX dana_global_x1 ON dana_global (kode)');
        $this->addSql('CREATE INDEX dana_global_x2 ON dana_global (verified_by)');
        $this->addSql('CREATE INDEX dana_global_x3 ON dana_global (posted_by)');
        $this->addSql('CREATE INDEX dana_global_x4 ON dana_global (updated_by)');
        $this->addSql('COMMENT ON COLUMN dana_global.status IS \'Valid values: NEW, UPDATED, VERIFIED, REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_global.jumlah IS \'Nilai anggaran dana (dalam Rupiah)\'');
        $this->addSql('COMMENT ON COLUMN dana_global.kode IS \'Kode untuk referensi Riwayat Transaksi. Format: DG-yy.0000\'');

        $this->addSql('CREATE TABLE dana_program (dp_id BIGSERIAL NOT NULL, db_id BIGINT NOT NULL, msid INT NOT NULL, verified_by INT, approved_by INT, closed_by INT, posted_by INT, updated_by INT, status VARCHAR(50) DEFAULT \'NEW\' NOT NULL, kode VARCHAR(25) NOT NULL, jumlah DOUBLE PRECISION NOT NULL, alokasi DOUBLE PRECISION DEFAULT \'0\', realisasi DOUBLE PRECISION DEFAULT \'0\', tags JSONB, title VARCHAR(255) NOT NULL, tahun_awal INT NOT NULL, tahun_akhir INT NOT NULL, tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE, tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE, tgl_closed TIMESTAMP(0) WITHOUT TIME ZONE, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, last_updated TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(dp_id))');
        $this->addSql('CREATE INDEX dana_program_x1 ON dana_program (db_id)');
        $this->addSql('CREATE INDEX dana_program_x2 ON dana_program (msid)');
        $this->addSql('CREATE INDEX dana_program_x3 ON dana_program (kode)');
        $this->addSql('CREATE INDEX dana_program_x4 ON dana_program (verified_by)');
        $this->addSql('CREATE INDEX dana_program_x5 ON dana_program (approved_by)');
        $this->addSql('CREATE INDEX dana_program_x6 ON dana_program (closed_by)');
        $this->addSql('CREATE INDEX dana_program_x7 ON dana_program (posted_by)');
        $this->addSql('CREATE INDEX dana_program_x8 ON dana_program (updated_by)');
        $this->addSql('COMMENT ON COLUMN dana_program.status IS \'Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED\'');
        $this->addSql('COMMENT ON COLUMN dana_program.kode IS \'Kode untuk referensi Riwayat Transaksi. Format:
             * DG-yy.0000 => untuk transaksi Dana Global
             * DB-yy.0000 => untuk transaksi Dana Bidang
             * DP-yy.0000 => untuk transaksi Dana Program
             * BP-yy.0000 => untuk transaksi Dana Batch\'');

        $this->addSql('CREATE TABLE donatur_sumber_dana (ids BIGSERIAL NOT NULL, bp_id BIGINT NOT NULL, ref_id INT NOT NULL, jumlah DOUBLE PRECISION NOT NULL, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX donatur_sumber_dana_x1 ON donatur_sumber_dana (ref_id)');
        $this->addSql('CREATE INDEX donatur_sumber_dana_x2 ON donatur_sumber_dana (bp_id)');

        $this->addSql('CREATE TABLE log_aprv_pendanaan (log_id BIGSERIAL NOT NULL, dg_id BIGINT, db_id BIGINT, dp_id BIGINT, bp_id BIGINT, posted_by INT, kategori SMALLINT NOT NULL, log_action VARCHAR(50) NOT NULL, log_title VARCHAR(255) NOT NULL, refcode VARCHAR(25) NOT NULL, from_status VARCHAR(50) NOT NULL, to_status VARCHAR(50) NOT NULL, keterangan TEXT, jumlah DOUBLE PRECISION, tahun_awal INT, tahun_akhir INT, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(log_id))');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x1 ON log_aprv_pendanaan (kategori)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x2 ON log_aprv_pendanaan (refcode)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x3 ON log_aprv_pendanaan (dg_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x4 ON log_aprv_pendanaan (db_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x5 ON log_aprv_pendanaan (dp_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x6 ON log_aprv_pendanaan (bp_id)');
        $this->addSql('CREATE INDEX log_aprv_pendanaan_x7 ON log_aprv_pendanaan (posted_by)');
        $this->addSql('COMMENT ON TABLE log_aprv_pendanaan IS \'Riwayat verifikasi dan approval pendanaan\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.kategori IS \'Kategori log pendanaan. Valid values:
            1 = Dana Global
            2 = Dana Bidang
            3 = Dana Program
            4 = Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.refcode IS \'Kode referensi transaksi\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.log_action IS \'Tindakan yang dilakukan\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.from_status IS \'Status pendanaan sebelum tindakan verifikasi ataupun approval\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_pendanaan.to_status IS \'Status pendanaan setelah tindakan verifikasi ataupun approval\'');

        $this->addSql('CREATE TABLE ts_dana_batch (tsid BIGSERIAL NOT NULL, posted_by INT, refcode VARCHAR(25) NOT NULL, ts_name VARCHAR(255) NOT NULL, tahun_awal INT, tahun_akhir INT, jumlah DOUBLE PRECISION NOT NULL, saldo DOUBLE PRECISION NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(tsid))');
        $this->addSql('CREATE INDEX ts_dana_batch_x1 ON ts_dana_batch (posted_by)');
        $this->addSql('COMMENT ON TABLE ts_dana_batch IS \'Data riwayat transaksi Dana Batch\'');
        $this->addSql('COMMENT ON COLUMN ts_dana_batch.refcode IS \'Kode referensi transaksi\'');

        $this->addSql('CREATE TABLE ts_dana_bidang (tsid BIGSERIAL NOT NULL, posted_by INT, refcode VARCHAR(25) NOT NULL, ts_name VARCHAR(255) NOT NULL, tahun_awal INT, tahun_akhir INT, jumlah DOUBLE PRECISION NOT NULL, saldo DOUBLE PRECISION NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(tsid))');
        $this->addSql('CREATE INDEX ts_dana_bidang_x1 ON ts_dana_bidang (posted_by)');
        $this->addSql('COMMENT ON TABLE ts_dana_bidang IS \'Data riwayat transaksi Dana Bidang\'');
        $this->addSql('COMMENT ON COLUMN ts_dana_bidang.refcode IS \'Kode referensi transaksi\'');

        $this->addSql('CREATE TABLE ts_dana_global (tsid BIGSERIAL NOT NULL, posted_by INT, refcode VARCHAR(25) NOT NULL, ts_name VARCHAR(255) NOT NULL, tahun_awal INT, tahun_akhir INT, jumlah DOUBLE PRECISION NOT NULL, saldo DOUBLE PRECISION NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(tsid))');
        $this->addSql('CREATE INDEX ts_dana_global_x1 ON ts_dana_global (posted_by)');
        $this->addSql('COMMENT ON TABLE ts_dana_global IS \'Data riwayat transaksi Dana Global\'');
        $this->addSql('COMMENT ON COLUMN ts_dana_global.refcode IS \'Kode referensi transaksi\'');

        $this->addSql('CREATE TABLE ts_dana_program (tsid BIGSERIAL NOT NULL, posted_by INT, refcode VARCHAR(25) NOT NULL, ts_name VARCHAR(255) NOT NULL, tahun_awal INT, tahun_akhir INT, jumlah DOUBLE PRECISION NOT NULL, saldo DOUBLE PRECISION NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE, PRIMARY KEY(tsid))');
        $this->addSql('CREATE INDEX ts_dana_program_x1 ON ts_dana_program (posted_by)');
        $this->addSql('COMMENT ON TABLE ts_dana_program IS \'Data riwayat transaksi Dana Program\'');
        $this->addSql('COMMENT ON COLUMN ts_dana_program.refcode IS \'Kode referensi transaksi\'');

        $this->addSql('ALTER TABLE dana_batch ADD CONSTRAINT FK_5BCFB3A858AF9A2E FOREIGN KEY (dp_id) REFERENCES dana_program (dp_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_batch ADD CONSTRAINT FK_5BCFB3A8F39EBE7A FOREIGN KEY (batch_id) REFERENCES ms_batch_csr (batch_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_batch ADD CONSTRAINT FK_5BCFB3A860D6A0BF FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_batch ADD CONSTRAINT FK_5BCFB3A84EA3CB3D FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_batch ADD CONSTRAINT FK_5BCFB3A888F6E01 FOREIGN KEY (closed_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_batch ADD CONSTRAINT FK_5BCFB3A8AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_batch ADD CONSTRAINT FK_5BCFB3A816FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_bidang ADD CONSTRAINT FK_EC3590A9FE9AA8A1 FOREIGN KEY (bidang_id) REFERENCES jenis_program_csr (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_bidang ADD CONSTRAINT FK_EC3590A960D6A0BF FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_bidang ADD CONSTRAINT FK_EC3590A94EA3CB3D FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_bidang ADD CONSTRAINT FK_EC3590A988F6E01 FOREIGN KEY (closed_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_bidang ADD CONSTRAINT FK_EC3590A9AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_bidang ADD CONSTRAINT FK_EC3590A916FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_global ADD CONSTRAINT FK_B115727560D6A0BF FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_global ADD CONSTRAINT FK_B1157275AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_global ADD CONSTRAINT FK_B115727516FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_program ADD CONSTRAINT FK_C66A3054A2BF053A FOREIGN KEY (db_id) REFERENCES dana_bidang (db_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_program ADD CONSTRAINT FK_C66A3054405F5364 FOREIGN KEY (msid) REFERENCES ms_program_csr (msid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_program ADD CONSTRAINT FK_C66A305460D6A0BF FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_program ADD CONSTRAINT FK_C66A30544EA3CB3D FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_program ADD CONSTRAINT FK_C66A305488F6E01 FOREIGN KEY (closed_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_program ADD CONSTRAINT FK_C66A3054AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dana_program ADD CONSTRAINT FK_C66A305416FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE donatur_sumber_dana ADD CONSTRAINT FK_CBE4157FD7EF6F8E FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE donatur_sumber_dana ADD CONSTRAINT FK_CBE4157F21B741A9 FOREIGN KEY (ref_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_aprv_pendanaan ADD CONSTRAINT FK_CA2E7EC99561F508 FOREIGN KEY (dg_id) REFERENCES dana_global (dg_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_aprv_pendanaan ADD CONSTRAINT FK_CA2E7EC9A2BF053A FOREIGN KEY (db_id) REFERENCES dana_bidang (db_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_aprv_pendanaan ADD CONSTRAINT FK_CA2E7EC958AF9A2E FOREIGN KEY (dp_id) REFERENCES dana_program (dp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_aprv_pendanaan ADD CONSTRAINT FK_CA2E7EC9D7EF6F8E FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log_aprv_pendanaan ADD CONSTRAINT FK_CA2E7EC9AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_dana_batch ADD CONSTRAINT FK_75EEF1A8AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_dana_bidang ADD CONSTRAINT FK_EC1BB1EBAE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_dana_global ADD CONSTRAINT FK_B13B5337AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_dana_program ADD CONSTRAINT FK_5EB83EC9AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');


        $this->addSql('TRUNCATE institusi_in_program, individu_in_program');
        $this->addSql('ALTER TABLE institusi_in_program DROP CONSTRAINT FK_764F18CB3EB8070A');
        $this->addSql('ALTER TABLE institusi_in_program ALTER program_id TYPE BIGINT');
        $this->addSql('ALTER TABLE institusi_in_program ALTER program_id DROP DEFAULT');
        $this->addSql('ALTER TABLE institusi_in_program ADD CONSTRAINT FK_764F18CB3EB8070A FOREIGN KEY (program_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE individu_in_program DROP CONSTRAINT FK_D0EACA2A3EB8070A');
        $this->addSql('ALTER TABLE individu_in_program ALTER program_id TYPE BIGINT');
        $this->addSql('ALTER TABLE individu_in_program ALTER program_id DROP DEFAULT');
        $this->addSql('ALTER TABLE individu_in_program ADD CONSTRAINT FK_D0EACA2A3EB8070A FOREIGN KEY (program_id) REFERENCES dana_batch (bp_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('TRUNCATE item_anggaran, sub_kegiatan, kegiatan RESTART IDENTITY CASCADE');
        $this->addSql('ALTER TABLE kegiatan DROP CONSTRAINT fk_b6706c4b3eb8070a');
        $this->addSql('DROP INDEX kegiatan_x1');
        $this->addSql('DROP INDEX kegiatan_x3');
        $this->addSql('DROP INDEX kegiatan_x4');
        $this->addSql('DROP INDEX kegiatan_x5');
        $this->addSql('DROP INDEX kegiatan_x6');
        $this->addSql('ALTER TABLE kegiatan ADD submitted_by INT');
        $this->addSql('ALTER TABLE kegiatan ADD bp_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE kegiatan ADD verified_by INT');
        $this->addSql('ALTER TABLE kegiatan ADD approved_by INT');
        $this->addSql('ALTER TABLE kegiatan ADD posted_realisasi_by INT');
        $this->addSql('ALTER TABLE kegiatan ADD closed_by INT');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_verified TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_approved TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_posted_realisasi TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_closed TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE kegiatan DROP program_id');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_closing');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_update_realisasi');
        $this->addSql('ALTER TABLE kegiatan ALTER tgl_submitted TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE kegiatan ALTER tgl_submitted DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN kegiatan.submitted IS \'Sudah di submit ke Adaro atau belum. Penjelasan:
            1 = Submitted for Review
            2 = Submitted for Approval\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_step IS \'Tahapan approval yang sudah dilalui. Penjelasan:
            1 = Verified Tahap-1
            2 = Verified Tahap-2
            3 = Approved Tahap-1
            4 = Approved Tahap-2
            5 = Approved Tahap-3\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_status IS \'Valid values: REJECTED, VERIFIED, APPROVED\'');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4B641EE842 FOREIGN KEY (submitted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4BD7EF6F8E FOREIGN KEY (bp_id) REFERENCES dana_batch (bp_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4B60D6A0BF FOREIGN KEY (verified_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4B4EA3CB3D FOREIGN KEY (approved_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4B189B6986 FOREIGN KEY (posted_realisasi_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT FK_B6706C4B88F6E01 FOREIGN KEY (closed_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX kegiatan_x1 ON kegiatan (bp_id)');
        $this->addSql('CREATE INDEX kegiatan_x3 ON kegiatan (closed)');
        $this->addSql('CREATE INDEX kegiatan_x4 ON kegiatan (payment_method)');
        $this->addSql('CREATE INDEX kegiatan_x5 ON kegiatan (submitted_by)');
        $this->addSql('CREATE INDEX kegiatan_x6 ON kegiatan (posted_realisasi_by)');
        $this->addSql('CREATE INDEX kegiatan_x7 ON kegiatan (verified_by)');
        $this->addSql('CREATE INDEX kegiatan_x8 ON kegiatan (approved_by)');
        $this->addSql('CREATE INDEX kegiatan_x9 ON kegiatan (closed_by)');
        $this->addSql('CREATE INDEX kegiatan_x10 ON kegiatan (posted_by)');

        $this->addSql('DROP INDEX sub_kegiatan_x1');
        $this->addSql('DROP INDEX sub_kegiatan_x2');
        $this->addSql('ALTER TABLE sub_kegiatan ADD posted_realisasi_by INT');
        $this->addSql('ALTER TABLE sub_kegiatan ADD closed_by INT');
        $this->addSql('ALTER TABLE sub_kegiatan ADD tgl_posted_realisasi TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD tgl_closed TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE sub_kegiatan DROP tgl_update_realisasi');
        $this->addSql('ALTER TABLE sub_kegiatan DROP tgl_closing');
        $this->addSql('CREATE INDEX sub_kegiatan_x1 ON sub_kegiatan (kegiatan_id)');
        $this->addSql('CREATE INDEX sub_kegiatan_x2 ON sub_kegiatan (closed_by)');
        $this->addSql('CREATE INDEX sub_kegiatan_x3 ON sub_kegiatan (posted_realisasi_by)');
        $this->addSql('CREATE INDEX sub_kegiatan_x4 ON sub_kegiatan (posted_by)');
        $this->addSql('ALTER TABLE sub_kegiatan ADD CONSTRAINT FK_CDF8212189B6986 FOREIGN KEY (posted_realisasi_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD CONSTRAINT FK_CDF821288F6E01 FOREIGN KEY (closed_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD log_action VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD log_title VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE log_aprv_kegiatan DROP title');
        $this->addSql('ALTER TABLE log_aprv_kegiatan DROP status');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.log_action IS \'Valid values: REJECTED, VERIFIED, APPROVED\'');

        $this->addSql('COMMENT ON COLUMN menu_item.route_url IS \'VueJs routing url\'');
        $this->addSql('COMMENT ON COLUMN menu_item.url_type IS \'Valid values:
            0 = Route URL internal dan tampil pada MainMenu
            1 = Route URL eksternal dan tampil pada MainMenu
            2 = Route URL internal dan tidak tampil pada MainMenu
            3 = Menu parent, tampil pada MainMenu dan tanpa route URL
            4 = Menu separator, tanpa route URL\'');

        $this->addSql('ALTER TABLE user_account ADD mitra_id INT');
        $this->addSql('ALTER TABLE user_account ADD CONSTRAINT FK_253B48AE39D91309 FOREIGN KEY (mitra_id) REFERENCES ms_mitra (mitra_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX user_account_x3 ON user_account (email)');
        $this->addSql('CREATE INDEX user_account_x4 ON user_account (token_key)');
        $this->addSql('CREATE INDEX user_account_x5 ON user_account (mitra_id)');

        $this->addSql('UPDATE menu_item set route_url = \'/pendanaan/global\', route_alias = \'app_pendanaan_danaglobal\' WHERE menu_id = 3');
        $this->addSql('UPDATE menu_item set enabled = \'true\', route_alias = \'app_pendanaan_danabidang\' WHERE menu_id = 4');
        $this->addSql('UPDATE menu_item set enabled = \'true\', route_alias = \'app_pendanaan_danaprogram\' WHERE menu_id = 5');
        $this->addSql('UPDATE menu_item set enabled = \'true\', route_alias = \'app_pendanaan_danabatch\' WHERE menu_id = 6');
        $this->addSql('UPDATE menu_item set enabled = \'false\' WHERE menu_id = 7');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE institusi_in_program DROP CONSTRAINT fk_764f18cb3eb8070a');
        $this->addSql('ALTER TABLE individu_in_program DROP CONSTRAINT fk_d0eaca2a3eb8070a');
        $this->addSql('ALTER TABLE kegiatan DROP CONSTRAINT FK_B6706C4BD7EF6F8E');
        $this->addSql('ALTER TABLE donatur_sumber_dana DROP CONSTRAINT FK_CBE4157FD7EF6F8E');
        $this->addSql('ALTER TABLE dana_program DROP CONSTRAINT FK_C66A3054A2BF053A');
        $this->addSql('ALTER TABLE log_aprv_pendanaan DROP CONSTRAINT FK_CA2E7EC9D7EF6F8E');
        $this->addSql('ALTER TABLE log_aprv_pendanaan DROP CONSTRAINT FK_CA2E7EC9A2BF053A');
        $this->addSql('ALTER TABLE log_aprv_pendanaan DROP CONSTRAINT FK_CA2E7EC99561F508');
        $this->addSql('ALTER TABLE log_aprv_pendanaan DROP CONSTRAINT FK_CA2E7EC958AF9A2E');
        $this->addSql('ALTER TABLE dana_batch DROP CONSTRAINT FK_5BCFB3A858AF9A2E');

        $this->addSql('CREATE TABLE sumber_dana (ids SERIAL NOT NULL, program_id INT NOT NULL, ref_id INT NOT NULL, jumlah DOUBLE PRECISION NOT NULL, PRIMARY KEY(ids))');
        $this->addSql('CREATE INDEX sumber_dana_x1 ON sumber_dana (ref_id)');
        $this->addSql('CREATE INDEX sumber_dana_x2 ON sumber_dana (program_id)');
        $this->addSql('ALTER TABLE sumber_dana ADD CONSTRAINT fk_3a784bc121b741a9 FOREIGN KEY (ref_id) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sumber_dana ADD CONSTRAINT fk_3a784bc13eb8070a FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('DROP TABLE app_setting');
        $this->addSql('DROP TABLE ms_numbering');
        $this->addSql('DROP TABLE dana_batch');
        $this->addSql('DROP TABLE dana_bidang');
        $this->addSql('DROP TABLE dana_global');
        $this->addSql('DROP TABLE dana_program');
        $this->addSql('DROP TABLE donatur_sumber_dana');
        $this->addSql('DROP TABLE log_aprv_pendanaan');
        $this->addSql('DROP TABLE ts_dana_batch');
        $this->addSql('DROP TABLE ts_dana_bidang');
        $this->addSql('DROP TABLE ts_dana_global');
        $this->addSql('DROP TABLE ts_dana_program');

        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD title VARCHAR(250) NOT NULL');
        $this->addSql('ALTER TABLE log_aprv_kegiatan ADD status VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE log_aprv_kegiatan DROP log_action');
        $this->addSql('ALTER TABLE log_aprv_kegiatan DROP log_title');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.status IS \'Valid values: REJECTED, APPROVED\'');
        $this->addSql('COMMENT ON COLUMN log_aprv_kegiatan.approval_step IS \'Tahapan approval, keterangan:
                    1 = Verified Tahap-1
                    2 = Verified Tahap-2
                    3 = Approved Tahap-1
                    4 = Approved Tahap-2
                    5 = Approved Tahap-3\'');

        $this->addSql('ALTER TABLE kegiatan DROP CONSTRAINT FK_B6706C4B641EE842');
        $this->addSql('ALTER TABLE kegiatan DROP CONSTRAINT FK_B6706C4B60D6A0BF');
        $this->addSql('ALTER TABLE kegiatan DROP CONSTRAINT FK_B6706C4B4EA3CB3D');
        $this->addSql('ALTER TABLE kegiatan DROP CONSTRAINT FK_B6706C4B189B6986');
        $this->addSql('ALTER TABLE kegiatan DROP CONSTRAINT FK_B6706C4B88F6E01');
        $this->addSql('DROP INDEX kegiatan_x7');
        $this->addSql('DROP INDEX kegiatan_x8');
        $this->addSql('DROP INDEX kegiatan_x9');
        $this->addSql('DROP INDEX kegiatan_x10');
        $this->addSql('DROP INDEX kegiatan_x1');
        $this->addSql('DROP INDEX kegiatan_x3');
        $this->addSql('DROP INDEX kegiatan_x4');
        $this->addSql('DROP INDEX kegiatan_x5');
        $this->addSql('DROP INDEX kegiatan_x6');
        $this->addSql('ALTER TABLE kegiatan ADD program_id INT NOT NULL');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_closing DATE');
        $this->addSql('ALTER TABLE kegiatan ADD tgl_update_realisasi DATE');
        $this->addSql('ALTER TABLE kegiatan DROP submitted_by');
        $this->addSql('ALTER TABLE kegiatan DROP bp_id');
        $this->addSql('ALTER TABLE kegiatan DROP verified_by');
        $this->addSql('ALTER TABLE kegiatan DROP approved_by');
        $this->addSql('ALTER TABLE kegiatan DROP posted_realisasi_by');
        $this->addSql('ALTER TABLE kegiatan DROP closed_by');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_verified');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_approved');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_posted_realisasi');
        $this->addSql('ALTER TABLE kegiatan DROP tgl_closed');
        $this->addSql('ALTER TABLE kegiatan ALTER tgl_submitted TYPE DATE');
        $this->addSql('ALTER TABLE kegiatan ALTER tgl_submitted DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN kegiatan.submitted IS \'Sudah di submit ke Adaro atau belum. Keterangan:
                    1 = Submitted for Review
                    2 = Submitted for Approval\'');
        $this->addSql('COMMENT ON COLUMN kegiatan.approval_step IS \'Tahapan approval yang sudah dilalui. Keterangan:
                    1 = Verified Tahap-1
                    2 = Verified Tahap-2
                    3 = Approved Tahap-1
                    4 = Approved Tahap-2
                    5 = Approved Tahap-3\'');
        $this->addSql('ALTER TABLE kegiatan ADD CONSTRAINT fk_b6706c4b3eb8070a FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX kegiatan_x1 ON kegiatan (program_id)');
        $this->addSql('CREATE INDEX kegiatan_x3 ON kegiatan (submitted, approval_step)');
        $this->addSql('CREATE INDEX kegiatan_x4 ON kegiatan (closed)');
        $this->addSql('CREATE INDEX kegiatan_x5 ON kegiatan (payment_method)');
        $this->addSql('CREATE INDEX kegiatan_x6 ON kegiatan (posted_by)');
        $this->addSql('ALTER TABLE sub_kegiatan DROP CONSTRAINT FK_CDF8212189B6986');
        $this->addSql('ALTER TABLE sub_kegiatan DROP CONSTRAINT FK_CDF821288F6E01');
        $this->addSql('DROP INDEX sub_kegiatan_x3');
        $this->addSql('DROP INDEX sub_kegiatan_x4');
        $this->addSql('DROP INDEX sub_kegiatan_x2');
        $this->addSql('ALTER TABLE sub_kegiatan ADD tgl_update_realisasi DATE');
        $this->addSql('ALTER TABLE sub_kegiatan ADD tgl_closing DATE');
        $this->addSql('ALTER TABLE sub_kegiatan DROP posted_realisasi_by');
        $this->addSql('ALTER TABLE sub_kegiatan DROP closed_by');
        $this->addSql('ALTER TABLE sub_kegiatan DROP tgl_posted_realisasi');
        $this->addSql('ALTER TABLE sub_kegiatan DROP tgl_closed');
        $this->addSql('CREATE INDEX sub_kegiatan_x2 ON sub_kegiatan (posted_by)');

        $this->addSql('ALTER TABLE institusi_in_program ALTER program_id TYPE INT');
        $this->addSql('ALTER TABLE institusi_in_program ALTER program_id DROP DEFAULT');
        $this->addSql('ALTER TABLE institusi_in_program ADD CONSTRAINT fk_764f18cb3eb8070a FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE individu_in_program ALTER program_id TYPE INT');
        $this->addSql('ALTER TABLE individu_in_program ALTER program_id DROP DEFAULT');
        $this->addSql('ALTER TABLE individu_in_program ADD CONSTRAINT fk_d0eaca2a3eb8070a FOREIGN KEY (program_id) REFERENCES pendanaan_program (program_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE user_account DROP CONSTRAINT FK_253B48AE39D91309');
        $this->addSql('DROP INDEX user_account_x3');
        $this->addSql('DROP INDEX user_account_x4');
        $this->addSql('DROP INDEX user_account_x5');
        $this->addSql('ALTER TABLE user_account DROP mitra_id');

        $this->addSql('UPDATE menu_item set enabled = \'false\' WHERE menu_id = 4');
        $this->addSql('UPDATE menu_item set enabled = \'false\' WHERE menu_id = 5');
        $this->addSql('UPDATE menu_item set enabled = \'false\' WHERE menu_id = 6');
        $this->addSql('UPDATE menu_item set enabled = \'true\' WHERE menu_id = 7');
    }

}
