<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190127061844 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE capaian_kpi (idc BIGSERIAL NOT NULL, msid INT NOT NULL, batch_id INT NOT NULL, data_ref INT NOT NULL, tahun INT NOT NULL, dashboard BOOLEAN DEFAULT \'false\' NOT NULL, kuantitas BOOLEAN DEFAULT \'false\' NOT NULL, label VARCHAR(200) DEFAULT NULL, target DOUBLE PRECISION DEFAULT NULL, p1 DOUBLE PRECISION DEFAULT NULL, p2 DOUBLE PRECISION DEFAULT NULL, p3 DOUBLE PRECISION DEFAULT NULL, p4 DOUBLE PRECISION DEFAULT NULL, nilai DOUBLE PRECISION DEFAULT NULL, nilai_akhir DOUBLE PRECISION DEFAULT NULL, p1_verified BOOLEAN DEFAULT \'false\' NOT NULL, p2_verified BOOLEAN DEFAULT \'false\' NOT NULL, p3_verified BOOLEAN DEFAULT \'false\' NOT NULL, p4_verified BOOLEAN DEFAULT \'false\' NOT NULL, posted_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(idc))');
        $this->addSql('CREATE INDEX capaian_kpi_x1 ON capaian_kpi (msid)');
        $this->addSql('CREATE INDEX capaian_kpi_x2 ON capaian_kpi (batch_id)');
        $this->addSql('CREATE INDEX capaian_kpi_x3 ON capaian_kpi (data_ref)');
        $this->addSql('CREATE INDEX capaian_kpi_x4 ON capaian_kpi (tahun)');
        $this->addSql('CREATE INDEX capaian_kpi_x5 ON capaian_kpi (posted_by)');
        $this->addSql('CREATE INDEX capaian_kpi_x6 ON capaian_kpi (updated_by)');
        $this->addSql('COMMENT ON table capaian_kpi IS \'Table untuk data Target dan Capaian KPI\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.tahun IS \'Tahun anggaran\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.dashboard IS \'Tampil pada Dashboard atau tidak\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.kuantitas IS \'KPI kuantitatif atau kualitatif\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.label IS \'Dipergunakan untuk Dashboard ataupun Chart\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.target IS \'Target capaian KPI\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.p1 IS \'Nilai capaian periode-1 (Triwulan-1, Caturwulan-1, Semester-1)\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.p2 IS \'Nilai capaian periode-2 (Triwulan-2, Caturwulan-2, Semester-2)\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.p3 IS \'Nilai capaian periode-3 (Triwulan-3, Caturwulan-3)\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.p4 IS \'Nilai capaian periode-4 (Triwulan-4)\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.nilai IS \'Prosentase capaian KPI\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.p1_verified IS \'Nilai capaian periode-1 (Triwulan-1, Caturwulan-1, Semester-1) sudah diverifikasi?\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.p2_verified IS \'Nilai capaian periode-2 (Triwulan-2, Caturwulan-2, Semester-2) sudah diverifikasi?\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.p3_verified IS \'Nilai capaian periode-3 (Triwulan-3, Caturwulan-3) sudah diverifikasi?\'');
        $this->addSql('COMMENT ON COLUMN capaian_kpi.p4_verified IS \'Nilai capaian periode-4 (Triwulan-4) sudah diverifikasi?\'');
        $this->addSql('ALTER TABLE capaian_kpi ADD CONSTRAINT FK_474FCAB1405F5364 FOREIGN KEY (msid) REFERENCES ms_program_csr (msid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE capaian_kpi ADD CONSTRAINT FK_474FCAB1F39EBE7A FOREIGN KEY (batch_id) REFERENCES ms_batch_csr (batch_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE capaian_kpi ADD CONSTRAINT FK_474FCAB150E3DF16 FOREIGN KEY (data_ref) REFERENCES ms_dataref (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE capaian_kpi ADD CONSTRAINT FK_474FCAB1AE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE capaian_kpi ADD CONSTRAINT FK_474FCAB116FE72E1 FOREIGN KEY (updated_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.kategori IS \'Valid values:
            1 = PAUD
            2 = SMK
            3 = POLTEK
            4 = PESANTREN
            5 = LEMBAGA KEAGAMAAN\'');

        $this->addSql('INSERT INTO ms_dataref (protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'KPI-01\', \'Pembinaan PAUD\', \'Parameter target dan capaian KPI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'KPI-02\', \'Pendidikan Vokasi\', \'Parameter target dan capaian KPI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'KPI-03\', \'Pelatihan Vokasi\', \'Parameter target dan capaian KPI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'KPI-04\', \'Beasiswa\', \'Parameter target dan capaian KPI\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (protected, ref_code, ref_name, description, posted_date) 
        VALUES (true, \'KPI-05\', \'Tech-Based Education\', \'Parameter target dan capaian KPI\', NOW())');

        $jmlPaud = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'PAUD', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-01\'), true, \'KPI-01.001\', \'Jumlah PAUD\', 
        \'Parameter target dan capaian KPI Pembinaan PAUD\', \'' . json_encode($jmlPaud) . '\', NOW())');

        $jmlGuru = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'GURU', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-01\'), true, \'KPI-01.002\', \'Jumlah Guru\', 
        \'Parameter target dan capaian KPI Pembinaan PAUD\', \'' . json_encode($jmlGuru) . '\', NOW())');

        $jmlSiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'SISWA', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-01\'), true, \'KPI-01.003\', \'Jumlah Siswa\', 
        \'Parameter target dan capaian KPI Pembinaan PAUD\', \'' . json_encode($jmlSiswa) . '\', NOW())');

        $paud = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-01\'), true, \'KPI-01.004\', \'Level A (Baik)\', 
        \'Parameter target dan capaian KPI Pembinaan PAUD\', \'' . json_encode($paud) . '\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-01\'), true, \'KPI-01.005\', \'Level B (Cukup)\', 
        \'Parameter target dan capaian KPI Pembinaan PAUD\', \'' . json_encode($paud) . '\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-01\'), true, \'KPI-01.006\', \'Level C (Sedang)\', 
        \'Parameter target dan capaian KPI Pembinaan PAUD\', \'' . json_encode($paud) . '\', NOW())');

        $jmlSMK = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'SMK', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.001\', \'Jumlah SMK yang dibina\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($jmlSMK) . '\', NOW())');

        $jmlProgli = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'PROGLI', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.002\', 
        \'Jumlah Program Keahlian yang dijangkau Program\', \'Parameter target dan capaian KPI Pendidikan Vokasi\', 
        \'' . json_encode($jmlProgli) . '\', NOW())');

        $jmlManajemen = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'MANAJEMEN', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.003\', \'Jumlah Manajemen\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($jmlManajemen) . '\', NOW())');

        $jmlGuru = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'GURU', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.004\', \'Jumlah Guru\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($jmlGuru) . '\', NOW())');

        $jmlSiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'SMK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'SISWA', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.005\', \'Jumlah Siswa\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($jmlSiswa) . '\', NOW())');

        $jmlPoltek = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'POLTEK', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.006\', 
        \'Jumlah POLTEK yang dibina\', \'Parameter target dan capaian KPI Pendidikan Vokasi\', 
        \'' . json_encode($jmlPoltek) . '\', NOW())');

        $jmlProgli = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'PRODI', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.007\', 
        \'Jumlah Program Studi yang dijangkau Program\', \'Parameter target dan capaian KPI Pendidikan Vokasi\', 
        \'' . json_encode($jmlProgli) . '\', NOW())');

        $jmlManajemen = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'MANAJEMEN', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.008\', \'Jumlah Manajemen\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($jmlManajemen) . '\', NOW())');

        $jmlDosen = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'DOSEN', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.009\', \'Jumlah Dosen\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($jmlDosen) . '\', NOW())');

        $jmlMahasiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'institusi', 'value' => 'POLTEK', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'MAHASISWA', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.010\', \'Jumlah Mahasiswa\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($jmlMahasiswa) . '\', NOW())');

        $pendidikanVokasi1 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '5', 'type' => 'FLOAT']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.011\', 
        \'Jumlah Pimpinan Lembaga yang mendapat pelatihan School Management Skill Development\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($pendidikanVokasi1) . '\', NOW())');

        $pendidikanVokasi2 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '5', 'type' => 'FLOAT']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.012\', 
        \'Jumlah Guru/Dosen/Instruktur yang mendapat pelatihan Technical Skill Development\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($pendidikanVokasi2) . '\', NOW())');

        $pendidikanVokasi3 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '25', 'type' => 'FLOAT']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.013\', 
        \'Realisasi implementasi Program vs Planning\', \'Parameter target dan capaian KPI Pendidikan Vokasi\', 
        \'' . json_encode($pendidikanVokasi3) . '\', NOW())');

        $pendidikanVokasi4 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '40', 'type' => 'FLOAT'],
            ['property' => 'baseline', 'value' => '2.72', 'type' => 'FLOAT']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.014\', 
        \'Rerata Skor 7 x 7 matriks TEFA\', \'Parameter target dan capaian KPI Pendidikan Vokasi\', 
        \'' . json_encode($pendidikanVokasi4) . '\', NOW())');

        $pendidikanVokasi5 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '15', 'type' => 'FLOAT']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.015\', 
        \'Jumlah unit produksi yang telah dibentuk dan/atau dikembangkan\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($pendidikanVokasi5) . '\', NOW())');

        $pendidikanVokasi6 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '10', 'type' => 'FLOAT']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-02\'), true, \'KPI-02.016\', 
        \'Jurusan/Prodi memiliki income generating yang mampu menutup biaya operasional kegiatan praktikum\', 
        \'Parameter target dan capaian KPI Pendidikan Vokasi\', \'' . json_encode($pendidikanVokasi6) . '\', NOW())');

        $jmlPesantren = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'LEMBAGA', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.001\', 
        \'Jumlah Pesantren/Lembaga Keagamaan yang dibina\', \'Parameter target dan capaian KPI Pelatihan Vokasi\', 
        \'' . json_encode($jmlPesantren) . '\', NOW())');

        $jmlManajemen = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'MANAJEMEN', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.002\', \'Jumlah Manajemen\', 
        \'Parameter target dan capaian KPI Pelatihan Vokasi\', \'' . json_encode($jmlManajemen) . '\', NOW())');

        $jmlUstadz = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'USTADZ/GURU', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.003\', \'Jumlah Ustadz/Guru/Instruktur\', 
        \'Parameter target dan capaian KPI Pelatihan Vokasi\', \'' . json_encode($jmlUstadz) . '\', NOW())');

        $jmlSantri = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'SANTRI/SISWA', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.004\', \'Jumlah Santri/Siswa\', 
        \'Parameter target dan capaian KPI Pelatihan Vokasi\', \'' . json_encode($jmlSantri) . '\', NOW())');

        $jmlOmset = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.005\', 
        \'Omset kegiatan usaha Pesantren/Lembaga Keagamaan (Rp)\', 
        \'Parameter target dan capaian KPI Pelatihan Vokasi\', \'' . json_encode($jmlOmset) . '\', NOW())');

        $pelatihanVokasi1 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '20', 'type' => 'FLOAT']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.006\', 
        \'Jumlah unit usaha Pesantren/Lembaga Keagamaan yang mandiri\', 
        \'Parameter target dan capaian KPI Pelatihan Vokasi\', \'' . json_encode($pelatihanVokasi1) . '\', NOW())');

        $pelatihanVokasi2 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '30', 'type' => 'FLOAT']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.007\', 
        \'Implementasi program Planning\', \'Parameter target dan capaian KPI Pelatihan Vokasi\', 
        \'' . json_encode($pelatihanVokasi2) . '\', NOW())');

        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.008\', 
        \'Kewirausahaan menjadi kurikulum Pesantren/Lembaga Keagamaan\', 
        \'Parameter target dan capaian KPI Pelatihan Vokasi\', \'' . json_encode($pelatihanVokasi1) . '\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-03\'), true, \'KPI-03.009\', 
        \'Pesantren/Lembaga Keagamaan memiliki income generating dari unit usaha yang dikembangkan\', 
        \'Parameter target dan capaian KPI Pelatihan Vokasi\', \'' . json_encode($pelatihanVokasi2) . '\', NOW())');

        $jmlMahasiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'TOTAL', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-04\'), true, \'KPI-04.001\', \'Jumlah mahasiswa penerima beasiswa\', 
        \'Parameter target dan capaian KPI Beasiswa\', \'' . json_encode($jmlMahasiswa) . '\', NOW())');

        $jmlPria = [
            ['property' => 'targeted', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'LAKI-LAKI', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-04.001\'), true, \'KPI-04.002\', \'Laki-Laki\', 
        \'Parameter target dan capaian KPI Beasiswa\', \'' . json_encode($jmlPria) . '\', NOW())');

        $jmlWanita = [
            ['property' => 'targeted', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'PEREMPUAN', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-04.001\'), true, \'KPI-04.003\', \'Perempuan\', 
        \'Parameter target dan capaian KPI Beasiswa\', \'' . json_encode($jmlWanita) . '\', NOW())');

        $beasiswa = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-04\'), true, \'KPI-04.004\', \'Level A ( IPK >= 3,00 )\', 
        \'Parameter target dan capaian KPI Beasiswa\', \'' . json_encode($beasiswa) . '\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-04\'), true, \'KPI-04.005\', \'Level B ( IPK = 2,75 - 2,99 )\', 
        \'Parameter target dan capaian KPI Beasiswa\', \'' . json_encode($beasiswa) . '\', NOW())');
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-04\'), true, \'KPI-04.006\', \'Level C ( IPK < 2,75)\', 
        \'Parameter target dan capaian KPI Beasiswa\', \'' . json_encode($beasiswa) . '\', NOW())');

        $jmlGuruSD = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'GURU SD', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-05\'), true, \'KPI-05.001\', \'Jumlah Guru SD\', 
        \'Parameter target dan capaian KPI Tech-Based Education\', \'' . json_encode($jmlGuruSD) . '\', NOW())');

        $jmlGuruSMP = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUANTITAS', 'type' => 'STRING'],
            ['property' => 'Chart Label', 'value' => 'GURU SMP', 'type' => 'STRING']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-05\'), true, \'KPI-05.002\', \'Jumlah Guru SMP\', 
        \'Parameter target dan capaian KPI Tech-Based Education\', \'' . json_encode($jmlGuruSMP) . '\', NOW())');

        $techbased1 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '3', 'type' => 'INTEGER']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-05\'), true, \'KPI-05.003\', \'Kualifikasi A\', 
        \'Parameter target dan capaian KPI Tech-Based Education\', \'' . json_encode($techbased1) . '\', NOW())');

        $techbased2 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '2', 'type' => 'INTEGER']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-05\'), true, \'KPI-05.004\', \'Kualifikasi B\', 
        \'Parameter target dan capaian KPI Tech-Based Education\', \'' . json_encode($techbased2) . '\', NOW())');

        $techbased3 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '1', 'type' => 'INTEGER']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-05\'), true, \'KPI-05.005\', \'Kualifikasi C\', 
        \'Parameter target dan capaian KPI Tech-Based Education\', \'' . json_encode($techbased3) . '\', NOW())');

        $techbased4 = [
            ['property' => 'targeted', 'value' => 'TRUE', 'type' => 'BOOLEAN'],
            ['property' => 'dashboard', 'value' => 'FALSE', 'type' => 'BOOLEAN'],
            ['property' => 'grouping', 'value' => 'KUALITAS', 'type' => 'STRING'],
            ['property' => 'bobot', 'value' => '0', 'type' => 'INTEGER']
        ];
        $this->addSql('INSERT INTO ms_dataref (parent_id, protected, ref_code, ref_name, description, ref_value, posted_date) 
        VALUES ((SELECT id FROM ms_dataref WHERE ref_code = \'KPI-05\'), true, \'KPI-05.006\', \'Kualifikasi D\', 
        \'Parameter target dan capaian KPI Tech-Based Education\', \'' . json_encode($techbased4) . '\', NOW())');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE capaian_kpi');

        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-01.001\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-01.002\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-01.003\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-01.004\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-01.005\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-01.006\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-01\'');

        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.001\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.002\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.003\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.004\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.005\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.006\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.007\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.008\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.009\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.010\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.011\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.012\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.013\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.014\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.015\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02.016\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-02\'');

        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.001\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.002\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.003\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.004\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.005\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.006\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.007\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.008\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03.009\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-03\'');

        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-04.002\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-04.003\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-04.001\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-04.004\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-04.005\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-04.006\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-04\'');

        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-05.001\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-05.002\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-05.003\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-05.004\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-05.005\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-05.006\'');
        $this->addSql('DELETE FROM ms_dataref WHERE ref_code = \'KPI-05\'');

    }

}
