<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200806031021 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE finalisasi (finalisasi_id BIGSERIAL NOT NULL, kegiatan_id BIGINT DEFAULT NULL, mitra_id INT NOT NULL, judul VARCHAR(250) NOT NULL, latarbelakang TEXT DEFAULT NULL, tujuanprogram TEXT DEFAULT NULL, outcome TEXT DEFAULT NULL, ruanglingkup TEXT DEFAULT NULL, pemangkukepentingan TEXT DEFAULT NULL, status VARCHAR(20) DEFAULT \'NEW\' NOT NULL, PRIMARY KEY(finalisasi_id))');
        $this->addSql('CREATE INDEX finalisasi_x1 ON finalisasi (kegiatan_id)');
        $this->addSql('CREATE INDEX finalisasi_x2 ON finalisasi (mitra_id)');
        $this->addSql('COMMENT ON COLUMN finalisasi.status IS \'Valid values: NEW\'');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D783C3F230 FOREIGN KEY (kegiatan_id) REFERENCES kegiatan (kegiatan_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finalisasi ADD CONSTRAINT FK_CCE459D739D91309 FOREIGN KEY (mitra_id) REFERENCES ms_mitra (mitra_id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE finalisasi');
        $this->addSql('COMMENT ON COLUMN item_realisasi.status IS NULL');
    }
}
