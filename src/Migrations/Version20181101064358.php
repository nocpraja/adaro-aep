<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181101064358 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE ms_batch_csr (batch_id SERIAL NOT NULL, msid INT DEFAULT NULL, mitra_id INT DEFAULT NULL, nama_batch VARCHAR(100) NOT NULL, tahun_awal INT NOT NULL, tahun_akhir INT NOT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(batch_id))');
        $this->addSql('CREATE INDEX ms_batch_x1 ON ms_batch_csr (msid)');
        $this->addSql('CREATE INDEX ms_batch_x2 ON ms_batch_csr (mitra_id)');
        $this->addSql('CREATE TABLE ms_mitra (mitra_id SERIAL NOT NULL, msid INT DEFAULT NULL, kec_id INT NOT NULL, kel_id INT DEFAULT NULL, jenis_lembaga VARCHAR(25) NOT NULL, nama_lembaga VARCHAR(250) NOT NULL, akte_pendirian VARCHAR(50) NOT NULL, nama_pimpinan VARCHAR(250) DEFAULT NULL, pic_program VARCHAR(250) DEFAULT NULL, pimpinan_phone VARCHAR(25) DEFAULT NULL, pic_phone VARCHAR(25) DEFAULT NULL, alamat VARCHAR(255) NOT NULL, rt_rw VARCHAR(11) DEFAULT NULL, kode_pos VARCHAR(15) DEFAULT NULL, office_phone VARCHAR(25) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, website VARCHAR(200) DEFAULT NULL, posted_by VARCHAR(50) DEFAULT NULL, posted_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, geom geometry(POINT, 4326) DEFAULT NULL, search_fts TSVECTOR DEFAULT NULL, PRIMARY KEY(mitra_id))');
        $this->addSql('CREATE INDEX ms_mitra_x1 ON ms_mitra (kec_id)');
        $this->addSql('CREATE INDEX ms_mitra_x2 ON ms_mitra (kel_id)');
        $this->addSql('CREATE INDEX ms_mitra_x3 ON ms_mitra (msid)');
        $this->addSql('CREATE INDEX ms_mitra_x4 ON ms_mitra USING gin (search_fts)');
        $this->addSql('CREATE TABLE ts_pendanaan_global (tsid SERIAL NOT NULL, posted_by INT DEFAULT NULL, ts_name VARCHAR(255) NOT NULL, jumlah DOUBLE PRECISION NOT NULL, saldo DOUBLE PRECISION NOT NULL, tahun_awal INT DEFAULT NULL, tahun_akhir INT DEFAULT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(tsid))');
        $this->addSql('CREATE INDEX pendanaan_global_x1 ON ts_pendanaan_global (posted_by)');
        $this->addSql('CREATE INDEX pendanaan_global_x2 ON ts_pendanaan_global (last_updated)');
        $this->addSql('ALTER TABLE ms_batch_csr ADD CONSTRAINT FK_D0C0AE51405F5364 FOREIGN KEY (msid) REFERENCES ms_program_csr (msid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ms_batch_csr ADD CONSTRAINT FK_D0C0AE5139D91309 FOREIGN KEY (mitra_id) REFERENCES ms_mitra (mitra_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ms_mitra ADD CONSTRAINT FK_5D5C9BD4405F5364 FOREIGN KEY (msid) REFERENCES ms_program_csr (msid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ms_mitra ADD CONSTRAINT FK_5D5C9BD432B0E2C8 FOREIGN KEY (kec_id) REFERENCES kecamatan (kec_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ms_mitra ADD CONSTRAINT FK_5D5C9BD46AD3F29E FOREIGN KEY (kel_id) REFERENCES kelurahan (kel_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ts_pendanaan_global ADD CONSTRAINT FK_4E55A7DAAE36D154 FOREIGN KEY (posted_by) REFERENCES user_account (uid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('COMMENT ON TABLE ms_mitra IS \'Data profile Mitra kerjasama\'');
        $this->addSql('COMMENT ON TABLE ms_batch_csr IS \'Master data Batch untuk masing-masing Program CSR\'');
        $this->addSql('COMMENT ON TABLE ts_pendanaan_global IS \'Data transaksi Pendanaan Global\'');
        $this->addSql('COMMENT ON COLUMN ms_mitra.jenis_lembaga IS \'Valid values: NEGERI, SWASTA, YAYASAN, PERKUMPULAN\'');
        $this->addSql('COMMENT ON COLUMN ms_mitra.search_fts IS \'(DC2Type:tsvector)\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.kategori IS \'Valid values:
            1 = SANTRI
            2 = SISWA
            3 = MAHASISWA
            4 = USTADZ
            5 = GURU
            6 = DOSEN
            7 = MANAJEMEN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.jenis_kelamin IS \'Valid values:
            L = Laki-Laki, P = Perempuan\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_jabatan IS \'Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen.
            Valid values: PNS, DTT, DTY, GTT, GTY\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_individu.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.kategori IS \'Valid values:
            1 = PAUD
            2 = SMK
            3 = POLTEK
            4 = PESANTREN
            5 = NON-PESANTREN/YAYASAN\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_verifikasi IS \'Status verifikasi data, valid values:
            0 = Baru diisi dan belum diverifikasi,
            1 = Data sudah diverifikasi\'');
        $this->addSql('COMMENT ON COLUMN beneficiary_institusi.status_pembinaan IS \'Status pembinaan penerima manfaat, valid values:
            0 = Baru diikutsertakan
            1 = Pemantauan atau monitoring
            2 = Selesai pembinaan\'');
        $this->addSql('INSERT INTO ms_mitra (nama_lembaga, jenis_lembaga, akte_pendirian, email, website, nama_pimpinan, pic_program,
                      office_phone, pimpinan_phone, pic_phone, alamat, kec_id, kel_id, rt_rw, kode_pos, msid,
                      posted_by, posted_date) VALUES (\'Indonesia Heritage Foundation (Yayasan Warisan Nilau Luhur Bangsa)\', 
                      \'YAYASAN\', \'10/2001\', \'kesekretariatan.ihf@gmail.com\', \'www.ihf.or.id\', \'Ratna Megawangi\', 
                      \'Rahma Dona\', \'021-8712022\', NULL, \'+62 813-1522-3281\', \'Jalan Raya Bogor KM 31\', 2556, 31364, 
                      \'002/001\', \'16451\', 1, \'admin-aep\', NOW())');
        $this->addSql('INSERT INTO ms_mitra (nama_lembaga, jenis_lembaga, akte_pendirian, email, website, nama_pimpinan, pic_program,
                      office_phone, pimpinan_phone, pic_phone, alamat, kec_id, kel_id, rt_rw, kode_pos, msid,
                      posted_by, posted_date) VALUES (\'Yayasan Karya Akademi Teknik Mesin Industri\', \'YAYASAN\', 
                      \'14/2003\', NULL, NULL, \'Benedictus Bambang Triatmoko, SY\', \'Thomas Ambar Prihastomo\', 
                      \'+62-21-89106780 ext. 444\', NULL, \'+62 821-3564-1607\', \'Jalan Kampus Hijau No. 3 Kawasan Jababeka Education Park, Cikarang Baru\',
                      2454, 30654, NULL, \'17520\', 2, \'admin-aep\', NOW())');
        $this->addSql('INSERT INTO ms_mitra (nama_lembaga, jenis_lembaga, akte_pendirian, email, website, nama_pimpinan, pic_program,
                      office_phone, pimpinan_phone, pic_phone, alamat, kec_id, kel_id, rt_rw, kode_pos, msid,
                      posted_by, posted_date) VALUES (\'PT. Bina Swadaya Konsultan\', \'SWASTA\', \'15/2003\', NULL, 
                      \'www.binaswadayakonsultan.com\', \'CH. Frida Widuratmi\', \'Ika Sari\', \'021-4204402\', NULL, 
                      \'+62 811 1005 095\', \'Jl. Gunung Sahari III No. 7\', 1917, 25320, NULL, \'10610\', 3, \'admin-aep\', NOW())');
        $this->addSql('INSERT INTO ms_mitra (nama_lembaga, jenis_lembaga, akte_pendirian, email, website, nama_pimpinan, pic_program,
                      office_phone, pimpinan_phone, pic_phone, alamat, kec_id, kel_id, rt_rw, kode_pos, msid,
                      posted_by, posted_date) VALUES (\'Universitas Lambung Mangkurat\', \'NEGERI\', \'134/1960\', 
                      \'humas@ulm.ac.id\', \'www.ulm.ac.id\', \'Prof. Dr. Sutarto Hadi, M.Si, M.Sc\', 
                      \'Prof. Dr. Yudi Firmanul Arifin, M.Sc\', \'0511-3306603\', \'+62 822-5506-3129\', 
                      \'+62 815-2104-088\', \'Jalan Brigjen H. Hasan Basri No. 3\', 4987, 61364, \'000/002\', 
                      \'70124\', 4, \'admin-aep\', NOW())');
        $this->addSql('INSERT INTO ms_mitra (nama_lembaga, jenis_lembaga, akte_pendirian, email, website, nama_pimpinan, pic_program,
                      office_phone, pimpinan_phone, pic_phone, alamat, kec_id, kel_id, rt_rw, kode_pos, msid,
                      posted_by, posted_date) VALUES (\'Perkumpulan Pengajar Bahasa Berbasis Teknologi Informasi (iTELL)\', 
                      \'PERKUMPULAN\', \'17/2018\', \'indonesia.tell@gmail.com\', \'http://itell.or.id/conference/index.php/itell/itell2018\',
                      \'Dr. Anunsius Gumawang Jati, MA\', \'Finita Dewi\', \'0298-323672 Ext. 217\', \'+62 817-6446-600\', 
                      \'+62 859-7492-1440\', \'Jl. Kartini No. 15-17\', 3128, 39835, \'001/001\', NULL, 5, \'admin-aep\', NOW())');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql',
                       'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE ms_batch_csr DROP CONSTRAINT FK_D0C0AE5139D91309');
        $this->addSql('DROP TABLE ms_batch_csr');
        $this->addSql('DROP TABLE ms_mitra');
        $this->addSql('DROP TABLE ts_pendanaan_global');
    }
}
