<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180802200427 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE app_role (id INT NOT NULL, role VARCHAR(50) NOT NULL, role_title VARCHAR(250) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX ak_x1_app_role ON app_role (role)');
        $this->addSql('ALTER TABLE user_group ADD roles JSONB DEFAULT NULL');
        $this->addSql('INSERT INTO app_role values (1, \'ROLE_USER\', \'ROLE USER\')');
        $this->addSql('INSERT INTO app_role values (2, \'ROLE_VERIFICATOR\', \'ROLE VERIFICATOR\')');
        $this->addSql('INSERT INTO app_role values (3, \'ROLE_APPROVAL\', \'ROLE APPROVAL\')');
        $this->addSql('INSERT INTO app_role values (4, \'ROLE_ADMIN\', \'ROLE ADMIN\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE app_role');
        $this->addSql('ALTER TABLE user_group DROP roles');
    }
}
