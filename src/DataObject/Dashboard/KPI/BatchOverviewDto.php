<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */


namespace App\DataObject\Dashboard\KPI;


use App\Entity\MasterData\BatchCsr;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ProgramOverviewDto
 *
 * @package App\DataObject\Dashboard\KPI
 * @author  Mark Melvin
 * @since   10/25/2019, modified: 10/25/2019 11:38
 */
final class BatchOverviewDto
{
    /**
     * @var BatchCsr $batch
     * @Groups({"with_rel"})
     */
    private $batch;

    /**
     * @var KeyValueDto[]|null $capaianKpiKuantitas
     * @Groups({"with_rel"})
     */
    private $capaianKpiKuantitas;

    /**
     * @var KeyValueDto[]|null $capaianKpiKualitas
     * @Groups({"with_rel"})
     */
    private $capaianKpiKualitas;

    /**
     * @var int $tahun
     * @Groups({"with_rel", "without,rel"})
     *
     */
    private $tahun;

    /**
     * @var int $periode
     * @Groups({"with_rel", "without,rel"})
     *
     */
    private $periode;

    /**
     * @var string $narasi
     * @Groups({"with_rel", "without,rel"})
     *
     */
    private $narasi;

    /**
     * BatchOverviewDto constructor.
     * @param BatchCsr $batch
     */
    public function __construct(BatchCsr $batch)
    {
        $this->batch = $batch;
    }


    /**
     * @return BatchCsr
     */
    public function getBatch(): BatchCsr
    {
        return $this->batch;
    }

    /**
     * @param BatchCsr $batch
     */
    public function setBatch(BatchCsr $batch): void
    {
        $this->batch = $batch;
    }

    /**
     * @return KeyValueDto[]
     */
    public function getCapaianKpiKualitas(): ?array
    {
        return $this->capaianKpiKualitas;
    }

    /**
     * @param KeyValueDto[] $capaianKpiKualitas
     */
    public function setCapaianKpiKualitas(array $capaianKpiKualitas): void
    {
        $this->capaianKpiKualitas = $capaianKpiKualitas;
    }

    /**
     * @return KeyValueDto[]
     */
    public function getCapaianKpiKuantitas(): ?array
    {
        return $this->capaianKpiKuantitas;
    }

    /**
     * @param KeyValueDto[] $capaianKpiKuantitas
     */
    public function setCapaianKpiKuantitas(array $capaianKpiKuantitas): void
    {
        $this->capaianKpiKuantitas = $capaianKpiKuantitas;
    }

    /**
     * @return int
     */
    public function getTahun(): int
    {
        return $this->tahun;
    }

    /**
     * @param int $tahun
     */
    public function setTahun(int $tahun): void
    {
        $this->tahun = $tahun;
    }

    /**
     * @return int
     */
    public function getPeriode(): int
    {
        return $this->periode;
    }

    /**
     * @param int $periode
     */
    public function setPeriode(int $periode): void
    {
        $this->periode = $periode;
    }

    /**
     * @return string|null
     */
    public function getNarasi(): ?string
    {
        return $this->narasi;
    }

    /**
     * @param string|null $narasi
     */
    public function setNarasi(?string $narasi): void
    {
        $this->narasi = $narasi;
    }

}