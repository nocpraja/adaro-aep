<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */


namespace App\DataObject\Dashboard\KPI;


use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class KeyValueDto
 *
 * @package App\DataObject\Dashboard\KPI
 * @author  Mark Melvin
 * @since   10/25/2019, modified: 10/25/2019 11:38
 *
 *
 */
final class KeyValueDto
{

    /**
     * @var string $label
     * @Groups({"with_rel", "without_rel"})
     */
    private $label;

    /**
     * @var float $value
     * @Groups({"with_rel", "without_rel"})
     */
    private $value;

    /**
     * @var string|null $flag
     * @Groups({"with_rel", "without_rel"})
     *
     */
    private $flag;

    /**
     * KeyValueDto constructor.
     * @param string $label
     * @param float $value
     */
    public function __construct(string $label, float $value)
    {
        $this->label = $label;
        $this->value = $value;
    }


    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value): void
    {
        $this->value = $value;
    }

    /**
     * @return string|null
     */
    public function getFlag(): ?string
    {
        return $this->flag;
    }

    /**
     * @param string|null $flag
     */
    public function setFlag(?string $flag): void
    {
        $this->flag = $flag;
    }


}