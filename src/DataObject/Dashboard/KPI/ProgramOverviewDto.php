<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */


namespace App\DataObject\Dashboard\KPI;


use App\Entity\MasterData\ProgramCSR;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ProgramOverviewDto
 *
 * @package App\DataObject\Dashboard\KPI
 * @author  Mark Melvin
 * @since   10/25/2019, modified: 10/25/2019 11:38
 */
final class ProgramOverviewDto
{
    /**
     * @var ProgramCSR $program
     * @Groups({"with_rel"})
     */
    private $program;

    /**
     * @var string $executiveSummary
     * @Groups({"with_rel", "without_rel"})
     *
     */
    private $executiveSummary;

    /**
     * @var BatchOverviewDto[]|null $overviewBatches
     * @Groups({"with_rel"})
     */
    private $overviewBatches;

    /**
     * @var array $colorData
     * @Groups({"with_rel", "without_rel"})
     */
    private $colorData;

    /**
     * ProgramOverviewDto constructor.
     * @param ProgramCSR $program
     */
    public function __construct(ProgramCSR $program)
    {
        $this->program = $program;
    }

    /**
     * @return ProgramCSR
     */
    public function getProgram(): ProgramCSR
    {
        return $this->program;
    }

    /**
     * @param ProgramCSR $program
     */
    public function setProgram(ProgramCSR $program): void
    {
        $this->program = $program;
    }

    /**
     * @return string|null
     */
    public function getExecutiveSummary(): ?string
    {
        return $this->executiveSummary;
    }

    /**
     * @param string|null $executiveSummary
     */
    public function setExecutiveSummary(?string $executiveSummary): void
    {
        $this->executiveSummary = $executiveSummary;
    }

    /**
     * @return BatchOverviewDto[]|null
     */
    public function getOverviewBatches(): ?array
    {
        return $this->overviewBatches;
    }

    /**
     * @param BatchOverviewDto[]|null $overviewBatches
     */
    public function setOverviewBatches(?array $overviewBatches): void
    {
        $this->overviewBatches = $overviewBatches;
    }

    /**
     * @return array
     */
    public function getColorData(): array
    {
        return $this->colorData;
    }

    /**
     * @param array $colorData
     */
    public function setColorData(array $colorData): void
    {
        $this->colorData = $colorData;
    }



}