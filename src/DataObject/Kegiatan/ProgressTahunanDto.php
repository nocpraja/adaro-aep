<?php


namespace App\DataObject\Kegiatan;
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\DataObject\Kegiatan;

use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\KendaliKegiatan;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * Class ProgressTahunanDto
 *
 * @package App\DataObject\Kegiatan
 * @author  Mark Melvin
 * @since   10/25/2019, modified: 10/25/2019 11:38
 */
class ProgressTahunanDto
{
    /**
     * @var string $judul
     * @Groups({"with_rel", "without_rel"})
     */
    private $judul;
    /**
     * @var string $level
     * @Groups({"with_rel", "without_rel"})
     */
    private $level;
    /**
     * @var int $tahun
     * @Groups({"with_rel", "without_rel"})
     */
    private $tahun;
    /**
     * @var int $bulanAwal
     * @Groups({"with_rel", "without_rel"})
     */
    private $bulanAwal;
    /**
     * @var int $bulanAkhir
     * @Groups({"with_rel", "without_rel"})
     */
    private $bulanAkhir;

    /**
     * @var float|null $bobot
     * @Groups({"with_rel", "without_rel"})
     */
    private $bobot;

    /**
     * @var float|null $rencana1
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana1;

    /**
     * @var float|null $rencana2
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana2;

    /**
     * @var float|null $rencana3
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana3;

    /**
     * @var float|null $rencana4
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana4;

    /**
     * @var float|null $rencana5
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana5;

    /**
     * @var float|null $rencana6
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana6;

    /**
     * @var float|null $rencana7
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana7;

    /**
     * @var float|null $rencana8
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana8;

    /**
     * @var float|null $rencana9
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana9;

    /**
     * @var float|null $rencana10
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana10;

    /**
     * @var float|null $rencana11
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana11;

    /**
     * @var float|null $rencana12
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana12;


    /**
     * @var float|null $realisasi1
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi1;

    /**
     * @var float|null $realisasi2
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi2;

    /**
     * @var float|null $realisasi3
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi3;

    /**
     * @var float|null $realisasi4
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi4;

    /**
     * @var float|null $realisasi5
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi5;

    /**
     * @var float|null $realisasi6
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi6;

    /**
     * @var float|null $realisasi7
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi7;

    /**
     * @var float|null $realisasi8
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi8;

    /**
     * @var float|null $realisasi9
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi9;

    /**
     * @var float|null $realisasi10
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi10;

    /**
     * @var float|null $realisasi11
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi11;

    /**
     * @var float|null $realisasi12
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi12;

    /**
     * ProgressTahunanDto constructor.
     * @param string $judul
     * @param string $level
     * @param int $tahun
     * @param int $bulanAwal
     * @param int $bulanAkhir
     */
    public function __construct(string $judul, string $level,
                                int $tahun, int $bulanAwal=1, int $bulanAkhir=12)
    {
        $this->judul = $judul;
        $this->level = $level;
        $this->tahun = $tahun;
        $this->bulanAwal = $bulanAwal;
        $this->bulanAkhir = $bulanAkhir;
    }

    /**
     * @return string
     */
    public function getJudul(): string
    {
        return $this->judul;
    }

    /**
     * @param string $judul
     */
    public function setJudul(string $judul): void
    {
        $this->judul = $judul;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel(string $level): void
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getTahun(): int
    {
        return $this->tahun;
    }

    /**
     * @param int $tahun
     */
    public function setTahun(int $tahun): void
    {
        $this->tahun = $tahun;
    }

    /**
     * @return int
     */
    public function getBulanAwal(): int
    {
        return $this->bulanAwal;
    }

    /**
     * @param int $bulanAwal
     */
    public function setBulanAwal(int $bulanAwal): void
    {
        $this->bulanAwal = $bulanAwal;
    }

    /**
     * @return int
     */
    public function getBulanAkhir(): int
    {
        return $this->bulanAkhir;
    }

    /**
     * @param int $bulanAkhir
     */
    public function setBulanAkhir(int $bulanAkhir): void
    {
        $this->bulanAkhir = $bulanAkhir;
    }

    /**
     * @return float|null
     */
    public function getBobot(): ?float
    {
        return $this->bobot;
    }

    /**
     * @param float|null $bobot
     */
    public function setBobot(?float $bobot): void
    {
        $this->bobot = $bobot;
    }

    /**
     * @return float|null
     */
    public function getRencana1(): ?float
    {
        return $this->rencana1;
    }

    /**
     * @param float|null $rencana1
     */
    public function setRencana1(?float $rencana1): void
    {
        $this->rencana1 = $rencana1;
    }

    /**
     * @return float|null
     */
    public function getRencana2(): ?float
    {
        return $this->rencana2;
    }

    /**
     * @param float|null $rencana2
     */
    public function setRencana2(?float $rencana2): void
    {
        $this->rencana2 = $rencana2;
    }

    /**
     * @return float|null
     */
    public function getRencana3(): ?float
    {
        return $this->rencana3;
    }

    /**
     * @param float|null $rencana3
     */
    public function setRencana3(?float $rencana3): void
    {
        $this->rencana3 = $rencana3;
    }

    /**
     * @return float|null
     */
    public function getRencana4(): ?float
    {
        return $this->rencana4;
    }

    /**
     * @param float|null $rencana4
     */
    public function setRencana4(?float $rencana4): void
    {
        $this->rencana4 = $rencana4;
    }

    /**
     * @return float|null
     */
    public function getRencana5(): ?float
    {
        return $this->rencana5;
    }

    /**
     * @param float|null $rencana5
     */
    public function setRencana5(?float $rencana5): void
    {
        $this->rencana5 = $rencana5;
    }

    /**
     * @return float|null
     */
    public function getRencana6(): ?float
    {
        return $this->rencana6;
    }

    /**
     * @param float|null $rencana6
     */
    public function setRencana6(?float $rencana6): void
    {
        $this->rencana6 = $rencana6;
    }

    /**
     * @return float|null
     */
    public function getRencana7(): ?float
    {
        return $this->rencana7;
    }

    /**
     * @param float|null $rencana7
     */
    public function setRencana7(?float $rencana7): void
    {
        $this->rencana7 = $rencana7;
    }

    /**
     * @return float|null
     */
    public function getRencana8(): ?float
    {
        return $this->rencana8;
    }

    /**
     * @param float|null $rencana8
     */
    public function setRencana8(?float $rencana8): void
    {
        $this->rencana8 = $rencana8;
    }

    /**
     * @return float|null
     */
    public function getRencana9(): ?float
    {
        return $this->rencana9;
    }

    /**
     * @param float|null $rencana9
     */
    public function setRencana9(?float $rencana9): void
    {
        $this->rencana9 = $rencana9;
    }

    /**
     * @return float|null
     */
    public function getRencana10(): ?float
    {
        return $this->rencana10;
    }

    /**
     * @param float|null $rencana10
     */
    public function setRencana10(?float $rencana10): void
    {
        $this->rencana10 = $rencana10;
    }

    /**
     * @return float|null
     */
    public function getRencana11(): ?float
    {
        return $this->rencana11;
    }

    /**
     * @param float|null $rencana11
     */
    public function setRencana11(?float $rencana11): void
    {
        $this->rencana11 = $rencana11;
    }

    /**
     * @return float|null
     */
    public function getRencana12(): ?float
    {
        return $this->rencana12;
    }

    /**
     * @param float|null $rencana12
     */
    public function setRencana12(?float $rencana12): void
    {
        $this->rencana12 = $rencana12;
    }

    /**
     * @return float|null
     */
    public function getRealisasi1(): ?float
    {
        return $this->realisasi1;
    }

    /**
     * @param float|null $realisasi1
     */
    public function setRealisasi1(?float $realisasi1): void
    {
        $this->realisasi1 = $realisasi1;
    }

    /**
     * @return float|null
     */
    public function getRealisasi2(): ?float
    {
        return $this->realisasi2;
    }

    /**
     * @param float|null $realisasi2
     */
    public function setRealisasi2(?float $realisasi2): void
    {
        $this->realisasi2 = $realisasi2;
    }

    /**
     * @return float|null
     */
    public function getRealisasi3(): ?float
    {
        return $this->realisasi3;
    }

    /**
     * @param float|null $realisasi3
     */
    public function setRealisasi3(?float $realisasi3): void
    {
        $this->realisasi3 = $realisasi3;
    }

    /**
     * @return float|null
     */
    public function getRealisasi4(): ?float
    {
        return $this->realisasi4;
    }

    /**
     * @param float|null $realisasi4
     */
    public function setRealisasi4(?float $realisasi4): void
    {
        $this->realisasi4 = $realisasi4;
    }

    /**
     * @return float|null
     */
    public function getRealisasi5(): ?float
    {
        return $this->realisasi5;
    }

    /**
     * @param float|null $realisasi5
     */
    public function setRealisasi5(?float $realisasi5): void
    {
        $this->realisasi5 = $realisasi5;
    }

    /**
     * @return float|null
     */
    public function getRealisasi6(): ?float
    {
        return $this->realisasi6;
    }

    /**
     * @param float|null $realisasi6
     */
    public function setRealisasi6(?float $realisasi6): void
    {
        $this->realisasi6 = $realisasi6;
    }

    /**
     * @return float|null
     */
    public function getRealisasi7(): ?float
    {
        return $this->realisasi7;
    }

    /**
     * @param float|null $realisasi7
     */
    public function setRealisasi7(?float $realisasi7): void
    {
        $this->realisasi7 = $realisasi7;
    }

    /**
     * @return float|null
     */
    public function getRealisasi8(): ?float
    {
        return $this->realisasi8;
    }

    /**
     * @param float|null $realisasi8
     */
    public function setRealisasi8(?float $realisasi8): void
    {
        $this->realisasi8 = $realisasi8;
    }

    /**
     * @return float|null
     */
    public function getRealisasi9(): ?float
    {
        return $this->realisasi9;
    }

    /**
     * @param float|null $realisasi9
     */
    public function setRealisasi9(?float $realisasi9): void
    {
        $this->realisasi9 = $realisasi9;
    }

    /**
     * @return float|null
     */
    public function getRealisasi10(): ?float
    {
        return $this->realisasi10;
    }

    /**
     * @param float|null $realisasi10
     */
    public function setRealisasi10(?float $realisasi10): void
    {
        $this->realisasi10 = $realisasi10;
    }

    /**
     * @return float|null
     */
    public function getRealisasi11(): ?float
    {
        return $this->realisasi11;
    }

    /**
     * @param float|null $realisasi11
     */
    public function setRealisasi11(?float $realisasi11): void
    {
        $this->realisasi11 = $realisasi11;
    }

    /**
     * @return float|null
     */
    public function getRealisasi12(): ?float
    {
        return $this->realisasi12;
    }

    /**
     * @param float|null $realisasi12
     */
    public function setRealisasi12(?float $realisasi12): void
    {
        $this->realisasi12 = $realisasi12;
    }

    /**
     * Fungsi inisialisasi dari kegiatan
     * @param Kegiatan $kegiatan
     * @param int $bulanAkhir
     * @param int $bulanAwal
     */
    public function copyFromKegiatan(Kegiatan $kegiatan,
                                     int $bulanAkhir=12,
                                     int $bulanAwal=1) {
        $this->judul = $kegiatan->getNamaKegiatan();
        $kendaliKegiatan = $kegiatan->getKendaliKegiatan();
        if (!is_null($kendaliKegiatan)) {
            if ($bulanAkhir >= $bulanAwal) {
                $this->bulanAwal = $bulanAwal;
                $this->bulanAkhir = $bulanAkhir;
                $acumulatedRencana = 0;
                $acumulatedRealisasi = 0;
                for ($i=1; $i<=12; $i++){
                    $fieldRencana = "rencana".$i;
                    $methodGetRencana = "getRencana".$i;
                    $acumulatedRencana += $kendaliKegiatan->$methodGetRencana();
                    $this->$fieldRencana = $acumulatedRencana;
                    $fieldRealisasi = "realisasi".$i;
                    $methodGetRealisasi = "getRealisasi".$i;
                    $acumulatedRealisasi += $kendaliKegiatan->$methodGetRealisasi();
                    $this->$fieldRealisasi = $acumulatedRealisasi;
                }
                $this->bobot = $kendaliKegiatan->getBobot();
            }
        }
    }

    /**
     * Fungsi menjumlahkan dua angka dengan memperhatikan null
     *
     * @param float|null $current Jumlah sekarang
     * @param float|null $added   Angka ditambahkan
     * @return float|null         Hasil penjumlahan
     **/
    private function addNumberCoalescing(?float $current, ?float $added) :?float {
        if (is_null($current)) {
            return $added;
        }
        else {
            if (is_null($added)) {
                return $current;
            }
            else {
                return $current + $added;
            }
        }

    }

    public function addSumFromChildProgress(ProgressTahunanDto $child){
//        $this->bobot = $this->addNumberCoalescing($this->bobot, $child->getBobot());
        $this->rencana1 = $this->addNumberCoalescing($this->rencana1, $child->getRencana1());
        $this->rencana2 = $this->addNumberCoalescing($this->rencana2, $child->getRencana2());
        $this->rencana3 = $this->addNumberCoalescing($this->rencana3, $child->getRencana3());
        $this->rencana4 = $this->addNumberCoalescing($this->rencana4, $child->getRencana4());
        $this->rencana5 = $this->addNumberCoalescing($this->rencana5, $child->getRencana5());
        $this->rencana6 = $this->addNumberCoalescing($this->rencana6, $child->getRencana6());
        $this->rencana7 = $this->addNumberCoalescing($this->rencana7, $child->getRencana7());
        $this->rencana8 = $this->addNumberCoalescing($this->rencana8, $child->getRencana8());
        $this->rencana9 = $this->addNumberCoalescing($this->rencana9, $child->getRencana9());
        $this->rencana10 = $this->addNumberCoalescing($this->rencana10, $child->getRencana10());
        $this->rencana11 = $this->addNumberCoalescing($this->rencana11, $child->getRencana11());
        $this->rencana12 = $this->addNumberCoalescing($this->rencana12, $child->getRencana12());
        $this->realisasi1 = $this->addNumberCoalescing($this->realisasi1, $child->getRealisasi1());
        $this->realisasi2 = $this->addNumberCoalescing($this->realisasi2, $child->getRealisasi2());
        $this->realisasi3 = $this->addNumberCoalescing($this->realisasi3, $child->getRealisasi3());
        $this->realisasi4 = $this->addNumberCoalescing($this->realisasi4, $child->getRealisasi4());
        $this->realisasi5 = $this->addNumberCoalescing($this->realisasi5, $child->getRealisasi5());
        $this->realisasi6 = $this->addNumberCoalescing($this->realisasi6, $child->getRealisasi6());
        $this->realisasi7 = $this->addNumberCoalescing($this->realisasi7, $child->getRealisasi7());
        $this->realisasi8 = $this->addNumberCoalescing($this->realisasi8, $child->getRealisasi8());
        $this->realisasi9 = $this->addNumberCoalescing($this->realisasi9, $child->getRealisasi9());
        $this->realisasi10 = $this->addNumberCoalescing($this->realisasi10, $child->getRealisasi10());
        $this->realisasi11 = $this->addNumberCoalescing($this->realisasi11, $child->getRealisasi11());
        $this->realisasi12 = $this->addNumberCoalescing($this->realisasi12, $child->getRealisasi12());
    }

    public function getRealisasiFinal() : float
    {
        $value = 0.0;
        for ($i=1; $i<=12; $i++){
            $fieldRealisasi = "realisasi".$i;
            if ($this->$fieldRealisasi !== null){
                $value = $this->$fieldRealisasi;
            }
        }
        return $value;
    }

}