<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\DataObject\Kegiatan;

use App\Entity\Kegiatan\ItemAnggaran;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * Class ProgressTahunanDto
 *
 * @package App\DataObject\Kegiatan
 * @author  Mark Melvin
 * @since   01/10/2020, modified: 01/10/2020 16:38
 */
class SerapanTahunanDto
{
    /**
     * @var string $judul
     * @Groups({"with_rel", "without_rel"})
     */
    private $judul;
    /**
     * @var string $level
     * @Groups({"with_rel", "without_rel"})
     */
    private $level;
    /**
     * @var int $tahun
     * @Groups({"with_rel", "without_rel"})
     */
    private $tahun;
    /**
     * @var int $bulanAwal
     * @Groups({"with_rel", "without_rel"})
     */
    private $bulanAwal;
    /**
     * @var int $bulanAkhir
     * @Groups({"with_rel", "without_rel"})
     */
    private $bulanAkhir;

    /**
     * @var string $units
     * @Groups({"with_rel", "without_rel"})
     */
    private $units;
    /**
     * @var float|null $serapan1
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan1;

    /**
     * @var float|null $serapan2
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan2;

    /**
     * @var float|null $serapan3
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan3;

    /**
     * @var float|null $serapan4
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan4;

    /**
     * @var float|null $serapan5
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan5;

    /**
     * @var float|null $serapan6
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan6;

    /**
     * @var float|null $serapan7
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan7;

    /**
     * @var float|null $serapan8
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan8;

    /**
     * @var float|null $serapan9
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan9;

    /**
     * @var float|null $serapan10
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan10;

    /**
     * @var float|null $serapan11
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan11;

    /**
     * @var float|null $serapan12
     * @Groups({"without_rel", "with_rel"})
     */
    private $serapan12;

    /**
     * @var float|null $budget
     * @Groups({"without_rel", "with_rel"})
     */
    private $budget;

    /**
     * @var float|null $totalSerapan
     * @Groups({"without_rel", "with_rel"})
     */
    private $totalSerapan;

    /**
     * @var float|null $variance
     * @Groups({"without_rel", "with_rel"})
     */
    private $variance;

    /**
     * @var string|null $flag
     * @Groups({"without_rel", "with_rel"})
     */
    private $flag;

    /**
     * @return string
     */
    public function getJudul(): string
    {
        return $this->judul;
    }

    /**
     * @param string $judul
     */
    public function setJudul(string $judul): void
    {
        $this->judul = $judul;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel(string $level): void
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getTahun(): int
    {
        return $this->tahun;
    }

    /**
     * @param int $tahun
     */
    public function setTahun(int $tahun): void
    {
        $this->tahun = $tahun;
    }

    /**
     * @return int
     */
    public function getBulanAwal(): int
    {
        return $this->bulanAwal;
    }

    /**
     * @param int $bulanAwal
     */
    public function setBulanAwal(int $bulanAwal): void
    {
        $this->bulanAwal = $bulanAwal;
    }

    /**
     * @return int
     */
    public function getBulanAkhir(): int
    {
        return $this->bulanAkhir;
    }

    /**
     * @param int $bulanAkhir
     */
    public function setBulanAkhir(int $bulanAkhir): void
    {
        $this->bulanAkhir = $bulanAkhir;
    }

    /**
     * @return string
     */
    public function getUnits(): string
    {
        return $this->units;
    }

    /**
     * @param string $units
     */
    public function setUnits(string $units): void
    {
        $this->units = $units;
    }

    /**
     * @return float|null
     */
    public function getSerapan1(): ?float
    {
        return $this->serapan1;
    }

    /**
     * @param float|null $serapan1
     */
    public function setSerapan1(?float $serapan1): void
    {
        $this->serapan1 = $serapan1;
    }

    /**
     * @return float|null
     */
    public function getSerapan2(): ?float
    {
        return $this->serapan2;
    }

    /**
     * @param float|null $serapan2
     */
    public function setSerapan2(?float $serapan2): void
    {
        $this->serapan2 = $serapan2;
    }

    /**
     * @return float|null
     */
    public function getSerapan3(): ?float
    {
        return $this->serapan3;
    }

    /**
     * @param float|null $serapan3
     */
    public function setSerapan3(?float $serapan3): void
    {
        $this->serapan3 = $serapan3;
    }

    /**
     * @return float|null
     */
    public function getSerapan4(): ?float
    {
        return $this->serapan4;
    }

    /**
     * @param float|null $serapan4
     */
    public function setSerapan4(?float $serapan4): void
    {
        $this->serapan4 = $serapan4;
    }

    /**
     * @return float|null
     */
    public function getSerapan5(): ?float
    {
        return $this->serapan5;
    }

    /**
     * @param float|null $serapan5
     */
    public function setSerapan5(?float $serapan5): void
    {
        $this->serapan5 = $serapan5;
    }

    /**
     * @return float|null
     */
    public function getSerapan6(): ?float
    {
        return $this->serapan6;
    }

    /**
     * @param float|null $serapan6
     */
    public function setSerapan6(?float $serapan6): void
    {
        $this->serapan6 = $serapan6;
    }

    /**
     * @return float|null
     */
    public function getSerapan7(): ?float
    {
        return $this->serapan7;
    }

    /**
     * @param float|null $serapan7
     */
    public function setSerapan7(?float $serapan7): void
    {
        $this->serapan7 = $serapan7;
    }

    /**
     * @return float|null
     */
    public function getSerapan8(): ?float
    {
        return $this->serapan8;
    }

    /**
     * @param float|null $serapan8
     */
    public function setSerapan8(?float $serapan8): void
    {
        $this->serapan8 = $serapan8;
    }

    /**
     * @return float|null
     */
    public function getSerapan9(): ?float
    {
        return $this->serapan9;
    }

    /**
     * @param float|null $serapan9
     */
    public function setSerapan9(?float $serapan9): void
    {
        $this->serapan9 = $serapan9;
    }

    /**
     * @return float|null
     */
    public function getSerapan10(): ?float
    {
        return $this->serapan10;
    }

    /**
     * @param float|null $serapan10
     */
    public function setSerapan10(?float $serapan10): void
    {
        $this->serapan10 = $serapan10;
    }

    /**
     * @return float|null
     */
    public function getSerapan11(): ?float
    {
        return $this->serapan11;
    }

    /**
     * @param float|null $serapan11
     */
    public function setSerapan11(?float $serapan11): void
    {
        $this->serapan11 = $serapan11;
    }

    /**
     * @return float|null
     */
    public function getSerapan12(): ?float
    {
        return $this->serapan12;
    }

    /**
     * @param float|null $serapan12
     */
    public function setSerapan12(?float $serapan12): void
    {
        $this->serapan12 = $serapan12;
    }

    /**
     * @return float|null
     */
    public function getBudget(): ?float
    {
        return $this->budget;
    }

    /**
     * @param float|null $budget
     */
    public function setBudget(?float $budget): void
    {
        $this->budget = $budget;
    }

    /**
     * SerapanTahunanDto constructor.
     * @param string $judul
     * @param string $level
     * @param int $tahun
     * @param int $bulanAwal
     * @param int $bulanAkhir
     * @param string $units
     */
    public function __construct(string $judul, string $level,
                                int $tahun, int $bulanAwal=1, int $bulanAkhir=12, string $units='Rp. ')
    {
        $this->judul = $judul;
        $this->level = $level;
        $this->tahun = $tahun;
        $this->bulanAwal = $bulanAwal;
        $this->bulanAkhir = $bulanAkhir;
        $this->units = $units;
    }

    /**
     * Fungsi inisialisasi dari itemAnggaran
     * @param ItemAnggaran $itemAnggaran
     * @param $tahun
     * @param int $bulanAkhir
     * @param int $bulanAwal
     */
    public function copyFromItemAnggaran(ItemAnggaran $itemAnggaran,
                                     int $tahun,
                                     int $bulanAkhir=12,
                                     int $bulanAwal=1) {
        $this->judul = $itemAnggaran->getTitle();
        $this->budget = 0;
        $realisasiRabs = $itemAnggaran->getRealisasiRabs();
        if (!is_null($realisasiRabs)) {
            if ($bulanAkhir >= $bulanAwal) {
                $this->bulanAwal = $bulanAwal;
                $this->bulanAkhir = $bulanAkhir;
                foreach ($realisasiRabs as $itemRealisasiRab ){
                    if ($itemRealisasiRab->isVerified()){
                        $month = $itemRealisasiRab->getRealisasiDate()->format('m');
                        $year = $itemRealisasiRab->getRealisasiDate()->format('Y');
                        if ($year == $tahun) {
                            // set realisasi to month
                            if (($month >= $bulanAwal) && ($month <= $bulanAkhir)) {
                                $serapanMonth = 'serapan' . (int)$month;
                                $nilaiSerapan = $itemRealisasiRab->getRealisasi();
                                $this->$serapanMonth = $this->addNumberCoalescing($this->$serapanMonth,
                                    $nilaiSerapan);
                            }
                        }
                    }
                }
            }
        }
        $this->budget = $this->addNumberCoalescing($this->budget, $itemAnggaran->getAnggaran());
    }

    /**
     * Fungsi menjumlahkan dua angka dengan memperhatikan null
     *
     * @param float|null $current Jumlah sekarang
     * @param float|null $added   Angka ditambahkan
     * @return float|null         Hasil penjumlahan
     **/
    private function addNumberCoalescing(?float $current, ?float $added) :?float {
        if (is_null($current)) {
            return $added;
        }
        else {
            if (is_null($added)) {
                return $current;
            }
            else {
                return $current + $added;
            }
        }

    }

    public function addSumFromChildBudget(SerapanTahunanDto $child){
        $this->serapan1 = $this->addNumberCoalescing($this->serapan1, $child->getSerapan1());
        $this->serapan2 = $this->addNumberCoalescing($this->serapan2, $child->getSerapan2());
        $this->serapan3 = $this->addNumberCoalescing($this->serapan3, $child->getSerapan3());
        $this->serapan4 = $this->addNumberCoalescing($this->serapan4, $child->getSerapan4());
        $this->serapan5 = $this->addNumberCoalescing($this->serapan5, $child->getSerapan5());
        $this->serapan6 = $this->addNumberCoalescing($this->serapan6, $child->getSerapan6());
        $this->serapan7 = $this->addNumberCoalescing($this->serapan7, $child->getSerapan7());
        $this->serapan8 = $this->addNumberCoalescing($this->serapan8, $child->getSerapan8());
        $this->serapan9 = $this->addNumberCoalescing($this->serapan9, $child->getSerapan9());
        $this->serapan10 = $this->addNumberCoalescing($this->serapan10, $child->getSerapan10());
        $this->serapan11 = $this->addNumberCoalescing($this->serapan11, $child->getSerapan11());
        $this->serapan12 = $this->addNumberCoalescing($this->serapan12, $child->getSerapan12());
        $this->budget = $this->addNumberCoalescing($this->budget, $child->getBudget());
    }

    /**
     * @return float|null
     */
    public function getTotalSerapan(): ?float
    {
        $total = null;
        for ($month = 1; $month<=12; $month++) {
            $serapanMonth = 'serapan'.(int)$month;
            $total = $this->addNumberCoalescing($total, $this->$serapanMonth);
        }
        $this->totalSerapan = $total;
        return $this->totalSerapan;
    }

    /**
     * @return float|null
     */
    public function getVariance(): ?float
    {
        if (is_null($this->budget) ||($this->budget == 0)) {
            return null;
        }
        $this->variance = (100 * $this->getTotalSerapan()) / $this->budget;
        return $this->variance;
    }

    /**
     * @return string|null
     */
    public function getFlag(): ?string
    {
        return $this->flag;
    }

    /**
     * @param string|null $flag
     */
    public function setFlag(?string $flag): void
    {
        $this->flag = $flag;
    }

}