<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Dashboard;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\DataObject\Dashboard\KPI\BatchOverviewDto;
use App\DataObject\Dashboard\KPI\KeyValueDto;
use App\DataObject\Dashboard\KPI\ProgramOverviewDto;
use App\Entity\Kegiatan\CapaianKpi;
use App\Entity\MasterData\BatchCsr;
use App\Entity\MasterData\ProgramCSR;
use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\Security\UserAccount;
use App\Models\Kegiatan\KpiModel;
use App\Repository\Kegiatan\CapaianKpiRepository;
use App\Repository\MasterData\BatchCsrRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use App\Service\Security\AppUserInterface;

/**
 * Class DashboardModel
 *
 * @package App\Models\Dashboard
 * @author  Mark Melvin
 * @since   28/01/2019, modified: 24/07/2019 12:08
 */


class DashboardModel
{
    /**
     * @var ProgramCSRRepository $programRepository
     */
    private $programRepository;

    /**
     * @var BatchCsrRepository $batchRepository
     */
    private $batchRepository;
    /**
     * @var CapaianKpiRepository $capaianKpiRepository
     */
    private $capaianKpiRepository;

    /**
     * @var KpiModel $kpiModel
     */
    private $kpiModel;

    /**
     * DashboardModel constructor.
     *
     * @param KpiModel $kpiModel
     * @param ProgramCSRRepository $programRepository
     * @param BatchCsrRepository $batchRepository
     * @param CapaianKpiRepository $capaianKpiRepository
     */
    public function __construct(ProgramCSRRepository $programRepository,
                                BatchCsrRepository $batchRepository,
                                CapaianKpiRepository $capaianKpiRepository,
                                KpiModel $kpiModel)
    {
        $this->programRepository = $programRepository;
        $this->batchRepository = $batchRepository;
        $this->kpiModel = $kpiModel;
        $this->capaianKpiRepository = $capaianKpiRepository;
    }


    /**
     * Menampilkan array Overview Program
     * @param JenisProgramCSR|null $bidang optional filter menurut bidang
     * @return array
     */
    function overviewPrograms(?JenisProgramCSR $bidang = null) {
        $result = [];
        $filters = [];
        if ($bidang !== null) {
            $filters = [
                new SortOrFilter('jenis', null, $bidang)
            ];
        }

        $programs = $this->programRepository->findAllByCriteria($filters)->getResult();

        /** @var ProgramCSR $program */
        foreach ($programs as $program) {
                $programOvr = new ProgramOverviewDto($program);
                $programOvr->setOverviewBatches($this->overviewBatches($program));
                $programOvr->setColorData($this->chooseColorData($program));
                foreach ($programOvr->getOverviewBatches() as $batchOverviewDto) {
                    $programOvr->setExecutiveSummary($batchOverviewDto->getNarasi());
                }
                $msid = $program->getMsid();

                $result[$msid] = $programOvr;
        }

        return $result;
    }


    /**
     * Mencari nilai parameter KPI.
     *
     * @param array|null $params Input parameter
     * @param String     $key    The key to search
     *
     * @return String The parameter value
     */
    private function findElement(?array $params, string $key): ?string
    {

        if (is_array($params)) {
            $value = null;

            foreach ($params as $p) {
                if (is_array($p)) {
                    if ($p["property"] == $key) {
                        $value = $p["value"];
                    }
                }
            }

            return $value;
        }

        return null;
    }

    /**
     * Menampilkan array Overview Batch
     * @param ProgramCSR|null $program filter menurut program
     * @return array
     */
    function overviewBatches(?ProgramCSR $program)
    {
        $ProgramBatchesOvr = [];
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'programCsr', 'value' => $program]
            ]
        );
        $batches = $this->batchRepository->findAllByCriteria($filters)->getResult();

        /** @var BatchCsr $batch */
        foreach ($batches as $batch) {
            $batchOverview = new BatchOverviewDto($batch);
            $batchOverview->setBatch($batch);

            $batchId = $batch->getBatchId();
            $filters = QueryExpressionHelper::createCriterias(
                [
                    ['property' => 'periodeBatch.batchId', 'value' => $batchId]
                ]
            );
            $sorts = [
                new SortOrFilter('periodeBatch.tahunAwal', 'desc'),
                new SortOrFilter('periodeBatch.batchId', 'desc'),
                new SortOrFilter('tahun', 'desc')
            ];

            /** @var CapaianKpi[] $capaianKpis */
            $capaianKpis = $this->capaianKpiRepository->findAllByCriteria($filters, $sorts)
                ->getResult();
            $kuantitas = [];
            $badge = [];

            // Array capaian KPI yang diproses untuk data non-badge
            $capaianKpiKuantitas = array_filter($capaianKpis, function ($val) {
                /** @var CapaianKpi $val */
                return ($val->getShowInDashboard()==true) &&
                    ($val->getShowInDashboard()==true);
            });

            $latestVerified = $this->kpiModel->latestVerifiedOfProgram($capaianKpiKuantitas);

            // Array capaian KPI yang diproses untuk data non-badge
            $capaianKpiKuantitasLastVerified = array_filter($capaianKpiKuantitas, function ($val) use ($latestVerified) {
                /** @var CapaianKpi $val */
                return ($val->getTahun() == $latestVerified["tahun"]);
            });


            foreach ($capaianKpiKuantitasLastVerified as $capaianLast) {
                if ($capaianLast->getLabel()) {
                    $entry = new KeyValueDto(
                        $capaianLast->getLabel(),
                        $this->kpiModel->getValueByP($capaianLast, $latestVerified['periode']));
                    if ($program->getMsid()==2) {
                        $params = $capaianLast->getDataRef()->getParams();
                        $institusi = $this->findElement($params, "institusi");
                        if ($institusi) {
                            $entry->setFlag($institusi);
                        }
                    }

                    $kuantitas[] = $entry;
                }
            }
//            /* untuk pengecekan, nilai periode mana yang ditampilkan */
//            $kuantitas[] = new KeyValueDto("tahun", $latestVerified['tahun']);
//            $kuantitas[] = new KeyValueDto("periode", $latestVerified['periode']);


            // Array capaian KPI yang diproses untuk data badge
            $capaianKpiBadge = array_filter($capaianKpis, function ($val) {
                /** @var CapaianKpi $val */
                return !$val->getShowInDashboard();
            });

            $latestVerified = $this->kpiModel->latestVerifiedOfProgram($capaianKpis);

            // Array capaian KPI yang diproses untuk data non-badge
            $capaianKpiBadgeLastVerified = array_filter($capaianKpiBadge, function ($val) use ($latestVerified) {
                /** @var CapaianKpi $val */
                return ($val->getTahun() == $latestVerified["tahun"]);
            });

            $value = $this->calculateBadgeValue($capaianKpiBadgeLastVerified, $program->getMsid(), $latestVerified);
            $badge[] = new KeyValueDto("value", $value);

//            /* untuk pengecekan, nilai periode mana yang ditampilkan */
//            $badge[] = new KeyValueDto("tahun", $latestVerified['tahun']);
//            $badge[] = new KeyValueDto("periode", $latestVerified['periode']);

            $narasi = '';
            foreach ($batch->getTahuns() as $tahun) {
                if ($tahun){
                    if (!empty($tahun->getNarasiKpi())) {
                        $narasi = $tahun->getNarasiKpi();
                    }
                }
            }

            $batchOverview->setTahun($latestVerified['tahun']);
            $batchOverview->setPeriode($latestVerified['periode']);
            $batchOverview->setCapaianKpiKuantitas($kuantitas);
            $batchOverview->setCapaianKpiKualitas($badge);
            $batchOverview->setNarasi($narasi);

            $batchId = $batch->getBatchId();
            $ProgramBatchesOvr[$batchId] = $batchOverview;
        }
        return $ProgramBatchesOvr;
    }


    function calculateBadgeValue(array $capaian, $msid, $latestVerified) : float {
        switch ($msid) {
            case 1 :
                /** @var CapaianKpi $val */
                foreach ($capaian as $val) {
                    if ($val->getDataRef()->getCode() == 'KPI-01.004') {
                        return $this->kpiModel->chooseValueFromLatestP($val);
                    }
                }
                break;
            case 4 :
                /** @var CapaianKpi $val */
                foreach ($capaian as $val) {
                    if ($val->getDataRef()->getCode() == 'KPI-04.004') {
                        return $this->kpiModel->chooseValueFromLatestP($val);
                    }
                }
                break;
            case 5:
                $total = 0.0;
                $sumValue = 0.0;
                /** @var CapaianKpi $val */
                foreach ($capaian as $val) {
                    $value = $this->kpiModel->chooseValueFromLatestP($val);
                    $calcvalue = $this->kpiModel->calculateVerifiedNilaiAkhir($val, $latestVerified, 1);
                    $total += $calcvalue;
                    $sumValue += $value;
                }
                if ($sumValue) {
                    return $total / $sumValue;
                }
                else {
                    return 0;
                }
                break;
            default :
                $total = 0.0;
                /** @var CapaianKpi $val */
                foreach ($capaian as $val) {
                    $value = $this->kpiModel->calculateVerifiedNilaiAkhir($val, $latestVerified, $val->getTarget());
                    $total += $value;
                }
                return $total;
        }
        return 0.0;

    }

    /**
     * dummy prosedur untuk mensuplai nilai batas TLA untuk
     * diagram indikator kualitas
     *
     * @param ProgramCSR $program
     * @return array
     */
    private function chooseColorData(ProgramCSR $program):array {
        $colorData = [];
        $msid = $program->getMsid();
        switch ($msid) {
            case 1: //PAUD
                $colorData =  [
                    [0, 10, 25, 100],
                    [0, 25, 50, 100],
                    [0, 50, 75, 100],
                ];
                break;
            case 2: //Pendidikan vokasi
                $colorData =  [
                    [0, 50, 75, 100],
                    [0, 50, 75, 100],
                    [0, 50, 75, 100],
                    [0, 50, 75, 100],
                ];
                break;
            case 3: //Pelatihan
                $colorData = [
                    [0, 50, 75, 100],
                    [0, 50, 75, 100],
                    [0, 50, 75, 100],
                ];
                break;
            case 4:// beasiswa
                $colorData = [
                    [0, 25, 50, 100],
                    [0, 20, 70, 100],
                    [0, 10, 80, 100],
                    [0, 5, 90, 100]
                ];
                break;
            case 5: // tech based
                $colorData = [
                    [0, 150, 225, 300],
                    [0, 180, 255, 300],
                    [0, 210, 285, 300],
                ];
                break;
        }


        return $colorData;
    }
}