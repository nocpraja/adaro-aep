<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */


namespace App\Models\Kegiatan;

use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\DataObject\Kegiatan\ProgressTahunanDto;
use App\DataObject\Kegiatan\SerapanTahunanDto;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\MasterData\ProgramCSR;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\Kegiatan\KendaliKegiatanRepository;
use App\Repository\Kegiatan\PembobotanBidangRepository;
use App\Repository\Kegiatan\PembobotanProgramRepository;
use App\Repository\MasterData\BatchCsrRepository;
use App\Repository\MasterData\JenisProgramCSRRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ChartKegiatanModel
 *
 * @package App\Models\Kegiatan
 * @author  Mark Melvin
 * @since   17/7/2019
 */
class ChartKegiatanModel
{
    /**
     * @var JenisProgramCSRRepository
     */
    private $jenisProgramCsrRepository;

    /**
     * @var ProgramCSRRepository
     */
    private $programCsrRepository;

    /**
     * @var BatchCsrRepository
     */
    private $batchCsrRepository;

    /**
     * @var KendaliKegiatanRepository
     */
    private $kendaliKegiatanrepository;

    /**
     * @var KegiatanRepository
     */
    private $kegiatanRepository;

    /**
     * @var PembobotanBidangRepository
     */
    private $pembobotanBidangRepository;

    /**
     * @var PembobotanProgramRepository
     */
    private $pembobotanProgramRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MonitoringModel constructor.
     * @param JenisProgramCSRRepository $jenisProgramCsrRepository
     * @param ProgramCSRRepository $programCsrRepository
     * @param BatchCsrRepository $batchCsrRepository
     * @param KendaliKegiatanRepository $kendaliKegiatanrepository
     * @param KegiatanRepository $kegiatanRepository
     * @param PembobotanBidangRepository $pembobotanBidangRepository
     * @param PembobotanProgramRepository $pembobotanProgramRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(JenisProgramCSRRepository $jenisProgramCsrRepository,
                                ProgramCSRRepository $programCsrRepository,
                                BatchCsrRepository $batchCsrRepository,
                                KendaliKegiatanRepository $kendaliKegiatanrepository,
                                KegiatanRepository $kegiatanRepository,
                                PembobotanBidangRepository $pembobotanBidangRepository,
                                PembobotanProgramRepository $pembobotanProgramRepository,
                                EntityManagerInterface $em)
    {
        $this->jenisProgramCsrRepository = $jenisProgramCsrRepository;
        $this->programCsrRepository = $programCsrRepository;
        $this->batchCsrRepository = $batchCsrRepository;
        $this->kendaliKegiatanrepository = $kendaliKegiatanrepository;
        $this->kegiatanRepository = $kegiatanRepository;
        $this->pembobotanBidangRepository = $pembobotanBidangRepository;
        $this->pembobotanProgramRepository = $pembobotanProgramRepository;
        $this->em = $em;
    }

    /**
     * @return JenisProgramCSRRepository
     */
    public function getJenisProgramCsrRepository(): JenisProgramCSRRepository
    {
        return $this->jenisProgramCsrRepository;
    }


    public function buildReportBidang(JenisProgramCSR $bidang, int $tahun, AppUserInterface $user = null) : array
    {
        $filters = [ new SortOrFilter("jenis", null, $bidang)];

        $query = $this->programCsrRepository->findAllByCriteria($filters);
        $bidProgress = new ProgressTahunanDto(
            $bidang->getTitle(),
            "bidang",
            $tahun
        );
        $pembobotan = $this->pembobotanBidangRepository->findByBidang($bidang);
        if (isset($pembobotan)) {
            $bidProgress->setBobot($pembobotan->getBobot());
        }
        else {
            $bidProgress->setBobot(100);
        }
        $bidSerapan = new SerapanTahunanDto(
            $bidang->getTitle(),
            "bidang",
            $tahun
        );
        $totalBobot = 0.0;
        $debugProgres = [];
        $debugSerapan = [];
        /** @var ProgramCSR $program */
        foreach ($query->getResult() as $program) {
            $progResult = $this->buildReportProgram($program, $tahun);

            $bidProgress->addSumFromChildProgress(
                $this->multiplyProgressByBobot($progResult['progress']));
            $bidSerapan->addSumFromChildBudget($progResult['serapan']);
            $totalBobot += $progResult['progress']->getBobot();

            // debug
//            $debugProgres[$program->getName()] = $progResult['debugProgres'];
//            $debugSerapan[$program->getName()] = $progResult['debugSerapan'];
        }

        $bidProgress = $this->normalizeProgress($bidProgress, $totalBobot);

        return [
            'progress' => $bidProgress,
            'serapan' => $bidSerapan,
            'totalBobot' => $totalBobot,

//            'debugProgres' => $debugProgres,
//            'debugSerapan' => $debugSerapan,
        ];

    }

    public function buildReportProgram(ProgramCsr $program, int $tahun, AppUserInterface $user = null) : array
    {
        $filters = [
            QueryExpressionHelper::createCriteria(['property' => 'periodeBatch.programCsr',
                'value' => $program]),
            QueryExpressionHelper::createCriteria(['property' => 'danaBatch.tahun',
                'value' => $tahun ]),
        ];

        $query = $this->kegiatanRepository->findAllByCriteria($filters);
        $progProgress = new ProgressTahunanDto(
            $program->getName(),
            "program",
            $tahun
        );
        $pembobotan = $this->pembobotanProgramRepository->findByProgramCsr($program);
        if (isset($pembobotan)) {
            $progProgress->setBobot($pembobotan->getBobot());
        }
        else {
            $progProgress->setBobot(100);
        }
        $progSerapan = new SerapanTahunanDto(
            $program->getName(),
            "program",
            $tahun
        );
        $debugProgres = [];
        $debugSerapan = [];
        $totalBobot = 0.0;

        /** @var Kegiatan $kegiatan */
        foreach ($query->getResult() as $kegiatan) {
            if ($this->kegiatanIncudesYear($kegiatan, $tahun)) {
                $kegiatanResult = $this->buildReportKegiatan($kegiatan, $tahun);
                $progProgress->addSumFromChildProgress(
                    $this->multiplyProgressByBobot($kegiatanResult['progress']));
                $progSerapan->addSumFromChildBudget($kegiatanResult['serapan']);
                $totalBobot += $kegiatanResult['progress']->getBobot();

                // debug
//                $debugProgres[$kegiatan->getNamaKegiatan()] = $kegiatanResult['progress'];
//                $debugSerapan[$kegiatan->getNamaKegiatan()] = $kegiatanResult['debugSerapan'];
            }
        }
        $progProgress = $this->normalizeProgress($progProgress, $totalBobot);

        return [
            'progress' => $progProgress ,
            'serapan' => $progSerapan,
            'totalBobot' => $totalBobot,

//            'debugProgres' => $debugProgres,
//            'debugSerapan' => $debugSerapan,
        ];

    }


    private function multiplyProgressByBobot(ProgressTahunanDto $part) : ProgressTahunanDto {
        $newprogress = new ProgressTahunanDto(
            $part->getJudul(),
            $part->getLevel(),
            $part->getTahun(),
            $part->getBulanAwal(),
            $part->getBulanAkhir() );
        $newprogress->setBobot($part->getBobot());
        $multiplier = $part->getBobot();

        $newprogress->setRealisasi1( ($part->getRealisasi1()) * $multiplier );
        $newprogress->setRealisasi2( ($part->getRealisasi2()) * $multiplier );
        $newprogress->setRealisasi3( ($part->getRealisasi3()) * $multiplier );
        $newprogress->setRealisasi4( ($part->getRealisasi4()) * $multiplier );
        $newprogress->setRealisasi5( ($part->getRealisasi5()) * $multiplier );
        $newprogress->setRealisasi6( ($part->getRealisasi6()) * $multiplier );
        $newprogress->setRealisasi7( ($part->getRealisasi7()) * $multiplier );
        $newprogress->setRealisasi8( ($part->getRealisasi8()) * $multiplier );
        $newprogress->setRealisasi9( ($part->getRealisasi9()) * $multiplier );
        $newprogress->setRealisasi10( ($part->getRealisasi10()) * $multiplier );
        $newprogress->setRealisasi11( ($part->getRealisasi10()) * $multiplier );
        $newprogress->setRealisasi12( ($part->getRealisasi12()) * $multiplier );

        $newprogress->setRencana1( ($part->getRencana1()) * $multiplier );
        $newprogress->setRencana2( ($part->getRencana2()) * $multiplier );
        $newprogress->setRencana3( ($part->getRencana3()) * $multiplier );
        $newprogress->setRencana4( ($part->getRencana4()) * $multiplier );
        $newprogress->setRencana5( ($part->getRencana5()) * $multiplier );
        $newprogress->setRencana6( ($part->getRencana6()) * $multiplier );
        $newprogress->setRencana7( ($part->getRencana7()) * $multiplier );
        $newprogress->setRencana8( ($part->getRencana8()) * $multiplier );
        $newprogress->setRencana9( ($part->getRencana9()) * $multiplier );
        $newprogress->setRencana10( ($part->getRencana10()) * $multiplier );
        $newprogress->setRencana11( ($part->getRencana10()) * $multiplier );
        $newprogress->setRencana12( ($part->getRencana12()) * $multiplier );
        return $newprogress;
    }

    private function normalizeProgress(ProgressTahunanDto $total, $totalBobot) : ProgressTahunanDto {
        $newprogress = new ProgressTahunanDto(
            $total->getJudul(),
            $total->getLevel(),
            $total->getTahun(),
            $total->getBulanAwal(),
            $total->getBulanAkhir() );
        $newprogress->setBobot($total->getBobot());
        $divider = 100;

        $newprogress->setRealisasi1( ($total->getRealisasi1())/$divider );
        $newprogress->setRealisasi2( ($total->getRealisasi2())/$divider );
        $newprogress->setRealisasi3( ($total->getRealisasi3())/$divider );
        $newprogress->setRealisasi4( ($total->getRealisasi4())/$divider );
        $newprogress->setRealisasi5( ($total->getRealisasi5())/$divider );
        $newprogress->setRealisasi6( ($total->getRealisasi6())/$divider );
        $newprogress->setRealisasi7( ($total->getRealisasi7())/$divider );
        $newprogress->setRealisasi8( ($total->getRealisasi8())/$divider );
        $newprogress->setRealisasi9( ($total->getRealisasi9())/$divider );
        $newprogress->setRealisasi10( ($total->getRealisasi10())/$divider );
        $newprogress->setRealisasi11( ($total->getRealisasi10())/$divider );
        $newprogress->setRealisasi12( ($total->getRealisasi12())/$divider );

        $newprogress->setRencana1( ($total->getRencana1())/$divider );
        $newprogress->setRencana2( ($total->getRencana2())/$divider );
        $newprogress->setRencana3( ($total->getRencana3())/$divider );
        $newprogress->setRencana4( ($total->getRencana4())/$divider );
        $newprogress->setRencana5( ($total->getRencana5())/$divider );
        $newprogress->setRencana6( ($total->getRencana6())/$divider );
        $newprogress->setRencana7( ($total->getRencana7())/$divider );
        $newprogress->setRencana8( ($total->getRencana8())/$divider );
        $newprogress->setRencana9( ($total->getRencana9())/$divider );
        $newprogress->setRencana10( ($total->getRencana10())/$divider );
        $newprogress->setRencana11( ($total->getRencana10())/$divider );
        $newprogress->setRencana12( ($total->getRencana12())/$divider );
        return $newprogress;
    }

    private function buildReportKegiatan(Kegiatan $kegiatan, int $tahun) : array
    {
        $progress = new ProgressTahunanDto(
            $kegiatan->getNamaKegiatan(),
            "kegiatan",
            $tahun
        );
        if ($kegiatan->getKendaliKegiatan()) {
            $progress->copyFromKegiatan($kegiatan);
        }
        $serapan = new SerapanTahunanDto(
            $kegiatan->getNamaKegiatan(),
            "kegiatan",
            $tahun
        );

        $debugSerapan = [];
        if ($kegiatan->getChildren()) {
            foreach ($kegiatan->getChildren() as $subkegiatan){
                $serapanSub = new SerapanTahunanDto(
                    $subkegiatan->getTitle(),
                    "subkegiatan",
                    $tahun
                );
                if ($subkegiatan->getChildren()){
                    foreach ($subkegiatan->getChildren() as $rab){
                        $serapanRab = new SerapanTahunanDto(
                            $subkegiatan->getTitle(),
                            "itemrab",
                            $tahun
                        );

                        $serapanRab->copyFromItemAnggaran($rab, $tahun);
                        $serapanSub->addSumFromChildBudget($serapanRab);

                        // DEBUG
                        $debugSerapan[] = $serapanRab;
                    }
                }

                $serapan->addSumFromChildBudget($serapanSub);

                // DEBUG
                $debugSerapan[] = $serapanSub;
            }
        }

        return [
            'progress' => $progress,
            'serapan' => $serapan,

            'debugSerapan' => $debugSerapan        ];

    }

    public function buildOverview(int $tahun, ?JenisProgramCSR $bidang): array
    {
        $filters = [ new SortOrFilter("jenis", null, $bidang)];
        $query = $this->programCsrRepository->findAllByCriteria($filters);

        $data = [];
        /** @var ProgramCSR $program */
        foreach ($query->getResult() as $program) {
            /** @var ProgressTahunanDto $progress */
            $progress = $this->lastProgressProgram($program, $tahun);
            $value = $progress->getRealisasiFinal();
            $data[] = [
                'name' => $program->getName(),
                'y' => $value
            ];
        }
        return [
            'judul' => 'Bidang '. $bidang->getTitle() . ' tahun  ' . $tahun,
            'data' => $data
        ];
    }

    private function lastProgressProgram(ProgramCSR $program, int $tahun) : ProgressTahunanDto
    {
        $progResult = $this->buildReportProgram($program, $tahun);
        $progress = $progResult['progress'];
        return $progress;
    }

//    public function batchIncudesYear(BatchCsr $batch, int $tahun) : bool {
//        return ($batch->getTahunAwal() <= $tahun) && ($batch->getTahunAkhir() >= $tahun);
//    }
//
//    public function kegiatanIncudesYear(Kegiatan $kegiatan, int $tahun) : bool {
//        /** @var array $jadwal */
//        $jadwal = $kegiatan->getJadwalKegiatan();
//        if ($jadwal['awal'] && $jadwal['akhir']) {
//            return ($this->getYearPart($jadwal['awal'] <= $tahun))
//                && ($this->getYearPart($jadwal['akhir'] >= $tahun));
//        }
//        return false;
//    }
    public function kegiatanIncudesYear(Kegiatan $kegiatan, int $tahun) : bool {
        return $kegiatan->getDanaBatch()->getTahun() == $tahun;
    }

    private function getYearPart(string $ymdDate) : int {
        $date = explode('-', $ymdDate);
        $year = $date[0];
        return (int)$year;
    }
}