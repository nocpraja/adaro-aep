<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Kegiatan;


use App\Component\Workflow\Definition;
use App\Component\Workflow\Transition;
use App\Component\Workflow\Visitor\MarkingVisitorImpl;
use App\Component\Workflow\WorkflowImpl;
use App\Models\Pendanaan\Workflow\BaseConfigurer;
use App\Models\Pendanaan\Workflow\WorkflowTags;

/**
 * Class WorkflowConfigurer
 *
 * @package App\Models\Kegiatan
 * @author  Ahmad Fajar
 * @since   15/05/2019, modified: 18/05/2019 6:25
 */
final class WorkflowConfigurer extends BaseConfigurer
{

    protected function createPlaces(): void
    {
        $this->places = [
            WorkflowTags::NEW,
            WorkflowTags::UPDATED,
            WorkflowTags::SUBMITTED,
            WorkflowTags::REJECTED,
            WorkflowTags::VERIFIED,
            WorkflowTags::CLOSED,
            WorkflowTags::REALISASI_SUBMITTED,
            WorkflowTags::REALISASI_REJECTED,
            WorkflowTags::REALISASI_VERIFIED,
        ];
    }

    protected function createTransitions(): void
    {
        $this->transitions = [
            new Transition([
                               WorkflowTags::NEW,
                               WorkflowTags::UPDATED,
                               WorkflowTags::REJECTED
                           ],
                           WorkflowTags::UPDATED,
                           WorkflowTags::STEP_UPDATE),
            new Transition([
                               WorkflowTags::NEW,
                               WorkflowTags::UPDATED
                           ],
                           WorkflowTags::SUBMITTED,
                           WorkflowTags::STEP_SUBMIT),
            new Transition([WorkflowTags::SUBMITTED],
                           WorkflowTags::REJECTED,
                           WorkflowTags::STEP_REJECT),
            new Transition([WorkflowTags::SUBMITTED],
                           WorkflowTags::VERIFIED,
                           WorkflowTags::STEP_VERIFY),
            new Transition([WorkflowTags::VERIFIED, WorkflowTags::REALISASI_REJECTED],
                           WorkflowTags::REALISASI_SUBMITTED,
                           WorkflowTags::STEP_SUBMIT_REALISASI),
            new Transition([WorkflowTags::REALISASI_SUBMITTED],
                           WorkflowTags::REALISASI_REJECTED,
                           WorkflowTags::STEP_REJECT_REALISASI),
            new Transition([WorkflowTags::REALISASI_SUBMITTED],
                           WorkflowTags::REALISASI_VERIFIED,
                           WorkflowTags::STEP_VERIFY_REALISASI),
            new Transition([WorkflowTags::REALISASI_VERIFIED],
                           WorkflowTags::CLOSED,
                           WorkflowTags::STEP_CLOSING),
        ];
    }

    protected function createWorkflow(): void
    {
        $definition = new Definition($this->places, $this->transitions, $this->startPlace);
        $this->workflow = new WorkflowImpl($definition, new MarkingVisitorImpl('status'),
                                           $this->dispatcher, 'kegiatan');
    }

}
