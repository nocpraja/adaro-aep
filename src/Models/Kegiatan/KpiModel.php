<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Kegiatan;


use App\Component\DataObject\ProgramCsrDto;
use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Entity\Kegiatan\CapaianKpi;
use App\Entity\Kegiatan\IndikatorKpi;
use App\Entity\Kegiatan\ViewIndikator;
use App\Entity\MasterData\BatchCsr;
use App\Entity\MasterData\BatchCsrTahun;
use App\Entity\MasterData\DataReference;
use App\Entity\MasterData\ProgramCSR;
use App\Entity\Security\UserAccount;
use App\Repository\Beneficiary\BeneficiaryBeasiswaRepository;
use App\Repository\Beneficiary\BeneficiaryIndividuRepository;
use App\Repository\Kegiatan\CapaianKpiRepository;
use App\Repository\Kegiatan\IndikatorKpiRepository;
use App\Repository\Kegiatan\ViewIndikatorKpiRepository;
use App\Repository\MasterData\BatchCsrRepository;
use App\Repository\MasterData\BatchCsrTahunRepository;
use App\Repository\MasterData\DataReferenceRepository;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class KpiModel
 * @package App\Model\Kegiatan
 * @author  Mark Melvin
 * @since   28/01/2019, modified: 24/07/2019 12:08
 */
class KpiModel
{
    /**
     * @var DataReferenceRepository
     */
    private $paramRepository;

    /**
     * @var BatchCsrRepository
     */
    private $batchCsrRepository;

    /**
     * @var CapaianKpiRepository
     */

    private $capaianKpiRepository;

    /**
     * @var BatchCsrTahunRepository
     */

    private $batchTahunRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var IndikatorKpiRepository
     */
    private $indikatorKpiRepository;
    /**
     * @var DataReferenceRepository
     */
    private $dataReferenceRepository;
    /**
     * @var BeneficiaryIndividuRepository
     */
    private $individuRepository;
    /**
     * @var BeneficiaryBeasiswaRepository
     */
    private $beasiswaRepository;
    /**
     * @var ViewIndikatorKpiRepository
     */
    private $viewIndikatorKpiRepository;


    /**
     * KpiModel constructor.
     *
     * @param DataReferenceRepository       $paramRepository         Doctrine entity repository instance
     * @param BatchCsrRepository            $batchCsrRepository      Doctrine entity repository instance
     * @param CapaianKpiRepository          $capaianKpiRepository    Doctrine entity repository instance
     * @param IndikatorKpiRepository        $indikatorKpiRepository  Doctrine entity repository instance
     * @param BeneficiaryIndividuRepository $individuRepository      Doctrine entity repository instance
     * @param BeneficiaryBeasiswaRepository $beasiswaRepository
     * @param DataReferenceRepository       $dataReferenceRepository Doctrine entity repository instance
     * @param BatchCsrTahunRepository       $batchTahunRepository    Doctrine entity repository instance
     * @param ViewIndikatorKpiRepository    $viewIndikatorKpiRepository
     * @param EntityManagerInterface        $em                      Doctrine entity manager instance
     */
    public function __construct(DataReferenceRepository $paramRepository,
                                BatchCsrRepository $batchCsrRepository,
                                CapaianKpiRepository $capaianKpiRepository,
                                IndikatorKpiRepository $indikatorKpiRepository,
                                BeneficiaryIndividuRepository $individuRepository,
                                BeneficiaryBeasiswaRepository $beasiswaRepository,
                                DataReferenceRepository $dataReferenceRepository,
                                BatchCsrTahunRepository $batchTahunRepository,
                                ViewIndikatorKpiRepository $viewIndikatorKpiRepository,
                                EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->paramRepository = $paramRepository;
        $this->batchCsrRepository = $batchCsrRepository;
        $this->batchTahunRepository = $batchTahunRepository;
        $this->capaianKpiRepository = $capaianKpiRepository;
        $this->indikatorKpiRepository = $indikatorKpiRepository;
        $this->dataReferenceRepository = $dataReferenceRepository;
        $this->individuRepository = $individuRepository;
        $this->beasiswaRepository = $beasiswaRepository;
        $this->viewIndikatorKpiRepository = $viewIndikatorKpiRepository;
    }

    /**
     * Collect capaian KPI kualitas untuk program CSR.
     * @return ProgramCsrDto[] Collection of program CSR
     */
    public function collectProgramCsrKpiKualitas(): array
    {
        $programs = $this->findProgramCsrHasKpi();

        foreach ($programs as $program) {
            foreach ($program->getBatches() as $batch) {
                $filters = QueryExpressionHelper::createCriterias(
                    [
                        ['property' => 'periodeBatch.batchId', 'value' => $batch->getBatchId()],
                        ['property' => 'programCsr.msid', 'value' => $program->getMsid()],
                        ['property' => 'kuantitas', 'value' => false],
                    ]
                );
                $sorts = QueryExpressionHelper::createCriterias(
                    [
                        ['property' => 'tahun', 'direction' => 'asc'],
                        ['property' => 'dataRef.code', 'direction' => 'asc'],
                    ]
                );
                $query = $this->capaianKpiRepository->findAllByCriteria($filters, $sorts);
                /** @var CapaianKpi[] $capaianKpis */
                $capaianKpis = $query->getResult();

                if (!empty($capaianKpis)) {
                    $yearlyCapaian = $batch->getYearlyCapaian();
                    foreach ($yearlyCapaian as $kpiDto) {
                        foreach ($capaianKpis as $capaianKpi) {
                            if ($capaianKpi->getTahun() == $kpiDto->getYear()) {
                                $kpiDto->addParameter($capaianKpi);
                            }
                        }
                    }
                }
            }
        }

        return $programs;
    }

    /**
     * Collect capaian KPI kualitas untuk program CSR.
     *
     * @param UserAccount|AppUserInterface|null $user
     *
     * @return ProgramCsrDto[] Collection of program CSR
     */
    public function collectProgramCsrKpi(AppUserInterface $user = null): array
    {
        $programs = $this->findProgramCsrHasKpi($user);

        foreach ($programs as $program) {
            foreach ($program->getBatches() as $batch) {
                $filters = QueryExpressionHelper::createCriterias(
                    [
                        ['property' => 'periodeBatch.batchId', 'value' => $batch->getBatchId()],
                        ['property' => 'programCsr.msid', 'value' => $program->getMsid()]
                    ]
                );
                $sorts = QueryExpressionHelper::createCriterias(
                    [
                        ['property' => 'tahun', 'direction' => 'asc'],
                        ['property' => 'dataRef.code', 'direction' => 'asc'],
                    ]
                );

                $capaianKpis = $this->capaianKpiRepository->findAllByCriteria($filters, $sorts)
                    ->getResult();
                if (!empty($capaianKpis)) {
                    $yearlyCapaian = $batch->getYearlyCapaian();

                    foreach ($yearlyCapaian as $kpiDto) {
                        /** @var CapaianKpi $capaianKpi */
                        foreach ($capaianKpis as $capaianKpi) {
                            if ($capaianKpi->getTahun() == $kpiDto->getYear()) {
                                $kpiDto->addParameter($capaianKpi);
                            }
                        }
                    }
                }
            }
        }

        return $programs;
    }

    /**
     * List data referensi KPI Parameter untuk jenis ProgramCsr.
     *
     * @param int $msid ID Program CSR
     *
     * @return DataReference[] Collection of persistent entity object
     */
    public function getParams(int $msid): array
    {
        $head = 'KPI-' . str_pad($msid, 2, '0', STR_PAD_LEFT);
        $parent = $this->paramRepository->findOneBy(['code' => $head]);

        $children = $this->paramRepository
            ->findAllByCriteria(
                QueryExpressionHelper::createCriterias(
                    [
                        ['property' => 'parent', 'value' => $parent],
                        ['property' => 'protected', 'value' => true]
                    ]
                ),
                [new SortOrFilter('code', 'asc')]
//                [new SortOrFilter('parent', null, $parent)],
//                [new SortOrFilter('code', 'asc')]
            )
            ->getResult();

        $grandson = [];
        foreach ($children as $child) {
            $tempChild = $this->paramRepository
                ->findAllByCriteria(
                    QueryExpressionHelper::createCriterias(
                        [
                            ['property' => 'parent', 'value' => $child],
                            ['property' => 'protected', 'value' => true]
                        ]
                    ),
                    [new SortOrFilter('code', 'asc')]
//                    [new SortOrFilter('parent', null, $child)],
//                    [new SortOrFilter('code', 'asc')]
                )
                ->getResult();

            $grandson = array_merge($grandson, $tempChild);
        }

        if (count($grandson) > 0) {
            $children = array_merge($children, $grandson);
        }

        return $children;
    }

    /**
     * daftar KPI capaian target untuk tahun Batch pembinaan.
     *
     * @param BatchCsr $batch Persistent entity object of Batch pembinaan
     * @param int      $tahun Tahun anggaran
     *
     * @return CapaianKpi[]
     */
    public function getTargets(BatchCsr $batch, int $tahun): array
    {
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'periodeBatch', 'value' => $batch],
                ['property' => 'tahun', 'value' => $tahun],
            ]
        );
        $result = $this->capaianKpiRepository->findAllByCriteria($filters)->getResult();
        // susun sebagai array dengan index berdasarkan kode DataRef
        $tmpArray = array();

        /* @var CapaianKpi $capaianKpi */
        foreach ($result as $capaianKpi) {
            $code = $capaianKpi->getDataRef()->getCode();
            $tmpArray[$code] = $capaianKpi;
        }

        return $tmpArray;
    }

    /**
     * daftar KPI capaian target untuk seluruh Batch pembinaan.
     *
     * @param BatchCsr $batch Persistent entity object of Batch pembinaan
     *
     * @return array
     */
    public function getTargetsBatch(BatchCsr $batch): array
    {
        $kpiArray = [];
        $tahunAwal = $batch->getTahunAwal();
        $tahunAkhir = $batch->getTahunAkhir();

        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {
            // susun item detail target KPI sebagai rectangular array
            // dengan index tahun dan kode DataRef
            $targetTahun = $this->getTargets($batch, $tahun);
            $kpiArray[$tahun] = $targetTahun;
        }

        return $kpiArray;
    }

    /**
     * Initialize KPI Target baru untuk batch pembinaan.
     *
     * @param BatchCsr $batch Persistent entity object of Batch pembinaan
     */
    public function populateTargets(BatchCsr $batch): void
    {
        $program = $batch->getProgramCsr();
        $msid = $program->getMsid();
        $params = $this->getParams($msid);
        $tahunAwal = $batch->getTahunAwal();
        $tahunAkhir = $batch->getTahunAkhir();

        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {
            foreach ($params as $param) {
                if ($this->findDuplicate($program, $batch, $param, $tahun) == 0) {
                    $entity = new CapaianKpi();
                    $entity->setProgramCsr($program);
                    $entity->setPeriodeBatch($batch);
                    $entity->setDataRef($param);
                    $entity->setTahun($tahun);

                    $subParams = $param->getParams();
                    $dashboard = ($this->findElement($subParams, 'dashboard') == 'TRUE');
                    $entity->setShowInDashboard($dashboard);

                    $kuantitas = ($this->findElement($subParams, 'grouping') == 'KUANTITAS');
                    $entity->setKuantitas($kuantitas);

                    $chartLabel = $this->findElement($subParams, 'Chart Label');
                    $entity->setLabel($chartLabel);

                    $entity->setP1Verified(false);
                    $entity->setP2Verified(false);
                    $entity->setP3Verified(false);
                    $entity->setP4Verified(false);
                    $this->em->persist($entity);
                }
            }

            $this->em->flush();
        }
    }

    /**
     * Save capaian KPI untuk satu Batch pembinaan.
     *
     * @param BatchCsr   $batch   Persistent entity object of Batch pembinaan
     * @param CapaianKpi $capaian Persistent entity object of CapaianKpi
     */
    public function saveCapaianKpi(BatchCsr $batch, CapaianKpi $capaian): void
    {
        $capaianId = $capaian->getId();
        $entity = $this->capaianKpiRepository->find($capaianId);

        if ($entity->getPeriodeBatch()->getBatchId() == $batch->getBatchId()) {
            if (!empty($capaian->getP1())) {
                $entity->setP1($capaian->getP1());
            }
            if (!empty($capaian->getP2())) {
                $entity->setP2($capaian->getP2());
            }
            if (!empty($capaian->getP3())) {
                $entity->setP3($capaian->getP3());
            }
            if (!empty($capaian->getP4())) {
                $entity->setP4($capaian->getP4());
            }
            if (!empty($capaian->getP1Verified())) {
                $entity->setP1Verified($capaian->getP1Verified());
            }
            if (!empty($capaian->getP2Verified())) {
                $entity->setP2Verified($capaian->getP2Verified());
            }
            if (!empty($capaian->getP3Verified())) {
                $entity->setP3Verified($capaian->getP3Verified());
            }
            if (!empty($capaian->getP4Verified())) {
                $entity->setP4Verified($capaian->getP4Verified());
            }
            if (!empty($capaian->getNilai())) {
                $entity->setNilai($capaian->getNilai());
            }
            if (!empty($capaian->getNilaiAkhir())) {
                $entity->setNilaiAkhir($capaian->getNilaiAkhir());
            }
            $this->em->persist($entity);
            $this->em->flush();
        }
    }

    /**
     * save capaian KPI untuk Batch pembinaan.
     *
     * @param BatchCsr   $batch  Persistent entity object of Batch pembinaan
     * @param CapaianKpi $target Persistent entity object of CapaianKpi
     */
    public function saveTarget(BatchCsr $batch, CapaianKpi $target): void
    {
        $targetId = $target->getId();
        $targetValue = $target->getTarget();
        $entity = $this->capaianKpiRepository->find($targetId);

        if ($entity->getPeriodeBatch()->getBatchId() == $batch->getBatchId()) {
            $entity->setTarget($targetValue);
            $this->em->persist($entity);
            $this->em->flush();
        }
    }

    /**
     * Verifikasi capaian target untuk tahun Batch pembinaan.
     *
     * @param BatchCsr $batch  Persistent entity object of Batch pembinaan
     * @param int      $tahun  Tahun anggaran
     * @param int      $period Periode pelaksanaan untuk triwulan/caturwulan/semester ke berapa
     *
     * @return int
     */
    public function verifyCapaian(BatchCsr $batch, int $tahun, int $period): int
    {
        $field = 'p' . $period . "Verified";
        $q = $this->em->createQuery('update ' . CapaianKpi::class . ' c set c.' . $field .
            ' = TRUE where c.periodeBatch = :batch and c.tahun = :tahun');

        $q->setParameters(['batch' => $batch, 'tahun' => $tahun]);
        $numUpdated = $q->execute();

        return $numUpdated;
    }

    /**
     * Verifikasi capaian target kuantitas untuk tahun Batch pembinaan.
     *
     * @param BatchCsr $batch  Persistent entity object of Batch pembinaan
     * @param int      $tahun  Tahun anggaran
     * @param int      $period Periode pelaksanaan untuk triwulan/caturwulan/semester ke berapa
     *
     * @return int
     */
    public function verifyCapaianKuantitas(BatchCsr $batch, int $tahun, int $period): int
    {
        $field = 'p' . $period . "Verified";
        $q = $this->em->createQuery('update ' . CapaianKpi::class . ' c set c.' . $field .
            ' = TRUE where c.periodeBatch = :batch and c.tahun = :tahun and c.kuantitas = TRUE');

        $q->setParameters(['batch' => $batch, 'tahun' => $tahun]);
        $numUpdated = $q->execute();

        return $numUpdated;
    }

    /**
     * Verifikasi capaian target kualitas untuk tahun Batch pembinaan.
     *
     * @param BatchCsr $batch  Persistent entity object of Batch pembinaan
     * @param int      $tahun  Tahun anggaran
     * @param int      $period Periode pelaksanaan untuk triwulan/caturwulan/semester ke berapa
     *
     * @return int
     */
    public function verifyCapaianKualitas(BatchCsr $batch, int $tahun, int $period): int
    {
        $field = 'p' . $period . "Verified";
        $q = $this->em->createQuery('update ' . CapaianKpi::class . ' c set c.' . $field .
            ' = TRUE where c.periodeBatch = :batch and c.tahun = :tahun and c.kuantitas = FALSE');

        $q->setParameters(['batch' => $batch, 'tahun' => $tahun]);
        $numUpdated = $q->execute();

        return $numUpdated;
    }

    /**
     * cari duplikat target Capaian KPI yang sudah ada
     *
     * @param ProgramCSR    $program Persistent entity object of Program CSR
     * @param BatchCsr      $batch   Persistent entity object of Batch pembinaan
     * @param DataReference $param   Persistent entity object of DataReference
     * @param int           $tahun   Tahun anggaran
     *
     * @return int Jumlah duplikasi
     */
    private function findDuplicate(ProgramCSR $program, BatchCsr $batch, DataReference $param, int $tahun): ?int
    {
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'periodeBatch', 'value' => $batch],
                ['property' => 'tahun', 'value' => $tahun],
                ['property' => 'programCsr', 'value' => $program],
                ['property' => 'dataRef', 'value' => $param],
            ]
        );
        $query = $this->capaianKpiRepository->findAllByCriteria($filters);
        $result = $query->getResult();

        return count($result);
    }

    /**
     * Mencari nilai parameter KPI.
     *
     * @param array|null $params Input parameter
     * @param String     $key    The key to search
     *
     * @return String The parameter value
     */
    private function findElement(?array $params, string $key): ?string
    {

        if (is_array($params)) {
            $value = null;

            foreach ($params as $p) {
                if (is_array($p)) {
                    if ($p["property"] == $key) {
                        $value = $p["value"];
                    }
                }
            }

            return $value;
        }

        return null;
    }

    /**
     * @param UserAccount|AppUserInterface|null $user
     *
     * @return ProgramCsrDto[]|null
     */
    private function findProgramCsrHasKpi(AppUserInterface $user = null): array
    {
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'parent', 'value' => null],
                ['property' => 'code', 'value' => 'KPI', 'operator' => 'startwith'],
            ]
        );
        $results = $this->paramRepository->findAllByCriteria($filters)
            ->getResult();

        if (!empty($results)) {
            $ids = [];

            /** @var DataReference $item */
            foreach ($results as $item) {
                $ids[] = intval(str_replace('KPI-', '', $item->getCode()));
            }

            $filters = QueryExpressionHelper::createCriterias(
                [
                    ['property' => 'programcsr.msid', 'value' => $ids, 'operator' => 'in']
                ]
            );
            $sorts = QueryExpressionHelper::createCriterias(
                [
                    ['property' => 'programcsr.msid', 'direction' => 'asc'],
                    ['property' => 'batch.namaBatch', 'direction' => 'asc'],
                ]
            );
            if (!is_null($user) && !is_null($user->getMitraCompany())) {
                $filters[] = QueryExpressionHelper::createCriteria(
                    [
                        'property' => 'mitra.mitraId',
                        'value' => $user->getMitraCompany()->getMitraId()
                    ]
                );
            }

            $results = $this->batchCsrRepository->findAllByCriteria($filters, $sorts)
                ->getResult();

            return ProgramCsrDto::createFromEntities($results);
        } else {
            return [];
        }
    }

    /**
     * Method untuk menghitung nilai akhir dari capaian KPI sesuai perhitungan TLA.
     * Method ini hanya digunakan untuk nilai terkahir yang diverifikasi saja
     *
     * @param CapaianKpi $capaianKpi
     * @param array      $latestVerified Data tahun dan periode terakhir dari cpaianKpi diverifikasi
     * @param int|null   $pembagi        Nilai pembagi untuk persentase (dalam perhitungan nilai akhir)
     *
     * @return float
     */
    public function calculateVerifiedNilaiAkhir(CapaianKpi $capaianKpi, array $latestVerified, ?int $pembagi): float
    {
        if ($pembagi) { // Kasus 0 atau null lewati
            $periode = $latestVerified['periode'];
            $bobot = 0;

            foreach ($capaianKpi->getDataRef()->getParams() as $param) {
                if ($param['property'] == 'bobot') {
                    $bobot = (float)$param['value'];

                    break;
                }
            }

            if ($capaianKpi->getDataRef()->getCode() == "KPI-02.014") {
                $daftarSelisihTarget = [3.25, 3.5, 3.75, 4];
                $baseline = 0;

                foreach ($capaianKpi->getDataRef()->getParams() as $param) {
                    if ($param['property'] == 'baseline') {
                        $baseline = (float)$param['value'];
                        break;
                    }
                }
                $persentase = (($this->getValueByP($capaianKpi,
                                $periode) - $baseline) / ($daftarSelisihTarget[$latestVerified["tahun"] - $capaianKpi->getPeriodeBatch()
                                ->getTahunAwal()] - $baseline)) * 100;
            } else {
                $persentase = ($this->getValueByP($capaianKpi, $periode) / $pembagi) * 100;
            }

            if ($capaianKpi->getProgramCsr()->getMsId() != 5) {
                $bobot = $bobot / 100;
            }
            return $bobot * $persentase;
        } else {
            return 0;
        }
    }

    /**
     * Getting value of P in a capaian KPI.
     *
     * @param CapaianKpi $capaianKpi Capaian Kpi Model
     *
     * @return int|null
     */
    public function chooseValueFromLatestP(CapaianKpi $capaianKpi): ?int
    {
        if ($capaianKpi->getP4Verified()) return $capaianKpi->getP4();

        if ($capaianKpi->getP3Verified()) return $capaianKpi->getP3();

        if ($capaianKpi->getP2Verified()) return $capaianKpi->getP2();

        if ($capaianKpi->getP1Verified()) return $capaianKpi->getP1();

        return null;
    }

    /**
     * Getting latest year of capaian kpi that has been verified.
     *
     * @param array $capaianKpis Array of capaian kpi model
     *
     * @return array
     */
    public function latestVerifiedOfProgram(array $capaianKpis): array
    {
        $tahun = 0;
        $periode = 0;

        foreach ($capaianKpis as $capaianKpi) {
            if ($capaianKpi->getTahun() > $tahun) {
                $thisyearCapaians = array_filter($capaianKpis, function ($val) use ($capaianKpi) {
                    /** @var CapaianKpi $val */
                    return $val->getTahun() == $capaianKpi->getTahun();
                });

                $tempPeriode = $this->chooseFullVerfiedPeriode($thisyearCapaians);
                if ($tempPeriode > 0) {
                    $tahun = $capaianKpi->getTahun();
                    $periode = $tempPeriode;
                }
            }
        }

        return [
            "tahun" => $tahun,
            "periode" => $periode
        ];
    }

    /**
     * Mendapatkan periode dari capaian KPI yang nilainya telah diverifikasi baik kuantitas maupun kualitas.
     *
     * @param array $capaianKpis Daftar capaian KPI dari program
     *
     * @return int
     */
    public function chooseFullVerfiedPeriode(array $capaianKpis): int
    {
        for ($i = 4; $i > 0; --$i) {
            $chooseThis = true;
            foreach ($capaianKpis as $capaianKpi) {
                if (!$this->getIsVerifiedByP($capaianKpi, $i)) {
                    $chooseThis = false;
                    break;
                }
            }
            if ($chooseThis) {
                return $i;
            }
        }

        return 0;
    }

    /**
     * Get Apakah nilai Capaian untuk periode terentu telah diverifikasi atau tidak.
     *
     * @param CapaianKpi $capaian
     * @param int        $periode Periode capaian yang ingin diketahui diverifikasi atau belum.
     *
     * @return int|bool
     */
    public function getIsVerifiedByP(CapaianKpi $capaian, int $periode): ?bool
    {
        switch ($periode) {
            case 1 :
                return $capaian->getP1Verified();
            case 2 :
                return $capaian->getP2Verified();
            case 3 :
                return $capaian->getP3Verified();
            case 4 :
                return $capaian->getP4Verified();
            default :
                return null;
        }
    }

    /**
     * Get Nilai Capaian untuk periode terentu.
     *
     * @param CapaianKpi $capaian
     * @param int        $periode Periode capaian yang ingin diambil nilainya.
     *
     * @return float|null
     */
    public function getValueByP(CapaianKpi $capaian, int $periode): ?float
    {
        switch ($periode) {
            case 1 :
                return $capaian->getP1();
            case 2 :
                return $capaian->getP2();
            case 3 :
                return $capaian->getP3();
            case 4 :
                return $capaian->getP4();
            default :
                return null;
        }
    }


    // =============== Fungsi pengelolaan narasi statistik Capaian KPI ============


    /**
     * Get Statistk Capaian untuk tahun batch terentu.
     *
     * @param BatchCsr $batch
     * @param int      $tahun
     *
     * @return BatchCsrTahun|null
     */
    public function getStatistik(BatchCsr $batch, int $tahun): ?BatchCsrTahun
    {
        return $this->batchTahunRepository->findOneByBatchTahun($batch, $tahun);
    }


    /**
     * Set Statistk Capaian untuk tahun batch terentu.
     *
     * @param BatchCsr $batch
     * @param int      $tahun
     * @param string   $narasi
     *
     * @return BatchCsrTahun|null
     */
    public function saveStatistikNarasi(BatchCsr $batch, int $tahun, string $narasi): BatchCsrTahun
    {
        $entity = $this->getStatistik($batch, $tahun);

        if (is_null($entity)) {
            $entity = new BatchCsrTahun();
            $entity->setBatchCsr($batch);
            $entity->setTahun($tahun);
        }

        $entity->setNarasiKpi($narasi);
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }


}
