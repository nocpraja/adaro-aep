<?php

namespace App\Models\Kegiatan;

use App\Component\DataObject\SortOrFilter;
use App\Entity\Kegiatan\PembobotanProgram;
use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\MasterData\ProgramCSR;
use App\Repository\Kegiatan\PembobotanProgramRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PembobotannProgramModel
 *
 * @package App\Models\Kegiatan
 * @author  Mark Melvin
 * @since   17/7/2019
 */
class PembobotanProgramModel
{
    /**
     * @var PembobotanProgramRepository
     */
    private $repository;

    /**
     * @var ProgramCSRRepository
     */
    private $programRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProgramCsrModel constructor.
     *
     * @param PembobotanProgramRepository $repository Entity repository instance
     * @param ProgramCSRRepository $programRepository Entity repository instance
     * @param EntityManagerInterface $em Doctrine entity manager instance
     */
    public function __construct(PembobotanProgramRepository $repository,
                                ProgramCSRRepository $programRepository,
                                EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->programRepository = $programRepository;
        $this->em = $em;
    }

    /**
     * @param JenisProgramCSR $bidang
     * @return array
     */
    public function getAllPembobotanProgram(JenisProgramCSR $bidang) {
        $query = $this->programRepository->findAllByCriteria(
            [new SortOrFilter('jenis', null, $bidang)],
            [], 0, 0);
        $programs = $query->getResult();
        $result = [];
        /** @var ProgramCSR $prog */
        foreach ($programs as $prog) {
            $pembobotanProgram = $this->repository->findByProgramCsr($prog);
            $result[] = ['program' => $prog, 'pembobotan' => $pembobotanProgram];
        }
        return $result;
    }

    /**
     * @param ProgramCSR $program
     * @param float $bobot
     *
     * @@return boolean
     */
    public function savePembobotanProgram(ProgramCSR $program, float $bobot) {
        $pembobotanProgram = $this->repository->findByProgramCsr($program);
        if (isset($pembobotanProgram)) {
            $pembobotanProgram->setBobot($bobot);
            $this->em->persist($pembobotanProgram);
            $this->em->flush();
        }
        else
        {
            $pembobotanProgram = new PembobotanProgram($program, $program->getJenis());
            $pembobotanProgram->setBobot($bobot);
            $this->em->persist($pembobotanProgram);
            $this->em->flush();
        }

        return true;
    }

}