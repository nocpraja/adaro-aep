<?php


namespace App\Models\Kegiatan;

use App\Entity\Kegiatan\PembobotanBidang;
use App\Entity\MasterData\JenisProgramCSR;
use App\Repository\Kegiatan\PembobotanBidangRepository;
use App\Repository\MasterData\JenisProgramCSRRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PembobotannBidangModel
 *
 * @package App\Models\Kegiatan
 * @author  Mark Melvin
 * @since   17/7/2019
 */
class PembobotanBidangModel
{
    /**
     * @var PembobotanBidangRepository
     */
    private $repository;

    /**
     * @var JenisProgramCSRRepository
     */
    private $bidangRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * BidangCsrModel constructor.
     *
     * @param PembobotanBidangRepository $repository Entity repository instance
     * @param JenisProgramCSRRepository $bidangRepository Entity repository instance
     * @param EntityManagerInterface $em Doctrine entity manager instance
     */
    public function __construct(PembobotanBidangRepository $repository,
                                JenisProgramCSRRepository $bidangRepository,
                                EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->bidangRepository = $bidangRepository;
        $this->em = $em;
    }

    /**
     * @return array
     */
    public function getAllPembobotanBidang() {
        $query = $this->bidangRepository->findAllByCriteria([], [], 0, 0);
        $bidangCsr = $query->getResult();
        $result = [];
        /** @var JenisProgramCSR $bid */
        foreach ($bidangCsr as $bid) {
            $pembobotanBidang = $this->repository->findByBidang($bid);
            $result[] = ['bidang' => $bid, 'pembobotan' => $pembobotanBidang];
        }
        return $result;
    }

    /**
     * @param JenisProgramCSR $bidang
     * @param float $bobot
     *
     * @@return boolean
     */
    public function savePembobotanBidang(JenisProgramCSR $bidang, float $bobot) {
        $pembobotanBidang = $this->repository->findByBidang($bidang);
        if (isset($pembobotanBidang)) {
            $pembobotanBidang->setBobot($bobot);
            $this->em->persist($pembobotanBidang);
            $this->em->flush();
        }
        else
        {
            $pembobotanBidang = new PembobotanBidang($bidang);
            $pembobotanBidang->setBobot($bobot);
            $this->em->persist($pembobotanBidang);
            $this->em->flush();
        }

        return true;
    }

}