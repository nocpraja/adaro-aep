<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */


namespace App\Models\Kegiatan;

use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\DataObject\Kegiatan\SerapanTahunanDto;
use App\Entity\Kegiatan\ItemAnggaran;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\Subkegiatan;
use App\Entity\MasterData\BatchCsr;
use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\MasterData\ProgramCsr;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\MasterData\BatchCsrRepository;
use App\Repository\MasterData\JenisProgramCSRRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MonitoringModel
 *
 * @package App\Models\Kegiatan
 * @author  Mark Melvin
 * @since   16/12/2019
 */
class MonitoringModel
{

    /**
     * @var JenisProgramCSRRepository
     */
    private $bidangCsrRepository;

    /**
     * @var ProgramCSRRepository
     */
    private $programCsrRepository;

    /**
     * @var BatchCsrRepository
     */
    private $batchCsrRepository;

    /**
     * @var KegiatanRepository
     */
    private $kegiatanRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MonitoringModel constructor.
     * @param JenisProgramCSRRepository $bidangCsrRepository
     * @param ProgramCSRRepository $programCsrRepository
     * @param BatchCsrRepository $batchCsrRepository
     * @param KegiatanRepository $kegiatanRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(JenisProgramCSRRepository $bidangCsrRepository,
                                ProgramCSRRepository $programCsrRepository,
                                BatchCsrRepository $batchCsrRepository,
                                KegiatanRepository $kegiatanRepository,
                                EntityManagerInterface $em)
    {
        $this->bidangCsrRepository = $bidangCsrRepository;
        $this->programCsrRepository = $programCsrRepository;
        $this->batchCsrRepository = $batchCsrRepository;
        $this->kegiatanRepository = $kegiatanRepository;
        $this->em = $em;
    }


    private function parseYearMonth(string $yearmonth) {
        $parse = date_parse_from_format("Yn", $yearmonth);
        return [
            'year' => $parse['year'],
            'month' => $parse['month'],
            'error_count' => $parse['error_count'],
            'errors' => $parse['errors']
        ];
    }


    public function buildReport(ProgramCsr $program, string $periodeAwal, string $periodeAkhir, AppUserInterface $user = null) : array
    {
        $dt1 = $this->parseYearMonth($periodeAwal);
        $bulanAwal = $dt1['month'];
        $tahunAwal = $dt1['year'];
        $dt2 = $this->parseYearMonth($periodeAkhir);
        $bulanAkhir = $dt2['month'];
        $tahunAkhir = $dt2['year'];

        return $this->buildReportProgram($program, $tahunAwal, $tahunAkhir, $bulanAwal, $bulanAkhir, $user);
    }


    /**
     * @param ProgramCsr $program
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @param int $bulanAwal
     * @param int $bulanAkhir
     * @param AppUserInterface|null $user
     * @param bool $is_rekap
     * @return array
     */
    private function buildReportProgram(ProgramCsr $program,
                                        int $tahunAwal, int $tahunAkhir,
                                        int $bulanAwal, int $bulanAkhir,
                                        AppUserInterface $user = null,
                                        bool $is_rekap = false) : array
    {
        $filters = [ new SortOrFilter("programCsr.msid", null, $program->getMsid())];

        $query = $this->batchCsrRepository->findAllByCriteria($filters);
        $results = [];
        foreach ($query->getResult() as $batch) {
            //if ($this->batchIncudesYear($batch, $tahunAwal, $tahunAkhir)) {
                $batchSerapan = $this->buildReportBatch($batch, $tahunAwal, $tahunAkhir, $user, $is_rekap);
                foreach ($batchSerapan as $item) {
                    $results[] = $item;
                }
            //}
        }
        $progSerapan = [];
        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {
            $progSerapan[$tahun] = new SerapanTahunanDto(
                $program->getName(),
                "program",
                $tahun
            );
            if ($tahun == $tahunAwal) {
                $progSerapan[$tahun]->setBulanAwal($bulanAwal);
            } else {
                $progSerapan[$tahun]->setBulanAwal(1);
            }
            if ($tahun == $tahunAkhir) {
                $progSerapan[$tahun]->setBulanAkhir($bulanAkhir);
            } else {
                $progSerapan[$tahun]->setBulanAkhir(12);
            }
        }
        foreach ($results as $budget) {
            for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
                if ($budget[$th]->getLevel() == "batch") {
                    $progSerapan[$th]->addSumFromChildBudget($budget[$th]);
                }
            }
        }
        // handle multi-year totals
        $sum = ['budget'=> 0 , 'serapan' => 0];
        for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
            $sum = $this->buildSumArray($sum, $progSerapan[$th]);
        }
        $progSerapan['sum'] = $sum;

        array_unshift($results, $progSerapan);
        return $results;
    }

    /**
     * @param BatchCsr $batch
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @param AppUserInterface|null $user
     * @param bool $is_rekap
     * @return array
     */
    private function buildReportBatch(BatchCsr $batch,
                                      int $tahunAwal,
                                      int $tahunAkhir,
                                      AppUserInterface $user = null,
                                      bool $is_rekap = false) :array
    {
        $filters = [QueryExpressionHelper::createCriteria(['property' => 'periodeBatch', 'value' => $batch])];
        if (!is_null($user) && !is_null($user->getMitraCompany())) {
            $filters[] = QueryExpressionHelper::createCriteria(['property' => 'periodeBatch.mitra',
                'value' => $user->getMitraCompany()]);
        }

        $query = $this->kegiatanRepository->findAllByCriteria($filters);

        $results = [];
        foreach ($query->getResult() as $kegiatan) {
            $kegSerapan = $this->buildReportKegiatan($kegiatan, $tahunAwal, $tahunAkhir);
            foreach ($kegSerapan as $item) {
                $results[] = $item;
            }
        }
        $batchSerapan = [];
        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {
            $batchSerapan[$tahun] = new SerapanTahunanDto(
                $batch->getNamaBatch(),
                "batch",
                $tahun
            );
        }
        foreach ($results as $budget) {
            for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
                if ($budget[$th]->getLevel() == 'kegiatan') {
                    $batchSerapan[$th]->addSumFromChildBudget($budget[$th]);
                }
            }
        }
        if ($is_rekap) {
            for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
                // flag records with year outside batch's active period
                if (($th < $batch->getTahunAwal()) || ($th > $batch->getTahunAkhir())) {
                    $batchSerapan[$th]->setFlag('X');
                }
            }
            return [$batchSerapan];
        }
        // handle multi-year totals
        $sum = ['budget'=> 0 , 'serapan' => 0];
        for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
            $sum = $this->buildSumArray($sum, $batchSerapan[$th]);
        }
        $batchSerapan['sum'] = $sum;

        array_unshift($results, $batchSerapan);
        return $results;

    }

    /**
     * @param Kegiatan $kegiatan
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @return array
     */
    private function buildReportKegiatan(Kegiatan $kegiatan, int $tahunAwal, int $tahunAkhir) :array
    {
        $results = [];
        $kegSerapan = [];
        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {
            $kegSerapan[$tahun] = new SerapanTahunanDto(
                $kegiatan->getNamaKegiatan(),
                "kegiatan",
                $tahun
            );
        }
        foreach ($kegiatan->getChildren() as $subkegiatan) {
            $subSerapan = $this->buildReportSubkegiatan($subkegiatan, $tahunAwal, $tahunAkhir);
            $results[] = $subSerapan;
        }

        foreach ($results as $budget) {
            for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
                if ($budget[$th]->getLevel() == 'subkegiatan'){
                    $kegSerapan[$th]->addSumFromChildBudget($budget[$th]);
                }
            }
        }
        // handle multi-year totals
        $sum = ['budget'=> 0 , 'serapan' => 0];
        for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
            $sum = $this->buildSumArray($sum, $kegSerapan[$th]);
        }
        $kegSerapan['sum'] = $sum;

        array_unshift($results, $kegSerapan);
        return $results;
    }

    /**
     * @param Subkegiatan $subkegiatan
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @return SerapanTahunanDto[]
     */
    private function buildReportSubkegiatan(Subkegiatan $subkegiatan, int $tahunAwal, int $tahunAkhir) : array
    {
        $result = [];
        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {
            $subkSerapan = new SerapanTahunanDto(
                $subkegiatan->getTitle(),
                "subkegiatan",
                $tahun
            );
            $result[$tahun] = $subkSerapan;
        }
        foreach ($subkegiatan->getChildren() as $itemAnggaran) {
            $budget = $this->buildReportItem($itemAnggaran, $tahunAwal, $tahunAkhir);
            for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
                $result[$th]->addSumFromChildBudget($budget[$th]);
            }
        }
        // handle multi-year totals
        $sum = ['budget'=> 0 , 'serapan' => 0];
        for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
            $sum = $this->buildSumArray($sum, $result[$th]);
        }
        $result['sum'] = $sum;

        return $result;
    }

    private function buildSumArray(array $sum, SerapanTahunanDto $serapan) : array {
        // TODO implement half years

        $budget = $serapan->getBudget();
        $sum['budget'] = $serapan->getBudget();
        $total = 0;
        if (isset($sum['serapan'])) {
            $total = $sum['serapan'];
        }
        $total = $total + $serapan->getTotalSerapan();
        $sum['serapan'] = $total;
        if (($budget != null) && ($budget != 0)) {
            $sum['variance'] = 100 * $total / $budget;
        }
        else {
            $sum['variance'] = '';
        }
        return $sum;
    }

    /**
     * @param ItemAnggaran $itemAnggaran
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @return SerapanTahunanDto[]
     */
    private function buildReportItem(ItemAnggaran $itemAnggaran, int $tahunAwal, int $tahunAkhir) : array
    {
        $result = [];
        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {

            $itemSerapan = new SerapanTahunanDto(
                $itemAnggaran->getTitle(),
                "itemRab",
                $tahun
            );
            $itemSerapan->copyFromItemAnggaran($itemAnggaran, $tahun);
            $result[$tahun] = $itemSerapan;

        }
        return $result;
    }


    public function batchIncudesYear(BatchCsr $batch, int $tahunAwal, int $tahunAkhir) : bool {
        return ($batch->getTahunAwal() <= $tahunAkhir) || ($batch->getTahunAkhir() >= $tahunAwal);
    }

    public function kegiatanIncudesYear(Kegiatan $kegiatan, int $tahun) : bool {
        /** @var array $jadwal */
        $jadwal = $kegiatan->getJadwalKegiatan();
        if ($jadwal['awal'] && $jadwal['akhir']) {
            return ($this->getYearPart($jadwal['awal'] <= $tahun))
                && ($this->getYearPart($jadwal['akhir'] >= $tahun));
        }
        return false;
    }

    private function getYearPart(string $ymdDate) : int {
        $date = explode('-', $ymdDate);
        $year = $date[0];
        return (int)$year;
    }

    /**
     * Menyusun rekap dari dana global s/d dana batch
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @param AppUserInterface|null $user
     * @return array
     */

    public function buildRekap(int $tahunAwal, int $tahunAkhir, AppUserInterface $user = null) : array
    {
        return $this->buildReportGlobal($tahunAwal, $tahunAkhir, 1, 12, $user);
    }

     /**
      * Menyusun rekap dari dana bidang s/d dana batch
      * @param JenisProgramCSR $bidang
      * @param int $tahunAwal
      * @param int $tahunAkhir
      * @param AppUserInterface|null $user
      * @return array
      */

    public function buildRekapBidang(JenisProgramCSR $bidang, int $tahunAwal, int $tahunAkhir, AppUserInterface $user = null) : array
    {
        return $this->buildReportBidang($bidang, $tahunAwal, $tahunAkhir, 1, 12, $user);
    }



    /**
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @param int $bulanAwal
     * @param int $bulanAkhir
     * @param AppUserInterface|null $user
     * @return array
     */
    private function buildReportGlobal(int $tahunAwal, int $tahunAkhir,
                                       int $bulanAwal, int $bulanAkhir,
                                       AppUserInterface $user = null) : array
    {

        $query = $this->bidangCsrRepository->findAllByCriteria();
        $results = [];
        foreach ($query->getResult() as $bidang) {
            $bidangSerapan = $this->buildReportBidang($bidang, $tahunAwal, $tahunAkhir, $bulanAwal, $bulanAkhir, $user);
            foreach ($bidangSerapan as $item) {
                $results[] = $item;
            }
        }

        $globSerapan = [];
        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {
            $globSerapan[$tahun] = new SerapanTahunanDto(
                'Dana Global',
                "global",
                $tahun
            );
        }
        foreach ($results as $budget) {
            for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
                if ($budget[$th]->getLevel() == "bidang") {
                    $globSerapan[$th]->addSumFromChildBudget($budget[$th]);
                }
            }
        }
        array_unshift($results, $globSerapan);
        return $results;

    }


    /**
     * @param JenisProgramCSR $bidang
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @param int $bulanAwal
     * @param int $bulanAkhir
     * @param AppUserInterface|null $user
     * @return array
     */
    private function buildReportBidang(JenisProgramCSR $bidang,
                                        int $tahunAwal, int $tahunAkhir,
                                        int $bulanAwal, int $bulanAkhir,
                                        AppUserInterface $user = null) : array
    {
        $filters = [ new SortOrFilter("jenis.id", null, $bidang->getId())];

        $query = $this->programCsrRepository->findAllByCriteria($filters);
        $results = [];
        foreach ($query->getResult() as $program) {
            $programSerapan = $this->buildReportProgram($program, $tahunAwal, $tahunAkhir, $bulanAwal, $bulanAkhir, $user, true);
            foreach ($programSerapan as $item) {
                $results[] = $item;
            }
        }
        $bidSerapan = [];
        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {
            $bidSerapan[$tahun] = new SerapanTahunanDto(
                $bidang->getTitle(),
                "bidang",
                $tahun
            );
        }
        foreach ($results as $budget) {
            for ($th = $tahunAwal; $th <= $tahunAkhir; $th++) {
                if ($budget[$th]->getLevel() == "program") {
                    $bidSerapan[$th]->addSumFromChildBudget($budget[$th]);
                }
            }
        }
        array_unshift($results, $bidSerapan);
        return $results;
    }

    /**
     * Catatan tentang STRUKTUR DATA
     *
     * Output yang dikembalikan fungsi _buildReport_ dan _builsRekap adalah
     * array [0..n] di mana setiap barisnya adalah data multi-tahun
     *
     * data multi-tahun itu sendiri merupakan array [2018..2020] dari SerapanTahunanDto
     * untuk tiap tahun monitoring dan ['sum'] yang merupakan hitungan total dari
     * serapan multi-tahun tersebut
     *
     * Tiap SerapanTahunanDto diidentifikasi dengan
     *  - title, yaitu nama lingkup dana / kegiatan
     *  - level, yaitu posisi dalam struktur pendanaan
     *  - tahun,
     *  - flag, untuk informasi tambahan, misal: tahu di luar masa berlaku Dana Batch
     *
     */


}