<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Kegiatan;


use App\Component\DataObject\DataQuery;
use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\KendaliKegiatan;
use App\Entity\MasterData\ProgramCSR;
use App\Entity\Security\UserAccount;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\Kegiatan\KendaliKegiatanRepository;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;

/**
 * Class KendaliKegiatanModel
 *
 * @package App\Models\Kegiatan
 * @author  Mark Melvin
 * @since   17/7/2019
 */
final class KendaliKegiatanModel
{
    /**
     * @var KendaliKegiatanRepository
     */
    private $repository;

    /**
     * @var KegiatanRepository
     */
    private $kegiatanRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * KegiatanModel constructor.
     *
     * @param KendaliKegiatanRepository $repository Entity repository instance
     * @param KegiatanRepository $kegiatanRepository Entity repository instance
     * @param EntityManagerInterface $em Doctrine entity manager instance
     */
    public function __construct(KendaliKegiatanRepository $repository,
                                KegiatanRepository $kegiatanRepository,
                                EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->kegiatanRepository = $kegiatanRepository;
        $this->em = $em;
    }

    /**
     * Menampilkan daftar Kegiatan berdasarkan kriteria tertentu.
     *
     * @param DataQuery $dq Request query
     * @param UserAccount|AppUserInterface|null $user UserAccount yang sedang login
     *
     * @return Paginator
     */
    public function displayAllKegiatan(DataQuery $dq, AppUserInterface $user = null): Paginator
    {
        if (!is_null($user) && !is_null($user->getMitraCompany())) {
            $filters = $dq->getFilters();
            $filters[] = QueryExpressionHelper::createCriteria(['property' => 'periodeBatch.mitra',
                'value' => $user->getMitraCompany()]);
            $dq->setFilters($filters);
        }

        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
            $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @param ProgramCSR $program
     * @param int $tahun
     *
     * @return array
     */
    public function listByProgramYear(ProgramCSR $program, int $tahun) {

        $query = $this->repository->findAllByCriteria(
            [
                new SortOrFilter('periodeBatch.programCsr', null, $program),
                //new SortOrFilter('status', null, 'VERIFIED'),
                new SortOrFilter('danaBatch.tahun', null, $tahun),
            ],
            [],0, 0);
        $kks = $query->getResult();
        return $kks;

    }


    /**
     * save rencana progress kegiatan.
     *
     * @param Kegiatan $kegiatan Persistent entity object of Kegiatan
     * @param array $rencana Persistent entity object of KendaliKegiatan
     *
     * @return KendaliKegiatan|null
     */
    public function saveRencana(Kegiatan $kegiatan, array $rencana): ?KendaliKegiatan
    {
        $kegiatanId = $kegiatan->getKegiatanId(); // primary key adalah sama (1-to-1)

        $entity = $this->repository->find($kegiatanId);
        if (!isset($entity)) {
            // Menginisialisasi rencana kendali proyek.
            $entity = new KendaliKegiatan();
            $entity->setKegiatan($kegiatan);
        }

        // hanya meng-update bagian rencana
        $entity->setBobot($rencana['bobot']);
        $entity->setRencana1($rencana['rencana1']);
        $entity->setRencana2($rencana['rencana2']);
        $entity->setRencana3($rencana['rencana3']);
        $entity->setRencana4($rencana['rencana4']);
        $entity->setRencana5($rencana['rencana5']);
        $entity->setRencana6($rencana['rencana6']);
        $entity->setRencana7($rencana['rencana7']);
        $entity->setRencana8($rencana['rencana8']);
        $entity->setRencana9($rencana['rencana9']);
        $entity->setRencana10($rencana['rencana10']);
        $entity->setRencana11($rencana['rencana11']);
        $entity->setRencana12($rencana['rencana12']);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }


    /**
     * save realisasi progress kegiatan.
     *
     * @param Kegiatan $kegiatan Persistent entity object of Kegiatan
     * @param array $realisasi Persistent entity object of KendaliKegiatan
     *
     * @return KendaliKegiatan|null
     */
    public function saveRealisasi(Kegiatan $kegiatan, array $realisasi): ?KendaliKegiatan
    {
        $kegiatanId = $kegiatan->getKegiatanId(); // primary key adalah sama (1-to-1)

        $entity = $this->repository->find($kegiatanId);
        if (!isset($entity)) {
            // tidak ditemukan record rencana
            throw new LogicException(sprintf('Kegiatan <b>%s</b> belum memiliki ' .
                'rencana Kendali Proyek. Tidak dapat menyimpan Realisasi.',
                $kegiatan->getNamaKegiatan()));
        }

        // hanya meng-update bagian realisasi
        $entity->setRealisasi1($realisasi['realisasi1']);
        $entity->setRealisasi2($realisasi['realisasi2']);
        $entity->setRealisasi3($realisasi['realisasi3']);
        $entity->setRealisasi4($realisasi['realisasi4']);
        $entity->setRealisasi5($realisasi['realisasi5']);
        $entity->setRealisasi6($realisasi['realisasi6']);
        $entity->setRealisasi7($realisasi['realisasi7']);
        $entity->setRealisasi8($realisasi['realisasi8']);
        $entity->setRealisasi9($realisasi['realisasi9']);
        $entity->setRealisasi10($realisasi['realisasi10']);
        $entity->setRealisasi11($realisasi['realisasi11']);
        $entity->setRealisasi12($realisasi['realisasi12']);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }


}
