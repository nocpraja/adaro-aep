<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
*/


namespace App\Models\Kegiatan;


use App\Component\DataObject\SortOrFilter;
use App\Entity\MasterData\ProgramCSR;
use App\Repository\Pencairan\PencairanRepository;

class RiwayatPembayaranModel
{

    /** @var PencairanRepository */
    private $repository;

    /**
     * RiwayatPembayaranModel constructor.
     * @param PencairanRepository $repository
     */
    public function __construct(PencairanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function displayByProgramYear(ProgramCSR $program, int $tahun)
    {
        $query = $this->repository->findAllByProgramYear($program, $tahun);

        return $query->getResult();
    }
}