<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Kegiatan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Kegiatan\ItemAnggaran;
use App\Entity\Kegiatan\ItemRealisasiRab;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\LogApprovalKegiatan;
use App\Entity\Kegiatan\Subkegiatan;
use App\Entity\Security\UserAccount;
use App\Models\Pendanaan\DanaBatchModel;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\Kegiatan\ItemAnggaranRepository;
use App\Repository\Kegiatan\ItemRealisasiRabRepository;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\Kegiatan\LogApprovalKegiatanRepository;
use App\Repository\Kegiatan\SubkegiatanRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use DateTime;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use LogicException;

/**
 * Class KegiatanModel
 * @package App\Models\Kegiatan
 * @author  Ahmad Fajar
 * @since   13/11/2018, modified: 12/10/2019 22:13
 */
final class KegiatanModel
{

    /**
     * @var KegiatanRepository
     */
    private $repository;

    /**
     * @var SubkegiatanRepository
     */
    private $subkegiatanRepository;

    /**
     * @var ItemAnggaranRepository
     */
    private $itemRabRepository;

    /**
     * @var LogApprovalKegiatanRepository
     */
    private $approvalKegiatanRepository;

    /**
     * @var DanaBatchModel
     */
    private $danaBatchModel;

    /**
     * @var WorkflowConfigurer
     */
    private $workflowConfigurer;

    /**
     * @var AuditLogger
     */
    private $logger;
    /**
     * @var ItemRealisasiRabRepository
     */
    private $realisasiRabRepository;


    /**
     * KegiatanModel constructor.
     *
     * @param KegiatanRepository            $repository             Entity repository instance
     * @param SubkegiatanRepository         $subkegiatanRepository  Entity repository instance
     * @param ItemAnggaranRepository        $anggaranRepository     Entity repository instance
     * @param ItemRealisasiRabRepository    $realisasiRabRepository Entity repository instance
     * @param LogApprovalKegiatanRepository $approvalKegiatanRepository
     * @param WorkflowConfigurer            $configurer             Pengelolaan Kegiatan workflow configurator
     * @param DanaBatchModel                $danaBatchModel         Dana Batch model
     */
    public function __construct(KegiatanRepository $repository,
                                SubkegiatanRepository $subkegiatanRepository,
                                ItemAnggaranRepository $anggaranRepository,
                                ItemRealisasiRabRepository $realisasiRabRepository,
                                LogApprovalKegiatanRepository $approvalKegiatanRepository,
                                WorkflowConfigurer $configurer,
                                DanaBatchModel $danaBatchModel)
    {
        $this->repository = $repository;
        $this->subkegiatanRepository = $subkegiatanRepository;
        $this->itemRabRepository = $anggaranRepository;
        $this->realisasiRabRepository = $realisasiRabRepository;
        $this->approvalKegiatanRepository = $approvalKegiatanRepository;
        $this->workflowConfigurer = $configurer;
        $this->danaBatchModel = $danaBatchModel;
        $this->logger = $danaBatchModel->getLogger();
    }

    /**
     * Proses workflow transition pada Rencana Kegiatan.
     *
     * @param Kegiatan     $entity Persistent entity object
     * @param ActionObject $data   Transient data object
     */
    public function actionTransition(Kegiatan $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();
        $transition = $data->getAction();
        $context = ['status' => $entity->getStatus(), 'keterangan' => $data->getMessage()];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum bisa dilakukan.');
        }

        $workflow->apply($entity, $transition, $context);
    }

    /**
     * Memeriksa apakah item RAB masih dapat diperbarui ataupun dihapus atau tidak.
     *
     * @param ItemAnggaran $entity Persistent entity object of item RAB
     *
     * @return bool TRUE jika masih dapat diperbarui ataupun dihapus, dan FALSE jika sebaliknya
     */
    public function canUpdateItemRab(ItemAnggaran $entity): bool
    {
        return $this->canUpdateKegiatan($entity->getParent()->getKegiatan());
    }

    /**
     * Memeriksa apakah Kegiatan masih dapat diperbarui ataupun dihapus atau tidak.
     *
     * @param Kegiatan $entity Persistent entity object of Subkegiatan
     *
     * @return bool TRUE jika masih dapat diperbarui ataupun dihapus, dan FALSE jika sebaliknya
     */
    public function canUpdateKegiatan(Kegiatan $entity): bool
    {
        return $this->workflowConfigurer->getWorkflow()->can($entity, WorkflowTags::STEP_UPDATE);
    }

    /**
     * Memeriksa apakah Subkegiatan masih dapat diperbarui ataupun dihapus atau tidak.
     *
     * @param Subkegiatan $entity Persistent entity object of Subkegiatan
     *
     * @return bool TRUE jika masih dapat diperbarui ataupun dihapus, dan FALSE jika sebaliknya
     */
    public function canUpdateSubkegiatan(Subkegiatan $entity): bool
    {
        return $this->canUpdateKegiatan($entity->getKegiatan());
    }

    /**
     * Menambahkan data rencana item RAB baru.
     *
     * @param ItemAnggaran $entity Unmanaged entity object
     *
     * @throws ORMException
     */
    public function createItemRab(ItemAnggaran $entity): void
    {
        $em = $this->logger->getEntityManager();
        $kegiatan = $entity->getParent()->getKegiatan();
        $mgtFee = $kegiatan->getPeriodeBatch()->getManagementFee();
        $title = 'TAMBAH entri RAB: ' . $entity->getTitle();

        $entity->setMgtFeeConst($mgtFee)
            ->setAnggaran($this->calcNilaiRab($entity, $mgtFee));
        $em->persist($entity);
        $em->flush();

        $this->logger->info('TAMBAH record rencana Item RAB baru.', ItemAnggaran::class,
            $entity, [JsonController::CONTEXT_NON_REL]);

        $this->calcAnggaranSubkegiatan($entity->getParent());
        $this->danaBatchModel->appendTransaction($kegiatan->getDanaBatch(), ($entity->getAnggaran() * -1),
            $kegiatan->getKode(), $title, true);
    }

    /**
     * Menambahkan data rencana item RAB dan realisasi baru.
     *
     * @param ItemAnggaran $entity Unmanaged entity object
     * @param              $request
     *
     * @throws ORMException
     * @throws Exception
     */
    public function createItemRabRealisasi(ItemAnggaran $entity, $request): void
    {
        $em = $this->logger->getEntityManager();
        $kegiatan = $entity->getParent()->getKegiatan();
        $mgtFee = $kegiatan->getPeriodeBatch()->getManagementFee();
        $title = 'TAMBAH entri RAB dan Realisasi: ' . $entity->getTitle();

        $realisasi = new ItemRealisasiRab();
        $obj = $request->request->get('realisasiRabs');
        $realisasi->setParentRab($entity)
            ->setKegiatanRab($entity->getParent()->getKegiatan()->getKegiatanId())
            ->setSubRab($entity->getParent()->getSid())
            ->setRealisasiDate(new DateTime($obj['realisasiDate']))
            ->setRealisasi($obj['realisasi'])
            ->setStatus(WorkflowTags::REALISASI_CREATED);

        $entity->setMgtFeeConst($mgtFee)
            ->setAnggaran($this->calcNilaiRab($entity, $mgtFee));

        $em->persist($entity);
        //prevent to insert item realisasi to table item_realisasi
        //$em->persist($realisasi);
        $em->flush();

        $this->logger->info('TAMBAH record item RAB dan Realisasi baru.', ItemAnggaran::class,
            $entity, [JsonController::CONTEXT_NON_REL]);

        $this->calcAnggaranSubkegiatan($entity->getParent());
        $this->danaBatchModel->appendTransaction($kegiatan->getDanaBatch(), ($entity->getAnggaran() * -1),
            $kegiatan->getKode(), $title, true);
    }

    /**
     * Menambahkan data realisasi per item RAB.
     *
     * @param ItemRealisasiRab $entity Unmanaged entity object
     */
    public function createItemRealisasi(ItemRealisasiRab $entity): void
    {
        $existing = $this->getRealisasiRabRepository()->findByDate($entity->getRealisasiDate()->format('Y-m-d'), $entity->getParentRab()->getId());
        if (count($existing) > 0) {
            throw new LogicException(sprintf('Realisasi RAB bulan <b>%s</b> sudah pernah dientri. Silahkan entri realisasi bulan lainnya.', $entity->getRealisasiDate()->format('m/Y')));
        }

        $em = $this->logger->getEntityManager();
        $entity->setKegiatanRab($entity->getParentRab()->getParent()->getKegiatan()->getKegiatanId())
            ->setSubRab($entity->getParentRab()->getParent()->getSid())
            ->setParentRab($entity->getParentRab())
            ->setStatus(WorkflowTags::REALISASI_CREATED);
        $em->persist($entity);
        $em->flush();

        $this->logger->info('TAMBAH record Realisasi RAB baru.', ItemRealisasiRab::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Menambahkan data rencana kegiatan baru.
     *
     * @param Kegiatan $entity Unmanaged entity object
     *
     * @throws ORMException
     */
    public function createKegiatan(Kegiatan $entity): void
    {
        $em = $this->logger->getEntityManager();
        $numberingRepository = $this->danaBatchModel->getNumberingRepository();
        $batch = $entity->getDanaBatch();

        if (!is_null($batch)) {
            $numbering = $numberingRepository->nextSequence('Pendanaan Kegiatan',
                'PK-' . substr($batch->getTahun(), -2));
        } else {
            $numbering = $numberingRepository->nextSequence('Pendanaan Kegiatan', 'PK-' . date('y'));
        }

        $entity->setKode($numbering->toStringCode(4))
            ->setStatus(WorkflowTags::NEW);
        $em->persist($entity);
        $em->flush();

        $this->logger->info('TAMBAH record rencana Kegiatan baru.', Kegiatan::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * @return ItemRealisasiRabRepository
     */
    public function getRealisasiRabRepository(): ItemRealisasiRabRepository
    {
        return $this->realisasiRabRepository;
    }

    /**
     * Menambahkan data rencana Subkegiatan baru.
     *
     * @param Subkegiatan $entity Unmanaged entity object
     */
    public function createSubkegiatan(Subkegiatan $entity): void
    {
        $parent = $entity->getKegiatan();
        if ($parent->getStatus() == 'VERIFIED') {
            $entity->setIsRealisasi(true);
        }

        $em = $this->logger->getEntityManager();
        $em->persist($entity);
        $em->flush();

        $this->collectJadwalKegiatan($parent);

        $this->logger->info('TAMBAH record rencana Subkegiatan baru.', Subkegiatan::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Proses hapus data rencana item RAB.
     *
     * @param ItemAnggaran $entity Persistent entity object
     *
     * @throws LogicException
     * @throws ORMException
     */
    public function deleteItemRab(ItemAnggaran $entity): void
    {
        if (!$this->canUpdateItemRab($entity) || $entity->getParent()->isClosed()) {
            throw new LogicException(sprintf('Entri RAB <b>%s</b> telah dikunci. ' .
                'Proses penghapusan entri sudah tidak diperkenankan lagi.',
                $entity->getTitle()));
        }

        $anggaran = $entity->getAnggaran();
        $title = 'HAPUS entri RAB: ' . $entity->getTitle();
        $parent = $entity->getParent();
        $kegiatan = $parent->getKegiatan();
        $this->itemRabRepository->remove($entity);

        $this->logger->notice(sprintf('HAPUS record item RAB: %s.', $entity->getTitle()),
            ItemAnggaran::class, $entity, [JsonController::CONTEXT_NON_REL]);
        $this->danaBatchModel->appendTransaction($kegiatan->getDanaBatch(), $anggaran, $kegiatan->getKode(),
            $title, true);
        $this->calcAnggaranSubkegiatan($parent);
    }

    /**
     * Proses hapus rencana kegiatan.
     *
     * @param Kegiatan $entity Persistent entity object
     *
     * @throws LogicException
     * @throws ORMException
     */
    public function deleteKegiatan(Kegiatan $entity): void
    {
        if (!$this->canUpdateKegiatan($entity)) {
            throw new LogicException(sprintf('Kegiatan <b>%s</b> telah dikunci. ' .
                'Proses penghapusan data sudah tidak diperkenankan lagi.',
                $entity->getNamaKegiatan()));
        }
        if ($entity->hasChildren() || $entity->isSubmitted()) {
            throw new LogicException(sprintf('Kegiatan <b>%s</b> tidak boleh dihapus, ' .
                'masih ada subkegiatan yang terkait dengannya.',
                $entity->getNamaKegiatan()));
        }

        $title = 'HAPUS ' . $entity->getNamaKegiatan();
        $anggaran = $entity->getAnggaran();
        $this->repository->remove($entity);

        $this->logger->notice(sprintf('HAPUS record Kegiatan: %s.', $entity->getNamaKegiatan()),
            Kegiatan::class, $entity, [JsonController::CONTEXT_NON_REL]);
        $this->danaBatchModel->appendTransaction($entity->getDanaBatch(), $anggaran, $entity->getKode(),
            $title, true);
    }

    /**
     * Proses hapus data rencana Subkegiatan.
     *
     * @param Subkegiatan $entity Persistent entity object
     *
     * @throws LogicException
     * @throws ORMException
     */
    public function deleteSubkegiatan(Subkegiatan $entity): void
    {
        if (!$this->canUpdateSubkegiatan($entity) || $entity->isClosed()) {
            throw new LogicException(sprintf('Subkegiatan <b>%s</b> telah dikunci. ' .
                'Proses penghapusan data sudah tidak diperkenankan lagi.',
                $entity->getTitle()));
        }
        if ($entity->hasChildren()) {
            throw new LogicException(sprintf('Subkegiatan <b>%s</b> tidak boleh dihapus, ' .
                'masih ada entri RAB yang terkait dengannya.', $entity->getTitle()));
        }

        $anggaran = $entity->getAnggaran();
        $title = 'HAPUS Subkegiatan: ' . $entity->getTitle();
        $kegiatan = $entity->getKegiatan();
        $this->subkegiatanRepository->remove($entity);

        $this->logger->notice(sprintf('HAPUS record Subkegiatan: %s.', $entity->getTitle()),
            Subkegiatan::class, $entity, [JsonController::CONTEXT_NON_REL]);
        $this->danaBatchModel->appendTransaction($kegiatan->getDanaBatch(), $anggaran, $kegiatan->getKode(),
            $title, true);

        $this->collectJadwalKegiatan($kegiatan);
        $this->calcAnggaranKegiatan($kegiatan);
    }

    /**
     * Menampilkan daftar Kegiatan berdasarkan kriteria tertentu.
     *
     * @param DataQuery                         $dq   Request query
     * @param UserAccount|AppUserInterface|null $user UserAccount yang sedang login
     *
     * @return Paginator
     */
    public function displayAllKegiatan(DataQuery $dq, AppUserInterface $user = null): Paginator
    {
        if (!is_null($user) && !is_null($user->getMitraCompany())) {
            $filters = $dq->getFilters();
            $filters[] = QueryExpressionHelper::createCriteria(['property' => 'periodeBatch.mitra',
                'value' => $user->getMitraCompany()]);
            $dq->setFilters($filters);
        }

        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
            $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Menampilkan daftar Subkegiatan berdasarkan kriteria tertentu.
     *
     * @param DataQuery $dq Request query
     *
     * @return Paginator
     */
    public function displayAllSubkegiatan(DataQuery $dq): Paginator
    {
        $query = $this->subkegiatanRepository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
            $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Menampilkan histori approval suatu Kegiatan.
     *
     * @param Kegiatan $kegiatan Persistent entity object
     *
     * @return LogApprovalKegiatan[]
     */
    public function fetchLogApproval(Kegiatan $kegiatan): array
    {
        return $this->approvalKegiatanRepository->findBy(['kegiatan' => $kegiatan], ['postedDate' => 'desc']);
    }

    /**
     * @return DanaBatchRepository
     */
    public function getDanaBatchRepository(): DanaBatchRepository
    {
        return $this->danaBatchModel->getRepository();
    }

    /**
     * @return ItemAnggaranRepository
     */
    public function getItemRabRepository(): ItemAnggaranRepository
    {
        return $this->itemRabRepository;
    }

    /**
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * @return KegiatanRepository
     */
    public function getRepository(): KegiatanRepository
    {
        return $this->repository;
    }

    /**
     * @return SubkegiatanRepository
     */
    public function getSubkegiatanRepository(): SubkegiatanRepository
    {
        return $this->subkegiatanRepository;
    }

    /**
     * Proses simpan pembaruan data rencana item RAB.
     *
     * @param ItemAnggaran $entity Persistent entity object
     *
     * @throws LogicException
     * @throws ORMException
     */
    public function updateItemRab(ItemAnggaran $entity): void
    {
        if (!$this->canUpdateItemRab($entity)) {
            throw new LogicException(sprintf('Entri RAB <b>%s</b> telah dikunci. ' .
                'Pembaruan entri sudah tidak diperkenankan lagi.',
                $entity->getTitle()));
        }

        $workflow = $this->workflowConfigurer->getWorkflow();
        $kegiatan = $entity->getParent()->getKegiatan();
        $oldAnggaran = $entity->getParent()->getAnggaran();
        $mgtFee = $kegiatan->getPeriodeBatch()->getManagementFee();

        $entity->setMgtFeeConst($mgtFee)
            ->setAnggaran($this->calcNilaiRab($entity, $mgtFee));
        $this->itemRabRepository->save($entity);

        $this->logger->info('UPDATE record rencana Item RAB.', ItemAnggaran::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
        $newAnggaran = $this->calcAnggaranSubkegiatan($entity->getParent());

        $selisih = $newAnggaran - $oldAnggaran;
        if ($selisih != 0.0) {
            $title = 'UPDATE entri RAB: ' . $entity->getTitle();
            $this->danaBatchModel->appendTransaction($kegiatan->getDanaBatch(), ($selisih * -1),
                $kegiatan->getKode(), $title, true);
        }

        $workflow->apply($kegiatan, WorkflowTags::STEP_UPDATE, [], $this->logger->getEntityManager());
    }

    /**
     * Proses simpan pembaruan data realisasi item RAB.
     *
     * @param ItemRealisasiRab $entity Persistent entity object
     */
    public function updateRealisasi(ItemRealisasiRab $entity): void
    {
//
    }

    /**
     * Proses simpan pembaruan data realisasi item RAB.
     *
     * @param Kegiatan $entity Persistent entity object
     * @param string   $status
     */
    public function updateKegiatanRealisasi(Kegiatan $entity, string $status): void
    {
        $em = $this->logger->getEntityManager();
        $entity->setStatus($status);
        $em->persist($entity);
        $em->flush();

        $this->logger->info('UPDATE status realisasi Kegiatan.', Kegiatan::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Proses simpan pembaruan rencana kegiatan.
     *
     * @param Kegiatan $entity Persistent entity object
     *
     * @throws LogicException
     */
    public function updateKegiatan(Kegiatan $entity): void
    {
        if (!$this->canUpdateKegiatan($entity)) {
            throw new LogicException(sprintf('Kegiatan <b>%s</b> telah dikunci. ' .
                'Pembaruan data sudah tidak diperkenankan lagi.',
                $entity->getNamaKegiatan()));
        }

        $this->collectJadwalKegiatan($entity);
        $workflow = $this->workflowConfigurer->getWorkflow();
        $workflow->apply($entity, WorkflowTags::STEP_UPDATE, []);

        $this->logger->info('UPDATE record rencana Kegiatan.', Kegiatan::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Update realisasi anggaran
     *
     * @param Subkegiatan|null $entity Persistent entity object of Subkegiatan
     *
     * @throws LogicException
     * @throws Exception
     */
    public function updateRealisasiSubkegiatan(?Subkegiatan $entity): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();

        if (!$workflow->can($entity->getKegiatan(), WorkflowTags::STEP_SUBMIT_REALISASI)) {
            throw new LogicException(sprintf('Update realisasi anggaran Subkegiatan <b>%s</b> belum diperkenankan.',
                $entity->getTitle()));
        }

        $em = $this->logger->getEntityManager();
        $entity->setTanggalPostedRealisasi(new DateTime());
        $em->persist($entity);
        $em->flush();

        $this->logger->info('UPDATE realisasi anggaran Subkegiatan.', Subkegiatan::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Proses simpan pembaruan data rencana Subkegiatan.
     *
     * @param Subkegiatan $entity Persistent entity object
     *
     * @throws LogicException
     */
    public function updateSubkegiatan(Subkegiatan $entity): void
    {
        if (!$this->canUpdateSubkegiatan($entity)) {
            throw new LogicException(sprintf('Subkegiatan <b>%s</b> telah dikunci. ' .
                'Pembaruan subkegiatan sudah tidak diperkenankan lagi.',
                $entity->getTitle()));
        }

        $em = $this->logger->getEntityManager();
        $workflow = $this->workflowConfigurer->getWorkflow();

        $em->persist($entity);
        $em->flush();

        $kegiatan = $entity->getKegiatan();
        $this->collectJadwalKegiatan($kegiatan);
        $workflow->apply($kegiatan, WorkflowTags::STEP_UPDATE, []);

        $this->logger->info('UPDATE record rencana Subkegiatan.', Subkegiatan::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Calculate nilai anggaran suatu Kegiatan.
     *
     * @param Kegiatan $entity Persistent entity object
     */
    private function calcAnggaranKegiatan(Kegiatan $entity): void
    {
        $total = 0.0;
        $children = $entity->getChildren();
        $em = $this->logger->getEntityManager();

        if ($children->count() > 0) {
            foreach ($children as $child) {
                $total += $child->getAnggaran();
            }
        }

        $entity->setAnggaran($total);
        $em->persist($entity);
        $em->flush();
    }

    /**
     * Calculate nilai anggaran Subkegiatan.
     *
     * @param Subkegiatan $entity Persistent entity object
     *
     * @return float Nilai anggaran Subkegiatan
     */
    private function calcAnggaranSubkegiatan(Subkegiatan $entity): float
    {
        $total = 0.0;
        $children = $entity->getChildren();
        $em = $this->logger->getEntityManager();

        if ($children->count() > 0) {
            foreach ($children as $child) {
                $total += $child->getAnggaran();
            }
        }

        $entity->setAnggaran($total);
        $em->persist($entity);
        $em->flush();

        $this->calcAnggaranKegiatan($entity->getKegiatan());

        return $total;
    }

    /**
     * Menghitung nilai entri RAB berdasarkan rumus tertentu.
     *
     * @param ItemAnggaran $entity Persistent entity object
     * @param float        $mgtfee Persentase management fee
     *
     * @return float Nilai total anggaran item RAB
     */
    private function calcNilaiRab(ItemAnggaran $entity, float $mgtfee): float
    {
        $total = $entity->getJumlahQty1() * $entity->getJumlahQty2() * $entity->getUnitPrice() * $entity->getFrequency();

        if (!$entity->isFixedCost()) {
            return ($total * ($mgtfee / 100)) + $total;
        }

        return $total;
    }

    /**
     * Collect jadwal pelaksanaan kegiatan dari setiap Subkegiatan
     * dan simpan datanya pada entity Kegiatan.
     *
     * @param Kegiatan $entity Persistent entity object
     */
    private function collectJadwalKegiatan(Kegiatan $entity): void
    {
        $children = $entity->getChildren();

        if ($children->count() > 0) {
            $tglAwal = null;
            $tglAkhir = null;

            foreach ($children as $child) {
                if ($tglAwal == null) {
                    $tglAwal = $child->getTanggalMulai();
                }
                if ($tglAkhir == null) {
                    $tglAkhir = $child->getTanggalAkhir();
                }
                if ($tglAwal >= $child->getTanggalMulai()) {
                    $tglAwal = $child->getTanggalMulai();
                }
                if ($tglAkhir <= $child->getTanggalAkhir()) {
                    $tglAkhir = $child->getTanggalAkhir();
                }
            }

            $jadwal = [
                'awal' => $tglAwal->format('Y-m-d'),
                'akhir' => $tglAkhir->format('Y-m-d')
            ];
            $entity->setJadwalKegiatan($jadwal);
        } else {
            $entity->setJadwalKegiatan(null);
        }
    }

    public function updateStatusKegiatan(int $idKegiatan, string $status)
    {
        $em = $this->logger->getEntityManager();
        $kegiatan = $this->repository->find($idKegiatan);
        $kegiatan->setStatus($status);
        $kegiatan->setLastUpdated(new \DateTime());
        $em->persist($kegiatan);
        $em->flush();
    }

}
