<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\MasterData;

use App\Component\Controller\JsonController;
use App\Component\DataObject\SortOrFilter;
use App\Entity\MasterData\Mitra;
use App\Repository\MasterData\KecamatanRepository;
use App\Repository\MasterData\KelurahanRepository;
use App\Repository\MasterData\MitraRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class MitraModel
 *
 * @package App\Models\MasterData
 * @author  Tri N
 * @since   24/01/2019, modified: 24/01/2019 14:55
 */
final class MitraModel
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MitraRepository
     */
    private $repository;

    /**
     * @var ProgramCSRRepository
     */
    private $programCSRRepository;

    /**
     * @var KecamatanRepository
     */
    private $kecamatanRepository;

    /**
     * @var KelurahanRepository
     */
    private $kelurahanRepository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * MitraModel constructor.
     *
     * @param EntityManagerInterface $entityManager        Doctrine entity manager
     * @param MitraRepository        $repository           Entity repository instance
     * @param ProgramCSRRepository   $programCSRRepository Entity repository instance
     * @param KecamatanRepository    $kecamatanRepository  Entity repository instance
     * @param KelurahanRepository    $kelurahanRepository  Entity repository instance
     * @param AuditLogger            $logger               AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                MitraRepository $repository,
                                ProgramCSRRepository $programCSRRepository,
                                KecamatanRepository $kecamatanRepository,
                                KelurahanRepository $kelurahanRepository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->programCSRRepository = $programCSRRepository;
        $this->kecamatanRepository = $kecamatanRepository;
        $this->kelurahanRepository = $kelurahanRepository;
        $this->logger = $logger;
    }

    /**
     * Remove Data Mitra
     *
     * @param Mitra $entity Persistent entity object
     */
    public function delete(Mitra $entity): void
    {
        $this->em->remove($entity);
        $this->logger->notice('HAPUS record Mitra.', Mitra::class,
                              $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Manampilkan daftar Mitra
     *
     * @param SortOrFilter[] $filters Filter kriteria
     * @param SortOrFilter[] $sorts   Sort kriteria
     * @param integer        $limit   Jumlah record untuk ditampilkan
     * @param integer        $offset  Posisi record awal
     *
     * @return Paginator
     */
    public function displayAllMitra(array $filters, array $sorts, int $limit, int $offset): Paginator
    {
        $query = $this->repository->findAllByCriteria($filters, $sorts, $limit, $offset);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Get Doctrine Entity Manager
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * Get Mitra Repository
     *
     * @return MitraRepository
     */
    public function getRepository(): MitraRepository
    {
        return $this->repository;
    }

    /**
     * Get ProgramCSR Repository
     *
     * @return ProgramCSRRepository
     */
    public function getProgramCSRRepository(): ProgramCSRRepository
    {
        return $this->programCSRRepository;
    }

    /**
     * Get Kecamatan Repository
     *
     * @return KecamatanRepository
     */
    public function getKecamatanRepository(): KecamatanRepository
    {
        return $this->kecamatanRepository;
    }

    /**
     * Get Kelurahan Repository
     *
     * @return KelurahanRepository
     */
    public function getKelurahanRepository(): KelurahanRepository
    {
        return $this->kelurahanRepository;
    }

    /**
     * Get Audit Logger Service
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * Simpan Data Mitra
     *
     * @param Mitra $entity Persistent entity object
     */
    public function save(Mitra $entity): void
    {
        $fts = [];
        if (!empty($entity->getKodeLembaga())) {
            $fts[] = $entity->getKodeLembaga();
        }
        if (!empty($entity->getNamaLembaga())) {
            $fts[] = $entity->getNamaLembaga();
        }
        if (!empty($entity->getJenisLembaga())) {
            $fts[] = $entity->getJenisLembaga();
        }
        if (!empty($entity->getAlamat())) {
            $fts[] = $entity->getAlamat();
        }
        if (!empty($entity->getAktePendirian())) {
            $fts[] = $entity->getAktePendirian();
        }
        if (!empty($entity->getEmail())) {
            $fts[] = $entity->getEmail();
        }
        if (!empty($entity->getWebsite())) {
            $fts[] = $entity->getWebsite();
        }
        if (!empty($entity->getNamaPimpinan())) {
            $fts[] = $entity->getNamaPimpinan();
        }
        if (!empty($entity->getPicProgram())) {
            $fts[] = $entity->getPicProgram();
        }

        $kelurahan = $entity->getKelurahan();
        $kecamatan = $entity->getKecamatan();
        $kabupaten = $kecamatan->getKabupaten();
        $provinsi = $kabupaten->getProvinsi();

        $fts[] = $provinsi->getNamaProvinsi();
        $fts[] = $kabupaten->getNamaKabupaten();
        $fts[] = $kecamatan->getNamaKecamatan();
        $fts[] = $kelurahan->getNamaKelurahan();

        $entity->setSearchFts($fts);

        $this->em->persist($entity);
        $this->em->flush();
    }
}