<?php
/**
 * Description: DashboardModel.php generated by PhpStorm.
 * @project     adaro-aep
 * @package     App\Models\MasterData\KonfigurasiKpi
 * @author      alex
 * @created     2020-05-18, modified: 2020-05-18 06:24
 * @copyright   Copyright (c) 2020
 */

namespace App\Models\MasterData\KonfigurasiKpi;


use App\Repository\MasterData\KonfigurasiKpi\DashboardRepository;

final class DashboardModel
{
    /**
     * @var DashboardRepository
     */
    private $repository;

    /**
     * DashboardModel constructor.
     *
     * @param DashboardRepository $repository
     */
    public function __construct(DashboardRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return DashboardRepository
     */
    public function getRepository(): DashboardRepository
    {
        return $this->repository;
    }


}
