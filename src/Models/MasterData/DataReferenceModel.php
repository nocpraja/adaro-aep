<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\MasterData;


use App\Component\Controller\JsonController;
use App\Component\DataObject\SortOrFilter;
use App\Entity\MasterData\DataReference;
use App\Repository\MasterData\DataReferenceRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class DataReferenceModel
 *
 * @package App\Models\MasterData
 * @author  Tri N
 * @since   07/02/2019, modified: 11/02/2019 4:26
 */
final class  DataReferenceModel
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var DataReferenceRepository
     */
    private $repository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * DataReferenceModel constructor.
     *
     * @param EntityManagerInterface  $entityManager Doctrine entity manager
     * @param DataReferenceRepository $repository    Entity repository instance
     * @param AuditLogger             $logger        AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                DataReferenceRepository $repository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * Remove DataReference
     *
     * @param DataReference $entity Persistent entity object
     * @throws \LogicException
     */
    public function delete(DataReference $entity): void
    {
        if ($entity->getProtected()) {
            throw new \LogicException('Record diproteksi, tidak boleh dihapus.');
        }

        $this->em->remove($entity);
        $this->logger->notice('HAPUS record Data Referensi.', DataReference::class,
                              $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Manampilkan daftar DataReference
     *
     * @param SortOrFilter[] $filters Filter kriteria
     * @param SortOrFilter[] $sorts   Sort kriteria
     * @param integer        $limit   Jumlah record untuk ditampilkan
     * @param integer        $offset  Posisi record awal
     *
     * @return Paginator
     */
    public function displayAllDataRef(array $filters, array $sorts, int $limit, int $offset): Paginator
    {
        $query = $this->repository->findAllByCriteria($filters, $sorts, $limit, $offset);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Get Doctrine Entity Manager
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * Get DataReference Repository
     *
     * @return DataReferenceRepository
     */
    public function getRepository(): DataReferenceRepository
    {
        return $this->repository;
    }

    /**
     * Get Audit Logger Service
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * Proses simpan DataReference
     *
     * @param DataReference $entity Persistent entity object
     */
    public function save(DataReference $entity): void
    {
        $this->updateParams($entity);
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Assign or update list params dari DataReference
     *
     * @param DataReference $dataReference Persistent entity object
     */
    private function updateParams(DataReference $dataReference): void
    {
        $srcParams = $dataReference->getParams();

        if (empty($srcParams)) {
            $dataReference->setParams(null);
        } else {
            $dstParams = [];

            foreach ($srcParams as $param) {
                $property = trim($param['property']);
                $value = trim($param['value']);
                $type = trim($param['type']);

                if (!empty($property) && !empty($value)) {
                    $dstParams[] = [
                        'property' => $property,
                        'value'    => $value,
                        'type'     => $type
                    ];
                }
            }
            if (!empty($dstParams)) {
                $dataReference->setParams($dstParams);
            } else {
                $dataReference->setParams(null);
            }
        }
    }
}