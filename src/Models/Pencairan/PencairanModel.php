<?php


namespace App\Models\Pencairan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Kegiatan\ItemAnggaran;
use App\Entity\Kegiatan\ItemRealisasiRab;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\Subkegiatan;
use App\Entity\MasterData\Mitra;
use App\Entity\Pencairan\ItemPencairan;
use App\Entity\Pencairan\Pencairan;
use App\Entity\Security\UserAccount;
use App\Repository\Kegiatan\ItemRealisasiRabRepository;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\Kegiatan\SubkegiatanRepository;
use App\Repository\Pencairan\PencairanRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use App\Service\Uploadable\FileHelper;
use App\Service\Uploadable\ImageTransform;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PencairanModel
{

    /** @var PencairanRepository */
    private $repository;
    /** @var AuditLogger */
    private $logger;
    /** @var WorkflowConfigurer */
    private $workflowConfigurer;
    /** @var FileHelper */
    private $filesystem;
    /** @var KegiatanRepository */
    private $kegiatanRepository;
    /** @var SubkegiatanRepository */
    private $subkegiatanRepository;
    /** @var ItemRealisasiRabRepository */
    private $itemRealisasiRepository;
    /** @var EntityManagerInterface */
    private $em;

    /**
     * PencairanModel constructor.
     * @param PencairanRepository $repository
     * @param AuditLogger $logger
     * @param WorkflowConfigurer $workflowConfigurer
     * @param FileHelper $filesystem
     * @param KegiatanRepository $kegiatanRepository
     * @param SubkegiatanRepository $subkegiatanRepository
     * @param ItemRealisasiRabRepository $itemRealisasiRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(PencairanRepository $repository, AuditLogger $logger, WorkflowConfigurer $workflowConfigurer, FileHelper $filesystem, KegiatanRepository $kegiatanRepository, SubkegiatanRepository $subkegiatanRepository, ItemRealisasiRabRepository $itemRealisasiRepository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->logger = $logger;
        $this->workflowConfigurer = $workflowConfigurer;
        $this->filesystem = $filesystem;
        $this->kegiatanRepository = $kegiatanRepository;
        $this->subkegiatanRepository = $subkegiatanRepository;
        $this->itemRealisasiRepository = $itemRealisasiRepository;
        $this->em = $em;
    }


    /**
     * Proses workflow transition pada Pencairan Dana Kegiatan.
     *
     * @param Pencairan  $entity Persistent entity object
     * @param ActionObject $data   Transient data object
     */
    public function actionTransition(Pencairan $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();
        $transition = $data->getAction();
        $context = ['status' => $entity->getStatus(), 'keterangan' => $data->getMessage()];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum bisa dilakukan.'.
            '[Status = '. $entity->getStatus().', Action = '.$transition.' ]');
        }

        $workflow->apply($entity, $transition, $context);
    }

    /**
     * Menampilkan daftar pencairan berdasarkan kriteria tertentu.
     *
     * @param DataQuery                         $dq   Request query
     * @param UserAccount|AppUserInterface|null $user UserAccount yang sedang login
     *
     * @return Paginator
     */
    public function displayAllByCriteria(DataQuery $dq, ?AppUserInterface $user): Paginator
    {
        $filters = [];
        if (!is_null($user) && !is_null($user->getMitraCompany())) {
            $filters[] = QueryExpressionHelper::createCriteria(['property' => 'mitra', 'value' => $user->getMitraCompany()]);
        }

        $dq->setFilters($filters);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
            $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Membuat pembayaran baru dengan status created
     *
     * @param bool $isExpense
     * @param bool $isPelunasan
     * @param UserAccount $user
     * @return Pencairan
     * @throws LogicException
     * @throws \Exception
     */
    public function createPencairan(bool $isExpense, bool $isPelunasan, UserAccount $user) : Pencairan {
        /** @var Mitra $mitra */
        $mitra = $user->getMitraCompany();
        if ($mitra != null) {
            $kodeMitra = $mitra->getKodeLembaga();
        } else {
            throw new LogicException("Pencairan hanya bisa di-create oleh Mitra");
        }
        $currentDate = new DateTime();
        $tahun = $currentDate->format("Y");
        $urutan = $this->getNextUrutan($mitra, $tahun);
        $nomorSurat = 'PD-'.
            $kodeMitra.'-'.
            $currentDate->format('y').'-'.
            str_pad($urutan, 4, "0", STR_PAD_LEFT);
        $entity = new Pencairan();
        $entity->setIsExpense($isExpense);
        $entity->setIsPelunasan($isPelunasan);
        $entity->setStatus(WorkflowTags::NEW);
        $entity->setMitra($mitra);
        $entity->setTahun($tahun);
        $entity->setUrutan($urutan);
        $entity->setNomorSurat($nomorSurat);
        $entity->setTipeTermin($isExpense ? 'EXPENSE' : 'TERMIN' );
        $entity->setPersen(100);
        $entity->setCreatedBy($user);
        $entity->setCreatedDate(new DateTime());
        $this->em->persist($entity);
        $this->em->flush();

        $this->logger->info('CREATE record ' . $entity->getNomorSurat() . '.', Pencairan::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
        return $entity;
    }

    /**
     * menghitung urutan berdasarkan mitra dan taun yang sama
     * @param Mitra $mitra
     * @param int $tahun
     * @return int
     */
    private function getNextUrutan(Mitra $mitra, int $tahun) : int {

        $last = 0;
        $query = $this->em->createQuery('SELECT MAX(p.urutan) FROM App:Pencairan\Pencairan p WHERE p.mitra = :mitra AND p.tahun = :tahun');
        $query->setParameter('tahun', $tahun);
        $query->setParameter('mitra', $mitra);

        try {
            $last = $query->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return $last + 1;
    }

    /**
     * Proses update pencairan.
     * @param Pencairan|null $entity
     * @param string $tipe_termin
     * @param int $persen
     * @param array $items
     * @param float $total_budget
     * @param float $total_pencairan
     * @return Pencairan
     */
    public function update(?Pencairan $entity, string $tipe_termin, ?int $persen, array $items,
                           ?float $total_budget, float $total_pencairan) : Pencairan
    {
        $entity->setTipeTermin($tipe_termin);
        if ($entity->getIsExpense()==true ){
            $entity->setPersen(100);
        }
        else {
            $entity->setPersen($persen);
        }
        $entity->setTotalBudget($total_budget);
        $entity->setTotalPencairan($total_pencairan);
        $this->em->persist($entity);
        $this->clearItems($entity);
        foreach ($items as $item) {
            $kegiatanId = $item['kegiatanId'];
            $subkegiatanId = null;
            if (array_key_exists('subkegiatanId', $item) ) {
                $subkegiatanId = $item['subkegiatanId'];
            };
            $title = $item['title'];
            $amount = $item['amount'];
            if (($kegiatanId != null) && ($amount != 0)) {
                $newItem = new ItemPencairan();
                $newItem->setPencairan($entity);
                $kegiatan = $this->kegiatanRepository->find($kegiatanId);
                $newItem->setKegiatan($kegiatan);

                if ($entity->getIsExpense()=== true){
                    $kegiatan->setJenisPembayaran('EXPENSE');
                    $this->em->persist($kegiatan);
                } elseif (($entity->getIsExpense()=== false) && ($entity->getIsPelunasan()===false)) {
                    $kegiatan->setJenisPembayaran('TERMIN');
                    $kegiatan->setTermin1(true);
                    $kegiatan->setTermin1Percent($persen);
                    $this->em->persist($kegiatan);
                } else if ($entity->getIsPelunasan()===true) {
                    $kegiatan->setTerminPelunasan(true);
                    $this->em->persist($kegiatan);
                }


                if ($subkegiatanId != null) {
                    $subkegiatan = $this->subkegiatanRepository->find($subkegiatanId);
                    $newItem->setSubkegiatan($subkegiatan);
                }
                else {
                    $newItem->setSubkegiatan(null);
                }

                $ids = [];
                if (array_key_exists('ids', $item) ) {
                    $ids = $item['ids'];
                };
                foreach ($ids as $id){
                    $realisasi = $this->itemRealisasiRepository->find($id);
                    $newItem->addItemRealisasi($realisasi);
                }
                $newItem->setTitle($title);
                $newItem->setAmount($amount);
                $this->em->persist($newItem);
                $entity->addItemPencairan($newItem);
            }
        }
        $this->em->flush();

        return $entity;
    }

    /**
     * kosongkan item pencairan.
     *
     * @param Pencairan $entity Persistent entity object
     */
    public function clearItems(Pencairan $entity): void
    {
        $items = $entity->getChildren();
        foreach ($items as $item) {
            $this->em->remove($item);
        }
        $this->em->flush();
    }


    /**
     * Proses hapus data pencairan.
     *
     * @param Pencairan $entity Persistent entity object
     */
    public function delete(Pencairan $entity): void
    {
        $this->em->remove($entity);
        $this->em->flush();

        $this->logger->info('HAPUS record ' . $entity->getNomorSurat() . ': ' . $entity->getTipeTermin() . '.', Pencairan::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * @param Pencairan $entity
     * @param ActionObject $data
     * @param UserAccount $user
     * @throws
     */
    public function doWorkflow(Pencairan $entity, ActionObject $data, UserAccount $user) {
        $action = $data->getAction();
        $message = $data->getMessage();

        if ($action == WorkflowTags::STEP_PRINT) {
            $entity->setPrintedBy($user);
            $entity->setPrintedDate(new DateTime());
            $this->em->persist($entity);
            $this->em->flush();
        }
        else if ($action == WorkflowTags::STEP_SEND) {
            $entity->setSentBy($user);
            $entity->setSentDate(new DateTime());
            $this->em->persist($entity);
            $this->em->flush();
        }
        else if ($action == WorkflowTags::STEP_RECEIVE) {
            $entity->setReceivedBy($user);
            $entity->setReceivedDate(new DateTime());
            $this->em->persist($entity);
            $this->em->flush();
        }
        else if ($action == WorkflowTags::STEP_REJECT) {
            $entity->setRejectedBy($user);
            $entity->setRejectedDate(new DateTime());
            $entity->setRejectedReason($message);
            $this->em->persist($entity);
            $this->em->flush();
        }
        else if ($action == WorkflowTags::STEP_VERIFICATION) {
            $entity->setApprovedBy($user);
            $entity->setApprovedDate(new DateTime());
            $this->em->persist($entity);
            $this->em->flush();
        }
        else if ($action == WorkflowTags::STEP_PAY) {
            $entity->setPaidBy($user);
            $entity->setPaidDate(new DateTime());
            $this->em->persist($entity);
            $this->em->flush();
        }
        else if ($action == WorkflowTags::STEP_REVISE) {
//            $this->em->persist($entity);
//            $this->em->flush();
        }
        else {
            throw new LogicException('Transaksi '.$action.' tidak dapat dijalankan di Workflow ini.');
        }

    }

    /**
     * @param Pencairan|null $entity
     * @param UploadedFile|null $file
     * @param array|null $params
     *
     */
    public function upload(?Pencairan $entity, ?UploadedFile $file, ?array $params) : void
    {
        if (!is_null($file) && !is_null($params)) {
            $uploaded = $this->uploadFile($file, $params['uploadPath']);
            if (is_null($uploaded)) {
                throw new LogicException('Bukti pembayaran gagal terupload. Silahkan ulangi kembali');
            }

            /**
             * Remove existing attachment and thumbnail if exist
             */
            if (!is_null($entity->getFileName()) && $this->filesystem->existFile($entity->getFileUrl())) {
                $this->filesystem->removeFile($entity->getFileUrl());

            }
            /**
             * Set new attachment
             */
            $entity->setFilePath($uploaded['filePath']);
            $entity->setFileName($uploaded['fileName']);
            $entity->setFileExt($uploaded['fileExt']);
            $entity->setFileSize($uploaded['fileSize']);
        }

        $this->em->persist($entity);
        $this->em->flush();

    }
    /**
     * @param UploadedFile $file
     * @param string       $uploadPath
     *
     * @return array
     */
    private function uploadFile(UploadedFile $file, string $uploadPath)
    {
        $originalFilename = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        $fileSize = $file->getSize();
        $newFilename = uniqid('pembayaran_') . '.' . $file->guessExtension();

        try {
            if ($fileSize < 1) {
                throw new LogicException(sprintf('Lampiran <b>%s</b> tidak dapat diunggah karena ada kesalahan pada dokumen (document corrupt).',
                    $originalFilename));
            }

            $attachment = null;
            if ($file->move($uploadPath, $newFilename)) {
                $attachment = [
                    'filePath' => $uploadPath,
                    'fileName' => $newFilename,
                    'fileExt' => $fileExtension,
                    'fileSize' => $fileSize
                ];
//                if (in_array($fileExtension, ['jpg', 'jpeg', 'png', 'gif'])) {
//                    $source = $uploadPath . '/' . $newFilename;
//                    $target = $uploadPath . '/thumbs/' . $newFilename;
//                    $thumbClass = new ImageTransform();
//                    $thumbClass->createThumbnail($source, $target, 350, 350);
//                }
            }
            return $attachment;

        } catch (FileException $e) {
            throw $e;
        }
    }


    public function updateTglTransferNoVoucher(?Pencairan $pencairan, $tglTransfer, $nomorVoucher)
    {
        $date = DateTime::createFromFormat('Y-m-d', $tglTransfer);
        if ($date) {
            $pencairan->setTransferDate($date);
        }
        $pencairan->setNomorVoucherBdvc($nomorVoucher);

        $this->em->persist($pencairan);
        $this->em->flush();
        return $pencairan;
    }

    private function setPaidRealisasi(Pencairan $entity)
    {
        $children = $entity->getChildren();
        /** @var ItemPencairan $itemPencairan */
        foreach ($children as $itemPencairan){
            /** @var ItemRealisasiRab $itemRealisasi */
            $itemRealisasi = $itemPencairan->getItemRealisasi();
            if ($itemRealisasi) {
                $amount = $itemPencairan->getAmount();
                $paid = $itemRealisasi->getRealisasiPaid();
                $itemRealisasi->setRealisasiPaid($paid+$amount);
                $this->em->persist($itemRealisasi);

                $kegiatan = $itemPencairan->getKegiatan();
                $cair = $kegiatan->getPencairan();
                $kegiatan->setPencairan($cair + $amount);
                $this->em->persist($kegiatan);
            }
            else {
                $amount = $itemPencairan->getAmount();

                $kegiatan = $itemPencairan->getKegiatan();
                $cair = $kegiatan->getPencairan();
                $kegiatan->setPencairan($cair + $amount);
                $this->em->persist($kegiatan);
            }
            $this->em->flush();
        }
    }

    public function getRealisasiList(Kegiatan $kegiatan) {
        $kegiatanId = $kegiatan->getKegiatanId();
        $realisasis = $this->itemRealisasiRepository->findBy(['kegiatanRab' => $kegiatanId], ['subRab' => 'asc']);

        $bulans = [];
        foreach ($realisasis as $item){
            $tglbulan = $item->getRealisasiDate()->format('Y-m');
            $bulans[$tglbulan][]  = [
                'approved' => $item->getStatus() == 'REALISASI_VERIFIED',
                'amount' => $item->getRealisasi(),
                'item' => $item
            ];
        }

        $listitems = [];
        foreach ($bulans as $bulanlabel => $bulan) {
            $tglawal = $bulanlabel.'-01';
            $date = DateTime::createFromFormat('Y-m-d', $tglawal);
            $tgllabel = $date->format('M Y');

            $approved = true;
            $amount = 0;
            $ids = [];
            $description = null;
            foreach ($bulan as $details){
                $approved = $approved && $details['approved'];
                $amount += $details['amount'];
                $ids[] = $details['item']->getId();
                $description = $this->commaAdd($description, $details['item']->getParentRab()->getTitle());
            }
            if ($approved) {
                $listitems[] = [
                    'tglbulan' => $tgllabel,
                    'amount' => $amount,
                    'approved' => $approved,
                    //'bulan' => $bulan,
                    'ids' => $ids,
                    'label' => 'Pencairan ' . $tgllabel,
                    'desc' => substr($description, 0, 160)
                ];
            }
        }

        return $listitems;
    }

    private function commaAdd(?string $s, ?string $added) : string {
        if (!$s) {
            return $added;
        }
        else {
            return $s . ', ' . $added;
        }
    }

    public function generateAccountDetailPencairan(Pencairan $pencairan)
    {
        $isExpense = $pencairan->getIsExpense();
        $children = $pencairan->getChildren();
        $rows = [];
        if (!$isExpense) {
            foreach ($children as $itemPencairan){
                $kegiatan = $itemPencairan->getKegiatan();
                $subkegiatan = $itemPencairan->getSubkegiatan();
                if ($subkegiatan === null) {
                    $itemRows = $this->generateAccountDetailKegiatan($itemPencairan, $kegiatan, $rows);
                }
                else {
                    $itemRows = $this->generateAccountDetailSubkegiatan($itemPencairan, $kegiatan, $subkegiatan, $rows);
                }
                $rows = array_merge($rows, $itemRows);
            }
        }
        return $rows;
    }

    public function generateAccountDetailKegiatan(ItemPencairan $itemPencairan, Kegiatan $kegiatan, array $rows) {
        $anggaran = $kegiatan->getAnggaran();
        $paid = $itemPencairan->getAmount();
        $ratio = $paid / $anggaran;

        $kegiatanRow = [
            'level' => 'kegiatan',
            'nama' => $kegiatan->getNamaKegiatan(),
            'anggaran' => $anggaran,
            'paid' => $paid
        ];
        $rows[] = $kegiatanRow;
        $subkegiatans = $kegiatan->getChildren();
        foreach ($subkegiatans as $subkegiatan){
            $subkegiatanRow = [
                'level' => 'subkegiatan',
                'nama' => $subkegiatan->getTitle(),
                'anggaran' => $subkegiatan->getAnggaran(),
                'paid' => $subkegiatan->getAnggaran() * $ratio
            ];
            $rows[] = $subkegiatanRow;

            $rabs = $subkegiatan->getChildren();
            foreach ($rabs as $rab) {
                $rabRow = [
                    'level' => 'rab',
                    'nama' => $rab->getTitle(),
                    'anggaran' => $rab->getAnggaran(),
                    'paid' => $rab->getAnggaran() * $ratio
                ];
                $rows[] = $rabRow;
            }

        }

        return $rows;
    }

    public function generateAccountDetailSubkegiatan(ItemPencairan $itemPencairan, Kegiatan $kegiatan, Subkegiatan $subkegiatan, array $rows) {
        $anggaran = $subkegiatan->getAnggaran();
        $paid = $itemPencairan->getAmount();
        $ratio = $paid / $anggaran;

        $kegiatanRow = [
            'level' => 'kegiatan',
            'nama' => $kegiatan->getNamaKegiatan(),
            'anggaran' => $anggaran
        ];

        $subkegiatanRow = [
            'level' => 'subkegiatan',
            'nama' => $subkegiatan->getTitle(),
            'anggaran' => $subkegiatan->getAnggaran(),
            'paid' => $paid
        ];
        $rows[] = $subkegiatanRow;

        $rabs = $subkegiatan->getChildren();
        foreach ($rabs as $rab) {
            $rabRow = [
                'level' => 'rab',
                'nama' => $rab->getTitle(),
                'anggaran' => $rab->getAnggaran(),
                'paid' => $rab->getAnggaran() * $ratio
            ];
            $rows[] = $rabRow;
        }

        return $rows;
    }

    public function generateAccountDetailPencairanExpense(Pencairan $pencairan)
    {
        $isExpense = $pencairan->getIsExpense();
        $children = $pencairan->getChildren();
        $rows = [];
        if ($isExpense) {
            foreach ($children as $itemPencairan){
                $kegiatan = $itemPencairan->getKegiatan();
                $itemRows = $this->generateAccountDetailRealisasi($itemPencairan, $kegiatan, $rows);
                $rows = array_merge($rows, $itemRows);
            }
        }
        return $rows;
    }
    public function generateAccountDetailRealisasi(ItemPencairan $itemPencairan, Kegiatan $kegiatan, array $rows) : array
    {
        $arrayRealisasi = $itemPencairan->getItemRealisasis();
        $kegiatanRow = [
            'level' => 'kegiatan',
            'tanggal' => '',
            'namaKegiatan' => $kegiatan->getNamaKegiatan(),
            'amount' => $itemPencairan->getAmount()
        ];
        $rows[] = $kegiatanRow;
        /** @var ItemRealisasiRab $itemRealisasi */
        foreach ($arrayRealisasi as $itemRealisasi) {
            /** @var ItemAnggaran $itemRab */
            $itemRab = $itemRealisasi->getParentRab();
            $subkegiatan = $itemRab->getParent();
            $namasub = $subkegiatan->getTitle();
            $namaRab = $itemRab->getTitle();
            $amount = $itemRealisasi->getRealisasi();
            $tglRealisasi = $itemRealisasi->getRealisasiDate();
            if ($tglRealisasi != null) {
                $tanggal = $tglRealisasi->format('M Y');
            }
            else {
                $tanggal = '';
            }
            $realisasiRow = [
                'level' => 'rab',
                'tanggal' => $tanggal,
                'subkegiatan' => $namasub,
                'itemrab' => $namaRab,
                'amount' => $amount
            ];
            $rows[] = $realisasiRow;
        }

        return $rows;
    }

}