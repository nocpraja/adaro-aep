<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */
namespace App\Models\Pencairan;

use App\Component\Workflow\Transition;
use App\Models\Pendanaan\Workflow\BaseConfigurer;
use App\Models\Pencairan\WorkflowTags;
use App\Component\Workflow\Definition;
use App\Component\Workflow\Visitor\MarkingVisitorImpl;
use App\Component\Workflow\WorkflowImpl;

/**
 * Class WorkflowConfigurer
 * @package App\Models\Pencairan
 * @author  Mark Melvin
 * @since   08/06/2020, modified: 08/06/2020 03:13
 */
class WorkflowConfigurer extends BaseConfigurer
{
    protected function createPlaces(): void
    {
        $this->places = [
            WorkflowTags::NEW,
            WorkflowTags::PRINTED,
            WorkflowTags::SENT,
            WorkflowTags::RECEIVED,
            WorkflowTags::REJECTED,
            WorkflowTags::VERIFIED,
            WorkflowTags::PAID,
            WorkflowTags::REVISED
        ];
    }

    protected function createTransitions(): void
    {
        $this->transitions = [
            new Transition([WorkflowTags::NEW, WorkflowTags::REVISED],
                WorkflowTags::PRINTED,
                WorkflowTags::STEP_PRINT),
            new Transition([WorkflowTags::PRINTED, WorkflowTags::REVISED],
                WorkflowTags::SENT,
                WorkflowTags::STEP_SEND),
            new Transition([WorkflowTags::SENT],
                WorkflowTags::RECEIVED,
                WorkflowTags::STEP_RECEIVE),
            new Transition([WorkflowTags::RECEIVED],
                WorkflowTags::REJECTED,
                WorkflowTags::STEP_REJECT),
            new Transition([WorkflowTags::RECEIVED],
                WorkflowTags::VERIFIED,
                WorkflowTags::STEP_VERIFICATION),
            new Transition([WorkflowTags::VERIFIED],
                WorkflowTags::PAID,
                WorkflowTags::STEP_PAY),
            new Transition([WorkflowTags::RECEIVED],
                WorkflowTags::REJECTED,
                WorkflowTags::STEP_REJECT),
            new Transition([WorkflowTags::REJECTED],
                WorkflowTags::REVISED,
                WorkflowTags::STEP_REVISE)
            ];
    }

    protected function createWorkflow(): void
    {
        $definition = new Definition($this->places, $this->transitions, $this->startPlace);
        $this->workflow = new WorkflowImpl($definition, new MarkingVisitorImpl('status'),
            $this->dispatcher, 'pencairan');
    }
}
