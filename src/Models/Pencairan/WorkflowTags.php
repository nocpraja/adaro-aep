<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */
namespace App\Models\Pencairan;


class WorkflowTags
{
    const NEW = 'NEW';
    const PRINTED = 'PRINTED';
    const SENT = 'SENT';
    const RECEIVED = 'RECEIVED';
    const VERIFIED = 'VERIFIED';
    const REJECTED = 'REJECTED';
    const PAID = 'PAID';
    const REVISED = 'REVISED';

    const STEP_PRINT = 'printing';
    const STEP_SEND = 'sending';
    const STEP_RECEIVE = 'receiving';
    const STEP_REJECT = 'reject';
    const STEP_VERIFICATION = 'verify';
    const STEP_PAY = 'paying';
    const STEP_REVISE = 'revising';

}