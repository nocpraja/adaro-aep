<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Beneficiary;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Entity\Beneficiary\BeneficiaryInstitusi;
use App\Entity\Beneficiary\LogAprvBeneficiary;
use App\Entity\Pendanaan\DanaBatch;
use App\Library\Helper;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\Beneficiary\BeneficiaryInstitusiRepository;
use App\Repository\Beneficiary\LogAprvBeneficiaryRepository;
use App\Repository\MasterData\KecamatanRepository;
use App\Repository\MasterData\KelurahanRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;

/**
 * Class BeneficiaryInstitusiModel
 * @package App\Models\Beneficiary
 * @author  Tri N
 * @since   31/09/2018, modified: 13/08/2020 01:37
 */
final class BeneficiaryInstitusiModel
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BeneficiaryInstitusiRepository
     */
    private $repository;

    /**
     * @var DanaBatchRepository
     */
    private $programRepository;

    /**
     * @var KecamatanRepository
     */
    private $kecamatanRepository;

    /**
     * @var KelurahanRepository
     */
    private $kelurahanRepository;

    /**
     * @var AuditLogger
     */
    private $logger;
    /**
     * @var WorkflowConfigurer
     */
    private $workflowConfigurer;
    /**
     * @var LogAprvBeneficiaryRepository
     */
    private $logAprvBeneficiaryRepository;

    /**
     * BeneficiaryInstitusiModel Constructor.
     *
     * @param EntityManagerInterface         $entityManager       Doctrine entity manager
     * @param BeneficiaryInstitusiRepository $repository          Entity repository instance
     * @param DanaBatchRepository            $programRepository   Entity repository instance
     * @param KecamatanRepository            $kecamatanRepository Entity repository instance
     * @param KelurahanRepository            $kelurahanRepository Entity repository instance
     * @param WorkflowConfigurer             $workflowConfigurer  Workflow configurator
     * @param LogAprvBeneficiaryRepository   $logAprvBeneficiaryRepository
     * @param AuditLogger                    $logger              AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                BeneficiaryInstitusiRepository $repository,
                                DanaBatchRepository $programRepository,
                                KecamatanRepository $kecamatanRepository,
                                KelurahanRepository $kelurahanRepository,
                                WorkflowConfigurer $workflowConfigurer,
                                LogAprvBeneficiaryRepository $logAprvBeneficiaryRepository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->programRepository = $programRepository;
        $this->kecamatanRepository = $kecamatanRepository;
        $this->kelurahanRepository = $kelurahanRepository;
        $this->logAprvBeneficiaryRepository = $logAprvBeneficiaryRepository;
        $this->workflowConfigurer = $workflowConfigurer;
        $this->logger = $logger;
    }

    /**
     * Menambahkan penerima manfaat Insitusi ke dalam program (DPT).
     *
     * @param BeneficiaryInstitusi $entity  Persistent entity object
     * @param DanaBatch            $program Persistent entity object
     */
    public function addToProgram(BeneficiaryInstitusi $entity, DanaBatch $program): void
    {
        $collection = $entity->getPrograms();
        if ($collection->contains($program)) {
            throw new LogicException('Data penerima manfaat Institusi tidak boleh duplikat.');
        }

        $entity->addProgram($program);
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Remove penerima manfaat Institusi dari database.
     *
     * @param BeneficiaryInstitusi $institusi Persistent entity object
     *
     * @throws LogicException If an error is occured
     */
    public function delete(BeneficiaryInstitusi $institusi): void
    {
        if ($institusi->getStatusVerifikasi() > 0 || $institusi->getStatusPembinaan() > 0 ||
            $institusi->getPrograms()->count() > 1) {
            throw new LogicException("Data institusi tidak boleh dihapus.");
        }

        $this->em->remove($institusi);
        $this->logger->notice('HAPUS record penerima manfaat institusi.', BeneficiaryInstitusi::class,
                              $institusi, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Menampilkan daftar PAUD dalam program
     *
     * @param DataQuery $dq         Request query
     * @param bool      $forProgram Tampilkan beneficiary yang ikut program atau tidak
     *
     * @return Paginator
     */
    public function displayAllPaud(DataQuery $dq, bool $forProgram = false): Paginator
    {
        $query = $this->repository->findAllPaudByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                          $dq->getOffset(), $forProgram, $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Menampilkan daftar Pesantren & Yayasan yang ikut program tertentu.
     *
     * @param DataQuery $dq         Request query
     * @param bool      $forProgram Tampilkan beneficiary yang ikut program atau tidak
     *
     * @return Paginator
     */
    public function displayAllPesantrenYayasan(DataQuery $dq, bool $forProgram): Paginator
    {
        $query = $this->repository->findAllPesantrenAndYayasanByCriteria($dq->getFilters(), $dq->getSorts(),
                                                                         $dq->getPageSize(), $dq->getOffset(),
                                                                         $forProgram, $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Menampilkan daftar SMK & Poltek yang ikut program tertentu.
     *
     * @param DataQuery $dq         Request query
     * @param bool      $forProgram Tampilkan beneficiary yang ikut program atau tidak
     *
     * @return Paginator
     */
    public function displayAllSmkAndPoltek(DataQuery $dq, bool $forProgram): Paginator
    {
        $query = $this->repository->findAllSmkAndPoltekByCriteria($dq->getFilters(), $dq->getSorts(),
                                                                  $dq->getPageSize(), $dq->getOffset(),
                                                                  $forProgram, $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @param BeneficiaryInstitusi $entity Persistent entity object
     *
     * @return LogAprvBeneficiary
     */
    public function fetchLogApproval(BeneficiaryInstitusi $entity)
    {
        return $this->logAprvBeneficiaryRepository->findOneBy(['institusi' => $entity, 'logAction' => WorkflowTags::STEP_REJECT], ['postedDate' => 'desc']);
    }

    /**
     * Get Doctrine Entity Manager
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @return KecamatanRepository
     */
    public function getKecamatanRepository(): KecamatanRepository
    {
        return $this->kecamatanRepository;
    }

    /**
     * @return KelurahanRepository
     */
    public function getKelurahanRepository(): KelurahanRepository
    {
        return $this->kelurahanRepository;
    }

    /**
     * Get Audit Logger Service
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * @return DanaBatchRepository
     */
    public function getProgramRepository(): DanaBatchRepository
    {
        return $this->programRepository;
    }

    /**
     * @return BeneficiaryInstitusiRepository
     */
    public function getRepository(): BeneficiaryInstitusiRepository
    {
        return $this->repository;
    }

    /**
     * Melepaskan data penerima manfaat Insitusi dari program Dana Batch.
     *
     * @param BeneficiaryInstitusi $entity  Persistent entity object
     * @param DanaBatch            $program Persistent entity object
     */
    public function removeFromProgram(BeneficiaryInstitusi $entity, DanaBatch $program): void
    {
        $entity->removeProgram($program);
        $this->em->persist($entity);
        $this->logger->notice('HAPUS record penerima manfaat institusi dari program Dana Batch.',
                              BeneficiaryInstitusi::class, $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Simpan data Institusi beserta program terkait.
     * Caller harus bertanggung jawab memanggil EntityManager::flush()
     *
     * @param BeneficiaryInstitusi $entity Persistent entity object
     */
    public function save(BeneficiaryInstitusi $entity): void
    {
        $fts = [];
        if (!empty($entity->getKodeInstitusi())) {
            $fts[] = $entity->getKodeInstitusi();
        }
        if (!empty($entity->getNamaInstitusi())) {
            $fts[] = $entity->getNamaInstitusi();
        }
        if (!empty($entity->getNamaPimpinan())) {
            $fts[] = $entity->getNamaPimpinan();
        }
        if (!empty($entity->getAlamat())) {
            $fts[] = $entity->getAlamat();
        }
        if (!empty($entity->getEmail())) {
            $fts[] = $entity->getEmail();
        }
        if (!empty($entity->getWebsite())) {
            $fts[] = $entity->getWebsite();
        }
        if (!empty($entity->getOfficePhone())) {
            $fts[] = $entity->getOfficePhone();
        }
        if (!empty($entity->getNomorAkte())) {
            $fts[] = $entity->getNomorAkte();
        }
        if (!empty($entity->getTahunBerdiri())) {
            $fts[] = $entity->getTahunBerdiri();
        }

        $kelurahan = $entity->getKelurahan();
        $kecamatan = $entity->getKecamatan();
        $kabupaten = $kecamatan->getKabupaten();
        $provinsi = $kabupaten->getProvinsi();

        $fts[] = $provinsi->getNamaProvinsi();
        $fts[] = $kabupaten->getNamaKabupaten();
        $fts[] = $kecamatan->getNamaKecamatan();
        $fts[] = $kelurahan->getNamaKelurahan();

        $entity->setSearchFts($fts);

        $workflow = $this->workflowConfigurer->getWorkflow();
        if ($workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            $entity->setStatusVerifikasi(0);
            $entity->setStatus($entity->getInstitusiId() ? WorkflowTags::UPDATED : WorkflowTags::NEW);
        }

        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Proses workflow transition.
     *
     * @param BeneficiaryInstitusi $entity Persistent entity object
     * @param ActionObject         $data   Transient data object
     */
    public function actionTransition(BeneficiaryInstitusi $entity, ActionObject $data): void
    {

        $workflow = $this->workflowConfigurer->getWorkflow();
        $transition = $data->getAction();
        $context = [
            'status' => $entity->getStatus(),
            'keterangan' => $data->getMessage(),
            'title' => ucfirst($transition) . ' beneficiary ' . strtolower(Helper::institusiCat($entity->getKategori())) . ' ' . $entity->getNamaInstitusi()
        ];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum bisa dilakukan.');
        }

        $workflow->apply($entity, $transition, $context);
    }
}
