<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Beneficiary;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\DataObject\SortOrFilter;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Entity\Beneficiary\BeneficiaryInstitusi;
use App\Entity\Beneficiary\LogAprvBeneficiary;
use App\Entity\Pendanaan\DanaBatch;
use App\Library\Helper;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\Beneficiary\BeneficiaryIndividuRepository;
use App\Repository\Beneficiary\BeneficiaryInstitusiRepository;
use App\Repository\Beneficiary\LogAprvBeneficiaryRepository;
use App\Repository\MasterData\KecamatanRepository;
use App\Repository\MasterData\KelurahanRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;

/**
 * Class BeneficiaryIndividuModel
 * @package App\Models\Beneficiary
 * @author  Tri N
 * @since   1/11/2018, modified: 12/08/2020 20:45
 */
final class BeneficiaryIndividuModel
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BeneficiaryIndividuRepository
     */
    private $repository;

    /**
     * @var DanaBatchRepository
     */
    private $programRepository;

    /**
     * @var KecamatanRepository
     */
    private $kecamatanRepository;

    /**
     * @var KelurahanRepository
     */
    private $kelurahanRepository;

    /**
     * @var BeneficiaryInstitusiRepository
     */
    private $institusiRepository;

    /**
     * @var AuditLogger
     */
    private $logger;
    /**
     * @var WorkflowConfigurer
     */
    private $workflowConfigurer;
    /**
     * @var LogAprvBeneficiaryRepository
     */
    private $logAprvBeneficiaryRepository;

    /**
     * BeneficiaryIndividuModel constructor.
     *
     * @param BeneficiaryIndividuRepository  $repository          Entity repository instance
     * @param BeneficiaryInstitusiRepository $institusiRepository Entity repository instance
     * @param DanaBatchRepository            $programRepository   Entity repository instance
     * @param KecamatanRepository            $kecamatanRepository Entity repository instance
     * @param KelurahanRepository            $kelurahanRepository Entity repository instance
     * @param WorkflowConfigurer             $workflowConfigurer  Workflow configurator
     * @param LogAprvBeneficiaryRepository   $logAprvBeneficiaryRepository
     * @param AuditLogger                    $logger              AuditLogger service
     */
    public function __construct(BeneficiaryIndividuRepository $repository,
                                BeneficiaryInstitusiRepository $institusiRepository,
                                DanaBatchRepository $programRepository,
                                KecamatanRepository $kecamatanRepository,
                                KelurahanRepository $kelurahanRepository,
                                WorkflowConfigurer $workflowConfigurer,
                                LogAprvBeneficiaryRepository $logAprvBeneficiaryRepository,
                                AuditLogger $logger)
    {
        $this->em = $logger->getEntityManager();
        $this->repository = $repository;
        $this->programRepository = $programRepository;
        $this->kecamatanRepository = $kecamatanRepository;
        $this->kelurahanRepository = $kelurahanRepository;
        $this->institusiRepository = $institusiRepository;
        $this->workflowConfigurer = $workflowConfigurer;
        $this->logAprvBeneficiaryRepository = $logAprvBeneficiaryRepository;
        $this->logger = $logger;
    }

    /**
     * Menambahkan penerima manfaat Individu ke dalam program (DPT).
     *
     * @param BeneficiaryIndividu $entity  Persistent entity object
     * @param DanaBatch           $program Persistent entity object
     */
    public function addToProgram(BeneficiaryIndividu $entity, DanaBatch $program): void
    {
        $collection = $entity->getPrograms();
        if ($collection->contains($program)) {
            throw new LogicException('Data penerima manfaat Individu tidak boleh duplikat.');
        }

        $institusi = $entity->getInstitusi();
        $kategori = $institusi->getKategori();
        $results = $this->findInstitusiInProgram($program, $kategori, $institusi->getInstitusiId());
        if (count($results) == 0) {
            throw new LogicException('Institusi penerima manfaat belum terdaftar.');
        }

        $entity->addProgram($program);
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Remove penerima manfaat Individu dari database.
     *
     * @param BeneficiaryIndividu $entity Persistent entity object
     *
     * @throws LogicException If an error is occured
     */
    public function delete(BeneficiaryIndividu $entity): void
    {
        if ($entity->getStatusVerifikasi() > 0 || $entity->getPrograms()->count() > 1) {
            throw new LogicException("Data penerima manfaat Individu tidak boleh dihapus.");
        }

        $this->em->remove($entity);
        $this->logger->notice('HAPUS record penerima manfaat individu.', BeneficiaryIndividu::class,
                              $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Menampilkan daftar Guru pada institusi/lembaga dan program tertentu.
     *
     * @param DataQuery $dq               Filter kriteria
     * @param boolean   $forProgram       Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string    $lembaga          Jenis lembaga dari guru yang akan ditampilkan
     *                                    [PAUD|SMK-POLTEK|PESANTREN-YAYASAN|]
     *
     * @return Paginator
     */
    public function displayAllGuru(DataQuery $dq, bool $forProgram, string $lembaga = 'PAUD'): Paginator
    {
        $filters = $dq->getFilters();
        $sorts = $dq->getSorts();
        $limit = $dq->getPageSize();
        $offset = $dq->getOffset();
        $whereClause = $dq->getCondition();

        if ($lembaga === "PAUD") {
            $query = $this->repository->findAllGuruPaudByCriteria($filters, $sorts, $limit, $offset, $forProgram,
                                                                  $whereClause);
        } else if ($lembaga === "SMK-POLTEK") {
            $query = $this->repository->findAllGuruSmkAndPoltekByCriteria($filters, $sorts, $limit, $offset,
                                                                          $forProgram, $whereClause);
        } else if ($lembaga === "PESANTREN-YAYASAN") {
            $query = $this->repository->findAllGuruPesantrenAndYayasanByCriteria($filters, $sorts, $limit, $offset,
                                                                                 $forProgram, $whereClause);
        } else {
            $query = $this->repository->findAllByCriteria($filters, $sorts, $limit, $offset, $forProgram, $whereClause);
        }

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Menampilkan daftar Manajemen pada institusi/lembaga dan program tertentu.
     *
     * @param DataQuery $dq               Filter kriteria
     * @param boolean   $forProgram       Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string    $lembaga          Jenis lembaga dari manajemen yang akan ditampilkan
     *                                    [SMK-POLTEK|PESANTREN-YAYASAN|]
     *
     * @return Paginator
     */
    public function displayAllManajemen(DataQuery $dq, bool $forProgram,
                                        string $lembaga = 'SMK-POLTEK'): Paginator
    {
        $filters = $dq->getFilters();
        $sorts = $dq->getSorts();
        $limit = $dq->getPageSize();
        $offset = $dq->getOffset();
        $whereClause = $dq->getCondition();

        if ($lembaga === "SMK-POLTEK") {
            $query = $this->repository->findAllManajemenSmkAndPoltekByCriteria($filters, $sorts, $limit, $offset,
                                                                               $forProgram, $whereClause);
        } else if ($lembaga === "PESANTREN-YAYASAN") {
            $query = $this->repository->findAllManajemenPesantrenAndYayasanByCriteria($filters, $sorts, $limit, $offset,
                                                                                      $forProgram, $whereClause);
        } else {
            $query = $this->repository->findAllByCriteria($filters, $sorts, $limit, $offset, $forProgram, $whereClause);
        }

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Menampilkan daftar Siswa pada institusi/lembaga dan program tertentu.
     *
     * @param DataQuery $dq               Filter kriteria
     * @param boolean   $forProgram       Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string    $lembaga          Jenis lembaga dari siswa yang akan ditampilkan
     *                                    [PAUD|SMK-POLTEK|PESANTREN-YAYASAN|]
     *
     * @return Paginator
     */
    public function displayAllSiswa(DataQuery $dq, bool $forProgram,
                                    string $lembaga = 'PAUD'): Paginator
    {
        $filters = $dq->getFilters();
        $sorts = $dq->getSorts();
        $limit = $dq->getPageSize();
        $offset = $dq->getOffset();
        $whereClause = $dq->getCondition();

        if ($lembaga === "PAUD") {
            $query = $this->repository->findAllSiswaPaudByCriteria($filters, $sorts, $limit, $offset, $forProgram,
                                                                   $whereClause);
        } else if ($lembaga === "SMK-POLTEK") {
            $query = $this->repository->findAllSiswaSmkAndPoltekByCriteria($filters, $sorts, $limit, $offset,
                                                                           $forProgram, $whereClause);
        } else if ($lembaga === "PESANTREN-YAYASAN") {
            $query = $this->repository->findAllSiswaPesantrenAndYayasanByCriteria($filters, $sorts, $limit, $offset,
                                                                                  $forProgram, $whereClause);
        } else {
            $query = $this->repository->findAllByCriteria($filters, $sorts, $limit, $offset, $forProgram, $whereClause);
        }

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @param BeneficiaryIndividu $entity Persistent entity object
     *
     * @return LogAprvBeneficiary
     */
    public function fetchLogApproval(BeneficiaryIndividu $entity)
    {
        return $this->logAprvBeneficiaryRepository->findOneBy(['individu' => $entity, 'logAction' => WorkflowTags::STEP_REJECT], ['postedDate' => 'desc']);
    }

    /**
     * Find penerima manfaat Institusi pada suatu program dengan kriteria tertentu.
     *
     * @param DanaBatch $program  Persistent entity object
     *
     * @param int       $kategori Kategori institusi, valid values:
     *                            1 = PAUD
     *                            2 = SMK
     *                            3 = POLTEK
     *                            4 = PESANTREN
     *                            5 = NON KEAGAMAAN
     * @param int       $id       The institusi ID
     *
     * @return BeneficiaryInstitusi[]
     */
    public function findInstitusiInProgram(DanaBatch $program, int $kategori, ?int $id)
    {
        $filters = [
            new SortOrFilter('kategori', null, $kategori),
            new SortOrFilter('programs.id', null, $program->getId())
        ];

        if (!empty($id)) $filters[] = new SortOrFilter('institusiId', null, $id);
        $query = $this->institusiRepository->findAllByCriteria($filters, [], 0, 0, true);

        return $query->getResult();
    }

    /**
     * Get Doctrine Entity Manager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * Get InstitusiRepository.
     *
     * @return BeneficiaryInstitusiRepository
     */
    public function getInstitusiRepository(): BeneficiaryInstitusiRepository
    {
        return $this->institusiRepository;
    }

    /**
     * Get KecamatanRepository.
     *
     * @return KecamatanRepository
     */
    public function getKecamatanRepository(): KecamatanRepository
    {
        return $this->kecamatanRepository;
    }

    /**
     * Get KelurahanRepository.
     *
     * @return KelurahanRepository
     */
    public function getKelurahanRepository(): KelurahanRepository
    {
        return $this->kelurahanRepository;
    }

    /**
     * Get Audit Logger Service.
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * Get ProgramRepository.
     *
     * @return DanaBatchRepository
     */
    public function getProgramRepository(): DanaBatchRepository
    {
        return $this->programRepository;
    }

    /**
     * @return BeneficiaryIndividuRepository
     */
    public function getRepository(): BeneficiaryIndividuRepository
    {
        return $this->repository;
    }

    /**
     * Melepaskan data penerima manfaat Insitusi dari program (DPT).
     *
     * @param BeneficiaryIndividu $entity  Persistent entity object
     * @param DanaBatch           $program Persistent entity object
     */
    public function removeFromProgram(BeneficiaryIndividu $entity, DanaBatch $program): void
    {
        $entity->removeProgram($program);
        $this->em->persist($entity);
        $this->logger->notice('HAPUS record penerima manfaat individu dari program Dana Batch.',
                              BeneficiaryIndividu::class, $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Simpan data Individu beserta program terkait.
     *
     * @param BeneficiaryIndividu $entity Persistent entity object
     */
    public function save(BeneficiaryIndividu $entity): void
    {
        $this->updateKompetensiWirausaha($entity);

        $fts = [];
        $institusi = $entity->getInstitusi();
        $fts[] = $institusi->getNamaInstitusi();

        if (!empty($entity->getNamaLengkap())) $fts[] = $entity->getNamaLengkap();
        if (!empty($entity->getNik())) $fts[] = $entity->getNik();
        if (!empty($entity->getTempatLahir())) $fts[] = $entity->getTempatLahir();
        if (!empty($entity->getPhone())) $fts[] = $entity->getPhone();
        if (!empty($entity->getEmail())) $fts[] = $entity->getEmail();
        if (!empty($entity->getPendidikan())) $fts[] = $entity->getPendidikan();
        if (!empty($entity->getJabatan())) $fts[] = $entity->getJabatan();
        if (!empty($entity->getStatusJabatan())) $fts[] = $entity->getStatusJabatan();
        if (!empty($entity->getAlamat())) $fts[] = $entity->getAlamat();

        if (!empty($entity->getKompetensiWirausaha())) {
            foreach ($entity->getKompetensiWirausaha() as $item) {
                $fts[] = $item['text'];
            }
        }

        $kelurahan = $entity->getKelurahan();
        $kecamatan = $entity->getKecamatan();
        $kabupaten = !empty($kecamatan) ? $kecamatan->getKabupaten() : null;
        $provinsi = !empty($kabupaten) ? $kabupaten->getProvinsi() : null;

        if (!empty($kelurahan)) $fts[] = $kelurahan->getNamaKelurahan();
        if (!empty($kecamatan)) $fts[] = $kecamatan->getNamaKecamatan();
        if (!empty($kabupaten)) $fts[] = $kabupaten->getNamaKabupaten();
        if (!empty($provinsi)) $fts[] = $provinsi->getNamaProvinsi();

        $workflow = $this->workflowConfigurer->getWorkflow();
        if ($workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            $entity->setStatusVerifikasi(0);
            $entity->setStatus($entity->getIndividuId() ? WorkflowTags::UPDATED : WorkflowTags::NEW);
        }

        $entity->setSearchFts($fts);
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Assign or update keahlian/kompetensi kewirausahaan.
     *
     * @param BeneficiaryIndividu $entity Persistent entity object
     */
    private function updateKompetensiWirausaha(BeneficiaryIndividu $entity): void
    {
        $srcWiraUsaha = $entity->getKompetensiWirausaha();

        if (empty($srcWiraUsaha)) {
            $entity->setKompetensiWirausaha(null);
        } else {
            $dstWiraUsaha = [];
            foreach ($srcWiraUsaha as $value) {
                $_val = trim($value['text']);
                if (!empty($_val)) {
                    $dstWiraUsaha[] = ['text' => $_val];
                }
            }
            if (!empty($dstWiraUsaha)) {
                $entity->setKompetensiWirausaha($dstWiraUsaha);
            } else {
                $entity->setKompetensiWirausaha(null);
            }
        }
    }

    /**
     * Proses workflow transition.
     *
     * @param BeneficiaryIndividu $entity Persistent entity object
     * @param ActionObject        $data   Transient data object
     */
    public function actionTransition(BeneficiaryIndividu $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();
        $transition = $data->getAction();
        $context = [
            'status' => $entity->getStatus(),
            'keterangan' => $data->getMessage(),
            'title' => ucfirst($transition) . ' beneficiary ' . strtolower(Helper::individuCat($entity->getKategori())) . ' ' . $entity->getNamaLengkap()
        ];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum bisa dilakukan.');
        }

        $workflow->apply($entity, $transition, $context);
    }
}
