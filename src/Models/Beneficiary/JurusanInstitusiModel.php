<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Beneficiary;


use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Beneficiary\JurusanInstitusi;
use App\Entity\Beneficiary\MitraDudiJurusan;
use App\Repository\Beneficiary\BeneficiaryInstitusiRepository;
use App\Repository\Beneficiary\JurusanInstitusiRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;

/**
 * Class JurusanInstitusiModel
 *
 * @package App\Models\Beneficiary
 * @author  Mark Melvin
 * @since   28/02/2019, modified: 10/05/2019 1:59
 */
final class JurusanInstitusiModel
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var JurusanInstitusiRepository
     */
    private $repository;

    /**
     * @var BeneficiaryInstitusiRepository
     */
    private $institusiRepository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * JurusanInstitusiModel Constructor.
     *
     * @param EntityManagerInterface         $entityManager       Doctrine entity manager
     * @param JurusanInstitusiRepository     $repository          Entity repository instance
     * @param BeneficiaryInstitusiRepository $institusiRepository Entity repository instance
     * @param AuditLogger                    $logger              AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                BeneficiaryInstitusiRepository $institusiRepository,
                                JurusanInstitusiRepository $repository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->institusiRepository = $institusiRepository;
        $this->logger = $logger;
    }

    /**
     * Menghapus record Jurusan Institusi dari database.
     *
     * @param JurusanInstitusi $entity Persistent entity object of JurusanInstitusi
     *
     * @throws LogicException If an error is occured
     */
    public function delete(JurusanInstitusi $entity): void
    {
        $institusi = $entity->getInstitusi();
        $cntDudi = $entity->getMitraDudis()->count();
        $jenis = $entity->getJenis() == 2 ? 'Program Keahlian' : 'Program Studi';

        if ($institusi->getStatusVerifikasi() > 0 || $cntDudi > 0) {
            throw new LogicException(
                sprintf('Data %s tidak boleh dihapus, masih ada data yang terkait dengannya.', $jenis));
        }

        $this->em->remove($entity);
        $this->logger->notice(sprintf('HAPUS record %s.', $jenis), JurusanInstitusi::class,
                              $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Menampilkan daftar Program Keahlian/Studi.
     *
     * @param DataQuery $dq Request query
     *
     * @return Paginator
     */
    public function displayAll(DataQuery $dq): Paginator
    {
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                      $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Get BeneficiaryInstitusi repository instance.
     *
     * @return BeneficiaryInstitusiRepository
     */
    public function getInstitusiRepository(): BeneficiaryInstitusiRepository
    {
        return $this->institusiRepository;
    }

    /**
     * Get Audit Logger Service.
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * Get JurusanInstitusi repository instance.
     *
     * @return JurusanInstitusiRepository
     */
    public function getRepository(): JurusanInstitusiRepository
    {
        return $this->repository;
    }

    /**
     * Proses tambah record Program Keahlian/Studi untuk SMK ataupun Poltek.
     *
     * @param JurusanInstitusi $entity Persistent entity object of JurusanInstitusi
     */
    public function insert(JurusanInstitusi $entity): void
    {
        $this->save($entity);
        $jenis = $entity->getJenis() == 2 ? 'Program Keahlian' : 'Program Studi';
        $this->logger->info(sprintf('TAMBAH record %s.', $jenis), JurusanInstitusi::class,
                            $entity, [JsonController::CONTEXT_NON_REL]);

    }

    /**
     * Proses update record Program Keahlian/Studi dari suatu SMK ataupun Poltek.
     *
     * @param JurusanInstitusi $entity Persistent entity object of JurusanInstitusi
     */
    public function update(JurusanInstitusi $entity): void
    {
        $this->save($entity);
        $jenis = $entity->getJenis() == 2 ? 'Program Keahlian' : 'Program Studi';
        $this->logger->info(sprintf('UPDATE record %s.', $jenis), JurusanInstitusi::class,
                            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Proses simpan record Jurusan untuk SMK ataupun Poltek.
     *
     * @param JurusanInstitusi $entity Persistent entity object of JurusanInstitusi
     */
    private function save(JurusanInstitusi $entity): void
    {
        $fts = [];
        $fts[] = $entity->getInstitusi()->getNamaInstitusi();
        $fts[] = $entity->getNamaJurusan();
        $fts[] = $entity->getKepalaJurusan();

        if (!empty($entity->getContactPhone())) {
            $fts[] = $entity->getContactPhone();
        }
        $unitProduksi = $entity->getUnitProduksi();
        if (!empty($unitProduksi) && !empty($unitProduksi->getNamaUnit())) {
            $fts[] = $unitProduksi->getNamaUnit();
        }

        $entity->setSearchFts($fts);
        $this->updatePesertaDidik($entity);
        $dudis = $entity->getMitraDudis();
        foreach ($dudis as $dudi) {
            $this->updateFtsMitraDudi($dudi, $entity);
        }

        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Assign or update FullTextSearch untuk Mitra Dudi dan link entity Mitra DUDI dengan entity Jurusan.
     *
     * @param MitraDudiJurusan $mitraDudi Persistent entity object of Mitra DUDI
     * @param JurusanInstitusi $jurusan   Persistent entity object of JurusanInstitusi
     */
    private function updateFtsMitraDudi(MitraDudiJurusan $mitraDudi, JurusanInstitusi $jurusan): void
    {
        $fts = [];
        $institusi = $jurusan->getInstitusi();
        $fts[] = $institusi->getNamaInstitusi();
        $fts[] = $jurusan->getNamaJurusan();
        $fts[] = $jurusan->getKepalaJurusan();
        $fts[] = $mitraDudi->getNamaUsaha();
        $fts[] = $mitraDudi->getNamaPimpinan();
        $fts[] = $mitraDudi->getJenisUsaha();

        if (!empty($mitraDudi->getContactPhone())) {
            $fts[] = $mitraDudi->getContactPhone();
        }
        if (!empty($mitraDudi->getAlamat())) {
            $fts[] = $mitraDudi->getAlamat();
        }

        $mitraDudi->setSearchFts($fts);
        $mitraDudi->setJurusan($jurusan);
    }

    /**
     * Assign or update list pesertaDidik dari JurusanInstitusi
     *
     * @param JurusanInstitusi $jurusan Persistent entity object of JurusanInstitusi
     */
    private function updatePesertaDidik(JurusanInstitusi $jurusan): void
    {
        $pesertaDidik = $jurusan->getPesertaDidik();

        if (empty($pesertaDidik)) {
            $jurusan->setPesertaDidik(null);
        } else {
            $dstPesertaDidik = [];

            foreach ($pesertaDidik as $item) {
                $tahun = trim($item['tahun']);
                $jumlah = trim($item['jumlah']);

                if (!empty($tahun) && !empty($jumlah)) {
                    $dstPesertaDidik[] = [
                        'tahun'  => $tahun,
                        'jumlah' => $jumlah
                    ];
                }
            }

            if (!empty($dstPesertaDidik)) {
                $jurusan->setPesertaDidik($dstPesertaDidik);
            } else {
                $jurusan->setPesertaDidik(null);
            }
        }
    }

}