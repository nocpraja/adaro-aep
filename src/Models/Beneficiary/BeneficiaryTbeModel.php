<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Beneficiary;

use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Entity\Beneficiary\BeneficiaryTbe;
use App\Entity\Beneficiary\LogAprvBeneficiary;
use App\Entity\Pendanaan\DanaBatch;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\Beneficiary\BeneficiaryInstitusiRepository;
use App\Repository\Beneficiary\BeneficiaryTbeRepository;
use App\Repository\Beneficiary\LogAprvBeneficiaryRepository;
use App\Repository\MasterData\KecamatanRepository;
use App\Repository\MasterData\KelurahanRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;

/**
 * Class class BeneficiaryTbeModel
 * @package App\Models\Beneficiary
 * @author  Tri N
 * @since   03/12/2018, modified: 13/08/2020 04:54
 */
final class BeneficiaryTbeModel
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BeneficiaryTbeRepository
     */
    private $repository;

    /**
     * @var BeneficiaryInstitusiRepository
     */
    private $institusiRepository;

    /**
     * @var DanaBatchRepository
     */
    private $programRepository;

    /**
     * @var KecamatanRepository
     */
    private $kecamatanRepository;

    /**
     * @var KelurahanRepository
     */
    private $kelurahanRepository;

    /**
     * @var AuditLogger
     */
    private $logger;
    /**
     * @var WorkflowConfigurer
     */
    private $workflowConfigurer;
    /**
     * @var LogAprvBeneficiaryRepository
     */
    private $logAprvBeneficiaryRepository;

    /**
     * BeneficiaryTbeModel constructor.
     *
     * @param BeneficiaryTbeRepository       $repository          Entity repository instance
     * @param BeneficiaryInstitusiRepository $institusiRepository Entity repository instance
     * @param DanaBatchRepository            $programRepository   Entity repository instance
     * @param KecamatanRepository            $kecamatanRepository Entity repository instance
     * @param KelurahanRepository            $kelurahanRepository Entity repository instance
     * @param WorkflowConfigurer             $workflowConfigurer  Workflow configurator
     * @param LogAprvBeneficiaryRepository   $logAprvBeneficiaryRepository
     * @param AuditLogger                    $logger              AuditLogger service
     */
    public function __construct(BeneficiaryTbeRepository $repository,
                                BeneficiaryInstitusiRepository $institusiRepository,
                                DanaBatchRepository $programRepository,
                                KecamatanRepository $kecamatanRepository,
                                KelurahanRepository $kelurahanRepository,
                                WorkflowConfigurer $workflowConfigurer,
                                LogAprvBeneficiaryRepository $logAprvBeneficiaryRepository,
                                AuditLogger $logger)
    {
        $this->em = $logger->getEntityManager();
        $this->repository = $repository;
        $this->institusiRepository = $institusiRepository;
        $this->programRepository = $programRepository;
        $this->kecamatanRepository = $kecamatanRepository;
        $this->kelurahanRepository = $kelurahanRepository;
        $this->logAprvBeneficiaryRepository = $logAprvBeneficiaryRepository;
        $this->workflowConfigurer = $workflowConfigurer;
        $this->logger = $logger;
    }

    /**
     * Menambahkan penerima manfaat peserta TechBase ke dalam program (DPT).
     *
     * @param BeneficiaryTbe $techBase Persistent entity object
     * @param DanaBatch      $program  Persistent entity object
     */
    public function addToProgram(BeneficiaryTbe $techBase, DanaBatch $program): void
    {
        $entity = $techBase->getBiodata();
        $collection = $entity->getPrograms();

        if ($collection->contains($program)) {
            throw new LogicException('Data penerima manfaat peserta TBE tidak boleh duplikat.');
        }

        $entity->addProgram($program);
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Remove penerima manfaat TechBase dari database.
     *
     * @param BeneficiaryTbe $techBase Persistent entity object
     *
     * @throws LogicException If an error is occured
     */
    public function delete(BeneficiaryTbe $techBase): void
    {
        $biodata = $techBase->getBiodata();
        if ($biodata->getStatusVerifikasi() > 0 || $biodata->getPrograms()->count() > 1) {
            throw new LogicException("Data peserta Tech Base Education tidak boleh dihapus.");
        }

        $this->logger->notice('HAPUS record peserta Tech Base Education.', BeneficiaryTbe::class,
            $techBase, [JsonController::CONTEXT_NON_REL]);
        $this->em->remove($techBase);
        $this->em->remove($biodata);
        $this->em->flush();
    }

    /**
     * Menampilkan daftar Individu pada program TechBase tertentu.
     *
     * @param DataQuery $dq         Request query
     * @param boolean   $forProgram Tampilkan beneficiary yang hanya ikut program tertentu
     *
     * @return Paginator
     */
    public function displayAll(DataQuery $dq, bool $forProgram): Paginator
    {
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
            $dq->getOffset(), $forProgram, $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @param BeneficiaryTbe $entity Persistent entity object
     *
     * @return LogAprvBeneficiary
     */
    public function fetchLogApproval(BeneficiaryTbe $entity)
    {
        return $this->logAprvBeneficiaryRepository->findOneBy(['tbe' => $entity, 'logAction' => WorkflowTags::STEP_REJECT], ['postedDate' => 'desc']);
    }

    /**
     * Get Doctrine Entity Manager.
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * Get BeneficiaryInstitusiRepository.
     * @return BeneficiaryInstitusiRepository
     */
    public function getInstitusiRepository(): BeneficiaryInstitusiRepository
    {
        return $this->institusiRepository;
    }

    /**
     * Get KecamatanRepository.
     * @return KecamatanRepository
     */
    public function getKecamatanRepository(): KecamatanRepository
    {
        return $this->kecamatanRepository;
    }

    /**
     * Get KelurahanRepository.
     * @return KelurahanRepository
     */
    public function getKelurahanRepository(): KelurahanRepository
    {
        return $this->kelurahanRepository;
    }

    /**
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * Get DanaBatchRepository.
     * @return DanaBatchRepository
     */
    public function getProgramRepository(): DanaBatchRepository
    {
        return $this->programRepository;
    }

    /**
     * Get BeneficiaryTbeRepository.
     * @return BeneficiaryTbeRepository
     */
    public function getRepository(): BeneficiaryTbeRepository
    {
        return $this->repository;
    }

    /**
     * Proses tambah record peserta TechBase baru.
     *
     * @param BeneficiaryTbe $techBase Unmanaged entity object
     */
    public function insert(BeneficiaryTbe $techBase): void
    {
        $this->save($techBase);
        $this->logger->info('TAMBAH record peserta Tech Base Education baru.', BeneficiaryTbe::class,
            $techBase, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Remove penerima manfaat peserta TechBase dari program (DPT). jika mengikuti lebih dr 1 program
     *
     * @param BeneficiaryTbe $techBase Persistent entity object
     * @param DanaBatch      $program  Persistent entity object
     */
    public function remove(BeneficiaryTbe $techBase, DanaBatch $program): void
    {
        $entity = $techBase->getBiodata();
        $entity->removeProgram($program);
        $this->em->persist($entity);
        $this->logger->notice('HAPUS record peserta Tech Base Education dari program Dana Batch.',
            BeneficiaryTbe::class, $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Proses update record peserta TechBase.
     *
     * @param BeneficiaryTbe $entity
     */
    public function update(BeneficiaryTbe $entity): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();
        if ($workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            $entity->setStatus(WorkflowTags::UPDATED);
            $entity->getBiodata()->setStatusVerifikasi(0);
            $entity->getBiodata()->setStatus(WorkflowTags::UPDATED);
        }

        $this->em->persist($entity);
        $this->em->flush();

        $this->logger->info('UPDATE record peserta Tech Base Education.', BeneficiaryTbe::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Proses simpan data peserta TechBase.
     *
     * @param BeneficiaryTbe $techBase Persistent entity object
     */
    private function save(BeneficiaryTbe $techBase): void
    {
        $biodata = $techBase->getBiodata();
        $biodata->setKompetensiWirausaha(null);

        $this->updateMapel($techBase);
        $this->updatePrestasi($techBase);
        $this->updatePelatihan($techBase);
        $this->updateFtsValues($biodata);

        $this->em->persist($biodata);
        $biodata->setTbe($techBase);
        $this->em->persist($techBase);
        $this->em->flush();
    }

    /**
     * Assign or update FullTextSearch values.
     *
     * @param BeneficiaryIndividu $biodata Persistent entity object
     */
    private function updateFtsValues(BeneficiaryIndividu $biodata): void
    {
        $fts = [];

        if (!empty($biodata->getNamaLengkap())) {
            $fts[] = $biodata->getNamaLengkap();
        }
        if (!empty($biodata->getNik())) {
            $fts[] = $biodata->getNik();
        }
        if (!empty($biodata->getTempatLahir())) {
            $fts[] = $biodata->getTempatLahir();
        }
        if (!empty($biodata->getPhone())) {
            $fts[] = $biodata->getPhone();
        }
        if (!empty($biodata->getEmail())) {
            $fts[] = $biodata->getEmail();
        }
        if (!empty($biodata->getAlamat())) {
            $fts[] = $biodata->getAlamat();
        }
        if (!empty($biodata->getFacebook())) {
            $fts[] = $biodata->getFacebook();
        }
        if (!empty($biodata->getInstagram())) {
            $fts[] = $biodata->getInstagram();
        }
        if (!empty($biodata->getPendidikan())) {
            $fts[] = $biodata->getPendidikan();
        }
        if (!empty($biodata->getJabatan())) {
            $fts[] = $biodata->getJabatan();
        }
        if (!empty($biodata->getStatusJabatan())) {
            $fts[] = $biodata->getStatusJabatan();
        }

        $kelurahan = $biodata->getKelurahan();
        $kecamatan = $biodata->getKecamatan();
        $kabupaten = $kecamatan->getKabupaten();
        $provinsi = $kabupaten->getProvinsi();

        $fts[] = $provinsi->getNamaProvinsi();
        $fts[] = $kabupaten->getNamaKabupaten();
        $fts[] = $kecamatan->getNamaKecamatan();
        $fts[] = $kelurahan->getNamaKelurahan();

        $biodata->setSearchFts($fts);
    }

    /**
     * Assign or update mapel dari peserta Techbase.
     *
     * @param BeneficiaryTbe $techBase Persistent entity object
     */
    private function updateMapel(BeneficiaryTbe $techBase): void
    {
        $srcMapel = $techBase->getMapel();

        if (empty($srcMapel)) {
            $techBase->setMapel(null);
        } else {
            $dstMapel = [];
            foreach ($srcMapel as $value) {
                $_val = trim($value['text']);
                if (!empty($_val)) {
                    $dstMapel[] = ['text' => $_val];
                }
            }
            if (!empty($dstMapel)) {
                $techBase->setMapel($dstMapel);
            } else {
                $techBase->setMapel(null);
            }
        }
    }

    /**
     * Assign or update list pelatihan dari peserta TechBase
     *
     * @param BeneficiaryTbe $techBase Persistent entity object
     */
    private function updatePelatihan(BeneficiaryTbe $techBase): void
    {
        $srcPelatihan = $techBase->getPelatihan();

        if (empty($srcPelatihan)) {
            $techBase->setPelatihan(null);
        } else {
            $dstPelatihan = [];
            foreach ($srcPelatihan as $value) {
                $_nama = trim($value['nama']);
                $_tahun = trim($value['tahun']);

                if (!empty($_nama) && !empty($_tahun)) {
                    $dstPelatihan[] = [
                        'nama' => $_nama,
                        'tahun' => $_tahun
                    ];
                }
            }
            if (!empty($dstPelatihan)) {
                $techBase->setPelatihan($dstPelatihan);
            } else {
                $techBase->setPelatihan(null);
            }
        }
    }

    /**
     * Assign or update list prestasi dari peserta TechBase
     *
     * @param BeneficiaryTbe $techBase Persistent entity object
     */
    private function updatePrestasi(BeneficiaryTbe $techBase): void
    {
        $srcPrestasi = $techBase->getPrestasi();

        if (empty($srcPrestasi)) {
            $techBase->setPrestasi(null);
        } else {
            $dstPrestasi = [];
            foreach ($srcPrestasi as $value) {
                $_nama = trim($value['nama']);
                $_tingkat = trim($value['tingkat']);
                $_tahun = trim($value['tahun']);

                if (!empty($_nama) && !empty($_tahun)) {
                    $dstPrestasi[] = [
                        'nama' => $_nama,
                        'tingkat' => $_tingkat,
                        'tahun' => $_tahun
                    ];
                }
            }
            if (!empty($dstPrestasi)) {
                $techBase->setPrestasi($dstPrestasi);
            } else {
                $techBase->setPrestasi(null);
            }
        }
    }

    /**
     * Proses workflow transition.
     *
     * @param BeneficiaryTbe $entity Persistent entity object
     * @param ActionObject   $data   Transient data object
     */
    public function actionTransition(BeneficiaryTbe $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();
        $transition = $data->getAction();
        $context = [
            'status' => $entity->getStatus(),
            'keterangan' => $data->getMessage(),
            'title' => ucfirst($transition) . ' beneficiary tech-based education ' . $entity->getBiodata()->getNamaLengkap()
        ];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum bisa dilakukan.');
        }

        $workflow->apply($entity, $transition, $context);
    }
}
