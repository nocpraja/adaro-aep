<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Beneficiary;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Entity\Beneficiary\BeneficiaryBeasiswa;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Entity\Beneficiary\LogAprvBeneficiary;
use App\Entity\Pendanaan\DanaBatch;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\Beneficiary\BeneficiaryBeasiswaRepository;
use App\Repository\Beneficiary\BeneficiaryInstitusiRepository;
use App\Repository\Beneficiary\LogAprvBeneficiaryRepository;
use App\Repository\MasterData\DataReferenceRepository;
use App\Repository\MasterData\KecamatanRepository;
use App\Repository\MasterData\KelurahanRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;


/**
 * Class class BeneficiaryBeasiswaModel
 * @package App\Models\Beneficiary
 * @author  Tri N
 * @since   13/11/2018, modified: 04/03/2020 22:28
 */
final class BeneficiaryBeasiswaModel
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BeneficiaryBeasiswaRepository
     */
    private $repository;

    /**
     * @var DanaBatchRepository
     */
    private $programRepository;

    /**
     * @var KecamatanRepository
     */
    private $kecamatanRepository;

    /**
     * @var KelurahanRepository
     */
    private $kelurahanRepository;

    /**
     * @var BeneficiaryInstitusiRepository
     */
    private $institusiRepository;

    /**
     * @var DataReferenceRepository
     */
    private $referenceRepository;

    /**
     * @var AuditLogger
     */
    private $logger;
    /**
     * @var WorkflowConfigurer
     */
    private $workflowConfigurer;
    /**
     * @var LogAprvBeneficiaryRepository
     */
    private $logAprvBeneficiaryRepository;


    /**
     * BeneficiaryBeasiswaModel constructor.
     *
     * @param EntityManagerInterface         $entityManager       Doctrine entity manager
     * @param BeneficiaryBeasiswaRepository  $repository          Entity repository instance
     * @param DanaBatchRepository            $programRepository   Entity repository instance
     * @param KecamatanRepository            $kecamatanRepository Entity repository instance
     * @param KelurahanRepository            $kelurahanRepository Entity repository instance
     * @param BeneficiaryInstitusiRepository $institusiRepository Entity repository instance
     * @param DataReferenceRepository        $referenceRepository Entity repository instance
     * @param WorkflowConfigurer             $workflowConfigurer  Workflow configurator
     * @param LogAprvBeneficiaryRepository   $logAprvBeneficiaryRepository
     * @param AuditLogger                    $logger              AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                BeneficiaryBeasiswaRepository $repository,
                                DanaBatchRepository $programRepository,
                                KecamatanRepository $kecamatanRepository,
                                KelurahanRepository $kelurahanRepository,
                                BeneficiaryInstitusiRepository $institusiRepository,
                                DataReferenceRepository $referenceRepository,
                                WorkflowConfigurer $workflowConfigurer,
                                LogAprvBeneficiaryRepository $logAprvBeneficiaryRepository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->programRepository = $programRepository;
        $this->kecamatanRepository = $kecamatanRepository;
        $this->kelurahanRepository = $kelurahanRepository;
        $this->institusiRepository = $institusiRepository;
        $this->referenceRepository = $referenceRepository;
        $this->logAprvBeneficiaryRepository = $logAprvBeneficiaryRepository;
        $this->workflowConfigurer = $workflowConfigurer;
        $this->logger = $logger;
    }

    /**
     * Menambahkan penerima manfaat peserta Beasiswa ke dalam program (DPT).
     *
     * @param BeneficiaryBeasiswa $beasiswa Persistent entity object
     * @param DanaBatch           $program  Persistent entity object
     *
     */
    public function addToProgram(BeneficiaryBeasiswa $beasiswa, DanaBatch $program): void
    {
        $entity = $beasiswa->getBiodata();
        $collection = $entity->getPrograms();
        if ($collection->contains($program)) {
            throw new LogicException('Data penerima manfaat peserta beasiswa tidak boleh duplikat.');
        }

        $entity->addProgram($program);
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Remove penerima manfaat Beasiswa dari database.
     *
     * @param BeneficiaryBeasiswa $beasiswa Persistent entity object
     *
     * @throws LogicException If an error is occured
     */
    public function delete(BeneficiaryBeasiswa $beasiswa): void
    {
        $biodata = $beasiswa->getBiodata();
        if ($biodata->getStatusVerifikasi() > 0 || $biodata->getPrograms()->count() > 1) {
            throw new LogicException("Data peserta beasiswa tidak boleh dihapus.");
        }

        $this->logger->notice('HAPUS record peserta beasiswa.', BeneficiaryBeasiswa::class,
                              $beasiswa, [JsonController::CONTEXT_NON_REL]);
        $this->em->remove($beasiswa);
        $this->em->remove($biodata);
        $this->em->flush();
    }

    /**
     * Menampilkan daftar Individu pada program Beasiswa tertentu.
     *
     * @param DataQuery $dq         Request query
     * @param boolean   $forProgram Tampilkan beneficiary yang hanya ikut program tertentu
     *
     * @return Paginator
     */
    public function displayAll(DataQuery $dq, bool $forProgram): Paginator
    {
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                      $dq->getOffset(), $forProgram, $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @param BeneficiaryBeasiswa $entity Persistent entity object
     *
     * @return LogAprvBeneficiary
     */
    public function fetchLogApproval(BeneficiaryBeasiswa $entity)
    {
        return $this->logAprvBeneficiaryRepository->findOneBy(['beasiswa' => $entity, 'logAction' => WorkflowTags::STEP_REJECT], ['postedDate' => 'desc']);
    }

    /**
     * Get Doctrine Entity Manager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * Get BeneficiaryInstitusiRepository.
     *
     * @return BeneficiaryInstitusiRepository
     */
    public function getInstitusiRepository(): BeneficiaryInstitusiRepository
    {
        return $this->institusiRepository;
    }

    /**
     * Get KecamatanRepository.
     *
     * @return KecamatanRepository
     */
    public function getKecamatanRepository(): KecamatanRepository
    {
        return $this->kecamatanRepository;
    }

    /**
     * Get KelurahanRepository.
     *
     * @return KelurahanRepository
     */
    public function getKelurahanRepository(): KelurahanRepository
    {
        return $this->kelurahanRepository;
    }

    /**
     * Get Audit Logger Service.
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * Get ProgramRepository.
     *
     * @return DanaBatchRepository
     */
    public function getProgramRepository(): DanaBatchRepository
    {
        return $this->programRepository;
    }

    /**
     * Get ReferenceRepository.
     *
     * @return DataReferenceRepository
     */
    public function getReferenceRepository(): DataReferenceRepository
    {
        return $this->referenceRepository;
    }

    /**
     * @return BeneficiaryBeasiswaRepository
     */
    public function getRepository(): BeneficiaryBeasiswaRepository
    {
        return $this->repository;
    }

    /**
     * Proses tambah record peserta Beasiswa baru.
     *
     * @param BeneficiaryBeasiswa $beasiswa Unmanaged entity object
     */
    public function insert(BeneficiaryBeasiswa $beasiswa): void
    {
        $this->save($beasiswa);
        $this->logger->info('TAMBAH record Beasiswa baru.', BeneficiaryBeasiswa::class,
                            $beasiswa, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Remove peserta Beasiswa dari suatu program (DPT).
     *
     * @param BeneficiaryBeasiswa $beasiswa Persistent entity object
     * @param DanaBatch           $program  Persistent entity object
     */
    public function remove(BeneficiaryBeasiswa $beasiswa, DanaBatch $program)
    {
        $biodata = $beasiswa->getBiodata();
        $biodata->removeProgram($program);
        $this->em->persist($biodata);
        $this->logger->notice('HAPUS record peserta Beasiswa dari program Dana Batch.', BeneficiaryIndividu::class,
                              $biodata, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Proses update record peserta Beasiswa.
     *
     * @param BeneficiaryBeasiswa $entity
     */
    public function update(BeneficiaryBeasiswa $entity): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();
        if ($workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            $entity->setStatus(WorkflowTags::UPDATED);
            $entity->getBiodata()->setStatusVerifikasi(0);
            $entity->getBiodata()->setStatus(WorkflowTags::UPDATED);
        }

        $this->em->persist($entity);
        $this->em->flush();

        $this->logger->info('UPDATE record Beasiswa.', BeneficiaryBeasiswa::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Proses simpan data peserta beasiswa.
     *
     * @param BeneficiaryBeasiswa $beasiswa Persistent entity object
     */
    private function save(BeneficiaryBeasiswa $beasiswa): void
    {
        $biodata = $beasiswa->getBiodata();
        $biodata->setKompetensiWirausaha(null);

        $this->updatePrestasi($beasiswa);
        $this->updateFtsValues($biodata);

        $this->em->persist($biodata);
        $biodata->setBeasiswa($beasiswa);
        $this->em->persist($beasiswa);
        $this->em->flush();
    }

    /**
     * Assign or update FullTextSearch values.
     *
     * @param BeneficiaryIndividu $biodata Persistent entity object
     */
    private function updateFtsValues(BeneficiaryIndividu $biodata): void
    {
        $fts = [];

        if (!empty($biodata->getNamaLengkap())) {
            $fts[] = $biodata->getNamaLengkap();
        }
        if (!empty($biodata->getNik())) {
            $fts[] = $biodata->getNik();
        }
        if (!empty($biodata->getTempatLahir())) {
            $fts[] = $biodata->getTempatLahir();
        }
        if (!empty($biodata->getPhone())) {
            $fts[] = $biodata->getPhone();
        }
        if (!empty($biodata->getEmail())) {
            $fts[] = $biodata->getEmail();
        }
        if (!empty($biodata->getAlamat())) {
            $fts[] = $biodata->getAlamat();
        }
        if (!empty($biodata->getFacebook())) {
            $fts[] = $biodata->getFacebook();
        }
        if (!empty($biodata->getInstagram())) {
            $fts[] = $biodata->getInstagram();
        }

        $kelurahan = $biodata->getKelurahan();
        $kecamatan = $biodata->getKecamatan();
        $kabupaten = $kecamatan->getKabupaten();
        $provinsi = $kabupaten->getProvinsi();

        $fts[] = $provinsi->getNamaProvinsi();
        $fts[] = $kabupaten->getNamaKabupaten();
        $fts[] = $kecamatan->getNamaKecamatan();
        $fts[] = $kelurahan->getNamaKelurahan();

        $biodata->setSearchFts($fts);
    }

    /**
     * Assign or update prestasi dari peserta Beasiswa.
     *
     * @param BeneficiaryBeasiswa $beasiswa Persistent entity object
     */
    private function updatePrestasi(BeneficiaryBeasiswa $beasiswa): void
    {
        $srcPrestasi = $beasiswa->getPrestasi();

        if (empty($srcPrestasi)) {
            $beasiswa->setPrestasi(null);
        } else {
            $dstPrestasi = [];
            foreach ($srcPrestasi as $value) {
                $_val = trim($value['text']);
                if (!empty($_val)) {
                    $dstPrestasi[] = ['text' => $_val];
                }
            }
            if (!empty($dstPrestasi)) {
                $beasiswa->setPrestasi($dstPrestasi);
            } else {
                $beasiswa->setPrestasi(null);
            }
        }
    }

    /**
     * Proses workflow transition.
     *
     * @param BeneficiaryBeasiswa  $entity Persistent entity object
     * @param ActionObject $data   Transient data object
     */
    public function actionTransition(BeneficiaryBeasiswa $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();
        $transition = $data->getAction();
        $context = [
            'status' => $entity->getStatus(),
            'keterangan' => $data->getMessage(),
            'title' => ucfirst($transition) . ' beneficiary beasiswa ' . $entity->getBiodata()->getNamaLengkap()
        ];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum bisa dilakukan.');
        }

        $workflow->apply($entity, $transition, $context);
    }
}
