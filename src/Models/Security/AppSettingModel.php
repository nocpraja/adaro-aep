<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Security;


use App\Component\Controller\JsonController;
use App\Component\DataObject\SortOrFilter;
use App\Entity\Security\AppSetting;
use App\Repository\Security\AppSettingRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class AppSettingModel
 *
 * @package App\Models\Security
 * @author  Mark Melvin
 * @since   23/02/2020, modified: 23/03/2020 2:00
 */
final class AppSettingModel
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var AppSettingRepository
     */
    private $repository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * AppSettingModel constructor.
     *
     * @param EntityManagerInterface  $entityManager Doctrine entity manager
     * @param AppSettingRepository $repository    Entity repository instance
     * @param AuditLogger             $logger        AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                AppSettingRepository $repository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * Remove AppSetting
     *
     * @param AppSetting $entity Persistent entity object
     * @throws \LogicException
     */
    public function delete(AppSetting $entity): void
    {
        $this->em->remove($entity);
        $this->logger->notice('HAPUS record AppSetting.', AppSetting::class,
                              $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Manampilkan daftar AppSetting
     *
     * @param SortOrFilter[] $filters Filter kriteria
     * @param SortOrFilter[] $sorts   Sort kriteria
     * @param integer        $limit   Jumlah record untuk ditampilkan
     * @param integer        $offset  Posisi record awal
     *
     * @return Paginator
     */
    public function displayAllAppSetting(array $filters, array $sorts, int $limit, int $offset): Paginator
    {
        $query = $this->repository->findAllByCriteria($filters, $sorts, $limit, $offset);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Get Doctrine Entity Manager
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * Get AppSetting Repository
     *
     * @return AppSettingRepository
     */
    public function getRepository(): AppSettingRepository
    {
        return $this->repository;
    }

    /**
     * Get Audit Logger Service
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * Proses simpan AppSetting
     *
     * @param AppSetting $entity Persistent entity object
     */
    public function save(AppSetting $entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * Daftar section yang berlaku
     */
    public function getSections(): array
    {
        $query = $this->em->createQuery('SELECT DISTINCT s.section FROM App:Security\AppSetting s');
        $sections = $query->getResult();
        return $sections;
    }
    
    /**
     * Ambil nilai setting yang ada
     *
     * @param String $section
     * @param String $attr
     * @return String|null
     */
    public function getAttributeValue(String $section, String $attr): ?String
    {
        $entity = $this->getRepository()->findBySectionAndAttribute($section, $attr);
        if (empty($entity)) {
            return '';
        }

        return $entity->getAttributeValue();
    }

}