<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Security;


use App\Component\Controller\JsonController;
use App\Component\DataObject\MenuItemDto;
use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Security\MenuAccessRule;
use App\Entity\Security\MenuItem;
use App\Entity\Security\UserGroup;
use App\Repository\Security\MenuAccessRuleRepository;
use App\Repository\Security\MenuItemRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MenuItemModel
 *
 * @package App\Models
 * @author  Ahmad Fajar
 * @since   31/03/2019, modified: 11/04/2019 14:06
 */
final class AccessRightsModel
{

    const ADMIN_RULES = ['READ', 'EDIT', 'DELETE'];

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MenuItemRepository
     */
    private $repository;

    /**
     * @var MenuAccessRuleRepository
     */
    private $maclRepository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * MenuItemModel constructor.
     *
     * @param EntityManagerInterface   $manager        Doctrine entity manager
     * @param MenuItemRepository       $repository     Entity repository instance
     * @param MenuAccessRuleRepository $maclRepository Entity repository instance
     * @param AuditLogger              $logger         AuditLogger service
     */
    public function __construct(EntityManagerInterface $manager, MenuItemRepository $repository,
                                MenuAccessRuleRepository $maclRepository, AuditLogger $logger)
    {
        $this->em = $manager;
        $this->repository = $repository;
        $this->maclRepository = $maclRepository;
        $this->logger = $logger;
    }

    /**
     * Find available menu items for the given criterias.
     *
     * @param UserGroup      $group     The user's group
     * @param SortOrFilter[] $criterias Filter criterias
     *
     * @return MenuItemDto[]
     */
    public function findMenus(?UserGroup $group, array $criterias = [])
    {
        $results = [];

        if ($group === null || $group->isAdmin()) {
            $query = $this->repository->findAllByCriteria($criterias);
            $menus = $query->useResultCache(true, MenuItemRepository::CACHE_TTL)->getResult();

            /** @var MenuItem $item */
            foreach ($menus as $item) {
                if ($item->getEnabled() === true) {
                    $results[] = MenuItemDto::create($item);
                }
            }
        } else {
            $criterias[] = QueryExpressionHelper::createCriteria(['menuAccessRule.userGroup' => $group]);
            $query = $this->maclRepository->findAllByCriteria($criterias);
            $accessRules = $query->getResult();

            /** @var MenuAccessRule $rule */
            foreach ($accessRules as $rule) {
                $item = $rule->getMenuItem();
                if ($item->getEnabled() === true) {
                    $results[] = MenuItemDto::create($item)->setAccessRules($rule->getAccess());
                }
            }
        }

        return $results;
    }

    /**
     * Get Audit Logger Service
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * Insert or Update hak akses grup pengguna.
     *
     * @param UserGroup|null $group Grup pengguna
     * @param array          $items Daftar hak akses
     */
    public function insertOrUpdate(?UserGroup $group, array $items): void
    {
        foreach ($items as $item) {
            $menu = $this->repository->find($item['menuId']);
            $acl = $this->maclRepository->findOneBy(['userGroup' => $group, 'menuItem' => $menu]);

            if (empty($acl)) {
                $acl = new MenuAccessRule();
                $acl->setUserGroup($group)
                    ->setMenuItem($menu);
            }

            $rules = empty($item['accessRules']) ? null : $item['accessRules'];
            if (!empty($rules) && !in_array('READ', $rules)) {
                array_unshift($rules, 'READ');
            }

            $acl->setAccess($rules);
            $this->em->persist($acl);
        }

        $this->logger->notice('UPDATE hak akses grup pengguna.', UserGroup::class,
                              $group, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Load all available navigation Menus for the given user's group.
     *
     * @param UserGroup $group The user's group
     *
     * @return MenuItemDto[]
     */
    public function loadNavigationMenus(UserGroup $group)
    {
        $criterias = QueryExpressionHelper::createCriterias(['menuItem.parent' => null, 'menuItem.enabled' => true]);
        $rootMenus = $this->findMenus($group, $criterias);
        $mainMenus = [];

        foreach ($rootMenus as $menu) {
            if ($group->isAdmin()) {
                $menu->setAccessRules(self::ADMIN_RULES);
            }
            if (!empty($menu->getAccessRules())) {
                $menu->setChildren($this->loadSubMenus($group, $menu));
                $mainMenus[] = $menu;
            }
        }

        return $mainMenus;
    }

    /**
     * Load all available menu items for the given user's group in TreeList structures.
     *
     * @param UserGroup|null $group The user's group
     *
     * @return MenuItemDto[]
     */
    public function loadTreeItems(?UserGroup $group): array
    {
        $criterias = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'menuItem.parent', 'value' => null],
                ['property' => 'menuItem.enabled', 'value' => true],
                ['property' => 'menuItem.urlType', 'value' => 4, 'operator' => 'lt']
            ]
        );
        $rootMenus = $this->findMenus(null, $criterias);

        if ($group !== null && !$group->isAdmin()) {
            $criterias = QueryExpressionHelper::createCriterias(
                [
                    ['property' => 'menuAccessRule.userGroup', 'value' => $group],
                    ['property' => 'menuItem.enabled', 'value' => true],
                ]
            );
            $query = $this->maclRepository->findAllByCriteria($criterias);
            $accessRules = $query->getResult();
        }

        foreach ($rootMenus as $menu) {
            if (isset($accessRules)) {
                /** @var MenuAccessRule $rule */
                foreach ($accessRules as $rule) {
                    if ($rule->getMenuItem()->getMenuId() == $menu->getId()) {
                        $menu->setAccessRules($rule->getAccess());
                        break;
                    }
                }
            } elseif ($group !== null && $group->isAdmin()) {
                $menu->setAccessRules(self::ADMIN_RULES);
            }
            $menu->setChildren($this->loadSubItems($group, $menu, (isset($accessRules) ? $accessRules : null)));
        }

        return $rootMenus;
    }

    /**
     * Load all available child items.
     *
     * @param UserGroup|null $group       The user's group
     * @param MenuItemDto    $menu        Parent item
     * @param array|null     $accessRules Collection of access rules
     *
     * @return MenuItemDto[]
     */
    private function loadSubItems(?UserGroup $group, MenuItemDto $menu, array $accessRules = null)
    {
        $parent = $menu->toEntity();
        $criterias = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'menuItem.parent', 'value' => $parent],
                ['property' => 'menuItem.enabled', 'value' => true],
                ['property' => 'menuItem.urlType', 'value' => 4, 'operator' => 'lt']
            ]
        );
        $menus = $this->findMenus(null, $criterias);

        if (empty($menus)) {
            $menu->setChildren(null);
        } else {
            foreach ($menus as $item) {
                if (!empty($accessRules)) {
                    /** @var MenuAccessRule $rule */
                    foreach ($accessRules as $rule) {
                        if ($rule->getMenuItem()->getMenuId() == $item->getId()) {
                            $item->setAccessRules($rule->getAccess());
                            break;
                        }
                    }
                } elseif ($group !== null && $group->isAdmin()) {
                    $item->setAccessRules(self::ADMIN_RULES);
                }

                $item->setChildren($this->loadSubItems($group, $item, $accessRules));
            }
        }

        return $menus;
    }

    /**
     * Load ChildMenus for the given user's group.
     *
     * @param UserGroup   $group The user's group
     * @param MenuItemDto $menu  Parent menu
     *
     * @return MenuItemDto[]
     */
    private function loadSubMenus(UserGroup $group, MenuItemDto $menu)
    {
        $parent = $menu->toEntity();
        $criterias = QueryExpressionHelper::createCriterias(['menuItem.parent'  => $parent,
                                                             'menuItem.enabled' => true]);
        $menus = $this->findMenus($group, $criterias);
        $submenus = [];

        if (empty($menus)) {
            $menu->setChildren(null);
        } else {
            foreach ($menus as $item) {
                if ($group->isAdmin()) {
                    $item->setAccessRules(self::ADMIN_RULES);
                }
                if (!empty($item->getAccessRules())) {
                    $item->setChildren($this->loadSubMenus($group, $item));
                    $submenus[] = $item;
                }
            }
        }

        return $submenus;
    }

}