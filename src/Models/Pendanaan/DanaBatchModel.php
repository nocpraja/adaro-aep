<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\EntityDanaInterface;
use App\Entity\Pendanaan\TransaksiDanaBatch;
use App\Entity\Security\UserAccount;
use App\Models\Pendanaan\Workflow\DanaBatchConfigurer;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\MsNumberingRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use App\Repository\Pendanaan\TransaksiDanaBatchRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;

/**
 * Class DanaBatchModel
 *
 * @package App\Models\Pendanaan
 * @author  Ahmad Fajar
 * @since   30/04/2019, modified: 11/07/2019 2:05
 */
final class DanaBatchModel implements PendanaanDomainModel
{

    /**
     * @var DanaBatchRepository
     */
    private $repository;

    /**
     * @var TransaksiDanaBatchRepository
     */
    private $transactionRepository;

    /**
     * @var MsNumberingRepository
     */
    private $numberingRepository;

    /**
     * @var DanaBatchConfigurer
     */
    private $workflowConfigurator;

    /**
     * @var DanaProgramModel
     */
    private $programModel;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * DanaBatchModel constructor.
     *
     * @param DanaBatchRepository          $repository            Entity repository instance
     * @param TransaksiDanaBatchRepository $transactionRepository Entity repository instance
     * @param DanaBatchConfigurer          $workflowConfigurator  Dana Batch workflow configurator
     * @param DanaProgramModel             $programModel          Dana Program domain model
     */
    public function __construct(DanaBatchRepository $repository, TransaksiDanaBatchRepository $transactionRepository,
                                DanaBatchConfigurer $workflowConfigurator, DanaProgramModel $programModel)
    {
        $this->repository = $repository;
        $this->transactionRepository = $transactionRepository;
        $this->workflowConfigurator = $workflowConfigurator;
        $this->programModel = $programModel;
        $this->numberingRepository = $programModel->getNumberingRepository();
        $this->logger = $programModel->getLogger();
    }

    /**
     * @inheritDoc
     */
    public function actionTransition(EntityDanaInterface $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();
        $transition = $data->getAction();
        $context = ['status' => $entity->getStatus(), 'keterangan' => $data->getMessage()];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum sampai pada tempatnya.');
        }
        if ($transition == WorkflowTags::STEP_CLOSING && !$this->canClose($entity)) {
            throw new LogicException('Proses TUTUP Dana Batch belum bisa dilakukan, masih ada Kegiatan yang belum ditutup.');
        }

        $workflow->apply($entity, $transition, $context);
    }

    /**
     * Mencatat transaksi penambahan ataupun pengurangan dana.
     * Dipergunakan oleh pendanaan lainnya sesuai dengan keperluan bisnis process.
     *
     * @param DanaBatch $danaBatch      Persistent entity object
     * @param float     $jumlah         Jumlah penambahan/pengurangan dana
     * @param string    $refcode        Kode referensi transaksi
     * @param string    $name           Nama transaksi
     * @param bool      $saveAllocation Catat alokasi dana dan kalkulasi ulang saldo Dana Program?
     *
     * @throws ORMException
     */
    public function appendTransaction(DanaBatch $danaBatch, float $jumlah, string $refcode,
                                      ?string $name, bool $saveAllocation = false): void
    {
        if (empty($name)) {
            if (substr($refcode, 0, 2) == 'DP') {
                $name = $jumlah < 0 ? 'Pengurangan jumlah Dana Batch' : 'Penambahan jumlah Dana Batch';
            } else {
                $name = $jumlah < 0 ? 'Pengurangan jumlah Dana' : 'Penambahan jumlah Dana';
            }
        }

        $lastRecord = $this->lastTransaction($danaBatch);

        $entity = new TransaksiDanaBatch();
        $entity->setTitle($name)
               ->setRefcode($refcode)
               ->setJumlah($jumlah)
               ->setTahunAwal($danaBatch->getTahun())
               ->setDanaBatch($danaBatch);

        $saldo = !is_null($lastRecord) ? ($lastRecord->getSaldo() + $jumlah) : $jumlah;
        $entity->setSaldo($saldo);
        $this->transactionRepository->save($entity);

        if ($saveAllocation) {
            $alokasi = $danaBatch->getAlokasi();
            $danaBatch->setAlokasi($alokasi + ($jumlah * -1));
        }

        $this->logger->getEntityManager()->flush();
        $this->logger->info($this->logger->formatMessage($name), TransaksiDanaBatch::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * @inheritDoc
     */
    public function canClose(EntityDanaInterface $entity): bool
    {
        $em = $this->logger->getEntityManager();

        /** @var KegiatanRepository $repo */
        $repo = $em->getRepository(Kegiatan::class);
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'danaBatch', 'value' => $entity, 'operator' => 'eq'],
                // sementara saja sebelum entity Kegiatan diperbaiki
                ['property' => 'closed', 'value' => false, 'operator' => 'eq'],
            ]
        );

        $results = $repo->findAllByCriteria($filters)
                        ->getResult();

        return empty($results);
    }

    /**
     * Mendapatkan data program pendanaan yang bisa dijadikan sebagai sumber dana untuk revisi Dana Batch.
     *
     * @param DanaBatch $entity Persistent entity object
     *
     * @return array
     */
    public function collectSumberDanaRevisi(DanaBatch $entity): array
    {
        $items = [];
        $parent = $entity->getDanaProgram();
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'id', 'value' => $entity->getId(), 'operator' => 'neq'],
                ['property' => 'danaProgram', 'value' => $parent, 'operator' => 'eq'],
                ['property' => 'status',
                 'value'    => [WorkflowTags::ONGOING, WorkflowTags::REVISED, WorkflowTags::REVISI_REJECTED],
                 'operator' => 'in'],
            ]
        );
        $results = $this->repository->findAllByCriteria($filters)->getResult();

        /** @var DanaBatch $result */
        foreach ($results as $result) {
            $items[] = ['kode'  => $result->getKode(),
                        'title' => $result->getTitle(),
                        'saldo' => $result->getDanaTersedia()];
        }
        $items[] = ['kode'  => $parent->getKode(),
                    'title' => $parent->getTitle(),
                    'saldo' => $parent->getDanaTersedia()];

        return $items;
    }

    /**
     * Create Dana Batch.
     *
     * @param DanaBatch|EntityDanaInterface $entity Unmanaged entity object
     *
     * @throws ORMException
     */
    public function create(EntityDanaInterface $entity): void
    {
        if ($entity instanceof DanaBatch) {
            $year = substr($entity->getTahun(), -2);
        } else {
            $year = date('y');
        }

        $numbering = $this->numberingRepository->nextSequence('Pendanaan Batch', 'PB-' . $year);
        $title = 'SETUP ' . $entity->getNamaProgram();

        $entity->setStatus(WorkflowTags::NEW)
               ->setAnggaran($entity->getNewValue())
               ->setKode($numbering->toStringCode(4))
               ->setTags(null);

        $donaturs = $entity->getDonaturs()->toArray();
        $entity->setDonaturs($donaturs);
        $this->repository->save($entity);
        $this->logger->getEntityManager()->flush();

        $this->logger->info($this->logger->formatMessage($title), DanaBatch::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);

        $this->appendTransaction($entity, $entity->getAnggaran(), $entity->getKode(), $title);
        $this->programModel->appendTransaction($entity->getDanaProgram(), $entity->getAnggaran() * -1,
                                               $entity->getKode(), $title, true);
    }

    /**
     * Hapus Dana Batch berikut dengan histori transaksinya.
     *
     * @param DanaBatch|EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function delete(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            throw new LogicException('Dana Batch telah dikunci. ' .
                                     'Proses penghapusan data sudah tidak diperkenankan lagi.');
        }

        $em = $this->logger->getEntityManager();
        $parent = $entity->getDanaProgram();
        $repo = $em->getRepository(Kegiatan::class);
        $result = $repo->findBy(['danaBatch' => $entity], [], 1);

        if (!empty($result)) {
            throw new LogicException('Dana Batch tidak boleh dihapus, masih ada data yang terkait dengannya.');
        }

        $anggaran = $entity->getAnggaran();
        $refcode = $entity->getKode();
        $title = 'HAPUS ' . $entity->getNamaProgram();
        $em->remove($entity);

        $this->logger->notice($this->logger->formatMessage($title), DanaBatch::class, $entity,
                              [JsonController::CONTEXT_NON_REL]);
        $this->programModel->appendTransaction($parent, $anggaran, $refcode, $title, true);
    }

    /**
     * Menampilkan data Pendanaan Batch.
     *
     * @param DataQuery                    $dq   Request query
     * @param UserAccount|AppUserInterface $user UserAccount yang sedang login
     *
     * @return Paginator
     */
    public function displayAll(DataQuery $dq, AppUserInterface $user = null): Paginator
    {
        if (!is_null($user) && !is_null($user->getMitraCompany())) {
            $filters = $dq->getFilters();
            $filters[] = QueryExpressionHelper::createCriteria(['property' => 'batch.mitra',
                                                                'value'    => $user->getMitraCompany()]);
            $dq->setFilters($filters);
        }
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                      $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Menampilkan data riwayat transaksi Pendanaan Batch.
     *
     * @param DataQuery                    $dq   Request query
     * @param UserAccount|AppUserInterface $user UserAccount yang sedang login
     *
     * @return Paginator
     */
    public function displayTransactionHistories(DataQuery $dq, AppUserInterface $user = null): Paginator
    {
        $query = $this->transactionRepository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                                 $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @inheritDoc
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * @inheritDoc
     */
    public function getNumberingRepository(): MsNumberingRepository
    {
        return $this->numberingRepository;
    }

    /**
     * @return DanaProgramModel
     */
    public function getProgramModel(): DanaProgramModel
    {
        return $this->programModel;
    }

    /**
     * @return DanaBatchRepository
     */
    public function getRepository(): DanaBatchRepository
    {
        return $this->repository;
    }

    /**
     * @return TransaksiDanaBatchRepository
     */
    public function getTransactionRepository(): TransaksiDanaBatchRepository
    {
        return $this->transactionRepository;
    }

    /**
     * Menampilkan informasi transaksi terakhir Dana Batch.
     *
     * @param DanaBatch           $batch   Persistent entity object
     * @param SortOrFilter[]|null $filters Additional filters to apply
     *
     * @return TransaksiDanaBatch|null
     */
    public function lastTransaction(DanaBatch $batch, array $filters = null): ?TransaksiDanaBatch
    {
        $_filters[] = QueryExpressionHelper::createCriteria(['property' => 'transaksi.danaBatch', 'value' => $batch]);

        if (!empty($filters)) {
            foreach ($filters as $filter) {
                if ($filter instanceof SortOrFilter) {
                    $_filters[] = $filter;
                }
            }
        }

        $result = $this->transactionRepository->getLastNRecords(1, $_filters);

        return (empty($result) ? null : $result[0]);
    }

    /**
     * Proses request revisi anggaran Dana Batch.
     *
     * @param DanaBatch|EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function revisi(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_REQUEST)) {
            throw new LogicException('Request revisi Dana Batch belum dapat dilakukan.');
        }

        $title = 'REQUEST REVISI ' . $entity->getNamaProgram();
        $anggaran = $entity->getAnggaran();
        $tags = $entity->getTags();
        $revnum = empty($tags) ? 1 : $tags['revnum'] + 1;
        $fromDanaProgram = 0;
        $tambahanDana = 0;

        $requests = $entity->getRequests();
        foreach ($requests as $request) {
            if (substr($request->getRefcode(), 0, 2) == 'DP') {
                $request->setTitle($entity->getDanaProgram()->getTitle());
                $fromDanaProgram = $request->getJumlah();
            } else {
                $danaBatch = $this->repository->findOneBy(['kode' => $request->getRefcode()]);
                $request->setTitle($danaBatch->getTitle());
            }

            $tambahanDana += $request->getJumlah();
            $request->setRevnum($revnum)
                    ->setStatus(WorkflowTags::NEW);
            $entity->addRevisiAnggaran($request);
        }

        $entity->setAnggaran($anggaran + $tambahanDana)
               ->setTags(['request' => true, 'revnum' => $revnum]);

        $workflow->apply($entity, WorkflowTags::STEP_REQUEST, [], $this->logger->getEntityManager());
        $this->appendTransaction($entity, $tambahanDana, $entity->getKode(), $title);

        if ($fromDanaProgram > 0) {
            $this->programModel->appendTransaction($entity->getDanaProgram(), ($fromDanaProgram * -1),
                                                   $entity->getKode(), $title, true);
        }

        foreach ($requests as $item) {
            if (substr($item->getRefcode(), 0, 2) == 'BP') {
                $danaBatch = $this->repository->findOneBy(['kode' => $item->getRefcode()]);

                if (!is_null($danaBatch)) {
                    $this->appendTransaction($danaBatch, ($item->getJumlah() * -1),
                                             $entity->getKode(), $title, true);
                }
            }
        }
    }

    /**
     * Simpan perubahan data Dana Batch.
     *
     * @param DanaBatch|EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function update(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            throw new LogicException('Dana Batch telah dikunci untuk pembaruan. ' .
                                     'Lakukan request perubahan dana untuk memperbaruinya.');
        }

        $donaturs = $entity->getDonaturs()->toArray();
        $entity->setDonaturs($donaturs);

        $selisih = $entity->getNewValue() - $entity->getAnggaran();
        $entity->setAnggaran($entity->getNewValue())
               ->setTags(null);
        $workflow->apply($entity, WorkflowTags::STEP_UPDATE, [], $this->logger->getEntityManager());

        if ($selisih != 0.0) {
            $title = 'REVISI ' . $entity->getNamaProgram();
            $this->logger->info($this->logger->formatMessage($title), DanaBatch::class, $entity,
                                [JsonController::CONTEXT_NON_REL]);

            $this->appendTransaction($entity, $selisih, $entity->getKode(), $title);
            $this->programModel->appendTransaction($entity->getDanaProgram(), ($selisih * -1),
                                                   $entity->getKode(), $title, true);
        }
    }

}