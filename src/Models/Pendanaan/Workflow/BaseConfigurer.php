<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan\Workflow;


use App\Component\Workflow\Transition;
use App\Component\Workflow\WorkflowInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class BaseConfigurer
 *
 * @package App\Models\Pendanaan\Workflow
 * @author  Ahmad Fajar
 * @since   27/04/2019, modified: 02/05/2019 2:22
 */
abstract class BaseConfigurer
{
    /**
     * @var string[]
     */
    protected $places = [];

    /**
     * @var Transition[]
     */
    protected $transitions = [];

    /**
     * @var string
     */
    protected $startPlace = WorkflowTags::NEW;

    /**
     * @var WorkflowInterface
     */
    protected $workflow = null;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;


    /**
     * BaseConfigurator constructor.
     *
     * @param EventDispatcherInterface $dispatcher Symfony event dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->createPlaces();
        $this->createTransitions();
        $this->createWorkflow();

    }

    /**
     * Get a workflow for Dana Global.
     *
     * @return WorkflowInterface
     */
    public function getWorkflow(): WorkflowInterface
    {
        return $this->workflow;
    }

    abstract protected function createPlaces(): void;

    abstract protected function createTransitions(): void;

    abstract protected function createWorkflow(): void;

}