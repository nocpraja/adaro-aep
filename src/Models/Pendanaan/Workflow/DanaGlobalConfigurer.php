<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan\Workflow;


use App\Component\Workflow\Definition;
use App\Component\Workflow\Transition;
use App\Component\Workflow\Visitor\MarkingVisitorImpl;
use App\Component\Workflow\WorkflowImpl;

/**
 * Class DanaGlobalConfigurator
 *
 * @package App\Models\Pendanaan\Workflow
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 02/05/2019 2:23
 */
final class DanaGlobalConfigurer extends BaseConfigurer
{

    protected function createPlaces(): void
    {
        $this->places = [
            WorkflowTags::NEW,
            WorkflowTags::UPDATED,
            WorkflowTags::REJECTED,
            WorkflowTags::VERIFIED
        ];
    }

    protected function createTransitions(): void
    {
        $this->transitions = [
            new Transition([WorkflowTags::NEW, WorkflowTags::UPDATED, WorkflowTags::REJECTED], WorkflowTags::UPDATED,
                           WorkflowTags::STEP_UPDATE),
            new Transition([WorkflowTags::NEW, WorkflowTags::UPDATED], WorkflowTags::VERIFIED,
                           WorkflowTags::STEP_VERIFY),
            new Transition([WorkflowTags::NEW, WorkflowTags::UPDATED], WorkflowTags::REJECTED,
                           WorkflowTags::STEP_REJECT),
        ];
    }

    protected function createWorkflow(): void
    {
        $definition = new Definition($this->places, $this->transitions, $this->startPlace);
        $this->workflow = new WorkflowImpl($definition, new MarkingVisitorImpl('status'),
                                           $this->dispatcher, 'dana_global');
    }

}