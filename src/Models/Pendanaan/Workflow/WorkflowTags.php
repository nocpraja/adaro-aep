<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan\Workflow;


/**
 * Class WorkflowTags
 * @package App\Models\Pendanaan\Workflow
 * @author  Ahmad Fajar
 * @since   26/04/2019, modified: 18/05/2019 6:25
 */
final class WorkflowTags
{
    // workflow states or places
    const NEW = 'NEW';
    const UPDATED = 'UPDATED';
    const SUBMITTED = 'SUBMITTED';
    const PLANNED = 'PLANNED';
    const ONGOING = 'ONGOING';
    const CLOSED = 'CLOSED';
    const VERIFIED = 'VERIFIED';
    const APPROVED = 'APPROVED';
    const REJECTED = 'REJECTED';
    const VERIFIED_1 = 'VERIFIED_1';
    const VERIFIED_2 = 'VERIFIED_2';
    const APPROVED_1 = 'APPROVED_1';
    const APPROVED_2 = 'APPROVED_2';
    const APPROVED_3 = 'APPROVED_3';
    const REOPEN = 'REOPEN';
    const REVISED = 'REVISED';
    const REVISI = 'REVISI';
    const REVISI_REJECTED = 'REVISI_REJECTED';
    const REALISASI = 'REALISASI';
    const REALISASI_SUBMITTED = 'REALISASI_SUBMITTED';
    const REALISASI_CREATED = 'REALISASI_CREATED';
    const REALISASI_REJECTED = 'REALISASI_REJECTED';
    const REALISASI_VERIFIED = 'REALISASI_VERIFIED';
    const UNPUBLISHED = 'UNPUBLISHED';

    // workflow transition names or steps
    const STEP_UPDATE = 'update';
    const STEP_REJECT = 'reject';
    const STEP_SUBMIT = 'submit';
    const STEP_VERIFY = 'verify';
    const STEP_VERIFY_1 = 'verify_1';
    const STEP_VERIFY_2 = 'verify_2';
    const STEP_APPROVAL_1 = 'approval_1';
    const STEP_APPROVAL_2 = 'approval_2';
    const STEP_APPROVAL_3 = 'approval_3';
    const STEP_AKTIVASI = 'aktivasi';
    const STEP_REQUEST = 'request';
    const STEP_VERIFY_REQUEST = 'verify_request';
    const STEP_REJECT_REQUEST = 'reject_request';
    const STEP_CREATE_REALISASI = 'create_realisasi';
    const STEP_SUBMIT_REALISASI = 'submit_realisasi';
    const STEP_REJECT_REALISASI = 'reject_realisasi';
    const STEP_VERIFY_REALISASI = 'verify_realisasi';
    const STEP_CLOSING = 'closing';
    const STEP_APPROVAL = 'approval';

    // Kategori pendanaan
    const DANA_GLOBAL = 1;
    const DANA_BIDANG = 2;
    const DANA_PROGRAM = 3;
    const DANA_BATCH = 4;

    // Kategori beneficiary
    const CAT_BEASISWA = 1;
    const CAT_TBE = 2;
    const CAT_INSTITUSI = 3;
    const CAT_INDIVIDU = 4;

    const PAUD = 'PAUD';
    const SMK = 'SMK';
    const POLTEK = 'POLTEK';
    const PESANTREN = 'PESANTREN';
    const LEMBAGA_KEAGAMAAN = 'LEMBAGA KEAGAMAAN';

    const SANTRI = 'SANTRI';
    const SISWA = 'SISWA';
    const MAHASISWA = 'MAHASISWA';
    const USTADZ = 'USTADZ';
    const GURU = 'GURU';
    const DOSEN = 'DOSEN';
    const MANAJEMEN = 'MANAJEMEN';
    const INSTRUKTUR = 'USTADZ & INSTRUKTUR';

    /**
     * Disable WorkflowTags constructor.
     */
    private function __construct()
    {
    }

    /**
     * Disable WorkflowTags cloning.
     */
    private function __clone()
    {
    }

}
