<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan\Workflow;


use App\Component\Workflow\Definition;
use App\Component\Workflow\Visitor\MarkingVisitorImpl;
use App\Component\Workflow\WorkflowImpl;

/**
 * Class DanaProgramConfigurer
 *
 * @package App\Models\Pendanaan\Workflow
 * @author  Ahmad Fajar
 * @since   27/04/2019, modified: 02/05/2019 2:23
 */
final class DanaProgramConfigurer extends DanaBidangConfigurer
{

    protected function createWorkflow(): void
    {
        $definition = new Definition($this->places, $this->transitions, $this->startPlace);
        $this->workflow = new WorkflowImpl($definition, new MarkingVisitorImpl('status'),
                                           $this->dispatcher, 'dana_program');
    }

}