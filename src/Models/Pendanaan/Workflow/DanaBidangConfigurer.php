<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan\Workflow;


use App\Component\Workflow\Definition;
use App\Component\Workflow\Transition;
use App\Component\Workflow\Visitor\MarkingVisitorImpl;
use App\Component\Workflow\WorkflowImpl;

/**
 * Class DanaBidangConfigurer
 *
 * @package App\Models\Pendanaan\Workflow
 * @author  Ahmad Fajar
 * @since   27/04/2019, modified: 03/05/2019 6:16
 */
class DanaBidangConfigurer extends BaseConfigurer
{

    protected function createPlaces(): void
    {
        $this->places = [
            WorkflowTags::NEW,
            WorkflowTags::UPDATED,
            WorkflowTags::PLANNED,
            //            WorkflowTags::APPROVED_1,
            //            WorkflowTags::APPROVED_2,
            //            WorkflowTags::APPROVED_3,
            WorkflowTags::REJECTED,
            WorkflowTags::ONGOING,
            WorkflowTags::REVISI,
            WorkflowTags::REVISI_REJECTED,
            WorkflowTags::REVISED,
            WorkflowTags::CLOSED,
        ];
    }

    protected function createTransitions(): void
    {
        $this->transitions = [
            new Transition([
                               WorkflowTags::NEW,
                               WorkflowTags::UPDATED,
                               WorkflowTags::REJECTED
                           ],
                           WorkflowTags::UPDATED,
                           WorkflowTags::STEP_UPDATE),
            new Transition([
                               WorkflowTags::NEW,
                               WorkflowTags::UPDATED
                           ],
                           WorkflowTags::PLANNED,
                           WorkflowTags::STEP_VERIFY),
            //            new Transition([
            //                               WorkflowTags::PLANNED,
            //                               WorkflowTags::REVISED
            //                           ],
            //                           WorkflowTags::APPROVED_1,
            //                           WorkflowTags::STEP_APPROVAL_1),
            //            new Transition([WorkflowTags::APPROVED_1],
            //                           WorkflowTags::APPROVED_2,
            //                           WorkflowTags::STEP_APPROVAL_2),
            //            new Transition([WorkflowTags::APPROVED_2],
            //                           WorkflowTags::APPROVED_3,
            //                           WorkflowTags::STEP_APPROVAL_3),
            new Transition([
                               WorkflowTags::NEW,
                               WorkflowTags::UPDATED,
                               //                               WorkflowTags::PLANNED,
                               //                               WorkflowTags::REVISED,
                               //                               WorkflowTags::APPROVED_1,
                               //                               WorkflowTags::APPROVED_2
                           ],
                           WorkflowTags::REJECTED,
                           WorkflowTags::STEP_REJECT),
            new Transition([WorkflowTags::PLANNED],
                           WorkflowTags::ONGOING,
                           WorkflowTags::STEP_AKTIVASI),
            new Transition([
                               WorkflowTags::ONGOING,
                               WorkflowTags::REVISED,
                               WorkflowTags::REVISI_REJECTED
                           ],
                           WorkflowTags::REVISI,
                           WorkflowTags::STEP_REQUEST),
            new Transition([WorkflowTags::REVISI],
                           WorkflowTags::REVISED,
                           WorkflowTags::STEP_VERIFY_REQUEST),
            new Transition([WorkflowTags::REVISI],
                           WorkflowTags::REVISI_REJECTED,
                           WorkflowTags::STEP_REJECT_REQUEST),
            new Transition([
                               WorkflowTags::ONGOING,
                               WorkflowTags::REVISED,
                               WorkflowTags::REVISI_REJECTED
                           ],
                           WorkflowTags::CLOSED,
                           WorkflowTags::STEP_CLOSING),
        ];
    }

    protected function createWorkflow(): void
    {
        $definition = new Definition($this->places, $this->transitions, $this->startPlace);
        $this->workflow = new WorkflowImpl($definition, new MarkingVisitorImpl('status'),
                                           $this->dispatcher, 'dana_bidang');
    }

}