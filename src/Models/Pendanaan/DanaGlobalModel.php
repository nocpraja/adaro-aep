<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Models\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Entity\Pendanaan\DanaGlobal;
use App\Entity\Pendanaan\LogApprovalPendanaan;
use App\Entity\Pendanaan\TransaksiDanaGlobal;
use App\Models\Pendanaan\Workflow\DanaGlobalConfigurer;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\MsNumberingRepository;
use App\Repository\Pendanaan\DanaGlobalRepository;
use App\Repository\Pendanaan\LogApprovalPendanaanRepository;
use App\Repository\Pendanaan\TransaksiDanaGlobalRepository;
use App\Service\Logger\AuditLogger;
use DateTime;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use LogicException;

/**
 * Class DanaGlobalModel dipergunakan untuk transaksi Pendanaan Global.
 *
 * @package App\Models\Pendanaan
 * @author  Ahmad Fajar
 * @since   03/11/2018, modified: 17/07/2019 1:45
 */
final class DanaGlobalModel
{

    /**
     * @var DanaGlobalRepository
     */
    private $repository;

    /**
     * @var TransaksiDanaGlobalRepository
     */
    private $transactionRepository;

    /**
     * @var LogApprovalPendanaanRepository
     */
    private $logApprovalRepository;

    /**
     * @var MsNumberingRepository
     */
    private $numberingRepository;

    /**
     * @var DanaGlobalConfigurer
     */
    private $workflowConfigurator;
    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * DanaGlobalModel constructor.
     *
     * @param DanaGlobalRepository           $danaRepository        Entity repository instance
     * @param TransaksiDanaGlobalRepository  $transactionRepository Entity repository instance
     * @param LogApprovalPendanaanRepository $logApprovalRepository Entity repository instance
     * @param MsNumberingRepository          $numberingRepository   Entity repository instance
     * @param DanaGlobalConfigurer           $workflowConfigurator  Dana Global workflow configurator
     * @param AuditLogger                    $logger                AuditLogger service
     */
    public function __construct(DanaGlobalRepository $danaRepository,
                                TransaksiDanaGlobalRepository $transactionRepository,
                                LogApprovalPendanaanRepository $logApprovalRepository,
                                MsNumberingRepository $numberingRepository,
                                DanaGlobalConfigurer $workflowConfigurator,
                                AuditLogger $logger)
    {
        $this->logger = $logger;
        $this->repository = $danaRepository;
        $this->transactionRepository = $transactionRepository;
        $this->logApprovalRepository = $logApprovalRepository;
        $this->numberingRepository = $numberingRepository;
        $this->workflowConfigurator = $workflowConfigurator;
    }

    /**
     * Proses workflow transition pada Dana Global.
     *
     * @param DanaGlobal   $entity Persistent entity object
     * @param ActionObject $data   Transient data object
     *
     * @throws LogicException
     * @throws ORMException
     */
    public function actionTransition(DanaGlobal $entity, ActionObject $data)
    {
        $workflow = $this->workflowConfigurator->getWorkflow();
        $transition = $data->getAction();
        $context = ['status' => $entity->getStatus(), 'keterangan' => $data->getMessage()];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Dana Global belum bisa diverifikasi.');
        }
        if ($transition == WorkflowTags::STEP_VERIFY) {
            $this->appendTransaction($entity->getAnggaran(), $entity->getKode(), $entity->getTitle());
        }

        $workflow->apply($entity, $transition, $context);
    }

    /**
     * Mencatat transaksi penambahan ataupun pengurangan dana.
     * Dipergunakan oleh pendanaan lainnya sesuai dengan keperluan bisnis process.
     *
     * @param float  $jumlah  Jumlah penambahan/pengurangan dana
     * @param string $refcode Kode referensi transaksi
     * @param string $name    Nama transaksi
     *
     * @throws ORMException
     */
    public function appendTransaction(float $jumlah, string $refcode, ?string $name): void
    {
        if (empty($name)) {
            if (substr($refcode, 0, 2) == 'DG') {
                $name = $jumlah < 0 ? 'Pengurangan jumlah Dana Global' : 'Penambahan jumlah Dana Global';
            } else {
                $name = $jumlah < 0 ? 'Pengurangan jumlah Dana' : 'Penambahan jumlah Dana';
            }
        }

        $dg = $this->repository->getLastNRecords(1);
        $lastRecord = $this->lastTransaction();

        $entity = new TransaksiDanaGlobal();
        $entity->setTitle($name)
               ->setRefcode($refcode)
               ->setJumlah($jumlah);

        if (!empty($dg)) {
            $entity->setTahunAwal($dg[0]->getTahunAwal())
                   ->setTahunAkhir($dg[0]->getTahunAkhir());
        }

        $saldo = !is_null($lastRecord) ? ($lastRecord->getSaldo() + $jumlah) : $jumlah;
        $entity->setSaldo($saldo);
        $this->transactionRepository->save($entity);
        $this->logger->getEntityManager()->flush();

        $this->logger->info($this->logger->formatMessage($name), TransaksiDanaGlobal::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Setup awal Dana Global ataupun menyimpan request perubahan Dana Global.
     *
     * @param DanaGlobal $entity Unmanaged entity object
     *
     * @throws ORMException
     */
    public function create(DanaGlobal $entity): void
    {
        $lastRecord = $this->repository->getLastNRecords(1);

        if (!empty($lastRecord) && $lastRecord[0]->getStatus() !== WorkflowTags::VERIFIED) {
            throw new LogicException('Dana Global hanya bisa dibuat untuk sekali saja. ' .
                                     'Lakukan request perubahan dana untuk memperbaruinya.');
        }

        if ($entity instanceof DanaGlobal) {
            $year = substr($entity->getTahunAwal(), -2);
        } else {
            $year = date('y');
        }

        $numbering = $this->numberingRepository->nextSequence('Dana Global', 'DG-' . $year);

        $entity->setStatus(WorkflowTags::NEW)
               ->setAnggaran($entity->getNewValue())
               ->setKode($numbering->toStringCode(4));
        $this->repository->save($entity);
        $this->logger->getEntityManager()->flush();

        $this->logger->info($this->logger->formatMessage($entity->getTitle()), DanaGlobal::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Menampilkan data Pendanaan Global.
     *
     * @param DataQuery $dq Request query
     *
     * @return Paginator
     */
    public function displayAll(DataQuery $dq): Paginator
    {
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                      $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Menampilkan daftar riwayat approval transaksi Dana Global.
     *
     * @param DanaGlobal $entity Persistent entity object
     *
     * @return LogApprovalPendanaan[]
     */
    public function displayLogApproval(DanaGlobal $entity): ?array
    {
        return $this->logApprovalRepository->findBy(['danaGlobal' => $entity, 'kategori' => WorkflowTags::DANA_GLOBAL]);
    }

    /**
     * Menampilkan data riwayat transaksi Pendanaan Global.
     *
     * @param DataQuery $dq Request query
     *
     * @return Paginator
     */
    public function displayTransactionHistories(DataQuery $dq): Paginator
    {
        $query = $this->transactionRepository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                                 $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Get AuditLogger service.
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * @return MsNumberingRepository
     */
    public function getNumberingRepository(): MsNumberingRepository
    {
        return $this->numberingRepository;
    }

    /**
     * Get Dana Global entity repository instance.
     *
     * @return DanaGlobalRepository
     */
    public function getRepository(): DanaGlobalRepository
    {
        return $this->repository;
    }

    /**
     * Get riwayat transaksi Dana Global entity repository instance.
     *
     * @return TransaksiDanaGlobalRepository
     */
    public function getTransactionRepository(): TransaksiDanaGlobalRepository
    {
        return $this->transactionRepository;
    }

    /**
     * Menampilkan informasi transaksi terakhir Dana Global.
     *
     * @return TransaksiDanaGlobal
     */
    public function lastTransaction(): ?TransaksiDanaGlobal
    {
        $result = $this->transactionRepository->getLastNRecords(1);

        return (empty($result) ? null : $result[0]);
    }

    /**
     * Simpan perubahan data Dana Global.
     *
     * @param DanaGlobal $entity Persistent entity object
     *
     * @throws Exception
     */
    public function update(DanaGlobal $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            throw new LogicException('Dana Global telah dikunci untuk pembaruan. ' .
                                     'Lakukan request perubahan dana untuk memperbaruinya.');
        }

//        $selisih = $entity->getNewValue() - $entity->getAnggaran();
        $title = 'REVISI ' . $entity->getTitle();
        $entity->setLastUpdated(new DateTime())
               ->setAnggaran($entity->getNewValue());
        $workflow->apply($entity, WorkflowTags::STEP_UPDATE, [], $this->logger->getEntityManager());

        $this->logger->info($this->logger->formatMessage($title), DanaGlobal::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);

//        $this->appendTransaction($selisih, $entity->getKode(), $title);
    }

}