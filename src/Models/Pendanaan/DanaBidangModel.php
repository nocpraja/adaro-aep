<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Pendanaan\DanaBidang;
use App\Entity\Pendanaan\DanaProgram;
use App\Entity\Pendanaan\EntityDanaInterface;
use App\Entity\Pendanaan\TransaksiDanaBidang;
use App\Models\Pendanaan\Workflow\DanaBidangConfigurer;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\MsNumberingRepository;
use App\Repository\Pendanaan\DanaBidangRepository;
use App\Repository\Pendanaan\DanaProgramRepository;
use App\Repository\Pendanaan\TransaksiDanaBidangRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;

/**
 * Class DanaBidangModel
 *
 * @package App\Models\Pendanaan
 * @author  Ahmad Fajar
 * @since   27/04/2019, modified: 11/07/2019 2:05
 */
final class DanaBidangModel implements PendanaanDomainModel
{

    /**
     * @var DanaBidangRepository
     */
    private $repository;

    /**
     * @var TransaksiDanaBidangRepository
     */
    private $transactionRepository;

    /**
     * @var MsNumberingRepository
     */
    private $numberingRepository;

    /**
     * @var DanaBidangConfigurer
     */
    private $workflowConfigurator;

    /**
     * @var DanaGlobalModel
     */
    private $danaGlobalModel;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * DanaBidangModel constructor.
     *
     * @param DanaBidangRepository          $repository            Entity repository instance
     * @param TransaksiDanaBidangRepository $transactionRepository Entity repository instance
     * @param DanaBidangConfigurer          $workflowConfigurator  Dana Bidang workflow configurator
     * @param DanaGlobalModel               $danaGlobalModel       Dana Global domain model
     */
    public function __construct(DanaBidangRepository $repository,
                                TransaksiDanaBidangRepository $transactionRepository,
                                DanaBidangConfigurer $workflowConfigurator,
                                DanaGlobalModel $danaGlobalModel)
    {
        $this->repository = $repository;
        $this->danaGlobalModel = $danaGlobalModel;
        $this->transactionRepository = $transactionRepository;
        $this->workflowConfigurator = $workflowConfigurator;
        $this->numberingRepository = $danaGlobalModel->getNumberingRepository();
        $this->logger = $danaGlobalModel->getLogger();
    }

    /**
     * @inheritDoc
     */
    public function actionTransition(EntityDanaInterface $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();
        $transition = $data->getAction();
        $context = ['status' => $entity->getStatus(), 'keterangan' => $data->getMessage()];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum sampai pada tahapnya.');
        }
        if ($transition == WorkflowTags::STEP_CLOSING && !$this->canClose($entity)) {
            throw new LogicException('Proses TUTUP Dana Bidang belum bisa dilakukan, masih ada Dana Program yang belum ditutup.');
        }

        $workflow->apply($entity, $transition, $context);
    }

    /**
     * Mencatat transaksi penambahan ataupun pengurangan dana.
     * Dipergunakan oleh pendanaan lainnya sesuai dengan keperluan bisnis process.
     *
     * @param DanaBidang|EntityDanaInterface $danaBidang     Persistent entity object
     * @param float                          $jumlah         Jumlah penambahan/pengurangan dana
     * @param string                         $refcode        Kode referensi transaksi
     * @param string                         $name           Nama transaksi
     * @param bool                           $saveAllocation Catat alokasi dana dan kalkulasi ulang saldo Dana Bidang?
     *
     * @throws ORMException
     */
    public function appendTransaction(DanaBidang $danaBidang, float $jumlah, string $refcode, ?string $name,
                                      bool $saveAllocation = false): void
    {
        if (empty($name)) {
            if (substr($refcode, 0, 2) == 'DB') {
                $name = $jumlah < 0 ? 'Pengurangan jumlah Dana Bidang' : 'Penambahan jumlah Dana Bidang';
            } else {
                $name = $jumlah < 0 ? 'Pengurangan jumlah Dana' : 'Penambahan jumlah Dana';
            }
        }

        $lastRecord = $this->lastTransaction($danaBidang);

        $entity = new TransaksiDanaBidang();
        $entity->setTitle($name)
               ->setRefcode($refcode)
               ->setJumlah($jumlah)
               ->setTahunAwal($danaBidang->getTahunAwal())
               ->setTahunAkhir($danaBidang->getTahunAkhir())
               ->setDanaBidang($danaBidang);

        $saldo = !is_null($lastRecord) ? ($lastRecord->getSaldo() + $jumlah) : $jumlah;
        $entity->setSaldo($saldo);
        $this->transactionRepository->save($entity);

        if ($saveAllocation) {
            $alokasi = $danaBidang->getAlokasi();
            $danaBidang->setAlokasi($alokasi + ($jumlah * -1));
        }

        $this->logger->getEntityManager()->flush();
        $this->logger->info($this->logger->formatMessage($name), TransaksiDanaBidang::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * @inheritDoc
     */
    public function canClose(EntityDanaInterface $entity): bool
    {
        $em = $this->logger->getEntityManager();

        /** @var DanaProgramRepository $repo */
        $repo = $em->getRepository(DanaProgram::class);
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'danaBidang', 'value' => $entity, 'operator' => 'eq'],
                ['property' => 'status', 'value' => WorkflowTags::CLOSED, 'operator' => 'neq'],
            ]
        );

        $results = $repo->findAllByCriteria($filters)
                        ->getResult();

        return empty($results);
    }

    /**
     * Create Dana Bidang.
     *
     * @param DanaBidang|EntityDanaInterface $entity Unmanaged entity object
     *
     * @throws ORMException
     */
    public function create(EntityDanaInterface $entity): void
    {
        if ($entity instanceof DanaBidang) {
            $year = substr($entity->getTahunAwal(), -2);
        } else {
            $year = date('y');
        }

        $numbering = $this->numberingRepository->nextSequence('Dana Bidang', 'DB-' . $year);
        $title = 'SETUP ' . $entity->getTitle();

        $entity->setStatus(WorkflowTags::NEW)
               ->setAnggaran($entity->getNewValue())
               ->setKode($numbering->toStringCode(4))
               ->setTags(null);
        $this->repository->save($entity);
        $this->logger->getEntityManager()->flush();

        $this->logger->info($this->logger->formatMessage($title), DanaBidang::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);

        $this->appendTransaction($entity, $entity->getAnggaran(), $entity->getKode(), $title);
        $this->danaGlobalModel->appendTransaction($entity->getAnggaran() * -1, $entity->getKode(), $title);
    }

    /**
     * @inheritDoc
     *
     * @throws ORMException
     */
    public function delete(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            throw new LogicException('Dana Bidang telah dikunci. ' .
                                     'Proses penghapusan data sudah tidak diperkenankan lagi.');
        }

        $em = $this->logger->getEntityManager();
        $repo = $em->getRepository(DanaProgram::class);
        $result = $repo->findBy(['danaBidang' => $entity], [], 1);

        if (!empty($result)) {
            throw new LogicException('Dana Bidang tidak boleh dihapus, masih ada data yang terkait dengannya.');
        }

        $anggaran = $entity->getAnggaran();
        $refcode = $entity->getKode();
        $title = 'HAPUS ' . $entity->getTitle();
        $em->remove($entity);

        $this->logger->notice($this->logger->formatMessage($title), DanaBidang::class, $entity,
                              [JsonController::CONTEXT_NON_REL]);
        $this->danaGlobalModel->appendTransaction($anggaran, $refcode, $title);
    }

    /**
     * @inheritDoc
     */
    public function displayAll(DataQuery $dq, AppUserInterface $user = null): Paginator
    {
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                      $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @inheritDoc
     */
    public function displayTransactionHistories(DataQuery $dq, AppUserInterface $user = null): Paginator
    {
        $query = $this->transactionRepository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                                 $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @return DanaGlobalModel
     */
    public function getDanaGlobalModel(): DanaGlobalModel
    {
        return $this->danaGlobalModel;
    }

    /**
     * @inheritDoc
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * @inheritDoc
     */
    public function getNumberingRepository(): MsNumberingRepository
    {
        return $this->numberingRepository;
    }

    /**
     * Get Dana Bidang entity repository instance.
     *
     * @return DanaBidangRepository
     */
    public function getRepository(): DanaBidangRepository
    {
        return $this->repository;
    }

    /**
     * Get riwayat transaksi Dana Bidang entity repository instance.
     *
     * @return TransaksiDanaBidangRepository
     */
    public function getTransactionRepository(): TransaksiDanaBidangRepository
    {
        return $this->transactionRepository;
    }

    /**
     * Menampilkan informasi transaksi terakhir Dana Bidang.
     *
     * @param DanaBidang          $bidang  Persistent entity object
     * @param SortOrFilter[]|null $filters Additional filters to apply
     *
     * @return TransaksiDanaBidang
     */
    public function lastTransaction(DanaBidang $bidang, array $filters = null): ?TransaksiDanaBidang
    {
        $_filters[] = QueryExpressionHelper::createCriteria(['property' => 'transaksi.danaBidang', 'value' => $bidang]);

        if (!empty($filters)) {
            foreach ($filters as $filter) {
                if ($filter instanceof SortOrFilter) {
                    $_filters[] = $filter;
                }
            }
        }

        $result = $this->transactionRepository->getLastNRecords(1, $_filters);

        return (empty($result) ? null : $result[0]);
    }

    /**
     * @inheritDoc
     *
     * @throws ORMException
     */
    public function revisi(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_REQUEST)) {
            throw new LogicException('Request revisi Dana Bidang belum dapat dilakukan.');
        }

        $title = 'REQUEST REVISI ' . $entity->getTitle();
        $jumlah = $entity->getNewValue();
        $anggaran = $entity->getAnggaran();
        $tags = $entity->getTags();
        $revnum = empty($tags) ? 1 : $tags['revnum'] + 1;

        $entity->setAnggaran($anggaran + $jumlah)
               ->setTags(['request' => true, 'revnum' => $revnum]);

        $workflow->apply($entity, WorkflowTags::STEP_REQUEST, [], $this->logger->getEntityManager());

        $this->appendTransaction($entity, $jumlah, $entity->getKode(), $title);
        $this->danaGlobalModel->appendTransaction(($jumlah * -1), $entity->getKode(), $title);
    }

    /**
     * @inheritDoc
     *
     * @throws ORMException
     */
    public function update(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            throw new LogicException('Dana Bidang telah dikunci untuk pembaruan. ' .
                                     'Lakukan request perubahan dana untuk memperbaruinya.');
        }

        $selisih = $entity->getNewValue() - $entity->getAnggaran();
        $entity->setAnggaran($entity->getNewValue())
               ->setTags(null);
        $workflow->apply($entity, WorkflowTags::STEP_UPDATE, [], $this->logger->getEntityManager());

        if ($selisih != 0.0) {
            $title = 'REVISI ' . $entity->getTitle();
            $this->logger->info($this->logger->formatMessage($title), DanaBidang::class, $entity,
                                [JsonController::CONTEXT_NON_REL]);

            $this->appendTransaction($entity, $selisih, $entity->getKode(), $title);
            $this->danaGlobalModel->appendTransaction(($selisih * -1), $entity->getKode(), $title);
        }
    }


}