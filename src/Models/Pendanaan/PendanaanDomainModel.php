<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan;


use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Entity\Pendanaan\EntityDanaInterface;
use App\Repository\MsNumberingRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * interface PendanaanDomainModel dipergunakan untuk menyeragamkan method bisnis proses yang umum dipakai.
 *
 * @package App\Models\Pendanaan
 * @author  Ahmad Fajar
 * @since   05/05/2019, modified: 05/05/2019 13:03
 */
interface PendanaanDomainModel
{

    /**
     * Proses workflow transition pada Dana Bidang, Program ataupun Batch.
     *
     * @param EntityDanaInterface $entity Persistent entity object
     * @param ActionObject        $data   Transient data object
     */
    public function actionTransition(EntityDanaInterface $entity, ActionObject $data): void;

    /**
     * Memeriksa apakah Dana Bidang, Program ataupun Batch sudah dapat di close atau tidak.
     *
     * @param EntityDanaInterface $entity Persistent entity object
     *
     * @return bool
     */
    public function canClose(EntityDanaInterface $entity): bool;

    /**
     * Create Dana Bidang, Program ataupun Batch.
     *
     * @param EntityDanaInterface $entity Unmanaged entity object
     *
     * @throws ORMException
     */
    public function create(EntityDanaInterface $entity): void;

    /**
     * Hapus Dana Bidang, Program ataupun Batch berikut dengan histori transaksinya.
     *
     * @param EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function delete(EntityDanaInterface $entity): void;

    /**
     * Menampilkan data Pendanaan Bidang, Program ataupun Batch.
     *
     * @param DataQuery        $dq   Request query
     * @param AppUserInterface $user UserAccount yang sedang login
     *
     * @return Paginator
     */
    public function displayAll(DataQuery $dq, AppUserInterface $user = null): Paginator;

    /**
     * Menampilkan data riwayat transaksi Pendanaan Bidang, Program ataupun Batch.
     *
     * @param DataQuery        $dq   Request query
     * @param AppUserInterface $user UserAccount yang sedang login
     *
     * @return Paginator
     */
    public function displayTransactionHistories(DataQuery $dq, AppUserInterface $user = null): Paginator;

    /**
     * Get AuditLogger service.
     *
     * @return AuditLogger
     */
    public function getLogger(): AuditLogger;

    /**
     * @return MsNumberingRepository
     */
    public function getNumberingRepository(): MsNumberingRepository;

    /**
     * Proses request revisi anggaran Dana Bidang, Program ataupun Batch.
     *
     * @param EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function revisi(EntityDanaInterface $entity): void;

    /**
     * Simpan perubahan data Dana Bidang, Program ataupun Batch.
     *
     * @param EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function update(EntityDanaInterface $entity): void;

}