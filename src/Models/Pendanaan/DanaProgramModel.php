<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Models\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\DanaProgram;
use App\Entity\Pendanaan\EntityDanaInterface;
use App\Entity\Pendanaan\TransaksiDanaProgram;
use App\Models\Pendanaan\Workflow\DanaProgramConfigurer;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Repository\MsNumberingRepository;
use App\Repository\Pendanaan\DanaBatchRepository;
use App\Repository\Pendanaan\DanaBidangRepository;
use App\Repository\Pendanaan\DanaProgramRepository;
use App\Repository\Pendanaan\TransaksiDanaProgramRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use LogicException;

/**
 * Class DanaProgramModel
 *
 * @package App\Models\Pendanaan
 * @author  Ahmad Fajar
 * @since   29/04/2019, modified: 11/07/2019 2:05
 */
final class DanaProgramModel implements PendanaanDomainModel
{

    /**
     * @var DanaProgramRepository
     */
    private $repository;

    /**
     * @var TransaksiDanaProgramRepository
     */
    private $transactionRepository;

    /**
     * @var MsNumberingRepository
     */
    private $numberingRepository;

    /**
     * @var DanaProgramConfigurer
     */
    private $workflowConfigurator;

    /**
     * @var DanaBidangModel
     */
    private $bidangModel;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * DanaProgramModel constructor.
     *
     * @param DanaProgramRepository          $repository            Entity repository instance
     * @param TransaksiDanaProgramRepository $transactionRepository Entity repository instance
     * @param DanaProgramConfigurer          $workflowConfigurator  Dana Program workflow configurator
     * @param DanaBidangModel                $danaBidangModel       Dana Bidang domain model
     */
    public function __construct(DanaProgramRepository $repository,
                                TransaksiDanaProgramRepository $transactionRepository,
                                DanaProgramConfigurer $workflowConfigurator,
                                DanaBidangModel $danaBidangModel)
    {
        $this->repository = $repository;
        $this->bidangModel = $danaBidangModel;
        $this->transactionRepository = $transactionRepository;
        $this->workflowConfigurator = $workflowConfigurator;
        $this->numberingRepository = $danaBidangModel->getNumberingRepository();
        $this->logger = $danaBidangModel->getLogger();
    }

    /**
     * @inheritDoc
     */
    public function actionTransition(EntityDanaInterface $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();
        $transition = $data->getAction();
        $context = ['status' => $entity->getStatus(), 'keterangan' => $data->getMessage()];

        if (!$workflow->can($entity, $transition)) {
            throw new LogicException('Proses workflow tidak dikenali atau belum sampai pada tempatnya.');
        }
        if ($transition == WorkflowTags::STEP_CLOSING && !$this->canClose($entity)) {
            throw new LogicException('Proses TUTUP Dana Program belum bisa dilakukan, masih ada Dana Batch yang belum ditutup.');
        }

        $workflow->apply($entity, $transition, $context);
    }

    /**
     * Mencatat transaksi penambahan ataupun pengurangan dana.
     * Dipergunakan oleh pendanaan lainnya sesuai dengan keperluan bisnis process.
     *
     * @param DanaProgram|EntityDanaInterface $danaProgram    Persistent entity object
     * @param float                           $jumlah         Jumlah penambahan/pengurangan dana
     * @param string                          $refcode        Kode referensi transaksi
     * @param string                          $name           Nama transaksi
     * @param bool                            $saveAllocation Catat alokasi dana dan kalkulasi ulang saldo Dana Program?
     *
     * @throws ORMException
     */
    public function appendTransaction(DanaProgram $danaProgram, float $jumlah, string $refcode,
                                      ?string $name, bool $saveAllocation = false): void
    {
        if (empty($name)) {
            if (substr($refcode, 0, 2) == 'DP') {
                $name = $jumlah < 0 ? 'Pengurangan jumlah Dana Program' : 'Penambahan jumlah Dana Program';
            } else {
                $name = $jumlah < 0 ? 'Pengurangan jumlah Dana' : 'Penambahan jumlah Dana';
            }
        }

        $lastRecord = $this->lastTransaction($danaProgram);

        $entity = new TransaksiDanaProgram();
        $entity->setTitle($name)
               ->setRefcode($refcode)
               ->setJumlah($jumlah)
               ->setTahunAwal($danaProgram->getTahunAwal())
               ->setTahunAkhir($danaProgram->getTahunAkhir())
               ->setDanaProgram($danaProgram);

        $saldo = !is_null($lastRecord) ? ($lastRecord->getSaldo() + $jumlah) : $jumlah;
        $entity->setSaldo($saldo);
        $this->transactionRepository->save($entity);

        if ($saveAllocation) {
            $alokasi = $danaProgram->getAlokasi();
            $danaProgram->setAlokasi($alokasi + ($jumlah * -1));
        }

        $this->logger->getEntityManager()->flush();
        $this->logger->info($this->logger->formatMessage($name), TransaksiDanaProgram::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * @inheritDoc
     */
    public function canClose(EntityDanaInterface $entity): bool
    {
        $em = $this->logger->getEntityManager();

        /** @var DanaBatchRepository $repo */
        $repo = $em->getRepository(DanaBatch::class);
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'danaProgram', 'value' => $entity, 'operator' => 'eq'],
                ['property' => 'status', 'value' => WorkflowTags::CLOSED, 'operator' => 'neq'],
            ]
        );

        $results = $repo->findAllByCriteria($filters)
                        ->getResult();

        return empty($results);
    }

    /**
     * Create Dana Program.
     *
     * @param DanaProgram|EntityDanaInterface $entity Unmanaged entity object
     *
     * @throws ORMException
     */
    public function create(EntityDanaInterface $entity): void
    {
        if ($entity instanceof DanaProgram) {
            $year = substr($entity->getTahunAwal(), -2);
        } else {
            $year = date('y');
        }

        $numbering = $this->numberingRepository->nextSequence('Dana Program', 'DP-' . $year);
        $title = 'SETUP ' . $entity->getTitle();

        $entity->setStatus(WorkflowTags::NEW)
               ->setAnggaran($entity->getNewValue())
               ->setKode($numbering->toStringCode(4))
               ->setTags(null);
        $this->repository->save($entity);
        $this->logger->getEntityManager()->flush();

        $this->logger->info($this->logger->formatMessage($title), DanaProgram::class, $entity,
                            [JsonController::CONTEXT_NON_REL]);

        $this->appendTransaction($entity, $entity->getAnggaran(), $entity->getKode(), $title);
        $this->bidangModel->appendTransaction($entity->getDanaBidang(), $entity->getAnggaran() * -1,
                                              $entity->getKode(), $title, true);
    }

    /**
     * Hapus Dana Program berikut dengan histori transaksinya.
     *
     * @param DanaProgram|EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function delete(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            throw new LogicException('Dana Program telah dikunci. ' .
                                     'Proses penghapusan data sudah tidak diperkenankan lagi.');
        }

        $em = $this->logger->getEntityManager();
        $parent = $entity->getDanaBidang();
        $repo = $em->getRepository(DanaBatch::class);
        $result = $repo->findBy(['danaProgram' => $entity], [], 1);

        if (!empty($result)) {
            throw new LogicException('Dana Program tidak boleh dihapus, masih ada data yang terkait dengannya.');
        }

        $anggaran = $entity->getAnggaran();
        $refcode = $entity->getKode();
        $title = 'HAPUS ' . $entity->getTitle();
        $em->remove($entity);

        $this->logger->notice($this->logger->formatMessage($title), DanaProgram::class, $entity,
                              [JsonController::CONTEXT_NON_REL]);
        $this->bidangModel->appendTransaction($parent, $anggaran, $refcode, $title, true);
    }

    /**
     * @inheritDoc
     */
    public function displayAll(DataQuery $dq, AppUserInterface $user = null): Paginator
    {
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                      $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @inheritDoc
     */
    public function displayTransactionHistories(DataQuery $dq, AppUserInterface $user = null): Paginator
    {
        $query = $this->transactionRepository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
                                                                 $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * @return DanaBidangModel
     */
    public function getBidangModel(): DanaBidangModel
    {
        return $this->bidangModel;
    }

    /**
     * Get entity repository instance for Dana Bidang.
     *
     * @return DanaBidangRepository
     */
    public function getDanaBidangRepository(): DanaBidangRepository
    {
        return $this->bidangModel->getRepository();
    }

    /**
     * @inheritDoc
     */
    public function getLogger(): AuditLogger
    {
        return $this->logger;
    }

    /**
     * @inheritDoc
     */
    public function getNumberingRepository(): MsNumberingRepository
    {
        return $this->numberingRepository;
    }

    /**
     * Get entity repository instance for Dana Program.
     *
     * @return DanaProgramRepository
     */
    public function getRepository(): DanaProgramRepository
    {
        return $this->repository;
    }

    /**
     * Get entity repository instance for transaksi Dana Program.
     *
     * @return TransaksiDanaProgramRepository
     */
    public function getTransactionRepository(): TransaksiDanaProgramRepository
    {
        return $this->transactionRepository;
    }

    /**
     * Menampilkan informasi transaksi terakhir Dana Program.
     *
     * @param DanaProgram         $program Persistent entity object
     * @param SortOrFilter[]|null $filters Additional filters to apply
     *
     * @return TransaksiDanaProgram|null
     */
    public function lastTransaction(DanaProgram $program, array $filters = null): ?TransaksiDanaProgram
    {
        $_filters[] = QueryExpressionHelper::createCriteria(['property' => 'transaksi.danaProgram',
                                                             'value'    => $program]);

        if (!empty($filters)) {
            foreach ($filters as $filter) {
                if ($filter instanceof SortOrFilter) {
                    $_filters[] = $filter;
                }
            }
        }

        $result = $this->transactionRepository->getLastNRecords(1, $_filters);

        return (empty($result) ? null : $result[0]);
    }

    /**
     * Proses request revisi anggaran Dana Program.
     *
     * @param DanaProgram|EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function revisi(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_REQUEST)) {
            throw new LogicException('Request revisi Dana Program belum dapat dilakukan.');
        }

        $title = 'REQUEST REVISI ' . $entity->getTitle();
        $jumlah = $entity->getNewValue();
        $anggaran = $entity->getAnggaran();
        $tags = $entity->getTags();
        $revnum = empty($tags) ? 1 : $tags['revnum'] + 1;

        $entity->setAnggaran($anggaran + $jumlah)
               ->setTags(['request' => true, 'revnum' => $revnum]);

        $workflow->apply($entity, WorkflowTags::STEP_REQUEST, [], $this->logger->getEntityManager());

        $this->appendTransaction($entity, $jumlah, $entity->getKode(), $title);
        $this->bidangModel->appendTransaction($entity->getDanaBidang(), ($jumlah * -1),
                                              $entity->getKode(), $title, true);
    }

    /**
     * Simpan perubahan data Dana Program.
     *
     * @param DanaProgram|EntityDanaInterface $entity Persistent entity object
     *
     * @throws ORMException
     */
    public function update(EntityDanaInterface $entity): void
    {
        $workflow = $this->workflowConfigurator->getWorkflow();

        if (!$workflow->can($entity, WorkflowTags::STEP_UPDATE)) {
            throw new LogicException('Dana Program telah dikunci untuk pembaruan. ' .
                                     'Lakukan request perubahan dana untuk memperbaruinya.');
        }

        $selisih = $entity->getNewValue() - $entity->getAnggaran();
        $entity->setAnggaran($entity->getNewValue())
               ->setTags(null);
        $workflow->apply($entity, WorkflowTags::STEP_UPDATE, [], $this->logger->getEntityManager());

        if ($selisih != 0.0) {
            $title = 'REVISI ' . $entity->getTitle();
            $this->logger->info($this->logger->formatMessage($title), DanaProgram::class, $entity,
                                [JsonController::CONTEXT_NON_REL]);

            $this->appendTransaction($entity, $selisih, $entity->getKode(), $title);
            $this->bidangModel->appendTransaction($entity->getDanaBidang(), ($selisih * -1),
                                                  $entity->getKode(), $title, true);
        }
    }

}