### Direktori: src\Models

Direktori ini dipergunakan untuk script PHP yang berhubungan dengan bisnis process.
Direktori sebaiknya disusun dalam beberapa sub-direktori yang disesuaikan dengan topik bisnis process.

Pada controller tidak diperkenankan melakukan bisnis process. Melainkan hanya bertindak sebagai
gateway ataupun intermediate yang memanggil fungsi pada `EntityRepository` classes ataupun `Model` classes.

Contoh struktur direktori:
```text
-- Models
   |--- Kegiatan
   |--- MasterData
   |--- ModuleName
```