<?php


namespace App\Models\Finalisasi\Workflow;

use App\Component\Workflow\Definition;
use App\Component\Workflow\Transition;
use App\Component\Workflow\Visitor\MarkingVisitorImpl;
use App\Component\Workflow\WorkflowImpl;
use App\Models\Pendanaan\Workflow\BaseConfigurer;
use App\Models\Finalisasi\Workflow\WorkflowTags;

class WorkflowConfigurer extends BaseConfigurer
{
    protected function createPlaces(): void
    {
        $this->places = [
            WorkflowTags::NEW,
            WorkflowTags::PRESUBMITTED,
            WorkflowTags::SUBMITTED,
            WorkflowTags::VERIFIED1,
            WorkflowTags::VERIFIED2,
            WorkflowTags::APPROVED1,
            WorkflowTags::APPROVED2,
            WorkflowTags::APPROVED3
        ];
    }

    protected function createTransitions(): void
    {
        $this->transitions = [
            new Transition([WorkflowTags::NEW], WorkflowTags::PRESUBMITTED, WorkflowTags::STEP_PRESUBMIT),
            new Transition([WorkflowTags::PRESUBMITTED], WorkflowTags::SUBMITTED, WorkflowTags::STEP_SUBMIT),
            new Transition([WorkflowTags::SUBMITTED], WorkflowTags::VERIFIED1, WorkflowTags::STEP_VERIFY1),
            new Transition([WorkflowTags::VERIFIED1], WorkflowTags::VERIFIED2, WorkflowTags::STEP_VERIFY2),
            new Transition([WorkflowTags::VERIFIED2], WorkflowTags::APPROVED1, WorkflowTags::STEP_APPROVE1),
            new Transition([WorkflowTags::APPROVED1], WorkflowTags::APPROVED2, WorkflowTags::STEP_APPROVE2),
            new Transition([WorkflowTags::APPROVED2], WorkflowTags::APPROVED3, WorkflowTags::STEP_APPROVE3),

        ];


    }

    protected function createWorkflow(): void
    {
        $definition = new Definition($this->places, $this->transitions, $this->startPlace);
        $this->workflow = new WorkflowImpl($definition, new MarkingVisitorImpl('status'),
            $this->dispatcher, 'finalisasi');
    }
}