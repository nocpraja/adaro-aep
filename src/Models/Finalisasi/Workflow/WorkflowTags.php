<?php


namespace App\Models\Finalisasi\Workflow;


class WorkflowTags
{
    const NEW = 'NEW';
    const PRESUBMITTED = 'PRESUBMITTED';
    const SUBMITTED = 'SUBMITTED';
    const VERIFIED1 = 'VERIFIED1';
    const VERIFIED2 = 'VERIFIED2';
    const APPROVED1 = 'APPROVED1';
    const APPROVED2 = 'APPROVED2';
    const APPROVED3 = 'APPROVED3';

    const STEP_PRESUBMIT = 'presubmit';
    const STEP_SUBMIT = 'submit';
    const STEP_VERIFY1 = 'verify1';
    const STEP_VERIFY2 = 'verify2';
    const STEP_APPROVE1 = 'approve1';
    const STEP_APPROVE2 = 'approve2';
    const STEP_APPROVE3 = 'approve3';
}