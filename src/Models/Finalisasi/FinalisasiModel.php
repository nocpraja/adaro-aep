<?php


namespace App\Models\Finalisasi;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Finalisasi\Finalisasi;
use App\Entity\Security\UserAccount;
use App\Models\Finalisasi\Workflow\WorkflowConfigurer;
use App\Repository\Finalisasi\FinalisasiRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class FinalisasiModel
{
    /**
     * @var FinalisasiRepository
     */
    private $repository;
    /**
     * @var AuditLogger
     */
    private $logger;
    /**
     * @var WorkflowConfigurer
     */
    private $workflowConfigurer;

    /**
     * FinalisasiModel constructor.
     * @param FinalisasiRepository $repository
     * @param AuditLogger $logger
     * @param WorkflowConfigurer $workflowConfigurer
     */
    public function __construct(FinalisasiRepository $repository, AuditLogger $logger, WorkflowConfigurer $workflowConfigurer)
    {
        $this->repository = $repository;
        $this->logger = $logger;
        $this->workflowConfigurer = $workflowConfigurer;
    }


    /**
     * Menampilkan daftar finalisasi berdasarkan kriteria tertentu.
     *
     * @param DataQuery                         $dq   Request query
     * @param UserAccount|AppUserInterface|null $user UserAccount yang sedang login
     *
     * @return Paginator
     */
    public function displayAllByCriteria(DataQuery $dq, ?AppUserInterface $user): Paginator
    {
        $filters = [];
        if (!is_null($user) && !is_null($user->getUid())) {
            $filters[] = QueryExpressionHelper::createCriteria(['property' => 'postedBy', 'value' => $user->getUid()]);
        }

        $dq->setFilters($filters);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(), $dq->getPageSize(),
            $dq->getOffset(), $dq->getCondition());
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    /**
     * Proses insert or update data finalisasi.
     *
     * @param Finalisasi       $entity Persistent entity object
     * @param array|null       $params
     */
    public function update(Finalisasi $entity): void
    {
        $em = $this->logger->getEntityManager();

        $em->persist($entity);
        $em->flush();

        $this->logger->info($entity->getStatus() . ' record ' . $entity->getJudul() .  '.', Finalisasi::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }



    /**
     * Proses hapus data finalisasi.
     *
     * @param Finalisasi $entity Persistent entity object
     */
    public function delete(Finalisasi $entity): void
    {
        $em = $this->logger->getEntityManager();

        $em->remove($entity);
        $em->flush();

        $this->logger->info('HAPUS record ' . $entity->getKegiatan()->getNamaKegiatan() . ': ' . $entity->getJudul() . '.', Finalisasi::class,
            $entity, [JsonController::CONTEXT_NON_REL]);
    }


    /**
     * Proses workflow transition pada finalisasi.
     *
     * @param Finalisasi  $entity Persistent entity object
     * @param ActionObject $data   Transient data object
     */
    public function actionTransition(Finalisasi $entity, ActionObject $data): void
    {
        $workflow = $this->workflowConfigurer->getWorkflow();
        $transition = $data->getAction();
        $context = ['status' => $entity->getStatus(), 'keterangan' => $data->getMessage()];

        if (!$workflow->can($entity, $transition)) {
            throw new \LogicException('Proses workflow tidak dikenali atau belum bisa dilakukan.');
        }

        $workflow->apply($entity, $transition, $context);
    }

}