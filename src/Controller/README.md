### Direktori: src\Controller

Direktori ini dipergunakan untuk php-script **Application Controller**. Apakah itu untuk *REST-API Controller* 
ataupun *Standard Controller*. Direktori sebaiknya disusun dalam beberapa sub-direktori yang disesuaikan 
dengan topik bisnis process.

Pada controller tidak diperkenankan melakukan bisnis process. Melainkan hanya bertindak sebagai
gateway ataupun intermediate yang memanggil fungsi pada `EntityRepository` classes ataupun `Model` classes.

Contoh struktur direktori:
```text
-- Controller
   |--- Security
   |--- Kegiatan
   |--- MasterData
   |--- ModuleName
```