<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\BatchCsr;
use App\Form\MasterData\BatchCsrType;
use App\Repository\MasterData\BatchCsrRepository;
use App\Repository\MasterData\MitraRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BatchCsrController
 *
 * @package App\Controller\MasterData
 * @author  Ahmad Fajar
 * @since   08/11/2018, modified: 30/03/2019 18:37
 *
 * @Route("/app/master-data/batch")
 */
class BatchCsrController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BatchCsrRepository
     */
    private $repository;

    /**
     * @var MitraRepository
     */
    private $mitraRepository;

    /**
     * @var ProgramCSRRepository
     */
    private $programRepository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * BatchCsrController constructor.
     *
     * @param EntityManagerInterface $em                Doctrine entity manager
     * @param BatchCsrRepository     $repository        Entity repository instance
     * @param MitraRepository        $mitraRepository   Entity repository instance
     * @param ProgramCSRRepository   $programRepository Entity repository instance
     * @param AuditLogger            $logger            AuditLogger service
     */
    public function __construct(EntityManagerInterface $em,
                                BatchCsrRepository $repository,
                                MitraRepository $mitraRepository,
                                ProgramCSRRepository $programRepository,
                                AuditLogger $logger)
    {
        $this->em = $em;
        $this->repository = $repository;
        $this->mitraRepository = $mitraRepository;
        $this->programRepository = $programRepository;
        $this->logger = $logger;
    }

    /**
     * Menampilkan daftar Master Batch.
     *
     * @Route("", name="master_data_batchcsr_index", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(),
                                   [self::CONTEXT_NON_REL, 'with_custom']);
    }

    /**
     * Menampilkan detail record Master Batch.
     *
     * @Route("/{batchId}", name="master_data_batchcsr_fetch", methods={"GET"},
     *                      requirements={"batchId":"\d+"})
     *
     * @param BatchCsr $entity Persistent entity object of BatchCsr
     *
     * @return JsonResponse
     */
    public function fetch(?BatchCsr $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, [self::CONTEXT_NON_REL, 'with_custom']);
    }

    /**
     * Menambahkan record Master Batch CSR baru ke dalam database.
     *
     * @Route("/create", name="master_data_batchcsr_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(BatchCsrType::class, new BatchCsr(),
                                  ['mitra_repo' => $this->mitraRepository, 'program_repo' => $this->programRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        /** @var BatchCsr $entity */
        $entity = $form->getData();
        $duplicates = $this->repository->findDuplicates($entity);

        if (count($duplicates) > 0) {
            return $this->jsonError('Nama Batch tidak boleh duplicate.', JsonResponse::HTTP_BAD_REQUEST);
        } else {
            $this->em->persist($entity);
            $this->em->flush();
            $this->logger->info('TAMBAH record Batch CSR baru.', BatchCsr::class,
                                $entity, [self::CONTEXT_NON_REL]);

            return $this->jsonResponse($entity, -1, $this->context_norels);
        }
    }

    /**
     * Memperbarui record Master Batch CSR yang sudah ada.
     *
     * @Route("/update/{batchId}", name="master_data_batchcsr_update", methods={"POST"},
     *                             requirements={"batchId":"\d+"})
     *
     * @param Request  $request HTTP Request
     * @param BatchCsr $entity  Persistent entity object of BatchCsr
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?BatchCsr $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(BatchCsrType::class, $entity,
                                  ['mitra_repo' => $this->mitraRepository, 'program_repo' => $this->programRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $duplicates = $this->repository->findDuplicates($entity);

        if (count($duplicates) > 0) {
            return $this->jsonError('Nama Batch tidak boleh duplicate.', JsonResponse::HTTP_BAD_REQUEST);
        } else {
            $this->em->persist($entity);
            $this->logger->info('UPDATE record Batch CSR.', BatchCsr::class,
                                $entity, [self::CONTEXT_NON_REL]);

            return $this->jsonResponse($entity, -1, $this->context_norels);
        }
    }

    /**
     * Menghapus record Master Batch CSR dari database.
     *
     * @Route("/delete/{batchId}", name="master_data_batchcsr_delete", methods={"DELETE"},
     *                             requirements={"batchId":"\d+"})
     *
     * @param BatchCsr $entity Persistent entity object of BatchCsr
     *
     * @return JsonResponse
     */
    public function delete(?BatchCsr $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Batch tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->em->remove($entity);
            $this->logger->notice('HAPUS record Batch CSR.', BatchCsr::class,
                                  $entity, [self::CONTEXT_NON_REL]);

            return $this->jsonSuccess('Data Batch telah berhasil dihapus dari database');
        } catch (\Exception $ex) {
            return $this->jsonError('Data Batch tidak boleh dihapus, masih ada record yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }
    }

}