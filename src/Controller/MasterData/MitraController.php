<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\Mitra;
use App\Form\MasterData\MitraType;
use App\Models\MasterData\MitraModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MitraController
 *
 * @package App\Controller\MasterData
 * @author  Ahmad Fajar
 * @since   08/11/2018, modified: 11/02/2019 3:08
 *
 * @Route("/app/master-data/mitra")
 */
class MitraController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var MitraModel
     */
    private $model;


    /**
     * MitraController constructor.
     *
     * @param MitraModel $model
     */
    public function __construct(MitraModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan daftar Mitra.
     *
     * @Route("", name="master_data_mitra_index", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $paginator = $this->model->displayAllMitra($dq->getFilters(), $dq->getSorts(),
                                                   $dq->getPageSize(), $dq->getOffset());

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menambahkan Mitra Baru
     *
     * @Route("/create", name="master_data_mitra_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(MitraType::class, new Mitra(), [
            'programCsr_repo' => $this->model->getProgramCSRRepository(),
            'kecamatan_repo'  => $this->model->getKecamatanRepository(),
            'kelurahan_repo'  => $this->model->getKelurahanRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->model->save($entity);
        $this->model->getLogger()->info('TAMBAH record Mitra baru.', Mitra::class, $entity,
                                        [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail record Mitra
     *
     * @Route("/{mitraId}", methods={"GET"}, name="master_data_mitra_fetch", requirements={"mitraId":"\d+"})
     *
     * @param Mitra $entity Persistent entity object of Mitra
     *
     * @return JsonResponse
     */
    public function fetch(?Mitra $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data mitra tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_customrels);
    }

    /**
     * Memperbarui data Mitra
     *
     * @Route("/update/{mitraId}", methods={"POST"}, name="master_data_mitra_update", requirements={"mitraId":"\d+"})
     *
     * @param Request $request HTTP Request
     * @param Mitra   $entity  Persistent entity object of Mitra
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?Mitra $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data mitra tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(MitraType::class, $entity, [
            'programCsr_repo' => $this->model->getProgramCSRRepository(),
            'kecamatan_repo'  => $this->model->getKecamatanRepository(),
            'kelurahan_repo'  => $this->model->getKelurahanRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->model->save($entity);
        $this->model->getLogger()->info('UPDATE record Mitra.', Mitra::class, $entity,
                                        [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapsu data mitra dari database
     *
     * @Route("/delete/{mitraId}", name="master_data_mitra_delete", methods={"DELETE"}, requirements={"mitraId":"\d+"})
     *
     * @param Mitra $entity Persistent entity object of Mitra
     *
     * @return JsonResponse
     */
    public function delete(?Mitra $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data mitra tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->model->delete($entity);
            return $this->jsonSuccess('Data Mitra telah berhasil dihapus dari database');
        } catch (\Exception $exception) {
            return $this->jsonError('Gagal menghapus record mitra, masih ada data yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }
    }

}