<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\ProgramCSR;
use App\Form\MasterData\ProgramCSRType;
use App\Repository\MasterData\JenisProgramCSRRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProgramCSRController
 *
 * @package App\Controller\MasterData
 * @author  Mark Melvin
 * @since   9/27/2018 4:09 PM, modified: 25/06/2019 3:45
 *
 * @Route("/app/master-data/program-csr")
 */
class ProgramCsrController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var JenisProgramCSRRepository
     */
    private $repository;

    /**
     * @var JenisProgramCSRRepository
     */
    private $bidangCsrRepository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * ProgramCsrController constructor.
     *
     * @param ProgramCSRRepository      $repository          Entity repository instance
     * @param JenisProgramCSRRepository $bidangCsrRepository Entity repository instance
     * @param EntityManagerInterface    $entityManager       Doctrine entity manager
     * @param AuditLogger               $logger              AuditLogger service
     */
    public function __construct(ProgramCSRRepository $repository,
                                JenisProgramCSRRepository $bidangCsrRepository,
                                EntityManagerInterface $entityManager,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->bidangCsrRepository = $bidangCsrRepository;
        $this->logger = $logger;
    }

    /**
     * Menampilkan daftar Jenis CSR.
     *
     * @Route("", name="master_data_program_csr_index", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_parentrels);
    }

    /**
     * Menampilkan detail record ProgramCSR.
     *
     * @Route("/{id}", name="master_data_program_csr_fetch", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param ProgramCSR $entity Persistent entity object of JenisProgramCsr
     *
     * @return JsonResponse
     */
    public function fetch(?ProgramCSR $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_parentrels);
    }

    /**
     * Menambahkan Program CSR baru ke dalam database.
     *
     * @Route("/create", name="master_data_program_csr_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(ProgramCSRType::class, new ProgramCSR(),
                                  ['object_repo' => $this->bidangCsrRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->em->persist($entity);
        $this->em->flush();
        $this->logger->info('TAMBAH record Program CSR baru.', ProgramCSR::class, $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Memperbarui record Jenis Program CSR yang sudah ada.
     *
     * @Route("/update/{id}", name="master_data_program_csr_update", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request    $request HTTP Request
     * @param ProgramCSR $entity  Persistent entity object of JennisProgramCSR
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?ProgramCSR $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ProgramCSRType::class, $entity, ['object_repo' => $this->bidangCsrRepository]);
        $form->submit($request->request->all());
        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->em->persist($entity);
        $this->logger->info('UPDATE record Program CSR.', ProgramCSR::class, $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record Program CSR dari database.
     *
     * @Route("/delete/{id}", name="master_data_program_csr_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     *
     * @param ProgramCSR $entity Persistent entity object of JenisProgramCsr
     *
     * @return JsonResponse
     */
    public function delete(?ProgramCSR $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Record Program CSR tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->em->remove($entity);
            $this->logger->notice('HAPUS record Program CSR.', ProgramCSR::class, $entity, [self::CONTEXT_NON_REL]);

            return $this->jsonSuccess('Record Program CSR telah berhasil dihapus dari database');
        } catch (\Exception $e) {
            return $this->jsonError('Gagal menghapus record Program CSR, masih ada data yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }
    }

}