<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\Kelurahan;
use App\Form\MasterData\KelurahanType;
use App\Repository\MasterData\KecamatanRepository;
use App\Repository\MasterData\KelurahanRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KelurahanController
 *
 * @package App\Controller\MasterData
 * @author  Ahmad Fajar
 * @since   06/09/2018, modified: 03/02/2019 10:07
 *
 * @Route("/app/master-data/wilayah/kelurahan")
 */
class KelurahanController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var KelurahanRepository
     */
    private $repository;

    /**
     * @var KecamatanRepository
     */
    private $kecRepository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * KelurahanController constructor.
     *
     * @param EntityManagerInterface $entityManager       Doctrine entity manager
     * @param KelurahanRepository    $repository          Entity repository instance
     * @param KecamatanRepository    $kecamatanRepository Entity repository instance
     * @param AuditLogger            $logger              AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                KelurahanRepository $repository,
                                KecamatanRepository $kecamatanRepository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->kecRepository = $kecamatanRepository;
        $this->logger = $logger;
    }

    /**
     * Menampilkan daftar Kelurahan.
     *
     * @Route("", name="master_data_kelurahan_index", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }


    /**
     * Menampilkan detail record Kelurahan.
     *
     * @Route("/{kelurahanId}", name="master_data_kelurahan_fetch", methods={"GET"}, requirements={"kelurahanId":"\d+"})
     *
     * @param Kelurahan $entity Persistent entity object of Kelurahan
     *
     * @return JsonResponse
     */
    public function fetch(?Kelurahan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_withrels);
    }

    /**
     * Menambahkan Kelurahan baru ke dalam database.
     *
     * @Route("/create", name="master_data_kelurahan_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(KelurahanType::class, new Kelurahan(), ['object_repo' => $this->kecRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->em->persist($entity);
        $this->em->flush();
        $this->logger->info('TAMBAH record kelurahan baru.', 'Kelurahan', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Memperbarui record Kelurahan yang sudah ada.
     *
     * @Route("/update/{kelurahanId}", name="master_data_kelurahan_update", methods={"POST"},
     *                                 requirements={"kelurahanId":"\d+"})
     *
     * @param Request   $request HTTP Request
     * @param Kelurahan $entity  Persistent entity object of Kelurahan
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?Kelurahan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(KelurahanType::class, $entity, ['object_repo' => $this->kecRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->em->persist($entity);
        $this->logger->info('UPDATE record kelurahan.', 'Kelurahan', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record Kelurahan dari database.
     *
     * @Route("/delete/{kelurahanId}", name="master_data_kelurahan_delete", methods={"DELETE"},
     *                                 requirements={"kelurahanId":"\d+"})
     *
     * @param Kelurahan $entity Persistent entity object of Kelurahan
     *
     * @return JsonResponse
     */
    public function delete(?Kelurahan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data kelurahan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->em->remove($entity);
            $this->logger->notice('HAPUS record kelurahan.', 'Kelurahan', $entity, [self::CONTEXT_NON_REL]);

            return $this->jsonSuccess('Data Kelurahan telah berhasil dihapus dari database');
        } catch (\Exception $exception) {
            return $this->jsonError('Gagal menghapus record Kelurahan, masih ada data yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }
    }

}
