<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\JenisProgramCSR;
use App\Form\MasterData\JenisProgramCSRType;
use App\Repository\MasterData\JenisProgramCSRRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class JenisProgramCSRController
 *
 * @package App\Controller\MasterData
 * @author  Mark Melvin
 * @since   9/27/2018 4:09 PM, modified: 03/02/2019 9:58
 *
 * @Route("/app/master-data/bidang-csr")
 */
class BidangCsrController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var JenisProgramCSRRepository
     */
    private $repository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * BidangCsrController constructor.
     *
     * @param JenisProgramCSRRepository $repository    Entity repository instance
     * @param EntityManagerInterface    $entityManager Doctrine entity manager
     * @param AuditLogger               $logger        AuditLogger service
     */
    public function __construct(JenisProgramCSRRepository $repository,
                                EntityManagerInterface $entityManager,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * Menampilkan daftar Jenis CSR.
     *
     * @Route("", name="master_data_bidang_csr_index", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menampilkan detail record JenisProgramCSR.
     *
     * @Route("/{id}", name="master_data_bidang_csr_fetch", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param JenisProgramCSR $entity Persistent entity object of JenisProgramCsr
     *
     * @return JsonResponse
     */
    public function fetch(?JenisProgramCSR $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menambahkan Jenis Program CSR baru ke dalam database.
     *
     * @Route("/create", name="master_data_bidang_csr_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(JenisProgramCSRType::class, new JenisProgramCSR());
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->em->persist($entity);
        $this->em->flush();
        $this->logger->info('TAMBAH record Bidang CSR baru.', JenisProgramCSR::class, $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Memperbarui record Jenis Program CSR yang sudah ada.
     *
     * @Route("/update/{id}", name="master_data_bidang_csr_update", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request         $request HTTP Request
     * @param JenisProgramCSR $entity  Persistent entity object of JennisProgramCSR
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?JenisProgramCSR $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(JenisProgramCSRType::class, $entity);
        $form->submit($request->request->all());
        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->em->persist($entity);
        $this->logger->info('UPDATE record Bidang CSR.', JenisProgramCSR::class, $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record Jenis Program CSR dari database.
     *
     * @Route("/delete/{id}", name="master_data_bidang_csr_delete", methods={"DELETE"},
     *                        requirements={"id":"\d+"})
     *
     * @param JenisProgramCSR $entity Persistent entity object of JenisProgramCsr
     *
     * @return JsonResponse
     */
    public function delete(?JenisProgramCSR $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Bidang CSR tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        } elseif ($entity->getPrograms()->count() > 0) {
            return $this->jsonError('Record Bidang CSR tidak boleh dihapus, masih ada record yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }

        $this->em->remove($entity);
        $this->logger->notice('HAPUS record Bidang CSR.', JenisProgramCSR::class, $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonSuccess('Record Bidang CSR telah berhasil dihapus dari database');
    }

}