<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\DataReference;
use App\Form\MasterData\DataReferenceType;
use App\Models\MasterData\DataReferenceModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DataReferenceController
 *
 * @package App\Controller\MasterData
 * @author  Tri
 * @since   06/02/2019, modified: 31/03/2019 2:00
 *
 * @Route("/app/master-data/data-reference")
 */
class DataReferenceController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var DataReferenceModel
     */
    private $model;


    /**
     * DataReferenceController constructor.
     *
     * @param DataReferenceModel $model
     */
    public function __construct(DataReferenceModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan DataReference dengan kriteria tertentu
     *
     * @Route("", name="master_data_reference_index", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $paginator = $this->model->displayAllDataRef($dq->getFilters(), $dq->getSorts(),
                                                     $dq->getPageSize(), $dq->getOffset());

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_parentrels);
    }

    /**
     * Menambahkan DataReference Baru
     *
     * @Route("/create", name="master_data_reference_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(DataReferenceType::class, new DataReference(), [
            'dataReference_repo' => $this->model->getRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->model->save($entity);
        $this->model->getLogger()->info('TAMBAH record Data Referensi baru.', DataReference::class, $entity,
                                        [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail record DataReference
     *
     * @Route("/{id}", methods={"GET"}, name="master_data_reference_fetch", requirements={"id":"\d+"})
     *
     * @param DataReference $entity Persistent entity object of DataReference
     *
     * @return JsonResponse
     */
    public function fetch(?DataReference $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data referensi tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_parentrels);
    }

    /**
     * Memperbarui DataReference
     *
     * @Route("/update/{id}", methods={"POST"}, name="master_data_reference_update", requirements={"id":"\d+"})
     *
     * @param Request       $request HTTP Request
     * @param DataReference $entity  Persistent entity object of DataReference
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?DataReference $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data referensi tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(DataReferenceType::class, $entity, [
            'dataReference_repo' => $this->model->getRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->model->save($entity);
        $this->model->getLogger()->info('UPDATE record Data Referensi.', DataReference::class, $entity,
                                        [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus DataReference dari database
     *
     * @Route("/delete/{id}", name="master_data_reference_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     *
     * @param DataReference $entity Persistent entity object of DataReference
     *
     * @return JsonResponse
     */
    public function delete(?DataReference $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data referensi tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->model->delete($entity);
            return $this->jsonSuccess('Data referensi telah berhasil dihapus dari database');
        } catch (\LogicException $exc) {
            return $this->jsonError($exc->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (\Exception $exception) {
            return $this->jsonError('Gagal menghapus record data referensi, masih ada data yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }
    }
}
