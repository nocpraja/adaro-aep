<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\Kecamatan;
use App\Form\MasterData\KecamatanType;
use App\Repository\MasterData\KabupatenRepository;
use App\Repository\MasterData\KecamatanRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KecamatanController
 *
 * @package App\Controller\MasterData
 * @author  Ahmad Fajar
 * @since   06/09/2018, modified: 03/02/2019 10:05
 *
 * @Route("/app/master-data/wilayah/kecamatan")
 */
class KecamatanController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var KecamatanRepository
     */
    private $repository;

    /**
     * @var KabupatenRepository
     */
    private $kabRepository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * KecamatanController constructor.
     *
     * @param EntityManagerInterface $entityManager       Doctrine entity manager
     * @param KecamatanRepository    $repository          Entity repository instance
     * @param KabupatenRepository    $kabupatenRepository Entity repository instance
     * @param AuditLogger            $logger              AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                KecamatanRepository $repository,
                                KabupatenRepository $kabupatenRepository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->kabRepository = $kabupatenRepository;
        $this->logger = $logger;
    }

    /**
     * Menampilkan daftar Kecamatan.
     *
     * @Route("", name="master_data_kecamatan_index", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menampilkan detail record Kecamatan.
     *
     * @Route("/{kecamatanId}", name="master_data_kecamatan_fetch", methods={"GET"},
     *                          requirements={"kecamatanId":"\d+"})
     *
     * @param Kecamatan $entity Persistent entity object of Kecamatan
     *
     * @return JsonResponse
     */
    public function fetch(?Kecamatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_withrels);
    }

    /**
     * Menambahkan Kecamatan baru ke dalam database.
     *
     * @Route("/create", name="master_data_kecamatan_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(KecamatanType::class, new Kecamatan(), ['object_repo' => $this->kabRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->em->persist($entity);
        $this->em->flush();
        $this->logger->info('TAMBAH record kecamatan baru.', 'Kecamatan', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Memperbarui record Kecamatan yang sudah ada.
     *
     * @Route("/update/{kecamatanId}", name="master_data_kecamatan_update", methods={"POST"},
     *                                 requirements={"kecamatanId":"\d+"})
     *
     * @param Request   $request HTTP Request
     * @param Kecamatan $entity  Persistent entity object of Kecamatan
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?Kecamatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(KecamatanType::class, $entity, ['object_repo' => $this->kabRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->em->persist($entity);
        $this->logger->info('UPDATE record kecamatan.', 'Kecamatan', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record Kecamatan dari database.
     *
     * @Route("/delete/{kecamatanId}", name="master_data_kecamatan_delete", methods={"DELETE"},
     *                                 requirements={"kecamatanId":"\d+"})
     *
     * @param Kecamatan $entity Persistent entity object of Kecamatan
     *
     * @return JsonResponse
     */
    public function delete(?Kecamatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data kecamatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        } elseif ($entity->getKelurahans()->count() > 0) {
            return $this->jsonError('Data Kecamatan tidak boleh dihapus, masih ada record yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }

        $this->em->remove($entity);
        $this->logger->notice('HAPUS record kecamatan.', 'Kecamatan', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonSuccess('Data Kecamatan telah berhasil dihapus dari database');
    }
}