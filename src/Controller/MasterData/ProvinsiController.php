<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\Provinsi;
use App\Form\MasterData\ProvinsiType;
use App\Repository\MasterData\ProvinsiRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProvinsiController
 *
 * @package App\Controller\MasterData
 * @author  Ahmad Fajar
 * @since   06/09/2018, modified: 24/02/2019 14:14
 *
 * @Route("/app/master-data/wilayah/provinsi")
 */
class ProvinsiController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ProvinsiRepository
     */
    private $repository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * ProvinsiController constructor.
     *
     * @param ProvinsiRepository     $repository    Entity repository instance
     * @param EntityManagerInterface $entityManager Doctrine entity manager
     * @param AuditLogger            $logger        AuditLogger service
     */
    public function __construct(ProvinsiRepository $repository,
                                EntityManagerInterface $entityManager,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * Menampilkan daftar Provinsi.
     *
     * @Route("", name="master_data_provinsi_index", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset(),
                                                      $dq->getCondition());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menampilkan detail record Provinsi.
     *
     * @Route("/{provinsiId}", name="master_data_provinsi_fetch", methods={"GET"}, requirements={"provinsiId":"\d+"})
     *
     * @param Provinsi $entity Persistent entity object of Provinsi
     *
     * @return JsonResponse
     */
    public function fetch(?Provinsi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menambahkan Provinsi baru ke dalam database.
     *
     * @Route("/create", name="master_data_provinsi_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(ProvinsiType::class, new Provinsi());
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->em->persist($entity);
        $this->em->flush();
        $this->logger->info('TAMBAH record provinsi baru.', 'Provinsi', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Memperbarui record Provinsi yang sudah ada.
     *
     * @Route("/update/{provinsiId}", name="master_data_provinsi_update", methods={"POST"},
     *                                requirements={"provinsiId":"\d+"})
     *
     * @param Request  $request HTTP Request
     * @param Provinsi $entity  Persistent entity object of Provinsi
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?Provinsi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ProvinsiType::class, $entity);
        $form->submit($request->request->all());
        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->em->persist($entity);
        $this->logger->info('UPDATE record provinsi.', 'Provinsi', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record Provinsi dari database.
     *
     * @Route("/delete/{provinsiId}", name="master_data_provinsi_delete", methods={"DELETE"},
     *                                requirements={"provinsiId":"\d+"})
     *
     * @param Provinsi $entity Persistent entity object of Provinsi
     *
     * @return JsonResponse
     */
    public function delete(?Provinsi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data provinsi tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        } elseif ($entity->getKabupatens()->count() > 0) {
            return $this->jsonError('Data Provinsi tidak boleh dihapus, masih ada record yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }

        $this->em->remove($entity);
        $this->logger->notice('HAPUS record provinsi.', 'Provinsi', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonSuccess('Data provinsi telah berhasil dihapus dari database');
    }

}
