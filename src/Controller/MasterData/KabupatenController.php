<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MasterData;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\Kabupaten;
use App\Form\MasterData\KabupatenType;
use App\Repository\MasterData\KabupatenRepository;
use App\Repository\MasterData\ProvinsiRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KabupatenController
 *
 * @package App\Controller\MasterData
 * @author  Ahmad Fajar
 * @since   06/09/2018, modified: 24/02/2019 14:16
 *
 * @Route("/app/master-data/wilayah/kabupaten")
 */
class KabupatenController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var KabupatenRepository
     */
    private $repository;

    /**
     * @var ProvinsiRepository
     */
    private $provRepository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * KabupatenController constructor.
     *
     * @param EntityManagerInterface $entityManager  Doctrine entity manager
     * @param KabupatenRepository    $repository     Entity repository instance
     * @param ProvinsiRepository     $provRepository Entity repository instance
     * @param AuditLogger            $logger         AuditLogger service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                KabupatenRepository $repository,
                                ProvinsiRepository $provRepository,
                                AuditLogger $logger)
    {
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->provRepository = $provRepository;
        $this->logger = $logger;
    }

    /**
     * Menampilkan daftar Kabupaten.
     *
     * @Route("", name="master_data_kabupaten_index", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset(),
                                                      $dq->getCondition());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menampilkan single data Kabupaten
     *
     * @Route("/{kabupatenId}", name="master_data_kabupaten_fetch", methods={"GET"},
     *                          requirements={"kabupatenId":"\d+"})
     *
     * @param Kabupaten $entity Persistent entity object of Kabupaten
     *
     * @return JsonResponse
     */
    public function fetch(?Kabupaten $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_withrels);
    }

    /**
     * Menambah Kabupaten baru
     *
     * @Route("/create", name="master_data_kabupaten_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(KabupatenType::class, new Kabupaten(), ['object_repo' => $this->provRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->em->persist($entity);
        $this->em->flush();
        $this->logger->info('TAMBAH record kabupaten baru.', 'Kabupaten', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Memperbarui data Kabupaten yang sudah ada
     *
     * @Route("/update/{kabupatenId}", name="master_data_kabupaten_update", methods={"POST"},
     *                                 requirements={"kabupatenId":"\d+"})
     *
     * @param Request   $request HTTP Request
     * @param Kabupaten $entity  Persistent entity object of Kabupaten
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?Kabupaten $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(KabupatenType::class, $entity, ['object_repo' => $this->provRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->em->persist($entity);
        $this->logger->info('UPDATE record kabupaten.', 'Kabupaten', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus data kabupaten
     *
     * @Route("/delete/{kabupatenId}", name="master_data_kabupaten_delete", methods={"DELETE"},
     *                                 requirements={"kabupatenId":"\d+"})
     *
     * @param Kabupaten $entity Persistent entity object of Kabupaten
     *
     * @return JsonResponse
     */
    public function delete(?Kabupaten $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data kabupaten tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        } else if ($entity->getKecamatans()->count() > 0) {
            return $this->jsonError('Data kabupaten tidak boleh dihapus, masih ada record yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }

        $this->em->remove($entity);
        $this->logger->notice('HAPUS record kabupaten.', 'Kabupaten', $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonSuccess('Data kabupaten telah berhasil dihapus dari database');
    }
}
