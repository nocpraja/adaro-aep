<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Controller\Pendanaan;


use App\Component\Controller\PendanaanController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Pendanaan\DanaProgram;
use App\Form\Pendanaan\DanaProgramType;
use App\Models\Pendanaan\DanaProgramModel;
use App\Repository\MasterData\ProgramCSRRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DanaProgramController
 *
 * @package App\Controller\Pendanaan
 * @author  Ahmad Fajar
 * @since   29/04/2019, modified: 05/05/2019 14:28
 *
 * @Route("/app/pendanaan/program")
 */
class DanaProgramController extends PendanaanController
{

    /**
     * @var ProgramCSRRepository
     */
    private $programRepository;


    /**
     * DanaProgramController constructor.
     *
     * @param DanaProgramModel     $model
     * @param ProgramCSRRepository $programRepository
     */
    public function __construct(DanaProgramModel $model, ProgramCSRRepository $programRepository)
    {
        $this->model = $model;
        $this->programRepository = $programRepository;
    }

    /**
     * Menampilkan data Dana Program.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request));

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menambah record Dana Program baru.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new DanaProgram();
        $form = $this->createForm(DanaProgramType::class, $entity, [
            'dana_repo'    => $this->model->getDanaBidangRepository(),
            'program_repo' => $this->programRepository
        ]);
        $form->submit($request->request->all());

        return $this->invokeCreate($form, $entity);
    }

    /**
     * Menampilkan detail informasi Dana Program.
     *
     * @Route("/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param DanaProgram|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function fetch(?DanaProgram $entity): JsonResponse
    {
        return $this->invokeFetch('Dana Program', $entity,
                                  [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE, self::CONTEXT_WITH_PARENT]);
    }

    /**
     * Menghapus data Dana Program dari database.
     *
     * @Route("/delete/{id}", requirements={"id":"\d+"}, methods={"DELETE"})
     *
     * @param DanaProgram|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function delete(?DanaProgram $entity): JsonResponse
    {
        return $this->invokeDelete('Dana Program', $entity);
    }

    /**
     * Proses workflow transition pada Dana Program.
     *
     * @Route("/stepflow/{id}", methods={"PATCH"}, requirements={"id":"\d+"})
     *
     * @param Request          $request HTTP Request
     * @param DanaProgram|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, ?DanaProgram $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Program tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        return $this->invokeWorkflowTransition($form, $entity, $data);
    }

    /**
     * Simpan perubahan data Dana Program.
     *
     * @Route("/update/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request          $request HTTP Request
     * @param DanaProgram|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?DanaProgram $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Program tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(DanaProgramType::class, $entity, [
            'dana_repo'    => $this->model->getDanaBidangRepository(),
            'program_repo' => $this->programRepository
        ]);
        $form->submit($request->request->all());

        return $this->invokeUpdate($form, $entity);
    }

    /**
     * Proses request revisi anggaran Dana Program.
     *
     * @Route("/revisi/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request          $request HTTP Request
     * @param DanaProgram|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function revisi(Request $request, ?DanaProgram $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Program tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(DanaProgramType::class, $entity, [
            'dana_repo'    => $this->model->getDanaBidangRepository(),
            'program_repo' => $this->programRepository
        ]);
        $form->submit($request->request->all());

        return $this->invokeRevisi($form, $entity);
    }

}