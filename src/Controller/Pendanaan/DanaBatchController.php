<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Controller\Pendanaan;


use App\Component\Controller\PendanaanController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Pendanaan\DanaBatch;
use App\Form\Pendanaan\DanaBatchRevisiType;
use App\Form\Pendanaan\DanaBatchType;
use App\Models\Pendanaan\DanaBatchModel;
use App\Repository\MasterData\BatchCsrRepository;
use App\Repository\MasterData\DataReferenceRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DanaBatchController
 *
 * @package App\Controller\Pendanaan
 * @author  Ahmad Fajar
 * @since   30/04/2019, modified: 12/05/2019 3:23
 *
 * @Route("/app/pendanaan/batch")
 */
class DanaBatchController extends PendanaanController
{

    /**
     * @var BatchCsrRepository
     */
    private $batchRepository;

    /**
     * @var DataReferenceRepository
     */
    private $datarefRepository;


    /**
     * DanaBatchController constructor.
     *
     * @param DanaBatchModel          $model
     * @param BatchCsrRepository      $batchRepository
     * @param DataReferenceRepository $datarefRepository
     */
    public function __construct(DanaBatchModel $model, BatchCsrRepository $batchRepository,
                                DataReferenceRepository $datarefRepository)
    {
        $this->model = $model;
        $this->batchRepository = $batchRepository;
        $this->datarefRepository = $datarefRepository;
    }

    /**
     * Menampilkan data Dana Batch.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request), $this->getUser());

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menambah record Dana Batch baru.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new DanaBatch();
        $form = $this->createForm(DanaBatchType::class, $entity, [
            'batch_repo'   => $this->batchRepository,
            'dataref_repo' => $this->datarefRepository,
            'dana_repo'    => $this->model->getProgramModel()->getRepository(),
        ]);
        $form->submit($request->request->all());

        return $this->invokeCreate($form, $entity);
    }

    /**
     * Menampilkan detail informasi Dana Batch.
     *
     * @Route("/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param DanaBatch|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function fetch(?DanaBatch $entity): JsonResponse
    {
        return $this->invokeFetch('Dana Batch', $entity,
                                  [self::CONTEXT_WITH_REL, self::CONTEXT_WITH_CHILD, self::CONTEXT_WITH_POSTDATE]);
    }

    /**
     * Menampilkan daftar program pendanaan yang bisa dijadikan sebagai sumber dana untuk revisi Dana Batch.
     *
     * @Route("/sumberdana-revisi", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function sumberDanaRevisi(Request $request): JsonResponse
    {
        $filters = DataQuery::parseRequest($request)->getFilters();

        if (!empty($filters)) {
            $entity = $this->model->getRepository()->find($filters[0]->getValue());

            if (!is_null($entity)) {
                $items = $this->model->collectSumberDanaRevisi($entity);
                return $this->jsonResponse($items, count($items));
            }
        }

        return $this->jsonResponse([]);
    }

    /**
     * Menghapus data Dana Batch dari database.
     *
     * @Route("/delete/{id}", requirements={"id":"\d+"}, methods={"DELETE"})
     *
     * @param DanaBatch|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function delete(?DanaBatch $entity): JsonResponse
    {
        return $this->invokeDelete('Dana Batch', $entity);
    }

    /**
     * Proses workflow transition pada Dana Batch.
     *
     * @Route("/stepflow/{id}", methods={"PATCH"}, requirements={"id":"\d+"})
     *
     * @param Request        $request HTTP Request
     * @param DanaBatch|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, ?DanaBatch $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Batch tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        return $this->invokeWorkflowTransition($form, $entity, $data);
    }

    /**
     * Simpan perubahan data Dana Batch.
     *
     * @Route("/update/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request        $request HTTP Request
     * @param DanaBatch|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?DanaBatch $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Batch tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(DanaBatchType::class, $entity, [
            'batch_repo'   => $this->batchRepository,
            'dataref_repo' => $this->datarefRepository,
            'dana_repo'    => $this->model->getProgramModel()->getRepository()
        ]);
        $form->submit($request->request->all());

        return $this->invokeUpdate($form, $entity);
    }

    /**
     * Proses request revisi anggaran Dana Batch.
     *
     * @Route("/revisi/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request        $request HTTP Request
     * @param DanaBatch|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function revisi(Request $request, ?DanaBatch $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Batch tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(DanaBatchRevisiType::class, $entity);
        $form->submit($request->request->all());

        return $this->invokeRevisi($form, $entity);
    }

}