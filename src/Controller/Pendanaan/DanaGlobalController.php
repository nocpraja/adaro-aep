<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Pendanaan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Pendanaan\DanaGlobal;
use App\Form\Pendanaan\DanaGlobalType;
use App\Models\Pendanaan\DanaGlobalModel;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DanaGlobalController
 *
 * @package App\Controller\Pendanaan
 * @author  Ahmad Fajar
 * @since   03/11/2018, modified: 11/02/2020 05:59
 *
 * @Route("/app/pendanaan/global")
 */
class DanaGlobalController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var DanaGlobalModel
     */
    private $model;


    /**
     * DanaGlobalController constructor.
     *
     * @param DanaGlobalModel $model
     */
    public function __construct(DanaGlobalModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan data Dana Global.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request));

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), [
            self::CONTEXT_NON_REL,
            self::CONTEXT_WITH_POSTDATE
        ]);
    }

    /**
     * Create or setup awal Dana Global.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new DanaGlobal();
        $form = $this->createForm(DanaGlobalType::class, $entity);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->create($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menampilkan detail informasi Dana Global.
     *
     * @Route("/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param DanaGlobal|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function fetch(?DanaGlobal $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError(sprintf("Data %s tidak ditemukan.", 'Dana Global'),
                JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE, self::CONTEXT_WITH_PARENT]);

    }

    /**
     * Menampilkan data terakhir Dana Global.
     *
     * @Route("/lastinfo", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function lastInfo(): JsonResponse
    {
        $results = $this->model->getRepository()->getLastNRecords(1);

        return $this->jsonResponse(
            (empty($results) ? null : $results[0]), -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]
        );
    }

    /**
     * Simpan perubahan data Dana Global.
     *
     * @Route("/update/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request         $request HTTP Request
     * @param DanaGlobal|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?DanaGlobal $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(DanaGlobalType::class, $entity);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $entity->setUpdatedBy($this->getUser());
            $this->model->update($entity);

            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Proses workflow transition pada Dana Global.
     *
     * @Route("/stepflow/{id}", methods={"PATCH"}, requirements={"id":"\d+"})
     *
     * @param Request         $request HTTP Request
     * @param DanaGlobal|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, ?DanaGlobal $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);

            if ($data->getAction() == WorkflowTags::STEP_REJECT) {
                $message = sprintf('REJECT transaksi Dana Global nomor %s berhasil.', $entity->getKode());
            } else {
                $message = sprintf('Transaksi Dana Global nomor %s telah berhasil diverifikasi.', $entity->getKode());
            }

            return $this->jsonSuccess($message);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
