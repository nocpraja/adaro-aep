<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Controller\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Component\DataObject\SortOrFilter;
use App\Entity\Pendanaan\DanaBidang;
use App\Models\Pendanaan\DanaBidangModel;
use App\Repository\Pendanaan\LogApprovalPendanaanRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DanaBidangRiwayatController
 *
 * @package App\Controller\Pendanaan
 * @author  Mark Melvin
 * @since   25/06/2019, modified: 11/02/2020 04:40
 *
 * @Route("/app/pendanaan/bidang/riwayat")
 */
class DanaBidangRiwayatController extends JsonController
{

    /**
     * @var DanaBidangModel
     */
    private $model;
    /**
     * @var LogApprovalPendanaanRepository
     */
    private $approvalPendanaanRepository;


    /**
     * RiwayatDanaBidangController constructor.
     *
     * @param DanaBidangModel                $model
     * @param LogApprovalPendanaanRepository $approvalPendanaanRepository
     */
    public function __construct(DanaBidangModel $model,
                                LogApprovalPendanaanRepository $approvalPendanaanRepository)
    {
        $this->model = $model;
        $this->approvalPendanaanRepository = $approvalPendanaanRepository;
    }

    /**
     * Menampilkan data riwayat transaksi Dana Bidang.
     *
     * @Route("/{id}", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param Request         $request HTTP Request
     * @param DanaBidang|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function index(Request $request, ?DanaBidang $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Bidang tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $dq = DataQuery::parseRequest($request);
        $filter = $dq->getFilters();
        $filter[] = new SortOrFilter('danaBidang', NULL, $entity);
        $dq->setFilters($filter);
        $paginator = $this->model->displayTransactionHistories($dq);

        return $this->jsonResponse(
            $paginator->getIterator(),
            $paginator->count(),
            [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]
        );
    }

    /**
     * Menampilkan informasi terakhir riwayat transaksi Dana Bidang.
     *
     * @Route("/lastinfo/", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function lastInfo(Request $request): JsonResponse
    {
        $tid = $request->query->getInt('tid');
        $danaBidang = $this->model->getRepository()->find($tid);
        $entity = $this->model->lastTransaction($danaBidang);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan data riwayat verifikasi Dana Bidang.
     * @Route("/verification/{id}", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function verification($id): JsonResponse
    {
        $result = $this->approvalPendanaanRepository->findBy(['danaBidang' => $id], ['postedDate' => 'asc']);
        if (empty($result)) {
            return $this->jsonError('Data Verifikasi Dana Bidang tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($result, count($result), [
            self::CONTEXT_NON_REL,
            self::CONTEXT_WITH_CHILD,
            self::CONTEXT_WITH_POSTDATE
        ]);
    }
}
