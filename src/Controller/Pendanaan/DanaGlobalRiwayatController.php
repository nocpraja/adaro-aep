<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Controller\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Pendanaan\DanaGlobal;
use App\Models\Pendanaan\DanaGlobalModel;
use App\Repository\Pendanaan\LogApprovalPendanaanRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RiwayatDanaGlobalController
 *
 * @package App\Controller\Pendanaan
 * @author  Ahmad Fajar
 * @since   24/04/2019, modified: 11/02/2020 04:40
 *
 * @Route("/app/pendanaan/global/riwayat")
 */
class DanaGlobalRiwayatController extends JsonController
{

    /**
     * @var DanaGlobalModel
     */
    private $model;
    /**
     * @var LogApprovalPendanaanRepository
     */
    private $approvalPendanaanRepository;


    /**
     * RiwayatDanaGlobalController constructor.
     *
     * @param DanaGlobalModel                $model
     * @param LogApprovalPendanaanRepository $approvalPendanaanRepository
     */
    public function __construct(DanaGlobalModel $model,
                                LogApprovalPendanaanRepository $approvalPendanaanRepository)
    {
        $this->model = $model;
        $this->approvalPendanaanRepository = $approvalPendanaanRepository;
    }

    /**
     * Menampilkan data riwayat transaksi Dana Global.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $result = $this->model->displayTransactionHistories(DataQuery::parseRequest($request));

        return $this->jsonResponse(
            $result->getIterator(), $result->count(), [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]
        );
    }

    /**
     * Menampilkan informasi terakhir riwayat transaksi Dana Global.
     *
     * @Route("/lastinfo", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function lastInfo(): JsonResponse
    {
        $entity = $this->model->lastTransaction();

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan data riwayat approval transaksi Dana Global.
     *
     * @Route("/approval/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param DanaGlobal|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function logApproval(?DanaGlobal $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $results = $this->model->displayLogApproval($entity);

        return $this->jsonResponse($results, count($results), [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]);
    }

    /**
     * Menampilkan data riwayat verifikasi Dana Global.
     * @Route("/verification/{id}", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function verification($id): JsonResponse
    {
        $result = $this->approvalPendanaanRepository->findBy(['danaGlobal' => $id], ['postedDate' => 'asc']);
        if (empty($result)) {
            return $this->jsonError('Data Verifikasi Dana Global tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($result, count($result), [
            self::CONTEXT_NON_REL,
            self::CONTEXT_WITH_CHILD,
            self::CONTEXT_WITH_POSTDATE
        ]);
    }
}
