<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Controller\Pendanaan;


use App\Component\Controller\PendanaanController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Pendanaan\DanaBidang;
use App\Form\Pendanaan\DanaBidangType;
use App\Models\Pendanaan\DanaBidangModel;
use App\Repository\MasterData\JenisProgramCSRRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DanaBidangController
 *
 * @package App\Controller\Pendanaan
 * @author  Ahmad Fajar
 * @since   27/04/2019, modified: 05/05/2019 14:21
 *
 * @Route("/app/pendanaan/bidang")
 */
class DanaBidangController extends PendanaanController
{

    /**
     * @var JenisProgramCSRRepository
     */
    private $bidangRepository;


    /**
     * DanaBidangController constructor.
     *
     * @param DanaBidangModel           $model            Dana Bidang domain model
     * @param JenisProgramCSRRepository $bidangRepository Entity repository instance
     */
    public function __construct(DanaBidangModel $model, JenisProgramCSRRepository $bidangRepository)
    {
        $this->model = $model;
        $this->bidangRepository = $bidangRepository;
    }

    /**
     * Menampilkan data Dana Bidang.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request));

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menambah record Dana Bidang baru.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new DanaBidang();
        $form = $this->createForm(DanaBidangType::class, $entity, ['repo' => $this->bidangRepository]);
        $form->submit($request->request->all());

        return $this->invokeCreate($form, $entity);
    }

    /**
     * Menampilkan detail informasi Dana Bidang.
     *
     * @Route("/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param DanaBidang|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function fetch(?DanaBidang $entity): JsonResponse
    {
        return $this->invokeFetch('Dana Bidang', $entity);
    }

    /**
     * Menghapus data Dana Bidang dari database.
     *
     * @Route("/delete/{id}", requirements={"id":"\d+"}, methods={"DELETE"})
     *
     * @param DanaBidang|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function delete(?DanaBidang $entity): JsonResponse
    {
        return $this->invokeDelete('Dana Bidang', $entity);
    }

    /**
     * Proses workflow transition pada Dana Bidang.
     *
     * @Route("/stepflow/{id}", methods={"PATCH"}, requirements={"id":"\d+"})
     *
     * @param Request    $request HTTP Request
     * @param DanaBidang $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, ?DanaBidang $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Bidang tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        return $this->invokeWorkflowTransition($form, $entity, $data);
    }

    /**
     * Simpan perubahan data Dana Bidang.
     *
     * @Route("/update/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request    $request HTTP Request
     * @param DanaBidang $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?DanaBidang $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Bidang tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(DanaBidangType::class, $entity, ['repo' => $this->bidangRepository]);
        $form->submit($request->request->all());

        return $this->invokeUpdate($form, $entity);
    }

    /**
     * Proses request revisi anggaran Dana Bidang.
     *
     * @Route("/revisi/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param Request         $request HTTP Request
     * @param DanaBidang|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function revisi(Request $request, ?DanaBidang $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Dana Bidang tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(DanaBidangType::class, $entity, ['repo' => $this->bidangRepository]);
        $form->submit($request->request->all());

        return $this->invokeRevisi($form, $entity);
    }

}