<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller;


use App\Component\Controller\JsonController;
use App\Entity\MasterData\JenisProgramCSR;
use App\Models\Dashboard\DashboardModel;
use App\Models\Kegiatan\KpiModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 *
 * @package App\Controller
 * @author  Leo Nadeak
 * @since   01/03/2019, modified: 05/04/2019 12:06
 *
 * @Route("/app/dashboard/")
 */
class DashboardController extends JsonController
{
    /**
     * @var DashboardModel
     */
    private $model;

    /**
     * @var KpiModel
     * @deprecated
     */
    private $kpiModel;


    /**
     * DashboardController constructor.
     *
     * @param KpiModel $kpiModel Domain model
     * @param DashboardModel $model
     */
    public function __construct(DashboardModel $model,
                                KpiModel $kpiModel)
    {
        $this->model = $model;
        $this->kpiModel = $kpiModel;
    }

    /**
     * @Route("overview", methods={"GET"})
     * @deprecated
     */
    public function index(): JsonResponse
    {
        $result = [];

        return $this->jsonResponse($result);
    }


    /**
     * @Route("program-overview", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function programOvr(): JsonResponse
    {
        $result = $this->model->overviewPrograms();

        return $this->jsonResponse($result, -1, $this->context_withrels+['skip_null_values' => true]);
    }


}
