<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller;


use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class AuthenticationController
 *
 * @package App\Controller
 * @author  Ahmad Fajar
 * @since   16/08/2018, modified: 19/05/2019 15:48
 */
class AuthenticationController extends AbstractController
{
    /**
     * @var AuthenticationUtils
     */
    private $authUtils;


    /**
     * AuthenticationController constructor.
     *
     * @param AuthenticationUtils $authUtils
     */
    public function __construct(AuthenticationUtils $authUtils)
    {
        $this->authUtils = $authUtils;
    }

    /**
     * Menampilkan halaman Login.
     *
     * @Route("/login", name="auth_login_form")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $session = $request->hasPreviousSession() ? $request->getSession() : $this->get('session');

        if (!empty($session)) {
            if (null !== ($token = $session->get('_security_secured_area'))) {
                $token = unserialize($token);
                if ($token instanceof TokenInterface) {
                    if (null !== ($user = $token->getUser()) && is_object($user)) {
                        return $this->redirectToRoute('default_homepage');
                    }
                }
            }
        }

        return $this->render('authentication/index.html.twig', [
            'error' => $this->authUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * This is the route the login form submits to.
     *
     * @Route("/app/login_check", name="auth_login_check")
     *
     * @throws Exception
     */
    public function loginCheck()
    {
        throw new Exception('This should never be reached!');
    }

    /**
     * This is the route the user can use to logout.
     *
     * @Route("/app/logout", name="auth_logout")
     *
     * @throws Exception
     */
    public function logout()
    {
        throw new Exception('This should never be reached and executed!');
    }

}
