<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Controller\User;


use App\Component\Controller\JsonController;
use App\Component\DataObject\ChangePasswordDto;
use App\Component\Util\TokenGenerator;
use App\Entity\Security\UserAccount;
use App\Event\User\ForgotPasswordEvent;
use App\Form\User\ChangePasswordType;
use App\Form\User\ForgotPasswordType;
use App\Repository\Security\UserAccountRepository;
use App\Service\Logger\AuditLogger;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;

/**
 * Class ForgotPasswordController
 *
 * @package App\Controller\User
 * @author  Tri N
 * @since   28/03/2019, modified: 2019-04-18 11:32
 */
class ForgotPasswordController extends AbstractController
{

    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * @var UserAccountRepository
     */
    private $repository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;


    /**
     * ForgotPasswordController constructor.
     *
     * @param AuditLogger                  $auditLogger     Audit logger service
     * @param UserAccountRepository        $repository      Entity  repository instance
     * @param UserPasswordEncoderInterface $passwordEncoder Password encryption service
     */
    public function __construct(AuditLogger $auditLogger, UserAccountRepository $repository,
                                UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->repository = $repository;
        $this->logger = $auditLogger;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Menampilkan halaman forgot password (check email form)
     *
     * @Route("/forgot-password", methods={"GET", "POST"})
     *
     * @param Request                  $request    HTTP Request
     * @param EventDispatcherInterface $dispatcher Symfony event dispatcher service
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function index(Request $request, EventDispatcherInterface $dispatcher): Response
    {
        $action = '';
        $form = $this->createForm(ForgotPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $tokenKey = $request->request->get("_csrf");

            if ($form->isValid() && $this->isCsrfTokenValid(ForgotPasswordType::CSRF_NAME, $tokenKey)) {
                $action = "success";
                $formData = $form->getData();
                $user = $this->repository->loadUserByUsername($formData["email"]);

                if (!empty($user) && $user instanceof UserAccount) {
                    $this->addFlash('success', 'ForgotPassword.UserAccountValid');
                    $event = new ForgotPasswordEvent($user);
                    $dispatcher->dispatch(ForgotPasswordEvent::NAME, $event);
                } else {
                    $this->addFlash('success', 'ForgotPassword.UserAccountInvalid');
                }
            } else {
                $action = "error";
                $this->addFlash('danger', 'ForgotPassword.FormInvalid');
            }
        }

        return $this->render("user/forgot_password/index.html.twig", [
            "action" => $action,
            "form"   => $form->createView()
        ]);
    }

    /**
     * Menampilkan halaman token forgot pasword telah expired.
     *
     * @Route("/forgot-password/link-expired")
     *
     * @return Response
     */
    public function linkExpired(): Response
    {
        return $this->render('user/forgot_password/link_expired.html.twig');
    }

    /**
     * Memperbarui password melalui prosedur <b>Forgot Password</b>.
     * TokenKey dan TokenValue merupakan <b>key-pairs</b> sehingga keduanya haruslah match.
     *
     * @Route("/change-password/{tokenKey}/{tokenValue}", methods={"GET", "POST"})
     *
     * @param Request $request    HTTP Request
     * @param string  $tokenKey   Request Token key
     * @param string  $tokenValue Request Token value
     *
     * @return Response
     * @throws Exception
     */
    public function changePassword(Request $request, string $tokenKey, string $tokenValue): Response
    {
        $generator = new TokenGenerator();
        $token = new CsrfToken($tokenKey, $tokenValue);
        $userEntity = $this->repository->findOneBy(['tokenKey' => $tokenKey]);

        if (!is_null($userEntity) && $generator->isTokenValid($userEntity->getUsername(), $token)) {
            $now = new DateTime();

            if ($userEntity->getTokenExpiry() > $now) {
                $action = '';
                $dto = new ChangePasswordDto();
                $form = $this->createForm(ChangePasswordType::class, $dto);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid() &&
                    $this->isCsrfTokenValid(ChangePasswordType::CSRF_NAME, $request->request->get("_csrf"))) {
                    $password = $this->passwordEncoder->encodePassword($userEntity, $dto->getPassword());

                    $userEntity->setTokenExpiry(null)
                               ->setTokenKey(null)
                               ->setPassword($password);
                    $this->logger->notice(sprintf('KONFIRMASI reset password dengan token: %s berhasil.', $tokenKey),
                                          UserAccount::class, $userEntity, [JsonController::CONTEXT_NON_REL]);

                    $action = 'success';
                    $this->addFlash('success', 'ForgotPassword.ChangePasswordSuccess');
                } elseif ($form->isSubmitted() && !$this->isCsrfTokenValid(ChangePasswordType::CSRF_NAME,
                                                                           $request->request->get("_csrf"))) {
                    $action = "error";
                    $this->addFlash('danger', 'ForgotPassword.FormInvalid');
                }

                return $this->render("user/forgot_password/change_password.html.twig", [
                    "token"  => $token,
                    "action" => $action,
                    "form"   => $form->createView()
                ]);
            } else {
                $this->addFlash('notice', 'ForgotPassword.LinkExpired');
                return $this->redirectToRoute('app_user_forgotpassword_linkexpired');
            }
        }

        throw $this->createNotFoundException();
    }

}