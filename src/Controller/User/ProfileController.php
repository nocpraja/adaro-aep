<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Controller\User;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Entity\Security\UserAccount;
use App\Event\User\RequestChangeEmailEvent;
use App\Form\User\UserProfileType;
use App\Repository\Security\UserAccountRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AuthenticationController
 *
 * @Route("/app/user")
 *
 * @package App\Controller\User
 * @author  Mark Melvin
 * @since   03/02/2019, modified: 25/06/2019 3:53
 */
class ProfileController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var UserAccountRepository
     *
     */
    private $repository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * ProfileController constructor.
     *
     * @param UserAccountRepository        $userAccountRepository Entity repository instance
     * @param UserPasswordEncoderInterface $passwordEncoder       Password encryption service
     * @param AuditLogger                  $auditLogger           AuditLogger service
     */
    public function __construct(UserAccountRepository $userAccountRepository,
                                UserPasswordEncoderInterface $passwordEncoder,
                                AuditLogger $auditLogger)
    {
        $this->repository = $userAccountRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->logger = $auditLogger;
    }

    /**
     * Display current user profile.
     *
     * @Route("/profile", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function display(): JsonResponse
    {
        $userObj = $this->getUser();

        if ($userObj instanceof AppUserInterface) {
            return $this->jsonResponse($userObj, -1, [self::CONTEXT_WITH_REL, 'with_profile']);
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * Update current user profile.
     *
     * @Route("/profile/update", methods={"POST"})
     *
     * @param Request                  $request    HTTP Request
     * @param EventDispatcherInterface $dispatcher Symfony event dispatcher service
     *
     * @return JsonResponse
     */
    public function update(Request $request, EventDispatcherInterface $dispatcher): JsonResponse
    {
        $userObj = $this->getUser();

        if ($userObj instanceof AppUserInterface) {
            try {
                $entity = $this->repository->findByUsername($userObj->getUsername());
                $form = $this->createForm(UserProfileType::class, $entity);
                $form->submit($request->request->all());

                if ($form->isValid()) {
                    if (!empty($entity->getUserPassword()) &&
                        !$this->passwordEncoder->isPasswordValid($entity, $entity->getUserPassword())) {
                        return $this->jsonError('Password lama tidak cocok.',
                                                JsonResponse::HTTP_BAD_REQUEST);
                    }
                    if (!empty($entity->getNewPassword())) {
                        $password = $this->passwordEncoder->encodePassword($entity, $entity->getNewPassword());
                        $entity->setPassword($password);
                    }
                    if ($entity->getNewEmail() != $entity->getEmail()) {
                        $check = $this->repository->findOneBy(['email' => $entity->getNewEmail()]);

                        if (is_null($check) || $check->getUsername() == $userObj->getUsername()) {
                            $event = new RequestChangeEmailEvent($entity);
                        } else {
                            return $this->jsonError('Alamat email telah terdaftar pada akun pengguna lain.',
                                                    JsonResponse::HTTP_BAD_REQUEST);
                        }
                    } else {
                        $entity->setNewEmail(null);
                    }

                    $this->logger->getEntityManager()->persist($entity);
                    $this->logger->info('UPDATE profil akun pengguna.',
                                        UserAccount::class, $entity, [self::CONTEXT_NON_REL]);

                    if (isset($event)) {
                        $eventName = empty($entity->getEmail()) ? RequestChangeEmailEvent::CONFIRM : RequestChangeEmailEvent::REQUEST;
                        $dispatcher->dispatch($eventName, $event);
                    }

                    return $this->jsonResponse($entity, -1, [self::CONTEXT_WITH_REL, 'with_profile']);
                } else {
                    return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
                }
            } catch (NonUniqueResultException $e) {
                $this->logger->error('DUPLICATE akun pengguna saat update profil akun pengguna.',
                                     $userObj, [self::CONTEXT_NON_REL]);
            }
        }

        throw $this->createAccessDeniedException();
    }

}
