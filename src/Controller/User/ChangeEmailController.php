<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Controller\User;


use App\Component\Controller\JsonController;
use App\Component\Util\TokenGenerator;
use App\Entity\Security\UserAccount;
use App\Event\User\RequestChangeEmailEvent;
use App\Repository\Security\UserAccountRepository;
use App\Service\Logger\AuditLogger;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;

/**
 * Class ChangeEmailController
 *
 * @package App\Controller\User
 * @author  Ahmad Fajar
 * @since   2019-04-17, modified: 2019-04-18 14:13
 *
 * @Route("/change-email")
 */
class ChangeEmailController extends AbstractController
{

    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * @var UserAccountRepository
     */
    private $repository;


    /**
     * ChangeEmailController constructor.
     *
     * @param AuditLogger           $logger     Audit logger service
     * @param UserAccountRepository $repository Entity  repository instance
     */
    public function __construct(AuditLogger $logger, UserAccountRepository $repository)
    {
        $this->logger = $logger;
        $this->repository = $repository;
    }

    /**
     * Menampilkan halaman konfirmasi penggantian alamat email, dan kemudian mengirimkan
     * email konfirmasi kedua ke alamat email yang baru.
     *
     * @Route("/confirm/{tokenKey}/{tokenValue}", methods={"GET"})
     *
     * @param EventDispatcherInterface $dispatcher Symfony event dispatcher service
     * @param string                   $tokenKey   Request Token key
     * @param string                   $tokenValue Request Token value
     *
     * @return Response
     * @throws Exception
     */
    public function confirm(EventDispatcherInterface $dispatcher, string $tokenKey, string $tokenValue): Response
    {
        $generator = new TokenGenerator();
        $token = new CsrfToken($tokenKey, $tokenValue);
        $userEntity = $this->repository->findOneBy(['tokenKey' => $tokenKey]);

        if (!is_null($userEntity) && $generator->isTokenValid(
                ($userEntity->getUsername() . RequestChangeEmailEvent::REQUEST), $token)) {
            $now = new DateTime();

            if ($userEntity->getTokenExpiry() > $now) {
                $this->logger->notice(
                    sprintf('KONFIRMASI request perubahan alamat email untuk token: %s berhasil.', $tokenKey),
                    UserAccount::class, $userEntity, [JsonController::CONTEXT_NON_REL]);

                $event = new RequestChangeEmailEvent($userEntity);
                $dispatcher->dispatch(RequestChangeEmailEvent::CONFIRM, $event);

                return $this->render('user/change_email/confirm.html.twig');
            } else {
                $this->addFlash('notice', 'ChangeEmail.LinkExpired');
                return $this->redirectToRoute('app_user_changeemail_linkexpired');
            }
        }

        throw $this->createNotFoundException();
    }

    /**
     * @Route("/verified/{tokenKey}/{tokenValue}", methods={"GET"})
     *
     * @param string $tokenKey   Request Token key
     * @param string $tokenValue Request Token value
     *
     * @return Response
     * @throws Exception
     */
    public function verified(string $tokenKey, string $tokenValue): Response
    {
        $generator = new TokenGenerator();
        $token = new CsrfToken($tokenKey, $tokenValue);
        $userEntity = $this->repository->findOneBy(['tokenKey' => $tokenKey]);

        if (!is_null($userEntity) && $generator->isTokenValid(
                ($userEntity->getUsername() . RequestChangeEmailEvent::CONFIRM), $token)) {
            $now = new DateTime();

            if ($userEntity->getTokenExpiry() > $now) {
                $newEmail = $userEntity->getNewEmail();
                $userEntity->setTokenExpiry(null)
                           ->setTokenKey(null)
                           ->setNewEmail(null)
                           ->setEmail($newEmail);

                $this->logger->notice(
                    sprintf('VERIFIKASI perubahan alamat email untuk token: %s berhasil.', $tokenKey),
                    UserAccount::class, $userEntity, [JsonController::CONTEXT_NON_REL]);

                return $this->render('user/change_email/verified.html.twig');
            } else {
                $this->addFlash('notice', 'ChangeEmail.LinkExpired');
                return $this->redirectToRoute('app_user_changeemail_linkexpired');
            }

        }

        throw $this->createNotFoundException();
    }

    /**
     * Menampilkan halaman token Konfirmasi Change Email telah expired.
     *
     * @Route("/link-expired")
     *
     * @return Response
     */
    public function linkExpired(): Response
    {
        return $this->render('user/change_email/link_expired.html.twig');
    }

}
