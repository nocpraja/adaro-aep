<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan;

use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Kegiatan\PembobotanProgram;
use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\MasterData\ProgramCSR;
use App\Models\Kegiatan\PembobotanProgramModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PembobotanProgramController
 *
 * @package App\Controller\Kegiatan
 * @author  Mark Melvin
 * @since   18/2/2020
 *
 * @Route("/app/kegiatan/pembobotan-program")
 */
class PembobotanProgramController extends JsonController
{
    /**
     * @var PembobotanProgramModel $model
     */
    private $model;

    /**
     * PembobotanProgramController constructor.
     * @param PembobotanProgramModel $model
     */
    public function __construct(PembobotanProgramModel $model)
    {
        $this->model = $model;
    }


    /**
     * Menampilkan daftar pembobotan program.
     *
     * @Route("/{id}", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param JenisProgramCSR $bidang Persistent entity object
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(JenisProgramCSR $bidang, Request $request): JsonResponse
    {
        $result = $this->model->getAllPembobotanProgram($bidang);
        return $this->jsonResponse($result, count($result), $this->context_withrels);

    }

    /**
     * Menampilkan detail record Provinsi.
     *
     * @Route("/save/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param ProgramCSR $program Persistent entity object
     *
     * @return JsonResponse
     */
    public function savePembobotan(ProgramCSR $program, Request $request) {
        $bobot = (float)$request->get('bobot');
        $result = $this->model->savePembobotanProgram($program, $bobot);
        if ($result) {
            return $this->jsonSuccess('Berhasil update bobot');
        }
        else {
            return $this->jsonError('Gagal update bobot');
        }
    }



}