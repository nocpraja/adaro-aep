<?php


namespace App\Controller\Kegiatan\Finalisasi;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Finalisasi\Finalisasi;
use App\Entity\Security\UserAccount;
use App\Form\Kegiatan\FinalisasiType;
use App\Models\Finalisasi\FinalisasiModel;
use App\Repository\Kegiatan\KegiatanRepository;
use App\Repository\MasterData\MitraRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Class FinalisasiController
 *
 * @package App\Controller\Kegiatan\Finalisasi
 * @author  Mark Melvin
 * @since   9/27/2018 4:09 PM, modified: 03/02/2019 9:58
 *
 * @Route("/app/kegiatan/finalisasi")
 */
class FinalisasiController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /** @var FinalisasiModel */
    private $model;

    /** @var KegiatanRepository */
    private $kegiatan_repo;

    /** @var MitraRepository */
    private $mitra_repo;

    /**
     * FinalisasiController constructor.
     * @param FinalisasiModel $model
     * @param KegiatanRepository $kegiatan_repo
     * @param MitraRepository $mitra_repo
     */
    public function __construct(FinalisasiModel $model, KegiatanRepository $kegiatan_repo, MitraRepository $mitra_repo)
    {
        $this->model = $model;
        $this->kegiatan_repo = $kegiatan_repo;
        $this->mitra_repo = $mitra_repo;
    }


    /**
     * Menampilkan daftar finalisasi
     * @Route("/", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function fetchByCriteria(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAllByCriteria(DataQuery::parseRequest($request), null);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
    * Menampilkan single data finalisasi.
    * @Route("/{id}", requirements={"id":"\d+"}, methods={"GET"})
    *
    * @param Finalisasi|null $entity Persistent entity object of Finalisasi
    *
    * @return JsonResponse
    */
    public function fetch(?Finalisasi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data finalisasi tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_withrels);
    }

    /**
     * Proses creata data finalisasi kegiatan.
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        /** @var UserAccount $user */
        $user = $this->getUser();
        $entity = new Finalisasi();

        if ($user->getMitraCompany() === null){
            return $this->jsonError('Finalisasi kegiatan hanya bisa di create oleh Mitra',
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        $form = $this->createForm(FinalisasiType::class, $entity,
            [
                'kegiatan_repo' => $this->kegiatan_repo,
                'mitra_repo' => $this->mitra_repo
            ]);
        $form->submit($request->request->all());
        $mitra = $user->getMitraCompany();
        $entity->setMitra($mitra);
        $entity->setCreatedDate(new \DateTime());
        $entity->setCreatedBy($user);

        try {

            $this->model->update($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (\Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Menyimpan pembaruan data finalisasi kegiatan.
     * @Route("/update/{id}", requirements={"id":"\d+"}, methods={"POST"})
     *
     * @param Request          $request HTTP Request
     * @param Finalisasi|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?Finalisasi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data finalisasi kegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(FinalisasiType::class, $entity,
            [
                'kegiatan_repo' => $this->kegiatan_repo,
                'mitra_repo' => $this->mitra_repo
            ]);
        $form->submit($request->request->all());

        try {
            $this->model->update($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (\Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Proses workflow transition finalisasi kegiatan.
     * @Route("/stepflow/{id}", requirements={"id":"\d+"}, methods={"PATCH"})
     *
     * @param Request     $request HTTP Request
     * @param Finalisasi $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, Finalisasi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data finalisasi tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (\LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (\Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


}