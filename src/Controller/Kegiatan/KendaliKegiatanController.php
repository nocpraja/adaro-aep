<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\KendaliKegiatan;
use App\Entity\MasterData\ProgramCSR;
use App\Models\Kegiatan\KendaliKegiatanModel;
use App\Repository\Kegiatan\KegiatanRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KendaliKegiatanController
 *
 * @package App\Controller\Kegiatan
 * @author  Mark Melvin
 * @since   17/7/2019
 *
 * @Route("/app/kegiatan/kendalikegiatan")
 */
class KendaliKegiatanController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var KendaliKegiatanModel
     */
    private $model;

    /**
     * @var KegiatanRepository
     */
    private $kegiatanRepository;

    /**
     * KendaliKegiatanController constructor.
     *
     * @param KendaliKegiatanModel $model Domain model Kegiatan
     * @param KegiatanRepository $kegiatanRepository Repository untuk Kegiatan
     */
    public function __construct(KendaliKegiatanModel $model,
                                KegiatanRepository $kegiatanRepository)
    {
        $this->model = $model;
        $this->kegiatanRepository = $kegiatanRepository;
    }

    /**
     * Menampilkan daftar kendali kegiatan.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     * @throws
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAllKegiatan(DataQuery::parseRequest($request), $this->getUser());

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menampilkan daftar kendali kegiatan.
     *
     * @Route("/by-program/{msid}/{tahun}", methods={"GET"}, requirements={"msid":"\d+","tahun":"\d+"})
     *
     * @param ProgramCSR $program
     * @param int $tahun
     * @return JsonResponse
     */
    public function listByProgram(ProgramCSR $program, int $tahun) {
        $listing = $this->model->listByProgramYear($program, $tahun);
        return $this->jsonResponse($listing, count($listing), ['groups' => ['without_rel' ,'with_projectcontrol']]);
    }

    /**
     * Menampilkan detail record KendaliKegiatan.
     *
     * @Route("/{id}", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param KendaliKegiatan $entity Persistent entity object of KendaliKegiatan
     *
     * @return JsonResponse
     */
    public function fetch(?KendaliKegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_parentrels);
    }


    /**
     * Menyimpan rencana Kendali Proyek
     *
     * @Route("/save-rencana/{kegiatanId}",methods={"POST"}, requirements={"kegiatanId":"\d+"})
     *
     * @param Kegiatan $kegiatan
     * @param Request $request Http request
     *
     * @return JsonResponse
     */
    public function saveRencana(Kegiatan $kegiatan, Request $request): JsonResponse
    {
        $rencana = $request->get('kendaliKegiatan');
        if (isset($kegiatan)) {
            $result[] = $this->model->saveRencana($kegiatan, $rencana);
        }

        // sebagai result, kembalikan daftar target yang sudah diupdate
        return $this->jsonResponse($result, count($result), $this->context_norels);
    }


    /**
     * Menyimpan rencana Kendali Proyek
     *
     * @Route("/save-realisasi/{kegiatanId}",methods={"POST"}, requirements={"kegiatanId":"\d+"})
     *
     * @param Kegiatan $kegiatan
     * @param Request $request Http request
     *
     * @return JsonResponse
     */
    public function saveRealisasi(Kegiatan $kegiatan, Request $request): JsonResponse
    {
        $rencana = $request->get('kendaliKegiatan');
        if (isset($kegiatan)) {
            $result[] = $this->model->saveRealisasi($kegiatan, $rencana);
        }

        // sebagai result, kembalikan daftar target yang sudah diupdate
        return $this->jsonResponse($result, count($result), $this->context_norels);
    }

}
