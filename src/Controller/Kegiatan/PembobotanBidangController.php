<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan;

use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Kegiatan\PembobotanBidang;
use App\Entity\MasterData\JenisProgramCSR;
use App\Models\Kegiatan\PembobotanBidangModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PembobotanBidangController
 *
 * @package App\Controller\Kegiatan
 * @author  Mark Melvin
 * @since   18/2/2020
 *
 * @Route("/app/kegiatan/pembobotan-bidang")
 */
class PembobotanBidangController extends JsonController
{
    /**
     * @var PembobotanBidangModel $model
     */
    private $model;

    /**
     * PembobotanBidangController constructor.
     * @param PembobotanBidangModel $model
     */
    public function __construct(PembobotanBidangModel $model)
    {
        $this->model = $model;
    }


    /**
     * Menampilkan daftar pembobotan bidang.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $result = $this->model->getAllPembobotanBidang();
        return $this->jsonResponse($result, count($result), $this->context_norels);

    }

    /**
     * Menampilkan detail record Provinsi.
     *
     * @Route("/save/{id}", methods={"POST"}, requirements={"id":"\d+"})
     *
     * @param JenisProgramCSR $bidang Persistent entity object
     *
     * @return JsonResponse
     */
    public function savePembobotan(JenisProgramCSR $bidang, Request $request) {
        $bobot = (float)$request->get('bobot');
        $result = $this->model->savePembobotanBidang($bidang, $bobot);
        if ($result) {
            return $this->jsonSuccess('Berhasil update bobot');
        }
        else {
            return $this->jsonError('Gagal update bobot');
        }
    }



}