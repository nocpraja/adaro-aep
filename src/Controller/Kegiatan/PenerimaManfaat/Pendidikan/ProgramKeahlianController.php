<?php
/*
* Web aplikasi Adaro Education Program built with Symfony4.
*
* Copyright (C) 2018 - Ahmad Fajar
*/

namespace App\Controller\Kegiatan\PenerimaManfaat\Pendidikan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Beneficiary\JurusanInstitusi;
use App\Entity\Beneficiary\UnitProduksi;
use App\Form\Beneficiary\JurusanInstitusiType;
use App\Models\Beneficiary\JurusanInstitusiModel;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * ProgramKeahlianController
 *
 * @package App\Controller\Kegiatan\PenerimaManfaat\Pendidikan
 * @author  Mark Melvin
 * @since   21/02/2019, modified: 27/03/2019 1:46
 *
 * @Route("/app/kegiatan/penerima-manfaat/pendidikan/program-keahlian")
 */
class ProgramKeahlianController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var JurusanInstitusiModel
     */
    private $model;


    /**
     * ProgramKeahlianController constructor.
     *
     * @param JurusanInstitusiModel $model Domain model untuk memproses Program Keahlian/Studi
     */
    public function __construct(JurusanInstitusiModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan daftar Program Keahlian/Studi dengan kriteria tertentu
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request));

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menambahkan record Program Keahlian/Studi baru untuk SMK ataupun Poltek ke dalam database.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new JurusanInstitusi();
        $entity->setUnitProduksi(new UnitProduksi());
        $form = $this->createForm(JurusanInstitusiType::class, $entity, [
            'institusi_repo' => $this->model->getInstitusiRepository(),
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->model->insert($entity);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail record Program Keahlian/Studi dari suatu SMK ataupun Poltek.
     *
     * @Route("/{jurusanId}", methods={"GET"}, requirements={"jurusanId":"\d+"})
     *
     * @param JurusanInstitusi $entity Persistent entity object of JurusanInstitusi
     *
     * @return JsonResponse
     */
    public function fetch(?JurusanInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_customrels);
    }

    /**
     * Memperbarui record Program Keahlian/Studi SMK ataupun Poltek.
     *
     * @Route("/update/{jurusanId}", methods={"POST"}, requirements={"jurusanId":"\d+"})
     *
     * @param Request          $request HTTP Request
     * @param JurusanInstitusi $entity  Persistent entity object of JurusanInstitusi
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?JurusanInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(JurusanInstitusiType::class, $entity,
                                  ['institusi_repo' => $this->model->getInstitusiRepository()]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->model->update($entity);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record Program Keahlian/Studi SMK ataupun Poltek dari database.
     *
     * @Route("/delete/{jurusanId}", methods={"DELETE"}, requirements={"jurusanId":"\d+"})
     *
     * @param JurusanInstitusi $entity Persistent entity object of JurusanInstitusi
     *
     * @return JsonResponse
     */
    public function delete(?JurusanInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $jenis = $entity->getJenis() == 2 ? 'Program Keahlian' : 'Program Studi';
        try {
            $this->model->delete($entity);
            return $this->jsonSuccess(sprintf('Record %s telah berhasil dihapus dari database.', $jenis));
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError(sprintf('Gagal menghapus record %s, masih ada data yang terkait dengannya.',
                                            $jenis), JsonResponse::HTTP_FORBIDDEN);
        }
    }

}
