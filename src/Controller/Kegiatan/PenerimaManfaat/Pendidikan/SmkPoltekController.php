<?php
/*
* Web aplikasi Adaro Education Program built with Symfony4.
*
* Copyright (C) 2018 - Ahmad Fajar
*/

namespace App\Controller\Kegiatan\PenerimaManfaat\Pendidikan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Beneficiary\BeneficiaryInstitusi;
use App\Entity\Pendanaan\DanaBatch;
use App\Form\Beneficiary\BeneficiaryInstitusiType;
use App\Form\Beneficiary\BeneficiaryProgramType;
use App\Models\Beneficiary\BeneficiaryInstitusiModel;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SmkPoltekController
 * @package App\Controller\Kegiatan\PenerimaManfaat\Pendidikan
 * @author  Hendra
 * @since   31/10/2018, modified: 13/08/2020 04:59
 * @Route("/app/kegiatan/penerima-manfaat/pendidikan/smk-poltek")
 */
class SmkPoltekController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var BeneficiaryInstitusiModel
     */
    private $model;


    /**
     * PaudController Constructor.
     *
     * @param BeneficiaryInstitusiModel $model
     */
    public function __construct(BeneficiaryInstitusiModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan daftar penerima manfaat SMK & Poltek dengan kriteria tertentu
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAllSmkAndPoltek(DataQuery::parseRequest($request), false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menampilkan daftar penerima manfaat SMK & Poltek dengan kriteria tertentu
     *
     * @Route("/dpt", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listForProgram(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAllSmkAndPoltek(DataQuery::parseRequest($request), true);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_approvalrels);
    }

    /**
     * Menambahkan record SMK/POLTEK ke dalam program (DPT).
     *
     * @Route("/addto-program", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function addToProgram(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key'    => 'institusiId',
            'entry_repo'   => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryInstitusi $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            $this->model->addToProgram($entity, $program);
            $lembaga = $entity->getKategori() === 2 ? 'SMK' : 'POLTEK';
            $this->model->getLogger()->info(sprintf('TAMBAH record %s ke dalam program Dana Batch.', $lembaga),
                                            BeneficiaryInstitusi::class, $entity,
                                            [JsonController::CONTEXT_NON_REL]);

            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (Exception $ex) {
            return $this->jsonError('Gagal menambahkan data SMK/POLTEK.', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Menambahkan SMK & Poltek baru ke dalam database dengan disertakan ke dalam program.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryInstitusiType::class, new BeneficiaryInstitusi(), [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo'   => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->model->save($entity);

        $lembaga = $entity->getKategori() === 2 ? 'SMK' : 'POLTEK';
        $this->model->getLogger()->info(sprintf('TAMBAH record %s baru.', $lembaga),
                                        BeneficiaryInstitusi::class, $entity,
                                        [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail record penerima manfaat Institusi (SMK/POLTEK)
     *
     * @Route("/{institusiId}", methods={"GET"}, requirements={"institusiId":"\d+"})
     *
     * @param BeneficiaryInstitusi $entity Persistent entity object of BeneficiaryInstitusi
     *
     * @return JsonResponse
     */
    public function fetch(?BeneficiaryInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_customrels);
    }

    /**
     * Memperbarui data SMK & Poltek yang sudah ada
     *
     * @Route("/update/{institusiId}", methods={"POST"}, requirements={"institusiId":"\d+"})
     *
     * @param Request              $request HTTP Request
     * @param BeneficiaryInstitusi $entity  Persistent entity object of BeneficiaryInstitusi
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?BeneficiaryInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(BeneficiaryInstitusiType::class, $entity, [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo'   => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->model->save($entity);

        $lembaga = $entity->getKategori() === 2 ? 'SMK' : 'POLTEK';
        $this->model->getLogger()->info(sprintf('UPDATE record %s.', $lembaga),
                                        BeneficiaryInstitusi::class, $entity, [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record penerima manfaat Institusi (SMK/POLTEK).
     *
     * @Route("/delete", methods={"DELETE"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key'    => 'institusiId',
            'entry_repo'   => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryInstitusi $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];
        /** @var BeneficiaryInstitusi kategori institusi $lembaga */
        $lembaga = $entity->getKategori() === 2 ? 'SMK' : 'POLTEK';

        try {
            if ($entity->getPrograms()->count() > 1) {
                $this->model->removeFromProgram($entity, $program);
            } else {
                $this->model->delete($entity);
            }

            return $this->jsonSuccess('Data ' . $lembaga . ' telah berhasil dihapus.');
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError('Gagal menghapus data ' . $lembaga . ', masih ada record yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }
    }

    /**
     * Menampilkan histori approval.
     *
     * @Route("/approval/{institusiId}", requirements={"institusiId":"\d+"}, methods={"GET"})
     *
     * @param BeneficiaryInstitusi|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function logApproval(?BeneficiaryInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data beneficiary tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $results = $this->model->fetchLogApproval($entity);

        return $this->jsonResponse($results, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]);
    }

    /**
     * Proses workflow transition.
     * @Route("/stepflow/{institusiId}", requirements={"institusiId":"\d+"}, methods={"PATCH"})
     *
     * @param Request              $request HTTP Request
     * @param BeneficiaryInstitusi $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, BeneficiaryInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
