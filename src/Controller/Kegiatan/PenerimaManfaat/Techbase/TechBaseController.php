<?php
/*
* Web aplikasi Adaro Education Program built with Symfony4.
*
* Copyright (C) 2018 - Ahmad Fajar
*/

namespace App\Controller\Kegiatan\PenerimaManfaat\Techbase;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Entity\Beneficiary\BeneficiaryTbe;
use App\Entity\Pendanaan\DanaBatch;
use App\Form\Beneficiary\BeneficiaryProgramType;
use App\Form\Beneficiary\BeneficiaryTbeType;
use App\Models\Beneficiary\BeneficiaryTbeModel;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TechBaseController
 * @package App\Controller\Kegiatan\PenerimaManfaat
 * @author  Tri N
 * @since   29/11/2018, modified: 13/08/2020 04:55
 * @Route("/app/kegiatan/penerima-manfaat/tech-base")
 */
class TechBaseController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var BeneficiaryTbeModel
     */
    private $model;


    /**
     * TechBaseController constructor.
     *
     * @param BeneficiaryTbeModel $model
     */
    public function __construct(BeneficiaryTbeModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan daftar peserta Tech-Based Education (All Guru) diluar program dengan kriteria tertentu.
     * @Route("", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request), false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menampilkan daftar peserta Tech-Based Education (All Guru) didalam program dengan kriteria tertentu.
     * @Route("/dpt", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listForProgram(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request), true);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), [
            self::CONTEXT_WITH_REL,
            self::CONTEXT_WITH_POSTDATE,
            self::CONTEXT_WITH_APPROVAL
        ]);
    }

    /**
     * Menambahkan record peserta Tech-Based Education ke dalam program (DPT).
     * @Route("/addto-program", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function addToProgram(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key' => 'tbeId',
            'entry_repo' => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryTbe $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            $this->model->addToProgram($entity, $program);
            $this->model->getLogger()->info('TAMBAH record peserta TechBase ke dalam program Dana Batch.',
                BeneficiaryTbe::class, $entity,
                [JsonController::CONTEXT_NON_REL]);

            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (Exception $ex) {
            return $this->jsonError('Gagal menambahkan data peserta TechBase.', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Menambahkan record peserta Tech-Based Education baru ke dalam database dengan disertakan ke dalam program.
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return  JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $biodata = new BeneficiaryIndividu();
        $techBase = new BeneficiaryTbe();
        $techBase->setBiodata($biodata);

        $form = $this->createForm(BeneficiaryTbeType::class, $techBase, [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo' => $this->model->getProgramRepository(),
            'institusi_repo' => $this->model->getInstitusiRepository()
        ]);
        $form->submit($request->request->all());

//        if (false === $form->isValid()) {
//            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
//        }

        $this->model->insert($techBase);

        return $this->jsonResponse($techBase, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail record peserta Tech-Based Education.
     * @Route("/{tbeId}", methods={"GET"}, requirements={"tbeId":"\d+"})
     *
     * @param BeneficiaryTbe|null $entity Persistent entity object of BeneficiaryTbe
     *
     * @return JsonResponse
     */
    public function fetch(?BeneficiaryTbe $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_customrels);
    }

    /**
     * Perbarui record peserta Tech-Based Education.
     * @Route("/update/{tbeId}", methods={"POST"}, requirements={"tbeId":"\d+"})
     *
     * @param Request             $request HTTP Request
     * @param BeneficiaryTbe|null $entity  Persistent entity object of BeneficiaryTbe
     *
     * @return  JsonResponse
     */
    public function update(Request $request, ?BeneficiaryTbe $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data TBE tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(BeneficiaryTbeType::class, $entity, [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo'   => $this->model->getProgramRepository(),
            'institusi_repo' => $this->model->getInstitusiRepository()
        ]);
        $form->submit($request->request->all());

        $this->model->update($entity);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Hapus record peserta Tech-Based Education dari DPT Terkait
     * @Route("/delete", methods={"DELETE"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key' => 'tbeId',
            'entry_repo' => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryTbe $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            if ($entity->getBiodata()->getPrograms()->count() > 1) {
                $this->model->remove($entity, $program);
            } else {
                $this->model->delete($entity);
            }

            return $this->jsonSuccess('Data peserta TBE telah berhasil dihapus.');
        } catch (LogicException $th) {
            return $this->jsonError($th->getMessage(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (Exception $ex) {
            return $this->jsonError('Gagal menghapus data peserta TBE, masih ada record yang terkait dengannya.',
                JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Menampilkan histori approval.
     *
     * @Route("/approval/{tbeId}", requirements={"tbeId":"\d+"}, methods={"GET"})
     *
     * @param BeneficiaryTbe|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function logApproval(?BeneficiaryTbe $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data beneficiary tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $results = $this->model->fetchLogApproval($entity);

        return $this->jsonResponse($results, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]);
    }

    /**
     * Proses workflow transition.
     * @Route("/stepflow/{tbeId}", requirements={"tbeId":"\d+"}, methods={"PATCH"})
     *
     * @param Request        $request HTTP Request
     * @param BeneficiaryTbe $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, BeneficiaryTbe $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
