<?php
/*
* Web aplikasi Adaro Education Program built with Symfony4.
*
* Copyright (C) 2018 - Ahmad Fajar
*/

namespace App\Controller\Kegiatan\PenerimaManfaat\Beasiswa;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Beneficiary\BeneficiaryBeasiswa;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Entity\Beneficiary\OrangTua;
use App\Entity\Pendanaan\DanaBatch;
use App\Form\Beneficiary\BeneficiaryBeasiswaType;
use App\Form\Beneficiary\BeneficiaryProgramType;
use App\Models\Beneficiary\BeneficiaryBeasiswaModel;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BeasiswaController
 * @package App\Controller\Kegiatan\PenerimaManfaat\Beasiswa
 * @author  Tri N
 * @since   13/11/2018, modified: 13/08/2020 04:50
 * @Route("/app/kegiatan/penerima-manfaat/beasiswa")
 */
class BeasiswaController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var BeneficiaryBeasiswaModel
     */
    private $model;


    /**
     * BeasiswaController Constructor.
     *
     * @param BeneficiaryBeasiswaModel $model Domain model untuk proses Beasiswa
     */
    public function __construct(BeneficiaryBeasiswaModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan daftar penerima manfaat Beasiswa dengan kriteria tertentu.
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request), false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menampilkan daftar penerima manfaat Beasiswa yang ikut dalam program dengan kriteria tertentu
     * @Route("/dpt", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function listForProgram(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAll(DataQuery::parseRequest($request), true);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), [
            self::CONTEXT_WITH_REL,
            self::CONTEXT_WITH_POSTDATE,
            self::CONTEXT_WITH_APPROVAL
        ]);
    }

    /**
     * Menambahkan record peserta Beasiswa ke dalam program (DPT).
     * @Route("/addto-program", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function addToProgram(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key' => 'beasiswaId',
            'entry_repo' => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryBeasiswa $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            $this->model->addToProgram($entity, $program);
            $this->model->getLogger()->info('TAMBAH record peserta Beasiswa ke dalam program Dana Batch.',
                BeneficiaryBeasiswa::class, $entity,
                [JsonController::CONTEXT_NON_REL]);

            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (Exception $ex) {
            return $this->jsonError('Gagal menambahkan data peserta Beasiswa.', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Menambahkan record peserta Beasiswa baru ke dalam database dengan disertakan ke dalam program.
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return  JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $ortu = new OrangTua();
        $biodata = new BeneficiaryIndividu();
        $beasiswa = new BeneficiaryBeasiswa();
        $beasiswa->setBiodata($biodata)
            ->setOrangTua($ortu);

        $form = $this->createForm(BeneficiaryBeasiswaType::class, $beasiswa, [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo' => $this->model->getProgramRepository(),
            'reference_repo' => $this->model->getReferenceRepository(),
            'institusi_repo' => $this->model->getInstitusiRepository()
        ]);
        $form->submit($request->request->all());

//        if (false === $form->isValid()) {
//            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
//        }

        $this->model->insert($beasiswa);

        return $this->jsonResponse($beasiswa, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail record peserta Beasiswa.
     * @Route("/{beasiswaId}", methods={"GET"}, requirements={"beasiswaId":"\d+"})
     *
     * @param BeneficiaryBeasiswa|null $entity Persistent entity object of BeneficiaryBeasiswa
     *
     * @return JsonResponse
     */
    public function fetch(?BeneficiaryBeasiswa $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_customrels);
    }

    /**
     * Perbarui record peserta Beasiswa.
     * @Route("/update/{beasiswaId}", methods={"POST"}, requirements={"beasiswaId":"\d+"})
     *
     * @param Request                  $request HTTP Request
     * @param BeneficiaryBeasiswa|null $entity  Persistent entity object of BeneficiaryBeasiswa
     *
     * @return  JsonResponse
     */
    public function update(Request $request, ?BeneficiaryBeasiswa $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(BeneficiaryBeasiswaType::class, $entity, [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo'   => $this->model->getProgramRepository(),
            'reference_repo' => $this->model->getReferenceRepository(),
            'institusi_repo' => $this->model->getInstitusiRepository()
        ]);
        $form->submit($request->request->all());

        $this->model->update($entity);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Hapus record peserta Beasiswa.
     * @Route("/delete", methods={"DELETE"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key' => 'beasiswaId',
            'entry_repo' => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryBeasiswa $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            if ($entity->getBiodata()->getPrograms()->count() > 1) {
                $this->model->remove($entity, $program);
            } else {
                $this->model->delete($entity);
            }

            return $this->jsonSuccess('Data peserta Beasiswa telah berhasil dihapus.');
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError('Gagal menghapus record peserta Beasiswa, masih ada record yang terkait dengannya.',
                JsonResponse::HTTP_FORBIDDEN);
        }
    }

    /**
     * Menampilkan histori approval.
     *
     * @Route("/approval/{beasiswaId}", requirements={"beasiswaId":"\d+"}, methods={"GET"})
     *
     * @param BeneficiaryBeasiswa|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function logApproval(?BeneficiaryBeasiswa $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data beneficiary tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $results = $this->model->fetchLogApproval($entity);

        return $this->jsonResponse($results, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]);
    }

    /**
     * Proses workflow transition.
     * @Route("/stepflow/{beasiswaId}", requirements={"beasiswaId":"\d+"}, methods={"PATCH"})
     *
     * @param Request             $request HTTP Request
     * @param BeneficiaryBeasiswa $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, BeneficiaryBeasiswa $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
