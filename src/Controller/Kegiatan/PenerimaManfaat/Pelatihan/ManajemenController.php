<?php
/*
* Web aplikasi Adaro Education Program built with Symfony4.
*
* Copyright (C) 2018 - Ahmad Fajar
*/

namespace App\Controller\Kegiatan\PenerimaManfaat\Pelatihan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Entity\Pendanaan\DanaBatch;
use App\Form\Beneficiary\BeneficiaryIndividuType;
use App\Form\Beneficiary\BeneficiaryProgramType;
use App\Models\Beneficiary\BeneficiaryIndividuModel;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ManajemenController
 * @package App\Controller\Kegiatan\PenerimaManfaat
 * @author  Mark Melvin
 * @since   28/11/2018, modified: 13/08/2020 04:59
 * @Route("/app/kegiatan/penerima-manfaat/pelatihan/manajemen")
 */
class ManajemenController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var BeneficiaryIndividuModel
     */
    private $model;


    /**
     * PaudSiswaController constructor.
     *
     * @param BeneficiaryIndividuModel $model
     */
    public function __construct(BeneficiaryIndividuModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan daftar penerima manfaat Individu (Manajemen) dengan kriteria tertentu.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $paginator = $this->model->displayAllManajemen($dq, false, 'PESANTREN-YAYASAN');

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menampilkan daftar penerima manfaat Individu (Manajemen) dengan kriteria tertentu.
     *
     * @Route("/dpt", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listForProgram(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $paginator = $this->model->displayAllManajemen($dq, true, 'PESANTREN-YAYASAN');

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), [
            self::CONTEXT_WITH_REL,
            self::CONTEXT_WITH_POSTDATE,
            self::CONTEXT_WITH_APPROVAL
        ]);
    }

    /**
     * Menambahkan record Manajemen ke dalam program (DPT).
     *
     * @Route("/addto-program", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function addToProgram(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key'    => 'individuId',
            'entry_repo'   => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryIndividu $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            $this->model->addToProgram($entity, $program);
            $this->model->getLogger()->info('TAMBAH record Manajemen ke dalam program Dana Batch.',
                                            BeneficiaryIndividu::class, $entity,
                                            [JsonController::CONTEXT_NON_REL]);

            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (Exception $ex) {
            return $this->jsonError('Gagal menambahkan data Manajemen.', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Menambahkan Penerima Manfaat Individu (Manajemen) baru ke dalam database dengan disertakan ke dalam program.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return  JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryIndividuType::class, new BeneficiaryIndividu(), [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo'   => $this->model->getProgramRepository(),
            'institusi_repo' => $this->model->getInstitusiRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->model->save($entity);
        $this->model->getLogger()->info('TAMBAH record Manajemen baru.', BeneficiaryIndividu::class,
                                        $entity, [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail record penerima manfaat Individu (Manajemen).
     *
     * @Route("/{individuId}", methods={"GET"}, requirements={"individuId":"\d+"})
     *
     * @param BeneficiaryIndividu|null $entity Persistent entity object of BenediciaryIndividu
     *
     * @return JsonResponse
     */
    public function fetch(?BeneficiaryIndividu $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_customrels);
    }

    /**
     * memperbarui record penerima manfaat Individu (Manajemen).
     *
     * @Route("/update/{individuId}", methods={"POST"}, requirements={"individuId":"\d+"})
     *
     * @param Request                  $request HTTP Request
     * @param BeneficiaryIndividu|null $entity  Persistent entity object of BenediciaryIndividu
     *
     * @return  JsonResponse
     */
    public function update(Request $request, ?BeneficiaryIndividu $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(BeneficiaryIndividuType::class, $entity, [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo'   => $this->model->getProgramRepository(),
            'institusi_repo' => $this->model->getInstitusiRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->model->save($entity);
        $this->model->getLogger()->info('UPDATE record Manajemen.', BeneficiaryIndividu::class,
                                        $entity, [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record penerima manfaat individu (Manajemen).
     *
     * @Route("/delete", methods={"DELETE"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key'    => 'individuId',
            'entry_repo'   => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryIndividu $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            if ($entity->getPrograms()->count() > 1) {
                $this->model->removeFromProgram($entity, $program);
            } else {
                $this->model->delete($entity);
            }

            return $this->jsonSuccess('Data Manajemen telah berhasil dihapus.');
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError('Gagal menghapus data Manajemen, masih ada record yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }
    }

    /**
     * Menampilkan histori approval.
     *
     * @Route("/approval/{individuId}", requirements={"institusiId":"\d+"}, methods={"GET"})
     *
     * @param BeneficiaryIndividu|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function logApproval(?BeneficiaryIndividu $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data beneficiary tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $results = $this->model->fetchLogApproval($entity);

        return $this->jsonResponse($results, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]);
    }

    /**
     * Proses workflow transition.
     * @Route("/stepflow/{individuId}", requirements={"individuId":"\d+"}, methods={"PATCH"})
     *
     * @param Request             $request HTTP Request
     * @param BeneficiaryIndividu $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, BeneficiaryIndividu $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
