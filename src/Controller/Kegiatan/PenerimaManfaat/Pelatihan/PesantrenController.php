<?php
/*
* Web aplikasi Adaro Education Program built with Symfony4.
*
* Copyright (C) 2018 - Ahmad Fajar
*/

namespace App\Controller\Kegiatan\PenerimaManfaat\Pelatihan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Beneficiary\BeneficiaryInstitusi;
use App\Entity\Pendanaan\DanaBatch;
use App\Form\Beneficiary\BeneficiaryInstitusiType;
use App\Form\Beneficiary\BeneficiaryProgramType;
use App\Models\Beneficiary\BeneficiaryInstitusiModel;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PesantrenController
 * @package App\Controller\Kegiatan\PenerimaManfaat
 * @author  Mark Melvin
 * @since   28/11/2018, modified: 13/08/2020 05:00
 * @Route("/app/kegiatan/penerima-manfaat/pelatihan/institusi")
 */
class PesantrenController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var BeneficiaryInstitusiModel
     */
    private $model;


    /**
     * PaudController Constructor.
     *
     * @param BeneficiaryInstitusiModel $model
     */
    public function __construct(BeneficiaryInstitusiModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan daftar penerima manfaat Pesantren/Yayasan dengan kriteria tertentu.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAllPesantrenYayasan(DataQuery::parseRequest($request), false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_withrels);
    }

    /**
     * Menampilkan daftar penerima manfaat Pesantren/Yayasan dengan kriteria tertentu.
     *
     * @Route("/mapmarker/{idKegiatan}", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function mapMarker(Request $req, $idKegiatan): JsonResponse {
        //$data = $req->request->all();
        //exit(var_dump($idKegiatan));
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql =
        "SELECT 
            t2.nama_institusi, NULL AS nama_person, t2.nama_pimpinan, t2.office_phone AS phone, 
            NULL AS jabatan, 'Institusi' AS jenis_beneficiary, 
            t2.alamat, t2.kategori AS jenis_institusi, t2.latitude, t2.longitude
        FROM institusi_in_program t1
        JOIN beneficiary_institusi t2 ON t2.institusi_id = t1.institusi_id
        WHERE program_id = :idKegiatan
        UNION ALL
        SELECT 
            t3.nama_institusi, t2.nama_lengkap, NULL, t2.phone, t2.jabatan, 'Individu' AS jenis_benefit,
            t2.alamat, t2.kategori, t2.latitude, t2.longitude
        FROM individu_in_program t1
        JOIN beneficiary_individu t2 ON t2.individu_id = t1.individu_id
        LEFT JOIN beneficiary_institusi t3 ON t3.institusi_id = t2.institusi_id
        WHERE program_id = :idKegiatan2";

        $idKegiatan = $idKegiatan ? $idKegiatan : 0;
        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'idKegiatan' => $idKegiatan,
            'idKegiatan2' => $idKegiatan,
        ]);
        $result = $stmt->fetchAll();

        return $this->jsonResponse($result , -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_CHILD]);
    }

    /**
     * Menampilkan daftar penerima manfaat Pesantren/Yayasan dengan kriteria tertentu.
     *
     * @Route("/dpt", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function listForProgram(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAllPesantrenYayasan(DataQuery::parseRequest($request), true);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), [
            self::CONTEXT_WITH_REL,
            self::CONTEXT_WITH_POSTDATE,
            self::CONTEXT_WITH_APPROVAL
        ]);
    }

    /**
     * Menambahkan record Pesantren/Yayasan ke dalam program (DPT).
     *
     * @Route("/addto-program", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function addToProgram(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key'    => 'institusiId',
            'entry_repo'   => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryInstitusi $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            $this->model->addToProgram($entity, $program);
            $this->model->getLogger()->info('TAMBAH record Pesantren/Yayasan ke dalam program Dana Batch.',
                                            BeneficiaryInstitusi::class, $entity,
                                            [JsonController::CONTEXT_NON_REL]);

            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (Exception $ex) {
            return $this->jsonError('Gagal menambahkan data Pesantren/Yayasan.', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Menambahkan Pesantren/Yayasan baru ke dalam database dengan disertakan ke dalam program.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryInstitusiType::class, new BeneficiaryInstitusi(), [
            'kecamatan_repo'     => $this->model->getKecamatanRepository(),
            'kelurahan_repo'     => $this->model->getKelurahanRepository(),
            'program_repo'       => $this->model->getProgramRepository(),
            'allow_extra_fields' => true,
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->model->save($entity);
        $this->model->getLogger()->info('TAMBAH record Pesantren/Yayasan baru.', BeneficiaryInstitusi::class, $entity,
                                        [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail record penerima manfaat Institusi (Pesantren/Yayasan).
     *
     * @Route("/{institusiId}", methods={"GET"}, requirements={"institusiId":"\d+"})
     *
     * @param BeneficiaryInstitusi $entity Persistent entity object of BeneficiaryInstitusi
     *
     * @return JsonResponse
     */
    public function fetch(?BeneficiaryInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_customrels);
    }

    /**
     * Memperbarui data Pesantren/Yayasan yang sudah ada
     *
     * @Route("/update/{institusiId}", methods={"POST"}, requirements={"institusiId":"\d+"})
     *
     * @param Request              $request HTTP Request
     * @param BeneficiaryInstitusi $entity  Persistent entity object of BeneficiaryInstitusi
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?BeneficiaryInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(BeneficiaryInstitusiType::class, $entity, [
            'kecamatan_repo' => $this->model->getKecamatanRepository(),
            'kelurahan_repo' => $this->model->getKelurahanRepository(),
            'program_repo'   => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->model->save($entity);
        $this->model->getLogger()->info('UPDATE record Pesantren/Yayasan.', BeneficiaryInstitusi::class, $entity,
                                        [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record penerima manfaat Institusi (Pesantren/Yayasan).
     *
     * @Route("/delete", methods={"DELETE"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $form = $this->createForm(BeneficiaryProgramType::class, null, [
            'entry_key'    => 'institusiId',
            'entry_repo'   => $this->model->getRepository(),
            'program_repo' => $this->model->getProgramRepository()
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $data = $form->getNormData();
        /** @var BeneficiaryInstitusi $entity */
        $entity = $data['beneficiary'];
        /** @var DanaBatch $program */
        $program = $data['program'];

        try {
            if ($entity->getPrograms()->count() > 1) {
                $this->model->removeFromProgram($entity, $program);
            } else {
                $this->model->delete($entity);
            }

            return $this->jsonSuccess('Data Pesantren/Lembaga Keagamaan telah berhasil dihapus.');
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError('Gagal menghapus data Pesantren/Yayasan, masih ada record yang terkait dengannya.',
                                    JsonResponse::HTTP_FORBIDDEN);
        }
    }

    /**
     * Menampilkan histori approval.
     *
     * @Route("/approval/{institusiId}", requirements={"institusiId":"\d+"}, methods={"GET"})
     *
     * @param BeneficiaryInstitusi|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function logApproval(?BeneficiaryInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data beneficiary tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $results = $this->model->fetchLogApproval($entity);

        return $this->jsonResponse($results, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]);
    }

    /**
     * Proses workflow transition.
     * @Route("/stepflow/{institusiId}", requirements={"institusiId":"\d+"}, methods={"PATCH"})
     *
     * @param Request              $request HTTP Request
     * @param BeneficiaryInstitusi $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, BeneficiaryInstitusi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
