<?php
/**
 * Description: IndikatorKpiController.php generated by PhpStorm.
 * @project     adaro-aep
 * @package     App\Controller\Kegiatan
 * @author      alex
 * @created     2020-03-21, modified: 2020-03-21 20:01
 * @copyright   Copyright (c) 2020
 */

namespace App\Controller\Kegiatan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Entity\Kegiatan\IndikatorKpi;
use App\Form\Kegiatan\IndikatorKpiType;
use App\Models\Kegiatan\IndikatorKpiModel;
use App\Repository\Beneficiary\BeneficiaryBeasiswaRepository;
use App\Repository\Beneficiary\BeneficiaryIndividuRepository;
use App\Repository\Beneficiary\BeneficiaryInstitusiRepository;
use App\Repository\Beneficiary\BeneficiaryTbeRepository;
use App\Repository\Kegiatan\IndikatorKpiRepository;
use App\Repository\MasterData\BatchCsrRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndikatorKpiController
 *
 * @package App\Controller\Kegiatan
 * @author  Alex Gaspersz
 * @since   21/03/2020, modified: 28/04/2020 23:03
 *
 * @Route("/app/kegiatan/indikator-kpi")
 */
class IndikatorKpiController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var BatchCsrRepository
     */
    private $batchCsrRepository;
    /**
     * @var IndikatorKpiRepository
     */
    private $repository;
    /**
     * @var IndikatorKpiModel
     */
    private $model;
    /**
     * @var BeneficiaryBeasiswaRepository
     */
    private $beasiswaRepository;
    /**
     * @var ProgramCSRRepository
     */
    private $programRepository;
    /**
     * @var BeneficiaryTbeRepository
     */
    private $tbeRepository;
    /**
     * @var BeneficiaryInstitusiRepository
     */
    private $institusiRepository;
    /**
     * @var BeneficiaryIndividuRepository
     */
    private $individuRepository;

    /**
     * IndikatorKpiController constructor.
     *
     * @param BatchCsrRepository             $batchCsrRepository
     * @param IndikatorKpiRepository         $repository
     * @param BeneficiaryBeasiswaRepository  $beasiswaRepository
     * @param BeneficiaryTbeRepository       $tbeRepository
     * @param BeneficiaryInstitusiRepository $institusiRepository
     * @param BeneficiaryIndividuRepository  $individuRepository
     * @param ProgramCSRRepository           $programRepository
     * @param IndikatorKpiModel              $model
     */
    public function __construct(BatchCsrRepository $batchCsrRepository,
                                IndikatorKpiRepository $repository,
                                BeneficiaryBeasiswaRepository $beasiswaRepository,
                                BeneficiaryTbeRepository $tbeRepository,
                                BeneficiaryInstitusiRepository $institusiRepository,
                                BeneficiaryIndividuRepository $individuRepository,
                                ProgramCSRRepository $programRepository,
                                IndikatorKpiModel $model)
    {
        $this->batchCsrRepository = $batchCsrRepository;
        $this->repository = $repository;
        $this->beasiswaRepository = $beasiswaRepository;
        $this->tbeRepository = $tbeRepository;
        $this->institusiRepository = $institusiRepository;
        $this->individuRepository = $individuRepository;
        $this->programRepository = $programRepository;
        $this->model = $model;
    }

    /**
     * Menampilkan data indikator.
     * @Route("/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param IndikatorKpi|null $entity Persistent entity object of IndikatorKpi
     *
     * @return JsonResponse
     */
    public function fetch(?IndikatorKpi $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Indikator KPI tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $batch = $this->batchCsrRepository->find($request->get('periodeBatch'));
        if (empty($batch)) {
            return $this->jsonError('Batch Program CSR tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $entity = new IndikatorKpi();
        $entity->setProgramCsr($batch->getProgramCsr());
        $entity->setPeriodeBatch($batch);
        $entity->setTahun($request->get('tahun'));
        $entity->setSemester($request->get('semester'));

        if ($request->get('abfl')) {
            $abfl = $this->beasiswaRepository->find($request->get('abfl'));
            if (empty($abfl)) {
                return $this->jsonError('Data beneficiary ABFL tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
            }

            $entity->setAbfl($abfl);
            $entity->setAbflParams($request->get('abflParams'));
        }

        if ($request->get('tbe')) {
            $tbe = $this->tbeRepository->find($request->get('tbe'));
            if (empty($tbe)) {
                return $this->jsonError('Data beneficiary TBE tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
            }
            $entity->setTbe($tbe);
            $entity->setTbeParams($request->get('tbeParams'));
        }

        if ($request->get('institusi')) {
            $institusi = $this->institusiRepository->find($request->get('institusi'));
            if (empty($institusi)) {
                return $this->jsonError('Data beneficiary tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
            }
            $entity->setInstitusi($institusi);
            if ($request->get('paudParams')) { $entity->setPaudParams($request->get('paudParams')); }
            if ($request->get('vokasiPelatihanParams')) { $entity->setVokasiParams($request->get('vokasiPelatihanParams')); }
        }

        if ($request->get('individu')) {
            $individu = $this->individuRepository->find($request->get('individu'));
            if (empty($individu)) {
                return $this->jsonError('Data beneficiary tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
            }
            $entity->setIndividu($individu);
            if ($request->get('paudParams')) { $entity->setPaudParams($request->get('paudParams')); }
            if ($request->get('vokasiPelatihanParams')) { $entity->setVokasiParams($request->get('vokasiPelatihanParams')); }
        }

        try {
            $this->model->createIndikator($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
