<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan\Pencairan;

use App\Component\Controller\SpreadsheetController;
use App\Entity\Pencairan\Pencairan;
use App\Models\Pencairan\PencairanModel;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PencairanRincianReportController
 *
 * @package App\Controller\Kegiatan\Pencairan
 * @author  Mark Melvin
 * @since   16/05/2020
 *
 * @Route("/app/kegiatan/pencairan/rincianreport")
 */

class PencairanRincianReportController extends SpreadsheetController
{
    /** @var PencairanModel */
    private $model;


    public function __construct(PencairanModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param Pencairan $pencairan
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_header_rekap(Spreadsheet $spreadsheet, Pencairan $pencairan): Spreadsheet
    {
        // Add some data
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue( 'C3',
                $pencairan->getNomorSurat());
        $spreadsheet->getActiveSheet()
            ->setCellValue( 'C6',
                $pencairan->getTotalPencairan());

        return $spreadsheet;
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param array $dataItems
     * @param Pencairan $pencairan
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_laporan_pencairan_termin(Spreadsheet $spreadsheet,
                                                 int &$row,
                                                 array $dataItems,
                                                 Pencairan $pencairan) : Spreadsheet
    {
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 5);

        foreach ($dataItems as $item) {

            $spreadsheet->getActiveSheet()->getCell('B' . $row)->setValue($item['nama'])
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getCell('C' . $row)->setValue($item['anggaran'])
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
            $spreadsheet->getActiveSheet()->getCell('D' . $row)->setValue($item['paid'])
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_PERCENTAGE_00);

            if ($item['level'] =='kegiatan') {
                $spreadsheet->getActiveSheet()->getStyle('B' . $row.':D' . $row)->getBorders()
                    ->getOutline()->setBorderStyle(Border::BORDER_MEDIUM);
                $spreadsheet->getActiveSheet()->getStyle('B' . $row.':D' . $row)->getFont()->setBold(true);
            }
            else if ($item['level'] =='subkegiatan') {
                $spreadsheet->getActiveSheet()->getStyle('B' . $row.':D' . $row)->getBorders()
                    ->getOutline()->setBorderStyle(Border::BORDER_MEDIUM);
                $spreadsheet->getActiveSheet()->getStyle('B' . $row.':D' . $row)->getFont()->setBold(true)->setItalic(true);
            }
            else {
                $spreadsheet->getActiveSheet()->getStyle('B' . $row.':D' . $row)->getFont()->setBold(true)->setSize(9);
            }

            $row++;
        }

        return $spreadsheet;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param array $dataItems
     * @param Pencairan $pencairan
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_laporan_pencairan_expense(Spreadsheet $spreadsheet,
                                                 int &$row,
                                                 array $dataItems,
                                                 Pencairan $pencairan) : Spreadsheet
    {
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 5);

        foreach ($dataItems as $item) {
            if ($item['level'] == 'kegiatan'){
                $spreadsheet->getActiveSheet()->getCell('B' . $row)->setValue(
                    'KEGIATAN: '.$item['namaKegiatan'])
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
            }
            else {

                $spreadsheet->getActiveSheet()->getCell('A' . $row)->setValue($item['tanggal'])
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getCell('B' . $row)->setValue($item['subkegiatan'])
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getCell('C' . $row)->setValue($item['itemrab'])
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()
                    ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
            }
            $spreadsheet->getActiveSheet()->getCell('D' . $row)->setValue($item['amount'])
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_PERCENTAGE_00);

            if ($item['level'] =='kegiatan') {
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':D' . $row)->getBorders()
                    ->getOutline()->setBorderStyle(Border::BORDER_MEDIUM);
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':D' . $row)->getFont()->setBold(true);
            }
            else if ($item['level'] =='subkegiatan') {
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':D' . $row)->getBorders()
                    ->getOutline()->setBorderStyle(Border::BORDER_MEDIUM);
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':D' . $row)->getFont()->setBold(true)->setItalic(true);
            }
            else {
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':D' . $row)->getFont()->setBold(true)->setSize(9);
            }


            $row++;
        }

        return $spreadsheet;

    }



    /**
     * Menyusun Rincian pencairan
     * @Route("/full/{id}",
     *     methods={"GET"},
     *     requirements={"id":"\d+"}
     *  )
     *
     * @param Pencairan $pencairan
     * @throws Exception
     * @return Response
     */
    public function reportFull(Pencairan $pencairan) : Response
    {
        $isExpense = $pencairan->getIsExpense();
        if ($isExpense) {
            $dataItems = $this->model->generateAccountDetailPencairanExpense($pencairan);
        }
        else {
            $dataItems = $this->model->generateAccountDetailPencairan($pencairan);
        }

        if (empty($dataItems)) {
            return $this->jsonError("Data tidak ditemukan");
        }

        if ($isExpense) {
            $spreadsheet = $this->load("pencairan_rincian_expense.xlsx");
        }
        else {
            $spreadsheet = $this->load("pencairan_rincian_termin.xlsx");
        }

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Rincian Pencairan')
            ->setSubject('Rincian Pencairan')
            ->setDescription('Rincian Pencairan')
            ->setKeywords('pencairan')
            ->setCategory('Pencairan');

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BRincian Pencairan&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        // Set page orientation and size
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_rekap($spreadsheet, $pencairan);

        $row = 11;
        if ($isExpense) {
            $spreadsheet = $this->generate_laporan_pencairan_expense($spreadsheet, $row, $dataItems, $pencairan);
        }
        else {
            $spreadsheet = $this->generate_laporan_pencairan_termin($spreadsheet, $row, $dataItems, $pencairan);
        }

        $spreadsheet->getActiveSheet()->setTitle('Pencairan');
        $spreadsheet->getActiveSheet()->setShowGridLines(true);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        //IOFactory::registerWriter('Pdf', \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class);
        return $this->streamResponse($spreadsheet,
            'Rincian Pencairan'.' '.$pencairan->getNomorSurat(), 'Xlsx');

    }

    /**
     * Menyusun Rincian pencairan
     * @Route("/printfull/{id}",
     *     methods={"GET"},
     *     requirements={"id":"\d+"})
     *
     * @param Pencairan $pencairan
     * @throws Exception
     * @return Response
     */
    public function print_full(Pencairan $pencairan): Response
    {
        $isExpense = $pencairan->getIsExpense();
        if ($isExpense) {
            $dataItems = $this->model->generateAccountDetailPencairanExpense($pencairan);
        }
        else {
            $dataItems = $this->model->generateAccountDetailPencairan($pencairan);
        }

        if (empty($dataItems)) {
            return $this->jsonError("Data tidak ditemukan");
        }

        if ($isExpense) {
            $spreadsheet = $this->load("pencairan_rincian_expense.xlsx");
        }
        else {
            $spreadsheet = $this->load("pencairan_rincian_termin.xlsx");
        }

        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Rincian Pencairan')
            ->setSubject('Rincian Pencairan '.'Pencairan')
            ->setDescription('Rincian Pencairan')
            ->setKeywords('pencairan')
            ->setCategory('Pencairan');

        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BRincian Pencairan&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        // Set page orientation and size
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_rekap($spreadsheet, $pencairan);
        $row = 11;
        if ($isExpense) {
            $spreadsheet = $this->generate_laporan_pencairan_expense($spreadsheet, $row, $dataItems, $pencairan);
        }
        else {
            $spreadsheet = $this->generate_laporan_pencairan_termin($spreadsheet, $row, $dataItems, $pencairan);
        }

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Pencairan');
        $spreadsheet->getActiveSheet()->setShowGridLines(false);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        return $this->streamResponse($spreadsheet,
            'Rincian Pencairan'.' '.$pencairan->getNomorSurat(), 'pdf');
    }


}