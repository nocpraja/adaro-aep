<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */


namespace App\Controller\Kegiatan\Pencairan;

use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\Controller\WordDocumentController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Pencairan\Pencairan;
use App\Entity\Security\UserAccount;
use App\Models\Pencairan\PencairanModel;
use App\Models\Pencairan\WorkflowTags;
use App\Service\Uploadable\FileHelper;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use IntlDateFormatter;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Class PencairanDocumentController
 *
 * @package App\Controller\Kegiatan/Pencairan
 * @author  Mark Melvin
 * @since   9/27/2018 4:09 PM, modified: 03/02/2019 9:58
 *
 * @Route("/app/kegiatan/pencairan")
 */
class PencairanDocumentController extends WordDocumentController
{

    /**
     * Menampilkan single data pencairan.
     * @Route("/printdoc/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param Pencairan|null $entity Persistent entity object of Pencairan
     * @return JsonResponse
     */
    public function printdoc(?Pencairan $entity): Response
    {
        if (empty($entity)) {
            return $this->jsonError('Data Pencairan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        if ($entity->getIsExpense()==true) {
            return $this->printMultiKegiatanExpense($entity);
        }
        else {
            return $this->printMultiKegiatan($entity);
        }
    }



    private function printMultiKegiatan(Pencairan $entity) : Response {
        $template = $this->loadTemplate('MultiKegiatan.docx');

        $template->setValue('namaMitra', $entity->getMitra()->getNamaLembaga());
        $template->setValue('nomorSurat', $entity->getNomorSurat());
        $fmt = new IntlDateFormatter( "id-ID" ,IntlDateFormatter::SHORT, IntlDateFormatter::NONE,
            'Asia/Jakarta', IntlDateFormatter::GREGORIAN  );
        $tanggal = $fmt->format($entity->getCreatedDate());
        $template->setValue('tanggal', $tanggal);
        $template->setValue('program', $entity->getMitra()->getProgramCsr()->getName());

        $nRows = $entity->getChildren()->count();
        $template->cloneRow('title', $nRows);
        $no = 1;
        $totalBudget = 0;
        foreach ($entity->getChildren() as $item){
            $template->setValue('row#'.$no, $no);
            $template->setValue('title#'.$no, $item->getTitle());
            $amount = $item->getAmount();
            if ($item->getKegiatan()) {
                $amount = $item->getKegiatan()->getAnggaran();
            }
            $totalBudget += $amount;
            $template->setValue('amount#'.$no, 'Rp. '.number_format($amount,2,",","."));
            $no++;
        }
        $template->setValue('tipe_termin', $entity->getTipeTermin());
        $template->setValue('persen', $entity->getPersen());
        $template->setValue('totalBudget', 'Rp. '.number_format($totalBudget,2,",","."));
        $template->setValue('totalPencairan', 'Rp. '.number_format($entity->getTotalPencairan(),2,",","."));

        return $this->streamResponse($template, 'Tagihan-'.$entity->getNomorSurat().'.docx');
    }




    private function printMultiKegiatanExpense(Pencairan $entity) : Response {
        $template = $this->loadTemplate('MultiKegiatanExpense.docx');

        $template->setValue('namaMitra', $entity->getMitra()->getNamaLembaga());
        $template->setValue('nomorSurat', $entity->getNomorSurat());
        $fmt = new IntlDateFormatter( "id-ID" ,IntlDateFormatter::SHORT, IntlDateFormatter::NONE,
            'Asia/Jakarta', IntlDateFormatter::GREGORIAN  );
        $tanggal = $fmt->format($entity->getCreatedDate());
        $template->setValue('tanggal', $tanggal);
        $template->setValue('program', $entity->getMitra()->getProgramCsr()->getName());

        $nRows = $entity->getChildren()->count();
        $template->cloneRow('title', $nRows);
        $no = 1;
        foreach ($entity->getChildren() as $item){
            $template->setValue('row#'.$no, $no);
            $template->setValue('title#'.$no, $item->getTitle());
            $template->setValue('amount#'.$no, 'Rp. '.number_format($item->getAmount(),2,",","."));
            $no++;
        }
        $template->setValue('tipe_termin', $entity->getTipeTermin());
        $template->setValue('persen', $entity->getPersen());
        $template->setValue('totalPencairan', 'Rp. '.number_format($entity->getTotalPencairan(),2,",","."));

        return $this->streamResponse($template, 'Tagihan-'.$entity->getNomorSurat().'.docx');
    }




}