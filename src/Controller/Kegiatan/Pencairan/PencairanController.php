<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */


namespace App\Controller\Kegiatan\Pencairan;

use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Pencairan\Pencairan;
use App\Entity\Security\UserAccount;
use App\Models\Pencairan\PencairanModel;
use App\Models\Pencairan\WorkflowTags;
use App\Service\Uploadable\FileHelper;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PencairanController
 *
 * @package App\Controller\Kegiatan\Pencairan
 * @author  Mark Melvin
 * @since   9/27/2018 4:09 PM, modified: 03/02/2019 9:58
 *
 * @Route("/app/kegiatan/pencairan")
 */
class PencairanController extends JsonController
{
    use FormErrorsNormalizerTrait;

    const UPLOAD_PATH = 'uploads/pencairan/';

    /** @var PencairanModel */
    private $model;

    /** @var FileHelper */
    private $filesystem;

    /**
     * PencairanController constructor.
     * @param PencairanModel $model
     * @param FileHelper $filesystem
     */
    public function __construct(PencairanModel $model, FileHelper $filesystem)
    {
        $this->model = $model;
        $this->filesystem = $filesystem;
    }


    /**
     * @Route("", methods={"GET"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $paginator = $this->model->displayAllByCriteria($dq, $this->getUser());

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menampilkan single data pencairan.
     * @Route("/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param Pencairan|null $entity Persistent entity object of Pencairan
     * @return JsonResponse
     */
    public function fetch(?Pencairan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Pencairan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_withrels);
    }

    /**
     * Proses create data pencairan dana.
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function create(Request $request): JsonResponse
    {
        /** @var UserAccount $user */
        $user = $this->getUser();

        try {
            $isExpense = $request->request->get('isExpense');
            $isPelunasan = $request->request->get('isPelunasan');
            $entity = $this->model->createPencairan($isExpense, $isPelunasan, $user);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Proses update data pencairan dana.
     * @Route("/update/{id}", requirements={"id":"\d+"}, methods={"POST"})
     *
     * @param Request $request HTTP Request
     * @param Pencairan|null $pencairan
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, ?Pencairan $pencairan): JsonResponse
    {
        $tipe_termin = $request->request->get('tipe_termin');
        $persen = $request->request->get('persen');
        $children = $request->request->get('kegiatans');
        $total_budget = $request->request->get('totalBudget');
        $total_pencairan = $request->request->get('totalPencairan');
        $entity = $this->model->update($pencairan, $tipe_termin, $persen, $children, $total_budget, $total_pencairan);

        return $this->jsonResponse($entity, -1, $this->context_withrels);
    }


    /**
     * Menghapus record publikasi humas.
     * @Route("/delete/{id}", requirements={"id":"\d+"}, methods={"DELETE"})
     *
     * @param Pencairan|null $entity
     *
     * @return JsonResponse
     */
    public function delete(?Pencairan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data pencairan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->model->delete($entity);
            return $this->jsonSuccess(sprintf('Entri pencairan <b>%s</b> telah berhasil dihapus dari database',
                $entity->getNomorSurat()));
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Proses workflow transition pencairan
     * @Route("/stepflow/{id}", requirements={"id":"\d+"}, methods={"PATCH"})
     *
     * @param Request     $request HTTP Request
     * @param Pencairan   $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, Pencairan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data pencairan tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }
        $user = $this->getUser();

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            $this->model->doWorkflow($entity, $data, $user);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return array
     */
    private function createUploadDir(): array
    {
        $current = date('Y/m/d');
        $uploadPath = self::UPLOAD_PATH . $current;

        if (!$this->filesystem->existFile($uploadPath)) {
            /** Create base upload folder Y/m/d **/
            $this->filesystem->createDir($uploadPath);
        }
        $params = [
            'currentPath' => $current,
            'uploadPath' => $uploadPath,
        ];

        return $params;
    }

    /**
     * Proses upload data pencairan dana.
     * @Route("/upload/{id}", requirements={"id":"\d+"}, methods={"POST"})
     *
     * @param Request $request HTTP Request
     * @param Pencairan|null $pencairan
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function upload(Request $request, ?Pencairan $pencairan): JsonResponse
    {

        try {
            $attachment = null;
            $params = [];
            if ($request->files->has('file')) {
                $attachment = $request->files->get('file');
                $params = $this->createUploadDir();
            }

            $this->model->upload($pencairan, $attachment, $params);
            return $this->jsonResponse($pencairan, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Proses update tgl transfer dan nomor voucher
     * @Route("/update-tgl-transfer-no-bdvc/{id}", requirements={"id":"\d+"}, methods={"POST"})
     *
     * @param Request $request HTTP Request
     * @param Pencairan|null $pencairan
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateTglTransferNoVoucher(Request $request, ?Pencairan $pencairan): JsonResponse
    {
        try {
            $tglTransfer = $request->request->get('tglTransfer');
            $nomorVoucher = $request->request->get('nomorVoucher');

            $pencairan = $this->model->updateTglTransferNoVoucher($pencairan, $tglTransfer, $nomorVoucher);
            return $this->jsonResponse($pencairan, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Proses menampilkan daftar realisasi gelondongan yang sudah approved
     * @Route("/realisasi-list/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param Kegiatan $kegiatan
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function realisasiList(Kegiatan $kegiatan) {
        $realisasis = $this->model->getRealisasiList($kegiatan);
        return $this->jsonResponse($realisasis, -1, ['groups' => ['with_rel']]);
    }

    /**
     * @Route("/rincian/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param Pencairan $pencairan
     *
     * @return JsonResponse
     */
    public function rincianPencairan(Pencairan $pencairan){
        $rincian = $this->model->generateAccountDetailPencairan($pencairan);
        return $this->jsonResponse($rincian, -1, $this->context_norels);
    }

    /**
     * @Route("/rincian-expense/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param Pencairan $pencairan
     *
     * @return JsonResponse
     */
    public function rincianPencairanExpense(Pencairan $pencairan){
        $rincian = $this->model->generateAccountDetailPencairanExpense($pencairan);
        return $this->jsonResponse($rincian, -1, $this->context_norels);
    }

    
}