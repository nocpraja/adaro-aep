<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan\LembarMonitoring;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Entity\Kegiatan\LembarMonitoring;
use App\Entity\Kegiatan\LembarMonitoringFile;
use App\Form\Kegiatan\LembarMonitoringType;
use App\Repository\Kegiatan\LembarMonitoringRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RolesController
 *
 * @package App\Controller\Kegiatan
 * @author  Mark Melvin
 * @since   16/11/2018
 *
 * @Route("/app/kegiatan/lembar-monitoring")
 */
class LembarMonitoringController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var LembarMonitoringRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * RolesController constructor.
     *
     * @param LembarMonitoringRepository $repository
     * @param EntityManagerInterface     $em
     * @param AuditLogger                $logger AuditLogger service
     */
    public function __construct(LembarMonitoringRepository $repository, EntityManagerInterface $em, AuditLogger $logger)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * Menampilkan daftar lembar monitoring.
     *
     * @Route("", name="lembar_monitoring_index", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function index()
    {
        $pages = $this->repository->findBy([], ['id' => 'desc']);

        return $this->jsonResponse($pages, count($pages));
    }


    /**
     * Menampilkan daftar lembar monitoring.
     *
     * @Route("/find/{type}/{id}", name="lembar_monitoring_find", requirements={"type":"\w+","id":"\d+"},
     *                             methods={"GET"})
     * @param string  $type
     * @param integer $id
     *
     * @return JsonResponse
     */
    public function find(string $type, int $id)
    {
        $pages = $this->repository->findBy([
                                               "type"          => $type,
                                               "beneficiaryId" => $id,
                                           ], ['id' => 'desc']);

        return $this->jsonResponse($pages, count($pages));
    }


    /**
     * Menampilkan subyek lembar monitoring.
     *
     * @Route("/who/{type}/{id}", name="lembar_monitoring_who", requirements={"type":"\w+","id":"\d+"}, methods={"GET"})
     * @param string  $type
     * @param integer $id
     *
     * @return JsonResponse
     */
    public function who(string $type, int $id)
    {
        if ($type == 'PAUD' || $type == 'SMKPOLTEK') {
            $sql = 'select institusi_id as id, nama_institusi as nama from beneficiary_institusi where institusi_id = ?';
        } else if ($type == 'GURUPAUD' || $type == 'SISWAPAUD' || $type == 'BEASISWA') {
            $sql = 'select individu_id as id, nama_lengkap as nama from beneficiary_individu where individu_id = ?';
        } else {
            return $this->jsonError("Data Tidak Ditemukan", JsonResponse::HTTP_NOT_FOUND);
        }

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('App\\Entity\\Kegiatan\\SubyekMonitoring', 's');
        $rsm->addFieldResult('s', 'id', 'id');
        $rsm->addFieldResult('s', 'nama', 'nama');

        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $id);

        $who = $query->getResult();

        return $this->jsonResponse($who, count($who));
    }

    /**
     * Menampilkan detail record LembarMonitoring.
     *
     * @Route("/{id}", name="lembar_monitoring_fetch", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param LembarMonitoring $entity Persistent entity object of LembarMonitoring
     *
     * @return JsonResponse
     */
    public function fetch(?LembarMonitoring $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_withrels);
    }

    /**
     * Menambahkan LembarMonitoring baru ke dalam database.
     *
     * @Route("/create", name="lembar_monitoring_create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(LembarMonitoringType::class, new LembarMonitoring());
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $this->em->persist($entity);
        $this->logger->info('TAMBAH record LembarMonitoring baru.', 'LembarMonitoring', $entity,
                            [self::CONTEXT_NON_REL]);
        $this->em->flush();

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }


    /**
     * Menghapus record LembarMonitoring dari database.
     *
     * @Route("/delete/{id}", name="lembar_monitoring_delete", methods={"DELETE"},
     *                                 requirements={"id":"\d+"})
     *
     * @param LembarMonitoring $entity Persistent entity object of LembarMonitoring
     *
     * @return JsonResponse
     */
    public function delete(?LembarMonitoring $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Lembar Monitoring tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }
        //hapus file;
        $fileName = $entity->getNamaFile();
        try {
            $localName = $this->getLocalSaveDirectory() . '/' . $fileName;
            if (file_exists($localName)) {
                unlink($localName);
            }
        } catch (\Exception $e) {

        }

        $this->logger->notice('HAPUS record Lembar Monitoring.', 'LembarMonitoring', $entity, [self::CONTEXT_NON_REL]);
        $this->em->remove($entity);
        $this->em->flush();

        return $this->jsonSuccess('Data Lembar Monitoring telah berhasil dihapus dari database');
    }

    /**
     *
     * @return string
     */
    public function getSaveDirectory(): string
    {
        return 'uploads';
    }

    /**
     *
     * @return string
     */
    public function getLocalSaveDirectory(): string
    {
        return realpath(__DIR__ . '/../../../../public/' . $this->getSaveDirectory());
    }

    /**
     * Menerima file upload lembar monitoring.
     *
     * @Route("/upload/{type}/{id}", name="lembar_monitoring_upload", requirements={"type":"\w+","id":"\d+"},
     *                               methods={"POST"})
     * @param string  $type
     * @param integer $id
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function upload(string $type, int $id, Request $request): JsonResponse
    {
        $form = $this->createFormBuilder(new LembarMonitoringFile())
                     ->add('file', FileType::class)
                     ->getForm();

        $form->submit($request->files->all());
        if ($form->isValid()) {
            $upload = $form->getData();
            $file = $upload->getFile();

            $tanggal = $request->request->get('tanggalInput');
            if (!isset($tanggal)) {
                $date = new \DateTime();
                $tanggal = $date->format('Y-m-d');
            }

            $fileName = sprintf('%d%s.%s-', $id, $type, $tanggal) .
                substr(htmlspecialchars(basename($file->getClientOriginalName(), '.' . $file->guessExtension())), 0,
                       200) .
                '.' . $file->guessExtension();
            $file->move($this->getSaveDirectory(), $fileName);

            $result['file'] = $fileName;
            return $this->jsonResponse($result);
        } else {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

    }


}