<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan\Pengelolaan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Entity\Kegiatan\ItemAnggaran;
use App\Entity\Kegiatan\ItemRealisasiRab;
use App\Form\Kegiatan\ItemAnggaranType;
use App\Form\Kegiatan\ItemRealisasiType;
use App\Models\Kegiatan\KegiatanModel;
use App\Repository\MasterData\DataReferenceRepository;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ItemRabController
 *
 * @package App\Controller\Kegiatan\Pengelolaan
 * @author  Ahmad Fajar
 * @since   18/11/2018, modified: 07/10/2019 11:24
 *
 * @Route("/app/kegiatan/pengelolaan/item-rab")
 */
class ItemRabController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var KegiatanModel
     */
    private $model;

    /**
     * @var DataReferenceRepository
     */
    private $dataReferenceRepository;


    /**
     * ItemRabController constructor.
     *
     * @param KegiatanModel           $model                   Domain model Kegiatan
     * @param DataReferenceRepository $dataReferenceRepository Doctrine entity repository instance
     */
    public function __construct(KegiatanModel $model, DataReferenceRepository $dataReferenceRepository)
    {
        $this->model = $model;
        $this->dataReferenceRepository = $dataReferenceRepository;
    }

    /**
     * Menambahkan data pengelolaan item RAB baru.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new ItemAnggaran();
        $form = $this->createForm(ItemAnggaranType::class, $entity, [
            'subkegiatan_repo' => $this->model->getSubkegiatanRepository(),
            'dataref_repo'     => $this->dataReferenceRepository
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->createItemRab($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menambahkan data pengelolaan item RAB & Realisasi baru.
     * @Route("/realisasi-rab/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function createRabRealisasi(Request $request): JsonResponse
    {
        $entity = new ItemAnggaran();
        $form = $this->createForm(ItemAnggaranType::class, $entity, [
            'subkegiatan_repo' => $this->model->getSubkegiatanRepository(),
            'dataref_repo'     => $this->dataReferenceRepository
        ]);
        $form->submit($request->request->all());

        try {
            $this->model->createItemRabRealisasi($entity, $request);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/realisasi/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function realisasicreate(Request $request): JsonResponse
    {
        $entity = new ItemRealisasiRab();
        $form = $this->createForm(ItemRealisasiType::class, $entity, [
            'itemanggaran_repo' => $this->model->getItemRabRepository()
        ]);
        $form->submit($request->request->all());

        try {
            $this->model->createItemRealisasi($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menampilkan data Item RAB.
     *
     * @Route("/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param ItemAnggaran|null $entity Persistent entity object of item RAB
     *
     * @return JsonResponse
     */
    public function fetch(?ItemAnggaran $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data entri RAB tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_CHILD]);
    }

    /**
     * Menampilkan data Item RAB.
     *
     * @Route("/fetch/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param ItemAnggaran|null $entity Persistent entity object of item RAB
     *
     * @return JsonResponse
     */
    public function fetch1(?ItemAnggaran $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data entri RAB tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_PARENT]);
    }

    /**
     * Menyimpan pembaruan data pengelolaan item RAB.
     *
     * @Route("/update/{id}", requirements={"id":"\d+"}, methods={"POST"})
     *
     * @param Request           $request HTTP Request
     * @param ItemAnggaran|null $entity  Persistent entity object of item RAB
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?ItemAnggaran $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data entri RAB tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ItemAnggaranType::class, $entity, [
            'subkegiatan_repo' => $this->model->getSubkegiatanRepository(),
            'dataref_repo'     => $this->dataReferenceRepository
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->updateItemRab($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menghapus record Item RAB.
     *
     * @Route("/delete/{id}", requirements={"id":"\d+"}, methods={"DELETE"})
     *
     * @param ItemAnggaran $entity Persistent entity object item RAB
     *
     * @return JsonResponse
     */
    public function delete(?ItemAnggaran $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data entri RAB tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->model->deleteItemRab($entity);
            return $this->jsonSuccess(sprintf('Entri RAB <b>%s</b> telah berhasil dihapus dari database',
                                              $entity->getTitle()));
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
