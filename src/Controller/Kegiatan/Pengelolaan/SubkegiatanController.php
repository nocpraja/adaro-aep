<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan\Pengelolaan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Kegiatan\Subkegiatan;
use App\Form\Kegiatan\SubkegiatanType;
use App\Models\Kegiatan\KegiatanModel;
use App\Repository\MasterData\KabupatenRepository;
use App\Repository\MasterData\ProvinsiRepository;
use Exception;
use LogicException;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class SubkegiatanController
 * @package App\Controller\Kegiatan\Pengelolaan
 * @author  Ahmad Fajar
 * @since   17/11/2018, modified: 20/02/2020 19:42
 * @Route("/app/kegiatan/pengelolaan/subkegiatan")
 */
class SubkegiatanController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var KegiatanModel
     */
    private $model;

    /**
     * @var ProvinsiRepository
     */
    private $provinsiRepository;

    /**
     * @var KabupatenRepository
     */
    private $kabupatenRepository;


    /**
     * SubkegiatanController constructor.
     *
     * @param KegiatanModel       $model               Domain model Kegiatan
     * @param ProvinsiRepository  $provinsiRepository  Doctrine entity repository instance
     * @param KabupatenRepository $kabupatenRepository Doctrine entity repository instance
     */
    public function __construct(KegiatanModel $model,
                                ProvinsiRepository $provinsiRepository,
                                KabupatenRepository $kabupatenRepository)
    {
        $this->model = $model;
        $this->provinsiRepository = $provinsiRepository;
        $this->kabupatenRepository = $kabupatenRepository;
    }

    /**
     * Menampilkan daftar pengelolaan Subkegiatan baru.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAllSubkegiatan(DataQuery::parseRequest($request));

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menambahkan data pengelolaan Subkegiatan baru.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new Subkegiatan();
        $form = $this->createForm(SubkegiatanType::class, $entity, [
            'kegiatan_repo'  => $this->model->getRepository(),
            'provinsi_repo'  => $this->provinsiRepository,
            'kabupaten_repo' => $this->kabupatenRepository
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->createSubkegiatan($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menampilkan data Subkegiatan.
     *
     * @Route("/{sid}", requirements={"sid":"\d+"}, methods={"GET"})
     *
     * @param Subkegiatan|null $entity Persistent entity object of Subkegiatan
     *
     * @return JsonResponse
     */
    public function fetch(?Subkegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Subkegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_CHILD]);
    }

    /**
     * Menyimpan pembaruan data pengelolaan Subkegiatan.
     *
     * @Route("/update/{sid}", requirements={"sid":"\d+"}, methods={"POST"})
     *
     * @param Request          $request HTTP Request
     * @param Subkegiatan|null $entity  Persistent entity object of Subkegiatan
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?Subkegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Subkegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(SubkegiatanType::class, $entity, [
            'kegiatan_repo'  => $this->model->getRepository(),
            'provinsi_repo'  => $this->provinsiRepository,
            'kabupaten_repo' => $this->kabupatenRepository,
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->updateSubkegiatan($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menghapus record pengelolaan Subkegiatan.
     *
     * @Route("/delete/{sid}", requirements={"sid":"\d+"}, methods={"DELETE"})
     *
     * @param Subkegiatan $entity Persistent entity object of Subkegiatan
     *
     * @return JsonResponse
     */
    public function delete(?Subkegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Subkegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->model->deleteSubkegiatan($entity);
            return $this->jsonSuccess(sprintf('Subkegiatan <b>%s</b> telah berhasil dihapus dari database',
                                              $entity->getTitle()));
        } catch (LogicException $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menyimpan pembaruan data realisasi anggaran Subkegiatan.
     *
     * @Route("/realisasi/{sid}", requirements={"sid":"\d+"}, methods={"PATCH"})
     *
     * @param Request          $request HTTP Request
     * @param Subkegiatan|null $entity  Persistent entity object of Subkegiatan
     *
     * @return JsonResponse
     */
    public function realisasi(Request $request, ?Subkegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Subkegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }
        $form = $this->createFormBuilder($entity)
                     ->add('realisasi', NumberType::class, [
                         'constraints' => new NotNull(['message' => 'Anggaran.Realisasi.NotNull'])
                     ])->getForm();

        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $entity->setPostedRealisasiBy($this->getUser());
            $this->model->updateRealisasiSubkegiatan($entity);

            return $this->jsonSuccess(sprintf('Realisasi anggaran <b>%s</b> telah berhasil disimpan',
                                              $entity->getTitle()));
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menampilkan daftar Subkegiatan Berdasarkan Kegiatan ID.
     * @Route("/all/{id}", requirements={"id":"\d+"}, methods={"GET"})
     *
     * @param Request $request
     * @param int     $id
     *
     * @return JsonResponse
     */
    public function fetchDocSubKegiatan(Request $request, int $id): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);

        $filters = [QueryExpressionHelper::createCriteria(['property' => 'kegiatan', 'value' => $id])];

        $dq->setFilters($filters);
        $paginator = $this->model->displayAllSubkegiatan($dq);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);

    }
}
