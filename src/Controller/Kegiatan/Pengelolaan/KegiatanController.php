<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan\Pengelolaan;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\ActionObject;
use App\Component\DataObject\DataQuery;
use App\Component\Form\Extension\ActionObjectType;
use App\Entity\Kegiatan\Kegiatan;
use App\Form\Kegiatan\KegiatanType;
use App\Models\Kegiatan\KegiatanModel;
use App\Repository\MasterData\BatchCsrRepository;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KegiatanController
 *
 * @package App\Controller\Kegiatan\Pengelolaan
 * @author  Ahmad Fajar
 * @since   13/11/2018, modified: 09/02/2020 02:58
 *
 * @Route("/app/kegiatan/pengelolaan")
 */
class KegiatanController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var KegiatanModel
     */
    private $model;

    /**
     * @var BatchCsrRepository
     */
    private $batchRepository;


    /**
     * KegiatanController constructor.
     *
     * @param KegiatanModel      $model           Domain model Kegiatan
     * @param BatchCsrRepository $batchRepository Doctrine entity repository instance
     */
    public function __construct(KegiatanModel $model, BatchCsrRepository $batchRepository)
    {
        $this->model = $model;
        $this->batchRepository = $batchRepository;
    }

    /**
     * Menampilkan daftar pengelolaan kegiatan.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginator = $this->model->displayAllKegiatan(DataQuery::parseRequest($request), $this->getUser());

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menambahkan data pengelolaan kegiatan baru.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new Kegiatan();
        $form = $this->createForm(KegiatanType::class, $entity, [
            'program_repo' => $this->model->getDanaBatchRepository(),
            'batch_repo'   => $this->batchRepository,
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->createKegiatan($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menampilkan data Kegiatan.
     *
     * @Route("/{kegiatanId}", requirements={"kegiatanId":"\d+"}, methods={"GET"})
     *
     * @param Kegiatan|null $entity Persistent entity object of Kegiatan
     *
     * @return JsonResponse
     */
    public function fetch(?Kegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Kegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, [self::CONTEXT_NON_REL, self::CONTEXT_WITH_APPROVAL]);
    }

    /**
     * Menampilkan histori approval suatu Kegiatan.
     *
     * @Route("/log-approval/{kegiatanId}", requirements={"kegiatanId":"\d+"}, methods={"GET"})
     *
     * @param Kegiatan|null $entity Persistent entity object of Kegiatan
     *
     * @return JsonResponse
     */
    public function logApproval(?Kegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Kegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $results = $this->model->fetchLogApproval($entity);

        return $this->jsonResponse($results, count($results), [self::CONTEXT_NON_REL, self::CONTEXT_WITH_POSTDATE]);
    }

    /**
     * Menampilkan data RAB Kegiatan.
     *
     * @Route("/view-rab/{kegiatanId}", requirements={"kegiatanId":"\d+"}, methods={"GET"})
     *
     * @param Kegiatan|null $entity Persistent entity object of Kegiatan
     *
     * @return JsonResponse
     */
    public function viewRab(?Kegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Kegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $children = $this->model->getSubkegiatanRepository()->findBy(['kegiatan' => $entity], ['postedDate' => 'asc']);

        return $this->jsonResponse($children, count($children), [self::CONTEXT_NON_REL, self::CONTEXT_WITH_CHILD]);
    }

    /**
     * Menyimpan pembaruan data pengelolaan Kegiatan.
     *
     * @Route("/update/{kegiatanId}", requirements={"kegiatanId":"\d+"}, methods={"POST"})
     *
     * @param Request       $request HTTP Request
     * @param Kegiatan|null $entity  Persistent entity object of Kegiatan
     *
     * @return JsonResponse
     */
    public function update(Request $request, ?Kegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Kegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(KegiatanType::class, $entity, [
            'program_repo' => $this->model->getDanaBatchRepository(),
            'batch_repo'   => $this->batchRepository,
        ]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->updateKegiatan($entity);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Menghapus record pengelolaan Kegiatan.
     *
     * @Route("/delete/{kegiatanId}", requirements={"kegiatanId":"\d+"}, methods={"DELETE"})
     *
     * @param Kegiatan $entity Persistent entity object of Kegiatan
     *
     * @return JsonResponse
     */
    public function delete(?Kegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Kegiatan tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->model->deleteKegiatan($entity);
            return $this->jsonSuccess(sprintf('Kegiatan <b>%s</b> telah berhasil dihapus dari database',
                $entity->getNamaKegiatan()));
        } catch (LogicException $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Proses workflow transition pada Rencana Kegiatan.
     *
     * @Route("/stepflow/{id}", methods={"PATCH"}, requirements={"id":"\d+"})
     *
     * @param Request       $request HTTP Request
     * @param Kegiatan|null $entity  Persistent entity object
     *
     * @return JsonResponse
     */
    public function stepflow(Request $request, ?Kegiatan $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data Kegiatan tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $data = new ActionObject();
        $form = $this->createForm(ActionObjectType::class, $data);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->model->actionTransition($entity, $data);
            return $this->jsonResponse($entity, -1, $this->context_norels);
        } catch (LogicException $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_FORBIDDEN);
        } catch (Exception $ex) {
            return $this->jsonError($ex->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
