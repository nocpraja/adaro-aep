<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan\Overview;

use App\Component\Controller\JsonController;
use App\DataObject\Kegiatan\ProgressTahunanDto;
use App\DataObject\Kegiatan\SerapanTahunanDto;
use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\MasterData\ProgramCSR;
use App\Models\Kegiatan\ChartKegiatanModel;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KegiatanController
 *
 * @package App\Controller\Kegiatan\Overview
 * @author  Mark Melvin
 * @since   13/11/2019, modified: 13/11/2019 07:08
 *
 * @Route("/app/kegiatan/chart")
 */
class ChartController extends JsonController
{

    /** @var ChartKegiatanModel $model */
    private $model;

    /**
     * ChartController constructor.
     * @param ChartKegiatanModel $model
     *
     */
    public function __construct(ChartKegiatanModel $model)
    {
        $this->model = $model;
    }


    /**
     * @Route("/", methods={"GET"})
     *
     * @return JsonResponse
     *
     */
    public function index() : JsonResponse
    {
        return $this->jsonResponse([
            "judul" => "Progress Kegiatan CSR",
            "data" => []
        ]);
    }

    /**
     * @Route("/{tahun}/bidang/{id}", requirements={"tahun":"\d+","id":"\d+"}, methods={"GET"})
     * @param int $tahun
     * @param JenisProgramCSR|null $bidangCsr
     * @return JsonResponse
     *
     */
    public function dataBidang(int $tahun, ?JenisProgramCSR $bidangCsr) : JsonResponse
    {
        if (!isset($bidangCsr)) {
            return $this->jsonError("Tidak ada bidang tersebut", Response::HTTP_NOT_FOUND);
        }
        $reportBidang = $this->model->buildReportBidang($bidangCsr, $tahun);
        $chartData = $this->prepareReportData(
            "Progress Kegiatan Bidang CSR ".$bidangCsr->getTitle(),
            $reportBidang);

        return $this->jsonResponse($chartData, -1, $this->context_norels);
    }

    /**
     * @Route("/{tahun}/program/{id}", requirements={"tahun":"\d+","id":"\d+"}, methods={"GET"})
     * @param int $tahun
     * @param ProgramCSR|null $programCsr
     * @return JsonResponse
     *
     */
    public function dataProgram(int $tahun, ?ProgramCSR $programCsr) : JsonResponse
    {
        if (!isset($programCsr)) {
            return $this->jsonError("Tidak ada program tersebut", Response::HTTP_NOT_FOUND);
        }
        $reportProgram = $this->model->buildReportProgram($programCsr, $tahun);
        $chartData = $this->prepareReportData(
            "Progress Kegiatan Program CSR ".$programCsr->getName(),
            $reportProgram);

        return $this->jsonResponse($chartData, -1, $this->context_norels);

    }

    /**
     * Menyusun data untuk ditampilkan di chart
     * @param string $judul
     * @param array|null $reportData
     * @return array
     */
    private function prepareReportData(string $judul, ?array $reportData) : array
    {
        $data = [];

        /** @var ProgressTahunanDto $progress */
        $progress = $reportData['progress'];
        /** @var SerapanTahunanDto $serapan */
        $serapan = $reportData['serapan'];

        //$totalBobot = $progress->getBobot();
        $totalBobot = $reportData['totalBobot'];

        $totalBudget = $serapan->getBudget();

        $serapanAkumulasi = 0.0;
        for ($month = 1; $month<=12; $month++) {
            $getrencanaMonth = 'getRencana'.(int)$month;
            $getrealisasiMonth = 'getRealisasi'.(int)$month;
            $getserapanMonth = 'getSerapan'.(int)$month;
            $serapanAkumulasi += $serapan->$getserapanMonth();
            $monthRencana = ($totalBobot != 0) ? $progress->$getrencanaMonth()*100/$totalBobot : 0.0;
            $monthRealisasi = ($totalBobot != 0) ? $progress->$getrealisasiMonth()*100/$totalBobot : 0.0;
            $monthSerapan =  $serapanAkumulasi;

            $data[] =
                [
                    "bulan" => $month,
                    "rencana" => $monthRencana,
                    "realisasi" => $monthRealisasi,
                    "serapan" => $monthSerapan,
                ];

        }
        $maxSerapan = max($totalBudget, $serapanAkumulasi);

        return [
            'judul' => $judul,
            'data' => $data,
            'maxSerapan' => $maxSerapan

            // DEBUGGING INFORMATION

            //'debugProgres' => $reportData['debugProgres'],
            //'debugSerapan' => $reportData['debugSerapan'],
        ];

    }

    /**
     * @Route("/rekap", methods={"GET"})
     * @return JsonResponse
     */
    public function overview(): JsonResponse
    {
        $tahun = (int)date('Y');
        $bidang = $this->model->getJenisProgramCsrRepository()->find(2); // bidang pendidikan
        return $this->overviewTahun($tahun, $bidang);
    }

    /**
     * @Route("/rekap/{tahun}/{id}", requirements={"tahun":"\d+", "id":"\d"}, methods={"GET"})
     * @param int $tahun
     * @param JenisProgramCSR $bidang
     * @return JsonResponse
     */
    public function overviewTahun(int $tahun, JenisProgramCSR $bidang): JsonResponse
    {
        $chartData = $this->model->buildOverview($tahun, $bidang);
        return $this->jsonResponse($chartData);
    }


}