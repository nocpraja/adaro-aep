<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan;


use App\Component\Controller\JsonController;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Kegiatan\CapaianKpi;
use App\Entity\Kegiatan\ViewIndikator;
use App\Entity\Kegiatan\ViewTargetKpi;
use App\Entity\MasterData\BatchCsr;
use App\Models\Kegiatan\KpiModel;
use App\Repository\Kegiatan\CapaianKpiRepository;
use App\Repository\Kegiatan\ViewIndikatorKpiRepository;
use App\Repository\Kegiatan\ViewTargetKpiRepository;
use App\Repository\MasterData\BatchCsrRepository;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KpiController
 *
 * @package App\Controller\Kegiatan
 * @author  Mark Melvin
 * @since   28/01/2019, modified: 25/02/2019 12:51
 *
 * @Route("/app/kegiatan/kpi")
 */
class KpiController extends JsonController
{

    /**
     * @var KpiModel
     */
    private $model;

    /**
     * @var BatchCsrRepository
     */
    private $batchCsrRepository;

    /**
     * @var CapaianKpiRepository
     */
    private $capaianKpiRepository;
    /**
     * @var ViewIndikatorKpiRepository
     */
    private $viewIndikatorKpiRepository;
    /**
     * @var ViewTargetKpiRepository
     */
    private $targetKpiRepository;


    /**
     * KpiController constructor.
     *
     * @param BatchCsrRepository         $batchCsrRepository   Doctrine entity repository instance
     * @param CapaianKpiRepository       $capaianKpiRepository Doctrine entity repository instance
     * @param ViewIndikatorKpiRepository $viewIndikatorKpiRepository
     * @param ViewTargetKpiRepository    $targetKpiRepository
     * @param KpiModel                   $model                Model Capaian KPI
     */
    public function __construct(BatchCsrRepository $batchCsrRepository,
                                CapaianKpiRepository $capaianKpiRepository,
                                ViewIndikatorKpiRepository $viewIndikatorKpiRepository,
                                ViewTargetKpiRepository $targetKpiRepository,
                                KpiModel $model)
    {
        $this->batchCsrRepository = $batchCsrRepository;
        $this->capaianKpiRepository = $capaianKpiRepository;
        $this->model = $model;
        $this->viewIndikatorKpiRepository = $viewIndikatorKpiRepository;
        $this->targetKpiRepository = $targetKpiRepository;
    }

    /**
     * Menampilkan data monitoring capaian KPI untuk semua Program CSR.
     *
     * @Route("/monitor-programs", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function monitorCapaianPrograms(): JsonResponse
    {
        $programs = $this->model->collectProgramCsrKpi($this->getUser());

        return $this->jsonResponse($programs, count($programs), $this->context_norels);
    }

    /**
     * Menampilkan detail target untuk batch
     *
     * @Route("/fetch-target/{batch}", requirements={"batch":"\d+"}, methods={"GET"})
     *
     * @param BatchCsr $batch Persistent entity object of Batch pembinaan
     *
     * @return JsonResponse
     */
    public function fetchTarget(BatchCsr $batch): JsonResponse
    {
        $program = $batch->getProgramCsr();
        $msid = $program->getMsid();
        $params = $this->model->getParams($msid);
        $targetArray = $this->model->getTargetsBatch($batch);

        $detail = array(
            'batch'   => $batch,
            'params'  => $params,
            'targets' => $targetArray
        );

        return $this->jsonResponse($detail, -1, $this->context_norels);
    }

    /**
     * Menampilkan detail capaian untuk tahun batch
     *
     * @Route("/fetch-capaian/{batch}/{tahun}", requirements={"batch":"\d+","tahun":"\d+"}, methods={"GET"})
     *
     * @param BatchCsr $batch Persistent entity object of Batch pembinaan
     * @param int      $tahun Tahun anggaran
     *
     * @return JsonResponse
     */
    public function fetchCapaian(BatchCsr $batch, int $tahun): JsonResponse
    {
        $program = $batch->getProgramCsr();
        $msid = $program->getMsid();
        $params = $this->model->getParams($msid);
        $targetArray = $this->getTargets($batch, $tahun);

        $detail = array(
            'batch'   => $batch,
            'params'  => $params,
            'targets' => $targetArray
        );
        return $this->jsonResponse($detail, -1, $this->context_norels);
    }

    /**
     * @param BatchCsr $batch
     * @param int      $tahun
     *
     * @return array
     */
    private function getTargets(BatchCsr $batch, int $tahun)
    {
        $filterTarget = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'periodeBatch', 'value' => $batch],
                ['property' => 'tahun', 'value' => $tahun],
                ['property' => 'showInDashboard', 'value' => false]
            ]
        );

        $totalBeneficiary = $this->targetKpiRepository->findAllByCriteria(QueryExpressionHelper::createCriterias(
            [
                ['property' => 'periodeBatch', 'value' => $batch],
                ['property' => 'tahun', 'value' => $tahun],
                ['property' => 'showInDashboard', 'value' => true]
            ]
        ))->getResult();


        $conn = $this->getDoctrine()->getManager()->getConnection();

        $array = [];
        /* @var CapaianKpi $row */
        foreach ($totalBeneficiary as $row) {
            $code = $row->getDataRef()->getCode();
            $filters = QueryExpressionHelper::createCriterias(
                [
                    ['property' => 'periodeBatch', 'value' => $batch],
                    ['property' => 'tahun', 'value' => $tahun]
                ]
            );
            $indikatorArr = $this->viewIndikatorKpiRepository->findAllByCriteria($filters)->getResult();
            $tmpArr = [];
            /* @var ViewIndikator $kpis */
            foreach ($indikatorArr as $kpis) {
                $sql = "SELECT * FROM vw_kpi_indikator WHERE batch_id = :batch AND tahun = :tahun AND period = :period AND ref_code = :code";
                $stmt = $conn->prepare($sql);
                $stmt->execute(['tahun' => $tahun, 'batch' => $batch->getBatchId(), 'period' => $kpis->getPeriod(), 'code' => $code]);
                $result = $stmt->fetchAll();
                $tmpArr[$kpis->getPeriod()] = count($result);
            }
            $array[$code] = [
                'target' => @$row ? $row->getTarget() : 0,
                'period' => $tmpArr
            ];
        }

        $target = $this->targetKpiRepository->findAllByCriteria($filterTarget)->getResult();
        $tmpArray = [];
//        dd($target);
        /* @var CapaianKpi $targetKpi */
        foreach ($target as $targetKpi) {
            $code = $targetKpi->getDataRef()->getCode();
            $filters = QueryExpressionHelper::createCriterias(
                [
                    ['property' => 'periodeBatch', 'value' => $batch],
                    ['property' => 'tahun', 'value' => $tahun],
                    ['property' => 'dataRef', 'value' => $targetKpi->getDataRef()],
                ]
            );
            $indikator = $this->viewIndikatorKpiRepository->findAllByCriteria($filters)->getResult();
            $tmp = [];
            /* @var ViewIndikator $kpi */
            foreach ($indikator as $kpi) {
                $filters = QueryExpressionHelper::createCriterias(
                    [
                        ['property' => 'periodeBatch', 'value' => $batch],
                        ['property' => 'tahun', 'value' => $tahun],
                        ['property' => 'dataRef', 'value' => $targetKpi->getDataRef()->getId()],
                        ['property' => 'period', 'value' => $kpi->getPeriod()],
                    ]
                );

                $semester = $this->viewIndikatorKpiRepository->findAllByCriteria($filters)->getResult();
                $tmp[$kpi->getPeriod()] = count($semester);
            }

            $filtersVokasi = QueryExpressionHelper::createCriterias(
                [
                    ['property' => 'periodeBatch', 'value' => $batch],
                    ['property' => 'tahun', 'value' => $tahun]
                ]
            );
            $vokasi = $this->viewIndikatorKpiRepository->findAllByCriteria($filtersVokasi)->getResult();
            $vok = [];
            foreach ($vokasi as $kpi) {
                $vok[$kpi->getPeriod()] = $kpi->getVokasiParams()['ref']['code'][$code];
            }

            $tmpArray[$code] = [
                'target' => @$targetKpi ? $targetKpi->getTarget() : 0,
                'period' => $tmp,
                'vokasi' => $vok
            ];
        }
//        dd(array_merge($array, $tmpArray));
        return array_merge($array, $tmpArray);
    }

    /**
     * Menginisialisasi KPI Target baru untuk batch
     *
     * @Route("/populate-batch/{batch}", requirements={"batch":"\d+"}, methods={"POST"})
     *
     * @param BatchCsr $batch Persistent entity object of Batch pembinaan
     *
     * @return JsonResponse
     */
    public function populateBatchTargets(BatchCsr $batch): JsonResponse
    {
        $this->model->populateTargets($batch);
        $result = $this->model->getTargetsBatch($batch);

        // sebagai result, kembalikan daftar target yang sudah dicreate
        return $this->jsonResponse($result, count($result), $this->context_norels);
    }

    /**
     * Menyimpan target KPI untuk batch
     *
     * @Route("/save-targets/{batch}", requirements={"batch":"\d+","tahun":"\d+"}, methods={"POST"})
     *
     * @param Request  $request Http request
     * @param BatchCsr $batch   Persistent entity object of Batch pembinaan
     *
     * @return JsonResponse
     */
    public function massSaveTargets(Request $request, BatchCsr $batch): JsonResponse
    {
        $arrayKpi = $request->get('targets');

        if (is_array($arrayKpi)) {
            foreach ($arrayKpi as $kpi) {
                $capaianId = $kpi["id"];
                $capaianTarget = $kpi["target"];
                $entity = $this->capaianKpiRepository->find($capaianId);

                if (isset($entity)) {
                    $entity->setTarget($capaianTarget);
                    $this->model->saveTarget($batch, $entity);
                }
            }
        }
        $result = $this->model->getTargetsBatch($batch);

        // sebagai result, kembalikan daftar target yang sudah diupdate
        return $this->jsonResponse($result, count($result), $this->context_norels);
    }

    /**
     * Menyimpan target KPI untuk batch
     *
     * @Route("/save-capaian/{batch}/{tahun}", requirements={"batch":"\d+","tahun":"\d+"}, methods={"POST"})
     *
     * @param Request  $request Http request
     * @param BatchCsr $batch   Persistent entity object of Batch pembinaan
     * @param int      $tahun   Tahun anggaran
     *
     * @return JsonResponse
     */
    public function massSaveCapaian(Request $request, BatchCsr $batch, int $tahun): JsonResponse
    {
        $allCapaian = $request->get('capaian');

        if (is_array($allCapaian)) {
            foreach ($allCapaian as $key => $capaian) {
                $capaianId = $capaian["id"];
                $entity = $this->capaianKpiRepository->find($capaianId);

                if (isset($entity)) {
                    // Penangana khusus untuk total siswa di program Beasiswa
                    if ($key == 'KPI-04.001') {
                        $entity->setP1((int)$allCapaian["KPI-04.002"]["p1"] + (int)$allCapaian["KPI-04.003"]["p1"]);
                        $entity->setP2((int)$allCapaian["KPI-04.002"]["p2"] + (int)$allCapaian["KPI-04.003"]["p2"]);
                    } else {
                        $entity->setP1($capaian["p1"]);
                        $entity->setP2($capaian["p2"]);
                    }
                    $entity->setP3($capaian["p3"]);
                    $entity->setP4($capaian["p4"]);

                    if ($entity->getTarget() != null) {
                        if ($entity->getProgramCsr()->getMsid() == 1 || $entity->getProgramCsr()->getMsid() == 4) {
                            if ($entity->getP4() != null) {
                                $entity->setNilai($entity->getP4());
                            } else if ($entity->getP3() != null) {
                                $entity->setNilai($entity->getP3() / $entity->getTarget());
                            } else if ($entity->getP2() != null) {
                                $entity->setNilai($entity->getP2() / $entity->getTarget());
                            } else if ($entity->getP1() != null) {
                                $entity->setNilai($entity->getP1() / $entity->getTarget());
                            } else {
                                $entity->setNilai($capaian["nilai"]);
                            }
                        } else {
                            if ($entity->getP4() != null) {
                                $entity->setNilai(($entity->getP4() / $entity->getTarget()) * 100);
                            } else if ($entity->getP3() != null) {
                                $entity->setNilai(($entity->getP3() / $entity->getTarget()) * 100);
                            } else if ($entity->getP2() != null) {
                                $entity->setNilai(($entity->getP2() / $entity->getTarget()) * 100);
                            } else if ($entity->getP1() != null) {
                                $entity->setNilai(($entity->getP1() / $entity->getTarget()) * 100);
                            } else {
                                $entity->setNilai($capaian["nilai"]);
                            }
                        }

                    }

                    if ($entity->getProgramCsr()->getMsid() == 1 || $entity->getProgramCsr()->getMsid() == 4) {
                        if ($entity->getP4() != null) {
                            $entity->setNilaiAkhir($entity->getP4());
                        } else if ($entity->getP3() != null) {
                            $entity->setNilaiAkhir($entity->getP3());
                        } else if ($entity->getP2() != null) {
                            $entity->setNilaiAkhir($entity->getP2());
                        } else if ($entity->getP1() != null) {
                            $entity->setNilaiAkhir($entity->getP1());
                        }
                    } else {
                        if (isset ($capaian["nilaiAkhir"])) {
                            $entity->setNilaiAkhir($capaian["nilaiAkhir"]);
                        }

                    }


                    $this->model->saveTarget($batch, $entity);

                }
            }
        }
        $result = $this->model->getTargets($batch, $tahun);

        // sebagai result, kembalikan daftar capaian yang sudah diupdate
        return $this->jsonResponse($result, count($result), $this->context_norels);
    }

    /**
     * Memproses verifikasi capaian KPI untuk periode, tahun dan batch tertentu.
     *
     * @Route("/verify-capaian/{batch}/{tahun}/{periode}", requirements={"batch":"\d+","tahun":"\d+", "periode":"\d"},
     *                                                     methods={"POST"})
     *
     * @param BatchCsr $batch   Persistent entity object of Batch pembinaan
     * @param int      $tahun   Tahun anggaran
     * @param int      $periode Periode pelaksanaan untuk triwulan/caturwulan/semester ke berapa
     *
     * @return JsonResponse
     */
    public function verifyCapaian(BatchCsr $batch, int $tahun, int $periode): JsonResponse
    {
        //Get data parameter dari batch
        try {
            $target = $this->model->getTargets($batch, $tahun);
            $dataCapaianPeriode = [];

            //Cek setiap parameter apakah diset atau tidak
            foreach ($target as $data) {
                if ($periode == 1) {
                    $dataCapaianPeriode[] = $data->getP1();
                } else if ($periode == 2) {
                    $dataCapaianPeriode[] = $data->getP2();
                } else if ($periode == 3) {
                    $dataCapaianPeriode[] = $data->getP3();
                } else if ($periode == 4) {
                    $dataCapaianPeriode[] = $data->getP4();
                }
            }

            if (!in_array(null, $dataCapaianPeriode, true)) {
                $this->model->verifyCapaian($batch, $tahun, $periode);
                return $this->jsonSuccess('Data Capaian KPI berhasil diverifikasi.');
            } else {
                return $this->jsonError('Proses verifikasi belum bisa dilakukan.', JsonResponse::HTTP_FORBIDDEN);
            }
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Memproses verifikasi capaian KPI khusus data kuantitas untuk periode, tahun dan batch tertentu.
     *
     * @Route("/verify-capaian-kuantitas/{batch}/{tahun}/{periode}",
     *     requirements={"batch":"\d+","tahun":"\d+", "periode":"\d"}, methods={"POST"})
     *
     * @param BatchCsr $batch   Persistent entity object of Batch pembinaan
     * @param int      $tahun   Tahun anggaran
     * @param int      $periode Periode pelaksanaan untuk triwulan/caturwulan/semester ke berapa
     *
     * @return JsonResponse
     */
    public function verifyCapaianKuantitas(BatchCsr $batch, int $tahun, int $periode): JsonResponse
    {
        //Get data parameter dari batch
        try {
            $target = $this->model->getTargets($batch, $tahun);
            $dataCapaianPeriode = [];

            //Cek setiap parameter apakah diset atau tidak
            foreach ($target as $data) {
                if ($data->getKuantitas()) {
                    if ($periode == 1) {
                        $dataCapaianPeriode[] = $data->getP1();
                    } else if ($periode == 2) {
                        $dataCapaianPeriode[] = $data->getP2();
                    } else if ($periode == 3) {
                        $dataCapaianPeriode[] = $data->getP3();
                    } else if ($periode == 4) {
                        $dataCapaianPeriode[] = $data->getP4();
                    }
                }
            }

            if (!in_array(null, $dataCapaianPeriode, true)) {
                $this->model->verifyCapaianKuantitas($batch, $tahun, $periode);
                return $this->jsonSuccess('Data Capaian KPI berhasil diverifikasi.');
            } else {
                return $this->jsonError('Proses verifikasi belum bisa dilakukan.', JsonResponse::HTTP_FORBIDDEN);
            }
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Memproses verifikasi capaian KPI khusus data kualitas untuk periode, tahun dan batch tertentu.
     *
     * @Route("/verify-capaian-kualitas/{batch}/{tahun}/{periode}",
     *     requirements={"batch":"\d+","tahun":"\d+", "periode":"\d"}, methods={"POST"})
     *
     * @param BatchCsr $batch   Persistent entity object of Batch pembinaan
     * @param int      $tahun   Tahun anggaran
     * @param int      $periode Periode pelaksanaan untuk triwulan/caturwulan/semester ke berapa
     *
     * @return JsonResponse
     */
    public function verifyCapaianKualitas(BatchCsr $batch, int $tahun, int $periode): JsonResponse
    {
        //Get data parameter dari batch
        try {
            $target = $this->model->getTargets($batch, $tahun);
            $dataCapaianPeriode = [];

            //Cek setiap parameter apakah diset atau tidak
            foreach ($target as $data) {
                if (!$data->getKuantitas()) {
                    if ($periode == 1) {
                        $dataCapaianPeriode[] = $data->getP1();
                    } else if ($periode == 2) {
                        $dataCapaianPeriode[] = $data->getP2();
                    } else if ($periode == 3) {
                        $dataCapaianPeriode[] = $data->getP3();
                    } else if ($periode == 4) {
                        $dataCapaianPeriode[] = $data->getP4();
                    }
                }
            }

            if (!in_array(null, $dataCapaianPeriode, true)) {
                $this->model->verifyCapaianKualitas($batch, $tahun, $periode);
                return $this->jsonSuccess('Data Capaian KPI berhasil diverifikasi.');
            } else {
                return $this->jsonError('Proses verifikasi belum bisa dilakukan.', JsonResponse::HTTP_FORBIDDEN);
            }
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Memproses verifikasi capaian KPI untuk seluruh periode pada tahun dan batch tertentu.
     *
     * @Route("/mass-verify-capaian/{batch}/{tahun}/{totalPeriode}",
     *     requirements={"batch":"\d+","tahun":"\d+", "periode":"\d"}, methods={"POST"})
     *
     * @param BatchCsr $batch        Persistent entity object of Batch pembinaan
     * @param int      $tahun        Tahun anggaran
     * @param int      $totalPeriode Jumlah Periode pada batch
     *
     * @return JsonResponse
     */
    public function massVerifyCapaian(BatchCsr $batch, int $tahun, int $totalPeriode): JsonResponse
    {
        $issetAllData = true;
        $target = $this->model->getTargets($batch, $tahun);

        foreach ($target as $data) {
            $msidCsr = $data->getProgramCsr()->getMsid();

            if ($msidCsr === 1 || $msidCsr === 3) {
                if ($data->getP1() === null || $data->getP2() === null || $data->getP3() === null || $data->getP4() === null) {
                    $issetAllData = false;
                }
            } else {
                if ($data->getP1() === null || $data->getP2() === null) {
                    $issetAllData = false;
                }
            }
        }

        if (!$issetAllData) {
            return $this->jsonError('Proses verifikasi belum bisa dilakukan.', JsonResponse::HTTP_FORBIDDEN);
        } else {
            for ($periode = 1; $periode <= $totalPeriode; ++$periode) {
                $this->model->verifyCapaian($batch, $tahun, $periode);
            }

            return $this->jsonSuccess('Data Capaian KPI berhasil diverifikasi.');
        }
    }

    // =============== Fungsi pengelolaan narasi statistik Capaian KPI ============

    /**
     * Get Statistk Capaian untuk tahun batch terentu.
     *
     * @Route("/fetch-batch-tahun/{batch}/{tahun}", requirements={"batch":"\d+","tahun":"\d+"}, methods={"GET"})
     *
     * @param BatchCsr $batch
     * @param int      $tahun
     *
     * @return JsonResponse
     */
    public function getStatistikTahun(BatchCsr $batch, int $tahun) : JsonResponse {

        $statistik = $this->model->getStatistik($batch, $tahun);
        return $this->jsonResponse($statistik, -1, $this->context_norels);
    }


    /**
     * Set Statistk Capaian untuk tahun batch terentu.
     *
     * @Route("/update-batch-tahun/{batch}/{tahun}", requirements={"batch":"\d+","tahun":"\d+"}, methods={"POST"})
     *
     * @param Request  $request
     * @param BatchCsr $batch
     * @param int      $tahun
     *
     * @return JsonResponse
     */
    public function updateStatistikTahun(Request $request, BatchCsr $batch, int $tahun) : JsonResponse {
        $narasi = $request->get("narasi");
        $statistik = $this->model->saveStatistikNarasi($batch, $tahun, $narasi);
        return $this->jsonResponse($statistik, -1, $this->context_norels);
    }


}
