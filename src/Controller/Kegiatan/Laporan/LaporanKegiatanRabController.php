<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Kegiatan\Laporan;

use App\Component\Controller\SpreadsheetController;
use App\Entity\Kegiatan\Kegiatan;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LaporanKegiatanRabController
 *
 * @package App\Controller\Kegiatan\Laporan
 * @author  Mark Melvin
 * @since   15/11/2019
 *
 * @Route("/app/kegiatan/laporan")
 */
class LaporanKegiatanRabController extends SpreadsheetController
{

    /**
     * @Route("/")
     * @return Response
     */
    function index(){
        return $this->jsonError("Kegiatan tidak ditemukan");
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param Kegiatan $kegiatan
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_header_kegiatan(Spreadsheet $spreadsheet, Kegiatan $kegiatan): Spreadsheet
    {
        // Add some data
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('C3', $kegiatan->getDanaBatch()->getTitle())
            ->setCellValue('C4', $kegiatan->getNamaKegiatan())
            ->setCellValue('C5', $kegiatan->getPeriodeBatch()->getNamaBatch())
            ->setCellValue('C6', $kegiatan->getAnggaran());
        $spreadsheet->getActiveSheet()->getStyle('C6')->getNumberFormat()
            ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
        $spreadsheet->getActiveSheet()
            ->mergeCells('C3:K3')
            ->mergeCells('C4:K4')
            ->mergeCells('C5:K5')
            ->mergeCells('C6:E6');


        return $spreadsheet;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param Kegiatan $kegiatan
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_laporan_kegiatan_full(Spreadsheet $spreadsheet,
                                                    &$row,
                                                    Kegiatan $kegiatan) : Spreadsheet
    {
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(11, 12);

        $totalSub = 0.0;
        $subKegiatan = $kegiatan->getChildren();
        foreach ($subKegiatan as $sub) {
            // Nama subkegiatan
            $richText = new RichText();
            //$richText->createText('This invoice is ');
            $namaSubKegiatan = $richText->createTextRun($sub->getTitle());
            $namaSubKegiatan->getFont()->setBold(true);
            $richText->createText(', '.
                $sub->getTanggalMulai()->format('d-m-Y').
                ' s/d '.
                $sub->getTanggalAkhir()->format('d-m-Y').
                '.');
            $spreadsheet->getActiveSheet()->getCell('A'.$row)->setValue($richText)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getCell('A'.$row)
                ->getStyle()->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->mergeCells('A'.$row.':J'.$row);
            $spreadsheet->getActiveSheet()->getCell('K'.$row)->setValue($sub->getAnggaran())
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('K'.$row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
            $spreadsheet->getActiveSheet()->getStyle('K'.$row)->getFont()->setBold(true);
            $totalSub += $sub->getAnggaran();
            $row++;

            $itemrabs = $sub->getChildren();
            foreach ($itemrabs as $item) {
                $spreadsheet->getActiveSheet()->getCell('A'.$row)
                    ->setValue($item->getAccount()->getCode())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getCell('B'.$row)
                    ->setValue($item->getAccount()->getName())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getCell('B'.$row)
                    ->getStyle()->getAlignment()->setWrapText(true);
                $spreadsheet->getActiveSheet()->getCell('C'.$row)
                    ->setValue($item->getTitle())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getCell('D'.$row)
                    ->setValue($item->getJumlahQty1())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_NUMBER);
                $spreadsheet->getActiveSheet()->getCell('E'.$row)
                    ->setValue($item->getUnitQty1())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getCell('F'.$row)
                    ->setValue($item->getJumlahQty2())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('F'.$row)->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_NUMBER);
                $spreadsheet->getActiveSheet()->getCell('G'.$row)
                    ->setValue($item->getUnitQty2())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getCell('H'.$row)
                    ->setValue($item->getUnitPrice())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('H'.$row)->getNumberFormat()
                    ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
                $spreadsheet->getActiveSheet()->getCell('I'.$row)
                    ->setValue($item->getFrequency())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('I'.$row)->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_NUMBER);
                $spreadsheet->getActiveSheet()->getCell('J'.$row)
                    ->setValue($item->getUnitFrequency())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getCell('K'.$row)
                    ->setValue('=D'.$row.'*F'.$row.'*H'.$row.'*I'.$row);
                $spreadsheet->getActiveSheet()->getCell('K'.$row)
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('K'.$row)->getNumberFormat()
                    ->setFormatCode(self::FORMAT_ACCOUNTING_RP);

                $row++;
            }


        }
        // Total kegiatan
        $spreadsheet->getActiveSheet()->getCell('A'.$row)->setValue('JUMLAH TOTAL')
            ->getStyle()->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->mergeCells('A'.$row.':J'.$row)
            ->getStyle('A'.$row.':J'.$row)
            ->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
        $spreadsheet->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('K'.$row)
            ->setValue($totalSub)
            ->getStyle()->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getCell('K'.$row)
            ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
        $spreadsheet->getActiveSheet()->getStyle('K'.$row)->getNumberFormat()
            ->setFormatCode(self::FORMAT_ACCOUNTING_RP);

        return $spreadsheet;
    }

    /**
     * Menampilkan detail record Kegiatan.
     *
     * @Route("/full/{id}", name="kegiatan_excel_full", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param Kegiatan $entity Persistent entity object of Kegiatan
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @return Response
     */
    public function excel_full(?Kegiatan $entity): Response
    {
        if (empty($entity)) {
            return $this->jsonError("Kegiatan tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("kegiatan_full.xlsx");

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Detail Kegiatan')
            ->setSubject('Detail Kegiatan '.$entity->getNamaKegiatan())
            ->setDescription('Laporan Kegiatan dan RAB.')
            ->setKeywords('detail kegiatan')
            ->setCategory($entity->getKode());

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BDetail Kegiatan&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');

        $spreadsheet = $this->generate_header_kegiatan($spreadsheet, $entity);

        $row = 13;
        $spreadsheet = $this->generate_laporan_kegiatan_full($spreadsheet, $row, $entity);

        $spreadsheet->getActiveSheet()->setTitle($entity->getKode());
        $spreadsheet->getActiveSheet()->setShowGridLines(true);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        //IOFactory::registerWriter('Pdf', \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class);
        return $this->streamResponse($spreadsheet, 'Laporan RAB '.$entity->getKode(), 'Xlsx');
    }

    /**
     * Pencetakan detail record Kegiatan.
     *
     * @Route("/printfull/{id}", name="kegiatan_print_full", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param Kegiatan $entity Persistent entity object of Kegiatan
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @return Response
     */
    public function print_full(?Kegiatan $entity): Response
    {
        if (empty($entity)) {
            return $this->jsonError("Kegiatan tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("kegiatan_full.xlsx");

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Detail Kegiatan')
            ->setSubject('Detail Kegiatan '.$entity->getNamaKegiatan())
            ->setDescription('Laporan Kegiatan dan RAB.')
            ->setKeywords('detail kegiatan')
            ->setCategory($entity->getKode());

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BDetail Kegiatan&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_kegiatan($spreadsheet, $entity);

        $row = 13;
        $spreadsheet = $this->generate_laporan_kegiatan_full($spreadsheet, $row, $entity);


        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($entity->getKode());
        $spreadsheet->getActiveSheet()->setShowGridLines(false);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        return $this->streamResponse($spreadsheet, 'Laporan RAB '.$entity->getKode(), 'pdf');
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param Kegiatan $kegiatan
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_laporan_kegiatan_rekap(Spreadsheet $spreadsheet,
                                                    &$row,
                                                    Kegiatan $kegiatan) : Spreadsheet
    {
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(9, 10);
        $totalSub = 0.0;
        $subKegiatan = $kegiatan->getChildren();
        foreach ($subKegiatan as $sub) {

            $spreadsheet->getActiveSheet()->getCell('A'.$row)
                ->setValue($sub->getTanggalMulai()->format('d-m-Y'))
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getCell('B'.$row)
                ->setValue($sub->getTanggalAkhir()->format('d-m-Y'))
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getCell('C'.$row)
                ->setValue($sub->getTitle())
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getCell('C'.$row)
                ->getStyle()->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getCell('D'.$row)
                ->setValue($sub->getAnggaran())
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
            $spreadsheet->getActiveSheet()->getCell('E'.$row)
                ->setValue($sub->getAnggaran())
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
            $totalSub += $sub->getAnggaran();
            $row++;

        }
        // Total kegiatan
        $spreadsheet->getActiveSheet()->getCell('A'.$row)->setValue('JUMLAH TOTAL')
            ->getStyle()->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle('A'.$row)
            ->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
        $spreadsheet->getActiveSheet()->getCell('E'.$row)->setValue($totalSub)
            ->getStyle()->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getCell('E'.$row)
            ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
        $spreadsheet->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()
            ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
        $spreadsheet->getActiveSheet()
            ->getStyle('A'.$row.':E'.$row)
            ->getBorders()->getOutline()->setBorderStyle(Border::BORDER_THIN);

        return $spreadsheet;
    }



    /**
     * Menampilkan detail rekap Kegiatan.
     *
     * @Route("/rekap/{id}", name="kegiatan_excel_rekap", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param Kegiatan $entity Persistent entity object of Kegiatan
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @return Response
     */
    public function excel_rekap(?Kegiatan $entity): Response
    {
        if (empty($entity)) {
            return $this->jsonError("Kegiatan tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("kegiatan_rekap.xlsx");

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Rekap Kegiatan')
            ->setSubject('Rekap Kegiatan '.$entity->getNamaKegiatan())
            ->setDescription('Rekap Kegiatan dan RAB.')
            ->setKeywords('rekap kegiatan')
            ->setCategory($entity->getKode());

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BRekap Kegiatan&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');

        $spreadsheet = $this->generate_header_kegiatan($spreadsheet, $entity);

        $row = 11;
        $spreadsheet = $this->generate_laporan_kegiatan_rekap($spreadsheet, $row, $entity);

        $spreadsheet->getActiveSheet()->setTitle($entity->getKode());
        $spreadsheet->getActiveSheet()->setShowGridLines(true);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        //IOFactory::registerWriter('Pdf', \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class);
        return $this->streamResponse($spreadsheet, 'Rekap Kegiatan '.$entity->getKode(), 'Xlsx');
    }

    /**
     * Pencetakan detail rekap Kegiatan.
     *
     * @Route("/printrekap/{id}", name="kegiatan_print_rekap", methods={"GET"}, requirements={"id":"\d+"})
     *
     * @param Kegiatan $entity Persistent entity object of Kegiatan
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @return Response
     */
    public function print_rekap(?Kegiatan $entity): Response
    {
        if (empty($entity)) {
            return $this->jsonError("Kegiatan tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("kegiatan_rekap.xlsx");

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Rekap Kegiatan')
            ->setSubject('Rekap Kegiatan '.$entity->getNamaKegiatan())
            ->setDescription('Rekap Kegiatan dan RAB.')
            ->setKeywords('rekap kegiatan')
            ->setCategory($entity->getKode());

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BRekap Kegiatan&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_kegiatan($spreadsheet, $entity);

        $row = 11;
        $spreadsheet = $this->generate_laporan_kegiatan_rekap($spreadsheet, $row, $entity);


        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($entity->getKode());
        $spreadsheet->getActiveSheet()->setShowGridLines(false);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        return $this->streamResponse($spreadsheet, 'Rekap Kegiatan '.$entity->getKode(), 'pdf');
    }

}
