<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */


namespace App\Controller\RiwayatPembayaran;

use App\Component\Controller\SpreadsheetController;
use App\Entity\MasterData\ProgramCSR;
use App\Entity\Pencairan\Pencairan;
use App\Models\Kegiatan\RiwayatPembayaranModel;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PembayaranReportController
 *
 * @package App\Controller\RiwayatPembayaran
 * @author  Mark Melvin
 * @since   18/05/2020
 *
 * @Route("/app/riwayatpembayaran")
 */
class PembayaranReportController extends SpreadsheetController
{

    /** @var RiwayatPembayaranModel */
    private $model;

    /**
     * PembayaranReportController constructor.
     * @param RiwayatPembayaranModel $model
     */
    public function __construct(RiwayatPembayaranModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param ProgramCSR $program
     * @param int $tahun
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_header_pembayaran(Spreadsheet $spreadsheet, ProgramCSR $program, int $tahun): Spreadsheet
    {
        // Add some data
        $spreadsheet->setActiveSheetIndex(0)->setCellValue( 'C3', $program->getName());
        $spreadsheet->getActiveSheet()->setCellValue('C4', $tahun.' ');

        return $spreadsheet;
    }



    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param array $dataItems
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_laporan_pembayaran(Spreadsheet $spreadsheet,
                                                 int &$row,
                                                 array $dataItems) : Spreadsheet
    {
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 5);

        $counter = 1;


        /** @var Pencairan $item */
        foreach ($dataItems as $item) {
            $spreadsheet->getActiveSheet()->getCell('A' . $row)->setValue($counter++.'. ');
            $spreadsheet->getActiveSheet()->getCell('A' . $row)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);

            if ($item->getIsExpense()) {
                $spreadsheet->getActiveSheet()->getCell('B' . $row)->setValue('EXPENSE');
            }
            $firstItem = null;
            $children = $item->getChildren();
            if ($children->count() > 0){
                $firstItem = $children[0];
            }

            if ($firstItem != null) {
                $spreadsheet->getActiveSheet()->getCell('B' . $row)->setValue($firstItem->setTitle());
            }
            $spreadsheet->getActiveSheet()->getCell('B' . $row)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getCell('B' . $row)
                ->getStyle()->getAlignment()->setWrapText(true);

            if ($firstItem != null) {
                $spreadsheet->getActiveSheet()->getCell('C' . $row)->setValue($firstItem->getAmount());
            }
            $spreadsheet->getActiveSheet()->getCell('C' . $row)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);

            $spreadsheet->getActiveSheet()->getCell('D' . $row)->setValue($item->getTipeTermin())
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getCell('D' . $row)
                ->getStyle()->getAlignment()->setWrapText(true);

            $spreadsheet->getActiveSheet()->getCell('E' . $row)->setValue($item->getPersen()/100)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);

            $spreadsheet->getActiveSheet()->getCell('F' . $row)->setValue($item->getCreatedDate()->format('d M Y'))
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);

            $spreadsheet->getActiveSheet()->getCell('G' . $row)->setValue($item->getNomorSurat())
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);

            $spreadsheet->getActiveSheet()->getCell('H' . $row)->setValue($item->getTotalPencairan())
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('H' . $row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);

            if ($item->getPaidDate() != null) {
                $spreadsheet->getActiveSheet()->getCell('I' . $row)->setValue($item->getPaidDate()->format('d M Y'));
            }
            $spreadsheet->getActiveSheet()->getCell('I' . $row)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);

            $spreadsheet->getActiveSheet()->getCell('J' . $row)->setValue($item->getNomorVoucherBdvc())
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getCell('J' . $row)
                ->getStyle()->getAlignment()->setWrapText(true);

            if (($firstItem->getKegiatan() != null) && ($firstItem->getKegiatan()->getTanggalPostedRealisasi() != null)) {
                $spreadsheet->getActiveSheet()->getCell('K' . $row)->setValue($firstItem->getKegiatan()->getTanggalPostedRealisasi()->format('d M Y'));
            }
            $spreadsheet->getActiveSheet()->getCell('K' . $row)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);

            if (($firstItem->getKegiatan() != null)) {
                $spreadsheet->getActiveSheet()->getCell('L' . $row)->setValue($firstItem->getKegiatan()->getRealisasi());
            }
            $spreadsheet->getActiveSheet()->getCell('L' . $row)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('L' . $row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);

            if (($firstItem->getKegiatan() != null)) {
                $spreadsheet->getActiveSheet()->getCell('M' . $row)->setValue($firstItem->getKegiatan()->getAnggaran() - $firstItem->getKegiatan()->getRealisasi());
            }
            $spreadsheet->getActiveSheet()->getCell('M' . $row)
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle('M' . $row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);

            $row++;
        }
        return $spreadsheet;
    }

     /**
     * Menyusun daftar Riwayat Pembayaran
     * @Route("/full/{msid}/{tahun}",
     *     methods={"GET"},
     *     requirements={"msid":"\d+","tahun":"\d{0,6}"})
     *
     * @param ProgramCSR $program
     * @param int $tahun
     * @throws Exception
     * @return Response
     */
    public function reportFull(ProgramCSR $program, int $tahun) : Response
    {
        $dataItems = $this->model->displayByProgramYear($program, $tahun);


        // Create new Spreadsheet object
        $spreadsheet = $this->load("pembayaran.xlsx");

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Riwayat Pembayaran')
            ->setSubject('Riwayat Pembayaran')
            ->setDescription('Riwayat Pembayaran')
            ->setKeywords('riwayat pembayaran')
            ->setCategory('Riwayat Pembayaran');

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BRiwayat Pembayaran&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        // Set page orientation and size
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_pembayaran($spreadsheet, $program, $tahun);

        $row = 7;
        $spreadsheet = $this->generate_laporan_pembayaran($spreadsheet, $row, $dataItems);

        $spreadsheet->getActiveSheet()->setTitle('Riwayat Pembayaran');
        $spreadsheet->getActiveSheet()->setShowGridLines(true);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        return $this->streamResponse($spreadsheet,
            'Riwayat Pembayaran'.' '.$program->getName().' '.$tahun, 'Xlsx');

    }

    /**
     * Menyusun daftar Riwayat Pembayaran
     * @Route("/printfull/{msid}/{tahun}",
     *     methods={"GET"},
     *     requirements={"msid":"\d+","tahun":"\d{0,6}"})
     *
     * @param ProgramCSR $program
     * @param int $tahun
     * @throws Exception
     * @return Response
     */
    public function print_full(ProgramCSR $program, int $tahun): Response
    {
        $dataItems = $this->model->displayByProgramYear($program, $tahun);

        if (empty($dataItems)) {
            return $this->jsonError("Data tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("pembayaran.xlsx");

        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Riwayat Pembayaran')
            ->setSubject('Riwayat Pembayaran '.$program->getName())
            ->setDescription('Riwayat Pembayaran')
            ->setKeywords('riwayat pembayaran')
            ->setCategory('Riwayat Pembayaran');

        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BRiwayat Pembayaran&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        // Set page orientation and size
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_pembayaran($spreadsheet, $program, $tahun);
        $row = 7;
        $spreadsheet = $this->generate_laporan_pembayaran($spreadsheet, $row, $dataItems);

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Riwayat Pembayaran');
        $spreadsheet->getActiveSheet()->setShowGridLines(false);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        return $this->streamResponse($spreadsheet,
            'Riwayat Pembayaran'.' '.$program->getName().' '.$tahun, 'pdf');
    }



}