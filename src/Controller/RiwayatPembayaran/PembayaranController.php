<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */


namespace App\Controller\RiwayatPembayaran;

use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\MasterData\ProgramCSR;
use App\Models\Kegiatan\MonitoringModel;
use App\Models\Kegiatan\RiwayatPembayaranModel;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Class PembayaranController
 *
 * @package App\Controller\RiwayatPembayaran
 * @author  Mark Melvin
 * @since   18/05/2020
 *
 * @Route("/app/riwayatpembayaran")
 */

class PembayaranController extends JsonController
{

    /** @var RiwayatPembayaranModel */
    private $model;

    /**
     * PembayaranController constructor.
     * @param RiwayatPembayaranModel $model
     */
    public function __construct(RiwayatPembayaranModel $model)
    {
        $this->model = $model;
    }


    /**
     * @Route("/{msid}/{tahun}", methods={"GET"}, requirements={"msid":"\d+","tahun":"\d{0,6}"})
     *
     * @param ProgramCSR $program
     * @param int $tahun
     * @return JsonResponse
     * @throws
     */
    public function index(ProgramCSR $program, int $tahun): JsonResponse
    {
        $result = $this->model->displayByProgramYear($program, $tahun);

        return $this->jsonResponse($result, count($result), $this->context_withrels);
    }

}