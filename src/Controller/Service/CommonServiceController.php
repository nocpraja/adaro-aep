<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Service;


use App\Component\Controller\JsonController;
use App\Entity\Security\UserAccount;
use App\Models\Security\AccessRightsModel;
use App\Repository\MasterData\DataReferenceRepository;
use App\Repository\MasterData\ProgramCSRRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CommonServiceController
 *
 * @package App\Controller\Service
 * @author  Ahmad Fajar
 * @since   28/09/2018, modified: 11/04/2019 14:07
 *
 * @Route("/app/service")
 */
class CommonServiceController extends JsonController
{

    /**
     * @var AccessRightsModel
     */
    private $aclModel;

    /**
     * @var DataReferenceRepository
     */
    private $dataRefRepo;

    /**
     * @var ProgramCSRRepository
     */
    private $programCsrRepo;


    /**
     * CommonServiceController constructor.
     *
     * @param AccessRightsModel       $model          AccessRights domain model
     * @param DataReferenceRepository $dataRefRepo    Entity repository instance
     * @param ProgramCSRRepository    $programCsrRepo Entity repository instance
     */
    public function __construct(AccessRightsModel $model,
                                DataReferenceRepository $dataRefRepo,
                                ProgramCSRRepository $programCsrRepo)
    {
        $this->aclModel = $model;
        $this->dataRefRepo = $dataRefRepo;
        $this->programCsrRepo = $programCsrRepo;
    }

    /**
     * Menampilkan daftar hak akses User yang sedang aktif.
     *
     * @Route("/access-rights", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function accessRights(): JsonResponse
    {
        $userObj = $this->getUser();

        if ($userObj instanceof UserAccount) {
            $menus = $this->aclModel->loadNavigationMenus($userObj->getGroup());
            $roles = $userObj->getRoles();

            return $this->jsonResponse(['lists' => $menus, 'roles' => $roles],
                                       count($menus), $this->context_norels);
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * Menampilkan daftar perusahaan dalam organisasi ADARO.
     *
     * @Route("/list/company", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function adaroCompany(): JsonResponse
    {
        $result = $this->dataRefRepo->findOneBy(['code' => 'COMPANY']);
        $children = $result->getChildren();

        return $this->jsonResponse($children, count($children), $this->context_norels);
    }

    /**
     * Menampilkan daftar Program CSR.
     *
     * @Route("/list/program-csr", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function programCsr(): JsonResponse
    {
        $query = $this->programCsrRepo->findAllByCriteria();
        $result = $query->getResult();

        return $this->jsonResponse($result, count($result), $this->context_norels);
    }

    /**
     * Menampilkan daftar Data Reference berdasarkan kode tertentu.
     *
     * @Route("/list/reference/{code}", methods={"GET"})
     *
     * @param string $code
     *
     * @return JsonResponse
     */
    public function listReference(string $code): JsonResponse
    {
        $parent = $this->dataRefRepo->findOneBy(['code' => strtoupper($code)]);
        $children = $parent->getChildren();

        return $this->jsonResponse($children, $children->count(), $this->context_norels);
    }

}