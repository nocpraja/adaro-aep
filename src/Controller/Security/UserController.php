<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Security;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Security\UserAccount;
use App\Form\Security\UserAccountType;
use App\Repository\MasterData\MitraRepository;
use App\Repository\Security\UserAccountRepository;
use App\Repository\Security\UserGroupRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 *
 * @package App\Controller\Security
 * @author  Ahmad Fajar
 * @since   18/07/2018, modified: 25/06/2019 3:48
 *
 * @Route("/app/security/user")
 */
class UserController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var UserAccountRepository
     */
    private $repository;

    /**
     * @var UserGroupRepository
     */
    private $groupRepository;

    /**
     * @var MitraRepository
     */
    private $mitraRepository;

    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;


    /**
     * UserController constructor.
     *
     * @param UserAccountRepository        $repository      Entity repository instance
     * @param UserGroupRepository          $groupRepository Entity repository instance
     * @param MitraRepository              $mitraRepository Entity repository instance
     * @param AuditLogger                  $auditLogger     AuditLogger service
     * @param UserPasswordEncoderInterface $passwordEncoder Password encryption service
     */
    public function __construct(UserAccountRepository $repository, UserGroupRepository $groupRepository,
                                MitraRepository $mitraRepository, AuditLogger $auditLogger,
                                UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->repository = $repository;
        $this->groupRepository = $groupRepository;
        $this->mitraRepository = $mitraRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->logger = $auditLogger;
    }

    /**
     * Menampilkan semua daftar User.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset(),
                                                      $dq->getCondition());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(),
                                   [self::CONTEXT_WITH_REL, 'with_profile']);
    }

    /**
     * Menambahkan User baru ke dalam database.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     * @throws ORMException
     */
    public function create(Request $request): JsonResponse
    {
        $form = $this->createForm(UserAccountType::class, new UserAccount(),
                                  ['group_repo' => $this->groupRepository, 'mitra_repo' => $this->mitraRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $entity = $form->getData();
        $password = $this->passwordEncoder->encodePassword($entity, $entity->getUserPassword());
        $entity->setPassword($password);
        $this->repository->save($entity);
        $this->logger->getEntityManager()->flush();
        $this->logger->info('TAMBAH akun pengguna baru.', UserAccount::class,
                            $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, [self::CONTEXT_WITH_REL, 'with_profile']);
    }

    /**
     * Menampilkan single record User.
     *
     * @Route("/view/{uid}", methods={"GET"}, requirements={"uid":"\d+"})
     *
     * @param UserAccount|null $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function fetch(?UserAccount $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, [self::CONTEXT_WITH_REL, 'with_profile']);
    }

    /**
     * Mengupdate record User yang sudah ada.
     *
     * @Route("/update/{uid}", methods={"POST"}, requirements={"uid":"\d+"})
     *
     * @param Request          $request HTTP Request
     * @param UserAccount|null $entity  Persistent entity object
     *
     * @return JsonResponse
     * @throws ORMException
     */
    public function update(Request $request, ?UserAccount $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(UserAccountType::class, $entity,
                                  ['group_repo' => $this->groupRepository, 'mitra_repo' => $this->mitraRepository]);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        if (!empty($entity->getUserPassword())) {
            $password = $this->passwordEncoder->encodePassword($entity, $entity->getUserPassword());
            $entity->setPassword($password);
        } else {
            $entity->setPassword($entity->getPassword());
        }

        $this->repository->save($entity);
        $this->logger->info('UPDATE akun pengguna.', UserAccount::class, $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, [self::CONTEXT_WITH_REL, 'with_profile']);
    }

    /**
     * Menghapus single record User.
     *
     * @Route("/delete/{uid}", methods={"DELETE"}, requirements={"uid":"\d+"})
     *
     * @param UserAccount|null $entity Persistent entity object
     *
     * @return JsonResponse
     * @throws ORMException
     */
    public function delete(?UserAccount $entity): JsonResponse
    {
        if (!$entity) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $this->repository->remove($entity);
        $this->logger->notice('HAPUS akun pengguna.', UserAccount::class, $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonSuccess('Data pengguna telah berhasil dihapus dari database');
    }

}
