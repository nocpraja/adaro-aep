<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Security;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Security\UserGroup;
use App\Form\Security\UserGroupType;
use App\Repository\Security\UserGroupRepository;
use App\Service\Logger\AuditLogger;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @package App\Controller\Security
 * @author  Mark Melvin
 * @since   28/07/2018, modified: 18/05/2019 13:32
 *
 * @Route("/app/security/group")
 */
class GroupController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var UserGroupRepository
     */
    private $repository;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * GroupController constructor.
     *
     * @param UserGroupRepository $repository Entity repository instance
     * @param AuditLogger         $logger     AuditLogger service
     */
    public function __construct(UserGroupRepository $repository, AuditLogger $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * Menampilkan semua daftar Group.
     *
     * @Route("", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
                                                      $dq->getPageSize(), $dq->getOffset(),
                                                      $dq->getCondition());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), $this->context_norels);
    }

    /**
     * Menambahkan record grup pengguna baru ke dalam database.
     *
     * @Route("/create", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     * @throws ORMException
     */
    public function create(Request $request): JsonResponse
    {
        $entity = new UserGroup();
        $form = $this->createForm(UserGroupType::class, $entity);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->repository->save($entity);
        $this->logger->getEntityManager()->flush();
        $this->logger->info('TAMBAH record grup pengguna baru.', UserGroup::class,
                            $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menampilkan single record Group.
     *
     * @Route("/view/{groupId}", methods={"GET"}, requirements={"groupId":"\d+"})
     *
     * @param UserGroup $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function fetch(?UserGroup $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Memperbaru record grup pengguna yang sudah ada.
     *
     * @Route("/update/{groupId}", methods={"POST"}, requirements={"groupId":"\d+"})
     *
     * @param Request   $request HTTP Request
     * @param UserGroup $entity  Persistent entity object
     *
     * @return JsonResponse
     * @throws ORMException
     */
    public function update(Request $request, ?UserGroup $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(UserGroupType::class, $entity);
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->jsonError($this->normalizeFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->repository->save($entity);
        $this->logger->info('UPDATE record grup pengguna.', UserGroup::class,
                            $entity, [self::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Menghapus record grup pengguna dari database.
     *
     * @Route("/delete/{groupId}", methods={"DELETE"}, requirements={"groupId":"\d+"})
     *
     * @param UserGroup $entity Persistent entity object
     *
     * @return JsonResponse
     */
    public function delete(?UserGroup $entity): JsonResponse
    {
        if (empty($entity)) {
            return $this->jsonError('Data tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        try {
            $this->repository->remove($entity);
            $this->logger->notice('HAPUS record grup pengguna.', UserGroup::class,
                                  $entity, [self::CONTEXT_NON_REL]);

            return $this->jsonSuccess('Grup pengguna telah berhasil dihapus dari database');
        } catch (Exception $exception) {
            return $this->jsonError(
                'Gagal menghapus record grup pengguna, masih ada record yang terkait dengannya.',
                JsonResponse::HTTP_FORBIDDEN);
        }
    }

}
