<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Security;


use App\Component\Controller\JsonController;
use App\Repository\Security\AppRoleRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RolesController
 *
 * @package App\Controller\Security
 * @author  Ahmad Fajar
 * @since   03/08/2018, modified: 30/03/2019 18:33
 *
 * @Route("/app/security/roles")
 */
class RolesController extends JsonController
{

    /**
     * @var AppRoleRepository
     */
    private $repository;


    /**
     * RolesController constructor.
     *
     * @param AppRoleRepository $repository
     */
    public function __construct(AppRoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Menampilkan daftar ROLES aplikasi.
     *
     * @Route("", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $roles = $this->repository->findBy([], ['id' => 'asc']);

        return $this->jsonResponse($roles, count($roles));
    }

}