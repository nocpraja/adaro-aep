<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Security;


use App\Component\Controller\FormErrorsNormalizerTrait;
use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Entity\Security\AppSetting;
use App\Models\Security\AppSettingModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AppSettingController
 *
 * @package App\Controller\Security
 * @author  Mark Melvin
 * @since   23/02/2020, modified: 23/03/2020 2:00
 *
 * @Route("/app/security/appsetting")
 */
class AppSettingController extends JsonController
{
    use FormErrorsNormalizerTrait;

    /**
     * @var AppSettingModel
     */
    private $model;


    /**
     * AppSettingController constructor.
     *
     * @param AppSettingModel $model
     */
    public function __construct(AppSettingModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan AppSetting dengan kriteria tertentu
     *
     * @Route("", name="security_appsetting_index", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function index(Request $request): JsonResponse
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->model->getRepository()->findAllByCriteria($dq->getFilters(), $dq->getSorts(), 0, 0);

        return $this->jsonResponse($query->getResult(), -1, $this->context_norels);
    }

    /**
     * Menampilkan daftar section AppSetting
     *
     * @Route("/sections", name="security_appsetting_sections", methods={"GET"})
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function sections(): JsonResponse
    {
        $sections = $this->model->getSections();
        $section_names = [['label' => '[All]', 'value' => '']];
        foreach ($sections as $item) {
            $section_names[] = ['label' => $item['section'], 'value' => $item['section']];
        }
        return $this->jsonResponse($section_names, -1, $this->context_norels);
    }


    /**
     * Menampilkan detail record AppSetting
     *
     * @Route("/get/{section}/{attr}", methods={"GET"}, name="security_appsetting_fetch", requirements={"section":"\w+","attr":"\w+"})
     *
     * @param String $attr
     * @param String $section
     * @return JsonResponse
     */
    public function fetch(String $section, String $attr): JsonResponse
    {
        $entity = $this->model->getRepository()->findBySectionAndAttribute($section, $attr);
        if (empty($entity)) {
            return $this->jsonError('Data referensi tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }

    /**
     * Memperbarui AppSetting
     *
     * @Route("/update", methods={"POST"}, name="security_appsetting_update")
     *
     * @param Request       $request HTTP Request
     *
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        $section = $request->get('section', '');
        $attr = $request->get('attr' );
        $value = $request->get('value', '');

        $entity = $this->model->getRepository()->findBySectionAndAttribute($section, $attr);
        if (empty($entity)) {
            return $this->jsonError('AppSetting tidak ditemukan.', JsonResponse::HTTP_NOT_FOUND);
        }
        $entity->setAttributeValue($value);

        $this->model->save($entity);
        $this->model->getLogger()->info('UPDATE record AppSetting.', AppSetting::class, $entity,
                                        [JsonController::CONTEXT_NON_REL]);

        return $this->jsonResponse($entity, -1, $this->context_norels);
    }


}
