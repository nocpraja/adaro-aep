<?php

/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Security;


use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Repository\Security\AuditLogRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuditLogController
 *
 * @package App\Controller\Security
 * @author  Ahmad Fajar
 * @since   01/04/2019, modified: 11/04/2019 14:07
 *
 * @Route("/app/security/audit-log")
 */
class AuditLogController extends JsonController
{
    /**
     * @var AuditLogRepository
     */
    private $repository;

    /**
     * AccessRightsController constructor.
     *
     * @param AuditLogRepository $repository
     */
    public function __construct(AuditLogRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Load all available menu items for the given user's group in TreeList structures.
     *
     * @Route("", name="auditlog_list", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     * @throws
     */
    public function index(Request $request)
    {
        $dq = DataQuery::parseRequest($request);
        $query = $this->repository->findAllByCriteria($dq->getFilters(), $dq->getSorts(),
            $dq->getPageSize(), $dq->getOffset());

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $this->jsonResponse($paginator->getIterator(), $paginator->count(), ['groups' => [ self::CONTEXT_NON_REL, self::CONTEXT_WITH_REL ]]);
    }

}