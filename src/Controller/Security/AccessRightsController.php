<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\Security;


use App\Component\Controller\JsonController;
use App\Component\DataObject\DataQuery;
use App\Models\Security\AccessRightsModel;
use App\Repository\Security\UserGroupRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccessRightsController
 *
 * @package App\Controller\Security
 * @author  Ahmad Fajar
 * @since   01/04/2019, modified: 11/04/2019 14:07
 *
 * @Route("/app/security/access-rights")
 */
class AccessRightsController extends JsonController
{

    /**
     * @var AccessRightsModel
     */
    private $model;

    /**
     * @var UserGroupRepository
     */
    private $groupRepository;


    /**
     * AccessRightsController constructor.
     *
     * @param AccessRightsModel   $model      AccessRights domain model
     * @param UserGroupRepository $repository Entity repository instance
     */
    public function __construct(AccessRightsModel $model, UserGroupRepository $repository)
    {
        $this->model = $model;
        $this->groupRepository = $repository;
    }

    /**
     * Load all available menu items for the given user's group in TreeList structures.
     *
     * @Route("", name="app_security_acl_listall", methods={"GET"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function listAll(Request $request)
    {
        $group = null;
        $filters = DataQuery::parseRequest($request)->getFilters();

        if (!empty($filters)) {
            $query = $this->groupRepository->findAllByCriteria($filters, [], 1);
            $results = $query->getResult();
            if (!empty($results)) {
                $group = $results[0];
            }
        }

        $treeItems = $this->model->loadTreeItems($group);

        return $this->jsonResponse($treeItems, count($treeItems), $this->context_norels);
    }

    /**
     * Update access rights of the given user's group.
     *
     * @Route("/update", name="app_security_acl_update", methods={"POST"})
     *
     * @param Request $request HTTP Request
     *
     * @return JsonResponse
     */
    public function update(Request $request)
    {
        $group = $this->groupRepository->find($request->request->getInt('groupId'));
        $items = $request->request->get('items');

        try {
            if (!$group->isAdmin()) {
                $this->model->insertOrUpdate($group, $items);
            }

            return $this->jsonSuccess('Hak akses telah berhasil diperbarui.');
        } catch (\Exception $ex) {
            return $this->jsonError('Ada kesalahan pada pengiriman data.', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

}