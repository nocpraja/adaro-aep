<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller;


use App\Entity\Security\UserAccount;
use App\Models\Security\AccessRightsModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @package App\Controller
 * @author  Ahmad Fajar
 * @since   18/07/2018, modified: 11/04/2019 14:07
 */
class DefaultController extends AbstractController
{

    /**
     * @var AccessRightsModel
     */
    private $model;


    /**
     * DefaultController constructor.
     *
     * @param AccessRightsModel $model AccessRights domain model
     */
    public function __construct(AccessRightsModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menampilkan halaman utama.
     *
     * @Route("/", name="default_index")
     *
     * @return RedirectResponse
     */
    public function index(): Response
    {
        return $this->redirectToRoute('auth_login_form');
    }

    /**
     * Menampilkan halaman homepage aplikasi.
     *
     * @Route("/app", name="default_homepage")
     */
    public function homepage(): Response
    {
        $userObj = $this->getUser();

        if ($userObj instanceof UserAccount) {
            $menus = $this->model->loadNavigationMenus($userObj->getGroup());
            return $this->render('default/apphome.html.twig', ['menus' => $menus]);
        }

        throw $this->createAccessDeniedException();
    }

}
