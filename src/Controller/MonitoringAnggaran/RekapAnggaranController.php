<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */


namespace App\Controller\MonitoringAnggaran;

use App\Component\Controller\JsonController;
use App\Entity\MasterData\JenisProgramCSR;
use App\Models\Kegiatan\MonitoringModel;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RekapAnggaranController
 *
 * @package App\Controller\MonitoringAnggaran
 * @author  Mark Melvin
 * @since   18/05/2020
 *
 * @Route("/app/rekapanggaran")
 */


class RekapAnggaranController extends JsonController
{

    /**
     * @var MonitoringModel
     */
    private $model;

    /**
     * RekapAnggaranController constructor.
     * @param MonitoringModel $model
     */
    public function __construct(MonitoringModel $model)
    {
        $this->model = $model;
    }


    /**
     * @Route("/", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws
     */
    public function index(Request $request) : Response {
        $tahunAwal = $request->get('from');
        if ($tahunAwal == null) {
            $tahunAwal = 2018;
        }
        if ($tahunAwal < 2018) {
            $tahunAwal = 2018;
        }
        $tahunAkhir = $tahunAwal;

        $result = $this->model->buildRekap($tahunAwal, $tahunAkhir);
        return $this->jsonResponse($result, -1, $this->context_withrels);
    }

    /**
     * @Route("/bidang/{id}/{tahun}", requirements={"id":"\d+", "tahun":"\d+"}, methods={"GET"})
     * @param JenisProgramCSR $bidang
     * @param int $tahun
     * @param Request $request
     * @return Response
     * @throws
     */
    public function bidang(JenisProgramCSR $bidang, int $tahun, Request $request) : Response {

        $result = $this->model->buildRekapBidang($bidang, $tahun, $tahun);
        return $this->jsonResponse($result, -1, $this->context_withrels);
    }


}