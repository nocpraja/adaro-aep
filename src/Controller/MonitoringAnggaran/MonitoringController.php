<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MonitoringAnggaran;

use App\Component\Controller\JsonController;
use App\Entity\MasterData\ProgramCSR;
use App\Models\Kegiatan\MonitoringModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LaporanKegiatanRabController
 *
 * @package App\Controller\MonitoringAnggaran
 * @author  Mark Melvin
 * @since   16/12/2019
 *
 * @Route("/app/monitoring")
 */
class MonitoringController extends JsonController
{
    /**
     * @var MonitoringModel
     */
    private $model;

    /**
     * MonitoringController constructor.
     * @param MonitoringModel $model
     */
    public function __construct(MonitoringModel $model)
    {
        $this->model = $model;
    }

    /**
     * Menyusun laporan monitoring anggaran
     * @Route("/report/{msid}/{periodeAwal}/{periodeAkhir}",
     *     methods={"GET"},
     *     requirements={"msid":"\d+","periodeAwal":"\d{0,6}","periodeAkhir":"\d{0,6}"},
     *     defaults={"periodeAwal":"","periodeAkhir":""})
     *
     * @param ProgramCSR $program
     * @param string $periodeAwal
     * @param string $periodeAkhir
     * @return JsonResponse
     */
    public function buildReport(ProgramCSR $program, string $periodeAwal='', string $periodeAkhir='') : JsonResponse
    {
        return $this->jsonResponse($this->model->buildReport($program, $periodeAwal, $periodeAkhir),-1,
            $this->context_withrels);
    }

}