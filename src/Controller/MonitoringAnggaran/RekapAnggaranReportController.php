<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MonitoringAnggaran;

use App\Component\Controller\SpreadsheetController;
use App\DataObject\Kegiatan\SerapanTahunanDto;
use App\Entity\MasterData\JenisProgramCSR;
use App\Models\Kegiatan\MonitoringModel;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RekapAnggaranReportController
 *
 * @package App\Controller\MonitoringAnggaran
 * @author  Mark Melvin
 * @since   16/05/2020
 *
 * @Route("/app/rekapanggaranreport")
 */

class RekapAnggaranReportController extends SpreadsheetController
{
    /**
     * @var MonitoringModel
     */
    private $model;

    /**
     * MonitoringController constructor.
     * @param MonitoringModel $model
     */
    public function __construct(MonitoringModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param JenisProgramCSR $bidang
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_header_rekap(Spreadsheet $spreadsheet, JenisProgramCSR $bidang): Spreadsheet
    {
        // Add some data
        $bidangLine = '';
        if ($bidang !== null) {
            $bidangLine = 'Bidang '.$bidang->getTitle();
        }
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue( 'A3', $bidangLine);
        $spreadsheet->getActiveSheet()
            ->mergeCells('A3:K3');

        return $spreadsheet;
    }

    /**
     * @param array $dataItems
     * @return array|null
     */
    private function getTopRow(array $dataItems) : ?array {
        if (isset($dataItems) && isset($dataItems[0])){
            return $dataItems[0];
        }
        return null;
    }

    /**
     * @param array $dataRow
     * @param int $tahun
     * @return SerapanTahunanDto|null
     */
    private function getRowPart(array $dataRow, int $tahun) : ?SerapanTahunanDto {
        if (isset($dataRow) && isset($dataRow[$tahun])) {
            return $dataRow[$tahun];
        }
        return null;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param array $dataItems
     * @param int $year
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_table_column_headers(Spreadsheet $spreadsheet,
                                                   int &$row,
                                                   array $dataItems,
                                                   int $year) : Spreadsheet
    {
        $spreadsheet->getActiveSheet()->setCellValue('A'.$row, 'Tahun '.$year);
        $spreadsheet->getActiveSheet()
            ->duplicateStyle(
                $spreadsheet->getActiveSheet()->getStyle('A5'),
                'A'.$row.':'.'K'.$row
            );
        $row++;
        $spreadsheet->getActiveSheet()->setCellValue('A'.$row, 'Detail Anggaran');
        $columnInt = Coordinate::columnIndexFromString("B");
        $dataRow = $this->getTopRow($dataItems);
        $column = 'B';
        /* @var SerapanTahunanDto $yearRow */
        $yearRow = $this->getRowPart($dataRow, $year);
        for ($month = $yearRow->getBulanAwal(); $month <= $yearRow->getBulanAkhir(); $month++) {
            $column = Coordinate::stringFromColumnIndex($columnInt);
            $spreadsheet->getActiveSheet()->setCellValue($column.$row, $month.'/'.$year);
            $columnInt++;
        }
        $column = Coordinate::stringFromColumnIndex($columnInt);
        $spreadsheet->getActiveSheet()->setCellValue($column.$row, 'Serapan');
        $columnInt++;
        $column = Coordinate::stringFromColumnIndex($columnInt);
        $spreadsheet->getActiveSheet()->setCellValue($column.$row, 'Budget');
        $columnInt++;
        $column = Coordinate::stringFromColumnIndex($columnInt);
        $spreadsheet->getActiveSheet()->setCellValue($column.$row, 'Variance');
        $spreadsheet->getActiveSheet()
            ->duplicateStyle(
                $spreadsheet->getActiveSheet()->getStyle('A6'),
                'A'.$row.':'.$column.$row
            );

        $row++;

        return $spreadsheet;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param array $dataItems
     * @param int $tahunAwal
     * @param int $tahunAkhir
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_laporan_rekap_full(Spreadsheet $spreadsheet,
                                                 int &$row,
                                                 array $dataItems,
                                                 int $tahunAwal,
                                                 int $tahunAkhir) : Spreadsheet
    {
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 5);

        for ($tahun = $tahunAwal; $tahun <= $tahunAkhir; $tahun++) {

            $this->generate_table_column_headers($spreadsheet, $row, $dataItems, $tahun);

            foreach ($dataItems as $dataRow) {
                /** @var SerapanTahunanDto $item */
                $item = $this->getRowPart($dataRow, $tahun);
                if ($item->getLevel()=='global') {
                    $spreadsheet->getActiveSheet()->getCell('A' . $row)
                        ->setValue('DANA GLOBAL');
                }
                else if ($item->getLevel()=='bidang') {
                    $spreadsheet->getActiveSheet()->getCell('A' . $row)
                        ->setValue(' Dana Bidang '.$item->getJudul());
                }
                else if ($item->getLevel()=='program') {
                    $spreadsheet->getActiveSheet()->getCell('A' . $row)
                        ->setValue('   '.$item->getJudul());
                }
                else  {
                    $spreadsheet->getActiveSheet()->getCell('A' . $row)
                        ->setValue('       '.$item->getJudul());
                }
                $spreadsheet->getActiveSheet()->getCell('A' . $row)
                    ->getStyle()->getAlignment()->setWrapText(true);
                $spreadsheet->getActiveSheet()->getCell('A' . $row)
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);

                $columnInt = Coordinate::columnIndexFromString("B");

                for ($month = $item->getBulanAwal(); $month <= $item->getBulanAkhir(); $month++) {
                    $method = 'getSerapan' . $month;
                    $value = $item->$method();
                    $column = Coordinate::stringFromColumnIndex($columnInt);
                    $spreadsheet->getActiveSheet()->getCell($column . $row)->setValue($value)
                        ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                    $spreadsheet->getActiveSheet()->getStyle($column . $row)->getNumberFormat()
                        ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
                    $columnInt++;
                }

                $spreadsheet->getActiveSheet()->getCell('N' . $row)->setValue($item->getTotalSerapan())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('N' . $row)->getNumberFormat()
                    ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
                $spreadsheet->getActiveSheet()->getCell('O' . $row)->setValue($item->getBudget())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('O' . $row)->getNumberFormat()
                    ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
                $spreadsheet->getActiveSheet()->getCell('P' . $row)->setValue($item->getVariance())
                    ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                $spreadsheet->getActiveSheet()->getStyle('P' . $row)->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_PERCENTAGE_00);

                if ($item->getLevel()=='global') {
                    $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getBorders()
                        ->getOutline()->setBorderStyle(Border::BORDER_MEDIUM);
                    $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getFont()->setBold(true);
                }
                else if ($item->getLevel()=='bidang') {
                    $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getBorders()
                        ->getOutline()->setBorderStyle(Border::BORDER_MEDIUM);
                    $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getFont()->setBold(true)->setItalic(true);
                }
                else if ($item->getLevel()=='program') {
                    $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getFont()->setBold(true)->setSize(9);
                }
                else {
                    $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getFont()->setSize(8);
                }

                $row++;
            }
            $row++;
        }

        return $spreadsheet;
    }



    /**
     * Menyusun Rekap monitoring anggaran
     * @Route("/full/{tahun}/{id}",
     *     methods={"GET"},
     *     requirements={"tahun":"\d{0,6}","id":"\d+"},
     *     defaults={"tahun":"","id":"0"})
     *
     * @param string $tahun
     * @param JenisProgramCSR $bidang
     * @throws Exception
     * @return Response
     */
    public function reportFull(int $tahun, JenisProgramCSR $bidang) : Response
    {
        if ($bidang !== null) {
            $dataItems = $this->model->buildRekapBidang($bidang, $tahun, $tahun);
        }
        else {
            $dataItems = $this->model->buildRekap($tahun, $tahun);
        }
        if (empty($dataItems)) {
            return $this->jsonError("Data tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("rekap_full.xlsx");

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Rekap Monitoring Anggaran')
            ->setSubject('Rekap Monitoring Anggaran')
            ->setDescription('Rekap Monitoring Anggaran')
            ->setKeywords('monitoring anggaran')
            ->setCategory('Monitoring Anggaran');

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BRekap Monitoring Anggaran&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        // Set page orientation and size
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_rekap($spreadsheet, $bidang);

        $row = 5;
        $spreadsheet = $this->generate_laporan_rekap_full($spreadsheet, $row, $dataItems, $tahun, $tahun);

        $spreadsheet->getActiveSheet()->setTitle('Monitoring Anggaran');
        $spreadsheet->getActiveSheet()->setShowGridLines(true);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        //IOFactory::registerWriter('Pdf', \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class);
        return $this->streamResponse($spreadsheet,
            'Rekap Monitoring Anggaran'.' '.$tahun, 'Xlsx');

    }

    /**
     * Menyusun Rekap monitoring anggaran
     * @Route("/printfull/{tahun}/{id}",
     *     methods={"GET"},
     *     requirements={"tahun":"\d{0,6}","id":"\d+"},
     *     defaults={"tahun":"","id":"0"})
     * @param JenisProgramCSR $bidang
     * @param string $tahun
     * @return Response
     * @throws Exception
     */
    public function print_full(int $tahun, JenisProgramCSR $bidang): Response
    {
        if ($bidang !== null) {
            $dataItems = $this->model->buildRekapBidang($bidang, $tahun, $tahun);
        }
        else {
            $dataItems = $this->model->buildRekap($tahun, $tahun);
        }
        if (empty($dataItems)) {
            return $this->jsonError("Data tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("rekap_full.xlsx");

        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Rekap Monitoring Anggaran')
            ->setSubject('Rekap Monitoring Anggaran '.'Monitoring Anggaran')
            ->setDescription('Rekap Monitoring Anggaran')
            ->setKeywords('monitoring anggaran')
            ->setCategory('Monitoring Anggaran');

        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BRekap Monitoring Anggaran&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        // Set page orientation and size
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_rekap($spreadsheet, $bidang);
        $row = 5;
        $spreadsheet = $this->generate_laporan_rekap_full($spreadsheet, $row, $dataItems, $tahun, $tahun);

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Monitoring Anggaran');
        $spreadsheet->getActiveSheet()->setShowGridLines(false);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        return $this->streamResponse($spreadsheet,
            'Rekap Monitoring Anggaran'.' '.$tahun, 'pdf');
    }


}