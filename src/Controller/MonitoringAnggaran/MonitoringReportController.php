<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Controller\MonitoringAnggaran;

use App\Component\Controller\SpreadsheetController;
use App\DataObject\Kegiatan\SerapanTahunanDto;
use App\Entity\MasterData\ProgramCSR;
use App\Models\Kegiatan\MonitoringModel;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MonitoringReportController
 *
 * @package App\Controller\MonitoringAnggaran
 * @author  Mark Melvin
 * @since   16/12/2019
 *
 * @Route("/app/monitoringreport")
 */

class MonitoringReportController extends SpreadsheetController
{
    /**
     * @var MonitoringModel
     */
    private $model;

    /**
     * MonitoringController constructor.
     * @param MonitoringModel $model
     */
    public function __construct(MonitoringModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param ProgramCSR $program
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_header_programcsr(Spreadsheet $spreadsheet, ProgramCSR $program, $periodeAwal, $periodeAkhir): Spreadsheet
    {
        // Add some data
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A3', 'Program CSR '.$program->getName())
            ->setCellValue('A4', 'Bidang '.$program->getJenis()->getTitle())
            ->setCellValue( 'A5',
                'Periode '.$this->formatPeriode($periodeAwal).' s/d '.$this->formatPeriode($periodeAkhir));
        $spreadsheet->getActiveSheet()
            ->mergeCells('A3:K3')
            ->mergeCells('A4:K4')
            ->mergeCells('A5:K5');

        return $spreadsheet;
    }

    /**
     * @param array $dataItems
     * @return array|null
     */
    private function getTopRow(array $dataItems) : ?array {
        if (isset($dataItems) && isset($dataItems[0])){
            return $dataItems[0];
        }
        return null;
    }

    /**
     * @param array $dataRow
     * @param int $tahun
     * @return SerapanTahunanDto|null
     */
    private function getRowPart(array $dataRow, int $tahun) : ?SerapanTahunanDto {
        if (isset($dataRow) && isset($dataRow[$tahun])) {
            return $dataRow[$tahun];
        }
        return null;
    }

    /**
     * @param array $dataRow
     * @param int $tahun
     * @return SerapanTahunanDto|null
     */
    private function getSumArray(array $dataRow) : ?array {
        if (isset($dataRow) && isset($dataRow['sum'])) {
            return $dataRow['sum'];
        }
        return null;
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param array $dataItems
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_table_column_headers(Spreadsheet $spreadsheet,
                                                   array $dataItems) : Spreadsheet
    {
        $columnInt = Coordinate::columnIndexFromString("B");
        $dataRow = $this->getTopRow($dataItems);
        $column = 'B';
        /* @var SerapanTahunanDto $yearRow */
        foreach ($dataRow as $year => $yearRow) {
            if (is_numeric($year)) {
                for ($month = $yearRow->getBulanAwal(); $month <= $yearRow->getBulanAkhir(); $month++) {
                    $column = Coordinate::stringFromColumnIndex($columnInt);
                    $spreadsheet->getActiveSheet()->insertNewColumnBeforeByIndex($columnInt);
                    $spreadsheet->getActiveSheet()->getDefaultColumnDimension($column)->setWidth(17);
                    $spreadsheet->getActiveSheet()->setCellValue($column . '9', $month . '/' . $year);
                    $columnInt++;
                }
            }
        }
        $spreadsheet->getActiveSheet()
            ->duplicateStyle(
                $spreadsheet->getActiveSheet()->getStyle('A9'),
                'B9:'.$column.'9'
            );
        $column = Coordinate::stringFromColumnIndex($columnInt);
        $spreadsheet->getActiveSheet()->setCellValue($column.'9', 'Serapan');
        $columnInt++;
        $column = Coordinate::stringFromColumnIndex($columnInt);
        $spreadsheet->getActiveSheet()->setCellValue($column.'9', 'Budget');
        $columnInt++;
        $column = Coordinate::stringFromColumnIndex($columnInt);
        $spreadsheet->getActiveSheet()->setCellValue($column.'9', 'Variance');

        return $spreadsheet;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @param int $row
     * @param array $dataItems
     * @return Spreadsheet
     * @throws Exception
     */
    private function generate_laporan_progress_full(Spreadsheet $spreadsheet,
                                                    &$row,
                                                    array $dataItems) : Spreadsheet
    {
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(8, 9);

        foreach ($dataItems as $item) {
            $columnInt = Coordinate::columnIndexFromString("B");
            /* @var SerapanTahunanDto $itemTahun */
            foreach ($item as $key => $itemTahun) {
                if (is_numeric($key)) {
                    if ($itemTahun->getLevel()=='program') {
                        $spreadsheet->getActiveSheet()->getCell('A' . $row)
                            ->setValue($itemTahun->getJudul());
                    }
                    else if ($itemTahun->getLevel()=='batch') {
                        $spreadsheet->getActiveSheet()->getCell('A' . $row)
                            ->setValue(' '.$itemTahun->getJudul());
                    }
                    else if ($itemTahun->getLevel()=='kegiatan') {
                        $spreadsheet->getActiveSheet()->getCell('A' . $row)
                            ->setValue('   '.$itemTahun->getJudul());
                    }
                    else  {
                        $spreadsheet->getActiveSheet()->getCell('A' . $row)
                            ->setValue('       '.$itemTahun->getJudul());
                    }
                    $spreadsheet->getActiveSheet()->getCell('A' . $row)
                        ->getStyle()->getAlignment()->setWrapText(true);
                    $spreadsheet->getActiveSheet()->getCell('A' . $row)
                        ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                    for ($month = $itemTahun->getBulanAwal(); $month <= $itemTahun->getBulanAkhir(); $month++) {
                        $method = 'getSerapan' . $month;
                        $value = $itemTahun->$method();
                        $column = Coordinate::stringFromColumnIndex($columnInt);
                        $spreadsheet->getActiveSheet()->setCellValue($column . $row, $value);
                        $spreadsheet->getActiveSheet()->getCell($column . $row)->setValue($value)
                            ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
                        $spreadsheet->getActiveSheet()->getStyle($column . $row)->getNumberFormat()
                            ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
                        $columnInt++;
                    }
                }
            }
            $sum = $this->getSumArray($item);
            $column = Coordinate::stringFromColumnIndex($columnInt);
            $spreadsheet->getActiveSheet()->getCell($column. $row)->setValue($sum['serapan'])
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle($column. $row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
            $columnInt++;
            $column = Coordinate::stringFromColumnIndex($columnInt);
            $spreadsheet->getActiveSheet()->getCell($column.$row)->setValue($sum['budget'])
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle($column.$row)->getNumberFormat()
                ->setFormatCode(self::FORMAT_ACCOUNTING_RP);
            $columnInt++;
            $column = Coordinate::stringFromColumnIndex($columnInt);
            $spreadsheet->getActiveSheet()->getCell($column.$row)->setValue($sum['variance'])
                ->getStyle()->getBorders()->getOutline()->setBorderStyle(Border::BORDER_HAIR);
            $spreadsheet->getActiveSheet()->getStyle($column.$row)->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_PERCENTAGE_00);
            $columnInt++;

            $itemTahun = $this->getRowPart($item, array_key_first($item));
            if ($itemTahun->getLevel()=='program') {
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getBorders()
                ->getOutline()->setBorderStyle(Border::BORDER_MEDIUM);
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getFont()->setBold(true);
            }
            else if ($itemTahun->getLevel()=='batch') {
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getBorders()
                ->getOutline()->setBorderStyle(Border::BORDER_MEDIUM);
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getFont()->setBold(true)->setItalic(true);
            }
            else if ($itemTahun->getLevel()=='kegiatan') {
                $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getFont()->setBold(true)->setSize(9);
            }
            else {
               $spreadsheet->getActiveSheet()->getStyle('A' . $row.':P' . $row)->getFont()->setSize(8);
            }
            $row++;
        }

        return $spreadsheet;
    }



    /**
     * Menyusun laporan monitoring anggaran
     * @Route("/full/{msid}/{periodeAwal}/{periodeAkhir}",
     *     methods={"GET"},
     *     requirements={"msid":"\d+","periodeAwal":"\d{0,6}","periodeAkhir":"\d{0,6}"},
     *     defaults={"periodeAwal":"","periodeAkhir":""})
     *
     * @param ProgramCSR $program
     * @param string $periodeAwal
     * @param string $periodeAkhir
     * @throws Exception
     * @return Response
     */
    public function reportFull(ProgramCSR $program, string $periodeAwal='', string $periodeAkhir='') : Response
    {
        $dataItems = $this->model->buildReport($program, $periodeAwal, $periodeAkhir);

        if (empty($dataItems)) {
            return $this->jsonError("Data tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("progress_full.xlsx");

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Monitoring Anggaran')
            ->setSubject('Monitoring Anggaran '.$program->getName())
            ->setDescription('Laporan Pelaksanaan Program CSR')
            ->setKeywords('monitoring anggaran')
            ->setCategory($program->getName());

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BMonitoring Anggaran&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        // Set page orientation and size
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_programcsr($spreadsheet, $program, $periodeAwal, $periodeAkhir);
        $spreadsheet = $this->generate_table_column_headers($spreadsheet, $dataItems);

        $row = 10;
        $spreadsheet = $this->generate_laporan_progress_full($spreadsheet, $row, $dataItems);

        $spreadsheet->getActiveSheet()->setTitle($program->getName());
        $spreadsheet->getActiveSheet()->setShowGridLines(true);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        //IOFactory::registerWriter('Pdf', \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class);
        return $this->streamResponse($spreadsheet,
            'Monitoring Anggaran '.$program->getName().' '.$periodeAwal.'-'.$periodeAkhir, 'Xlsx');

    }

    /**
     * Menyusun laporan monitoring anggaran
     * @Route("/printfull/{msid}/{periodeAwal}/{periodeAkhir}",
     *     methods={"GET"},
     *     requirements={"msid":"\d+","periodeAwal":"\d{0,6}","periodeAkhir":"\d{0,6}"},
     *     defaults={"periodeAwal":"","periodeAkhir":""})
     *
     * @param ProgramCSR $program
     * @param string $periodeAwal
     * @param string $periodeAkhir
     * @throws Exception
     * @return Response
     */
    public function print_full(ProgramCSR $program, string $periodeAwal='', string $periodeAkhir=''): Response
    {
        $dataItems = $this->model->buildReport($program, $periodeAwal, $periodeAkhir);

        if (empty($dataItems)) {
            return $this->jsonError("Data tidak ditemukan");
        }

        // Create new Spreadsheet object
        $spreadsheet = $this->load("progress_full.xlsx");

        $spreadsheet->getProperties()->setCreator('Admin AEP')
            ->setLastModifiedBy('Admin AEP')
            ->setTitle('Monitoring Anggaran')
            ->setSubject('Monitoring Anggaran '.$program->getName())
            ->setDescription('Laporan Pelaksanaan Program CSR')
            ->setKeywords('monitoring anggaran')
            ->setCategory($program->getName());

        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BMonitoring Anggaran&RPer Tanggal &D');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $spreadsheet->getProperties()->getTitle() . '&RHalaman &P dari &N');
        // Set page orientation and size
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet = $this->generate_header_programcsr($spreadsheet, $program, $periodeAwal, $periodeAkhir);
        $spreadsheet = $this->generate_table_column_headers($spreadsheet, $dataItems);

        $row = 10;
        $spreadsheet = $this->generate_laporan_progress_full($spreadsheet, $row, $dataItems);

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($program->getName());
        $spreadsheet->getActiveSheet()->setShowGridLines(false);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        return $this->streamResponse($spreadsheet,
            'Monitoring Anggaran '.$program->getName().' '.$periodeAwal.'-'.$periodeAkhir, 'pdf');
    }


    private function formatPeriode($period) {
        $date = date_create_from_format('Ym', $period);
        return date_format($date, 'M Y');
    }

}