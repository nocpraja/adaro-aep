<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Beneficiary;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BeneficiaryIndividu|null find($id, $lockMode = null, $lockVersion = null)
 * @method BeneficiaryIndividu|null findOneBy(array $criteria, array $orderBy = null)
 * @method BeneficiaryIndividu[]    findAll()
 * @method BeneficiaryIndividu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 30/04/2019 4:50
 */
class BeneficiaryIndividuRepository extends BaseEntityRepository
{

    /**
     * BeneficiaryIndividuRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BeneficiaryIndividu::class);
    }

    /**
     * Menampilkan daftar penerima manfaat individu dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      bool $forProgram = false,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Guru PAUD dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllGuruPaudByCriteria(array $filters = [], array $sorts = [],
                                              int $limit = 0, int $offset = 0,
                                              bool $forProgram = false,
                                              string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori = 1')
           ->andWhere('individu.kategori = 5');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Guru Pesantren dan Yayasan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllGuruPesantrenAndYayasanByCriteria(array $filters = [], array $sorts = [],
                                                             int $limit = 0, int $offset = 0,
                                                             bool $forProgram = false,
                                                             string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori > 3')
            ->andWhere('individu.kategori in (4, 5, 8)');
//           ->andWhere('individu.kategori < 6');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Guru SMK dan POLTEK dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllGuruSmkAndPoltekByCriteria(array $filters = [], array $sorts = [],
                                                      int $limit = 0, int $offset = 0,
                                                      bool $forProgram = false,
                                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori > 1')
           ->andWhere('institusi.kategori < 4')
           ->andWhere('individu.kategori > 4')
           ->andWhere('individu.kategori < 7');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Mahasiswa POLTEK dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllMahasiswaPoltekByCriteria(array $filters = [], array $sorts = [],
                                                     int $limit = 0, int $offset = 0,
                                                     bool $forProgram = false,
                                                     string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori = 3')
           ->andWhere('individu.kategori = 3');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Manajemen Pesantren dan Yayasan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllManajemenPesantrenAndYayasanByCriteria(array $filters = [], array $sorts = [],
                                                                  int $limit = 0, int $offset = 0,
                                                                  bool $forProgram = false,
                                                                  string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori > 3')
           ->andWhere('individu.kategori = 7');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Manajemen SMK dan Poltek dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllManajemenSmkAndPoltekByCriteria(array $filters = [], array $sorts = [],
                                                           int $limit = 0, int $offset = 0,
                                                           bool $forProgram = false,
                                                           string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori > 1')
           ->andWhere('institusi.kategori < 4')
           ->andWhere('individu.kategori = 7');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Siswa PAUD dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllSiswaPaudByCriteria(array $filters = [], array $sorts = [],
                                               int $limit = 0, int $offset = 0,
                                               bool $forProgram = false,
                                               string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori = 1')
           ->andWhere('individu.kategori = 2');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Siswa Pesantren dan Yayasan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllSiswaPesantrenAndYayasanByCriteria(array $filters = [], array $sorts = [],
                                                              int $limit = 0, int $offset = 0,
                                                              bool $forProgram = false,
                                                              string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori > 3')
           ->andWhere('individu.kategori < 3');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Siswa SMK dan Poltek dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllSiswaSmkAndPoltekByCriteria(array $filters = [], array $sorts = [],
                                                       int $limit = 0, int $offset = 0,
                                                       bool $forProgram = false,
                                                       string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori > 1')
           ->andWhere('institusi.kategori < 4')
           ->andWhere('individu.kategori > 1')
           ->andWhere('individu.kategori < 4');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Siswa SMK dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllSiswaSmkByCriteria(array $filters = [], array $sorts = [],
                                              int $limit = 0, int $offset = 0,
                                              bool $forProgram = false,
                                              string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori = 2')
           ->andWhere('individu.kategori = 2');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            $shortField = $this->getShortFieldname($filter->getProperty());
            if (in_array($shortField, ['kelurahanId', 'kodeKelurahan', 'namaKelurahan'])) {
                $field = 'kelurahan.' . $shortField;
            } elseif (in_array($shortField, ['kecamatanId', 'kodeKecamatan', 'namaKecamatan'])) {
                $field = 'kecamatan.' . $shortField;
            } elseif (in_array($shortField, ['kabupatenId', 'kodeKabupaten', 'namaKabupaten'])) {
                $field = 'kabupaten.' . $shortField;
            } elseif (in_array($shortField, ['provinsiId', 'kodeProvinsi', 'namaProvinsi'])) {
                $field = 'provinsi.' . $shortField;
            } elseif (in_array($shortField, ['id'])) {
                $field = 'programs.' . $shortField;
            } elseif (in_array($shortField, ['institusiId',
                                             'kodeInstitusi',
                                             'namaInstitusi',
                                             'nomorAkte',
                                             'akreditasi',
                                             'nilaiAkreditasi',
                                             'tahunBerdiri',
                                             'website',
                                             'officePhone',
                                             'contactPhone',
                                             'namaPimpinan'])) {
                $field = 'institusi.' . $shortField;
            } else {
                $field = 'individu.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('individu.namaLengkap', 'asc');
        }
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['individu',
                     'kelurahan',
                     'kecamatan',
                     'kabupaten',
                     'provinsi',
                     'institusi',
                     'postedBy',
                     'updatedBy'])
           ->from('App:Beneficiary\BeneficiaryIndividu', 'individu')
           ->leftJoin('individu.kecamatan', 'kecamatan')
           ->leftJoin('kecamatan.kabupaten', 'kabupaten')
           ->leftJoin('kabupaten.provinsi', 'provinsi')
           ->leftJoin('individu.kelurahan', 'kelurahan')
           ->leftJoin('individu.institusi', 'institusi')
           ->leftJoin('individu.postedBy', 'postedBy')
           ->leftJoin('individu.updatedBy', 'updatedBy');

        return $qb;
    }

    /**
     * Create doctrine SELECT query untuk Penerima Manfaat yang ikut program tertentu.
     *
     * @return QueryBuilder
     */
    private function createSelectQueryForProgram(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['individu',
                     'kelurahan',
                     'kecamatan',
                     'kabupaten',
                     'provinsi',
                     'institusi',
                     'postedBy',
                     'updatedBy'])
           ->from('App:Beneficiary\BeneficiaryIndividu', 'individu')
           ->leftJoin('individu.kecamatan', 'kecamatan')
           ->leftJoin('kecamatan.kabupaten', 'kabupaten')
           ->leftJoin('kabupaten.provinsi', 'provinsi')
           ->leftJoin('individu.kelurahan', 'kelurahan')
           ->leftJoin('individu.institusi', 'institusi')
           ->leftJoin('individu.programs', 'programs')
           ->leftJoin('individu.postedBy', 'postedBy')
           ->leftJoin('individu.updatedBy', 'updatedBy');

        return $qb;
    }

}
