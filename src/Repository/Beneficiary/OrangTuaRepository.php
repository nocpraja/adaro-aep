<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Beneficiary;


use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Beneficiary\OrangTua;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OrangTua|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrangTua|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrangTua[]    findAll()
 * @method OrangTua[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 26/10/2018 13:06
 */
class OrangTuaRepository extends BaseEntityRepository
{

    /**
     * OrangTuaRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrangTua::class);
    }

}
