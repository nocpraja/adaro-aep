<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Beneficiary;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Beneficiary\MitraDudiJurusan;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MitraDudiJurusan|null find($id, $lockMode = null, $lockVersion = null)
 * @method MitraDudiJurusan|null findOneBy(array $criteria, array $orderBy = null)
 * @method MitraDudiJurusan[]    findAll()
 * @method MitraDudiJurusan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Beneficiary
 * @author  Ahmad Fajar
 * @since   27/02/2019, modified: 27/02/2019 12:28
 */
class MitraDudiJurusanRepository extends BaseEntityRepository
{

    /**
     * MitraDudiJurusanRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MitraDudiJurusan::class);
    }

    /**
     * Menampilkan daftar unit produksi dari suatu program keahlian/studi dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause Logic operator untuk WHERE clause
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'mitraDudiJurusan.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('mitraDudiJurusan.postedDate', 'desc');
        }
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['jurusan', 'institusi', 'mitraDudiJurusan'])
           ->from('App:Beneficiary\MitraDudiJurusan', 'mitraDudiJurusan')
           ->join('mitraDudiJurusan.jurusan', 'jurusan')
           ->join('jurusan.institusi', 'institusi');

        return $qb;
    }

}
