<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Beneficiary;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Beneficiary\BeneficiaryBeasiswa;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BeneficiaryBeasiswa|null find($id, $lockMode = null, $lockVersion = null)
 * @method BeneficiaryBeasiswa|null findOneBy(array $criteria, array $orderBy = null)
 * @method BeneficiaryBeasiswa[]    findAll()
 * @method BeneficiaryBeasiswa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 01/03/2020 23:53
 */
class BeneficiaryBeasiswaRepository extends BaseEntityRepository
{

    /**
     * BeneficiaryBeasiswaRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BeneficiaryBeasiswa::class);
    }

    /**
     * Menampilkan daftar penerima beasiswa dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      bool $forProgram = false,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            $shortField = $this->getShortFieldname($filter->getProperty());
            if (in_array($shortField, ['kelurahanId', 'kodeKelurahan', 'namaKelurahan'])) {
                $field = 'kelurahan.' . $shortField;
            } elseif (in_array($shortField, ['kecamatanId', 'kodeKecamatan', 'namaKecamatan'])) {
                $field = 'kecamatan.' . $shortField;
            } elseif (in_array($shortField, ['kabupatenId', 'kodeKabupaten', 'namaKabupaten'])) {
                $field = 'kabupaten.' . $shortField;
            } elseif (in_array($shortField, ['provinsiId', 'kodeProvinsi', 'namaProvinsi'])) {
                $field = 'provinsi.' . $shortField;
            } elseif (in_array($shortField, ['individuId',
                                             'kategori',
                                             'namaLengkap',
                                             'nik',
                                             'jenisKelamin',
                                             'tempatLahir',
                                             'tanggalLahir',
                                             'phone',
                                             'email',
                                             'facebook',
                                             'instagram',
                                             'pendidikan'])) {
                $field = 'biodata.' . $shortField;
            } elseif (in_array($shortField, ['id'])) {
                $field = 'programs.' . $shortField;
            } elseif (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'beasiswa.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('biodata.namaLengkap', 'asc');
        }
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['beasiswa',
                     'biodata',
                     'kelurahan',
                     'kecamatan',
                     'kabupaten',
                     'provinsi',
                     'orangtua',
                     'fakultas',
                     'jurusan',
                     'postedBy',
                     'updatedBy'])
           ->from('App:Beneficiary\BeneficiaryBeasiswa', 'beasiswa')
           ->join('beasiswa.biodata', 'biodata')
           ->join('biodata.kecamatan', 'kecamatan')
           ->join('kecamatan.kabupaten', 'kabupaten')
           ->join('kabupaten.provinsi', 'provinsi')
           ->leftJoin('biodata.kelurahan', 'kelurahan')
           ->leftJoin('beasiswa.orangTua', 'orangtua')
           ->leftJoin('beasiswa.fakultas', 'fakultas')
           ->leftJoin('beasiswa.jurusan', 'jurusan')
           ->leftJoin('beasiswa.postedBy', 'postedBy')
           ->leftJoin('beasiswa.updatedBy', 'updatedBy')
           ->leftJoin('beasiswa.verifiedBy', 'verifiedBy')
           ->leftJoin('beasiswa.approvedBy', 'approvedBy');

        return $qb;
    }

    /**
     * Create doctrine SELECT query untuk Penerima Manfaat yang ikut program beasiswa tertentu.
     *
     * @return QueryBuilder
     */
    private function createSelectQueryForProgram(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['beasiswa',
                     'biodata',
                     'kelurahan',
                     'kecamatan',
                     'kabupaten',
                     'provinsi',
                     'orangtua',
                     'fakultas',
                     'jurusan',
                     'postedBy',
                     'updatedBy'])
           ->from('App:Beneficiary\BeneficiaryBeasiswa', 'beasiswa')
           ->join('beasiswa.biodata', 'biodata')
           ->join('biodata.kecamatan', 'kecamatan')
           ->join('kecamatan.kabupaten', 'kabupaten')
           ->join('kabupaten.provinsi', 'provinsi')
           ->leftJoin('biodata.kelurahan', 'kelurahan')
           ->leftJoin('biodata.programs', 'programs')
           ->leftJoin('beasiswa.orangTua', 'orangtua')
           ->leftJoin('beasiswa.fakultas', 'fakultas')
           ->leftJoin('beasiswa.jurusan', 'jurusan')
           ->leftJoin('beasiswa.postedBy', 'postedBy')
           ->leftJoin('beasiswa.updatedBy', 'updatedBy')
           ->leftJoin('beasiswa.verifiedBy', 'verifiedBy')
           ->leftJoin('beasiswa.approvedBy', 'approvedBy');

        return $qb;
    }

}
