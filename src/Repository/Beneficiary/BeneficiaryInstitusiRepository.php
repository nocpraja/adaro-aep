<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Beneficiary;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Beneficiary\BeneficiaryInstitusi;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BeneficiaryInstitusi|null find($id, $lockMode = null, $lockVersion = null)
 * @method BeneficiaryInstitusi|null findOneBy(array $criteria, array $orderBy = null)
 * @method BeneficiaryInstitusi[]    findAll()
 * @method BeneficiaryInstitusi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 30/04/2019 4:50
 */
class BeneficiaryInstitusiRepository extends BaseEntityRepository
{

    /**
     * BeneficiaryInstitusiRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BeneficiaryInstitusi::class);
    }

    /**
     * Menampilkan daftar penerima manfaat lembaga dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      bool $forProgram = false,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar penerima manfaat PAUD dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllPaudByCriteria(array $filters = [], array $sorts = [],
                                          int $limit = 0, int $offset = 0,
                                          bool $forProgram = false,
                                          string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori = 1');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar Pesantren dan Yayasan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllPesantrenAndYayasanByCriteria(array $filters = [], array $sorts = [],
                                                         int $limit = 0, int $offset = 0,
                                                         bool $forProgram = false,
                                                         string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori > 3');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar POLTEK dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllPoltekByCriteria(array $filters = [], array $sorts = [],
                                            int $limit = 0, int $offset = 0,
                                            bool $forProgram = false,
                                            string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori = 3');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar SMK dan POLTEK dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllSmkAndPoltekByCriteria(array $filters = [], array $sorts = [],
                                                  int $limit = 0, int $offset = 0,
                                                  bool $forProgram = false,
                                                  string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori > 1')
           ->andWhere('institusi.kategori < 4');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan daftar SMK dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param boolean        $forProgram  Tampilkan beneficiary yang hanya ikut program tertentu
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllSmkByCriteria(array $filters = [], array $sorts = [],
                                         int $limit = 0, int $offset = 0,
                                         bool $forProgram = false,
                                         string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $forProgram ? $this->createSelectQueryForProgram() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        $qb->andWhere('institusi.kategori = 2');

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            $shortField = $this->getShortFieldname($filter->getProperty());
            if (in_array($shortField, ['kelurahanId', 'kodeKelurahan', 'namaKelurahan'])) {
                $field = 'kelurahan.' . $shortField;
            } elseif (in_array($shortField, ['kecamatanId', 'kodeKecamatan', 'namaKecamatan'])) {
                $field = 'kecamatan.' . $shortField;
            } elseif (in_array($shortField, ['kabupatenId', 'kodeKabupaten', 'namaKabupaten'])) {
                $field = 'kabupaten.' . $shortField;
            } elseif (in_array($shortField, ['provinsiId', 'kodeProvinsi', 'namaProvinsi'])) {
                $field = 'provinsi.' . $shortField;
            } elseif (in_array($shortField, ['id'])) {
                $field = 'programs.' . $shortField;
            } else {
                $field = 'institusi.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('institusi.namaInstitusi', 'asc');
        }
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['institusi', 'kecamatan', 'kelurahan', 'kabupaten', 'provinsi', 'postedBy', 'updatedBy'])
           ->from('App:Beneficiary\BeneficiaryInstitusi', 'institusi')
           ->join('institusi.kecamatan', 'kecamatan')
           ->join('kecamatan.kabupaten', 'kabupaten')
           ->join('kabupaten.provinsi', 'provinsi')
           ->leftJoin('institusi.kelurahan', 'kelurahan')
           ->leftJoin('institusi.postedBy', 'postedBy')
           ->leftJoin('institusi.updatedBy', 'updatedBy');

        return $qb;
    }

    /**
     * Create doctrine SELECT query untuk Penerima Manfaat yang ikut program tertentu.
     *
     * @return QueryBuilder
     */
    private function createSelectQueryForProgram(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['institusi', 'kecamatan', 'kelurahan', 'kabupaten', 'provinsi', 'postedBy', 'updatedBy'])
           ->from('App:Beneficiary\BeneficiaryInstitusi', 'institusi')
           ->join('institusi.kecamatan', 'kecamatan')
           ->join('kecamatan.kabupaten', 'kabupaten')
           ->join('kabupaten.provinsi', 'provinsi')
           ->leftJoin('institusi.kelurahan', 'kelurahan')
           ->leftJoin('institusi.programs', 'programs')
           ->leftJoin('institusi.postedBy', 'postedBy')
           ->leftJoin('institusi.updatedBy', 'updatedBy');

        return $qb;
    }

}
