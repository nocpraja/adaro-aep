<?php


namespace App\Repository\Pencairan;

use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\MasterData\ProgramCSR;
use App\Entity\Pencairan\Pencairan;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Pencairan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pencairan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pencairan[]    findAll()
 * @method Pencairan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Pencairan
 * @author  Mark Melvin
 * @since   31/05/2020, modified: 31/05/2020 02:23
 */
class PencairanRepository extends BaseEntityRepository
{

    /**
     * PencairanRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pencairan::class);
    }


    /**
     * Menampilkan data dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Create doctrine SELECT query.
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['pencairan',
            'created_by',
            'printed_by',
            'sent_by',
            'received_by',
            'approved_by',
            'rejected_by',
            'paid_by'])
            ->from('App:Pencairan\Pencairan', 'pencairan')
            ->leftJoin('pencairan.created_by', 'created_by')
            ->leftJoin('pencairan.printed_by', 'printed_by')
            ->leftJoin('pencairan.sent_by', 'sent_by')
            ->leftJoin('pencairan.received_by', 'received_by')
            ->leftJoin('pencairan.approved_by', 'approved_by')
            ->leftJoin('pencairan.rejected_by', 'rejected_by')
            ->leftJoin('pencairan.paid_by', 'paid_by');

        return $qb;
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            return $field = 'pencairan.' . $filter->getProperty();
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('pencairan.id', 'desc');
        }
    }

    public function findAllByProgramYear(ProgramCSR $program, int $tahun) :Query {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['pencairan', 'mitra', 'program' ])
            ->from('App:Pencairan\Pencairan', 'pencairan')
            ->leftJoin('pencairan.mitra', 'mitra')
            ->leftJoin('mitra.programCsr', 'program')
            ->where('program = :prog')
            ->setParameter('prog', $program)
            ->andWhere('pencairan.tahun = :tahun')
            ->setParameter('tahun', $tahun)
            ->andWhere('pencairan.status = \'PAID\'')
;

        return $this->buildQueryResult($qb, 0, 0);
    }

}
