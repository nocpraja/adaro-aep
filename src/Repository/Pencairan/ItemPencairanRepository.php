<?php

namespace App\Repository\Pencairan;

use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\MasterData\ProgramCSR;
use App\Entity\Pencairan\ItemPencairan;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ItemPencairan|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemPencairan|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemPencairan[]    findAll()
 * @method ItemPencairan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Pencairan
 * @author  Mark Melvin
 * @since   31/05/2020, modified: 31/05/2020 02:23
 */

class ItemPencairanRepository extends BaseEntityRepository
{

    /**
     * PencairanRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ItemPencairan::class);
    }


}