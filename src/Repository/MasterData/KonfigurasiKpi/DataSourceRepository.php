<?php
/**
 * Description: DataSourceRepository.phpnerated by PhpStorm.
 * @project     adaro-aep
 * @package     App\Repository\MasterData\KonfigurasiKpi
 * @author      alex
 * @created     2020-05-17, modified: 2020-05-17 18:56
 * @copyright   Copyright (c) 2020
 */

namespace App\Repository\MasterData\KonfigurasiKpi;


use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\MasterData\KonfigurasiKpi\DataSourceEntity;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Component\DataObject\SortOrFilter;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;

/**
 * Class DataSourceRepository
 * @method DataSourceEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataSourceEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataSourceEntity[]    findAll()
 * @method DataSourceEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\MasterData\KonfigurasiKpi
 */
class DataSourceRepository extends BaseEntityRepository
{
    /**
     * DataSourceRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DataSourceEntity::class);
    }

    /**
     * @param SortOrFilter[] $filters
     * @param SortOrFilter[] $sorts
     * @param integer        $limit
     * @param integer        $offset
     * @param string         $whereClause
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['source'])
            ->from('App:MasterData\KonfigurasiKpi\DataSourceEntity', 'source');

        $this->createOrderBy($qb, $sorts);
        $operator = strtolower($whereClause);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'source.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('source.sourceName', 'asc');
        }
    }

    /**
     * @param string $query
     *
     * @return mixed[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function fetchDataByQuery(string $query)
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll();
    }

}
