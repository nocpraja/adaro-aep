<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\MasterData;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\MasterData\JenisProgramCSR;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method JenisProgramCSR|null find($id, $lockMode = null, $lockVersion = null)
 * @method JenisProgramCSR|null findOneBy(array $criteria, array $orderBy = null)
 * @method JenisProgramCSR[]    findAll()
 * @method JenisProgramCSR[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\MasterData
 * @author  Ahmad Fajar
 * @since   20/08/2018, modified: 19/11/2018 23:05
 */
class JenisProgramCSRRepository extends BaseEntityRepository
{

    /**
     * JenisProgramCSRRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, JenisProgramCSR::class);
    }

    /**
     * Menampilkan data master "JenisProgramCSR" dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $qb = $this->createQueryBuilder('jenis');
        $this->createOrderBy($qb, $sorts);
        $operator = strtolower($whereClause);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            return $field = 'jenis.' . $filter->getProperty();
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('jenis.id', 'asc');
        }
    }

}
