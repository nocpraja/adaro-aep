<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\MasterData;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\MasterData\Kabupaten;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Kabupaten|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kabupaten|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kabupaten[]    findAll()
 * @method Kabupaten[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\MasterData
 * @author  Ahmad Fajar
 * @since   17/08/2018, modified: 07/09/2018 07:31
 */
class KabupatenRepository extends BaseEntityRepository
{

    /**
     * KabupatenRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Kabupaten::class);
    }

    /**
     * Menampilkan daftar "Kabupaten" dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['prov', 'kab'])
           ->from('App:MasterData\Kabupaten', 'kab')
           ->join('kab.provinsi', 'prov');

        $this->createOrderBy($qb, $sorts);
        $operator = strtolower($whereClause);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }
        if (!empty($limit)) {
            $qb->setMaxResults($limit);
        }
        if (!empty($offset)) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()
                  ->setCacheable(true)
                  ->useQueryCache(true)
                  ->setQueryCacheLifetime(self::CACHE_TTL);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            $property = strrchr($filter->getProperty(), '.');
            $shortField = $property === false ? $filter->getProperty() : ltrim($property, "\0. ");
            if (in_array($shortField, ['provinsiId', 'namaProvinsi', 'kodeProvinsi'])) {
                $field = 'prov.' . $shortField;
            } else {
                $field = 'kab.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('prov.namaProvinsi', 'asc')
               ->addOrderBy('kab.namaKabupaten', 'asc');
        }
    }

}
