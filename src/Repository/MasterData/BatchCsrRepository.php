<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\MasterData;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\MasterData\BatchCsr;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BatchCsr|null find($id, $lockMode = null, $lockVersion = null)
 * @method BatchCsr|null findOneBy(array $criteria, array $orderBy = null)
 * @method BatchCsr[]    findAll()
 * @method BatchCsr[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\MasterData
 * @author  Ahmad Fajar
 * @since   31/10/2018, modified: 08/11/2018 07:23
 */
class BatchCsrRepository extends BaseEntityRepository
{

    /**
     * BatchCsrRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BatchCsr::class);
    }

    /**
     * Menampilkan data master "Mitra ProgramCSR" dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['batch', 'programcsr', 'mitra'])
           ->from('App:MasterData\BatchCsr', 'batch')
           ->join('batch.mitra', 'mitra')
           ->join('batch.programCsr', 'programcsr');

        $this->createOrderBy($qb, $sorts);
        $operator = strtolower($whereClause);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Find duplicate Batch name.
     *
     * @param BatchCsr $search The Batch to find
     *
     * @return BatchCsr[]
     */
    public function findDuplicates(BatchCsr $search): array
    {
        $filters = QueryExpressionHelper::createCriterias(
            [
                ['property' => 'namaBatch',
                 'value'    => $search->getNamaBatch()],
                ['property' => 'programCsr.msid',
                 'value'    => $search->getProgramCsr()->getMsid()],
            ]
        );
        $query = $this->findAllByCriteria($filters);
        /** @var BatchCsr[] $results */
        $results = $query->getResult();
        $duplicates = [];

        foreach ($results as $item) {
            if ($item->getBatchId() != $search->getBatchId()) {
                $duplicates[] = $item;
            }
        }

        return $duplicates;
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            $shortField = $this->getShortFieldname($filter->getProperty());

            if (in_array($shortField, ['batchId', 'namaBatch', 'tahunAwal', 'tahunAkhir', 'managementFee'])) {
                $field = 'batch.' . $shortField;
            } elseif (in_array($shortField, ['msid', 'name', 'templateName', 'description'])) {
                $field = 'programcsr.' . $shortField;
            } else {
                $field = 'mitra.' . $shortField;
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('programcsr.name', 'asc')
               ->addOrderBy('batch.namaBatch', 'asc');
        }
    }

}
