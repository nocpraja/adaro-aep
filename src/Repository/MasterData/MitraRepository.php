<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\MasterData;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\MasterData\Mitra;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Mitra|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mitra|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mitra[]    findAll()
 * @method Mitra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\MasterData
 * @author  Ahmad Fajar
 * @since   31/10/2018, modified: 29/01/2019 1:40
 */
class MitraRepository extends BaseEntityRepository
{

    /**
     * MitraRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Mitra::class);
    }

    /**
     * Menampilkan data master "Mitra ProgramCSR" dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['mitra', 'programcsr', 'kelurahan', 'kecamatan', 'kabupaten', 'provinsi', 'postedBy', 'updatedBy'])
           ->from('App:MasterData\Mitra', 'mitra')
           ->join('mitra.kecamatan', 'kecamatan')
           ->join('kecamatan.kabupaten', 'kabupaten')
           ->join('kabupaten.provinsi', 'provinsi')
           ->leftJoin('mitra.kelurahan', 'kelurahan')
           ->leftJoin('mitra.programCsr', 'programcsr')
           ->leftJoin('mitra.postedBy', 'postedBy')
           ->leftJoin('mitra.updatedBy', 'updatedBy');

        $this->createOrderBy($qb, $sorts);
        $operator = strtolower($whereClause);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            $shortField = $this->getShortFieldname($filter->getProperty());

            if (in_array($shortField, ['kelurahanId', 'kodeKelurahan', 'namaKelurahan'])) {
                $field = 'kelurahan.' . $shortField;
            } elseif (in_array($shortField, ['kecamatanId', 'kodeKecamatan', 'namaKecamatan'])) {
                $field = 'kecamatan.' . $shortField;
            } elseif (in_array($shortField, ['kabupatenId', 'kodeKabupaten', 'namaKabupaten'])) {
                $field = 'kabupaten.' . $shortField;
            } elseif (in_array($shortField, ['provinsiId', 'kodeProvinsi', 'namaProvinsi'])) {
                $field = 'provinsi.' . $shortField;
            } elseif (in_array($shortField, ['msid', 'name', 'templateName', 'description'])) {
                $field = 'programcsr.' . $shortField;
            } else {
                $field = 'mitra.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('mitra.namaLembaga', 'asc');
        }
    }
}
