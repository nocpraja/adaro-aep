<?php

namespace App\Repository\MasterData;

use App\Entity\MasterData\BatchCsr;
use App\Entity\MasterData\BatchCsrTahun;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BatchCsrTahun|null find($id, $lockMode = null, $lockVersion = null)
 * @method BatchCsrTahun|null findOneBy(array $criteria, array $orderBy = null)
 * @method BatchCsrTahun[]    findAll()
 * @method BatchCsrTahun[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BatchCsrTahunRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BatchCsrTahun::class);
    }

     /**
      * @param BatchCsr $batch
      * @param int $tahun
      * @return BatchCsrTahun[] Returns an array of BatchCsrTahun objects
      */
    public function findByBatchTahun(BatchCsr $batch, int $tahun)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.batchCsr = :batch')
            ->andWhere('b.tahun = :tahun')
            ->setParameter('batch', $batch)
            ->setParameter('tahun', $tahun)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param BatchCsr $batch
     * @param int $tahun
     * @return BatchCsrTahun|null Returns a BatchCsrTahun objects
     * @throws
     */
    public function findOneByBatchTahun(BatchCsr $batch, int $tahun): ?BatchCsrTahun
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.batchCsr = :batch')
            ->andWhere('b.tahun = :tahun')
            ->setParameter('batch', $batch)
            ->setParameter('tahun', $tahun)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
