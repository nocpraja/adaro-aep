<?php


namespace App\Repository\Finalisasi;

use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Finalisasi\Finalisasi;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method \App\Entity\Finalisasi\Finalisasi|null find($id, $lockMode = null, $lockVersion = null)
 * @method \App\Entity\Finalisasi\Finalisasi|null findOneBy(array $criteria, array $orderBy = null)
 * @method \App\Entity\Finalisasi\Finalisasi[]    findAll()
 * @method \App\Entity\Finalisasi\Finalisasi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FinalisasiRepository extends BaseEntityRepository
{

    /**
     * HumasRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Finalisasi::class);
    }


    /**
     * Menampilkan data dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Create doctrine SELECT query.
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['finalisasi',
            'kegiatan',
            'mitra'])
            ->from('App:Finalisasi\Finalisasi', 'finalisasi')
            ->leftJoin('finalisasi.kegiatan', 'kegiatan')
            ->leftJoin('finalisasi.mitra', 'mitra');

        return $qb;
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('finalisasi.id', 'desc');
        }
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'finalisasi.' . $filter->getProperty();
            }

            return $field;
        }
    }


}