<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository;


use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\MsNumbering;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MsNumbering|null find($id, $lockMode = null, $lockVersion = null)
 * @method MsNumbering|null findOneBy(array $criteria, array $orderBy = null)
 * @method MsNumbering[]    findAll()
 * @method MsNumbering[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 23/04/2019 1:43
 */
class MsNumberingRepository extends BaseEntityRepository
{

    /**
     * MsNumberingRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MsNumbering::class);
    }

    /**
     * Find entity by its category and parameter.
     *
     * @param string $category The category to find
     * @param string $param    The parameter to find
     *
     * @return MsNumbering|null
     */
    final public function findByCategoryAndParam(string $category, string $param): ?MsNumbering
    {
        return $this->findOneBy(['category' => $category, 'param1' => $param]);
    }

    /**
     * Generate next numbering sequence.
     *
     * @param string $category Numbering category
     * @param string $param    Numbering prefix
     * @param bool   $autoSave Automatically save entity after generate new sequence or save it later manually
     *
     * @return MsNumbering
     * @throws ORMException
     */
    final public function nextSequence(string $category, string $param, bool $autoSave = true): MsNumbering
    {
        $entity = $this->findByCategoryAndParam($category, $param);

        if (is_null($entity)) {
            $entity = new MsNumbering($category, $param);
        }
        $value = $entity->getValue();
        $entity->setValue($value + 1);

        if ($autoSave) {
            $this->save($entity);
        }

        return $entity;
    }

}
