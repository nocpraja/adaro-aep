<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Security;


use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Security\AppRole;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AppRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppRole[]    findAll()
 * @method AppRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Security
 * @author  Ahmad Fajar
 * @since   03/08/2018, modified: 03/08/2018 02:23
 */
class AppRoleRepository extends BaseEntityRepository
{

    /**
     * AppRoleRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AppRole::class);
    }

}
