<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Security;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Entity\Security\UserAccount;
use App\Service\Security\AppUserInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * @method UserAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAccount[]    findAll()
 * @method UserAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Security
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 25/05/2019 0:57
 */
class UserAccountRepository extends BaseEntityRepository implements UserLoaderInterface
{

    /**
     * UserAccountRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserAccount::class);
    }

    /**
     * Menampilkan daftar Users dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['userAccount', 'userGroup', 'mitraCompany'])
           ->from('App:Security\UserAccount', 'userAccount')
            ->join('userAccount.group', 'userGroup')
            ->leftJoin('userAccount.mitraCompany', 'mitraCompany');

        $this->createOrderBy($qb, $sorts);
        $operator = strtolower($whereClause);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Finds user account for the given username.
     *
     * @param string $username The username to find
     * @param bool   $cached   Cache the result or not
     *
     * @return UserAccount
     * @throws NonUniqueResultException
     */
    public function findByUsername(string $username, bool $cached = false): ?UserAccount
    {
        $filters = QueryExpressionHelper::createCriterias(['username' => $username]);

        return $this->findAllByCriteria($filters, [], 1)
                    ->useResultCache($cached, self::CACHE_TTL_LOWEST)
                    ->getOneOrNullResult();
    }

    /**
     * Loads the user for the given username or email address.
     *
     * This method must return null if the user is not found.
     *
     * @param string $username The username or email address to find
     *
     * @return AppUserInterface|null
     * @throws NonUniqueResultException
     */
    public function loadUserByUsername($username): ?AppUserInterface
    {
        $filters = QueryExpressionHelper::createCriterias(['username' => $username, 'email' => $username]);

        return $this->findAllByCriteria($filters, [], 1, 0, 'OR')
                    ->getOneOrNullResult();
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        }

        $property = strrchr($filter->getProperty(), '.');
        $searchField = $property === false ? $filter->getProperty() : ltrim($property, "\0. ");
        if (in_array($searchField, ['groupId', 'groupName'])) {
            $field = 'userGroup.' . $searchField;
        } else {
            $field = 'userAccount.' . $filter->getProperty();
        }

        return $field;
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('userAccount.fullName', 'asc');
        }
    }

}
