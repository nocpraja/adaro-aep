<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository\Pendanaan;


use App\Component\Doctrine\ORM\TransaksiPendanaanRepository;
use App\Entity\Pendanaan\TransaksiDanaBidang;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TransaksiDanaBidang|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransaksiDanaBidang|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransaksiDanaBidang[]    findAll()
 * @method TransaksiDanaBidang[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Pendanaan
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 28/04/2019 14:18
 */
class TransaksiDanaBidangRepository extends TransaksiPendanaanRepository
{

    /**
     * TransaksiDanaBidangRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TransaksiDanaBidang::class);
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    protected function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['transaksi', 'danaBidang'])
           ->from('App:Pendanaan\TransaksiDanaBidang', 'transaksi')
           ->leftJoin('transaksi.danaBidang', 'danaBidang');

        return $qb;
    }

}
