<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository\Pendanaan;


use App\Component\Doctrine\ORM\TransaksiPendanaanRepository;
use App\Entity\Pendanaan\TransaksiDanaProgram;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TransaksiDanaProgram|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransaksiDanaProgram|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransaksiDanaProgram[]    findAll()
 * @method TransaksiDanaProgram[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Pendanaan
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 28/04/2019 14:19
 */
class TransaksiDanaProgramRepository extends TransaksiPendanaanRepository
{

    /**
     * TransaksiDanaProgramRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TransaksiDanaProgram::class);
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    protected function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['transaksi', 'danaProgram'])
           ->from('App:Pendanaan\TransaksiDanaProgram', 'transaksi')
           ->leftJoin('transaksi.danaProgram', 'danaProgram');

        return $qb;
    }

}
