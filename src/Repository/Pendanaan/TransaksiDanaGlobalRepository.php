<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository\Pendanaan;


use App\Component\Doctrine\ORM\TransaksiPendanaanRepository;
use App\Entity\Pendanaan\TransaksiDanaGlobal;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TransaksiDanaGlobal|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransaksiDanaGlobal|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransaksiDanaGlobal[]    findAll()
 * @method TransaksiDanaGlobal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Pendanaan
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 23/04/2019 2:04
 */
class TransaksiDanaGlobalRepository extends TransaksiPendanaanRepository
{

    /**
     * TransaksiDanaGlobalRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TransaksiDanaGlobal::class);
    }

}
