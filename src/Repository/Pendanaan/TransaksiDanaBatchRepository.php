<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository\Pendanaan;


use App\Component\Doctrine\ORM\TransaksiPendanaanRepository;
use App\Entity\Pendanaan\TransaksiDanaBatch;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TransaksiDanaBatch|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransaksiDanaBatch|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransaksiDanaBatch[]    findAll()
 * @method TransaksiDanaBatch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Pendanaan
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 30/04/2019 2:36
 */
class TransaksiDanaBatchRepository extends TransaksiPendanaanRepository
{

    /**
     * TransaksiDanaBatchRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TransaksiDanaBatch::class);
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    protected function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['transaksi', 'danaBatch'])
           ->from('App:Pendanaan\TransaksiDanaBatch', 'transaksi')
           ->leftJoin('transaksi.danaBatch', 'danaBatch');

        return $qb;
    }

}
