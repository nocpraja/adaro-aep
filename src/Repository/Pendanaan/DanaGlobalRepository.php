<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository\Pendanaan;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Pendanaan\DanaGlobal;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DanaGlobal|null find($id, $lockMode = null, $lockVersion = null)
 * @method DanaGlobal|null findOneBy(array $criteria, array $orderBy = null)
 * @method DanaGlobal[]    findAll()
 * @method DanaGlobal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Pendanaan
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 24/04/2019 2:58
 */
class DanaGlobalRepository extends BaseEntityRepository
{

    /**
     * DanaGlobalRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DanaGlobal::class);
    }

    /**
     * Menampilkan data pendanaan Global dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause Logic operator untuk WHERE clause
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $this->createQueryBuilder('danaGlobal');
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Menampilkan sejumlah N records terakhir.
     *
     * @param int            $limit Jumlah record untuk ditampilkan
     * @param SortOrFilter[] $sorts Sort method
     *
     * @return DanaGlobal[]
     */
    public function getLastNRecords(int $limit, array $sorts = []): array
    {
        $qb = $this->createQueryBuilder('danaGlobal');
        $this->createOrderBy($qb, $sorts);

        return $this->buildQueryResult($qb, $limit)->getResult();
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'danaGlobal.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('danaGlobal.postedDate', 'desc');
        }
    }

}
