<?php

namespace App\Repository\Kegiatan;

use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Kegiatan\KendaliKegiatan;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method KendaliKegiatan|null find($id, $lockMode = null, $lockVersion = null)
 * @method KendaliKegiatan|null findOneBy(array $criteria, array $orderBy = null)
 * @method KendaliKegiatan[]    findAll()
 * @method KendaliKegiatan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KendaliKegiatanRepository extends BaseEntityRepository
{

    /**
     * KendaliKegiatanRepostory constructor.
     *
     * @param RegistryInterface $registry
     */

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, KendaliKegiatan::class);
    }

    /**
     * Menampilkan daftar kegiatan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters Filter kriteria
     * @param SortOrFilter[] $sorts Sort method
     * @param integer $limit Jumlah record untuk ditampilkan
     * @param integer $offset Posisi record awal
     * @param string $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['kegiatan',
            'danaBatch',
            'periodeBatch',
            'mitra',
            'kendaliKegiatan'])
            ->from('App:Kegiatan\Kegiatan', 'kegiatan')
            ->join('kegiatan.danaBatch', 'danaBatch')
            ->leftJoin('kegiatan.periodeBatch', 'periodeBatch')
            ->leftJoin('periodeBatch.mitra', 'mitra')
            ->leftJoin('kegiatan.kendaliKegiatan', 'kendaliKegiatan');

        return $qb;
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('kegiatan.postedDate', 'desc');
        }
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'kegiatan.' . $filter->getProperty();
            }

            return $field;
        }
    }

}
