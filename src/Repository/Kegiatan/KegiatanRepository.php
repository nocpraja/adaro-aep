<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Kegiatan;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Kegiatan\Kegiatan;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Kegiatan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kegiatan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kegiatan[]    findAll()
 * @method Kegiatan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @package App\Repository\Kegiatan
 * @author  Ahmad Fajar
 * @since   13/11/2018, modified: 20/02/2020 23:55
 */
class KegiatanRepository extends BaseEntityRepository
{

    /**
     * KegiatanRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Kegiatan::class);
    }

    /**
     * Menampilkan daftar kegiatan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     * @param bool           $doc
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND', bool $doc = false): Query
    {
        $operator = strtolower($whereClause);
        $qb = $doc ? $this->createSelectDocQuery() : $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'kegiatan.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('kegiatan.postedDate', 'desc');
        }
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['kegiatan',
                     'danaBatch',
                     'periodeBatch',
                     'programCsr',
                     'mitra',
                     'verifiedBy',
                     'approvedBy',
                     'postedRealisasiBy',
                     'closedBy',
                     'postedBy',
                     'updatedBy'])
           ->from('App:Kegiatan\Kegiatan', 'kegiatan')
           ->join('kegiatan.danaBatch', 'danaBatch')
           ->leftJoin('kegiatan.periodeBatch', 'periodeBatch')
           ->leftJoin('periodeBatch.programCsr', 'programCsr')
           ->leftJoin('periodeBatch.mitra', 'mitra')
           ->leftJoin('kegiatan.verifiedBy', 'verifiedBy')
           ->leftJoin('kegiatan.approvedBy', 'approvedBy')
           ->leftJoin('kegiatan.postedRealisasiBy', 'postedRealisasiBy')
           ->leftJoin('kegiatan.closedBy', 'closedBy')
           ->leftJoin('kegiatan.postedBy', 'postedBy')
           ->leftJoin('kegiatan.updatedBy', 'updatedBy');

        return $qb;
    }

    /**
     * Create doctrine SELECT query kegiatan yang memiliki dokumentasi.
     * @return QueryBuilder
     */
    public function createSelectDocQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['kegiatan',
            'subkegiatan',
            'dokumentasi',
            'danaBatch',
            'periodeBatch',
            'programCsr',
            'mitra'])
            ->from('App:Kegiatan\Kegiatan', 'kegiatan')
            ->join('kegiatan.danaBatch', 'danaBatch')
            ->innerJoin('kegiatan.children', 'subkegiatan')
            ->innerJoin('subkegiatan.dokumentasi', 'dokumentasi')
            ->leftJoin('dokumentasi.verifiedBy', 'verifiedBy')
            ->leftJoin('kegiatan.periodeBatch', 'periodeBatch')
            ->leftJoin('periodeBatch.programCsr', 'programCsr')
            ->leftJoin('periodeBatch.mitra', 'mitra');

        return $qb;
    }
}
