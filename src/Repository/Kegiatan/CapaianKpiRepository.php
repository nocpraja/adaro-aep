<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Kegiatan;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Kegiatan\CapaianKpi;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CapaianKpi|null find($id, $lockMode = null, $lockVersion = null)
 * @method CapaianKpi|null findOneBy(array $criteria, array $orderBy = null)
 * @method CapaianKpi[]    findAll()
 * @method CapaianKpi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Kegiatan
 * @author  Ahmad Fajar
 * @since   27/01/2019, modified: 29/01/2019 1:47
 */
class CapaianKpiRepository extends BaseEntityRepository
{

    /**
     * CapaianKpiRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CapaianKpi::class);
    }

    /**
     * Menampilkan daftar kegiatan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'capaian.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('programCsr.msid', 'asc')
               ->addOrderBy('periodeBatch.batchId', 'asc')
               ->addOrderBy('capaian.tahun', 'asc')
               ->addOrderBy('dataRef.code', 'asc');
        }
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['capaian', 'programCsr', 'periodeBatch', 'dataRef', 'postedBy', 'updatedBy'])
           ->from('App:Kegiatan\CapaianKpi', 'capaian')
           ->join('capaian.programCsr', 'programCsr')
           ->join('capaian.periodeBatch', 'periodeBatch')
           ->join('capaian.dataRef', 'dataRef')
           ->leftJoin('capaian.postedBy', 'postedBy')
           ->leftJoin('capaian.updatedBy', 'updatedBy');

        return $qb;
    }

}
