<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Repository\Kegiatan;


use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Kegiatan\Subkegiatan;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Subkegiatan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Subkegiatan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Subkegiatan[]    findAll()
 * @method Subkegiatan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Kegiatan
 * @author  Ahmad Fajar
 * @since   13/11/2018, modified: 10/05/2019 1:42
 */
class SubkegiatanRepository extends BaseEntityRepository
{

    /**
     * SubkegiatanRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Subkegiatan::class);
    }

    /**
     * Menampilkan daftar sub-kegiatan dengan kriteria tertentu.
     *
     * @param SortOrFilter[] $filters     Filter kriteria
     * @param SortOrFilter[] $sorts       Sort method
     * @param integer        $limit       Jumlah record untuk ditampilkan
     * @param integer        $offset      Posisi record awal
     * @param string         $whereClause WHERE clause operator
     *
     * @return Query
     */
    public function findAllByCriteria(array $filters = [], array $sorts = [],
                                      int $limit = 0, int $offset = 0,
                                      string $whereClause = 'AND'): Query
    {
        $operator = strtolower($whereClause);
        $qb = $this->createSelectQuery();
        $this->createOrderBy($qb, $sorts);

        if ($operator == 'or') {
            $this->createOrWhereCriteria($qb, $filters);
        } else {
            $this->createWhereCriteria($qb, $filters);
        }

        return $this->buildQueryResult($qb, $limit, $offset);
    }

    /**
     * Parse filter property and returns a full-qualified fieldname.
     *
     * @param SortOrFilter $filter
     *
     * @return string
     */
    protected function parseField(SortOrFilter $filter): string
    {
        if ($filter->isExpression() === true) {
            return $filter->getProperty();
        } else {
            if (strpos($filter->getProperty(), '.') !== false) {
                $field = $filter->getProperty();
            } else {
                $field = 'subkegiatan.' . $filter->getProperty();
            }

            return $field;
        }
    }

    /**
     * Build <var>ORDER BY</var> query expression.
     *
     * @param QueryBuilder   $qb
     * @param SortOrFilter[] $orderBy
     */
    private function createOrderBy(QueryBuilder $qb, array $orderBy = []): void
    {
        if (!empty($orderBy)) {
            foreach ($orderBy as $item) {
                $field = $this->parseField($item);
                $sortX = new OrderBy($field, $item->getDirection());
                $qb->addOrderBy($sortX);
            }
        } else {
            $qb->addOrderBy('subkegiatan.sid', 'asc');
        }
    }

    /**
     * Create doctrine SELECT query.
     *
     * @return QueryBuilder
     */
    private function createSelectQuery(): QueryBuilder
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(['subkegiatan',
                     'kegiatan',
                     'danaBatch',
                     'periodeBatch',
                     'postedRealisasiBy',
                     'closedBy',
                     'postedBy',
                     'updatedBy'])
           ->from('App:Kegiatan\Subkegiatan', 'subkegiatan')
           ->join('subkegiatan.kegiatan', 'kegiatan')
           ->join('kegiatan.danaBatch', 'danaBatch')
           ->leftJoin('kegiatan.periodeBatch', 'periodeBatch')
           ->leftJoin('subkegiatan.postedRealisasiBy', 'postedRealisasiBy')
           ->leftJoin('subkegiatan.closedBy', 'closedBy')
           ->leftJoin('subkegiatan.postedBy', 'postedBy')
           ->leftJoin('subkegiatan.updatedBy', 'updatedBy');

        return $qb;
    }

}
