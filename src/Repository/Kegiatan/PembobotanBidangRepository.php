<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository\Kegiatan;


use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Kegiatan\PembobotanBidang;
use App\Entity\MasterData\JenisProgramCSR;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PembobotanBidang|null find($id, $lockMode = null, $lockVersion = null)
 * @method PembobotanBidang|null findOneBy(array $criteria, array $orderBy = null)
 * @method PembobotanBidang[]    findAll()
 * @method PembobotanBidang[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 23/04/2019 10:24
 */
class PembobotanBidangRepository extends BaseEntityRepository
{

    /**
     * PembobotanBidangRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PembobotanBidang::class);
    }

    /**
     * Find entity by its parent.
     *
     * @param JenisProgramCSR $bidang
     *
     * @return PembobotanBidang|null
     */
    public function findByBidang(JenisProgramCSR $bidang): ?PembobotanBidang
    {
        return $this->findOneBy(['bidang' => $bidang]);
    }

}
