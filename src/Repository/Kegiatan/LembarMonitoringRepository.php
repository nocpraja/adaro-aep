<?php

namespace App\Repository\Kegiatan;

use App\Entity\Kegiatan\LembarMonitoring;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LembarMonitoring|null find($id, $lockMode = null, $lockVersion = null)
 * @method LembarMonitoring|null findOneBy(array $criteria, array $orderBy = null)
 * @method LembarMonitoring[]    findAll()
 * @method LembarMonitoring[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LembarMonitoringRepository extends ServiceEntityRepository
{

    /**
     * LembarMonitoringRepostory constructor.
     *
     * @param RegistryInterface $registry
     */

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LembarMonitoring::class);
    }

    // /**
    //  * @return LembarMonitoring[] Returns an array of LembarMonitoring objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LembarMonitoring
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
