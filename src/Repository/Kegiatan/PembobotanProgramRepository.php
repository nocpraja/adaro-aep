<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository\Kegiatan;


use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Kegiatan\PembobotanProgram;
use App\Entity\MasterData\ProgramCSR;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PembobotanProgram|null find($id, $lockMode = null, $lockVersion = null)
 * @method PembobotanProgram|null findOneBy(array $criteria, array $orderBy = null)
 * @method PembobotanProgram[]    findAll()
 * @method PembobotanProgram[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 23/04/2019 10:24
 */
class PembobotanProgramRepository extends BaseEntityRepository
{

    /**
     * PembobotanProgramRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PembobotanProgram::class);
    }

    /**
     * Find entity by its parent.
     *
     * @param ProgramCSR $program
     *
     * @return PembobotanProgram|null
     */
    public function findByProgramCsr(ProgramCSR $program): ?PembobotanProgram
    {
        return $this->findOneBy(['program' => $program]);
    }

}
