<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Repository\Kegiatan;

use App\Component\Doctrine\ORM\BaseEntityRepository;
use App\Entity\Kegiatan\ItemAnggaran;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ItemAnggaran|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemAnggaran|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemAnggaran[]    findAll()
 * @method ItemAnggaran[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @package App\Repository\Pendanaan
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 23/04/2019 2:46
 */
class ItemAnggaranRepository extends BaseEntityRepository
{

    /**
     * ItemAnggaranRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ItemAnggaran::class);
    }

}
