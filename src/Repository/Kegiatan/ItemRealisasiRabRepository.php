<?php

namespace App\Repository\Kegiatan;

use App\Entity\Kegiatan\ItemRealisasiRab;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ItemRealisasiRab|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemRealisasiRab|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemRealisasiRab[]    findAll()
 * @method ItemRealisasiRab[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @package App\Repository\Kegiatan
 * @author  Alex Gaspersz
 * @since   28/08/2019, modified: 07/10/2019 13:45
 */
class ItemRealisasiRabRepository extends ServiceEntityRepository
{
    /**
     * ItemRealisasiRabRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ItemRealisasiRab::class);
    }

    /**
     * @param $period
     * @param $parent
     *
     * @return mixed
     */
    public function findByDate($period, $parent)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('object.realisasiDate')
            ->from('App:Kegiatan\ItemRealisasiRab', 'object')
            ->where("object.parentRab = :parentRab")
            ->andwhere("DATE_PART('month', object.realisasiDate) = :month")
            ->andwhere("DATE_PART('year', object.realisasiDate) = :year")
            ->setParameter('year', date( "Y" , strtotime($period)))
            ->setParameter('month',  date( "m" , strtotime($period) ))
            ->setParameter('parentRab', $parent);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     * @param $period
     *
     * @return mixed
     */
    public function findSubmittedByPeriod($id, $period)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('object')
            ->from('App:Kegiatan\ItemRealisasiRab', 'object')
            ->where("object.kegiatanRab = :kegiatanRab")
            ->andwhere("object.status = :submitted")
            ->andwhere("DATE_PART('month', object.realisasiDate) = :month")
            ->andwhere("DATE_PART('year', object.realisasiDate) = :year")
            ->setParameter('year', date( "Y" , strtotime($period . '-01')))
            ->setParameter('month',  date( "m" , strtotime($period . '-01') ))
            ->setParameter('kegiatanRab', $id)
            ->setParameter('submitted', WorkflowTags::REALISASI_SUBMITTED);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     * @param $period
     *
     * @return mixed
     */
    public function verifySubmittedRealisasi($id, $period)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App:Kegiatan\ItemRealisasiRab', 'object')
            ->set('object.status', ':verified')
            ->where("object.kegiatanRab = :kegiatanRab")
            ->andwhere("object.status = :submitted")
            ->andwhere("DATE_PART('month', object.realisasiDate) = :month")
            ->andwhere("DATE_PART('year', object.realisasiDate) = :year")
            ->setParameter('year', date( "Y" , strtotime($period . '-01')))
            ->setParameter('month',  date( "m" , strtotime($period . '-01') ))
            ->setParameter('kegiatanRab', $id)
            ->setParameter('submitted', WorkflowTags::REALISASI_SUBMITTED)
            ->setParameter('verified', WorkflowTags::REALISASI_VERIFIED);

        return $qb->getQuery()->execute();
    }

    /**
     * @param $id
     * @param $period
     *
     * @return mixed
     */
    public function submitRealisasi($id, $period)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App:Kegiatan\ItemRealisasiRab', 'object')
            ->set('object.status', ':submitted')
            ->where("object.kegiatanRab = :kegiatanRab")
            ->andwhere("object.status = :created")
            ->andwhere("DATE_PART('month', object.realisasiDate) = :month")
            ->andwhere("DATE_PART('year', object.realisasiDate) = :year")
            ->setParameter('year', date( "Y" , strtotime($period . '-01')))
            ->setParameter('month',  date( "m" , strtotime($period . '-01') ))
            ->setParameter('kegiatanRab', $id)
            ->setParameter('created', WorkflowTags::REALISASI_CREATED)
            ->setParameter('submitted', WorkflowTags::REALISASI_SUBMITTED);

        return $qb->getQuery()->execute();
    }

    /**
     * @param $id
     * @param $period
     * @param $reason
     *
     * @return mixed
     */
    public function rejectRealisasi($id, $period, $message)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App:Kegiatan\ItemRealisasiRab', 'object')
            ->set('object.status', ':reject')
            ->set('object.keterangan', ':reason')
            ->where("object.kegiatanRab = :kegiatanRab")
            ->andwhere("object.status = :submitted")
            ->andwhere("DATE_PART('month', object.realisasiDate) = :month")
            ->andwhere("DATE_PART('year', object.realisasiDate) = :year")
            ->setParameter('year', date( "Y" , strtotime($period . '-01')))
            ->setParameter('month',  date( "m" , strtotime($period . '-01') ))
            ->setParameter('kegiatanRab', $id)
            ->setParameter('reason', $message)
            ->setParameter('submitted', WorkflowTags::REALISASI_SUBMITTED)
            ->setParameter('reject', WorkflowTags::REALISASI_REJECTED);

        return $qb->getQuery()->execute();
    }

}
