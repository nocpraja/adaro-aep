<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait OfficePhoneTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   01/11/2018, modified: 01/11/2018 05:14
 */
trait OfficePhoneTrait
{

    /**
     * @ORM\Column(name="office_phone", type="string", length=25, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $officePhone;

    /**
     * Get office phone number.
     *
     * @return null|string
     */
    public function getOfficePhone(): ?string
    {
        return $this->officePhone;
    }

    /**
     * Set office phone number.
     *
     * @param null|string $officePhone The phone number
     * @return self
     */
    public function setOfficePhone(?string $officePhone): self
    {
        $this->officePhone = $officePhone;

        return $this;
    }

}