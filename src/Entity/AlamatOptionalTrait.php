<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use App\Entity\MasterData\Kecamatan;
use App\Entity\MasterData\Kelurahan;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait AlamatOptionalTrait untuk entity yang field alamat bersifat optional (non-mandatory).
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   2018-12-17, modified: 25/05/2019 2:52
 */
trait AlamatOptionalTrait
{

    /**
     * @ORM\Column(name="alamat", type="string", length=255, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $alamat;

    /**
     * @ORM\Column(name="rt_rw", type="string", length=11, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rtRw;

    /**
     * @ORM\Column(name="kode_pos", type="string", length=15, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $kodePos;

    /**
     * @var Kecamatan
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kecamatan")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Kecamatan")
     * @ORM\JoinColumn(name="kec_id", referencedColumnName="kec_id", nullable=true)
     * @Groups({"with_rel"})
     */
    private $kecamatan;

    /**
     * @var Kelurahan
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kelurahan")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Kelurahan")
     * @ORM\JoinColumn(name="kel_id", referencedColumnName="kel_id", nullable=true)
     * @Groups({"with_rel"})
     */
    private $kelurahan;


    /**
     * Get alamat rumah, institusi, dsb.
     *
     * @return null|string
     */
    public function getAlamat(): ?string
    {
        return $this->alamat;
    }

    /**
     * Set alamat rumah, institusi, dsb.
     *
     * @param string $alamat
     *
     * @return self
     */
    public function setAlamat(?string $alamat): self
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Get wilayah kecamatan.
     *
     * @return Kecamatan
     */
    public function getKecamatan(): ?Kecamatan
    {
        return $this->kecamatan;
    }

    /**
     * Set wilayah kecamatan.
     *
     * @param Kecamatan $kecamatan
     *
     * @return self
     */
    public function setKecamatan(?Kecamatan $kecamatan): self
    {
        $this->kecamatan = $kecamatan;

        return $this;
    }

    /**
     * Get wilayah kelurahan.
     *
     * @return Kelurahan
     */
    public function getKelurahan(): ?Kelurahan
    {
        return $this->kelurahan;
    }

    /**
     * Set wilayah kelurahan.
     *
     * @param Kelurahan $kelurahan
     *
     * @return self
     */
    public function setKelurahan(?Kelurahan $kelurahan): self
    {
        $this->kelurahan = $kelurahan;
        return $this;
    }

    /**
     * Get kode pos dari alamat.
     *
     * @return string|null
     */
    public function getKodePos(): ?string
    {
        return $this->kodePos;
    }

    /**
     * Set kode pos dari alamat.
     *
     * @param string|null $kodePos
     *
     * @return AlamatOptionalTrait
     */
    public function setKodePos(?string $kodePos): self
    {
        $this->kodePos = $kodePos;

        return $this;
    }

    /**
     * Get RT/RW dari alamat.
     *
     * @return string|null
     */
    public function getRtRw(): ?string
    {
        return $this->rtRw;
    }

    /**
     * Set RT/RW dari alamat.
     *
     * @param string|null $rtRw
     *
     * @return AlamatOptionalTrait
     */
    public function setRtRw(?string $rtRw): self
    {
        $this->rtRw = $rtRw;

        return $this;
    }

}