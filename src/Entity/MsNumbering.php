<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class MsNumbering is a data store which holds last generated code numbering.
 *
 * @ORM\Table(name="ms_numbering", options={"comment": "Table untuk membuat kode nomor urut"})
 * @ORM\Entity(repositoryClass="App\Repository\MsNumberingRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MsNumbering")
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 24/04/2019 3:57
 */
class MsNumbering
{

    /**
     * @ORM\Id()
     * @ORM\Column(name="category", type="string", length=250)
     */
    private $category;

    /**
     * @ORM\Id()
     * @ORM\Column(name="param1", type="string", length=25)
     */
    private $param1;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $param2;

    /**
     * @ORM\Column(type="integer")
     */
    private $value = 0;


    /**
     * MsNumbering constructor.
     *
     * @param string $category Numbering category
     * @param string $param    Numbering prefix
     */
    public function __construct(string $category, string $param)
    {
        $this->category = $category;
        $this->param1 = $param;
    }

    /**
     * Gets numbering category.
     *
     * @return string|null
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * Gets code numbering prefix.
     *
     * @return string|null
     */
    public function getParam1(): ?string
    {
        return $this->param1;
    }

    /**
     * Gets code numbering parameter.
     *
     * @return string|null
     */
    public function getParam2(): ?string
    {
        return $this->param2;
    }

    /**
     * Sets code numbering parameter.
     *
     * @param string|null $param2 Parameter
     *
     * @return MsNumbering
     */
    public function setParam2(?string $param2): self
    {
        $this->param2 = $param2;

        return $this;
    }

    /**
     * Gets last generated value.
     *
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->value;
    }

    /**
     * Sets last generated value.
     *
     * @param int $value
     *
     * @return MsNumbering
     */
    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Convert entity to a formatted string.
     *
     * @param int    $digits    Length of last part of auto generate number
     * @param string $separator Char separator between parts
     * @param bool   $upperCase Uppercase the output string or not
     *
     * @return string The formatted string
     */
    public function toStringCode(int $digits, string $separator = '.', bool $upperCase = true): string
    {
        $codes[] = $upperCase ? strtoupper($this->param1) : $this->param1;
        if (!empty($this->param2)) {
            $codes[] = $upperCase ? strtoupper($this->param2) : $this->param2;
        }

        $codes[] = str_pad($this->value, $digits, '0', STR_PAD_LEFT);

        return implode($separator, $codes);
    }

}
