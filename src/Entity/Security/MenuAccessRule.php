<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Security;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity MenuAccessRule is a data store which holds the registered menu and
 * its access rule for user group whithin the application.
 *
 * @ORM\Table(name="menu_access_rule", indexes={
 *     @ORM\Index(name="menu_access_x1", columns={"group_id"}),
 *     @ORM\Index(name="menu_access_x2", columns={"menu_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Security\MenuAccessRuleRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\MenuAccessRule")
 *
 * @package App\Entity\Security
 * @author  Ahmad Fajar
 * @since   30/03/2019, modified: 30/03/2019 17:46
 */
class MenuAccessRule
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="rule_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="access", type="json", nullable=true, options={
     *     "jsonb": true, "comment": "Menu access rule"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $access = [];

    /**
     * @var UserGroup
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserGroup")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserGroup")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="group_id", nullable=false, onDelete="CASCADE")
     * @Assert\NotNull(message="userGroup.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $userGroup;

    /**
     * @var MenuItem
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\MenuItem")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\MenuItem")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="menu_id", nullable=false, onDelete="CASCADE")
     * @Assert\NotNull(message="menuItem.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $menuItem;


    /**
     * Gets access rules.
     *
     * @return array|null
     */
    public function getAccess(): ?array
    {
        return $this->access;
    }

    /**
     * Sets access rules.
     *
     * @param array|null $access
     *
     * @return MenuAccessRule
     */
    public function setAccess(?array $access): self
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Gets entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Gets the menu item.
     *
     * @return MenuItem
     */
    public function getMenuItem(): MenuItem
    {
        return $this->menuItem;
    }

    /**
     * Sets the menu item.
     *
     * @param MenuItem $menuItem
     *
     * @return MenuAccessRule
     */
    public function setMenuItem(MenuItem $menuItem): self
    {
        $this->menuItem = $menuItem;

        return $this;
    }

    /**
     * Gets the user groups that are granted access rights to the menu.
     *
     * @return UserGroup
     */
    public function getUserGroup(): UserGroup
    {
        return $this->userGroup;
    }

    /**
     * Sets the user groups that are granted access rights to the menu.
     *
     * @param UserGroup $userGroup The user groups that are granted access rights
     *
     * @return MenuAccessRule
     */
    public function setUserGroup(UserGroup $userGroup): self
    {
        $this->userGroup = $userGroup;

        return $this;
    }

}
