<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Security;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="app_role", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="ak_x1_app_role", columns={"role"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Security\AppRoleRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\AppRole")
 *
 * @package App\Entity\Security
 * @author  Ahmad Fajar
 * @since   03/08/2018, modified: 03/08/2018 02:16
 */
class AppRole
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="role_title", type="string", length=250)
     */
    private $roleTitle;


    public function getId()
    {
        return $this->id;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getRoleTitle(): ?string
    {
        return $this->roleTitle;
    }

    public function setRoleTitle(string $roleTitle): self
    {
        $this->roleTitle = $roleTitle;

        return $this;
    }

}
