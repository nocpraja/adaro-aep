<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Security;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="app_setting", options={"comment": "Data konfigurasi aplikasi"})
 * @ORM\Entity(repositoryClass="App\Repository\Security\AppSettingRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\AppSetting")
 *
 * @package App\Entity\Security
 * @author  Ahmad Fajar
 * @since   23/04/2019, modified: 23/04/2019 10:19
 */
class AppSetting
{

    /**
     * @ORM\Id()
     * @ORM\Column(name="section", type="string", length=150)
     * @Groups({"without_rel"})
     */
    private $section;

    /**
     * @ORM\Id()
     * @ORM\Column(name="attribute", type="string", length=250)
     * @Groups({"without_rel"})
     */
    private $attribute;

    /**
     * @ORM\Column(name="attribute_type", type="string", length=25, options={
     *     "comment": "Valid value: [Boolean, String, Json, Integer, Float]"
     * })
     * @Groups({"without_rel"})
     */
    private $attributeType;

    /**
     * @ORM\Column(name="attribute_value", type="text")
     * @Groups({"without_rel"})
     */
    private $attributeValue;

    /**
     * @ORM\Column(name="hidden", type="boolean", options={"default": false})
     * @Groups({"without_rel"})
     */
    private $hidden = false;

    /**
     * @ORM\Column(name="position", type="integer", options={"default": 0})
     * @Groups({"without_rel"})
     */
    private $position = 0;

    /**
     * @ORM\Column(name="label", type="string", length=200, nullable=true)
     * @Groups({"without_rel"})
     */
    private $label;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"without_rel"})
     */
    private $description;


    /**
     * AppSetting constructor.
     *
     * @param string $section
     * @param string $attribute
     */
    public function __construct(string $section, string $attribute)
    {
        $this->section = $section;
        $this->attribute = $attribute;
    }

    /**
     * Get configuration's section name.
     *
     * @return string|null
     */
    public function getSection(): ?string
    {
        return $this->section;
    }

    /**
     * Get configuration's attribute.
     *
     * @return string|null
     */
    public function getAttribute(): ?string
    {
        return $this->attribute;
    }

    /**
     * Get configuration's attribute type.
     *
     * @return string|null One of these values: Boolean, String, Json, Integer, Float
     */
    public function getAttributeType(): ?string
    {
        return $this->attributeType;
    }

    /**
     * Set configuration's attribute type.
     *
     * @param string $type Valid value: [Boolean, String, Json, Integer, Float]
     *
     * @return AppSetting
     */
    public function setAttributeType(string $type): self
    {
        $this->attributeType = $type;

        return $this;
    }

    /**
     * Get configuration's attribute value.
     *
     * @return string|null
     */
    public function getAttributeValue(): ?string
    {
        return $this->attributeValue;
    }

    /**
     * Set configuration's attribute value.
     *
     * @param string $value
     *
     * @return AppSetting
     */
    public function setAttributeValue(string $value): self
    {
        $this->attributeValue = $value;

        return $this;
    }

    /**
     * Hide the configuration's attribute in the FrontEnd UI or not.
     *
     * @return bool|null
     */
    public function getHidden(): ?bool
    {
        return $this->hidden;
    }

    /**
     * Hide the configuration's attribute in the FrontEnd UI or not.
     *
     * @param bool $hidden
     *
     * @return AppSetting
     */
    public function setHidden(bool $hidden): self
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Ordered position in a section.
     *
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * Ordered position in a section.
     *
     * @param int $position
     *
     * @return AppSetting
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get text label to display in the FrontEnd UI.
     *
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * Set text label to display in the FrontEnd UI.
     *
     * @param string|null $label
     *
     * @return AppSetting
     */
    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get extra information about the configuration's attribute.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set extra information about the configuration's attribute.
     *
     * @param string|null $description
     *
     * @return AppSetting
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

}
