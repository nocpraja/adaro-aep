<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Security;


use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Service\Annotation as AppBundle;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Audit Log
 *
 * @ORM\Table(name="audit_log", indexes={
 *     @ORM\Index(name="audit_log_x1", columns={"log_message"}),
 *     @ORM\Index(name="audit_log_x2", columns={"posted_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Security\AuditLogRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\AuditLog")
 *
 * @package App\Entity\Security
 * @author  Ahmad Fajar
 * @since   18/08/2018, modified: 10/04/2019 18:17
 */
class AuditLog
{
    use PostedByFkTrait;
    use PostedDateTrait;

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="log_level", type="string", length=25)
     * @Groups({"without_rel", "with_rel"})
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="log_message", type="text")
     * @Groups({"without_rel", "with_rel"})
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_name", type="string", length=200, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $entityName;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=50, nullable=true)
     * @Gedmo\IpTraceable(on="create")
     * @Groups({"without_rel", "with_rel"})
     */
    private $ipAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="user_agent", type="string", length=500, nullable=true)
     * @AppBundle\UserAgent(on="create")
     * @Groups({"without_rel"})
     */
    private $userAgent;


    /**
     * Gets AuditLog ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Gets the log level.
     *
     * @return null|string
     */
    public function getLevel(): ?string
    {
        return $this->level;
    }

    /**
     * Sets the log level.
     *
     * @param string $level The log level
     * @return AuditLog Fluent object, returns itself
     */
    public function setLevel(string $level): self
    {
        $this->level = strtoupper($level);

        return $this;
    }

    /**
     * Gets log mesage.
     *
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Sets log message.
     *
     * @param string $message
     * @return AuditLog Fluent object, returns itself
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Gets entity or module name.
     *
     * @return null|string
     */
    public function getEntityName(): ?string
    {
        return $this->entityName;
    }

    /**
     * Sets entity or module name.
     *
     * @param null|string $entityName
     * @return AuditLog Fluent object, returns itself
     */
    public function setEntityName(?string $entityName): self
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * Gets User's IP Address.
     *
     * @return null|string
     */
    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    /**
     * Sets User's IP Address.
     *
     * @param null|string $ipAddress
     * @return AuditLog Fluent object, returns itself
     */
    public function setIpAddress(?string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Gets User's browser user agent.
     *
     * @return null|string
     */
    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    /**
     * Sets User's browser user agent.
     *
     * @param null|string $userAgent
     * @return AuditLog
     */
    public function setUserAgent(?string $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }

}
