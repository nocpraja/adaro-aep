<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Security;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity MenuItem is a data store which holds the registered menu whithin the application.
 *
 * @ORM\Table(name="menu_item", indexes={
 *     @ORM\Index(name="menu_item_x1", columns={"parent_id"}),
 *     @ORM\Index(name="menu_item_x2", columns={"route_url"}),
 *     @ORM\Index(name="menu_item_x3", columns={"route_alias"}),
 *     @ORM\Index(name="menu_item_x4", columns={"enabled"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Security\MenuItemRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\MenuItem")
 *
 * @package App\Entity\Security
 * @author  Ahmad Fajar
 * @since   30/03/2019, modified: 01/04/2019 14:16
 */
class MenuItem
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="menu_id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $menuId;

    /**
     * @ORM\Column(name="menu_label", type="string", length=250)
     * @Assert\NotBlank(message="menuLabel.notBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $menuLabel;

    /**
     * @ORM\Column(name="route_url", type="string", length=255, nullable=true, options={
     *     "comment": "VueJs routing url"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $routeUrl;

    /**
     * @ORM\Column(name="route_alias", type="string", length=255, nullable=true, options={
     *     "comment": "Symfony route name ataupun Backend API route URL"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $routeAlias;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $description;

    /**
     * @ORM\Column(name="enabled", type="boolean", options={"default": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $enabled = true;

    /**
     * @ORM\Column(name="position", type="integer", options={"default": 0})
     * @Assert\NotNull(message="position.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $position = 0;

    /**
     * @ORM\Column(name="url_type", type="smallint", options={
     *     "default": 0,
     *     "comment": "Valid values:
    0 = Route URL internal dan tampil pada MainMenu
    1 = Route URL eksternal dan tampil pada MainMenu
    2 = Route URL internal dan tidak tampil pada MainMenu
    3 = Menu parent, tampil pada MainMenu dan tanpa route URL
    4 = Menu separator, tanpa route URL"
     * })
     * @Assert\NotNull(message="urlType.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $urlType = 0;

    /**
     * @ORM\Column(name="icon", type="string", length=50, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $icon;

    /**
     * @ORM\Column(name="css_class", type="string", length=100, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $cssClass;

    /**
     * @var MenuItem
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\MenuItem")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\MenuItem", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="menu_id", nullable=true)
     * @Groups({"with_parent"})
     */
    private $parent;

    /**
     * @var MenuItem[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\MenuItem")
     * @ORM\OneToMany(targetEntity="App\Entity\Security\MenuItem", mappedBy="parent")
     * @ORM\OrderBy({"position" = "ASC"})
     * @Groups({"with_child"})
     */
    private $children;


    /**
     * MenuItem constructor.
     *
     * @param int|null $menuId Entity ID
     */
    public function __construct(int $menuId = null)
    {
        if (!empty($menuId)) {
            $this->menuId = $menuId;
        }

        $this->children = new ArrayCollection();
    }

    /**
     * Add a submenu or child menu.
     *
     * @param MenuItem $item The menu item to be added
     */
    public function addSubMenu(MenuItem $item): void
    {
        $item->setParent($this);

        if ($this->children instanceof Collection) {
            $this->children->add($item);
        } elseif (empty($this->children)) {
            $this->children = new ArrayCollection([$item]);
        } else {
            $this->children[] = $item;
        }
    }

    /**
     * Gets collection of child menu items.
     *
     * @return MenuItem[]|Collection Child menu items.
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Assign collection of child menu items.
     *
     * @param MenuItem[]|Collection $children Child menu items collection
     *
     * @return MenuItem
     */
    public function setChildren(?array $children): self
    {
        if ((!empty($children) && is_array($children)) || ($children instanceof Collection)) {
            $this->children->clear();
            foreach ($children as $item) {
                $this->addSubMenu($item);
            }
        } else {
            $this->children->clear();
        }

        return $this;
    }

    /**
     * Gets optional css classes that will be appended to the menu when it is rendered.
     *
     * @return string|null The css classes to be appended
     */
    public function getCssClass(): ?string
    {
        return $this->cssClass;
    }

    /**
     * Sets optional css classes that will be appended to the menu when it is rendered.
     *
     * @param string|null $cssClass The css classes to be appended
     *
     * @return MenuItem
     */
    public function setCssClass(?string $cssClass): self
    {
        $this->cssClass = $cssClass;

        return $this;
    }

    /**
     * Gets menu description.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Sets menu description.
     *
     * @param string|null $description
     *
     * @return MenuItem
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Checks whether a menu is enabled or disabled.
     *
     * @return bool|null
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * Enable or disable a menu.
     *
     * @param bool $enabled TRUE to enable the menu otherwise FALSE to disable the menu
     *
     * @return MenuItem
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Gets menu item's icon.
     *
     * @return string Icon name
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * Sets menu item's icon.
     *
     * @param string $icon Icon name
     *
     * @return MenuItem
     */
    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Gets entity ID.
     *
     * @return int|null
     */
    public function getMenuId(): ?int
    {
        return $this->menuId;
    }

    /**
     * Gets menu label.
     *
     * @return string|null
     */
    public function getMenuLabel(): ?string
    {
        return $this->menuLabel;
    }

    /**
     * Sets menu label.
     *
     * @param string $menuLabel The menu label
     *
     * @return MenuItem
     */
    public function setMenuLabel(string $menuLabel): self
    {
        $this->menuLabel = $menuLabel;

        return $this;
    }

    /**
     * Gets parent menu.
     *
     * @return MenuItem Parent menu
     */
    public function getParent(): ?MenuItem
    {
        return $this->parent;
    }

    /**
     * Sets parent menu.
     *
     * @param MenuItem $parent
     *
     * @return MenuItem
     */
    public function setParent(?MenuItem $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Gets menu order/position relative to its parent.
     *
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * Sets menu order/position relative to its parent.
     *
     * @param int $position Menu position
     *
     * @return MenuItem
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Gets back-end route URL or Symfony route name.
     *
     * @return string|null Route URL or Symfony route name
     */
    public function getRouteAlias(): ?string
    {
        return $this->routeAlias;
    }

    /**
     * Sets back-end route URL or Symfony route name.
     *
     * @param string|null $routeAlias Route URL or Symfony route name
     *
     * @return MenuItem
     */
    public function setRouteAlias(?string $routeAlias): self
    {
        $this->routeAlias = $routeAlias;

        return $this;
    }

    /**
     * Gets front-end or VueJs route URL.
     *
     * @return string|null Route URL
     */
    public function getRouteUrl(): ?string
    {
        return $this->routeUrl;
    }

    /**
     * Sets front-end or VueJS route URL.
     *
     * @param string|null $routeUrl Route URL
     *
     * @return MenuItem
     */
    public function setRouteUrl(?string $routeUrl): self
    {
        $this->routeUrl = $routeUrl;

        return $this;
    }

    /**
     * Gets the menu's route URL type.
     *
     * @return int|null Salah satu dari nilai berikut:<br>
     * 0 = Route URL internal dan tampil pada MainMenu,<br>
     * 1 = Route URL external dan tampil pada MainMenu,<br>
     * 2 = Route URL internal dan tidak tampil pada MainMenu<br>
     * 3 = Menu parent, tampil pada MainMenu dan tanpa route URL<br>
     * 4 = Menu separator, tanpa route URL
     */
    public function getUrlType(): ?int
    {
        return $this->urlType;
    }

    /**
     * Sets the menu's route URL type.
     *
     * @param int $urlType Salah satu dari nilai berikut:<br>
     *                     0 = Route URL internal dan tampil pada MainMenu,<br>
     *                     1 = Route URL external dan tampil pada MainMenu,<br>
     *                     2 = Route URL internal dan tidak tampil pada MainMenu,<br>
     *                     3 = Menu parent, tampil pada MainMenu dan tanpa route URL,<br>
     *                     4 = Menu separator, no route URL
     *
     * @return MenuItem
     */
    public function setUrlType(int $urlType): self
    {
        $this->urlType = $urlType;

        return $this;
    }

}
