<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Security;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserGroup
 *
 * @ORM\Table(name="user_group", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="ak_x1_user_group", columns={"group_name"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Security\UserGroupRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserGroup")
 * @UniqueEntity(fields={"groupName"}, message="UserGroup.Duplicates")
 *
 * @package App\Entity\Security
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 19/05/2019 13:50
 */
class UserGroup
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="group_id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(name="group_name", type="string", length=200)
     * @Assert\NotBlank(message="groupName.notBlank")
     * @Assert\Length(min="4")
     * @Groups({"without_rel", "with_rel"})
     */
    private $groupName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $enabled = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="protected", type="boolean", options={"default": false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $protected = false;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $description;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="json", nullable=true, options={"jsonb": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $roles = ['ROLE_USER'];


    /**
     * Get the group description.
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set the group description.
     *
     * @param string $description
     */
    public function setDescription(string $description = null): void
    {
        $this->description = $description;
    }

    /**
     * Get the group ID.
     *
     * @return int
     */
    public function getGroupId(): ?int
    {
        return $this->groupId;
    }

    /**
     * Get the group name.
     *
     * @return string
     */
    public function getGroupName(): ?string
    {
        return $this->groupName;
    }

    /**
     * Set the group name.
     *
     * @param string $groupName
     */
    public function setGroupName(string $groupName): void
    {
        $this->groupName = $groupName;
    }

    /**
     * Gets global ROLE for this group.
     *
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * Sets global ROLE for this group.
     *
     * @param array $roles
     */
    public function setRoles(?array $roles): void
    {
        if (empty($roles)) {
            $this->roles = ['ROLE_USER'];
        } elseif (!in_array('ROLE_USER', $roles)) {
            array_unshift($roles, 'ROLE_USER');
            $this->roles = $roles;
        } else {
            $this->roles = $roles;
        }
    }

    /**
     * Check whether current UserGroup has given ROLE or not.
     *
     * @param string $role The role to check
     *
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        $found = stripos($role, 'ROLE_');
        if ($found === false) {
            $role = 'ROLE_' . $role;
        }

        return in_array(strtoupper($role), $this->roles);
    }

    /**
     * Check whether current UserGroup is administrator or not.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return in_array('ROLE_ADMIN', $this->roles);
    }

    /**
     * Memeriksa status group, Enabled atau Disabled.
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Set the group state.
     *
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    /**
     * Memeriksa apakah group boleh dihapus dari sistem ataupun tidak.
     * Jika bernilai FALSE maka group boleh dihapus dari sistem.
     *
     * @return bool
     */
    public function isProtected(): bool
    {
        return $this->protected;
    }

    /**
     * Set the group protection. Untuk mencegah penghapusan dari sistem.
     *
     * @param bool $protected
     */
    public function setProtected(bool $protected): void
    {
        $this->protected = $protected;
    }

}
