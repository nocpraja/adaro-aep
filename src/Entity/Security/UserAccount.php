<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Security;


use App\Entity\MasterData\Mitra;
use App\Entity\PostedDateTrait;
use App\Service\Security\AppUserInterface;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserAccount is used as reflection of table USER_ACCOUNT.
 *
 * @ORM\Table(name="user_account", indexes={
 *     @ORM\Index(name="user_account_x1", columns={"full_name"}),
 *     @ORM\Index(name="user_account_x2", columns={"group_id"}),
 *     @ORM\Index(name="user_account_x3", columns={"email"}),
 *     @ORM\Index(name="user_account_x4", columns={"token_key"}),
 *     @ORM\Index(name="user_account_x5", columns={"mitra_id"})
 * }, uniqueConstraints={@ORM\UniqueConstraint(name="ak_x1_user_account", columns={"username"})})
 * @ORM\Entity(repositoryClass="App\Repository\Security\UserAccountRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
 * @UniqueEntity(fields={"username"}, message="UserAccount.Duplicates")
 * @UniqueEntity(fields={"email"}, message="UserAccount.EmailDuplicates")
 *
 * @package App\Entity\Security
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 25/05/2019 3:03
 */
class UserAccount implements AppUserInterface, EquatableInterface, Serializable
{

    use PostedDateTrait;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="uid", type="integer")
     * @Groups({"with_profile"})
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=50)
     * @Assert\NotBlank(message="username.notBlank")
     * @Assert\Length(min="4")
     * @Groups({"with_profile"})
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=250)
     * @Assert\NotBlank(message="fullName.notBlank")
     * @Assert\Length(min="6")
     * @Groups({"without_rel", "with_rel"})
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="user_password", type="string", length=250)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", nullable=true)
     * @Groups({"with_profile"})
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     * @Assert\Email()
     * @Groups({"with_profile"})
     */
    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $enabled = true;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="expiry", type="datetime", nullable=true)
     */
    private $expiry;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_visited", type="datetime", nullable=true)
     * @Groups({"with_profile"})
     */
    private $lastVisited;

    /**
     * @var string
     *
     * @ORM\Column(name="new_email", type="string", length=100, nullable=true, options={
     *     "comment": "Temporary email untuk menyimpan perubahan alamat email sebelum diverifikasi"
     * })
     * @Assert\Email()
     */
    private $newEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="token_key", type="string", length=250, nullable=true, options={
     *     "comment": "Token key untuk reset password atau perubahan alamat email"
     * })
     */
    private $tokenKey;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="token_expiry", type="datetime", nullable=true, options={
     *     "comment": "Token key expiration DateTime"
     * })
     */
    private $tokenExpiry;

    /**
     * @var UserGroup
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserGroup")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserGroup")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="group_id", nullable=false)
     * @Groups({"with_profile"})
     */
    private $group;

    /**
     * @var Mitra
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Mitra")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Mitra")
     * @ORM\JoinColumn(name="mitra_id", referencedColumnName="mitra_id")
     * @Groups({"with_profile"})
     */
    private $mitraCompany;

    /**
     * @var string
     */
    private $userPassword;

    /**
     * @var string
     */
    private $newPassword;


    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(): void
    {
    }

    /**
     * @return string
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     *
     * @return UserAccount
     */
    public function setAvatar(string $avatar = null): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return UserAccount
     */
    public function setEmail(string $email = null): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getExpiry(): ?DateTimeInterface
    {
        return $this->expiry;
    }

    /**
     * @param DateTimeInterface $expiry
     *
     * @return UserAccount
     */
    public function setExpiry(DateTimeInterface $expiry = null): self
    {
        $this->expiry = $expiry;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     *
     * @return UserAccount
     */
    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @return UserGroup
     */
    public function getGroup(): ?UserGroup
    {
        return $this->group;
    }

    /**
     * @param UserGroup $group
     *
     * @return UserAccount
     */
    public function setGroup(UserGroup $group): self
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastVisited(): ?DateTime
    {
        return $this->lastVisited;
    }

    /**
     * @param DateTime $lastVisited
     *
     * @return UserAccount
     */
    public function setLastVisited(DateTime $lastVisited = null): self
    {
        $this->lastVisited = $lastVisited;

        return $this;
    }

    /**
     * @return Mitra
     */
    public function getMitraCompany(): ?Mitra
    {
        return $this->mitraCompany;
    }

    /**
     * @param Mitra $mitraCompany
     *
     * @return UserAccount
     */
    public function setMitraCompany(?Mitra $mitraCompany): self
    {
        $this->mitraCompany = $mitraCompany;

        return $this;
    }

    /**
     * Temporary email untuk menyimpan perubahan alamat email sebelum diverifikasi.
     *
     * @return string
     */
    public function getNewEmail(): ?string
    {
        return $this->newEmail;
    }

    /**
     * Temporary email untuk menyimpan perubahan alamat email sebelum diverifikasi.
     *
     * @param string $email Alamat email baru
     *
     * @return UserAccount
     */
    public function setNewEmail(?string $email): self
    {
        $this->newEmail = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword(): ?string
    {
        return $this->newPassword;
    }

    /**
     * @param string $password
     *
     * @return UserAccount
     */
    public function setNewPassword(?string $password): self
    {
        $this->newPassword = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return UserAccount
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[]|string[] The user roles
     */
    public function getRoles(): array
    {
        if (null !== $this->getGroup()) {
            return $this->getGroup()->getRoles();
        }

        return ['ROLE_USER'];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * Get token key expiration datetime.
     *
     * @return DateTimeInterface
     */
    public function getTokenExpiry(): ?DateTimeInterface
    {
        return $this->tokenExpiry;
    }

    /**
     * Set token key expiration datetime.
     *
     * @param DateTimeInterface $expire Expiration datetime
     *
     * @return UserAccount
     */
    public function setTokenExpiry(?DateTimeInterface $expire): self
    {
        $this->tokenExpiry = $expire;

        return $this;
    }

    /**
     * Get token key untuk reset password atau perubahan alamat email.
     *
     * @return string
     */
    public function getTokenKey(): ?string
    {
        return $this->tokenKey;
    }

    /**
     * Set token key untuk reset password atau perubahan alamat email.
     *
     * @param string $tokenKey
     *
     * @return UserAccount
     */
    public function setTokenKey(?string $tokenKey): self
    {
        $this->tokenKey = $tokenKey;

        return $this;
    }

    /**
     * Gets entity object identifier value.
     *
     * @return int
     */
    public function getUid(): ?int
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getUserPassword(): ?string
    {
        return $this->userPassword;
    }

    /**
     * @param string $userPassword
     *
     * @return UserAccount
     */
    public function setUserPassword(?string $userPassword): self
    {
        $this->userPassword = $userPassword;

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @return string The username
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * Sets username to authenticate the UserAccount.
     *
     * @param string $username
     *
     * @return UserAccount
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Checks whether the user's account has expired or not.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @throws Exception
     * @see AccountExpiredException
     */
    public function isAccountNonExpired(): bool
    {
        if (is_null($this->expiry) || $this->expiry > new DateTime()) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        if (!empty($this->getGroup())) {
            return $this->enabled && $this->getGroup()->isEnabled();
        }

        return $this->enabled;
    }

    /**
     * Enable or disable the UserAccount.
     *
     * @param bool $enabled
     *
     * @return UserAccount
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * @param UserInterface $user
     *
     * @return bool
     * @throws Exception
     */
    public function isEqualTo(UserInterface $user): bool
    {
        if (!$user instanceof self) {
            return false;
        }

        if ($this->getUid() !== $user->getUid()) {
            return false;
        }
        if ($this->getUsername() !== $user->getUsername()) {
            return false;
        }
        if ($this->getEmail() !== $user->getEmail()) {
            return false;
        }
        if ($this->isEnabled() !== $user->isEnabled()) {
            return false;
        }
        if ($this->getExpiry() !== $user->getExpiry()) {
            return false;
        }
        if ($this->isAccountNonExpired() !== $user->isAccountNonExpired()) {
            return false;
        }

        return true;
    }

    /**
     * String representation of object.
     *
     * @link  http://php.net/manual/en/serializable.serialize.php
     *
     * @return string the string representation of the object or null
     */
    public function serialize(): string
    {
        return serialize([$this->uid,
                          $this->username,
                          $this->fullName,
                          $this->email,
                          $this->enabled,
                          $this->avatar,
                          $this->expiry]);
    }

    /**
     * Constructs the object.
     *
     * @link  http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized The string representation of the object.
     */
    public function unserialize($serialized): void
    {
        list (
            $this->uid, $this->username, $this->fullName,
            $this->email, $this->enabled, $this->avatar, $this->expiry
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }

}
