<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait LatLonTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   01/11/2018, modified: 01/11/2018 05:20
 */
trait LatLonTrait
{

    /**
     * @ORM\Column(name="latitude", type="float", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $latitude;

    /**
     * @ORM\Column(name="longitude", type="float", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $longitude;


    /**
     * Get Latitude coordinate.
     *
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * Set Latitude coordinate.
     *
     * @param float|null $latitude
     * @return self
     */
    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get Longitude coordinate.
     *
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * Set Longitude coordinate.
     *
     * @param float|null $longitude
     * @return self
     */
    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

}