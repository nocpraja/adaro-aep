<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait PostedByTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   26/02/2018, modified: 18/08/2018 16:26
 */
trait PostedByTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="posted_by", type="string", length=50, nullable=true)
     * @Gedmo\Blameable(on="create")
     * @Groups({"without_rel"})
     */
    private $postedBy;

    /**
     * Get the name of the author who made the post.
     *
     * @return string
     */
    public function getPostedBy(): ?string
    {
        return $this->postedBy;
    }

    /**
     * Set the name of the author who made the post.
     *
     * @param string $postedBy
     */
    public function setPostedBy(string $postedBy): void
    {
        $this->postedBy = $postedBy;
    }

}