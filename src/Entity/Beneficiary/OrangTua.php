<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Beneficiary;


use App\Entity\AlamatTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity untuk data orang tua penerima manfaat program Beasiswa.
 *
 * @ORM\Table(name="orang_tua", indexes={
 *     @ORM\Index(name="orang_tua_x1", columns={"kec_id"}),
 *     @ORM\Index(name="orang_tua_x2", columns={"kel_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\OrangTuaRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\OrangTua")
 *
 * @package App\Entity\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 26/10/2018 04:00
 */
class OrangTua
{

    use AlamatTrait;

    /**
     * @var BeneficiaryBeasiswa
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\BeneficiaryBeasiswa", inversedBy="orangTua")
     * @ORM\JoinColumn(name="beasiswa_id", referencedColumnName="beasiswa_id", nullable=false, onDelete="CASCADE")
     * @ORM\Cache(usage="READ_WRITE", region="Beneficiary\BeneficiaryBeasiswa")
     */
    private $beasiswa;

    /**
     * @ORM\Column(name="nama_ayah", type="string", length=250)
     * @Assert\NotBlank(message="NamaAyah.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaAyah;

    /**
     * @ORM\Column(name="nama_ibu", type="string", length=250)
     * @Assert\NotBlank(message="NamaIbu.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaIbu;

    /**
     * @ORM\Column(name="pekerjaan_ayah", type="string", length=250, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $pekerjaanAyah;

    /**
     * @ORM\Column(name="pekerjaan_ibu", type="string", length=250, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $pekerjaanIbu;

    /**
     * @ORM\Column(name="penghasilan", type="float", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $penghasilan;


    /**
     * Get parent entity.
     *
     * @return BeneficiaryBeasiswa
     */
    public function getBeasiswa(): ?BeneficiaryBeasiswa
    {
        return $this->beasiswa;
    }

    /**
     * Set parent entity.
     *
     * @param BeneficiaryBeasiswa $beasiswa
     * @return OrangTua
     */
    public function setBeasiswa(BeneficiaryBeasiswa $beasiswa): self
    {
        $this->beasiswa = $beasiswa;

        return $this;
    }

    public function getNamaAyah(): ?string
    {
        return $this->namaAyah;
    }

    public function setNamaAyah(string $namaAyah): self
    {
        $this->namaAyah = $namaAyah;

        return $this;
    }

    public function getNamaIbu(): ?string
    {
        return $this->namaIbu;
    }

    public function setNamaIbu(string $namaIbu): self
    {
        $this->namaIbu = $namaIbu;

        return $this;
    }

    public function getPekerjaanAyah(): ?string
    {
        return $this->pekerjaanAyah;
    }

    public function setPekerjaanAyah(?string $pekerjaanAyah): self
    {
        $this->pekerjaanAyah = $pekerjaanAyah;

        return $this;
    }

    public function getPekerjaanIbu(): ?string
    {
        return $this->pekerjaanIbu;
    }

    public function setPekerjaanIbu(?string $pekerjaanIbu): self
    {
        $this->pekerjaanIbu = $pekerjaanIbu;

        return $this;
    }

    public function getPenghasilan(): ?float
    {
        return $this->penghasilan;
    }

    public function setPenghasilan(?float $penghasilan): self
    {
        $this->penghasilan = $penghasilan;

        return $this;
    }
}
