<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Beneficiary;


use App\Entity\Kegiatan\IndikatorKpi;
use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\MasterData\DataReference;
use App\Entity\Pendanaan\ApprovedByTrait;
use App\Entity\Pendanaan\VerifiedByTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity penerima manfaat program Beasiswa.
 *
 * @ORM\Table(name="beneficiary_beasiswa", indexes={
 *     @ORM\Index(name="beneficiary_beasiswa_x2", columns={"fakultas_id"}),
 *     @ORM\Index(name="beneficiary_beasiswa_x3", columns={"jurusan_id"}),
 *     @ORM\Index(name="beneficiary_beasiswa_x4", columns={"nim"}),
 *     @ORM\Index(name="beneficiary_beasiswa_x5", columns={"posted_by"}),
 *     @ORM\Index(name="beneficiary_beasiswa_x6", columns={"updated_by"}),
 *     @ORM\Index(name="beasiswa_aprv_idx1", columns={"verified_by"}),
 *     @ORM\Index(name="beasiswa_aprv_idx2", columns={"approved_by"})
 * }, uniqueConstraints={@ORM\UniqueConstraint(name="beneficiary_beasiswa_x1", columns={"individu_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\BeneficiaryBeasiswaRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryBeasiswa")
 *
 * @package App\Entity\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 29/04/2020 00:42
 */
class BeneficiaryBeasiswa
{

    use PostedByFkTrait, LastUpdateByFkTrait, PostedDateTrait, LastUpdateTrait, VerifiedByTrait, ApprovedByTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="beasiswa_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $beasiswaId;

    /**
     * @ORM\Column(name="nim", type="string", length=50)
     * @Assert\NotBlank(message="NIM.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $nim;

    /**
     * @ORM\Column(name="tahun_masuk", type="integer", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahunMasuk;

    /**
     * @ORM\Column(name="jalur_masuk", type="string", length=150, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $jalurMasuk;

    /**
     * @ORM\Column(name="norek", type="string", length=100, nullable=true,
     *     options={"comment": "Nomor rekening Bank"})
     * @Assert\NotBlank(message="NomorRekening.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $nomorRekening;

    /**
     * @ORM\Column(name="nomor_cif", type="string", length=100, nullable=true,
     *     options={"comment": "Nomor CIF Rekening Bank"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $nomorCif;

    /**
     * @ORM\Column(name="nama_bank", type="string", length=250, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaBank;

    /**
     * @ORM\Column(name="kartu_sehat", type="string", length=100, nullable=true,
     *     options={"comment": "Nomor Kartu Indonesia Sehat/BPJS"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $kartuSehat;

    /**
     * @ORM\Column(name="golongan_darah", type="string", length=5, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $golonganDarah;

    /**
     * @ORM\Column(name="sekolah_asal", type="string", length=250, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $sekolahAsal;

    /**
     * @ORM\Column(name="alamat_sekolah", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $alamatSekolah;

    /**
     * @ORM\Column(name="prestasi", type="json", nullable=true, options={"jsonb": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $prestasi = [];

    /**
     * @var BeneficiaryIndividu
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryIndividu")
     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\BeneficiaryIndividu",
     *     inversedBy="beasiswa", cascade={"persist"})
     * @ORM\JoinColumn(name="individu_id", referencedColumnName="individu_id", nullable=false, onDelete="CASCADE")
     * @Assert\Valid()
     * @Groups({"without_rel", "with_rel"})
     */
    private $biodata;

    /**
     * @var DataReference
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\DataReference")
     * @ORM\JoinColumn(name="fakultas_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     * @Groups({"without_rel", "with_rel"})
     */
    private $fakultas;

    /**
     * @var DataReference
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\DataReference")
     * @ORM\JoinColumn(name="jurusan_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     * @Groups({"without_rel", "with_rel"})
     */
    private $jurusan;

    /**
     * @var OrangTua
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\OrangTua")
     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\OrangTua", mappedBy="beasiswa",
     *     cascade={"persist", "remove"})
     * @Groups({"with_rel"})
     * @Assert\Valid()
     */
    private $orangTua;

    /**
     * @var IndikatorKpi[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Kegiatan\IndikatorKpi", mappedBy="abfl",
     *     cascade={"persist", "remove"})
     * @ORM\OrderBy({"postedDate" = "ASC"})
     * @Groups({"without_rel", "with_rel"})
     *
     */
    private $kpi;

    /**
     * @ORM\Column(name="status", type="string", length=20, options={
     *     "default": "NEW",
     *     "comment": "Status valid values: NEW, UPDATED, VERIFIED, APPROVED, REJECTED"
     * })
     * @Groups({"without_rel", "with_rel"})
     */

    private $status = WorkflowTags::NEW;

    /**
     * BeneficiaryBeasiswa constructor.
     */
    public function __construct()
    {
        $this->kpi = new ArrayCollection();
    }

    /**
     * @return IndikatorKpi[]|Collection
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * @param array|null $kpi
     *
     * @return BeneficiaryBeasiswa
     */
    public function setKpi(?array $kpi): self
    {
        $this->kpi = $kpi;

        if (!empty($kpi) && is_array($kpi)) {
            $this->kpi = new ArrayCollection($kpi);
        } elseif ($kpi instanceof Collection) {
            $this->kpi = $kpi;
        } else {
            $this->kpi->clear();
        }

        return $this;

    }


    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return BeneficiaryBeasiswa
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get alamat sekolah asal.
     *
     * @return null|string
     */
    public function getAlamatSekolah(): ?string
    {
        return $this->alamatSekolah;
    }

    /**
     * Set alamat sekolah asal.
     *
     * @param null|string $alamatSekolah
     *
     * @return BeneficiaryBeasiswa
     */
    public function setAlamatSekolah(?string $alamatSekolah): self
    {
        $this->alamatSekolah = $alamatSekolah;

        return $this;
    }

    /**
     * Get the entity ID.
     *
     * @return int|null
     */
    public function getBeasiswaId(): ?int
    {
        return $this->beasiswaId;
    }

    /**
     * Get biodata penerima manfaat program Beasiswa.
     *
     * @return BeneficiaryIndividu
     */
    public function getBiodata(): ?BeneficiaryIndividu
    {
        return $this->biodata;
    }

    /**
     * Set biodata penerima manfaat program Beasiswa.
     *
     * @param BeneficiaryIndividu $biodata
     *
     * @return BeneficiaryBeasiswa
     */
    public function setBiodata(BeneficiaryIndividu $biodata): self
    {
        $this->biodata = $biodata;

        return $this;
    }

    /**
     * Get Fakultas penerima manfaat diterima kuliah.
     *
     * @return DataReference
     */
    public function getFakultas(): ?DataReference
    {
        return $this->fakultas;
    }

    /**
     * Set Fakultas penerima manfaat diterima kuliah.
     *
     * @param DataReference $fakultas
     *
     * @return BeneficiaryBeasiswa
     */
    public function setFakultas(?DataReference $fakultas): self
    {
        $this->fakultas = $fakultas;

        return $this;
    }

    /**
     * Get golongan darah penerima manfaat.
     *
     * @return null|string
     */
    public function getGolonganDarah(): ?string
    {
        return $this->golonganDarah;
    }

    /**
     * Set golongan darah penerima manfaat.
     *
     * @param null|string $golonganDarah
     *
     * @return BeneficiaryBeasiswa
     */
    public function setGolonganDarah(?string $golonganDarah): self
    {
        $this->golonganDarah = $golonganDarah;

        return $this;
    }

    /**
     * Get jalur masuk perguruan tinggi.
     *
     * @return null|string
     */
    public function getJalurMasuk(): ?string
    {
        return $this->jalurMasuk;
    }

    /**
     * Set jalur masuk perguruan tinggi.
     *
     * @param null|string $jalurMasuk
     *
     * @return BeneficiaryBeasiswa
     */
    public function setJalurMasuk(?string $jalurMasuk): self
    {
        $this->jalurMasuk = $jalurMasuk;

        return $this;
    }

    /**
     * Get Jurusan penerima manfaat diterima kuliah.
     *
     * @return DataReference
     */
    public function getJurusan(): ?DataReference
    {
        return $this->jurusan;
    }

    /**
     * Set Jurusan penerima manfaat diterima kuliah.
     *
     * @param DataReference $jurusan
     *
     * @return BeneficiaryBeasiswa
     */
    public function setJurusan(?DataReference $jurusan): self
    {
        $this->jurusan = $jurusan;

        return $this;
    }

    /**
     * Get Nomor Kartu Indonesia Sehat ataupun BPJS.
     *
     * @return null|string
     */
    public function getKartuSehat(): ?string
    {
        return $this->kartuSehat;
    }

    /**
     * Set Nomor Kartu Indonesia Sehat ataupun BPJS.
     *
     * @param null|string $kartuSehat Nomor Kartu
     *
     * @return BeneficiaryBeasiswa
     */
    public function setKartuSehat(?string $kartuSehat): self
    {
        $this->kartuSehat = $kartuSehat;

        return $this;
    }

    /**
     * Get nama Bank untuk proses transfer dana beasiswa.
     *
     * @return null|string
     */
    public function getNamaBank(): ?string
    {
        return $this->namaBank;
    }

    /**
     * Set nama Bank untuk proses transfer dana beasiswa.
     *
     * @param null|string $namaBank
     *
     * @return BeneficiaryBeasiswa
     */
    public function setNamaBank(?string $namaBank): self
    {
        $this->namaBank = $namaBank;

        return $this;
    }

    /**
     * Get Nomor Induk Mahasiswa (NIM).
     *
     * @return null|string
     */
    public function getNim(): ?string
    {
        return $this->nim;
    }

    /**
     * Set Nomor Induk Mahasiswa (NIM).
     *
     * @param string $nim Nomor Induk Mahasiswa
     *
     * @return BeneficiaryBeasiswa
     */
    public function setNim(string $nim): self
    {
        $this->nim = $nim;

        return $this;
    }

    /**
     * Get nomor CIF Rekening Bank.
     *
     * @return string
     */
    public function getNomorCif(): ?string
    {
        return $this->nomorCif;
    }

    /**
     * Set nomor CIF Rekening Bank.
     *
     * @param string $nomorCif
     *
     * @return BeneficiaryBeasiswa
     */
    public function setNomorCif(?string $nomorCif): self
    {
        $this->nomorCif = $nomorCif;

        return $this;
    }

    /**
     * Get nomor rekening bank untuk proses transfer dana beasiswa.
     *
     * @return null|string
     */
    public function getNomorRekening(): ?string
    {
        return $this->nomorRekening;
    }

    /**
     * Set nomor rekening bank untuk proses transfer dana beasiswa.
     *
     * @param null|string $nomorRekening
     *
     * @return BeneficiaryBeasiswa
     */
    public function setNomorRekening(?string $nomorRekening): self
    {
        $this->nomorRekening = $nomorRekening;

        return $this;
    }

    /**
     * Get data Orang Tua penerima manfaat program Beasiswa.
     *
     * @return OrangTua
     */
    public function getOrangTua(): ?OrangTua
    {
        return $this->orangTua;
    }

    /**
     * Set data Orang Tua penerima manfaat program Beasiswa.
     *
     * @param OrangTua $orangTua Data orang tua
     *
     * @return BeneficiaryBeasiswa
     */
    public function setOrangTua(?OrangTua $orangTua): self
    {
        $orangTua->setBeasiswa($this);
        $this->orangTua = $orangTua;

        return $this;
    }

    /**
     * Get prestasi yang pernah dicapai oleh penerima manfaat.
     *
     * @return array|null Array in key-value format
     */
    public function getPrestasi(): ?array
    {
        return $this->prestasi;
    }

    /**
     * Set prestasi yang pernah dicapai oleh penerima manfaat.
     *
     * @param array|null $prestasi Array in key-value format
     *
     * @return BeneficiaryBeasiswa
     */
    public function setPrestasi(?array $prestasi): self
    {
        $this->prestasi = $prestasi;

        return $this;
    }

    /**
     * Get nama sekolah asal sebelum masuk universitas.
     *
     * @return null|string
     */
    public function getSekolahAsal(): ?string
    {
        return $this->sekolahAsal;
    }

    /**
     * Set nama sekolah asal sebelum masuk universitas.
     *
     * @param string $sekolahAsal Nama sekolah asal
     *
     * @return BeneficiaryBeasiswa
     */
    public function setSekolahAsal(string $sekolahAsal): self
    {
        $this->sekolahAsal = $sekolahAsal;

        return $this;
    }

    /**
     * Get tahun masuk universitas.
     *
     * @return int|null
     */
    public function getTahunMasuk(): ?int
    {
        return $this->tahunMasuk;
    }

    /**
     * Set tahun masuk universitas.
     *
     * @param int $tahunMasuk
     *
     * @return BeneficiaryBeasiswa
     */
    public function setTahunMasuk(int $tahunMasuk): self
    {
        $this->tahunMasuk = $tahunMasuk;

        return $this;
    }

    /**
     * Memeriksa apakah entri sudah memiliki penilaian kpi atau belum.
     */
    public function hasKpi(): bool
    {
        return $this->getKpi()->count() > 0;
    }
}
