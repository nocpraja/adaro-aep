<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Beneficiary;


use App\Entity\AlamatOptionalTrait;
use App\Entity\EmailTrait;
use App\Entity\Kegiatan\IndikatorKpi;
use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\LatLonTrait;
use App\Entity\Pendanaan\ApprovedByTrait;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\VerifiedByTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Entity\SearchFtsTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity penerima manfaat Individu.
 * @ORM\Table(name="beneficiary_individu", indexes={
 *     @ORM\Index(name="beneficiary_individu_x1", columns={"kategori"}),
 *     @ORM\Index(name="beneficiary_individu_x3", columns={"kec_id"}),
 *     @ORM\Index(name="beneficiary_individu_x4", columns={"kel_id"}),
 *     @ORM\Index(name="beneficiary_individu_x5", columns={"institusi_id"}),
 *     @ORM\Index(name="beneficiary_individu_x6", columns={"search_fts"}, options={"USING": "gin"}),
 *     @ORM\Index(name="beneficiary_individu_x7", columns={"posted_by"}),
 *     @ORM\Index(name="beneficiary_individu_x8", columns={"updated_by"}),
 *     @ORM\Index(name="individu_aprv_idx1", columns={"verified_by"}),
 *     @ORM\Index(name="individu_aprv_idx2", columns={"approved_by"})
 * }, uniqueConstraints={@ORM\UniqueConstraint(name="beneficiary_individu_x2", columns={"nik"})},
 *     options={"comment": "Data utama penerima manfaat: Siswa, Mahasiswa, Santri, Guru, Dosen, Manajemen"})
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\BeneficiaryIndividuRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryIndividu")
 * @UniqueEntity(fields={"nik"}, message="NIK.Duplicates")
 * @package App\Entity\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 21/07/2020 02:47
 */
class BeneficiaryIndividu
{

    use AlamatOptionalTrait, EmailTrait, PostedByFkTrait, PostedDateTrait, LatLonTrait,
        LastUpdateByFkTrait, LastUpdateTrait, SearchFtsTrait, VerifiedByTrait, ApprovedByTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="individu_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $individuId;

    /**
     * @ORM\Column(name="kategori", type="smallint", options={"comment": "Valid values:
    1 = SANTRI
    2 = SISWA
    3 = MAHASISWA
    4 = USTADZ
    5 = GURU
    6 = DOSEN
    7 = MANAJEMEN
    8 = USTADZ & INSTRUKTUR"})
     * @Assert\NotBlank(message="KategoriBeneficiary.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kategori;

    /**
     * @ORM\Column(name="nama_lengkap", type="string", length=250)
     * @Assert\NotBlank(message="NamaLengkapBeneficiary.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaLengkap;

    /**
     * @ORM\Column(name="nik", type="string", length=50)
     * @Assert\NotBlank(message="NIK.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $nik;

    /**
     * @ORM\Column(name="jenis_kelamin", type="string", length=2, options={"comment": "Valid values:
    L = Laki-Laki, P = Perempuan"})
     * @Assert\NotBlank(message="JenisKelamin.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenisKelamin;

    /**
     * @ORM\Column(name="tempat_lahir", type="string", length=250)
     * @Assert\NotBlank(message="TempatLahir.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tempatLahir;

    /**
     * @ORM\Column(name="tgl_lahir", type="date")
     * @Assert\NotBlank(message="TanggalLahir.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tanggalLahir;

    /**
     * @ORM\Column(name="phone", type="string", length=15, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $phone;

    /**
     * @ORM\Column(name="facebook", type="string", length=100, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $facebook;

    /**
     * @ORM\Column(name="instagram", type="string", length=100, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $instagram;

    /**
     * @ORM\Column(name="pendidikan", type="string", length=250, nullable=true,
     *     options={"comment": "Pendidikan terakhir"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $pendidikan;

    /**
     * @ORM\Column(name="jabatan", type="string", length=250, nullable=true,
     *     options={"comment": "Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $jabatan;

    /**
     * @ORM\Column(name="status_jabatan", type="string", length=15, nullable=true,
     *     options={"comment": "Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen.
    Valid values: PNS, DTT, DTY, GTT, GTY"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $statusJabatan;

    /**
     * @ORM\Column(name="kompetensi_wirausaha", type="json", nullable=true, options={
     *     "jsonb": true, "comment": "Keahlian/Kompetensi Kewirausahaan"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $kompetensiWirausaha = [];

    /**
     * @ORM\Column(name="status_verifikasi", type="smallint",
     *     options={"default":0, "comment": "Status verifikasi data, valid values:
    0 = Baru diisi dan belum diverifikasi,
    1 = Data sudah diverifikasi"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $statusVerifikasi = 0;

    /**
     * @var BeneficiaryInstitusi
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Beneficiary\BeneficiaryInstitusi")
     * @ORM\JoinColumn(name="institusi_id", referencedColumnName="institusi_id")
     * @Groups({"without_rel", "with_rel"})
     */
    private $institusi;

    /**
     * @var BeneficiaryBeasiswa
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryBeasiswa")
     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\BeneficiaryBeasiswa", mappedBy="biodata",
     *     cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    private $beasiswa;

    /**
     * @var BeneficiaryTbe
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryTbe")
     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\BeneficiaryTbe", mappedBy="biodata",
     *     cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    private $tbe;

    /**
     * @var DanaBatch[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBatch")
     * @ORM\ManyToMany(targetEntity="App\Entity\Pendanaan\DanaBatch")
     * @ORM\JoinTable(
     *     name="individu_in_program",
     *     joinColumns={@ORM\JoinColumn(name="individu_id", referencedColumnName="individu_id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="program_id", referencedColumnName="bp_id", onDelete="CASCADE")}
     * )
     * @ORM\OrderBy({"namaProgram" = "ASC"})
     *
     * @Groups({"with_child"})
     */
    private $programs;

    /**
     * @var IndikatorKpi[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Kegiatan\IndikatorKpi", mappedBy="individu",
     *     cascade={"persist", "remove"})
     * @ORM\OrderBy({"postedDate" = "ASC"})
     * @Groups({"without_rel", "with_rel"})
     *
     */
    private $kpi;

    /**
     * @ORM\Column(name="status", type="string", length=20, options={
     *     "default": "NEW",
     *     "comment": "Status valid values: NEW, UPDATED, VERIFIED, APPROVED, REJECTED"
     * })
     * @Groups({"without_rel", "with_rel"})
     */

    private $status = WorkflowTags::NEW;

    /**
     * BeneficiaryIndividu constructor.
     */
    public function __construct()
    {
        $this->programs = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return BeneficiaryIndividu
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add a program to the collection.
     *
     * @param DanaBatch $program
     */
    public function addProgram(DanaBatch $program): void
    {
        $collection = $this->getPrograms();

        if ($collection instanceof ArrayCollection) {
            $collection->add($program);
        } elseif (empty($collection)) {
            $this->programs = new ArrayCollection();
            $this->programs->add($program);
        } else {
            $this->programs[] = $program;
        }
    }

    /**
     * Get data beasiswa, jika penerima manfaat ikut program BEASISWA.
     *
     * @return BeneficiaryBeasiswa
     */
    public function getBeasiswa(): ?BeneficiaryBeasiswa
    {
        return $this->beasiswa;
    }

    /**
     * Set data beasiswa, jika penerima manfaat ikut program BEASISWA.
     *
     * @param BeneficiaryBeasiswa $beasiswa
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setBeasiswa(?BeneficiaryBeasiswa $beasiswa): self
    {
        if (!is_null($beasiswa)) {
            $beasiswa->setBiodata($this);
        }

        $this->beasiswa = $beasiswa;

        return $this;
    }

    /**
     * Get account media sosial Facebook.
     *
     * @return null|string
     */
    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    /**
     * Set account media sosial Facebook.
     *
     * @param null|string $facebook Facebook's account
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get the entity ID.
     *
     * @return int|null
     */
    public function getIndividuId(): ?int
    {
        return $this->individuId;
    }

    /**
     * Get account media sosial Instagram.
     *
     * @return null|string
     */
    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    /**
     * Get account media sosial Instagram.
     *
     * @param null|string $instagram Instagram's account
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get Institusi tempat individu bersekolah, ataupun bekerja.
     *
     * @return BeneficiaryInstitusi
     */
    public function getInstitusi(): ?BeneficiaryInstitusi
    {
        return $this->institusi;
    }

    /**
     * Set Institusi tempat individu bersekolah, ataupun bekerja.
     *
     * @param BeneficiaryInstitusi $institusi
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setInstitusi(?BeneficiaryInstitusi $institusi): self
    {
        $this->institusi = $institusi;

        return $this;
    }

    /**
     * Get jabatan terakhir yang pernah dijabat.
     * Khusus Guru, Dosen, Ustadz, ataupun Manajemen.
     *
     * @return null|string
     */
    public function getJabatan(): ?string
    {
        return $this->jabatan;
    }

    /**
     * Jabatan terakhir yang pernah dijabat.
     * Khusus Guru, Dosen, Ustadz, ataupun Manajemen.
     *
     * @param null|string $jabatan Nama jabatan yang pernah dijabat
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setJabatan(?string $jabatan): self
    {
        $this->jabatan = $jabatan;

        return $this;
    }

    /**
     * Get jenis kelamin penerima manfaat.
     *
     * @return null|string L ataupun P, dimana L = Laki-Laki, dan P = Perempuan
     */
    public function getJenisKelamin(): ?string
    {
        return $this->jenisKelamin;
    }

    /**
     * Set jenis kelamin penerima manfaat.
     *
     * @param string $jenisKelamin Salah satu dari nilai berikut: L, P
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setJenisKelamin(string $jenisKelamin): self
    {
        $this->jenisKelamin = $jenisKelamin;

        return $this;
    }

    /**
     * Get kategori penerima manfaat.
     *
     * @return int|null Salah satu dari nilai berikut:<br>
     * 1 = SANTRI,<br>
     * 2 = SISWA,<br>
     * 3 = MAHASISWA,<br>
     * 4 = USTADZ,<br>
     * 5 = GURU,<br>
     * 6 = DOSEN,<br>
     * 7 = MANAJEMEN,<br>
     * 8 = USTADZ & INSTRUKTUR
     */
    public function getKategori(): ?int
    {
        return $this->kategori;
    }

    /**
     * Set kategori penerima manfaat.
     *
     * @param int $kategori Salah satu dari nilai berikut:<br>
     *                      1 = SANTRI,<br>
     *                      2 = SISWA,<br>
     *                      3 = MAHASISWA,<br>
     *                      4 = USTADZ,<br>
     *                      5 = GURU,<br>
     *                      6 = DOSEN,<br>
     *                      7 = MANAJEMEN,<br>
     *                      8 = USTADZ & INSTRUKTUR
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setKategori(int $kategori): self
    {
        $this->kategori = $kategori;

        return $this;
    }

    /**
     * Get keahlian/kompetensi kewirausahaan.
     *
     * @return array|null In key-value format
     */
    public function getKompetensiWirausaha(): ?array
    {
        return $this->kompetensiWirausaha;
    }

    /**
     * Assign keahlian/kompetensi kewirausahaan.
     *
     * @param array|null $kompetensiWirausaha Array in key-value format
     *
     * @return BeneficiaryIndividu
     */
    public function setKompetensiWirausaha(?array $kompetensiWirausaha): self
    {
        $this->kompetensiWirausaha = $kompetensiWirausaha;

        return $this;
    }

    /**
     * Get nama lengkap penerima manfaat.
     *
     * @return null|string
     */
    public function getNamaLengkap(): ?string
    {
        return $this->namaLengkap;
    }

    /**
     * Set nama lengkap penerima manfaat.
     *
     * @param string $namaLengkap
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setNamaLengkap(string $namaLengkap): self
    {
        $this->namaLengkap = $namaLengkap;

        return $this;
    }

    /**
     * Get Nomor Induk Kependudukan (NIK) penerima manfaat.
     *
     * @return null|string
     */
    public function getNik(): ?string
    {
        return $this->nik;
    }

    /**
     * Set Nomor Induk Kependudukan (NIK) penerima manfaat.
     *
     * @param string $nik Nomor Induk Kependudukan
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setNik(string $nik): self
    {
        $this->nik = $nik;

        return $this;
    }

    /**
     * Get pendidikan terakhir penerima manfaat.
     *
     * @return null|string
     */
    public function getPendidikan(): ?string
    {
        return $this->pendidikan;
    }

    /**
     * Set pendidikan terakhir penerima manfaat.
     *
     * @param null|string $pendidikan Pendidikan terakhir
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setPendidikan(?string $pendidikan): self
    {
        $this->pendidikan = $pendidikan;

        return $this;
    }

    /**
     * Get nomor Telpon ataupun Handphone penerima manfaat.
     *
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set nomor Telpon ataupun Handphone penerima manfaat.
     *
     * @param null|string $phone Nomor Telpon ataupun Handphone
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return DanaBatch[]|Collection
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * @param DanaBatch[]|Collection $programs
     *
     * @return BeneficiaryIndividu
     */
    public function setPrograms($programs): self
    {
        if ($programs instanceof Collection) {
            $this->programs = $programs;
        } else {
            $this->programs = new ArrayCollection($programs);
        }

        return $this;
    }

    /**
     * Get status jabatan ataupun kepegawaian.
     * Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen.
     *
     * @return null|string Salah satu dari nilai berikut: PNS, DTT, DTY, GTT, GTY
     */
    public function getStatusJabatan(): ?string
    {
        return $this->statusJabatan;
    }

    /**
     * Set status jabatan ataupun kepegawaian.
     * Khusus untuk Guru, Dosen, Ustadz, ataupun Manajemen.
     *
     * @param null|string $statusJabatan Valid values: PNS, DTT, DTY, GTT, GTY
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setStatusJabatan(?string $statusJabatan): self
    {
        $this->statusJabatan = $statusJabatan;

        return $this;
    }

    /**
     * Get status verifikasi data entri.
     *
     * @return int|null Salah satu dari nilai berikut:<br>
     *                  0 = Baru dientri dan belum diverifikasi,<br>
     *                  1 = Sudah diverifikasi
     */
    public function getStatusVerifikasi(): ?int
    {
        return $this->statusVerifikasi;
    }

    /**
     * Set status verifikasi data entri.
     *
     * @param int $statusVerifikasi Valid values: 0 = Baru, 1 = Sudah diverifikasi, 2 = Ditolak
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setStatusVerifikasi(int $statusVerifikasi): self
    {
        $this->statusVerifikasi = $statusVerifikasi;

        return $this;
    }

    /**
     * Get tanggal lahir penerima manfaat.
     *
     * @return DateTimeInterface|null
     */
    public function getTanggalLahir(): ?DateTimeInterface
    {
        return $this->tanggalLahir;
    }

    /**
     * Set tanggal lahir penerima manfaat.
     *
     * @param DateTimeInterface $tanggalLahir
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setTanggalLahir(DateTimeInterface $tanggalLahir): self
    {
        $this->tanggalLahir = $tanggalLahir;

        return $this;
    }

    /**
     * Get data TBE, jika penerima manfaat ikut program TBE.
     *
     * @return BeneficiaryTbe
     */
    public function getTbe(): BeneficiaryTbe
    {
        return $this->tbe;
    }

    /**
     * Set data TBE, jika penerima manfaat ikut program TBE.
     *
     * @param BeneficiaryTbe $tbe
     *
     * @return BeneficiaryIndividu
     */
    public function setTbe(?BeneficiaryTbe $tbe): self
    {
        if (!is_null($tbe)) {
            $tbe->setBiodata($this);
        }

        $this->tbe = $tbe;

        return $this;
    }

    /**
     * @return IndikatorKpi[]|Collection
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * @param array|null $kpi
     *
     * @return BeneficiaryIndividu
     */
    public function setKpi(?array $kpi): self
    {
        $this->kpi = $kpi;

        if (!empty($kpi) && is_array($kpi)) {
            $this->kpi = new ArrayCollection($kpi);
        } elseif ($kpi instanceof Collection) {
            $this->kpi = $kpi;
        } else {
            $this->kpi->clear();
        }

        return $this;

    }
    /**
     * Get tempat lahir penerima manfaat.
     *
     * @return null|string
     */
    public function getTempatLahir(): ?string
    {
        return $this->tempatLahir;
    }

    /**
     * Set tempat lahir penerima manfaat.
     *
     * @param string $tempatLahir
     *
     * @return BeneficiaryIndividu Fluent object, returns itself
     */
    public function setTempatLahir(string $tempatLahir): self
    {
        $this->tempatLahir = $tempatLahir;

        return $this;
    }

    /**
     * Remove a program from the collection.
     *
     * @param DanaBatch $program The program to be removed
     *
     * @return bool
     */
    public function removeProgram(DanaBatch $program): bool
    {
        return $this->getPrograms()->removeElement($program);
    }

}
