<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Beneficiary;


use App\Entity\LastUpdateTrait;
use App\Entity\PostedByTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity UnitProduksi untuk Program Keahlian/Studi SMK dan POLTEK.
 *
 * @ORM\Table(name="unit_produksi", indexes={
 *     @ORM\Index(name="unit_produksi_x1", columns={"jurusan_id"})
 * }, options={"comment": "Data utama unit produksi untuk Program Keahlian/Studi SMK dan POLTEK"})
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\UnitProduksiRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\UnitProduksi")
 *
 * @package App\Entity\Beneficiary
 * @author  Ahmad Fajar
 * @since   27/02/2019, modified: 27/05/2019 2:02
 */
class UnitProduksi
{

    use PostedByTrait, PostedDateTrait, LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint", name="produksi_id")
     * @Groups({"without_rel", "with_rel"})
     */
    private $produksiId;

    /**
     * @ORM\Column(type="string", name="nama_unit", length=250)
     * @Assert\NotBlank(message="UnitProduksi.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaUnit;

    /**
     * @ORM\Column(type="string", name="jenis", length=100, options={"comment": "Jenis unit produksi"})
     * @Assert\NotBlank(message="JenisUnitProduksi.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenis;

    /**
     * @ORM\Column(type="float", name="omset", nullable=true, options={"comment": "Omset per tahun, dalam Rupiah"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $omset;

    /**
     * @var JurusanInstitusi
     *
     * @ORM\Cache(usage="READ_WRITE", region="Beneficiary\JurusanInstitusi")
     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\JurusanInstitusi", inversedBy="unitProduksi")
     * @ORM\JoinColumn(name="jurusan_id", referencedColumnName="jurusan_id", nullable=false, onDelete="CASCADE")
     */
    private $jurusan;


    /**
     * Get jenis unit produksi.
     *
     * @return string|null
     */
    public function getJenis(): ?string
    {
        return $this->jenis;
    }

    /**
     * Set jenis unit produksi.
     *
     * @param string $jenis Jenis unit produksi.
     *
     * @return UnitProduksi
     */
    public function setJenis(string $jenis): self
    {
        $this->jenis = $jenis;

        return $this;
    }

    /**
     * Get program keahlian/studi.
     *
     * @return JurusanInstitusi
     */
    public function getJurusan(): ?JurusanInstitusi
    {
        return $this->jurusan;
    }

    /**
     * Set program keahlian/studi.
     *
     * @param JurusanInstitusi $jurusan
     *
     * @return UnitProduksi
     */
    public function setJurusan(JurusanInstitusi $jurusan): self
    {
        $this->jurusan = $jurusan;

        return $this;
    }

    /**
     * Get nama unit produksi.
     *
     * @return string|null
     */
    public function getNamaUnit(): ?string
    {
        return $this->namaUnit;
    }

    /**
     * Set nama unit produksi.
     *
     * @param string $namaUnit
     *
     * @return UnitProduksi
     */
    public function setNamaUnit(string $namaUnit): self
    {
        $this->namaUnit = $namaUnit;

        return $this;
    }

    /**
     * Get jumlah omset unit produksi per tahun, dalam rupiah.
     *
     * @return float|null
     */
    public function getOmset(): ?float
    {
        return $this->omset;
    }

    /**
     * Set jumlah omset unit produksi per tahun, dalam rupiah.
     *
     * @param float|null $omset Jumlah omset dalam rupiah
     *
     * @return UnitProduksi
     */
    public function setOmset(?float $omset): self
    {
        $this->omset = $omset;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getProduksiId(): ?int
    {
        return $this->produksiId;
    }

}
