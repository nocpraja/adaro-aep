<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Beneficiary;


use App\Entity\AlamatTrait;
use App\Entity\EmailTrait;
use App\Entity\Kegiatan\IndikatorKpi;
use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\LatLonTrait;
use App\Entity\OfficePhoneTrait;
use App\Entity\Pendanaan\ApprovedByTrait;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\VerifiedByTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Entity\SearchFtsTrait;
use App\Entity\WebsiteTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity penerima manfaat Lembaga ataupun Institusi.
 * @ORM\Table(name="beneficiary_institusi", indexes={
 *     @ORM\Index(name="beneficiary_institusi_x1", columns={"kategori"}),
 *     @ORM\Index(name="beneficiary_institusi_x2", columns={"kec_id"}),
 *     @ORM\Index(name="beneficiary_institusi_x3", columns={"kel_id"}),
 *     @ORM\Index(name="beneficiary_institusi_x5", columns={"nomor_akte"}),
 *     @ORM\Index(name="beneficiary_institusi_x6", columns={"search_fts"}, options={"USING": "gin"}),
 *     @ORM\Index(name="beneficiary_institusi_x7", columns={"posted_by"}),
 *     @ORM\Index(name="beneficiary_institusi_x8", columns={"updated_by"}),
 *     @ORM\Index(name="institusi_aprv_idx1", columns={"verified_by"}),
 *     @ORM\Index(name="institusi_aprv_idx2", columns={"approved_by"})
 * }, uniqueConstraints={@ORM\UniqueConstraint(name="beneficiary_institusi_x4", columns={"kode"})},
 *     options={"comment": "Data utama penerima manfaat: PAUD, SMK, POLTEK, Pesantren, NonPesantren"})
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\BeneficiaryInstitusiRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryInstitusi")
 * @UniqueEntity(fields={"kodeInstitusi"}, message="KodeInstitusi.Duplicates")
 * @package App\Entity\Beneficiary
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 15/07/2020 01:06
 */
class BeneficiaryInstitusi
{

    use AlamatTrait, EmailTrait, WebsiteTrait, OfficePhoneTrait,
        PostedByFkTrait, PostedDateTrait, LastUpdateByFkTrait, LastUpdateTrait,
        LatLonTrait, SearchFtsTrait, VerifiedByTrait, ApprovedByTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="institusi_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $institusiId;

    /**
     * @ORM\Column(name="kategori", type="smallint", options={"comment": "Valid values:
    1 = PAUD
    2 = SMK
    3 = POLTEK
    4 = PESANTREN
    5 = LEMBAGA KEAGAMAAN"})
     * @Assert\NotBlank(message="KategoriBeneficiary.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kategori;

    /**
     * @ORM\Column(name="swasta", type="boolean", options={"default":true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $swasta = true;

    /**
     * @ORM\Column(name="nama_institusi", type="string", length=250)
     * @Assert\NotBlank(message="NamaInstitusi.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaInstitusi;

    /**
     * @ORM\Column(name="kode", type="string", length=25, nullable=true,
     *     options={"comment": "Nomor Induk, Kode Institusi, ataupun kode penomoran lainnya"})
     * @Assert\NotBlank(message="KodeInstitusi.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kodeInstitusi;

    /**
     * @ORM\Column(name="nomor_akte", type="string", length=150, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $nomorAkte;

    /**
     * @ORM\Column(name="tahun_berdiri", type="integer", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahunBerdiri;

    /**
     * @ORM\Column(name="akreditasi", type="boolean", options={"default":false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $akreditasi;

    /**
     * @ORM\Column(name="nilai_akreditasi", type="string", length=15, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $nilaiAkreditasi;

    /**
     * @ORM\Column(name="jenis_paud", type="string", length=25, nullable=true,
     *     options={"comment": "Khusus untuk PAUD, valid values: [INTI, IMBAS]"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenisPaud;

    /**
     * @ORM\Column(name="contact_phone", type="string", length=25, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $contactPhone;

    /**
     * @ORM\Column(name="nama_pimpinan", type="string", length=250, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaPimpinan;

    /**
     * @ORM\Column(name="status_verifikasi", type="smallint",
     *     options={"default":0, "comment": "Status verifikasi data, valid values:
    0 = Baru diisi dan belum diverifikasi,
    1 = Data sudah diverifikasi"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $statusVerifikasi = 0;

    /**
     * @ORM\Column(name="status_pembinaan", type="smallint",
     *     options={"default":0, "comment": "Status pembinaan penerima manfaat, valid values:
    0 = Baru diikutsertakan
    1 = Pemantauan atau monitoring
    2 = Selesai pembinaan"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $statusPembinaan = 0;

    /**
     * @var DanaBatch[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBatch")
     * @ORM\ManyToMany(targetEntity="App\Entity\Pendanaan\DanaBatch")
     * @ORM\JoinTable(
     *     name="institusi_in_program",
     *     joinColumns={@ORM\JoinColumn(name="institusi_id", referencedColumnName="institusi_id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="program_id", referencedColumnName="bp_id", onDelete="CASCADE")}
     * )
     * @ORM\OrderBy({"namaProgram" = "ASC"})
     *
     * @Groups({"with_child"})
     */
    private $programs;

    /**
     * @var IndikatorKpi[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Kegiatan\IndikatorKpi", mappedBy="institusi",
     *     cascade={"persist", "remove"})
     * @ORM\OrderBy({"postedDate" = "ASC"})
     * @Groups({"without_rel", "with_rel"})
     *
     */
    private $kpi;

    /**
     * @ORM\Column(name="status", type="string", length=20, options={
     *     "default": "NEW",
     *     "comment": "Status valid values: NEW, UPDATED, VERIFIED, APPROVED, REJECTED"
     * })
     * @Groups({"without_rel", "with_rel"})
     */

    private $status = WorkflowTags::NEW;

    /**
     * BeneficiaryInstitusi constructor.
     */
    public function __construct()
    {
        $this->programs = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return BeneficiaryInstitusi
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add a program to the collection.
     *
     * @param DanaBatch $program
     */
    public function addProgram(DanaBatch $program): void
    {
        $collection = $this->getPrograms();

        if ($collection instanceof ArrayCollection) {
            $collection->add($program);
        } elseif (empty($collection)) {
            $this->programs = new ArrayCollection();
            $this->programs->add($program);
        } else {
            $this->programs[] = $program;
        }
    }

    /**
     * Get status akreditasi sekolah, poltek, pesantren ataupun yayasan.
     *
     * @return bool|null Jika TRUE berarti sudah terakreditasi dan FALSE adalah sebaliknya
     */
    public function getAkreditasi(): ?bool
    {
        return $this->akreditasi;
    }

    /**
     * Set status akreditasi sekolah, poltek, pesantren ataupun yayasan.
     *
     * @param bool $akreditasi Jika TRUE berarti sudah terakreditasi dan FALSE adalah sebaliknya
     *
     * @return BeneficiaryInstitusi
     */
    public function setAkreditasi(bool $akreditasi): self
    {
        $this->akreditasi = $akreditasi;

        return $this;
    }

    /**
     * Get nomor telpon ataupun handphone pimpinan institusi ataupun nomor kontak lainnya.
     *
     * @return null|string Nomor telpon ataupun handphone
     */
    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    /**
     * Get nomor telpon ataupun handphone pimpinan institusi ataupun nomor kontak lainnya.
     *
     * @param null|string $contactPhone Nomor telpon ataupun handphone
     *
     * @return BeneficiaryInstitusi
     */
    public function setContactPhone(?string $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * Get the entity ID.
     *
     * @return int|null
     */
    public function getInstitusiId(): ?int
    {
        return $this->institusiId;
    }

    /**
     * Get jenis PAUD.
     *
     * @return null|string Salah satu dari nilai berikut: INTI ataupun IMBAS
     */
    public function getJenisPaud(): ?string
    {
        return $this->jenisPaud;
    }

    /**
     * Set jenis PAUD.
     *
     * Hanya diisi bila kategori institusi adalah PAUD.
     *
     * @param null|string $jenisPaud Yakni: INTI ataupun IMBAS
     *
     * @return BeneficiaryInstitusi
     */
    public function setJenisPaud(?string $jenisPaud): self
    {
        $this->jenisPaud = $jenisPaud;

        return $this;
    }

    /**
     * Get kategori institusi penerima manfaat.
     *
     * @return int|null Salah satu dari nilai berikut:<br>
     * 1 = PAUD,<br>
     * 2 = SMK,<br>
     * 3 = POLTEK,<br>
     * 4 = PESANTREN,<br>
     * 5 = NON-PESANTREN/YAYASAN
     */
    public function getKategori(): ?int
    {
        return $this->kategori;
    }

    /**
     * Set kategori institusi penerima manfaat.
     *
     * @param int $kategori Salah satu dari nilai berikut:<br>
     *                      1 = PAUD,<br>
     *                      2 = SMK,<br>
     *                      3 = POLTEK,<br>
     *                      4 = PESANTREN,<br>
     *                      5 = NON-PESANTREN/YAYASAN
     *
     * @return BeneficiaryInstitusi
     */
    public function setKategori(int $kategori): self
    {
        $this->kategori = $kategori;

        return $this;
    }

    /**
     * Get Nomor Induk Sekolah, Kode Institusi, ataupun kode penomoran lainnya.
     *
     * @return null|string
     */
    public function getKodeInstitusi(): ?string
    {
        return $this->kodeInstitusi;
    }

    /**
     * Set Nomor Induk Sekolah, Kode Institusi, ataupun kode penomoran lainnya.
     *
     * @param null|string $kodeInstitusi Nomor Induk Sekolah, Kode Institusi atau sejenisnya
     *
     * @return BeneficiaryInstitusi
     */
    public function setKodeInstitusi(?string $kodeInstitusi): self
    {
        $this->kodeInstitusi = $kodeInstitusi;

        return $this;
    }

    /**
     * Get nama institusi penerima manfaat.
     *
     * @return null|string
     */
    public function getNamaInstitusi(): ?string
    {
        return $this->namaInstitusi;
    }

    /**
     * Set nama institusi penerima manfaat.
     *
     * @param string $namaInstitusi
     *
     * @return BeneficiaryInstitusi
     */
    public function setNamaInstitusi(string $namaInstitusi): self
    {
        $this->namaInstitusi = $namaInstitusi;

        return $this;
    }

    /**
     * Get nama lengkap pimpinan institusi atau lembaga.
     *
     * @return null|string
     */
    public function getNamaPimpinan(): ?string
    {
        return $this->namaPimpinan;
    }

    /**
     * Set nama lengkap pimpinan institusi atau lembaga.
     *
     * @param string $namaPimpinan Nama lengkap
     *
     * @return BeneficiaryInstitusi
     */
    public function setNamaPimpinan(string $namaPimpinan): self
    {
        $this->namaPimpinan = $namaPimpinan;

        return $this;
    }

    /**
     * Get nilai akreditasi sekolah, poltek, pesantren ataupun yayasan.
     *
     * @return null|string
     */
    public function getNilaiAkreditasi(): ?string
    {
        return $this->nilaiAkreditasi;
    }

    /**
     * Set nilai akreditasi sekolah, poltek, pesantren ataupun yayasan.
     *
     * Hanya diisi bila institusi sudah terakreditasi.
     *
     * @param null|string $value Nilai Akreditasi, valid values: [A, B, C]
     *
     * @return BeneficiaryInstitusi
     */
    public function setNilaiAkreditasi(?string $value): self
    {
        $this->nilaiAkreditasi = !empty($value) ? strtoupper($value) : $value;

        return $this;
    }

    /**
     * Get nomor akte pendirian institusi.
     *
     * @return null|string
     */
    public function getNomorAkte(): ?string
    {
        return $this->nomorAkte;
    }

    /**
     * Set nomor akte pendirian institusi.
     *
     * @param null|string $nomorAkte
     *
     * @return BeneficiaryInstitusi
     */
    public function setNomorAkte(?string $nomorAkte): self
    {
        $this->nomorAkte = $nomorAkte;

        return $this;
    }

    /**
     * @return DanaBatch[]|Collection
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * @param DanaBatch[]|Collection $programs
     *
     * @return BeneficiaryInstitusi
     */
    public function setPrograms($programs): self
    {
        if ($programs instanceof Collection) {
            $this->programs = $programs;
        } else {
            $this->programs = new ArrayCollection($programs);
        }

        return $this;
    }

    /**
     * Get status pembinaan institusi penerima manfaat.
     *
     * @return int|null Salah satu dari nilai berikut:<br>
     * 0 = Baru diikutsertakan,<br>
     * 1 = Pemantauan atau monitoring,<br>
     * 2 = Selesai pembinaan
     */
    public function getStatusPembinaan(): ?int
    {
        return $this->statusPembinaan;
    }

    /**
     * Set status pembinaan institusi penerima manfaat.
     *
     * @param int $statusPembinaan Salah satu dari nilai berikut:<br>
     *                             0 = Baru diikutsertakan,<br>
     *                             1 = Pemantauan atau monitoring,<br>
     *                             2 = Selesai pembinaan
     *
     * @return BeneficiaryInstitusi
     */
    public function setStatusPembinaan(int $statusPembinaan): self
    {
        $this->statusPembinaan = $statusPembinaan;

        return $this;
    }

    /**
     * Get status verifikasi data entri.
     *
     * @return int|null Salah satu dari nilai berikut:<br>
     *                  0 = Baru dientri dan belum diverifikasi,<br>
     *                  1 = Sudah diverifikasi
     */
    public function getStatusVerifikasi(): ?int
    {
        return $this->statusVerifikasi;
    }

    /**
     * Set status verifikasi data entri.
     *
     * @param int $statusVerifikasi Valid values: 0 = Baru, 1 = Sudah diverifikasi
     *
     * @return BeneficiaryInstitusi
     */
    public function setStatusVerifikasi(int $statusVerifikasi): self
    {
        $this->statusVerifikasi = $statusVerifikasi;

        return $this;
    }

    /**
     * Get status institusi, Swasta atau Negeri.
     *
     * @return bool|null If TRUE means Swasta otherwise Negeri
     */
    public function getSwasta(): ?bool
    {
        return $this->swasta;
    }

    /**
     * Set status institusi.
     *
     * @param bool $swasta If TRUE means Swasta otherwise Negeri
     *
     * @return BeneficiaryInstitusi
     */
    public function setSwasta(bool $swasta): self
    {
        $this->swasta = $swasta;

        return $this;
    }

    /**
     * Get tahun pendirian institusi.
     *
     * @return int|null
     */
    public function getTahunBerdiri(): ?int
    {
        return $this->tahunBerdiri;
    }

    /**
     * Set tahun pendirian institusi.
     *
     * @param int|null $tahunBerdiri
     *
     * @return BeneficiaryInstitusi
     */
    public function setTahunBerdiri(?int $tahunBerdiri): self
    {
        $this->tahunBerdiri = $tahunBerdiri;

        return $this;
    }

    /**
     * Remove a program from the collection.
     *
     * @param DanaBatch $program The program to be removed
     *
     * @return bool
     */
    public function removeProgram(DanaBatch $program): bool
    {
        return $this->getPrograms()->removeElement($program);
    }

    /**
     * @return IndikatorKpi[]|Collection
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * @param array|null $kpi
     *
     * @return BeneficiaryInstitusi
     */
    public function setKpi(?array $kpi): self
    {
        $this->kpi = $kpi;

        if (!empty($kpi) && is_array($kpi)) {
            $this->kpi = new ArrayCollection($kpi);
        } elseif ($kpi instanceof Collection) {
            $this->kpi = $kpi;
        } else {
            $this->kpi->clear();
        }

        return $this;

    }

    /**
     * Memeriksa apakah entri sudah memiliki penilaian kpi atau belum.
     */
    public function hasKpi(): bool
    {
        return $this->getKpi()->count() > 0;
    }
}
