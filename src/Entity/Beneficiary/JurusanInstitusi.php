<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Beneficiary;


use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Entity\SearchFtsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity Program Keahlian/Studi untuk SMK dan POLTEK.
 *
 * @ORM\Table(name="jurusan_institusi", indexes={
 *     @ORM\Index(name="jurusan_institusi_x1", columns={"institusi_id"}),
 *     @ORM\Index(name="jurusan_institusi_x2", columns={"jenis"}),
 *     @ORM\Index(name="jurusan_institusi_x3", columns={"posted_by"}),
 *     @ORM\Index(name="jurusan_institusi_x4", columns={"updated_by"}),
 *     @ORM\Index(name="jurusan_institusi_x5", columns={"search_fts"}, options={"USING": "gin"}),
 * }, options={"comment": "Data utama Program Keahlian/Studi untuk SMK dan POLTEK"})
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\JurusanInstitusiRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\JurusanInstitusi")
 *
 * @package App\Entity\Beneficiary
 * @author  Ahmad Fajar
 * @since   27/02/2019, modified: 27/05/2019 2:02
 */
class JurusanInstitusi
{

    use PostedByFkTrait, PostedDateTrait, LastUpdateByFkTrait, LastUpdateTrait;
    use SearchFtsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint", name="jurusan_id")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jurusanId;

    /**
     * @ORM\Column(type="smallint", name="jenis", options={
     *     "default": 1,
     *     "comment": "Valid values:
    1 = Program Keahlian,
    2 = Program Studi"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenis;

    /**
     * @ORM\Column(type="string", name="nama_jurusan", length=250)
     * @Assert\NotBlank(message="NamaJurusan.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaJurusan;

    /**
     * @ORM\Column(type="string", name="nilai_akreditasi", length=15, nullable=true)
     *
     * @Groups({"without_rel", "with_rel"})
     */
    private $nilaiAkreditasi;

    /**
     * @ORM\Column(type="string", name="kepala_jurusan", length=250, nullable=true)
     * @Assert\NotBlank(message="KepalaJurusan.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kepalaJurusan;

    /**
     * @ORM\Column(type="string", name="contact_phone", length=25, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="integer", name="guru_produktif", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $guruProduktif;

    /**
     * @ORM\Column(type="integer", name="guru_adaptif", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $guruAdaptif;

    /**
     * @ORM\Column(type="integer", name="guru_normatif", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $guruNormatif;

    /**
     * @ORM\Column(type="integer", name="dosen_mkdu", nullable=true, options={
     *     "comment": "Hanya diisi jika JENIS = 2 (Program Studi)"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $dosenMkdu;

    /**
     * @ORM\Column(type="integer", name="dosen_prodi", nullable=true, options={
     *     "comment": "Hanya diisi jika JENIS = 2 (Program Studi)"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $dosenProdi;

    /**
     * @ORM\Column(type="integer", name="rombel_x", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rombelX;

    /**
     * @ORM\Column(type="integer", name="rombel_xi", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rombelXi;

    /**
     * @ORM\Column(type="integer", name="rombel_xii", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rombelXii;

    /**
     * @ORM\Column(type="integer", name="rombel_xiii", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rombelXiii;

    /**
     * @ORM\Column(type="integer", name="jumlah_siswa", nullable=true, options={
     *     "comment": "Total jumlah siswa/mahasiswa terkini"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $jumlahSiswa;

    /**
     * @ORM\Column(type="json", name="peserta_didik", nullable=true, options={
     *     "jsonb": true,
     *     "comment": "Histori jumlah siswa/mahasiswa per tahun.
    Format: [{tahun: integer, jumlah: integer}]"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $pesertaDidik = [];

    /**
     * @var BeneficiaryInstitusi
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryInstitusi")
     * @ORM\ManyToOne(targetEntity="App\Entity\Beneficiary\BeneficiaryInstitusi")
     * @ORM\JoinColumn(name="institusi_id", referencedColumnName="institusi_id", nullable=false)
     * @Groups({"without_rel", "with_rel"})
     */
    private $institusi;

    /**
     * @var UnitProduksi
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\UnitProduksi")
     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\UnitProduksi",
     *     mappedBy="jurusan", cascade={"persist","merge"})
     * @Groups({"with_rel"})
     * @Assert\Valid()
     */
    private $unitProduksi;

    /**
     * @var MitraDudiJurusan[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\MitraDudiJurusan")
     * @ORM\OneToMany(targetEntity="App\Entity\Beneficiary\MitraDudiJurusan",
     *     mappedBy="jurusan", cascade={"persist","merge"})
     * @ORM\OrderBy({"postedDate" = "DESC"})
     * @Groups({"with_rel"})
     * @Assert\Valid()
     */
    private $mitraDudis;


    /**
     * JurusanInstitusi constructor.
     */
    public function __construct()
    {
        $this->mitraDudis = new ArrayCollection();
    }

    /**
     * Add mitra usaha (DUDI) to the collection.
     *
     * @param MitraDudiJurusan $mitraDudi Mitra usaha
     */
    public function addMitraDudi(MitraDudiJurusan $mitraDudi)
    {
        $mitraDudi->setJurusan($this);

        if ($this->mitraDudis instanceof Collection) {
            $this->mitraDudis->add($mitraDudi);
        } elseif (empty($this->mitraDudis)) {
            $this->mitraDudis = new ArrayCollection([$mitraDudi]);
        } else {
            $this->mitraDudis[] = $mitraDudi;
        }
    }

    /**
     * Get nomor kontak/HP kepala jurusan.
     *
     * @return string|null
     */
    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    /**
     * Set nomor kontak/HP kepala jurusan.
     *
     * @param string|null $phone Nomer telephone/HP
     *
     * @return JurusanInstitusi
     */
    public function setContactPhone(?string $phone): self
    {
        $this->contactPhone = $phone;

        return $this;
    }

    /**
     * Get jumlah dosen MKDU.
     *
     * @return int|null
     */
    public function getDosenMkdu(): ?int
    {
        return $this->dosenMkdu;
    }

    /**
     * Set jumlah dosen MKDU. Baru diisi jika <tt>jenis = 2</tt>.
     *
     * @param int|null $jumlahDosen Jumlah dosen
     *
     * @return JurusanInstitusi
     */
    public function setDosenMkdu(?int $jumlahDosen): self
    {
        $this->dosenMkdu = $jumlahDosen;

        return $this;
    }

    /**
     * Get jumlah dosen Program Studi.
     *
     * @return int|null
     */
    public function getDosenProdi(): ?int
    {
        return $this->dosenProdi;
    }

    /**
     * Set jumlah dosen Program Studi. Baru diisi jika <tt>jenis = 2</tt>.
     *
     * @param int|null $jumlahDosen Jumlah dosen
     *
     * @return JurusanInstitusi
     */
    public function setDosenProdi(?int $jumlahDosen): self
    {
        $this->dosenProdi = $jumlahDosen;

        return $this;
    }

    /**
     * Get jumlah guru adaptif.
     *
     * @return int|null
     */
    public function getGuruAdaptif(): ?int
    {
        return $this->guruAdaptif;
    }

    /**
     * Set jumlah guru adaptif.
     *
     * @param int|null $jumlahGuru Jumlah guru
     *
     * @return JurusanInstitusi
     */
    public function setGuruAdaptif(?int $jumlahGuru): self
    {
        $this->guruAdaptif = $jumlahGuru;

        return $this;
    }

    /**
     * Get jumlah guru normatif.
     *
     * @return int|null
     */
    public function getGuruNormatif(): ?int
    {
        return $this->guruNormatif;
    }

    /**
     * Set jumlah guru normatif.
     *
     * @param int|null $jumlahGuru Jumlah guru
     *
     * @return JurusanInstitusi
     */
    public function setGuruNormatif(?int $jumlahGuru): self
    {
        $this->guruNormatif = $jumlahGuru;

        return $this;
    }

    /**
     * Get jumlah guru produktif.
     *
     * @return int|null
     */
    public function getGuruProduktif(): ?int
    {
        return $this->guruProduktif;
    }

    /**
     * Set jumlah guru produktif.
     *
     * @param int|null $jumlahGuru Jumlah guru
     *
     * @return JurusanInstitusi
     */
    public function setGuruProduktif(?int $jumlahGuru): self
    {
        $this->guruProduktif = $jumlahGuru;

        return $this;
    }

    /**
     * Get beneficiary SMK/Poltek.
     *
     * @return BeneficiaryInstitusi
     */
    public function getInstitusi(): ?BeneficiaryInstitusi
    {
        return $this->institusi;
    }

    /**
     * Set benficiary SMK/Poltek.
     *
     * @param BeneficiaryInstitusi $institusi
     *
     * @return JurusanInstitusi
     */
    public function setInstitusi(BeneficiaryInstitusi $institusi): self
    {
        $this->institusi = $institusi;

        return $this;
    }

    /**
     * Get jenis/tipe jurusan.
     *
     * @return int|null Salah satu dari nilai berikut:
     *                  1 = Program Keahlian
     *                  2 = Proram Studi
     */
    public function getJenis(): ?int
    {
        return $this->jenis;
    }

    /**
     * Set jenis/tipe jurusan.
     *
     * @param int $jenis Salah satu dari nilai berikut:
     *                   1 = Program Keahlian
     *                   2 = Program Studi
     *
     * @return JurusanInstitusi
     */
    public function setJenis(int $jenis): self
    {
        $this->jenis = $jenis;

        return $this;
    }

    /**
     * Get total jumlah siswa/mahasiswa terkini (tahun terakhir).
     *
     * @return int|null
     */
    public function getJumlahSiswa(): ?int
    {
        return $this->jumlahSiswa;
    }

    /**
     * Set total jumlah siswa/mahasiswa terkini (tahun terakhir).
     *
     * @param int|null $jumlahSiswa Jumlah siswa
     *
     * @return JurusanInstitusi
     */
    public function setJumlahSiswa(?int $jumlahSiswa): self
    {
        $this->jumlahSiswa = $jumlahSiswa;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getJurusanId(): ?int
    {
        return $this->jurusanId;
    }

    /**
     * Get nama kepala jurusan.
     *
     * @return string|null
     */
    public function getKepalaJurusan(): ?string
    {
        return $this->kepalaJurusan;
    }

    /**
     * Set nama kepala jurusan.
     *
     * @param string|null $namaLengkap
     *
     * @return JurusanInstitusi
     */
    public function setKepalaJurusan(?string $namaLengkap): self
    {
        $this->kepalaJurusan = $namaLengkap;

        return $this;
    }

    /**
     * Get mitra usaha (DUDI) dari suatu progli/prodi.
     *
     * @return MitraDudiJurusan[]|Collection
     */
    public function getMitraDudis()
    {
        return $this->mitraDudis;
    }

    /**
     * Assign mitra usaha to the collection.
     *
     * @param MitraDudiJurusan[]|Collection|null $dudis
     *
     * @return JurusanInstitusi
     */
    public function setMitraDudis(?array $dudis): self
    {
        if ((!empty($dudis) && is_array($dudis)) || ($dudis instanceof Collection)) {
            $this->mitraDudis->clear();
            foreach ($dudis as $dudi) {
                $this->addMitraDudi($dudi);
            }
        } else {
            $this->mitraDudis->clear();
        }

        return $this;
    }

    /**
     * Get nama jurusan.
     *
     * @return string|null
     */
    public function getNamaJurusan(): ?string
    {
        return $this->namaJurusan;
    }

    /**
     * Define nama jurusan.
     *
     * @param string $namaJurusan
     *
     * @return JurusanInstitusi
     */
    public function setNamaJurusan(string $namaJurusan): self
    {
        $this->namaJurusan = $namaJurusan;

        return $this;
    }

    /**
     * Get nilai akreditasi suatu jurusan.
     *
     * @return string|null
     */
    public function getNilaiAkreditasi(): ?string
    {
        return $this->nilaiAkreditasi;
    }

    /**
     * Set nilai akreditasi suatu jurusan.
     *
     * @param string|null $nilaiAkreditasi
     *
     * @return JurusanInstitusi
     */
    public function setNilaiAkreditasi(?string $nilaiAkreditasi): self
    {
        $this->nilaiAkreditasi = $nilaiAkreditasi;

        return $this;
    }

    /**
     * Get histori jumlah siswa/mahasiswa per tahun.
     *
     * @return array|null
     */
    public function getPesertaDidik(): ?array
    {
        return $this->pesertaDidik;
    }

    /**
     * Assign histori jumlah siswa/mahasiswa per tahun.
     *
     * @param array|null $pesertaDidik Format: <code>[{tahun: integer, jumlah: integer}]</code>
     *
     * @return JurusanInstitusi
     */
    public function setPesertaDidik(?array $pesertaDidik): self
    {
        $this->pesertaDidik = $pesertaDidik;

        return $this;
    }

    /**
     * Get jumlah siswa rombongan belajar kelas X.
     *
     * @return int|null
     */
    public function getRombelX(): ?int
    {
        return $this->rombelX;
    }

    /**
     * Set jumlah siswa rombongan belajar kelas X.
     *
     * @param int|null $jumlahSiswa Jumlah siswa
     *
     * @return JurusanInstitusi
     */
    public function setRombelX(?int $jumlahSiswa): self
    {
        $this->rombelX = $jumlahSiswa;

        return $this;
    }

    /**
     * Get jumlah siswa rombongan belajar kelas XI.
     *
     * @return int|null
     */
    public function getRombelXi(): ?int
    {
        return $this->rombelXi;
    }

    /**
     * Set jumlah siswa rombongan belajar kelas XI.
     *
     * @param int|null $jumlahSiswa Jumlah siswa
     *
     * @return JurusanInstitusi
     */
    public function setRombelXi(?int $jumlahSiswa): self
    {
        $this->rombelXi = $jumlahSiswa;

        return $this;
    }

    /**
     * Get jumlah siswa rombongan belajar kelas XII.
     *
     * @return int|null
     */
    public function getRombelXii(): ?int
    {
        return $this->rombelXii;
    }

    /**
     * Set jumlah siswa rombongan belajar kelas XII.
     *
     * @param int|null $jumlahSiswa Jumlah siswa
     *
     * @return JurusanInstitusi
     */
    public function setRombelXii(?int $jumlahSiswa): self
    {
        $this->rombelXii = $jumlahSiswa;

        return $this;
    }

    /**
     * Get jumlah siswa rombongan belajar kelas XIII.
     *
     * @return int|null
     */
    public function getRombelXiii(): ?int
    {
        return $this->rombelXiii;
    }

    /**
     * Set jumlah siswa rombongan belajar kelas XIII.
     *
     * @param int|null $jumlahSiswa Jumlah siswa
     *
     * @return JurusanInstitusi
     */
    public function setRombelXiii(?int $jumlahSiswa): self
    {
        $this->rombelXiii = $jumlahSiswa;

        return $this;
    }

    /**
     * Get unit produksi dari SMK/Poltek.
     *
     * @return UnitProduksi
     */
    public function getUnitProduksi(): ?UnitProduksi
    {
        return $this->unitProduksi;
    }

    /**
     * Set unit produksi dari SMK/Poltek.
     *
     * @param UnitProduksi $unitProduksi
     *
     * @return JurusanInstitusi
     */
    public function setUnitProduksi(?UnitProduksi $unitProduksi): self
    {
        $unitProduksi->setJurusan($this);
        $this->unitProduksi = $unitProduksi;

        return $this;
    }

}
