<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Beneficiary;


use App\Entity\LastUpdateTrait;
use App\Entity\PostedByTrait;
use App\Entity\PostedDateTrait;
use App\Entity\SearchFtsTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity Mitra DUDI untuk Program Keahlian/Studi SMK dan POLTEK.
 *
 * @ORM\Table(name="mitra_dudi_jurusan", indexes={
 *     @ORM\Index(name="mitra_dudi_x1", columns={"jurusan_id"}),
 *     @ORM\Index(name="mitra_dudi_x2", columns={"jenis_usaha"}),
 *     @ORM\Index(name="mitra_dudi_x3", columns={"search_fts"}, options={"USING": "gin"})
 * }, options={"comment": "Data utama Mitra DUDI untuk Program Keahlian/Studi SMK dan POLTEK"})
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\MitraDudiJurusanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\MitraDudiJurusan")
 *
 * @package App\Entity\Beneficiary
 * @author  Ahmad Fajar
 * @since   27/02/2019, modified: 27/05/2019 2:02
 */
class MitraDudiJurusan
{

    use PostedByTrait, PostedDateTrait, LastUpdateTrait;
    use SearchFtsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint", name="dudi_id")
     * @Groups({"without_rel", "with_rel"})
     */
    private $dudiId;

    /**
     * @ORM\Column(type="string", name="nama_usaha", length=250)
     * @Assert\NotBlank(message="MitraDudi.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaUsaha;

    /**
     * @ORM\Column(type="string", name="jenis_usaha", length=150)
     * @Assert\NotBlank(message="JenisUsaha.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenisUsaha;

    /**
     * @ORM\Column(type="string", name="nama_pimpinan", length=250)
     * @Assert\NotBlank(message="NamaPimpinan.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaPimpinan;

    /**
     * @ORM\Column(type="string", name="contact_phone", length=25, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="text", name="alamat", nullable=true)
     * @Assert\NotBlank(message="Alamat.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $alamat;

    /**
     * @var JurusanInstitusi
     *
     * @ORM\Cache(usage="READ_WRITE", region="Beneficiary\JurusanInstitusi")
     * @ORM\ManyToOne(targetEntity="App\Entity\Beneficiary\JurusanInstitusi", inversedBy="mitraDudis")
     * @ORM\JoinColumn(name="jurusan_id", referencedColumnName="jurusan_id", nullable=false, onDelete="CASCADE")
     */
    private $jurusan;


    /**
     * Get alamat lengkap mitra usaha.
     *
     * @return string|null
     */
    public function getAlamat(): ?string
    {
        return $this->alamat;
    }

    /**
     * Set alamat lengkap mitra usaha.
     *
     * @param string|null $alamat Alamat lengkap
     *
     * @return MitraDudiJurusan
     */
    public function setAlamat(?string $alamat): self
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Get nomor kontak/HP pimpinan/direktur mitra usaha.
     *
     * @return string|null
     */
    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    /**
     * Get nomor kontak/HP pimpinan/direktur.
     *
     * @param string|null $phone Nomor telephone/HP
     *
     * @return MitraDudiJurusan
     */
    public function setContactPhone(?string $phone): self
    {
        $this->contactPhone = $phone;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getDudiId(): ?int
    {
        return $this->dudiId;
    }

    /**
     * Get jenis usaha/industri.
     *
     * @return string|null
     */
    public function getJenisUsaha(): ?string
    {
        return $this->jenisUsaha;
    }

    /**
     * Set jenis usaha/industri.
     *
     * @param string $jenisUsaha
     *
     * @return MitraDudiJurusan
     */
    public function setJenisUsaha(string $jenisUsaha): self
    {
        $this->jenisUsaha = $jenisUsaha;

        return $this;
    }

    /**
     * Get program keahlian/studi.
     *
     * @return JurusanInstitusi
     */
    public function getJurusan(): ?JurusanInstitusi
    {
        return $this->jurusan;
    }

    /**
     * Set program keahlian/studi.
     *
     * @param JurusanInstitusi $jurusan
     *
     * @return MitraDudiJurusan
     */
    public function setJurusan(JurusanInstitusi $jurusan): self
    {
        $this->jurusan = $jurusan;

        return $this;
    }

    /**
     * Get nama pimpinan/direktur mitra usaha.
     *
     * @return string|null
     */
    public function getNamaPimpinan(): ?string
    {
        return $this->namaPimpinan;
    }

    /**
     * Set nama pimpinan/direktur mitra usaha.
     *
     * @param string $namaPimpinan
     *
     * @return MitraDudiJurusan
     */
    public function setNamaPimpinan(string $namaPimpinan): self
    {
        $this->namaPimpinan = $namaPimpinan;

        return $this;
    }

    /**
     * Get nama usaha/industri.
     *
     * @return string|null
     */
    public function getNamaUsaha(): ?string
    {
        return $this->namaUsaha;
    }

    /**
     * Set nama usaha/industri.
     *
     * @param string $namaUsaha
     *
     * @return MitraDudiJurusan
     */
    public function setNamaUsaha(string $namaUsaha): self
    {
        $this->namaUsaha = $namaUsaha;

        return $this;
    }

}
