<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Beneficiary;


use App\Entity\Kegiatan\IndikatorKpi;
use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\Pendanaan\ApprovedByTrait;
use App\Entity\Pendanaan\VerifiedByTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity penerima manfaat program Tech-Based Education.
 *
 * @ORM\Table(name="beneficiary_tbe", indexes={
 *     @ORM\Index(name="beneficiary_tbe_x2", columns={"nip"}),
 *     @ORM\Index(name="beneficiary_tbe_x3", columns={"posted_by"}),
 *     @ORM\Index(name="beneficiary_tbe_x4", columns={"updated_by"}),
 *     @ORM\Index(name="tbe_aprv_idx1", columns={"verified_by"}),
 *     @ORM\Index(name="tbe_aprv_idx2", columns={"approved_by"})
 * }, uniqueConstraints={@ORM\UniqueConstraint(name="beneficiary_tbe_x1", columns={"individu_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\BeneficiaryTbeRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryTbe")
 *
 * @package App\Entity\Beneficiary
 * @author  Ahmad Fajar
 * @since   29/11/2018, modified: 15/07/2020 01:06
 */
class BeneficiaryTbe
{

    use PostedByFkTrait, LastUpdateByFkTrait, PostedDateTrait, LastUpdateTrait, VerifiedByTrait, ApprovedByTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="tbe_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tbeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="jenis_institusi", type="smallint", options={"comment": "Valid values:
    1 = SD, 2 = SMP, 3 = SMA", "default": 1})
     * @Assert\NotBlank(message="JenisInstitusi.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenisInstitusi = 1;

    /**
     * @ORM\Column(name="nama_institusi", type="string", length=250)
     * @Assert\NotBlank(message="NamaInstitusi.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaInstitusi;

    /**
     * @ORM\Column(name="alamat_institusi", type="text", nullable=true)
     * @Assert\NotBlank(message="AlamatInstitusi.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $alamatInstitusi;

    /**
     * @ORM\Column(name="nip", type="string", length=50, nullable=true)
     * @Assert\NotBlank(message="NIP.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $nip;

    /**
     * @ORM\Column(name="lama_mengajar", type="string", length=150, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $lamaMengajar;

    /**
     * @ORM\Column(name="sertifikasi", type="boolean", options={"default": false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $sertifikasi;

    /**
     * @ORM\Column(name="mapel", type="json", nullable=true, options={
     *     "jsonb": true, "comment": "Mata pelajaran yang diajarkan"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $mapel = [];

    /**
     * @ORM\Column(name="prestasi", type="json", nullable=true, options={
     *     "jsonb": true, "comment": "Sertifikat kompetensi/prestasi"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $prestasi = [];

    /**
     * @ORM\Column(name="pelatihan", type="json", nullable=true, options={
     *     "jsonb": true, "comment": "Pelatihan terkait pembelajaran teknologi yang pernah diikuti"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $pelatihan = [];

    /**
     * @var BeneficiaryIndividu
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\BeneficiaryIndividu")
     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\BeneficiaryIndividu", inversedBy="tbe", cascade={"persist"})
     * @ORM\JoinColumn(name="individu_id", referencedColumnName="individu_id", nullable=false, onDelete="CASCADE")
     * @Groups({"without_rel", "with_rel"})
     */
    private $biodata;

//    /**
//     * @var LogAprvBeneficiary
//     *
//     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Beneficiary\LogAprvBeneficiary")
//     * @ORM\OneToOne(targetEntity="App\Entity\Beneficiary\LogAprvBeneficiary", inversedBy="tbe", cascade={"persist"})
//     * @ORM\JoinColumn(name="tbe_id", referencedColumnName="tbe_id", nullable=true, onDelete="CASCADE")
//     * @Groups({"without_rel", "with_rel"})
//     */
//    private $approval;

    /**
     * @var IndikatorKpi[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Kegiatan\IndikatorKpi", mappedBy="tbe",
     *     cascade={"persist", "remove"})
     * @ORM\OrderBy({"postedDate" = "ASC"})
     * @Groups({"without_rel", "with_rel"})
     *
     */
    private $kpi;

    /**
     * @ORM\Column(name="status", type="string", length=20, options={
     *     "default": "NEW",
     *     "comment": "Status valid values: NEW, UPDATED, VERIFIED, APPROVED, REJECTED"
     * })
     * @Groups({"without_rel", "with_rel"})
     */

    private $status = WorkflowTags::NEW;

    /**
     * BeneficiaryTbe constructor.
     */
    public function __construct()
    {
        $this->kpi = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return BeneficiaryTbe
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get alamat lengkap institusi tempat peserta TBE mengajar.
     *
     * @return string|null
     */
    public function getAlamatInstitusi(): ?string
    {
        return $this->alamatInstitusi;
    }

    /**
     * Set alamat lengkap institusi tempat peserta TBE mengajar.
     *
     * @param string|null $alamat Alamat lengkap
     *
     * @return BeneficiaryTbe
     */
    public function setAlamatInstitusi(?string $alamat): self
    {
        $this->alamatInstitusi = $alamat;

        return $this;
    }

    /**
     * Get biodata penerima manfaat program TBE.
     *
     * @return BeneficiaryIndividu
     */
    public function getBiodata(): ?BeneficiaryIndividu
    {
        return $this->biodata;
    }

    /**
     * Set biodata penerima manfaat program TBE.
     *
     * @param BeneficiaryIndividu $biodata
     *
     * @return BeneficiaryTbe
     */
    public function setBiodata(BeneficiaryIndividu $biodata): self
    {
        $this->biodata = $biodata;

        return $this;
    }

    /**
     * Get jenis institusi tempat peserta TBE mengajar.
     *
     * @return int Salah satu dari nilai berikut:
     * 1 = SD, 2 = SMP, 3 = SMA
     */
    public function getJenisInstitusi(): ?int
    {
        return $this->jenisInstitusi;
    }

    /**
     * Set jenis institusi tempat peserta TBE mengajar.
     *
     * @param int $jenis Salah satu dari nilai berikut: 1 = SD, 2 = SMP, 3 = SMA
     *
     * @return BeneficiaryTbe
     */
    public function setJenisInstitusi(int $jenis): self
    {
        $this->jenisInstitusi = $jenis;

        return $this;
    }

    /**
     * Get lama mengajar.
     *
     * @return string
     */
    public function getLamaMengajar(): ?string
    {
        return $this->lamaMengajar;
    }

    /**
     * Set lama mengajar.
     *
     * @param string $lamaMengajar
     *
     * @return BeneficiaryTbe
     */
    public function setLamaMengajar(?string $lamaMengajar): self
    {
        $this->lamaMengajar = $lamaMengajar;

        return $this;
    }

    /**
     * Get mata pelajaran yang diajarkan pada institusi tempatnya mengajar.
     *
     * @return array|null In key-value format
     */
    public function getMapel(): ?array
    {
        return $this->mapel;
    }

    /**
     * Set mata pelajaran yang diajarkan pada institusi tempatnya mengajar.
     *
     * @param array|null $mapel Array in key-value format
     *
     * @return BeneficiaryTbe
     */
    public function setMapel(?array $mapel): self
    {
        $this->mapel = $mapel;

        return $this;
    }

    /**
     * Get nama institusi tempat peserta TBE mengajar.
     *
     * @return string|null
     */
    public function getNamaInstitusi(): ?string
    {
        return $this->namaInstitusi;
    }

    /**
     * Set nama Institusi tempat peserta TBE mengajar.
     *
     * @param string $nama Nama institusi
     *
     * @return BeneficiaryTbe
     */
    public function setNamaInstitusi(string $nama): self
    {
        $this->namaInstitusi = $nama;

        return $this;
    }

    /**
     * Get nomor induk pegawai peserta TBE.
     *
     * @return string|null
     */
    public function getNip(): ?string
    {
        return $this->nip;
    }

    /**
     * Set nomor induk pegawai peserta TBE.
     *
     * @param string|null $nip
     *
     * @return BeneficiaryTbe
     */
    public function setNip(?string $nip): self
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get pelatihan/training yang pernah diikuti terkait pembelajaran teknologi.
     *
     * @return array|null In key-value format
     */
    public function getPelatihan(): ?array
    {
        return $this->pelatihan;
    }

    /**
     * Set pelatihan/training yang pernah diikuti terkait pembelajaran teknologi.
     *
     * @param array|null $pelatihan Array in key-value format
     *
     * @return BeneficiaryTbe
     */
    public function setPelatihan(?array $pelatihan): self
    {
        $this->pelatihan = $pelatihan;

        return $this;
    }

    /**
     * Get sertifikat kompetensi ataupun prestasi yang pernah diperoleh.
     *
     * @return array|null In key-value format
     */
    public function getPrestasi(): ?array
    {
        return $this->prestasi;
    }

    /**
     * Set sertifikat kompetensi ataupun prestasi yang pernah diperoleh.
     *
     * @param array|null $prestasi Array in key-value format
     *
     * @return BeneficiaryTbe
     */
    public function setPrestasi(?array $prestasi): self
    {
        $this->prestasi = $prestasi;

        return $this;
    }

    /**
     * Check apakah guru memiliki sertifikat kompetensi atau tidak.
     *
     * @return bool|null TRUE jika memiliki sertifikat kompetensi atau sebaliknya.
     */
    public function getSertifikasi(): ?bool
    {
        return $this->sertifikasi;
    }

    /**
     * Apakah guru memiliki sertifikat kompetensi atau tidak?
     *
     * @param bool $hasSertifikasi IF TRUE berarti memiliki sertifikat kompetensi, otherwise FALSE
     *
     * @return BeneficiaryTbe
     */
    public function setSertifikasi(bool $hasSertifikasi): self
    {
        $this->sertifikasi = $hasSertifikasi;

        return $this;
    }

    /**
     * Get the entity ID.
     *
     * @return int|null
     */
    public function getTbeId(): ?int
    {
        return $this->tbeId;
    }

    /**
     * @return IndikatorKpi[]|Collection
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * @param array|null $kpi
     *
     * @return BeneficiaryTbe
     */
    public function setKpi(?array $kpi): self
    {
        $this->kpi = $kpi;

        if (!empty($kpi) && is_array($kpi)) {
            $this->kpi = new ArrayCollection($kpi);
        } elseif ($kpi instanceof Collection) {
            $this->kpi = $kpi;
        } else {
            $this->kpi->clear();
        }

        return $this;

    }

    /**
     * Memeriksa apakah entri sudah memiliki penilaian kpi atau belum.
     */
    public function hasKpi(): bool
    {
        return $this->getKpi()->count() > 0;
    }
}
