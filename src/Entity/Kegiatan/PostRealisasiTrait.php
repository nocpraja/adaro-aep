<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;


use App\Entity\Security\UserAccount;
use App\Service\Security\AppUserInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait PostRealisasiTrait
 *
 * @package App\Entity\Kegiatan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 3:05
 */
trait PostRealisasiTrait
{

    /**
     * @var UserAccount
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="posted_realisasi_by", referencedColumnName="uid", nullable=true)
     * @Groups({"with_approval"})
     */
    private $postedRealisasiBy;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="tgl_posted_realisasi", type="datetime", nullable=true)
     * @Groups({"with_approval"})
     */
    private $tanggalPostedRealisasi;


    /**
     * Get a User Account that updates realisasi anggaran.
     *
     * @return UserAccount
     */
    public function getPostedRealisasiBy(): ?UserAccount
    {
        return $this->postedRealisasiBy;
    }

    /**
     * Set a User Account that updates realisasi anggaran.
     *
     * @param UserAccount|AppUserInterface $userAccount
     *
     * @return self
     */
    public function setPostedRealisasiBy(?UserAccount $userAccount): self
    {
        $this->postedRealisasiBy = $userAccount;

        return $this;
    }

    /**
     * Get a datetime when realisasi anggaran is updated.
     *
     * @return DateTimeInterface
     */
    public function getTanggalPostedRealisasi(): ?DateTimeInterface
    {
        return $this->tanggalPostedRealisasi;
    }

    /**
     * Set a datetime when realisasi anggaran is updated.
     *
     * @param DateTimeInterface $dateTime A datetime when realisasi anggaran is updated
     *
     * @return self
     */
    public function setTanggalPostedRealisasi(?DateTimeInterface $dateTime): self
    {
        $this->tanggalPostedRealisasi = $dateTime;

        return $this;
    }

}