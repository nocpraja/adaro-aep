<?php
/**
 * Description: ViewIndikator.php generated by PhpStorm.
 * @project     adaro-aep
 * @package     App\Entity\Kegiatan
 * @author      alex
 * @created     2020-08-25, modified: 2020-08-25 13:21
 * @copyright   Copyright (c) 2020
 */

namespace App\Entity\Kegiatan;


use App\Entity\MasterData\BatchCsr;
use App\Entity\MasterData\DataReference;
use App\Entity\MasterData\ProgramCSR;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="vw_indikator", options={"comment": "Table view untuk data Indikator Penilaian KPI"})
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\ViewIndikatorKpiRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\ViewIndikator")
 *
 * @package App\Entity\Kegiatan
 * @author  Alex Gaspersz
 * @since   25/08/2020 13:22
 */
class ViewIndikator
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="idk", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $idk;

    /**
     * @var ProgramCSR
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\ProgramCSR")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\ProgramCSR")
     * @ORM\JoinColumn(name="msid", referencedColumnName="msid", nullable=true)
     * @Groups({"without_rel"})
     */
    private $programCsr;

    /**
     * @var BatchCsr
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\BatchCsr")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\BatchCsr")
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="batch_id", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $periodeBatch;

    /**
     * @var DataReference
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\DataReference")
     * @ORM\JoinColumn(name="data_ref", referencedColumnName="id", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $dataRef;

    /**
     * @ORM\Column(name="tahun", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahun;

    /**
     * @ORM\Column(name="period", type="string")
     * @Groups({"without_rel", "with_rel"})
     */
    private $period;

    /**
     * @ORM\Column(name="dashboard", type="boolean",
     *     options={"default": false, "comment": "Tampil pada Dashboard atau tidak"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $showInDashboard;

    /**
     * @ORM\Column(name="jenis_kelamin", type="string")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenisKelamin;

    /**
     * @var array
     *
     * @ORM\Column(name="vokasi_params", type="json", nullable=true, options={"jsonb": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $vokasiParams = null;

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getIdk(): ?int
    {
        return $this->idk;
    }

    /**
     * @return ProgramCSR|null
     */
    public function getProgramCsr(): ?ProgramCSR
    {
        return $this->programCsr;
    }

//    /**
//     * @param ProgramCSR|null $programCsr
//     *
//     * @return ViewIndikator
//     */
//    public function setProgramCsr(?ProgramCSR $programCsr): self
//    {
//        $this->programCsr = $programCsr;
//
//        return $this;
//    }

    /**
     * @return BatchCsr|null
     */
    public function getPeriodeBatch(): ?BatchCsr
    {
        return $this->periodeBatch;
    }

//    /**
//     * @param BatchCsr|null $periodeBatch
//     *
//     * @return ViewIndikator
//     */
//    public function setPeriodeBatch(?BatchCsr $periodeBatch): self
//    {
//        $this->periodeBatch = $periodeBatch;
//
//        return $this;
//    }

    /**
     * Nilai capaian KPI tampil di Dashboard atau tidak?
     *
     * @return bool|null
     */
    public function getShowInDashboard(): ?bool
    {
        return $this->showInDashboard;
    }

    /**
     * @return DataReference|null
     */
    public function getDataRef(): ?DataReference
    {
        return $this->dataRef;
    }

//    /**
//     * @param DataReference|null $dataRef
//     *
//     * @return ViewIndikator
//     */
//    public function setDataRef(?DataReference $dataRef): self
//    {
//        $this->dataRef = $dataRef;
//
//        return $this;
//    }

    /**
     * @return int|null
     */
    public function getTahun(): ?int
    {
        return $this->tahun;
    }

//    /**
//     * @param int|null $tahun
//     *
//     * @return ViewIndikator
//     */
//    public function setTahun(?int $tahun): self
//    {
//        $this->tahun = $tahun;
//
//        return $this;
//    }

    /**
     * @return string|null
     */
    public function getPeriod(): ?string
    {
        return $this->period;
    }

//    /**
//     * @param string|null $period
//     *
//     * @return ViewIndikator
//     */
//    public function setPeriod(?string $period): self
//    {
//        $this->period = $period;
//
//        return $this;
//    }

    /**
     * @return string|null
     */
    public function getJenisKelamin(): ?string
    {
        return $this->jenisKelamin;
    }

//    /**
//     * @param string|null $jenisKelamin
//     *
//     * @return ViewIndikator
//     */
//    public function setJenisKelamin(?string $jenisKelamin): self
//    {
//        $this->jenisKelamin = $jenisKelamin;
//
//        return $this;
//    }

    /**
     * @return array|null
     */
    public function getVokasiParams(): ?array
    {
        return $this->vokasiParams;
    }

}
