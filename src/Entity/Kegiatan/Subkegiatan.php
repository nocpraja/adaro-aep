<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;


use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\MasterData\Kabupaten;
use App\Entity\MasterData\Provinsi;
use App\Entity\Pendanaan\ClosedByTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="subkegiatan", indexes={
 *     @ORM\Index(name="subkegiatan_x1", columns={"kegiatan_id"}),
 *     @ORM\Index(name="subkegiatan_x2", columns={"closed_by"}),
 *     @ORM\Index(name="subkegiatan_x3", columns={"posted_realisasi_by"}),
 *     @ORM\Index(name="subkegiatan_x4", columns={"posted_by"}),
 *     @ORM\Index(name="subkegiatan_x5", columns={"updated_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\SubkegiatanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\Subkegiatan")
 * @package App\Entity\Kegiatan
 * @author  Ahmad Fajar
 * @since   13/11/2018, modified: 25/05/2019 3:08
 */
class Subkegiatan
{

    use PostRealisasiTrait, ClosedByTrait;
    use PostedByFkTrait, PostedDateTrait, LastUpdateByFkTrait, LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="sub_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $sid;

    /**
     * @ORM\Column(name="title", type="text")
     * @Assert\NotBlank(message="Subkegiatan.Title.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $title;

    /**
     * @ORM\Column(name="tgl_mulai", type="date")
     * @Assert\NotBlank(message="Subkegiatan.TanggalMulai.NotNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tanggalMulai;

    /**
     * @ORM\Column(name="tgl_akhir", type="date", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tanggalAkhir;

    /**
     * @ORM\Column(name="anggaran", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $anggaran = 0.0;

    /**
     * @ORM\Column(name="realisasi", type="float", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi;

    /**
     * @ORM\Column(name="closed", type="boolean", options={"default": false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $closed = false;

    /**
     * @ORM\Column(name="is_realisasi", type="boolean", options={"default": false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $isRealisasi = false;

    /**
     * @var float
     * @Groups({"without_rel"})
     */
    private $totalSubRealisasi = 0.0;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $totalDokumentasi = 0;

    /**
     * @var string
     * @Groups({"without_rel"})
     */
    private $latestDocument = null;

    /**
     * @var string
     * @Groups({"without_rel"})
     */
    private $latestGallery = null;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $totalFoto = 0;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $totalDokumen = 0;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $verifiedDokumen = 0;

    /**
     * @var float
     * @Groups({"without_rel"})
     */
    private $verifiedSubRealisasi = 0.0;

    /**
     * @var Kegiatan
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\Kegiatan")
     * @ORM\ManyToOne(targetEntity="App\Entity\Kegiatan\Kegiatan", inversedBy="children")
     * @ORM\JoinColumn(name="kegiatan_id", referencedColumnName="kegiatan_id", nullable=false, onDelete="CASCADE")
     * @Groups({"with_parent"})
     */
    private $kegiatan;

    /**
     * @var Provinsi[]|Collection
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Provinsi")
     * @ORM\ManyToMany(targetEntity="App\Entity\MasterData\Provinsi")
     * @ORM\JoinTable(
     *     name="subkegiatan_provinsi",
     *     joinColumns={@ORM\JoinColumn(name="sub_id", referencedColumnName="sub_id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="prov_id", referencedColumnName="prov_id")}
     * )
     * @ORM\OrderBy({"namaProvinsi" = "ASC"})
     * @Groups({"with_child"})
     */
    private $provincies;

    /**
     * @var Kabupaten[]|Collection
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kabupaten")
     * @ORM\ManyToMany(targetEntity="App\Entity\MasterData\Kabupaten")
     * @ORM\JoinTable(
     *     name="subkegiatan_kabupaten",
     *     joinColumns={@ORM\JoinColumn(name="sub_id", referencedColumnName="sub_id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="kab_id", referencedColumnName="kab_id")}
     * )
     * @ORM\OrderBy({"namaKabupaten" = "ASC"})
     * @Groups({"with_child"})
     */
    private $kabupatens;

    /**
     * @var ItemAnggaran[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Kegiatan\ItemAnggaran", mappedBy="parent")
     * @ORM\OrderBy({"postedDate" = "ASC"})
     * @Groups({"with_child"})
     */
    private $children;

    /**
     * @var DokumentasiEntity[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Kegiatan\DokumentasiEntity", mappedBy="parentDoc", cascade={"persist", "remove"})
     * @ORM\OrderBy({"postedDate" = "ASC"})
     * @Groups({"with_child"})
     */
    private $dokumentasi;

    /**
     * SubKegiatan constructor.
     */
    public function __construct()
    {
        $this->provincies = new ArrayCollection();
        $this->kabupatens = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->dokumentasi = new ArrayCollection();
    }

    public function getAnggaran(): ?float
    {
        return $this->anggaran;
    }

    public function setAnggaran(?float $anggaran): self
    {
        $this->anggaran = $anggaran;

        return $this;
    }

    /**
     * @return ItemAnggaran[]|Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ItemAnggaran[]|Collection $elements
     *
     * @return Subkegiatan
     */
    public function setChildren(?array $elements): self
    {
        if (!empty($elements) && is_array($elements)) {
            $this->children = new ArrayCollection($elements);
        } elseif ($elements instanceof Collection) {
            $this->children = $elements;
        } else {
            $this->children->clear();
        }

        foreach ($this->children as $child) {
            $child->setParent($this);
        }

        return $this;
    }

    /**
     * @param DokumentasiEntity[]|Collection $elements
     *
     * @return Subkegiatan
     */
    public function setDokumentasi(?array $elements): self
    {
        if (!empty($elements) && is_array($elements)) {
            $this->dokumentasi = new ArrayCollection($elements);
        } elseif ($elements instanceof Collection) {
            $this->dokumentasi = $elements;
        } else {
            $this->dokumentasi->clear();
        }

        foreach ($this->dokumentasi as $child) {
            $child->setParentDoc($this);
        }

        return $this;
    }

    /**
     * Get lokasi kegiatan.
     * @return Kabupaten[]|Collection
     */
    public function getKabupatens()
    {
        return $this->kabupatens;
    }

    /**
     * Set lokasi kegiatan.
     *
     * @param Kabupaten[]|Collection $elements
     *
     * @return Subkegiatan
     */
    public function setKabupatens(array $elements): self
    {
        if ($elements instanceof Collection) {
            $this->kabupatens = $elements;
        } else {
            $this->kabupatens = new ArrayCollection($elements);
        }

        return $this;
    }

    /**
     * @return Kegiatan
     */
    public function getKegiatan(): ?Kegiatan
    {
        return $this->kegiatan;
    }

    /**
     * @param Kegiatan $kegiatan
     *
     * @return Subkegiatan
     */
    public function setKegiatan(Kegiatan $kegiatan): self
    {
        $this->kegiatan = $kegiatan;

        return $this;
    }

    /**
     * Get lokasi kegiatan.
     * @return Provinsi[]|Collection
     */
    public function getProvincies()
    {
        return $this->provincies;
    }

    /**
     * Set lokasi kegiatan.
     *
     * @param Provinsi[]|Collection $elements
     *
     * @return Subkegiatan
     */
    public function setProvincies(array $elements): self
    {
        if ($elements instanceof Collection) {
            $this->provincies = $elements;
        } else {
            $this->provincies = new ArrayCollection($elements);
        }

        return $this;
    }

    public function getRealisasi(): ?float
    {
        return $this->realisasi;
    }

    public function setRealisasi(?float $realisasi): self
    {
        $this->realisasi = $realisasi;

        return $this;
    }

    public function getTotalSubRealisasi()
    {
        $total = 0;
        foreach ($this->children as $row) {
            $total += $row->getTotalRealisasi();
        }
        return $total;
    }

    public function getTotalDokumentasi()
    {
        return $this->dokumentasi ? count($this->dokumentasi) : 0;
    }

    /**
     * @return float
     */
    public function getVerifiedSubRealisasi()
    {
        $total = 0;
        foreach ($this->children as $row) {
            $total += $row->getVerifiedRealisasi();
        }
        return $total;
    }

    /**
     * Get entity ID.
     * @return int|null
     */
    public function getSid(): ?int
    {
        return $this->sid;
    }

    /**
     * Get tanggal berakhirnya jadwal pelaksanaan.
     * @return DateTimeInterface|null
     */
    public function getTanggalAkhir(): ?DateTimeInterface
    {
        return $this->tanggalAkhir;
    }

    /**
     * Set tanggal berakhirnya jadwal pelaksanaan.
     *
     * @param DateTimeInterface|null $tanggalAkhir
     *
     * @return Subkegiatan
     */
    public function setTanggalAkhir(?DateTimeInterface $tanggalAkhir): self
    {
        $this->tanggalAkhir = $tanggalAkhir;

        return $this;
    }

    /**
     * Get tanggal awal dari jadwal pelaksanaan.
     * @return DateTimeInterface|null
     */
    public function getTanggalMulai(): ?DateTimeInterface
    {
        return $this->tanggalMulai;
    }

    /**
     * Set tanggal awal dari jadwal pelaksanaan.
     *
     * @param DateTimeInterface $tanggalMulai
     *
     * @return Subkegiatan
     */
    public function setTanggalMulai(DateTimeInterface $tanggalMulai): self
    {
        $this->tanggalMulai = $tanggalMulai;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Memeriksa apakah Kegiatan sudah memiliki item anggaran atau belum.
     */
    public function hasChildren(): bool
    {
        return $this->getChildren()->count() > 0;
    }

    public function isClosed(): ?bool
    {
        return $this->closed;
    }

    public function setClosed(bool $closed): self
    {
        $this->closed = $closed;

        return $this;
    }

    public function isRealisasi(): ?bool
    {
        return $this->isRealisasi;
    }

    public function setIsRealisasi(bool $isRealisasi): self
    {
        $this->isRealisasi = $isRealisasi;

        return $this;
    }

    /**
     * @return DokumentasiEntity[]|Collection
     */
    public function getDokumentasi()
    {
        return $this->dokumentasi;
    }

    /**
     * @return string|null
     */
    public function getLatestDocument(): ?string
    {
        $doc = null;
        foreach ($this->dokumentasi as $child) {
            if ($child->getDocCategory() == 'DOSIR')
                $doc = $child->getExtension();
        }
        return $doc;
    }

    /**
     * @return string|null
     */
    public function getLatestGallery(): ?string
    {
        $doc = null;
        foreach ($this->dokumentasi as $child) {
            if ($child->getDocCategory() == 'GALLERY' && $child->getExtension() != 'mp4')
                $doc = $child->getThumbnail();
        }
        return $doc;
    }

    /**
     * @return int
     */
    public function getTotalFoto(): int
    {
        $total = 0;
        foreach ($this->dokumentasi as $row) {
            if ($row->getDocCategory() == 'GALLERY')
                $total += $row->getDocId();
        }
        return $total;
    }

    /**
     * @return int
     */
    public function getTotalDokumen(): int
    {
        $total = 0;
        foreach ($this->dokumentasi as $row) {
            if ($row->getDocCategory() == 'DOKUMEN')
                $total += $row->getDocId();
        }
        return $total;
    }

    /**
     * @return int
     */
    public function getVerifiedDokumen(): int
    {
        $total = 0;
        foreach ($this->dokumentasi as $row) {
            $total += $row->isIsverified();
        }
        return $total;
    }

}
