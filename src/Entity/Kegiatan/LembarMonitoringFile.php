<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @package App\Entity\Kegiatan
 * @author  Mark Melvin
 * @since   16/11/2018
 */
class LembarMonitoringFile
{
    /**
     * @Assert\NotBlank(message="File.NotBlank")
     * @Assert\File(mimeTypes={ "application/pdf", "image/jpg", "image/jpeg" }, mimeTypesMessage="Accepts only PDF or jpg")
     */
    private $file;

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     * @return LembarMonitoringFile
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }


}
