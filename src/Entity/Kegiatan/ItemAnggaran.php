<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;


use App\Entity\LastUpdateTrait;
use App\Entity\MasterData\DataReference;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="item_anggaran", indexes={
 *     @ORM\Index(name="item_anggaran_x1", columns={"parent_id"}),
 *     @ORM\Index(name="item_anggaran_x2", columns={"account_id"}),
 *     @ORM\Index(name="item_anggaran_x3", columns={"posted_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\ItemAnggaranRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\ItemAnggaran")
 *
 * @package App\Entity\Kegiatan
 * @author  Ahmad Fajar
 * @since   13/11/2018, modified: 24/10/2019 00:17
 */
class ItemAnggaran
{

    use PostedByFkTrait, PostedDateTrait, LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="ids", type="bigint")
     * @Groups({"with_rel", "without_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="deskripsi", type="text")
     * @Assert\NotBlank(message="EntriRAB.Title.NotBlank")
     * @Groups({"with_rel", "without_rel"})
     */
    private $title;

    /**
     * @ORM\Column(name="fixed_cost", type="boolean", options={"default": true})
     * @Groups({"without_rel"})
     */
    private $fixedCost = true;

    /**
     * @ORM\Column(name="mgt_fee_const", type="float", options={
     *     "default": 5, "comment": "Konstanta percentase management fee"
     * })
     * @Groups({"without_rel"})
     */
    private $mgtFeeConst = 5.0;

    /**
     * @ORM\Column(name="jml_qty1", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel"})
     */
    private $jumlahQty1 = 0.0;

    /**
     * @ORM\Column(name="jml_qty2", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel"})
     */
    private $jumlahQty2 = 0.0;

    /**
     * @ORM\Column(name="frequency", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel"})
     */
    private $frequency = 0.0;

    /**
     * @ORM\Column(name="unit_price", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel"})
     */
    private $unitPrice = 0.0;

    /**
     * @ORM\Column(name="anggaran", type="float", nullable=true, options={"default": 0})
     * @Groups({"with_rel", "without_rel"})
     */
    private $anggaran = 0.0;

    /**
     * @ORM\Column(name="realisasi", type="float", nullable=true)
     * @Groups({"with_rel", "without_rel"})
     */
    private $realisasi;

    /**
     * @var float
     * @Groups({"without_rel"})
     */
    private $totalRealisasi = 0.0;

    /**
     * @var float
     * @Groups({"without_rel"})
     */
    private $verifiedRealisasi = 0.0;

    /**
     * @ORM\Column(name="unit_qty1", type="string", length=100, nullable=true)
     * @Groups({"without_rel"})
     */
    private $unitQty1;

    /**
     * @ORM\Column(name="unit_qty2", type="string", length=100, nullable=true)
     * @Groups({"without_rel"})
     */
    private $unitQty2;

    /**
     * @ORM\Column(name="unit_freq", type="string", length=100, nullable=true)
     * @Groups({"without_rel"})
     */
    private $unitFrequency;

    /**
     * @ORM\Column(name="remark", type="text", nullable=true)
     * @Groups({"with_rel", "without_rel"})
     */
    private $remark;

    /**
     * @var Subkegiatan
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Kegiatan\Subkegiatan", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="sub_id", nullable=false, onDelete="CASCADE")
     * @Groups({"with_parent"})
     */
    private $parent;

    /**
     * @var DataReference
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\DataReference")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     * @Groups({"without_rel"})
     */
    private $account;

    /**
     * @var ItemRealisasiRab[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Kegiatan\ItemRealisasiRab", mappedBy="parentRab", cascade={"persist"})
     * @ORM\OrderBy({"postedDate" = "ASC"})
     * @Groups({"with_child"})
     */
    private $realisasiRabs;

    /**
     * @Groups({"without_rel"})
     */
    private $lastRealisasi;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $idSub;

    /**
     * @var string
     * @Groups({"without_rel"})
     */
    private $namaSub;

    /**
     * ItemAnggaran constructor.
     */
    public function __construct()
    {
        $this->realisasiRabs = new ArrayCollection();
    }

    /**
     * Get master account code.
     *
     * @return DataReference
     */
    public function getAccount(): ?DataReference
    {
        return $this->account;
    }

    /**
     * Set master account code.
     *
     * @param DataReference $account RAB account
     *
     * @return ItemAnggaran
     */
    public function setAccount(DataReference $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getAnggaran(): ?float
    {
        return $this->anggaran;
    }

    public function setAnggaran(?float $anggaran): self
    {
        $this->anggaran = $anggaran;

        return $this;
    }

    public function getFrequency(): ?float
    {
        return $this->frequency;
    }

    public function setFrequency(?float $frequency): self
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJumlahQty1(): ?float
    {
        return $this->jumlahQty1;
    }

    public function setJumlahQty1(?float $jumlahQty1): self
    {
        $this->jumlahQty1 = $jumlahQty1;

        return $this;
    }

    public function getJumlahQty2(): ?float
    {
        return $this->jumlahQty2;
    }

    public function setJumlahQty2(?float $jumlahQty2): self
    {
        $this->jumlahQty2 = $jumlahQty2;

        return $this;
    }

    /**
     * Konstanta nilai persentase management fee. Nilai ini dipergunakan untuk menghitung
     * biaya management fee dan total anggaran jika fixedCost = false.
     *
     * @return float
     */
    public function getMgtFeeConst(): float
    {
        return $this->mgtFeeConst;
    }

    /**
     * Konstanta nilai persentase management fee. Nilai ini dipergunakan untuk menghitung
     * biaya management fee dan total anggaran jika fixedCost = false.
     *
     * @param float $value Nilai konstanta
     *
     * @return ItemAnggaran
     */
    public function setMgtFeeConst(float $value): self
    {
        $this->mgtFeeConst = $value;

        return $this;
    }

    /**
     * Get parent relation.
     *
     * @return Subkegiatan
     */
    public function getParent(): ?Subkegiatan
    {
        return $this->parent;
    }

    /**
     * Set parent relation.
     *
     * @param Subkegiatan $parent
     *
     * @return ItemAnggaran
     */
    public function setParent(Subkegiatan $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdSub(): int
    {
        return $this->parent->getSid();
    }

    /**
     * @return string
     */
    public function getNamaSub(): string
    {
        return $this->parent->getTitle();
    }

    public function getRealisasi(): ?float
    {
        return $this->realisasi;
    }

    public function setRealisasi(?float $realisasi): self
    {
        $this->realisasi = $realisasi;

        return $this;
    }

    public function getRemark(): ?string
    {
        return $this->remark;
    }

    public function setRemark(?string $remark): self
    {
        $this->remark = $remark;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUnitFrequency(): ?string
    {
        return $this->unitFrequency;
    }

    public function setUnitFrequency(?string $unitFrequency): self
    {
        $this->unitFrequency = $unitFrequency;

        return $this;
    }

    public function getUnitPrice(): ?float
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?float $unitPrice): self
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    public function getUnitQty1(): ?string
    {
        return $this->unitQty1;
    }

    public function setUnitQty1(?string $unitQty1): self
    {
        $this->unitQty1 = $unitQty1;

        return $this;
    }

    public function getUnitQty2(): ?string
    {
        return $this->unitQty2;
    }

    public function setUnitQty2(?string $unitQty2): self
    {
        $this->unitQty2 = $unitQty2;

        return $this;
    }

    public function getTotalRealisasi()
    {
        $total = 0;
        foreach ($this->realisasiRabs as $row) {
            $total += $row->getRealisasi();
        }
        return $total;
    }

    /**
     * @return float
     */
    public function getVerifiedRealisasi()
    {
        $total = 0;
        foreach ($this->realisasiRabs as $row) {
            if($row->getStatus() == WorkflowTags::REALISASI_VERIFIED) {
                $total += $row->getRealisasi();
            }
        }
        return $total;
    }

    /**
     * @return mixed|null
     */
    public function getLastRealisasi()
    {
        $property = null;
        if (!$this->realisasiRabs->isEmpty()) {
            $property =  $this->realisasiRabs->last();
        }

        return $property;
    }


    /**
     * Apakah perhitungan jumlah anggaran menggunakan metode fixed-cost atau reimburse?
     *
     * @return bool|null
     */
    public function isFixedCost(): ?bool
    {
        return $this->fixedCost;
    }

    /**
     * Set metode perhitungan jumlah anggaran.
     *
     * @param bool $fixedCost If TRUE menggunakan fixed-cost, and if FALSE menggunakan metode reimburse
     *
     * @return ItemAnggaran
     */
    public function setFixedCost(bool $fixedCost): self
    {
        $this->fixedCost = $fixedCost;

        return $this;
    }

    /**
     * @return ItemRealisasiRab[]|Collection
     */
    public function getRealisasiRabs()
    {
        return $this->realisasiRabs;
    }

    /**
     * @param ItemRealisasiRab[]|Collection $elements
     *
     * @return ItemAnggaran
     */
    public function setRealisasiRabs(?array $elements): self
    {
        if (!empty($elements) && is_array($elements)) {
            $this->realisasiRabs = new ArrayCollection($elements);
        } elseif ($elements instanceof Collection) {
            $this->realisasiRabs = $elements;
        } else {
            $this->realisasiRabs->clear();
        }

        foreach ($this->realisasiRabs as $child) {
            $child->setParentRab($this);
        }

        return $this;
    }

    /**
     * Memeriksa apakah anggaran sudah memiliki item realisasi atau belum.
     */
    public function hasRealisasiRabs(): bool
    {
        return $this->getRealisasiRabs()->count() > 0;
    }

    /**
     * Add realisasi anggaran.
     *
     * @param ItemRealisasiRab|null $itemRealisasiRab
     *
     * @return $this
     */
    public function addItemRealisasiRab(?ItemRealisasiRab $itemRealisasiRab)
    {
        $itemRealisasiRab->setParentRab($this);
        $realisasiAnggaran = $this->getRealisasiRabs();

        if ($realisasiAnggaran instanceof Collection) {
            $realisasiAnggaran->add($itemRealisasiRab);
        } elseif (empty($realisasiAnggaran)) {
            $this->realisasiRabs = new ArrayCollection();
            $this->realisasiRabs->add($itemRealisasiRab);
        } else {
            $this->realisasiRabs[] = $itemRealisasiRab;
        }

        return $this;
    }
}
