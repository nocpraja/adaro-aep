<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="monitoring_riwayat", indexes={
 *     @ORM\Index(name="ix_bid", columns={"beneficiary_id"}),
 *     @ORM\Index(name="ix_type", columns={"type"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\LembarMonitoringRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\LembarMonitoring")
 *
 * @package App\Entity\Kegiatan
 * @author  Mark Melvin
 * @since   16/11/2018
 */
class LembarMonitoring
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="riwayat_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="text")
     * @Groups({"without_rel"})
     */
    private $type;

    /**
     * @ORM\Column(name="beneficiary_id", type="bigint", nullable=true)
     * @Groups({"without_rel"})
     */
    private $beneficiaryId;

    /**
     * @ORM\Column(name="tanggal_input", type="string", nullable=true)
     * @Groups({"without_rel"})
     */
    private $tanggalInput;

    /**
     * @ORM\Column(name="nama_file", type="string", length=250, nullable=true)
     * @Groups({"without_rel"})
     */
    private $namaFile;

    /**
     * @ORM\Column(name="eval_score", type="float", nullable=true)
     * @Groups({"without_rel"})
     */
    private $evalScore;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return LembarMonitoring
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return integer
     */
    public function getBeneficiaryId()
    {
        return $this->beneficiaryId;
    }

    /**
     * @param integer $beneficiaryId
     * @return LembarMonitoring
     */
    public function setBeneficiaryId($beneficiaryId)
    {
        $this->beneficiaryId = $beneficiaryId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTanggalInput()
    {
        return $this->tanggalInput;
    }

    /**
     * @param string $tanggalInput
     * @return LembarMonitoring
     */
    public function setTanggalInput($tanggalInput)
    {
        $this->tanggalInput = $tanggalInput;
        return $this;
    }

    /**
     * @return string
     */
    public function getNamaFile()
    {
        return $this->namaFile;
    }

    /**
     * @param string $namaFile
     * @return LembarMonitoring
     */
    public function setNamaFile($namaFile)
    {
        $this->namaFile = $namaFile;
        return $this;
    }

    /**
     * @return float
     */
    public function getEvalScore()
    {
        return $this->evalScore;
    }

    /**
     * @param float $evalScore
     * @return LembarMonitoring
     */
    public function setEvalScore($evalScore)
    {
        $this->evalScore = $evalScore;
        return $this;
    }


}
