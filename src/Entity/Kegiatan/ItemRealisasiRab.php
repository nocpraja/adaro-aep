<?php

namespace App\Entity\Kegiatan;

use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="item_realisasi", indexes={
 *     @ORM\Index(name="item_realisasi_x1", columns={"id_kegiatan"}),
 *     @ORM\Index(name="item_realisasi_x2", columns={"id_subkegiatan"}),
 *     @ORM\Index(name="item_realisasi_x3", columns={"id_rab"}),
 *     @ORM\Index(name="item_realisasi_x4", columns={"posted_by"}),
 * })
 *
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\ItemRealisasiRabRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\ItemRealisasiRab")
 *
 * @package App\Entity\Kegiatan
 * @author  Alex Gaspersz
 * @since   28/08/2019, modified: 24/10/2019 00:17
 */
class ItemRealisasiRab
{
    use PostedByFkTrait,
        PostedDateTrait,
        LastUpdateByFkTrait,
        LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id_realisasi", type="bigint")
     * @Groups({"with_rel", "without_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="id_kegiatan", type="bigint")
     * @Groups({"without_rel"})
     */
    private $kegiatanRab;

    /**
     * @ORM\Column(name="id_subkegiatan", type="bigint")
     * @Groups({"without_rel"})
     */
    private $subRab;

    /**
     * @ORM\Column(name="id_rab", type="bigint")
     * @Groups({"without_rel"})
     */
    private $rabId;

    /**
     * @var ItemAnggaran
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Kegiatan\ItemAnggaran", inversedBy="realisasiRabs")
     * @ORM\JoinColumn(name="id_rab", referencedColumnName="ids", nullable=false, onDelete="CASCADE")
     * @Groups({"with_parent", "with_rel"})
     *
     */
    private $parentRab;

    /**
     * @ORM\Column(name="realisasi", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi = 0.0;

    /**
     * @ORM\Column(name="status", type="string", length=50, options={
     *     "default": "REALISASI_CREATED",
     *     "comment": "Valid values: REALISASI_CREATED, REALISASI_SUBMITTED, REALISASI_VERIFIED, REALISASI_REJECTED"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $status = WorkflowTags::REALISASI_CREATED;

    /**
     * @ORM\Column(name="tgl_realisasi", type="date", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasiDate;

    /**
     * @ORM\Column(name="keterangan", type="text", nullable=true)
     * @Groups({"without_rel"})
     */
    private $keterangan;


    /**
     * @ORM\Column(name="realisasi_paid", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi_paid = 0.0;

    /**
     * @var float $realisasi_unpaid
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi_unpaid = 0.0;


    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKegiatanRab(): ?int
    {
        return $this->kegiatanRab;
    }

    public function setKegiatanRab(int $kegiatanRab): self
    {
        $this->kegiatanRab = $kegiatanRab;

        return $this;
    }

    public function getSubRab(): ?int
    {
        return $this->subRab;
    }

    public function setSubRab(int $subRab): self
    {
        $this->subRab = $subRab;

        return $this;
    }

    public function getRabId(): ?int
    {
        return $this->rabId;
    }

    public function setRabId(int $rabId): self
    {
        $this->rabId = $rabId;

        return $this;
    }

    /**
     * Get parent relation.
     *
     * @return ItemAnggaran|null
     */
    public function getParentRab(): ?ItemAnggaran
    {
        return $this->parentRab;
    }

    /**
     * Set parent relation.
     *
     * @param ItemAnggaran $parentRab
     *
     * @return ItemRealisasiRab|null
     */
    public function setParentRab(?ItemAnggaran $parentRab): self
    {
        $this->parentRab = $parentRab;

        return $this;
    }

    public function getRealisasi(): ?float
    {
        return $this->realisasi;
    }

    public function setRealisasi(?float $realisasi): self
    {
        $this->realisasi = $realisasi;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * @param $keterangan
     *
     * @return ItemRealisasiRab
     */
    public function setKeterangan($keterangan): self
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    /**
     * Get approval status.
     *
     * @return string Salah satu dari nilai berikut: REALISASI_CREATED, REALISASI_SUBMITTED, REALISASI_VERIFIED, REALISASI_REJECTED
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set approval status.
     *
     * @param null|string $status Valid values: REALISASI_CREATED, REALISASI_SUBMITTED, REALISASI_VERIFIED, REALISASI_REJECTED
     *
     * @return ItemRealisasiRab
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Check apakah sudah disubmit atau belum
     *
     * @return bool
     */
    public function isSubmitted(): bool
    {
        return $this->status == WorkflowTags::REALISASI_SUBMITTED ? true : false;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->status == WorkflowTags::REALISASI_VERIFIED ? true : false;
    }

    /**
     * @return bool
     */
    public function isRejected(): bool
    {
        return $this->status == WorkflowTags::REALISASI_REJECTED ? true : false;
    }

    /**
     * Get tanggal realisasi rab
     *
     * @return DateTimeInterface|null
     */
    public function getRealisasiDate(): ?DateTimeInterface
    {
        return $this->realisasiDate;
    }

    /**
     * Set tanggal realisasi rab
     *
     * @param DateTimeInterface|null $realisasiDate
     *
     * @return ItemRealisasiRab
     */
    public function setRealisasiDate(?DateTimeInterface $realisasiDate): self
    {
        $this->realisasiDate = $realisasiDate;

        return $this;
    }

    /**
     * @return float
     */
    public function getRealisasiPaid(): float
    {
        return $this->realisasi_paid;
    }

    /**
     * @param float $realisasi_paid
     */
    public function setRealisasiPaid(float $realisasi_paid): void
    {
        $this->realisasi_paid = $realisasi_paid;
    }

    /**
     * @return float
     */
    public function getRealisasiUnpaid(): float
    {
        return floatval($this->realisasi) - floatval($this->realisasi_paid);
    }


}
