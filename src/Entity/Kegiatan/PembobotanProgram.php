<?php


namespace App\Entity\Kegiatan;

use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\MasterData\ProgramCSR;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pembobotan_program", indexes={
 *     @ORM\Index(name="pembobotan_program_x1", columns={"bidang_id"}),
 *     @ORM\Index(name="pembobotan_program_x2", columns={"program_id"}),
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\PembobotanProgramRepository")
 *
 * @package App\Entity\Kegiatan
 * @author  Mark Melvin
 * @since   18/02/2020
 */
class PembobotanProgram
{
    use PostedByFkTrait, PostedDateTrait, LastUpdateByFkTrait, LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="bigint", nullable=false)
     */
    private $id;

    /**
     * @var JenisProgramCSR $bidang
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\JenisProgramCSR")
     * @ORM\JoinColumn(name="bidang_id", referencedColumnName="id", nullable=false)
     * @Groups({"with_parent"})
     */
    private $bidang;

    /**
     * @var ProgramCSR $program
     * @ORM\OneToOne(targetEntity="App\Entity\MasterData\ProgramCSR")
     * @ORM\JoinColumn(name="program_id", referencedColumnName="msid", nullable=false)
     * @Groups({"with_parent"})
     */
    private $program;

    /**
     * @var float $bobot
     * @ORM\Column(name="bobot", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $bobot;

    /**
     * PembobotanProgram constructor.
     * @param ProgramCSR $program
     * @param JenisProgramCSR $bidang
     */
    public function __construct(ProgramCSR $program, JenisProgramCSR $bidang)
    {
        $this->bidang = $bidang;
        $this->program = $program;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return JenisProgramCSR
     */
    public function getBidang(): JenisProgramCSR
    {
        return $this->bidang;
    }

    /**
     * @param JenisProgramCSR $bidang
     */
    public function setBidang(JenisProgramCSR $bidang): void
    {
        $this->bidang = $bidang;
    }

    /**
     * @return ProgramCSR
     */
    public function getProgram(): ProgramCSR
    {
        return $this->program;
    }

    /**
     * @param ProgramCSR $program
     */
    public function setProgram(ProgramCSR $program): void
    {
        $this->program = $program;
    }

    /**
     * @return float
     */
    public function getBobot(): float
    {
        return $this->bobot;
    }

    /**
     * @param float $bobot
     */
    public function setBobot(float $bobot): void
    {
        $this->bobot = $bobot;
    }


}