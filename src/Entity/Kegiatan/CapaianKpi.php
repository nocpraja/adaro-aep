<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;


use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\MasterData\BatchCsr;
use App\Entity\MasterData\DataReference;
use App\Entity\MasterData\ProgramCSR;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="capaian_kpi", indexes={
 *     @ORM\Index(name="capaian_kpi_x1", columns={"msid"}),
 *     @ORM\Index(name="capaian_kpi_x2", columns={"batch_id"}),
 *     @ORM\Index(name="capaian_kpi_x3", columns={"data_ref"}),
 *     @ORM\Index(name="capaian_kpi_x4", columns={"tahun"}),
 *     @ORM\Index(name="capaian_kpi_x5", columns={"posted_by"}),
 *     @ORM\Index(name="capaian_kpi_x6", columns={"updated_by"})
 * }, options={"comment": "Table untuk data Target dan Capaian KPI"})
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\CapaianKpiRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\CapaianKpi")
 *
 * @package App\Entity\Kegiatan
 * @author  Ahmad Fajar
 * @since   27/01/2019, modified: 25/05/2019 3:12
 */
class CapaianKpi
{

    use PostedByFkTrait, PostedDateTrait, LastUpdateByFkTrait, LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="idc", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="tahun", type="integer", options={"comment": "Tahun anggaran"})
     * @Assert\NotBlank(message="tahunAnggaran.notBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahun;

    /**
     * @ORM\Column(name="dashboard", type="boolean",
     *     options={"default": false, "comment": "Tampil pada Dashboard atau tidak"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $showInDashboard;

    /**
     * @ORM\Column(name="kuantitas", type="boolean",
     *     options={"default": false, "comment": "KPI kuantitatif atau kualitatif"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $kuantitas;

    /**
     * @ORM\Column(name="label", type="string", length=200, nullable=true,
     *     options={"comment": "Dipergunakan untuk Dashboard ataupun Chart"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $label;

    /**
     * @ORM\Column(name="target", type="float", nullable=true, options={"comment": "Target capaian KPI"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $target;

    /**
     * @ORM\Column(name="p1", type="float", nullable=true,
     *     options={"comment": "Nilai capaian periode-1 (Triwulan-1, Caturwulan-1, Semester-1)"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $p1;

    /**
     * @ORM\Column(name="p2", type="float", nullable=true,
     *     options={"comment": "Nilai capaian periode-2 (Triwulan-2, Caturwulan-2, Semester-2)"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $p2;

    /**
     * @ORM\Column(name="p3", type="float", nullable=true,
     *     options={"comment": "Nilai capaian periode-3 (Triwulan-3, Caturwulan-3)"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $p3;

    /**
     * @ORM\Column(name="p4", type="float", nullable=true,
     *     options={"comment": "Nilai capaian periode-4 (Triwulan-4)"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $p4;

    /**
     * @ORM\Column(name="nilai", type="float", nullable=true, options={"comment": "Prosentase capaian KPI"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $nilai;

    /**
     * @ORM\Column(name="nilai_akhir", type="float", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $nilaiAkhir;

    /**
     * @ORM\Column(name="p1_verified", type="boolean", options={
     *     "default": false,
     *     "comment": "Nilai capaian periode-1 (Triwulan-1, Caturwulan-1, Semester-1) sudah diverifikasi?"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $p1Verified = false;

    /**
     * @ORM\Column(name="p2_verified", type="boolean", options={
     *     "default": false,
     *     "comment": "Nilai capaian periode-2 (Triwulan-2, Caturwulan-2, Semester-2) sudah diverifikasi?"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $p2Verified = false;

    /**
     * @ORM\Column(name="p3_verified", type="boolean", options={
     *     "default": false,
     *     "comment": "Nilai capaian periode-3 (Triwulan-3, Caturwulan-3) sudah diverifikasi?"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $p3Verified = false;

    /**
     * @ORM\Column(name="p4_verified", type="boolean",
     *     options={"default": false, "comment": "Nilai capaian periode-4 (Triwulan-4) sudah diverifikasi?"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $p4Verified = false;

    /**
     * @var ProgramCSR
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\ProgramCSR")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\ProgramCSR")
     * @ORM\JoinColumn(name="msid", referencedColumnName="msid", nullable=false)
     * @Assert\NotNull(message="programCsr.notNull")
     * @Groups({"with_rel"})
     */
    private $programCsr;

    /**
     * @var BatchCsr
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\BatchCsr")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\BatchCsr")
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="batch_id", nullable=false)
     * @Assert\NotNull(message="periodeBatch.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $periodeBatch;

    /**
     * @var DataReference
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\DataReference")
     * @ORM\JoinColumn(name="data_ref", referencedColumnName="id", nullable=false)
     * @Assert\NotNull(message="dataReference.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $dataRef;


    /**
     * Get referensi parameter yang terkait dengan Target & Capaian KPI.
     *
     * @return DataReference
     */
    public function getDataRef(): ?DataReference
    {
        return $this->dataRef;
    }

    /**
     * Set referensi parameter yang terkait dengan Target & Capaian KPI.
     *
     * @param DataReference $dataRef Referensi parameter
     *
     * @return CapaianKpi
     */
    public function setDataRef(DataReference $dataRef): self
    {
        $this->dataRef = $dataRef;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * KPI kuantitatif atau kualitatif.
     *
     * @return bool|null
     */
    public function getKuantitas(): ?bool
    {
        return $this->kuantitas;
    }

    /**
     * KPI kuantitatif atau kualitatif.
     *
     * @param bool $kuantitatif
     *
     * @return CapaianKpi
     */
    public function setKuantitas(bool $kuantitatif): self
    {
        $this->kuantitas = $kuantitatif;

        return $this;
    }

    /**
     * Text label untuk dipergunakan pada Dashboard ataupun Chart.
     *
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * Text label untuk dipergunakan pada Dashboard ataupun Chart.
     *
     * @param string|null $label Text label
     *
     * @return CapaianKpi
     */
    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Prosentase ataupun nilai capaian KPI dari hasil kalkulasi.
     *
     * @return float|null
     */
    public function getNilai(): ?float
    {
        return $this->nilai;
    }

    /**
     * Prosentase ataupun nilai capaian KPI dari hasil kalkulasi.
     *
     * @param float|null $value
     *
     * @return CapaianKpi
     */
    public function setNilai(?float $value): self
    {
        $this->nilai = $value;

        return $this;
    }

    /**
     * Nilai akhir capaian KPI dari hasil kalkulasi.
     *
     * @return float|null
     */
    public function getNilaiAkhir(): ?float
    {
        return $this->nilaiAkhir;
    }

    /**
     * Nilai akhir capaian KPI dari hasil kalkulasi.
     *
     * @param float|null $value
     *
     * @return CapaianKpi
     */
    public function setNilaiAkhir(?float $value): self
    {
        $this->nilaiAkhir = $value;

        return $this;
    }

    /**
     * Nilai capaian KPI untuk periode-1 (Triwulan-1, Caturwulan-1, Semester-1).
     *
     * @return float|null
     */
    public function getP1(): ?float
    {
        return $this->p1;
    }

    /**
     * Nilai capaian KPI untuk periode-1 (Triwulan-1, Caturwulan-1, Semester-1).
     *
     * @param float|null $value Nilai capaian KPI
     *
     * @return CapaianKpi
     */
    public function setP1(?float $value): self
    {
        $this->p1 = $value;

        return $this;
    }

    /**
     * Nilai capaian KPI periode-1 (Triwulan-1, Caturwulan-1, Semester-1) sudah diverifikasi atau belum?
     *
     * @return bool|null
     */
    public function getP1Verified(): ?bool
    {
        return $this->p1Verified;
    }

    /**
     * Verifikasi nilai capaian KPI periode-1 (Triwulan-1, Caturwulan-1, Semester-1).
     *
     * @param bool $verified
     *
     * @return CapaianKpi
     */
    public function setP1Verified(bool $verified): self
    {
        $this->p1Verified = $verified;

        return $this;
    }

    /**
     * Nilai capaian KPI untuk periode-2 (Triwulan-2, Caturwulan-2, Semester-2).
     *
     * @return float|null
     */
    public function getP2(): ?float
    {
        return $this->p2;
    }

    /**
     * Nilai capaian KPI untuk periode-2 (Triwulan-2, Caturwulan-2, Semester-2).
     *
     * @param float|null $value Nilai capaian KPI
     *
     * @return CapaianKpi
     */
    public function setP2(?float $value): self
    {
        $this->p2 = $value;

        return $this;
    }

    /**
     * Nilai capaian KPI periode-2 (Triwulan-2, Caturwulan-2, Semester-2) sudah diverifikasi atau belum?
     *
     * @return bool|null
     */
    public function getP2Verified(): ?bool
    {
        return $this->p2Verified;
    }

    /**
     * Verifikasi nilai capaian KPI periode-2 (Triwulan-2, Caturwulan-2, Semester-2).
     *
     * @param bool $verified
     *
     * @return CapaianKpi
     */
    public function setP2Verified(bool $verified): self
    {
        $this->p2Verified = $verified;

        return $this;
    }

    /**
     * Nilai capaian KPI untuk periode-3 (Triwulan-3, Caturwulan-3).
     *
     * @return float|null
     */
    public function getP3(): ?float
    {
        return $this->p3;
    }

    /**
     * Nilai capaian KPI untuk periode-3 (Triwulan-3, Caturwulan-3).
     *
     * @param float|null $value Nilai capaian KPI
     *
     * @return CapaianKpi
     */
    public function setP3(?float $value): self
    {
        $this->p3 = $value;

        return $this;
    }

    /**
     * Nilai capaian KPI periode-3 (Triwulan-3, Caturwulan-3) sudah diverifikasi atau belum?
     *
     * @return bool|null
     */
    public function getP3Verified(): ?bool
    {
        return $this->p3Verified;
    }

    /**
     * Verifikasi nilai capaian KPI periode-3 (Triwulan-3, Caturwulan-3).
     *
     * @param bool $verified
     *
     * @return CapaianKpi
     */
    public function setP3Verified(bool $verified): self
    {
        $this->p3Verified = $verified;

        return $this;
    }

    /**
     * Nilai capaian KPI untuk periode-4 (Triwulan-4).
     *
     * @return float|null
     */
    public function getP4(): ?float
    {
        return $this->p4;
    }

    /**
     * Nilai capaian KPI untuk periode-4 (Triwulan-4).
     *
     * @param float|null $value Nilai capaian KPI
     *
     * @return CapaianKpi
     */
    public function setP4(?float $value): self
    {
        $this->p4 = $value;

        return $this;
    }

    /**
     * Nilai capaian KPI periode-4 (Triwulan-4) sudah diverifikasi atau belum?
     *
     * @return bool|null
     */
    public function getP4Verified(): ?bool
    {
        return $this->p4Verified;
    }

    /**
     * Verifikasi nilai capaian KPI periode-4 (Triwulan-4).
     *
     * @param bool $verified
     *
     * @return CapaianKpi
     */
    public function setP4Verified(bool $verified): self
    {
        $this->p4Verified = $verified;

        return $this;
    }

    /**
     * Get periode batch Program CSR yang terkait dengan Target & Capaian KPI.
     *
     * @return BatchCsr
     */
    public function getPeriodeBatch(): ?BatchCsr
    {
        return $this->periodeBatch;
    }

    /**
     * Set periode batch Program CSR yang terkait dengan Target & Capaian KPI.
     *
     * @param BatchCsr $periodeBatch
     *
     * @return CapaianKpi
     */
    public function setPeriodeBatch(BatchCsr $periodeBatch): self
    {
        $this->periodeBatch = $periodeBatch;

        return $this;
    }

    /**
     * Get Program CSR yang terkait dengan Target & Capaian KPI.
     *
     * @return ProgramCSR
     */
    public function getProgramCsr(): ?ProgramCSR
    {
        return $this->programCsr;
    }

    /**
     * Set Program CSR yang terkait dengan Target & Capaian KPI.
     *
     * @param ProgramCSR $programCsr
     *
     * @return CapaianKpi
     */
    public function setProgramCsr(ProgramCSR $programCsr): self
    {
        $this->programCsr = $programCsr;

        return $this;
    }

    /**
     * Nilai capaian KPI tampil di Dashboard atau tidak?
     *
     * @return bool|null
     */
    public function getShowInDashboard(): ?bool
    {
        return $this->showInDashboard;
    }

    /**
     * Set nilai capaian KPI untuk tampil di Dashboard.
     *
     * @param bool $show Show in dashboard or not
     *
     * @return CapaianKpi
     */
    public function setShowInDashboard(bool $show): self
    {
        $this->showInDashboard = $show;

        return $this;
    }

    /**
     * Get tahun anggaran dari periode Batch.
     *
     * @return int|null
     */
    public function getTahun(): ?int
    {
        return $this->tahun;
    }

    /**
     * Set tahun anggaran periode Batch.
     *
     * @param int $tahun Tahun anggaran
     *
     * @return CapaianKpi
     */
    public function setTahun(int $tahun): self
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get target capaian KPI.
     *
     * @return float|null
     */
    public function getTarget(): ?float
    {
        return $this->target;
    }

    /**
     * Set target capaian KPI.
     *
     * @param float|null $target
     *
     * @return CapaianKpi
     */
    public function setTarget(?float $target): self
    {
        $this->target = $target;

        return $this;
    }
}
