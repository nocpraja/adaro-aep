<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;


use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="log_aprv_kegiatan", indexes={
 *     @ORM\Index(name="log_aprv_kegiatan_x1", columns={"kegiatan_id"}),
 *     @ORM\Index(name="log_aprv_kegiatan_x2", columns={"posted_by"})
 * }, options={"comment": "Riwayat verifikasi dan approval Kegiatan"})
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\LogApprovalKegiatanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\LogApprovalKegiatan")
 *
 * @package App\Entity\Kegiatan
 * @author  Ahmad Fajar
 * @since   13/11/2018, modified: 25/05/2019 3:06
 */
class LogApprovalKegiatan
{

    use PostedByFkTrait, PostedDateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="log_id", type="bigint")
     */
    private $logId;

    /**
     * @ORM\Column(name="log_action", type="string", length=50, options={
     *     "comment": "Tindakan yang dilakukan"
     * })
     * @Groups({"without_rel"})
     */
    private $logAction;

    /**
     * @ORM\Column(name="log_title", type="string", length=255)
     * @Groups({"without_rel"})
     */
    private $logTitle;

    /**
     * @ORM\Column(name="from_status", type="string", length=50, options={
     *     "comment": "Status kegiatan sebelum tindakan verifikasi ataupun approval"
     * })
     * @Groups({"without_rel"})
     */
    private $fromStatus;

    /**
     * @ORM\Column(name="to_status", type="string", length=50, options={
     *     "comment": "Status kegiatan setelah tindakan verifikasi ataupun approval"
     * })
     * @Groups({"without_rel"})
     */
    private $toStatus;

    /**
     * @ORM\Column(name="keterangan", type="text", nullable=true)
     * @Groups({"without_rel"})
     */
    private $keterangan;

    /**
     * @var Kegiatan
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\Kegiatan")
     * @ORM\ManyToOne(targetEntity="App\Entity\Kegiatan\Kegiatan")
     * @ORM\JoinColumn(name="kegiatan_id", referencedColumnName="kegiatan_id", nullable=false, onDelete="CASCADE")
     * @Groups({"with_parent"})
     */
    private $kegiatan;


    /**
     * Get status Kegiatan sebelum tindakan verifikasi ataupun approval.
     *
     * @return string
     */
    public function getFromStatus(): ?string
    {
        return $this->fromStatus;
    }

    /**
     * Set status Kegiatan sebelum tindakan verifikasi ataupun approval
     *
     * @param string $status Status Kegiatan
     *
     * @return LogApprovalKegiatan
     */
    public function setFromStatus(string $status): self
    {
        $this->fromStatus = $status;

        return $this;
    }

    /**
     * @return Kegiatan
     */
    public function getKegiatan(): ?Kegiatan
    {
        return $this->kegiatan;
    }

    /**
     * @param Kegiatan $kegiatan
     *
     * @return LogApprovalKegiatan
     */
    public function setKegiatan(Kegiatan $kegiatan): self
    {
        $this->kegiatan = $kegiatan;

        return $this;
    }

    /**
     * Keterangan tambahan jika REJECTED, VERIFIED ataupun APPROVED.
     *
     * @return null|string
     */
    public function getKeterangan(): ?string
    {
        return $this->keterangan;
    }

    /**
     * Keterangan tambahan jika REJECTED, VERIFIED ataupun APPROVED.
     *
     * @param null|string $keterangan
     *
     * @return LogApprovalKegiatan
     */
    public function setKeterangan(?string $keterangan): self
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    /**
     * Tindakan verifikasi ataupun approval yang dilakukan.
     *
     * @return null|string
     */
    public function getLogAction(): ?string
    {
        return $this->logAction;
    }

    /**
     * Tindakan verifikasi ataupun approval yang dilakukan.
     *
     * @param string $action Tindakan yang dilakukan
     *
     * @return LogApprovalKegiatan
     */
    public function setLogAction(string $action): self
    {
        $this->logAction = $action;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getLogId(): ?int
    {
        return $this->logId;
    }

    /**
     * Get log approval title.
     *
     * @return null|string
     */
    public function getLogTitle(): ?string
    {
        return $this->logTitle;
    }

    /**
     * Set log approval title.
     *
     * @param string $logTitle
     *
     * @return LogApprovalKegiatan
     */
    public function setLogTitle(string $logTitle): self
    {
        $this->logTitle = $logTitle;

        return $this;
    }

    /**
     * Get status Kegiatan setelah tindakan verifikasi ataupun approval.
     *
     * @return string|null
     */
    public function getToStatus(): ?string
    {
        return $this->toStatus;
    }

    /**
     * Set status Kegiatan setelah tindakan verifikasi ataupun approval.
     *
     * @param string $status Status pendanaan
     *
     * @return LogApprovalKegiatan
     */
    public function setToStatus(string $status): self
    {
        $this->toStatus = $status;

        return $this;
    }

}
