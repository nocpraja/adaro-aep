<?php
/**
 * Created by PhpStorm.
 * User: melv
 * Date: 11/17/2018
 * Time: 3:57 AM
 */

namespace App\Entity\Kegiatan;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @package App\Entity\Kegiatan
 * @author  Mark Melvin
 * @since   17/11/2018
 *
 * @ORM\Entity()
 */
class SubyekMonitoring
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="bigint")
     * @Groups({"without_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="nama", type="text")
     * @Groups({"without_rel"})
     */
    private $nama;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return SubyekMonitoring
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * @param string $nama
     * @return SubyekMonitoring
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
        return $this;
    }


}