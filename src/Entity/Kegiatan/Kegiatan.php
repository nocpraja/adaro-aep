<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;


use App\Entity\MasterData\BatchCsr;
use App\Entity\MasterData\DataReference;
use App\Entity\Pendanaan\ApprovedByTrait;
use App\Entity\Pendanaan\ClosedByTrait;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\UpdatedByFkTrait;
use App\Entity\Pendanaan\VerifiedByTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Entity\Security\UserAccount;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Service\Security\AppUserInterface;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="kegiatan", indexes={
 *     @ORM\Index(name="kegiatan_x1", columns={"bp_id"}),
 *     @ORM\Index(name="kegiatan_x2", columns={"batch_id"}),
 *     @ORM\Index(name="kegiatan_x3", columns={"payment_method"}),
 *     @ORM\Index(name="kegiatan_x4", columns={"submitted_by"}),
 *     @ORM\Index(name="kegiatan_x5", columns={"posted_realisasi_by"}),
 *     @ORM\Index(name="kegiatan_x6", columns={"verified_by"}),
 *     @ORM\Index(name="kegiatan_x7", columns={"approved_by"}),
 *     @ORM\Index(name="kegiatan_x8", columns={"closed_by"}),
 *     @ORM\Index(name="kegiatan_x9", columns={"posted_by"}),
 *     @ORM\Index(name="kegiatan_x10", columns={"updated_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\KegiatanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\Kegiatan")
 *
 * @package App\Entity\Kegiatan
 * @author  Ahmad Fajar
 * @since   13/11/2018, modified: 25/05/2019 2:56
 */
class Kegiatan
{

    use VerifiedByTrait, ApprovedByTrait, PostRealisasiTrait, ClosedByTrait;
    use PostedByFkTrait, PostedDateTrait, UpdatedByFkTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="kegiatan_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kegiatanId;

    /**
     * @ORM\Column(name="status", type="string", length=50, options={
     *     "default": "NEW",
     *     "comment": "Valid values: NEW, UPDATED, SUBMITTED, CLOSED, VERIFIED, REJECTED, APPROVED_1,
     * APPROVED_2, APPROVED_3"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $status = WorkflowTags::NEW;

    /**
     * @ORM\Column(name="kode", type="string", length=25, options={
     *     "comment": "Kode untuk referensi Riwayat Transaksi Kegiatan. Format: PK-yy.0000"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $kode;

    /**
     * @ORM\Column(name="nama_kegiatan", type="string", length=250)
     * @Assert\NotBlank(message="Kegiatan.NamaKegiatan.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaKegiatan;

    /**
     * @ORM\Column(name="submitted", type="boolean", options={
     *     "default": false,
     *     "comment": "Sudah di submit ke Adaro atau belum"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $submitted = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="billed", type="boolean", options={
     *     "default": false,
     *     "comment": "Sudah melakukan penagihan ke Adaro atau belum"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $billed = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="closed", type="boolean", options={"default": false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $closed = false;

    /**
     * @ORM\Column(name="anggaran", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $anggaran = 0.0;

    /**
     * @ORM\Column(name="realisasi", type="float", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi;

    /**
     * @var float
     * @Groups({"without_rel"})
     */
    private $totalKegiatanRealisasi = 0.0;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $totalKegiatanDokumentasi = 0;

    /**
     * @var array
     *
     * @ORM\Column(name="jadwal_kegiatan", type="json", nullable=true, options={"jsonb": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $jadwalKegiatan = null;

    /**
     * @var UserAccount
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="submitted_by", referencedColumnName="uid", nullable=true)
     * @Groups({"with_approval"})
     */
    private $submittedBy;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="tgl_submitted", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tanggalSubmitted;

    /**
     * @var DanaBatch
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBatch")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaBatch")
     * @ORM\JoinColumn(name="bp_id", referencedColumnName="bp_id", nullable=false)
     * @Groups({"without_rel", "with_rel"})
     */
    private $danaBatch;

    /**
     * @var BatchCsr
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\BatchCsr")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\BatchCsr")
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="batch_id", nullable=false)
     * @Groups({"without_rel", "with_rel"})
     */
    private $periodeBatch;

    /**
     * @var DataReference
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\DataReference")
     * @ORM\JoinColumn(name="payment_method", referencedColumnName="id")
     * @Groups({"with_rel"})
     */
    private $paymentMethod;

    /**
     * @var Subkegiatan[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Kegiatan\Subkegiatan", mappedBy="kegiatan")
     * @ORM\OrderBy({"postedDate" = "ASC"})
     * @Groups({"with_rel"})
     */
    private $children;

    /**
     * @var KendaliKegiatan|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Kegiatan\KendaliKegiatan", mappedBy="kegiatan", orphanRemoval=true)
     * @Groups({"with_projectcontrol"})
     */
    private $kendaliKegiatan;

    /**
     * @var string
     * @Groups({"without_rel"})
     */
    private $latestDocument = null;

    /**
     * @var string
     * @Groups({"without_rel"})
     */
    private $latestGallery = null;

    /**
     * @var int
     * @Groups({"without_rel"})
     */
    private $verifiedDokumen = 0;

    /**
     * @ORM\Column(name="jenis_pembayaran", type="string", length=20,  nullable=true, options={"default": ""})
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenisPembayaran;

    /**
     * @ORM\Column(name="termin1", type="boolean",  nullable=true, options={"default": false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $termin1;

    /**
     * @ORM\Column(name="termin1_percent", type="integer",  nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $termin1_percent;

    /**
     * @ORM\Column(name="termin_pelunasan", type="boolean",  nullable=true, options={"default": false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $termin_pelunasan;

    /**
     * @ORM\Column(name="pencairan", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $pencairan = 0.0;

    /**
     * @var float $pencairan_sisa
     * @Groups({"without_rel", "with_rel"})
     */
    private $pencairan_sisa = 0.0;

    /**
     * Kegiatan constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Get jumlah anggaran RAB.
     *
     * @return float|null
     */
    public function getAnggaran(): ?float
    {
        return $this->anggaran;
    }

    /**
     * Set jumlah anggaran RAB.
     *
     * @param float|null $anggaran
     *
     * @return Kegiatan
     */
    public function setAnggaran(?float $anggaran): self
    {
        $this->anggaran = $anggaran;

        return $this;
    }

    /**
     * @return Subkegiatan[]|Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Subkegiatan[]|Collection $elements
     *
     * @return Kegiatan
     */
    public function setChildren(?array $elements): self
    {
        if (!empty($elements) && is_array($elements)) {
            $this->children = new ArrayCollection($elements);
        } elseif ($elements instanceof Collection) {
            $this->children = $elements;
        } else {
            $this->children->clear();
        }

        foreach ($this->children as $child) {
            $child->setKegiatan($this);
        }

        return $this;
    }

    /**
     * @return DanaBatch
     */
    public function getDanaBatch(): ?DanaBatch
    {
        return $this->danaBatch;
    }

    /**
     * @param DanaBatch $danaBatch
     *
     * @return Kegiatan
     */
    public function setDanaBatch(DanaBatch $danaBatch): self
    {
        $this->danaBatch = $danaBatch;

        return $this;
    }

    /**
     * Get collection of jadwal pelaksanaan kegiatan.
     *
     * @return array
     */
    public function getJadwalKegiatan(): ?array
    {
        return $this->jadwalKegiatan;
    }

    /**
     * Set jadwal pelaksanaan kegiatan.
     *
     * @param array $jadwal Collection of jadwal pelaksanaan
     *
     * @return Kegiatan
     */
    public function setJadwalKegiatan(?array $jadwal): self
    {
        $this->jadwalKegiatan = $jadwal;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getKegiatanId(): ?int
    {
        return $this->kegiatanId;
    }

    /**
     * Get kode kegiatan.
     *
     * @return string
     */
    public function getKode(): ?string
    {
        return $this->kode;
    }

    /**
     * Set kode kegiatan.
     *
     * @param string $kode Kode kegiatan
     *
     * @return Kegiatan
     */
    public function setKode(string $kode): self
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get nama kegiatan.
     *
     * @return null|string
     */
    public function getNamaKegiatan(): ?string
    {
        return $this->namaKegiatan;
    }

    /**
     * Set nama kegiatan.
     *
     * @param string $namaKegiatan
     *
     * @return Kegiatan
     */
    public function setNamaKegiatan(string $namaKegiatan): self
    {
        $this->namaKegiatan = $namaKegiatan;

        return $this;
    }

    /**
     * @return DataReference
     */
    public function getPaymentMethod(): ?DataReference
    {
        return $this->paymentMethod;
    }

    /**
     * @param DataReference $paymentMethod
     *
     * @return Kegiatan
     */
    public function setPaymentMethod(?DataReference $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * @return BatchCsr
     */
    public function getPeriodeBatch(): ?BatchCsr
    {
        return $this->periodeBatch;
    }

    /**
     * @param BatchCsr $periodeBatch
     *
     * @return Kegiatan
     */
    public function setPeriodeBatch(BatchCsr $periodeBatch): self
    {
        $this->periodeBatch = $periodeBatch;

        return $this;
    }

    /**
     * Get jumlah realisasi anggaran RAB.
     *
     * @return float|null
     */
    public function getRealisasi(): ?float
    {
        return $this->realisasi;
    }

    /**
     * Set jumlah realisasi anggaran RAB.
     *
     * @param float|null $realisasi
     *
     * @return Kegiatan
     */
    public function setRealisasi(?float $realisasi): self
    {
        $this->realisasi = $realisasi;

        return $this;
    }

    public function getTotalKegiatanRealisasi(): int
    {
        $total = 0;
        foreach ($this->children as $row) {
            $total += $row->getVerifiedSubRealisasi();
        }
        return $total;
    }

    /**
     * @return int
     */
    public function getTotalKegiatanDokumentasi(): int
    {
        $total = 0;
        foreach ($this->children as $row) {
            $total += $row->getTotalDokumentasi();
        }
        return $total;
    }

    /**
     * @return mixed|null
     */
    public function getLatestDocument(): ?string
    {
        $extension = null;
        foreach ($this->children as $row) {
            $extension = $row->getLatestDocument();
        }
        return $extension;
    }

    /**
     * @return string|null
     */
    public function getLatestGallery(): ?string
    {
        $url = null;
        foreach ($this->children as $row) {
            $url = $row->getLatestGallery();
        }
        return $url;
    }

    /**
     * @return int
     */
    public function getVerifiedDokumen(): int
    {
        $total = 0;
        foreach ($this->children as $row) {
            $total += $row->getVerifiedDokumen();
        }
        return $total;
    }

    /**
     * Get approval status.
     *
     * @return string Salah satu dari nilai berikut: NEW, UPDATED, SUBMITTED, CLOSED, VERIFIED, REJECTED,
     *                APPROVED_1, APPROVED_2, APPROVED_3
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set approval status.
     *
     * @param null|string $status Valid values: NEW, UPDATED, SUBMITTED, CLOSED, VERIFIED, REJECTED,
     *                            APPROVED_1, APPROVED_2, APPROVED_3
     *
     * @return Kegiatan
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return UserAccount
     */
    public function getSubmittedBy(): ?UserAccount
    {
        return $this->submittedBy;
    }

    /**
     * @param UserAccount|AppUserInterface $submittedBy
     *
     * @return Kegiatan
     */
    public function setSubmittedBy(?UserAccount $submittedBy): self
    {
        $this->submittedBy = $submittedBy;

        return $this;
    }

    /**
     * Get tanggal submission anggaran.
     *
     * @return DateTimeInterface
     */
    public function getTanggalSubmitted(): ?DateTimeInterface
    {
        return $this->tanggalSubmitted;
    }

    /**
     * Set tanggal submission anggaran.
     *
     * @param DateTimeInterface $date Tanggal submission
     *
     * @return Kegiatan
     */
    public function setTanggalSubmitted(?DateTimeInterface $date): self
    {
        $this->tanggalSubmitted = $date;

        return $this;
    }

    /**
     * Memeriksa apakah Kegiatan sudah memiliki subkegiatan atau belum.
     */
    public function hasChildren(): bool
    {
        return $this->getChildren()->count() > 0;
    }

    /**
     * Periksa apakah sudah melakukan penagihan ke Adaro atau belum.
     *
     * @return bool
     */
    public function isBilled(): bool
    {
        return $this->billed;
    }

    /**
     * Set flag: sudah melakukan penagihan ke Adaro atau belum.
     *
     * @param bool $billed If TRUE berarti sudah melakukan penagihan, FALSE sebaliknya
     *
     * @return Kegiatan
     */
    public function setBilled(bool $billed): self
    {
        $this->billed = $billed;

        return $this;
    }

    /**
     * Periksa apakah kegiatan sudah ditutup (CLOSE) atau belum.
     *
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }

    /**
     * Set flag kegiatan: OPEN or CLOSE.
     *
     * @param bool $closed If TRUE berarti CLOSE, FALSE berarti OPEN
     *
     * @return Kegiatan
     */
    public function setClosed(bool $closed): self
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Periksa apakah kegiatan sudah disubmit ke Adaro atau belum.
     *
     * @return boolean
     */
    public function isSubmitted(): ?bool
    {
        return $this->submitted;
    }

    /**
     * Set submission flag.
     *
     * @param boolean $submitted Submission flag
     *
     * @return Kegiatan
     */
    public function setSubmitted(bool $submitted): self
    {
        $this->submitted = $submitted;

        return $this;
    }

    /**
     * @return KendaliKegiatan|null
     */
    public function getKendaliKegiatan(): ?KendaliKegiatan
    {
        return $this->kendaliKegiatan;
    }

    /**
     * @param KendaliKegiatan|null $kendaliKegiatan
     */
    public function setKendaliKegiatan(?KendaliKegiatan $kendaliKegiatan): void
    {
        $this->kendaliKegiatan = $kendaliKegiatan;
    }

    /**
     * @return float
     */
    public function getPencairan(): float
    {
        return $this->pencairan;
    }

    /**
     * @param float $pencairan
     */
    public function setPencairan(float $pencairan): void
    {
        $this->pencairan = $pencairan;
    }

    /**
     * @return float
     */
    public function getPencairanSisa(): float
    {
        return floatval($this->anggaran) - floatval($this->pencairan);
    }

    /**
     * @return string
     */
    public function getJenisPembayaran()
    {
        return $this->jenisPembayaran;
    }

    /**
     * @param string $jenisPembayaran
     */
    public function setJenisPembayaran($jenisPembayaran): void
    {
        $this->jenisPembayaran = $jenisPembayaran;
    }

    /**
     * @return boolean
     */
    public function getTermin1()
    {
        return $this->termin1;
    }

    /**
     * @param boolean $termin1
     */
    public function setTermin1($termin1): void
    {
        $this->termin1 = $termin1;
    }

    /**
     * @return int
     */
    public function getTermin1Percent()
    {
        return $this->termin1_percent;
    }

    /**
     * @param int $termin1_percent
     */
    public function setTermin1Percent($termin1_percent): void
    {
        $this->termin1_percent = $termin1_percent;
    }

    /**
     * @return boolean
     */
    public function getTerminPelunasan()
    {
        return $this->termin_pelunasan;
    }

    /**
     * @param boolean $termin_pelunasan
     */
    public function setTerminPelunasan($termin_pelunasan): void
    {
        $this->termin_pelunasan = $termin_pelunasan;
    }


}
