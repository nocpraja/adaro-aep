<?php


namespace App\Entity\Kegiatan;

use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pembobotan_bidang", indexes={
 *     @ORM\Index(name="pembobotan_bidang_x1", columns={"bidang_id"}),
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\PembobotanBidangRepository")
 *
 * @package App\Entity\Kegiatan
 * @author  Mark Melvin
 * @since   18/02/2020
 */
class PembobotanBidang
{
    use PostedByFkTrait, PostedDateTrait, LastUpdateByFkTrait, LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="bigint", nullable=false)
     */
    private $id;

    /**
     * @var JenisProgramCSR $bidang
     * @ORM\OneToOne(targetEntity="App\Entity\MasterData\JenisProgramCSR")
     * @ORM\JoinColumn(name="bidang_id", referencedColumnName="id", nullable=false)
     * @Groups({"with_parent"})
     */
    private $bidang;

    /**
     * @var float $bobot
     * @ORM\Column(name="bobot", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $bobot;

    /**
     * PembobotanBidang constructor.
     * @param JenisProgramCSR $bidang
     */
    public function __construct(JenisProgramCSR $bidang)
    {
        $this->bidang = $bidang;
        $this->bobot = 0.0;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return JenisProgramCSR
     */
    public function getBidang(): JenisProgramCSR
    {
        return $this->bidang;
    }

    /**
     * @param JenisProgramCSR $bidang
     */
    public function setBidang(JenisProgramCSR $bidang): void
    {
        $this->bidang = $bidang;
    }

    /**
     * @return float
     */
    public function getBobot(): float
    {
        return $this->bobot;
    }

    /**
     * @param float $bobot
     */
    public function setBobot(float $bobot): void
    {
        $this->bobot = $bobot;
    }

}