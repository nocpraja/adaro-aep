<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Kegiatan;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="kegiatan_realisasi", indexes={
 *     @ORM\Index(name="ix_kegiatan", columns={"kegiatan_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Kegiatan\KendaliKegiatanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\KendaliKegiatan")
 *
 * @package App\Entity\Kegiatan
 * @author  Mark Melvin
 * @since   17/7/2019
 */
class KendaliKegiatan
{
    /**
     * @var Kegiatan $kegiatan
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Kegiatan\Kegiatan")
     * @ORM\OneToOne(targetEntity="App\Entity\Kegiatan\Kegiatan", inversedBy="kendaliKegiatan")
     * @ORM\JoinColumn(name="kegiatan_id", referencedColumnName="kegiatan_id", nullable=false)
     * @Groups({"with_parent"})
     */
    private $kegiatan;

    /**
     * @ORM\Id
     * @ORM\Column(name="kegiatan_id", type="bigint", nullable=false)
     */
    private $kegiatan_id;

    /**
     * @var float|null $bobot
     * @ORM\Column(name="bobot", type="float", nullable=true, options={"comment": "Bobot kegiatan dalam persen untuk perhitungan batch"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $bobot;

    /**
     * @var float|null $rencana1
     * @ORM\Column(name="rencana1", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 1 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana1;

    /**
     * @var float|null $rencana2
     * @ORM\Column(name="rencana2", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 2 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana2;

    /**
     * @var float|null $rencana3
     * @ORM\Column(name="rencana3", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 3 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana3;

    /**
     * @var float|null $rencana4
     * @ORM\Column(name="rencana4", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 4 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana4;

    /**
     * @var float|null $rencana5
     * @ORM\Column(name="rencana5", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 5 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana5;

    /**
     * @var float|null $rencana6
     * @ORM\Column(name="rencana6", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 6 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana6;

    /**
     * @var float|null $rencana7
     * @ORM\Column(name="rencana7", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 7 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana7;

    /**
     * @var float|null $rencana8
     * @ORM\Column(name="rencana8", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 8 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana8;

    /**
     * @var float|null $rencana9
     * @ORM\Column(name="rencana9", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 9 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana9;

    /**
     * @var float|null $rencana10
     * @ORM\Column(name="rencana10", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 10 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana10;

    /**
     * @var float|null $rencana11
     * @ORM\Column(name="rencana11", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 11 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana11;

    /**
     * @var float|null $rencana12
     * @ORM\Column(name="rencana12", type="float", nullable=true, options={"comment": "Rencana progress kegiatan untuk bulan 12 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $rencana12;


    /**
     * @var float|null $realisasi1
     * @ORM\Column(name="realisasi1", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 1 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi1;

    /**
     * @var float|null $realisasi2
     * @ORM\Column(name="realisasi2", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 2 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi2;

    /**
     * @var float|null $realisasi3
     * @ORM\Column(name="realisasi3", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 3 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi3;

    /**
     * @var float|null $realisasi4
     * @ORM\Column(name="realisasi4", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 4 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi4;

    /**
     * @var float|null $realisasi5
     * @ORM\Column(name="realisasi5", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 5 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi5;

    /**
     * @var float|null $realisasi6
     * @ORM\Column(name="realisasi6", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 6 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi6;

    /**
     * @var float|null $realisasi7
     * @ORM\Column(name="realisasi7", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 7 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi7;

    /**
     * @var float|null $realisasi8
     * @ORM\Column(name="realisasi8", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 8 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi8;

    /**
     * @var float|null $realisasi9
     * @ORM\Column(name="realisasi9", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 9 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi9;

    /**
     * @var float|null $realisasi10
     * @ORM\Column(name="realisasi10", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 10 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi10;

    /**
     * @var float|null $realisasi11
     * @ORM\Column(name="realisasi11", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 11 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi11;

    /**
     * @var float|null $realisasi12
     * @ORM\Column(name="realisasi12", type="float", nullable=true, options={"comment": "Realisasi progress kegiatan untuk bulan 12 "})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi12;

    /**
     * @return Kegiatan
     */
    public function getKegiatan(): ?Kegiatan
    {
        return $this->kegiatan;
    }

    /**
     * @param Kegiatan $kegiatan
     */
    public function setKegiatan(Kegiatan $kegiatan): void
    {
        $this->kegiatan = $kegiatan;
        if ($kegiatan!==null){
            $this->kegiatan_id = $kegiatan->getKegiatanId();
        }
    }

    /**
     * @return float
     */
    public function getBobot(): ?float
    {
        return $this->bobot;
    }

    /**
     * @param float $bobot
     */
    public function setBobot(?float $bobot): void
    {
        $this->bobot = $bobot;
    }

    /**
     * @return float|null
     */
    public function getRencana1(): ?float
    {
        return $this->rencana1;
    }

    /**
     * @param float|null $rencana1
     */
    public function setRencana1(?float $rencana1): void
    {
        $this->rencana1 = $rencana1;
    }

    /**
     * @return float|null
     */
    public function getRencana2(): ?float
    {
        return $this->rencana2;
    }

    /**
     * @param float|null $rencana2
     */
    public function setRencana2(?float $rencana2): void
    {
        $this->rencana2 = $rencana2;
    }

    /**
     * @return float|null
     */
    public function getRencana3(): ?float
    {
        return $this->rencana3;
    }

    /**
     * @param float|null $rencana3
     */
    public function setRencana3(?float $rencana3): void
    {
        $this->rencana3 = $rencana3;
    }

    /**
     * @return float|null
     */
    public function getRencana4(): ?float
    {
        return $this->rencana4;
    }

    /**
     * @param float|null $rencana4
     */
    public function setRencana4(?float $rencana4): void
    {
        $this->rencana4 = $rencana4;
    }

    /**
     * @return float|null
     */
    public function getRencana5(): ?float
    {
        return $this->rencana5;
    }

    /**
     * @param float|null $rencana5
     */
    public function setRencana5(?float $rencana5): void
    {
        $this->rencana5 = $rencana5;
    }

    /**
     * @return float|null
     */
    public function getRencana6(): ?float
    {
        return $this->rencana6;
    }

    /**
     * @param float|null $rencana6
     */
    public function setRencana6(?float $rencana6): void
    {
        $this->rencana6 = $rencana6;
    }

    /**
     * @return float|null
     */
    public function getRencana7(): ?float
    {
        return $this->rencana7;
    }

    /**
     * @param float|null $rencana7
     */
    public function setRencana7(?float $rencana7): void
    {
        $this->rencana7 = $rencana7;
    }

    /**
     * @return float|null
     */
    public function getRencana8(): ?float
    {
        return $this->rencana8;
    }

    /**
     * @param float|null $rencana8
     */
    public function setRencana8(?float $rencana8): void
    {
        $this->rencana8 = $rencana8;
    }

    /**
     * @return float|null
     */
    public function getRencana9(): ?float
    {
        return $this->rencana9;
    }

    /**
     * @param float|null $rencana9
     */
    public function setRencana9(?float $rencana9): void
    {
        $this->rencana9 = $rencana9;
    }

    /**
     * @return float|null
     */
    public function getRencana10(): ?float
    {
        return $this->rencana10;
    }

    /**
     * @param float|null $rencana10
     */
    public function setRencana10(?float $rencana10): void
    {
        $this->rencana10 = $rencana10;
    }

    /**
     * @return float|null
     */
    public function getRencana11(): ?float
    {
        return $this->rencana11;
    }

    /**
     * @param float|null $rencana11
     */
    public function setRencana11(?float $rencana11): void
    {
        $this->rencana11 = $rencana11;
    }

    /**
     * @return float|null
     */
    public function getRencana12(): ?float
    {
        return $this->rencana12;
    }

    /**
     * @param float|null $rencana12
     */
    public function setRencana12(?float $rencana12): void
    {
        $this->rencana12 = $rencana12;
    }

    /**
     * @return float|null
     */
    public function getRealisasi1(): ?float
    {
        return $this->realisasi1;
    }

    /**
     * @param float|null $realisasi1
     */
    public function setRealisasi1(?float $realisasi1): void
    {
        $this->realisasi1 = $realisasi1;
    }

    /**
     * @return float|null
     */
    public function getRealisasi2(): ?float
    {
        return $this->realisasi2;
    }

    /**
     * @param float|null $realisasi2
     */
    public function setRealisasi2(?float $realisasi2): void
    {
        $this->realisasi2 = $realisasi2;
    }

    /**
     * @return float|null
     */
    public function getRealisasi3(): ?float
    {
        return $this->realisasi3;
    }

    /**
     * @param float|null $realisasi3
     */
    public function setRealisasi3(?float $realisasi3): void
    {
        $this->realisasi3 = $realisasi3;
    }

    /**
     * @return float|null
     */
    public function getRealisasi4(): ?float
    {
        return $this->realisasi4;
    }

    /**
     * @param float|null $realisasi4
     */
    public function setRealisasi4(?float $realisasi4): void
    {
        $this->realisasi4 = $realisasi4;
    }

    /**
     * @return float|null
     */
    public function getRealisasi5(): ?float
    {
        return $this->realisasi5;
    }

    /**
     * @param float|null $realisasi5
     */
    public function setRealisasi5(?float $realisasi5): void
    {
        $this->realisasi5 = $realisasi5;
    }

    /**
     * @return float|null
     */
    public function getRealisasi6(): ?float
    {
        return $this->realisasi6;
    }

    /**
     * @param float|null $realisasi6
     */
    public function setRealisasi6(?float $realisasi6): void
    {
        $this->realisasi6 = $realisasi6;
    }

    /**
     * @return float|null
     */
    public function getRealisasi7(): ?float
    {
        return $this->realisasi7;
    }

    /**
     * @param float|null $realisasi7
     */
    public function setRealisasi7(?float $realisasi7): void
    {
        $this->realisasi7 = $realisasi7;
    }

    /**
     * @return float|null
     */
    public function getRealisasi8(): ?float
    {
        return $this->realisasi8;
    }

    /**
     * @param float|null $realisasi8
     */
    public function setRealisasi8(?float $realisasi8): void
    {
        $this->realisasi8 = $realisasi8;
    }

    /**
     * @return float|null
     */
    public function getRealisasi9(): ?float
    {
        return $this->realisasi9;
    }

    /**
     * @param float|null $realisasi9
     */
    public function setRealisasi9(?float $realisasi9): void
    {
        $this->realisasi9 = $realisasi9;
    }

    /**
     * @return float|null
     */
    public function getRealisasi10(): ?float
    {
        return $this->realisasi10;
    }

    /**
     * @param float|null $realisasi10
     */
    public function setRealisasi10(?float $realisasi10): void
    {
        $this->realisasi10 = $realisasi10;
    }

    /**
     * @return float|null
     */
    public function getRealisasi11(): ?float
    {
        return $this->realisasi11;
    }

    /**
     * @param float|null $realisasi11
     */
    public function setRealisasi11(?float $realisasi11): void
    {
        $this->realisasi11 = $realisasi11;
    }

    /**
     * @return float|null
     */
    public function getRealisasi12(): ?float
    {
        return $this->realisasi12;
    }

    /**
     * @param float|null $realisasi12
     */
    public function setRealisasi12(?float $realisasi12): void
    {
        $this->realisasi12 = $realisasi12;
    }

}
