<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait PostedDateTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   26/02/2018, 10/04/2019 20:56
 */
trait PostedDateTrait
{
    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="posted_date", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     * @Groups({"without_rel", "with_postdate"})
     */
    private $postedDate;

    /**
     * Get the posted date-time.
     *
     * @return DateTimeInterface
     */
    public function getPostedDate(): ?DateTimeInterface
    {
        return $this->postedDate;
    }

    /**
     * Set the posted date-time.
     *
     * @param DateTimeInterface $postedDate
     */
    public function setPostedDate(DateTimeInterface $postedDate): void
    {
        $this->postedDate = $postedDate;
    }

}