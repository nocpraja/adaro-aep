<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait LastUpdateTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 22/04/2019 10:41
 */
trait LastUpdateTrait
{

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_updated", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"with_postdate"})
     */
    private $lastUpdated;

    /**
     * Get last modified datetime.
     *
     * @return DateTimeInterface
     */
    public function getLastUpdated(): ?DateTimeInterface
    {
        return $this->lastUpdated;
    }

    /**
     * Set the modified datetime.
     *
     * @param DateTimeInterface $lastUpdated
     */
    public function setLastUpdated(?DateTimeInterface $lastUpdated): void
    {
        $this->lastUpdated = $lastUpdated;
    }

}