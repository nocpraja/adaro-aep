### Direktori: src\Entity

Direktori ini dipergunakan untuk Entity object yang merefleksikan struktur tabel pada database.

Sebaiknya disusun dalam beberapa sub-direktori yang disesuaikan dengan topik bisnis process,
dan sedapat mungkin konsisten dengan struktur folder pada: `assets\js\models` dan `src\Repository`.

Contoh struktur direktori:
```text
-- Entity
   |--- Security
   |--- Kegiatan
   |--- ModuleName
```