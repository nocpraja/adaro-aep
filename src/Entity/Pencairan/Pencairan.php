<?php

/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Pencairan;

use App\Entity\Kegiatan\Kegiatan;
use App\Entity\MasterData\Mitra;
use App\Entity\Security\UserAccount;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="pencairan", indexes={
 *     @ORM\Index(name="pencairan_x1", columns={"mitra_id"}),
 *     @ORM\Index(name="pencairan_x2", columns={"created_by"}),
 *     @ORM\Index(name="pencairan_x3", columns={"printed_by"}),
 *     @ORM\Index(name="pencairan_x4", columns={"sent_by"}),
 *     @ORM\Index(name="pencairan_x5", columns={"received_by"}),
 *     @ORM\Index(name="pencairan_x6", columns={"approved_by"}),
 *     @ORM\Index(name="pencairan_x7", columns={"rejected_by"}),
 *     @ORM\Index(name="pencairan_x8", columns={"paid_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Pencairan\PencairanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pencairan\Pencairan")
 *
 * @package App\Entity\Pencairan
 * @author  Mark Melvin
 * @since   31/05/2020, modified: 31/05/2020 02:23
 */
class Pencairan
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="pencairan_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @var Mitra
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Mitra")
     * @ORM\JoinColumn(name="mitra_id", referencedColumnName="mitra_id", nullable=false, onDelete="CASCADE")
     * @Groups({"without_rel", "with_rel"})
     */
    private $mitra;

    /**
     * @ORM\Column(name="tahun", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahun;

    /**
     * @ORM\Column(name="urutan", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $urutan;

    /**
     * @ORM\Column(name="nomor_surat", type="text", length=80)
     * @Groups({"without_rel", "with_rel"})
     */
    private $nomorSurat;

    /**
     * @ORM\Column(name="tipe_termin", type="string", length=80)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tipe_termin;

    /**
     * @ORM\Column(name="persen", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $persen;


    /**
     * @ORM\Column(name="is_expense", type="boolean", options={"default": false })
     * @Groups({"without_rel", "with_rel"})
     */
    private $isExpense;


    /**
     * @ORM\Column(name="is_pelunasan", type="boolean", options={"default": false })
     * @Groups({"without_rel", "with_rel"})
     */
    private $isPelunasan;


    /**
     * @ORM\Column(name="url_file_tagihan", type="string", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $urlFileTagihan = null;

    /**
     * @ORM\Column(name="att_path", type="string", nullable=true, length=200)
     * @Groups({"without_rel"})
     */
    private $filePath;

    /**
     * @ORM\Column(name="att_file", type="string", nullable=true, length=100)
     * @Groups({"without_rel"})
     */
    private $fileName;

    /**
     * @ORM\Column(name="att_type", type="string", nullable=true, length=10)
     * @Groups({"without_rel"})
     */
    private $fileExt;

    /**
     * @ORM\Column(name="att_size", type="integer", nullable=true)
     * @Groups({"without_rel"})
     */
    private $fileSize;


    /**
     * @ORM\Column(name="status", type="string", length=50)
     * @Groups({"without_rel", "with_rel"})
     */
    private $status;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $created_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $created_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="printed_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $printed_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="printed_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $printed_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="sent_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $sent_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="sent_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $sent_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="received_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $received_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="received_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $received_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="approved_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $approved_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="approved_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $approved_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="rejected_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rejected_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="rejected_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rejected_by;


    /**
     * @ORM\Column(name="rejected_reason", type="text", length=80, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rejected_reason;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="paid_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $paid_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="paid_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $paid_by;

    /**
     * @var ItemPencairan[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Pencairan\ItemPencairan", mappedBy="pencairan")
     * @ORM\OrderBy({"id" = "ASC"})
     * @Groups({"with_rel"})
     */
    private $children;

    /**
     * @ORM\Column(name="total_budget", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $totalBudget = 0.0;

    /**
     * @ORM\Column(name="total_pencairan", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $totalPencairan = 0.0;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="transfer_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $transfer_date;

    /**
     * @ORM\Column(name="nomor_voucher", type="string", nullable=true, length=80)
     * @Groups({"without_rel", "with_rel"})
     */
    private $nomorVoucherBdvc;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Mitra
     */
    public function getMitra(): Mitra
    {
        return $this->mitra;
    }

    /**
     * @param Mitra $mitra
     */
    public function setMitra(Mitra $mitra): void
    {
        $this->mitra = $mitra;
    }

    /**
     * @return mixed
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * @param mixed $tahun
     */
    public function setTahun($tahun): void
    {
        $this->tahun = $tahun;
    }

    /**
     * @return mixed
     */
    public function getUrutan()
    {
        return $this->urutan;
    }

    /**
     * @param mixed $urutan
     */
    public function setUrutan($urutan): void
    {
        $this->urutan = $urutan;
    }

    /**
     * @return mixed
     */
    public function getNomorSurat()
    {
        return $this->nomorSurat;
    }

    /**
     * @param mixed $nomorSurat
     */
    public function setNomorSurat($nomorSurat): void
    {
        $this->nomorSurat = $nomorSurat;
    }

    /**
     * @return mixed
     */
    public function getTipeTermin()
    {
        return $this->tipe_termin;
    }

    /**
     * @param mixed $tipe_termin
     */
    public function setTipeTermin($tipe_termin): void
    {
        $this->tipe_termin = $tipe_termin;
    }

    /**
     * @return int|null
     */
    public function getPersen()
    {
        return $this->persen;
    }

    /**
     * @param int|null $persen
     */
    public function setPersen(?int $persen): void
    {
        $this->persen = $persen;
    }

    /**
     * @return mixed
     */
    public function getIsExpense()
    {
        return $this->isExpense;
    }

    /**
     * @param mixed $isExpense
     */
    public function setIsExpense($isExpense): void
    {
        $this->isExpense = $isExpense;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedDate(): ?DateTimeInterface
    {
        return $this->created_date;
    }

    /**
     * @param DateTimeInterface $created_date
     */
    public function setCreatedDate(?DateTimeInterface $created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return UserAccount
     */
    public function getCreatedBy(): ?UserAccount
    {
        return $this->created_by;
    }

    /**
     * @param UserAccount $created_by
     */
    public function setCreatedBy(?UserAccount $created_by): void
    {
        $this->created_by = $created_by;
    }

    /**
     * @return DateTimeInterface
     */
    public function getPrintedDate(): ?DateTimeInterface
    {
        return $this->printed_date;
    }

    /**
     * @param DateTimeInterface $printed_date
     */
    public function setPrintedDate(?DateTimeInterface $printed_date): void
    {
        $this->printed_date = $printed_date;
    }

    /**
     * @return UserAccount
     */
    public function getPrintedBy(): ?UserAccount
    {
        return $this->printed_by;
    }

    /**
     * @param UserAccount $printed_by
     */
    public function setPrintedBy(?UserAccount $printed_by): void
    {
        $this->printed_by = $printed_by;
    }

    /**
     * @return DateTimeInterface
     */
    public function getSentDate(): ?DateTimeInterface
    {
        return $this->sent_date;
    }

    /**
     * @param DateTimeInterface $sent_date
     */
    public function setSentDate(?DateTimeInterface $sent_date): void
    {
        $this->sent_date = $sent_date;
    }

    /**
     * @return UserAccount
     */
    public function getSentBy(): ?UserAccount
    {
        return $this->sent_by;
    }

    /**
     * @param UserAccount $sent_by
     */
    public function setSentBy(?UserAccount $sent_by): void
    {
        $this->sent_by = $sent_by;
    }

    /**
     * @return DateTimeInterface
     */
    public function getReceivedDate(): ?DateTimeInterface
    {
        return $this->received_date;
    }

    /**
     * @param DateTimeInterface $received_date
     */
    public function setReceivedDate(?DateTimeInterface $received_date): void
    {
        $this->received_date = $received_date;
    }

    /**
     * @return UserAccount
     */
    public function getReceivedBy(): ?UserAccount
    {
        return $this->received_by;
    }

    /**
     * @param UserAccount $received_by
     */
    public function setReceivedBy(?UserAccount $received_by): void
    {
        $this->received_by = $received_by;
    }

    /**
     * @return DateTimeInterface
     */
    public function getApprovedDate(): ?DateTimeInterface
    {
        return $this->approved_date;
    }

    /**
     * @param DateTimeInterface $approved_date
     */
    public function setApprovedDate(?DateTimeInterface $approved_date): void
    {
        $this->approved_date = $approved_date;
    }

    /**
     * @return UserAccount
     */
    public function getApprovedBy(): ?UserAccount
    {
        return $this->approved_by;
    }

    /**
     * @param UserAccount $approved_by
     */
    public function setApprovedBy(?UserAccount $approved_by): void
    {
        $this->approved_by = $approved_by;
    }

    /**
     * @return DateTimeInterface
     */
    public function getRejectedDate(): ?DateTimeInterface
    {
        return $this->rejected_date;
    }

    /**
     * @param DateTimeInterface $rejected_date
     */
    public function setRejectedDate(?DateTimeInterface $rejected_date): void
    {
        $this->rejected_date = $rejected_date;
    }

    /**
     * @return UserAccount
     */
    public function getRejectedBy(): ?UserAccount
    {
        return $this->rejected_by;
    }

    /**
     * @param UserAccount $rejected_by
     */
    public function setRejectedBy(?UserAccount $rejected_by): void
    {
        $this->rejected_by = $rejected_by;
    }

    /**
     * @return mixed
     */
    public function getRejectedReason()
    {
        return $this->rejected_reason;
    }

    /**
     * @param mixed $rejected_reason
     */
    public function setRejectedReason($rejected_reason): void
    {
        $this->rejected_reason = $rejected_reason;
    }

    /**
     * @return DateTimeInterface
     */
    public function getPaidDate(): ?DateTimeInterface
    {
        return $this->paid_date;
    }

    /**
     * @param DateTimeInterface $paid_date
     */
    public function setPaidDate(?DateTimeInterface $paid_date): void
    {
        $this->paid_date = $paid_date;
    }

    /**
     * @return UserAccount
     */
    public function getPaidBy(): ?UserAccount
    {
        return $this->paid_by;
    }

    /**
     * @param UserAccount $paid_by
     */
    public function setPaidBy(?UserAccount $paid_by): void
    {
        $this->paid_by = $paid_by;
    }

    /**
     * @return ItemPencairan[]|Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ItemPencairan[]|Collection $children
     */
    public function setChildren($children): void
    {
        $this->children = $children;
    }

    public function addItemPencairan(ItemPencairan $itemPencairan): void
    {
        $this->children->add($itemPencairan);
    }

    /**
     * @return float|null
     */
    public function getTotalBudget(): ?float
    {
        return $this->totalBudget;
    }

    /**
     * @param float|null $totalBudget
     */
    public function setTotalBudget(?float $totalBudget): void
    {
        $this->totalBudget = $totalBudget;
    }

    /**
     * @return float|null
     */
    public function getTotalPencairan(): ?float
    {
        return $this->totalPencairan;
    }

    /**
     * @param float|null $totalPencairan
     */
    public function setTotalPencairan(?float $totalPencairan): void
    {
        $this->totalPencairan = $totalPencairan;
    }

    /**
     * @return null
     */
    public function getUrlFileTagihan()
    {
        return $this->urlFileTagihan;
    }

    /**
     * @param null $urlFileTagihan
     */
    public function setUrlFileTagihan($urlFileTagihan): void
    {
        $this->urlFileTagihan = $urlFileTagihan;
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param mixed $filePath
     */
    public function setFilePath($filePath): void
    {
        $this->filePath = $filePath;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getFileExt()
    {
        return $this->fileExt;
    }

    /**
     * @param mixed $fileExt
     */
    public function setFileExt($fileExt): void
    {
        $this->fileExt = $fileExt;
    }

    /**
     * @return mixed
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @param mixed $fileSize
     */
    public function setFileSize($fileSize): void
    {
        $this->fileSize = $fileSize;
    }

    /**
     * @return string|null
     */
    public function getFileUrl(): ?string
    {
        return $this->getFilePath() . '/' . $this->getFileName();
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getTransferDate(): ?DateTimeInterface
    {
        return $this->transfer_date;
    }

    /**
     * @param DateTimeInterface|null $transfer_date
     */
    public function setTransferDate(?DateTimeInterface $transfer_date): void
    {
        $this->transfer_date = $transfer_date;
    }

    /**
     * @return string|null
     */
    public function getNomorVoucherBdvc(): ?string
    {
        return $this->nomorVoucherBdvc;
    }

    /**
     * @param string|null $nomorVoucherBdvc
     */
    public function setNomorVoucherBdvc(?string $nomorVoucherBdvc): void
    {
        $this->nomorVoucherBdvc = $nomorVoucherBdvc;
    }

    /**
     * @return mixed
     */
    public function getIsPelunasan()
    {
        return $this->isPelunasan;
    }

    /**
     * @param mixed $isPelunasan
     */
    public function setIsPelunasan($isPelunasan): void
    {
        $this->isPelunasan = $isPelunasan;
    }


}