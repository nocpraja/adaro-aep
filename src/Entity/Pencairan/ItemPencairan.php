<?php

/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Pencairan;

use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\Subkegiatan;
use App\Entity\Kegiatan\ItemRealisasiRab;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="pencairan_items", indexes={
 *     @ORM\Index(name="itempencairan_x1", columns={"pencairan_id"}),
 *     @ORM\Index(name="itempencairan_x2", columns={"kegiatan_id"}),
 *     @ORM\Index(name="itempencairan_x3", columns={"subkegiatan_id"}),
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Pencairan\ItemPencairanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pencairan\ItemPencairan")
 *
 * @package App\Entity\Pencairan
 * @author  Mark Melvin
 * @since   31/05/2020, modified: 31/05/2020 02:23
 */
class ItemPencairan
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="itempencairan_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @var Pencairan
     * @ORM\ManyToOne(targetEntity="App\Entity\Pencairan\Pencairan", inversedBy="children")
     * @ORM\JoinColumn(name="pencairan_id", referencedColumnName="pencairan_id", nullable=false)
     * @Groups({"with_parent"})
     */
    private $pencairan;

    /**
     * @var Kegiatan|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Kegiatan\Kegiatan")
     * @ORM\JoinColumn(name="kegiatan_id", referencedColumnName="kegiatan_id")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kegiatan;

    /**
     * @var SubKegiatan|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Kegiatan\Subkegiatan")
     * @ORM\JoinColumn(name="subkegiatan_id", referencedColumnName="sub_id")
     * @Groups({"without_rel", "with_rel"})
     */
    private $subkegiatan;

    /**
     * @ORM\Column(name="title", type="string", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $title = '';

    /**
     * @ORM\Column(name="budget", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $budget = 0.0;

    /**
     * @ORM\Column(name="amount", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $amount = 0.0;

    /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Kegiatan\ItemRealisasiRab")
     * @ORM\JoinTable(name="pencairan_realisasi",
     *      joinColumns={@ORM\JoinColumn(name="itempencairan_id", referencedColumnName="itempencairan_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="realisasi_id", referencedColumnName="id_realisasi")}
     *      )
     */
    private $itemRealisasis;

    /**
     * Trick untuk calculated field array menghasilkan semua id realisasi
     * @var array|int[]
     * @Groups({"without_rel", "with_rel"})
     */
    private $ids;

    /**
     * Trick untuk calculated field array menghasilkan id kegiatan
     * @var int
     * @Groups({"without_rel", "with_rel"})
     */
    private $kegiatanId;

    /**
     * Trick untuk calculated field array menghasilkan id subkegiatan
     * @var int
     * @Groups({"without_rel", "with_rel"})
     */
    private $subkegiatanId;

    public function __construct() {
        $this->itemRealisasis = new ArrayCollection();
    }


    /**
     * @return Collection
     */
    public function getItemRealisasis(): Collection
    {
        return $this->itemRealisasis;
    }

    /**
     * @param Collection $itemRealisasis
     */
    public function setItemRealisasis(Collection $itemRealisasis): void
    {
        $this->itemRealisasis = $itemRealisasis;
    }

    public function addItemRealisasi(ItemRealisasiRab $itemRealisasiRab): void
    {
        $this->itemRealisasis->add($itemRealisasiRab);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Pencairan
     */
    public function getPencairan(): Pencairan
    {
        return $this->pencairan;
    }

    /**
     * @param Pencairan $pencairan
     */
    public function setPencairan(Pencairan $pencairan): void
    {
        $this->pencairan = $pencairan;
    }

    /**
     * @return Kegiatan|null
     */
    public function getKegiatan(): ?Kegiatan
    {
        return $this->kegiatan;
    }

    /**
     * @param Kegiatan|null $kegiatan
     */
    public function setKegiatan(?Kegiatan $kegiatan): void
    {
        $this->kegiatan = $kegiatan;
    }

    /**
     * @return Subkegiatan|null
     */
    public function getSubkegiatan(): ?Subkegiatan
    {
        return $this->subkegiatan;
    }

    /**
     * @param Subkegiatan|null $subkegiatan
     */
    public function setSubkegiatan(?Subkegiatan $subkegiatan): void
    {
        $this->subkegiatan = $subkegiatan;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return float
     */
    public function getBudget(): float
    {
        return $this->budget;
    }

    /**
     * @param float $budget
     */
    public function setBudget(float $budget): void
    {
        $this->budget = $budget;
    }

    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float|null $amount
     */
    public function setAmount(?float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return array|int[]
     */
    public function getIds()
    {
        $ids_array = [];
        /** @var ItemRealisasiRab $item */
        foreach ($this->itemRealisasis as $item) {
            $ids_array[] = $item->getId();
        }
        return $ids_array;
    }

    /**
     * @return int|null
     */
    public function getKegiatanId(): ?int
    {
        if ($this->kegiatan !== null) {
            return $this->kegiatan->getKegiatanId();
        }
        return null;
    }

    /**
     * @return int|null
     */
    public function getSubkegiatanId(): ?int
    {
        if ($this->subkegiatan !== null) {
            return $this->subkegiatan->getSid();
        }
        return null;
    }



}