<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Kecamatan
 *
 * @ORM\Table(name="kecamatan",
 *     indexes={@ORM\Index(name="kecamatan_x1", columns={"kab_id"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="ak_x1_kecamatan", columns={"kode_kecamatan"})}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\KecamatanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kecamatan")
 * @UniqueEntity(fields={"kodeKecamatan"}, message="Kecamatan.Duplicates")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   17/08/2018, modified: 25/05/2019 2:53
 */
class Kecamatan
{

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="kec_id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kecamatanId;

    /**
     * @var string
     *
     * @ORM\Column(name="kode_kecamatan", type="string", length=15,
     *     options={"comment": "Lihat kode kecamatan dari Depdagri"})
     * @Assert\NotBlank(message="kode_kecamatan.NotBlank")
     * @Assert\Length(min="8", max="15")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kodeKecamatan;

    /**
     * @var string
     *
     * @ORM\Column(name="nama_kecamatan", type="string", length=250)
     * @Assert\NotBlank(message="nama_kecamatan.NotBlank")
     * @Assert\Length(min="4")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaKecamatan;

    /**
     * @var Kabupaten
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kabupaten")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Kabupaten", inversedBy="kecamatans")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kab_id", referencedColumnName="kab_id", nullable=false)
     * })
     * @Groups({"with_rel"})
     */
    private $kabupaten;

    /**
     * @var Kelurahan[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kelurahan")
     * @ORM\OneToMany(targetEntity="App\Entity\MasterData\Kelurahan", mappedBy="kecamatan",
     *                cascade={"persist", "merge"}, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"namaKelurahan" = "ASC"})
     */
    private $kelurahans;


    /**
     * Kecamatan constructor.
     */
    public function __construct()
    {
        $this->kelurahans = new ArrayCollection();
    }

    /**
     * Add a Kelurahan to the collection.
     *
     * @param Kelurahan $kelurahan
     */
    public function addKelurahan(Kelurahan $kelurahan): void
    {
        $kelurahan->setKecamatan($this);
        $kelurahans = $this->getKelurahans();

        if ($kelurahans instanceof Collection) {
            $kelurahans->add($kelurahan);
        } elseif (empty($kelurahans)) {
            $this->kelurahans = new ArrayCollection();
            $this->kelurahans->add($kelurahan);
        } else {
            $this->kelurahans[] = $kelurahan;
        }
    }

    /**
     * Gets parent wilayah.
     *
     * @return Kabupaten
     */
    public function getKabupaten(): ?Kabupaten
    {
        return $this->kabupaten;
    }

    /**
     * Sets parent wilayah.
     *
     * @param Kabupaten $kabupaten
     * @return Kecamatan Fluent object, returns itself
     */
    public function setKabupaten(Kabupaten $kabupaten): self
    {
        $this->kabupaten = $kabupaten;

        return $this;
    }

    /**
     * Gets Kecamatan ID.
     *
     * @return int|null
     */
    public function getKecamatanId(): ?int
    {
        return $this->kecamatanId;
    }

    /**
     * Gets collection of Kelurahan.
     *
     * @return Kelurahan[]|Collection
     */
    public function getKelurahans()
    {
        return $this->kelurahans;
    }

    /**
     * Gets kode kecamatan.
     *
     * @return null|string
     */
    public function getKodeKecamatan(): ?string
    {
        return $this->kodeKecamatan;
    }

    /**
     * Sets kode kecamatan. Lihat kode kecamatan dari Depdagri.
     *
     * @param string $kodeKecamatan
     * @return Kecamatan Fluent object, returns itself
     */
    public function setKodeKecamatan(string $kodeKecamatan): self
    {
        $this->kodeKecamatan = $kodeKecamatan;

        return $this;
    }

    /**
     * Gets nama kecamatan.
     *
     * @return null|string
     */
    public function getNamaKecamatan(): ?string
    {
        return $this->namaKecamatan;
    }

    /**
     * Sets nama kecamatan.
     *
     * @param string $namaKecamatan
     * @return Kecamatan Fluent object, returns itself
     */
    public function setNamaKecamatan(string $namaKecamatan): self
    {
        $this->namaKecamatan = $namaKecamatan;

        return $this;
    }

    /**
     * Removes a Kelurahan from collection.
     *
     * @param Kelurahan $kelurahan
     * @return bool
     */
    public function removeKelurahan(Kelurahan $kelurahan): bool
    {
        return $this->kelurahans->removeElement($kelurahan);
    }
}
