<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Kabupaten
 *
 * @ORM\Table(name="kabupaten",
 *     indexes={@ORM\Index(name="kabupaten_x1", columns={"prov_id"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="ak_x1_kabupaten", columns={"kode_kabupaten"})}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\KabupatenRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kabupaten")
 * @UniqueEntity(fields={"kodeKabupaten"}, message="Kabupaten.Duplicates.")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   17/08/2018, modified: 25/05/2019 2:53
 */
class Kabupaten
{

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="kab_id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kabupatenId;

    /**
     * @var string
     *
     * @ORM\Column(name="kode_kabupaten", type="string", length=10,
     *     options={"comment": "Lihat kode kabupaten dari Depdagri"})
     * @Assert\NotBlank(message="kode_kabupaten.NotBlank")
     * @Assert\Length(min="5", max="5")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kodeKabupaten;

    /**
     * @var string
     *
     * @ORM\Column(name="nama_kabupaten", type="string", length=250)
     * @Assert\NotBlank(message="nama_kabupaten.NotBlank")
     * @Assert\Length(min="4")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaKabupaten;

    /**
     * @var Provinsi
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Provinsi")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Provinsi", inversedBy="kabupatens")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prov_id", referencedColumnName="prov_id", nullable=false)
     * })
     * @Groups({"with_rel"})
     */
    private $provinsi;

    /**
     * @var Kecamatan[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kecamatan")
     * @ORM\OneToMany(targetEntity="App\Entity\MasterData\Kecamatan", mappedBy="kabupaten",
     *                cascade={"persist", "merge"}, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"namaKecamatan" = "ASC"})
     */
    private $kecamatans;


    /**
     * Kabupaten constructor.
     */
    public function __construct()
    {
        $this->kecamatans = new ArrayCollection();
    }

    /**
     * Add a kecamatan to the collection.
     *
     * @param Kecamatan $kecamatan
     */
    public function addKecamatan(Kecamatan $kecamatan): void
    {
        $kecamatan->setKabupaten($this);
        $kecamatans = $this->getKecamatans();

        if ($kecamatans instanceof Collection) {
            $kecamatans->add($kecamatan);
        } elseif (empty($kecamatans)) {
            $this->kecamatans = new ArrayCollection();
            $this->kecamatans->add($kecamatan);
        } else {
            $this->kecamatans[] = $kecamatan;
        }
    }

    /**
     * Gets kabupaten ID.
     *
     * @return int|null
     */
    public function getKabupatenId(): ?int
    {
        return $this->kabupatenId;
    }

    /**
     * Gets the collection of Kecamatan.
     *
     * @return Kecamatan[]|Collection
     */
    public function getKecamatans()
    {
        return $this->kecamatans;
    }

    /**
     * Gets kode kabupaten.
     *
     * @return null|string
     */
    public function getKodeKabupaten(): ?string
    {
        return $this->kodeKabupaten;
    }

    /**
     * Sets kode kabupaten. Lihat kode kabupaten dari Depdagri.
     *
     * @param string $kodeKabupaten
     * @return Kabupaten Fluent object, returns itself
     */
    public function setKodeKabupaten(string $kodeKabupaten): self
    {
        $this->kodeKabupaten = $kodeKabupaten;

        return $this;
    }

    /**
     * Gets nama kabupaten.
     *
     * @return null|string
     */
    public function getNamaKabupaten(): ?string
    {
        return $this->namaKabupaten;
    }

    /**
     * Sets nama kabupaten.
     *
     * @param string $namaKabupaten
     * @return Kabupaten Fluent object, returns itself
     */
    public function setNamaKabupaten(string $namaKabupaten): self
    {
        $this->namaKabupaten = $namaKabupaten;

        return $this;
    }

    /**
     * Gets parent wilayah.
     *
     * @return Provinsi
     */
    public function getProvinsi(): ?Provinsi
    {
        return $this->provinsi;
    }

    /**
     * Sets parent wilayah.
     *
     * @param Provinsi $provinsi
     * @return Kabupaten Fluent object, returns itself
     */
    public function setProvinsi(?Provinsi $provinsi): self
    {
        $this->provinsi = $provinsi;

        return $this;
    }

    /**
     * Removes a kecamatan from collection.
     *
     * @param Kecamatan $kecamatan
     * @return bool
     */
    public function removeKecamatan(Kecamatan $kecamatan): bool
    {
        return $this->kecamatans->removeElement($kecamatan);
    }

}
