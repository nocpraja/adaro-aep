<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Provinsi
 *
 * @ORM\Table(name="provinsi",
 *     indexes={@ORM\Index(name="provinsi_x1", columns={"nama_provinsi"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="ak_x1_provinsi", columns={"kode_provinsi"})}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\ProvinsiRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Provinsi")
 * @UniqueEntity(fields={"kodeProvinsi"}, message="Province.Duplicates")
 * @UniqueEntity(fields={"namaProvinsi"}, message="Province.Duplicates")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   17/08/2018, modified: 20/08/2018 04:18
 */
class Provinsi
{

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="prov_id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $provinsiId;

    /**
     *
     * @ORM\Column(name="kode_provinsi", type="string", length=4,
     *     options={"comment": "Lihat kode provinsi dari Depdagri"})
     * @Assert\NotBlank(message="kode_provinsi.NotBlank")
     * @Assert\Length(min="2", max="4")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kodeProvinsi;

    /**
     * @ORM\Column(name="nama_provinsi", type="string", length=250)
     * @Assert\NotBlank(message="nama_provinsi.NotBlank")
     * @Assert\Length(min="4")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaProvinsi;

    /**
     * @var Kabupaten[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kabupaten")
     * @ORM\OneToMany(targetEntity="App\Entity\MasterData\Kabupaten", mappedBy="provinsi",
     *                cascade={"persist", "merge"}, fetch="LAZY")
     * @ORM\OrderBy({"namaKabupaten" = "ASC"})
     */
    private $kabupatens;


    /**
     * Provinsi constructor.
     */
    public function __construct()
    {
        $this->kabupatens = new ArrayCollection();
    }

    /**
     * Add a kabupaten to the collection.
     *
     * @param Kabupaten $kabupaten
     */
    public function addKabupaten(Kabupaten $kabupaten): void
    {
        $kabupaten->setProvinsi($this);
        $kabupatens = $this->getKabupatens();

        if ($kabupatens instanceof Collection) {
            $kabupatens->add($kabupaten);
        } elseif (empty($kabupatens)) {
            $this->kabupatens = new ArrayCollection();
            $this->kabupatens->add($kabupaten);
        } else {
            $this->kabupatens[] = $kabupaten;
        }
    }

    /**
     * Gets the collection of kabupaten.
     *
     * @return Kabupaten[]|Collection
     */
    public function getKabupatens(): Collection
    {
        return $this->kabupatens;
    }

    /**
     * Gets kode provinsi.
     *
     * @return null|string
     */
    public function getKodeProvinsi(): ?string
    {
        return $this->kodeProvinsi;
    }

    /**
     * Sets kode provinsi. Lihat kode provinsi dari Depdagri.
     *
     * @param string $kodeProvinsi
     * @return Provinsi Fluent object, returns itself
     */
    public function setKodeProvinsi(string $kodeProvinsi): self
    {
        $this->kodeProvinsi = $kodeProvinsi;

        return $this;
    }

    /**
     * Gets nama provinsi.
     *
     * @return null|string
     */
    public function getNamaProvinsi(): ?string
    {
        return $this->namaProvinsi;
    }

    /**
     * Sets nama provinsi.
     *
     * @param string $namaProvinsi
     * @return Provinsi Fluent object, returns itself
     */
    public function setNamaProvinsi(string $namaProvinsi): self
    {
        $this->namaProvinsi = $namaProvinsi;

        return $this;
    }

    /**
     * Gets provinsi ID.
     *
     * @return int|null
     */
    public function getProvinsiId(): ?int
    {
        return $this->provinsiId;
    }

    /**
     * Remove a kabupaten from the collection.
     *
     * @param Kabupaten $kabupaten
     * @return boolean
     */
    public function removeKabupaten(Kabupaten $kabupaten)
    {
        return $this->kabupatens->removeElement($kabupaten);
    }

}
