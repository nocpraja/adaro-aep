<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="kelurahan", indexes={
 *     @ORM\Index(name="kelurahan_x1", columns={"kec_id"}),
 *     @ORM\Index(name="kelurahan_x2", columns={"kode_kelurahan"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\KelurahanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kelurahan")
 * @UniqueEntity(fields={"kodeKelurahan"}, message="Kelurahan.Duplicates")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   17/08/2018, modified: 25/05/2019 2:53
 */
class Kelurahan
{

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="kel_id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kelurahanId;

    /**
     * @var string
     *
     * @ORM\Column(name="kode_kelurahan", type="string", length=25, nullable=true,
     *     options={"comment": "Lihat kode kelurahan dari Depdagri"})
     * @Assert\Length(min="13", max="15")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kodeKelurahan;

    /**
     * @var string
     *
     * @ORM\Column(name="nama_kelurahan", type="string", length=250)
     * @Assert\NotBlank(message="nama_kelurahan.NotBlank")
     * @Assert\Length(min="4")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaKelurahan;

    /**
     * @var Kecamatan
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Kecamatan")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Kecamatan", inversedBy="kelurahans")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kec_id", referencedColumnName="kec_id", nullable=false)
     * })
     * @Groups({"with_rel"})
     */
    private $kecamatan;

    /**
     * Gets parent wilayah.
     *
     * @return Kecamatan
     */
    public function getKecamatan(): ?Kecamatan
    {
        return $this->kecamatan;
    }

    /**
     * Sets parent wilayah.
     *
     * @param Kecamatan $kecamatan
     * @return Kelurahan Fluent object, returns itself
     */
    public function setKecamatan(Kecamatan $kecamatan): self
    {
        $this->kecamatan = $kecamatan;

        return $this;
    }

    /**
     * Gets kelurahan ID.
     *
     * @return int|null
     */
    public function getKelurahanId(): ?int
    {
        return $this->kelurahanId;
    }

    /**
     * Gets kode kelurahan.
     *
     * @return null|string
     */
    public function getKodeKelurahan(): ?string
    {
        return $this->kodeKelurahan;
    }

    /**
     * Sets kode kelurahan. Lihat kode kelurahan dari Depdagri.
     *
     * @param null|string $kodeKelurahan
     * @return Kelurahan Fluent object, returns itself
     */
    public function setKodeKelurahan(?string $kodeKelurahan): self
    {
        $this->kodeKelurahan = $kodeKelurahan;

        return $this;
    }

    /**
     * Gets nama kelurahan.
     *
     * @return null|string
     */
    public function getNamaKelurahan(): ?string
    {
        return $this->namaKelurahan;
    }

    /**
     * Sets nama kelurahan.
     *
     * @param string $namaKelurahan
     * @return Kelurahan Fluent object, returns itself
     */
    public function setNamaKelurahan(string $namaKelurahan): self
    {
        $this->namaKelurahan = $namaKelurahan;

        return $this;
    }

}
