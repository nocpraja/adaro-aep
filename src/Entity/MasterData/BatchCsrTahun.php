<?php

namespace App\Entity\MasterData;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Entity master data Batch Pembinaan.
 *
 * @ORM\Table(name="ms_batch_csr_tahun", indexes={
 *     @ORM\Index(name="ms_batch_tahun_x1", columns={"batch_id"}),
 * }, options={"comment": "Master data Batch untuk masing-masing Program CSR"})
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\BatchCsrTahunRepository")
 */
class BatchCsrTahun
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\BatchCsr")
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="batch_id", nullable=false)
     */
    private $batchCsr;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahun;

    /**
     * @Groups({"without_rel", "with_rel"})
     * @ORM\Column(name="narasi_kpi", type="string", length=500, nullable=true)
     */
    private $narasiKpi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBatchCsr(): ?BatchCsr
    {
        return $this->batchCsr;
    }

    public function setBatchCsr(?BatchCsr $batchCsr): self
    {
        $this->batchCsr = $batchCsr;

        return $this;
    }

    public function getTahun(): ?int
    {
        return $this->tahun;
    }

    public function setTahun(int $tahun): self
    {
        $this->tahun = $tahun;

        return $this;
    }

    public function getNarasiKpi(): ?string
    {
        return $this->narasiKpi;
    }

    public function setNarasiKpi(?string $narasiKpi): self
    {
        $this->narasiKpi = $narasiKpi;

        return $this;
    }
}
