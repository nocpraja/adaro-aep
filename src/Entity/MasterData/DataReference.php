<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;


use App\Entity\LastUpdateTrait;
use App\Entity\PostedByTrait;
use App\Entity\PostedDateTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ms_dataref",
 *     indexes={@ORM\Index(name="ms_dataref_x1", columns={"parent_id"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="ak_x1_ms_dataref", columns={"ref_code"})}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\DataReferenceRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
 * @UniqueEntity(fields={"code"}, message="DataReference.Duplicates")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   20/08/2018, modified: 27/01/2019 15:03
 */
class DataReference
{
    use PostedByTrait, PostedDateTrait, LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="protected", type="boolean", options={"default": false})
     * @Groups({"without_rel", "with_rel"})
     */
    private $protected = false;

    /**
     * @ORM\Column(name="ref_code", type="string", length=25)
     * @Assert\NotBlank(message="RefCode.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $code;

    /**
     * @ORM\Column(name="ref_name", type="string", length=200)
     * @Assert\NotBlank(message="RefName.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $name;

    /**
     * @ORM\Column(name="ref_value", type="json", nullable=true, options={"jsonb": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $params = [];

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $description;

    /**
     * @var DataReference
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\DataReference", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     * })
     * @Groups({"with_parent"})
     */
    private $parent;

    /**
     * @var DataReference[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
     * @ORM\OneToMany(targetEntity="App\Entity\MasterData\DataReference", mappedBy="parent", cascade={"persist"})
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $children;


    /**
     * @return DataReference[]|Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Gets the reference code.
     *
     * @return null|string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * Sets the reference code.
     *
     * @param string $code
     *
     * @return DataReference Fluent object, returns itself
     */
    public function setCode(string $code): self
    {
        $_code = str_replace([' ', '_'], '-', $code);
        $this->code = strtoupper($_code);

        return $this;
    }

    /**
     * Gets the reference's description.
     *
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Sets the reference's description.
     *
     * @param null|string $description
     *
     * @return DataReference Fluent object, returns itself
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets the entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the reference name.
     *
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Sets the reference name.
     *
     * @param string $name
     *
     * @return DataReference Fluent object, returns itself
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the optional parameter values.
     *
     * @return array|null Array in key-value format
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * Sets the optional parameter values.
     *
     * @param array|null $params Array in key-value format
     *
     * @return DataReference Fluent object, returns itself
     */
    public function setParams(?array $params): self
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Gets parent entity.
     *
     * @return DataReference
     */
    public function getParent(): ?DataReference
    {
        return $this->parent;
    }

    /**
     * Sets parent entity.
     *
     * @param DataReference $parent
     *
     * @return DataReference Fluent object, returns itself
     */
    public function setParent(?DataReference $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Is entity protected from accidental deletion?
     *
     * @return bool|null
     */
    public function getProtected(): ?bool
    {
        return $this->protected;
    }

    /**
     * Set protection from accidental deletion.
     *
     * @param bool $protected If <var>true</var> than it will be protected otherwise <var>false</var>
     *
     * @return DataReference Fluent object, returns itself
     */
    public function setProtected(bool $protected): self
    {
        $this->protected = $protected;

        return $this;
    }

}
