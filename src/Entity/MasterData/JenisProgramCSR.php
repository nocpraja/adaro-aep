<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="jenis_program_csr")
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\JenisProgramCSRRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\JenisProgramCSR")
 * @UniqueEntity(fields={"title"}, message="JenisProgramCSR.Duplicates")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   20/08/2018, modified: 28/09/2018 13:41
 */
class JenisProgramCSR
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer")
     * @Groups({"without_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=150)
     * @Assert\NotBlank(message="jenisCSR.NotBlank")
     * @Groups({"without_rel"})
     */
    private $title;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"without_rel"})
     */
    private $description;

    /**
     * @var ProgramCSR[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\ProgramCSR")
     * @ORM\OneToMany(targetEntity="App\Entity\MasterData\ProgramCSR", mappedBy="jenis",
     *                cascade={"persist", "merge"}, fetch="LAZY")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $programs;


    /**
     * JenisProgramCSR constructor.
     */
    public function __construct()
    {
        $this->programs = new ArrayCollection();
    }

    /**
     * @param ProgramCSR $program
     */
    public function addProgram($program): void
    {
        $program->setJenis($this);
        $programs = $this->getPrograms();

        if ($programs instanceof Collection) {
            $programs->add($program);
        } elseif (empty($programs)) {
            $this->programs = new ArrayCollection();
            $this->programs->add($program);
        } else {
            $this->programs[] = $program;
        }
    }

    /**
     * Gets description of jenis program CSR.
     *
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Sets the description of jenis program CSR.
     *
     * @param null|string $description
     * @return JenisProgramCSR Fluent object, returns itself
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets the entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|ProgramCSR[]
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * Gets title jenis program CSR.
     *
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Sets title jenis program CSR.
     *
     * @param string $title
     * @return JenisProgramCSR Fluent object, returns itself
     */
    public function setTitle(string $title): self
    {
        $this->title = strtoupper($title);

        return $this;
    }

    /**
     * Remove a program from the collection.
     *
     * @param ProgramCSR $program
     * @return boolean
     */
    public function removeProgram(ProgramCSR $program): bool
    {
        return $this->programs->removeElement($program);
    }
}
