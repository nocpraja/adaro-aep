<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;


use App\Entity\AlamatTrait;
use App\Entity\EmailTrait;
use App\Entity\LastUpdateByFkTrait;
use App\Entity\LastUpdateTrait;
use App\Entity\LatLonTrait;
use App\Entity\OfficePhoneTrait;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Entity\SearchFtsTrait;
use App\Entity\WebsiteTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ms_mitra", indexes={
 *     @ORM\Index(name="ms_mitra_x1", columns={"kec_id"}),
 *     @ORM\Index(name="ms_mitra_x2", columns={"kel_id"}),
 *     @ORM\Index(name="ms_mitra_x3", columns={"msid"}),
 *     @ORM\Index(name="ms_mitra_x4", columns={"search_fts"}, options={"USING": "gin"}),
 *     @ORM\Index(name="ms_mitra_x5", columns={"posted_by"}),
 *     @ORM\Index(name="ms_mitra_x6", columns={"updated_by"})
 * }, options={"comment": "Data profil Mitra kerjasama"})
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\MitraRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Mitra")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   31/10/2018, modified: 25/05/2019 3:10
 */
class Mitra
{

    use AlamatTrait, OfficePhoneTrait, EmailTrait, WebsiteTrait;
    use PostedByFkTrait, PostedDateTrait, LastUpdateByFkTrait, LastUpdateTrait;
    use LatLonTrait, SearchFtsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="mitra_id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $mitraId;

    /**
     * @ORM\Column(name="jenis_lembaga", type="string", length=25,
     *     options={"comment": "Valid values: NEGERI, SWASTA, YAYASAN, PERKUMPULAN"})
     * @Assert\NotBlank(message="JenisLembaga.notBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jenisLembaga;

    /**
     * @ORM\Column(name="nama_lembaga", type="string", length=250)
     * @Assert\NotBlank(message="NamaLembaga.notBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaLembaga;

    /**
     * @ORM\Column(name="kode_lembaga", type="string", length=25)
     * @Assert\NotBlank(message="KodeLembaga.notBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $kodeLembaga;

    /**
     * @ORM\Column(name="akte_pendirian", type="string", length=50)
     * @Assert\NotBlank(message="AktePendirian.notBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $aktePendirian;

    /**
     * @ORM\Column(name="nama_pimpinan", type="string", length=250, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaPimpinan;

    /**
     * @ORM\Column(name="pic_program", type="string", length=250, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $picProgram;

    /**
     * @ORM\Column(name="pimpinan_phone", type="string", length=25, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $pimpinanPhone;

    /**
     * @ORM\Column(name="pic_phone", type="string", length=25, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $picPhone;

    /**
     * @var ProgramCSR
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\ProgramCSR")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\ProgramCSR")
     * @ORM\JoinColumn(name="msid", referencedColumnName="msid")
     * @Groups({"with_rel"})
     */
    private $programCsr;


    public function getAktePendirian(): ?string
    {
        return $this->aktePendirian;
    }

    public function setAktePendirian(string $aktePendirian): self
    {
        $this->aktePendirian = $aktePendirian;

        return $this;
    }

    /**
     * Get jenis lembaga.
     *
     * @return null|string Salah satu dari nilai berikut: NEGERI, SWASTA, YAYASAN, PERKUMPULAN
     */
    public function getJenisLembaga(): ?string
    {
        return $this->jenisLembaga;
    }

    /**
     * Set jenis lembaga.
     *
     * @param string $jenisLembaga Valid values: NEGERI, SWASTA, YAYASAN, PERKUMPULAN
     *
     * @return Mitra
     */
    public function setJenisLembaga(string $jenisLembaga): self
    {
        $this->jenisLembaga = $jenisLembaga;

        return $this;
    }

    /**
     * Get kode lembaga/mitra.
     *
     * @return string
     */
    public function getKodeLembaga(): ?string
    {
        return $this->kodeLembaga;
    }

    /**
     * Set kode lembaga/mitra.
     *
     * @param string $kodeLembaga
     *
     * @return Mitra
     */
    public function setKodeLembaga(string $kodeLembaga): self
    {
        $this->kodeLembaga = $kodeLembaga;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getMitraId(): ?int
    {
        return $this->mitraId;
    }

    public function getNamaLembaga(): ?string
    {
        return $this->namaLembaga;
    }

    public function setNamaLembaga(string $namaLembaga): self
    {
        $this->namaLembaga = $namaLembaga;

        return $this;
    }

    public function getNamaPimpinan(): ?string
    {
        return $this->namaPimpinan;
    }

    public function setNamaPimpinan(?string $namaPimpinan): self
    {
        $this->namaPimpinan = $namaPimpinan;

        return $this;
    }

    public function getPicPhone(): ?string
    {
        return $this->picPhone;
    }

    public function setPicPhone(?string $picPhone): self
    {
        $this->picPhone = $picPhone;

        return $this;
    }

    public function getPicProgram(): ?string
    {
        return $this->picProgram;
    }

    public function setPicProgram(?string $picProgram): self
    {
        $this->picProgram = $picProgram;

        return $this;
    }

    public function getPimpinanPhone(): ?string
    {
        return $this->pimpinanPhone;
    }

    public function setPimpinanPhone(?string $pimpinanPhone): self
    {
        $this->pimpinanPhone = $pimpinanPhone;

        return $this;
    }

    /**
     * @return ProgramCSR
     */
    public function getProgramCsr(): ?ProgramCSR
    {
        return $this->programCsr;
    }

    /**
     * @param ProgramCSR $programCsr
     *
     * @return Mitra
     */
    public function setProgramCsr(?ProgramCSR $programCsr): self
    {
        $this->programCsr = $programCsr;

        return $this;
    }

}
