<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;


use App\Entity\PostedDateTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity master data Batch Pembinaan.
 *
 * @ORM\Table(name="ms_batch_csr", indexes={
 *     @ORM\Index(name="ms_batch_x1", columns={"msid"}),
 *     @ORM\Index(name="ms_batch_x2", columns={"mitra_id"})
 * }, options={"comment": "Master data Batch untuk masing-masing Program CSR"})
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\BatchCsrRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\BatchCsr")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   31/10/2018, modified: 19/05/2019 21:36
 */
class BatchCsr
{

    use PostedDateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="batch_id", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $batchId;

    /**
     * @ORM\Column(name="nama_batch", type="string", length=100)
     * @Assert\NotBlank(message="namaBatchCSR.notBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaBatch;

    /**
     * @ORM\Column(name="tahun_awal", type="integer")
     * @Assert\NotNull(message="TahunPeriodeAwal.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahunAwal;

    /**
     * @ORM\Column(name="tahun_akhir", type="integer")
     * @Assert\NotNull(message="TahunPeriodeAkhir.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahunAkhir;

    /**
     * @var float
     *
     * @ORM\Column(name="mgt_fee", type="float", nullable=true, options={
     *     "default": 5, "comment": "Manajemen Fee dalam persen"
     * })
     * @Assert\NotNull(message="ManagementFee.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $managementFee;

    /**
     * @var ProgramCSR
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\ProgramCSR")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\ProgramCSR")
     * @ORM\JoinColumn(name="msid", referencedColumnName="msid")
     * @Groups({"without_rel", "with_rel"})
     */
    private $programCsr;

    /**
     * @var Mitra
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\Mitra")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Mitra")
     * @ORM\JoinColumn(name="mitra_id", referencedColumnName="mitra_id")
     * @Groups({"with_custom"})
     */
    private $mitra;

    /**
     * @var BatchCsrTahun[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\MasterData\BatchCsrTahun", mappedBy="batchCsr", orphanRemoval=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahuns;

    public function __construct()
    {
        $this->tahuns = new ArrayCollection();
    }


    /**
     * Get the entity ID.
     *
     * @return int|null
     */
    public function getBatchId(): ?int
    {
        return $this->batchId;
    }

    /**
     * Get konstanta persentase manajemen fee.
     *
     * @return float
     */
    public function getManagementFee(): ?float
    {
        return $this->managementFee;
    }

    /**
     * Set konstanta persentase manajemen fee.
     *
     * @param float $fee Nilai manajemen fee dalam persen
     *
     * @return BatchCsr
     */
    public function setManagementFee(float $fee): self
    {
        $this->managementFee = $fee;

        return $this;
    }

    /**
     * Get mitra kerjasama program CSR.
     *
     * @return Mitra
     */
    public function getMitra(): ?Mitra
    {
        return $this->mitra;
    }

    /**
     * Set mitra kerjasama program CSR.
     *
     * @param Mitra $mitra
     *
     * @return BatchCsr
     */
    public function setMitra(Mitra $mitra): self
    {
        $this->mitra = $mitra;

        return $this;
    }

    /**
     * Get nama Batch.
     *
     * @return null|string
     */
    public function getNamaBatch(): ?string
    {
        return $this->namaBatch;
    }

    /**
     * Set nama Batch.
     *
     * @param string $namaBatch
     *
     * @return BatchCsr
     */
    public function setNamaBatch(string $namaBatch): self
    {
        $this->namaBatch = $namaBatch;

        return $this;
    }

    /**
     * @return ProgramCSR
     */
    public function getProgramCsr(): ?ProgramCSR
    {
        return $this->programCsr;
    }

    /**
     * @param ProgramCSR $programCsr
     *
     * @return BatchCsr
     */
    public function setProgramCsr(ProgramCSR $programCsr): self
    {
        $this->programCsr = $programCsr;

        return $this;
    }

    /**
     * Get periode akhir Batch.
     *
     * @return int|null
     */
    public function getTahunAkhir(): ?int
    {
        return $this->tahunAkhir;
    }

    /**
     * Set periode akhir Batch.
     *
     * @param int $tahunAkhir Tahun berakhir
     *
     * @return BatchCsr
     */
    public function setTahunAkhir(int $tahunAkhir): self
    {
        $this->tahunAkhir = $tahunAkhir;

        return $this;
    }

    /**
     * Get periode awal Batch.
     *
     * @return int|null
     */
    public function getTahunAwal(): ?int
    {
        return $this->tahunAwal;
    }

    /**
     * Set periode awal Batch.
     *
     * @param int $tahunAwal Tahun mulai
     *
     * @return BatchCsr
     */
    public function setTahunAwal(int $tahunAwal): self
    {
        $this->tahunAwal = $tahunAwal;

        return $this;
    }

    /**
     * @return Collection|BatchCsrTahun[]
     */
    public function getTahuns(): Collection
    {
        return $this->tahuns;
    }

    public function addTahun(BatchCsrTahun $tahun): self
    {
        if (!$this->tahuns->contains($tahun)) {
            $this->tahuns[] = $tahun;
            $tahun->setBatchCsr($this);
        }

        return $this;
    }

    public function removeTahun(BatchCsrTahun $tahun): self
    {
        if ($this->tahuns->contains($tahun)) {
            $this->tahuns->removeElement($tahun);
            // set the owning side to null (unless already changed)
            if ($tahun->getBatchCsr() === $this) {
                $tahun->setBatchCsr(null);
            }
        }

        return $this;
    }

}
