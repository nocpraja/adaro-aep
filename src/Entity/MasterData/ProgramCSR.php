<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\MasterData;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ms_program_csr", indexes={@ORM\Index(name="ms_program_csr_x1", columns={"jenis_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\MasterData\ProgramCSRRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\ProgramCSR")
 *
 * @package App\Entity\MasterData
 * @author  Ahmad Fajar
 * @since   20/08/2018, modified: 25/05/2019 3:11
 */
class ProgramCSR
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="msid", type="integer")
     * @Groups({"without_rel", "with_rel"})
     */
    private $msid;

    /**
     * @ORM\Column(name="program", type="string", length=200)
     * @Assert\NotBlank(message="namaProgramCSR.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $name;

    /**
     * @ORM\Column(name="template_name", type="string", length=250, nullable=true)
     * @Groups({"without_rel"})
     */
    private $templateName;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"without_rel"})
     */
    private $description;

    /**
     * @var JenisProgramCSR
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\JenisProgramCSR")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\JenisProgramCSR", inversedBy="programs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="jenis_id", referencedColumnName="id", nullable=false)
     * })
     * @Groups({"with_parent"})
     */
    private $jenis;


    /**
     * Gets description of program CSR.
     *
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Sets the description of program CSR.
     *
     * @param null|string $description
     * @return ProgramCSR Fluent object, returns itself
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets jenis program CSR.
     *
     * @return JenisProgramCSR
     */
    public function getJenis(): ?JenisProgramCSR
    {
        return $this->jenis;
    }

    /**
     * Sets jenis program CSR.
     *
     * @param JenisProgramCSR $jenis
     * @return ProgramCSR Fluent object, returns itself
     */
    public function setJenis(JenisProgramCSR $jenis): self
    {
        $this->jenis = $jenis;

        return $this;
    }

    /**
     * Gets the entity ID.
     *
     * @return int|null
     */
    public function getMsid(): ?int
    {
        return $this->msid;
    }

    /**
     * Gets nama program CSR.
     *
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Sets nama program CSR.
     *
     * @param string $name
     * @return ProgramCSR Fluent object, returns itself
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets template judul program CSR.
     *
     * @return null|string
     */
    public function getTemplateName(): ?string
    {
        return $this->templateName;
    }

    /**
     * Sets template judul program CSR.
     *
     * @param null|string $templateName Template judul program
     * @return ProgramCSR Fluent object, returns itself
     */
    public function setTemplateName(?string $templateName): self
    {
        $this->templateName = $templateName;

        return $this;
    }

}
