<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use App\Entity\MasterData\Kecamatan;
use App\Entity\MasterData\Kelurahan;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait AlamatTrait untuk entity yang field alamat bersifat mandatory.
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   25/10/2018, modified: 2018-12-17 18:42
 */
trait AlamatTrait
{

    /**
     * @ORM\Column(name="alamat", type="string", length=255)
     * @Assert\NotBlank(message="Alamat.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $alamat;

    /**
     * @ORM\Column(name="rt_rw", type="string", length=11, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $rtRw;

    /**
     * @ORM\Column(name="kode_pos", type="string", length=15, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $kodePos;

    /**
     * @var Kecamatan
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Kecamatan")
     * @ORM\JoinColumn(name="kec_id", referencedColumnName="kec_id", nullable=false)
     * @Assert\NotNull()
     * @Groups({"with_rel"})
     */
    private $kecamatan;

    /**
     * @var Kelurahan
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Kelurahan")
     * @ORM\JoinColumn(name="kel_id", referencedColumnName="kel_id", nullable=true)
     * @Groups({"with_rel"})
     */
    private $kelurahan;


    /**
     * Get alamat rumah, institusi, dsb.
     *
     * @return null|string
     */
    public function getAlamat(): ?string
    {
        return $this->alamat;
    }

    /**
     * Set alamat rumah, institusi, dsb.
     *
     * @param string $alamat
     *
     * @return self
     */
    public function setAlamat(string $alamat): self
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Get wilayah kecamatan.
     *
     * @return Kecamatan
     */
    public function getKecamatan(): ?Kecamatan
    {
        return $this->kecamatan;
    }

    /**
     * Set wilayah kecamatan.
     *
     * @param Kecamatan $kecamatan
     *
     * @return self
     */
    public function setKecamatan(Kecamatan $kecamatan): self
    {
        $this->kecamatan = $kecamatan;

        return $this;
    }

    /**
     * Get wilayah kelurahan.
     *
     * @return Kelurahan
     */
    public function getKelurahan(): ?Kelurahan
    {
        return $this->kelurahan;
    }

    /**
     * Set wilayah kelurahan.
     *
     * @param Kelurahan $kelurahan
     *
     * @return self
     */
    public function setKelurahan(?Kelurahan $kelurahan): self
    {
        $this->kelurahan = $kelurahan;
        return $this;
    }

    /**
     * Get kode pos alamat.
     *
     * @return string|null
     */
    public function getKodePos(): ?string
    {
        return $this->kodePos;
    }

    /**
     * Set kode pos alamat.
     *
     * @param string|null $kodePos
     *
     * @return AlamatTrait
     */
    public function setKodePos(?string $kodePos): self
    {
        $this->kodePos = $kodePos;

        return $this;
    }

    /**
     * Get RT/RW dari alamat.
     *
     * @return string|null
     */
    public function getRtRw(): ?string
    {
        return $this->rtRw;
    }

    /**
     * Set RT/RW dari alamat.
     *
     * @param string|null $rtRw
     *
     * @return AlamatTrait
     */
    public function setRtRw(?string $rtRw): self
    {
        $this->rtRw = $rtRw;

        return $this;
    }

}