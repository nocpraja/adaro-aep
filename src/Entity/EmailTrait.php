<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait EmailTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   01/11/2018, modified: 01/11/2018 05:07
 */
trait EmailTrait
{

    /**
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     * @Assert\Email()
     * @Groups({"without_rel", "with_rel"})
     */
    private $email;


    /**
     * Get email address.
     *
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set email address.
     *
     * @param null|string $email The email address
     * @return self
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

}