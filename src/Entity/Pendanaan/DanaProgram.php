<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\MasterData\ProgramCSR;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="dana_program", indexes={
 *     @ORM\Index(name="dana_program_x1", columns={"db_id"}),
 *     @ORM\Index(name="dana_program_x2", columns={"msid"}),
 *     @ORM\Index(name="dana_program_x3", columns={"kode"}),
 *     @ORM\Index(name="dana_program_x4", columns={"verified_by"}),
 *     @ORM\Index(name="dana_program_x5", columns={"approved_by"}),
 *     @ORM\Index(name="dana_program_x6", columns={"closed_by"}),
 *     @ORM\Index(name="dana_program_x7", columns={"posted_by"}),
 *     @ORM\Index(name="dana_program_x8", columns={"updated_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\DanaProgramRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaProgram")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 19/05/2019 21:35
 */
class DanaProgram implements EntityDanaInterface
{

    use AnggaranTrait, AnggaranMultiYearsTrait;
    use VerifiedByTrait, ApprovedByTrait, ClosedByTrait;
    use PostedByFkTrait, PostedDateTrait, UpdatedByFkTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="dp_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @var DanaBidang
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBidang")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaBidang")
     * @ORM\JoinColumn(name="db_id", referencedColumnName="db_id", nullable=false)
     * @Assert\NotNull(message="Pendanaan.DanaBidang.NotNull")
     * @Groups({"with_parent"})
     */
    private $danaBidang;

    /**
     * @var ProgramCSR
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\ProgramCSR")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\ProgramCSR")
     * @ORM\JoinColumn(name="msid", referencedColumnName="msid", nullable=false)
     * @Assert\NotNull(message="Pendanaan.ProgramCsr.NotNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $programCsr;


    /**
     * Get Dana Bidang sebagai sumber dana utama.
     *
     * @return DanaBidang
     */
    public function getDanaBidang(): ?DanaBidang
    {
        return $this->danaBidang;
    }

    /**
     * Set Dana Bidang sebagai sumber dana utama.
     *
     * @param DanaBidang $danaBidang
     *
     * @return self
     */
    public function setDanaBidang(DanaBidang $danaBidang): self
    {
        $this->danaBidang = $danaBidang;

        return $this;
    }

    /**
     * Get the Entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get Program CSR untuk penyaluran dana.
     *
     * @return ProgramCSR
     */
    public function getProgramCsr(): ?ProgramCSR
    {
        return $this->programCsr;
    }

    /**
     * Set Program CSR untuk penyaluran dana.
     *
     * @param ProgramCSR $programCsr
     *
     * @return self
     */
    public function setProgramCsr(ProgramCSR $programCsr): self
    {
        $this->programCsr = $programCsr;

        return $this;
    }

}
