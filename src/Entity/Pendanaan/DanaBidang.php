<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\MasterData\JenisProgramCSR;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="dana_bidang", indexes={
 *     @ORM\Index(name="dana_bidang_x1", columns={"bidang_id"}),
 *     @ORM\Index(name="dana_bidang_x2", columns={"kode"}),
 *     @ORM\Index(name="dana_bidang_x3", columns={"verified_by"}),
 *     @ORM\Index(name="dana_bidang_x4", columns={"approved_by"}),
 *     @ORM\Index(name="dana_bidang_x5", columns={"closed_by"}),
 *     @ORM\Index(name="dana_bidang_x6", columns={"posted_by"}),
 *     @ORM\Index(name="dana_bidang_x7", columns={"updated_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\DanaBidangRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBidang")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 3:00
 */
class DanaBidang implements EntityDanaInterface
{

    use AnggaranTrait, AnggaranMultiYearsTrait;
    use VerifiedByTrait, ApprovedByTrait, ClosedByTrait;
    use PostedByFkTrait, PostedDateTrait, UpdatedByFkTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="db_id", type="bigint")
     * @Groups({"without_rel"})
     */
    private $id;

    /**
     * @var JenisProgramCSR
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\JenisProgramCSR")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\JenisProgramCSR")
     * @ORM\JoinColumn(name="bidang_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull(message="Pendanaan.BidangCsr.NotNull")
     * @Groups({"without_rel"})
     */
    private $bidangCsr;


    /**
     * Get the Entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get Bidang CSR untuk penyaluran dana.
     *
     * @return JenisProgramCSR
     */
    public function getBidangCsr(): ?JenisProgramCSR
    {
        return $this->bidangCsr;
    }

    /**
     * Set Bidang CSR untuk penyaluran dana.
     *
     * @param JenisProgramCSR $bidangCsr
     *
     * @return DanaBidang
     */
    public function setBidangCsr(JenisProgramCSR $bidangCsr): self
    {
        $this->bidangCsr = $bidangCsr;

        return $this;
    }

}
