<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\Security\UserAccount;
use App\Service\Security\AppUserInterface;
use DateTimeInterface;

/**
 * Interface EntityDanaInterface.
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   05/05/2019, modified: 07/05/2019 22:57
 */
interface EntityDanaInterface
{

    /**
     * Get the Entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Get jumlah anggaran yang dialokasikan (dalam rupiah).
     *
     * @return float|null
     */
    public function getAlokasi(): ?float;

    /**
     * Set jumlah anggaran yang dialokasikan (dalam rupiah).
     *
     * @param float|null $alokasi Jumlah alokasi dana (dalam rupiah)
     *
     * @return self
     */
    public function setAlokasi(?float $alokasi): self;

    /**
     * Get nilai anggaran (dalam rupiah).
     *
     * @return float|null
     */
    public function getAnggaran(): ?float;

    /**
     * Set nilai anggaran (dalam rupiah).
     *
     * @param float $anggaran Nilai anggaran (dalam rupiah)
     *
     * @return self
     */
    public function setAnggaran(float $anggaran): self;

    /**
     * Get a User Account that approves the post.
     *
     * @return UserAccount
     */
    public function getApprovedBy(): ?UserAccount;

    /**
     * Set a User Account that approves the post.
     *
     * @param UserAccount|AppUserInterface $userAccount The user account that approves the post
     *
     * @return self
     */
    public function setApprovedBy(?UserAccount $userAccount);

    /**
     * Get a User Account that closes the post.
     *
     * @return UserAccount
     */
    public function getClosedBy(): ?UserAccount;

    /**
     * Set a User Account that closes the post.
     *
     * @param UserAccount|AppUserInterface $userAccount
     *
     * @return self
     */
    public function setClosedBy(?UserAccount $userAccount);

    /**
     * Jumlah dana yang tersedia setelah dikurangi dengan alokasi dana.
     *
     * @return float
     */
    public function getDanaTersedia(): float;

    /**
     * Get kode referensi untuk Riwayat Transaksi.
     *
     * @return string|null
     */
    public function getKode(): ?string;

    /**
     * Set kode referensi untuk Riwayat Transaksi.
     *
     * Contoh format:
     * <pre>
     * DG-yy.0000 => untuk transaksi Dana Global
     * DB-yy.0000 => untuk transaksi Dana Bidang
     * DP-yy.0000 => untuk transaksi Dana Program
     * BP-yy.0000 => untuk transaksi Dana Batch
     * </pre>
     *
     * @param string $kode Kode referensi
     *
     * @return self
     */
    public function setKode(string $kode): self;

    /**
     * Get last modified datetime.
     *
     * @return DateTimeInterface
     */
    public function getLastUpdated(): ?DateTimeInterface;

    /**
     * Set the modified datetime.
     *
     * @param DateTimeInterface $lastUpdated
     *
     * @return self
     */
    public function setLastUpdated(?DateTimeInterface $lastUpdated);

    /**
     * Transient field untuk perubahan nilai anggaran.
     *
     * @return float
     */
    public function getNewValue(): ?float;

    /**
     * Get jumlah anggaran yang terpakai atau telah terealisasi (dalam rupiah).
     *
     * @return float|null
     */
    public function getRealisasi(): ?float;

    /**
     * Set jumlah anggaran yang terpakai atau telah terealisasi (dalam rupiah).
     *
     * @param float|null $realisasi Jumlah realisasi pemakaian dana (dalam rupiah)
     *
     * @return self
     */
    public function setRealisasi(?float $realisasi): self;

    /**
     * Jumlah dana yang tersisa setelah dikurangi dengan realisasi.
     *
     * @return float
     */
    public function getSisaDana(): float;

    /**
     * Get status or progress pendanaan.
     *
     * @return string|null Salah satu dari nilai berikut: NEW, UPDATED, PLANNED, ONGOING, CLOSED, REJECTED,
     *                     VERIFIED, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REVISI_REJECTED
     */
    public function getStatus(): ?string;

    /**
     * Set status or progress pendanaan.
     *
     * @param string $status Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, REJECTED, VERIFIED,
     *                       APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REVISI_REJECTED
     *
     * @return self
     */
    public function setStatus(string $status): self;

    /**
     * Get additional information within a tags.
     *
     * @return array|null Additional information
     */
    public function getTags(): ?array;

    /**
     * Apply additional information.
     *
     * @param array|null $tags Additional information
     *
     * @return EntityDanaInterface
     */
    public function setTags(?array $tags): self;

    /**
     * Get a datetime when the post is approved.
     *
     * @return DateTimeInterface
     */
    public function getTanggalApproved(): ?DateTimeInterface;

    /**
     * Set a datetime when the post is approved.
     *
     * @param DateTimeInterface $dateTime A datetime when the post is approved
     *
     * @return self
     */
    public function setTanggalApproved(?DateTimeInterface $dateTime);

    /**
     * Get a datetime when the post is closed.
     *
     * @return DateTimeInterface
     */
    public function getTanggalClosed(): ?DateTimeInterface;

    /**
     * Set a datetime when the post is closed.
     *
     * @param DateTimeInterface $dateTime A datetime when the post is closed
     *
     * @return self
     */
    public function setTanggalClosed(?DateTimeInterface $dateTime);

    /**
     * Get a datetime when the post is verified.
     *
     * @return DateTimeInterface
     */
    public function getTanggalVerified(): ?DateTimeInterface;

    /**
     * Set a datetime when the post is verified.
     *
     * @param DateTimeInterface $dateTime A datetime when the post is verified
     *
     * @return self
     */
    public function setTanggalVerified(?DateTimeInterface $dateTime);

    /**
     * Get nama program pendanaan.
     *
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Get the UserAccount who made the update.
     *
     * @return null|UserAccount
     */
    public function getUpdatedBy(): ?UserAccount;

    /**
     * Set the UserAccount who made the update.
     *
     * @param UserAccount|AppUserInterface $updatedBy
     *
     * @return self
     */
    public function setUpdatedBy(?UserAccount $updatedBy);

    /**
     * Get a User Account that verifies the post.
     *
     * @return UserAccount
     */
    public function getVerifiedBy(): ?UserAccount;

    /**
     * Set a User Account that verifies the post.
     *
     * @param UserAccount|AppUserInterface $userAccount The user account that verifies the post
     *
     * @return self
     */
    public function setVerifiedBy(?UserAccount $userAccount);

}