<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\MasterData\DataReference;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="dana_batch_donatur", indexes={
 *     @ORM\Index(name="dana_batch_donatur_x1", columns={"ref_id"}),
 *     @ORM\Index(name="dana_batch_donatur_x2", columns={"bp_id"})
 * }, options={"comment": "Tabel jumlah dana yang diberikan oleh Perusahaan Donatur"})
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\DanaBatchDonaturRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\Donatur")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   27/09/2018, modified: 25/05/2019 2:47
 */
class DanaBatchDonatur
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $ids;

    /**
     * @ORM\Column(name="jumlah", type="float")
     * @Assert\NotNull(message="Pendanaan.JumlahDonasi.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jumlah;

    /**
     * @var DanaBatch
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBatch")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaBatch", inversedBy="donaturs")
     * @ORM\JoinColumn(name="bp_id", referencedColumnName="bp_id", nullable=false, onDelete="CASCADE")
     * @Groups({"with_parent"})
     */
    private $pendanaan;

    /**
     * @var DataReference
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\DataReference")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\DataReference")
     * @ORM\JoinColumn(name="ref_id", referencedColumnName="id", nullable=false)
     * @Groups({"without_rel", "with_rel"})
     */
    private $company;


    /**
     * Get perusahaan pemberi donasi.
     *
     * @return DataReference
     */
    public function getCompany(): ?DataReference
    {
        return $this->company;
    }

    /**
     * Set perusahaan pemberi donasi.
     *
     * @param DataReference $company
     *
     * @return DanaBatchDonatur Fluent interface, return itself
     */
    public function setCompany(DataReference $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get SumberDana entity ID.
     *
     * @return int|null
     */
    public function getIds(): ?int
    {
        return $this->ids;
    }

    /**
     * Get jumlah dana yang didonasikan.
     *
     * @return float|null
     */
    public function getJumlah(): ?float
    {
        return $this->jumlah;
    }

    /**
     * Set jumlah dana yang didonasikan.
     *
     * @param float $jumlah
     *
     * @return DanaBatchDonatur Fluent interface, return itself
     */
    public function setJumlah(float $jumlah): self
    {
        $this->jumlah = $jumlah;

        return $this;
    }

    /**
     * Get program pendanaan.
     *
     * @return DanaBatch
     */
    public function getPendanaan(): ?DanaBatch
    {
        return $this->pendanaan;
    }

    /**
     * Set program pendanaan.
     *
     * @param DanaBatch $pendanaan
     *
     * @return DanaBatchDonatur Fluent interface, return itself
     */
    public function setPendanaan(DanaBatch $pendanaan): self
    {
        $this->pendanaan = $pendanaan;

        return $this;
    }

}
