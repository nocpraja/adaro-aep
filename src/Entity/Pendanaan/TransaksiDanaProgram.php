<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="ts_dana_program", indexes={
 *     @ORM\Index(name="ts_dana_program_x1", columns={"dp_id"}),
 *     @ORM\Index(name="ts_dana_program_x2", columns={"posted_by"})
 * }, options={"comment": "Data riwayat transaksi Dana Program"})
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\TransaksiDanaProgramRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\TransaksiDanaProgram")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 3:02
 */
class TransaksiDanaProgram
{

    use TransaksiTrait, PostedByFkTrait, PostedDateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="tsid", type="bigint")
     * @Groups({"without_rel"})
     */
    private $tid;

    /**
     * @var DanaProgram
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaProgram")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaProgram")
     * @ORM\JoinColumn(name="dp_id", referencedColumnName="dp_id", onDelete="CASCADE")
     */
    private $danaProgram;


    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getTid(): ?int
    {
        return $this->tid;
    }

    /**
     * @return DanaProgram
     */
    public function getDanaProgram(): ?DanaProgram
    {
        return $this->danaProgram;
    }

    /**
     * @param DanaProgram $danaProgram
     *
     * @return TransaksiDanaProgram
     */
    public function setDanaProgram(?DanaProgram $danaProgram): self
    {
        $this->danaProgram = $danaProgram;

        return $this;
    }

}
