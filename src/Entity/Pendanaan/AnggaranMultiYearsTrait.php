<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait AnggaranMultiYearsTrait
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 22/04/2019 14:38
 */
trait AnggaranMultiYearsTrait
{

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank(message="Pendanaan.Title.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $title;

    /**
     * @ORM\Column(name="tahun_awal", type="integer")
     * @Assert\NotNull(message="Pendanaan.PeriodeAwal.NotNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahunAwal;

    /**
     * @ORM\Column(name="tahun_akhir", type="integer")
     * @Assert\NotNull(message="Pendanaan.PeriodeAkhir.NotNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahunAkhir;


    /**
     * Get periode akhir pendanaan.
     *
     * @return int|null
     */
    public function getTahunAkhir(): ?int
    {
        return $this->tahunAkhir;
    }

    /**
     * Set periode akhir pendanaan.
     *
     * @param int $tahun Tahun akhir
     *
     * @return self
     */
    public function setTahunAkhir(int $tahun): self
    {
        $this->tahunAkhir = $tahun;

        return $this;
    }

    /**
     * Get periode awal pendanaan.
     *
     * @return int|null
     */
    public function getTahunAwal(): ?int
    {
        return $this->tahunAwal;
    }

    /**
     * Set periode awal pendanaan.
     *
     * @param int $tahun Tahun awal
     *
     * @return self
     */
    public function setTahunAwal(int $tahun): self
    {
        $this->tahunAwal = $tahun;

        return $this;
    }

    /**
     * Get nama pendanaan.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set nama pendanaan.
     *
     * @param string $title Nama pendanaan
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

}