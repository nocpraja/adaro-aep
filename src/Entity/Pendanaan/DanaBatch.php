<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\MasterData\BatchCsr;
use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="dana_batch", indexes={
 *     @ORM\Index(name="dana_batch_x1", columns={"dp_id"}),
 *     @ORM\Index(name="dana_batch_x2", columns={"batch_id"}),
 *     @ORM\Index(name="dana_batch_x3", columns={"kode"}),
 *     @ORM\Index(name="dana_batch_x4", columns={"verified_by"}),
 *     @ORM\Index(name="dana_batch_x5", columns={"approved_by"}),
 *     @ORM\Index(name="dana_batch_x6", columns={"closed_by"}),
 *     @ORM\Index(name="dana_batch_x7", columns={"posted_by"}),
 *     @ORM\Index(name="dana_batch_x8", columns={"updated_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\DanaBatchRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBatch")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 19/05/2019 21:35
 */
class DanaBatch implements EntityDanaInterface
{

    use AnggaranTrait, VerifiedByTrait, ApprovedByTrait, ClosedByTrait;
    use PostedByFkTrait, PostedDateTrait, UpdatedByFkTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="bp_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="nama_program", type="string", length=255)
     * @Assert\NotBlank(message="Pendanaan.NamaProgram.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $namaProgram;

    /**
     * @var integer
     * @ORM\Column(name="tahun", type="integer")
     * @Assert\NotNull(message="Pendanaan.TahunAnggaran.NotNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahun;

    /**
     * @var DanaProgram
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaProgram")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaProgram")
     * @ORM\JoinColumn(name="dp_id", referencedColumnName="dp_id", nullable=false)
     * @Assert\NotNull(message="Pendanaan.DanaProgram.NotNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $danaProgram;

    /**
     * @var BatchCsr
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="MasterData\BatchCsr")
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\BatchCsr")
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="batch_id", nullable=false)
     * @Assert\NotNull(message="Pendanaan.Batch.NotNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $batch;

    /**
     * @var DanaBatchDonatur[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\Donatur")
     * @ORM\OneToMany(targetEntity="DanaBatchDonatur", mappedBy="pendanaan", cascade={"persist","merge"})
     * @ORM\OrderBy({"ids" = "ASC"})
     * @Groups({"with_rel"})
     */
    private $donaturs;

    /**
     * @var DanaBatchRevisiAnggaran[]|Collection
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\AnggaranRevisiDanaBatch")
     * @ORM\OneToMany(targetEntity="DanaBatchRevisiAnggaran", mappedBy="danaBatch", cascade={"persist","merge"})
     * @ORM\OrderBy({"id" = "ASC"})
     * @Groups({"with_child"})
     */
    private $revisiAnggarans;

    /**
     * @var DanaBatchRevisiAnggaran[]|Collection
     */
    private $requests;


    /**
     * DanaBatch constructor.
     */
    public function __construct()
    {
        $this->donaturs = new ArrayCollection();
        $this->revisiAnggarans = new ArrayCollection();
        $this->requests = new ArrayCollection();
    }

    /**
     * Add donatur penyandang dana to the collection.
     *
     * @param DanaBatchDonatur $donatur
     */
    public function addDonatur(DanaBatchDonatur $donatur)
    {
        $donatur->setPendanaan($this);
        $donaturs = $this->getDonaturs();

        if ($donaturs instanceof Collection) {
            $donaturs->add($donatur);
        } elseif (empty($donaturs)) {
            $this->donaturs = new ArrayCollection();
            $this->donaturs->add($donatur);
        } else {
            $this->donaturs[] = $donatur;
        }
    }

    /**
     * Add anggaran dana revisi.
     *
     * @param DanaBatchRevisiAnggaran $anggaran
     */
    public function addRevisiAnggaran(DanaBatchRevisiAnggaran $anggaran)
    {
        $anggaran->setDanaBatch($this);
        $revisiAnggarans = $this->getRevisiAnggarans();

        if ($revisiAnggarans instanceof Collection) {
            $revisiAnggarans->add($anggaran);
        } elseif (empty($revisiAnggarans)) {
            $this->revisiAnggarans = new ArrayCollection();
            $this->revisiAnggarans->add($anggaran);
        } else {
            $this->revisiAnggarans[] = $anggaran;
        }
    }

    /**
     * Get Batch CSR untuk penyaluran dana.
     *
     * @return BatchCsr
     */
    public function getBatch(): ?BatchCsr
    {
        return $this->batch;
    }

    /**
     * Set Batch CSR untuk penyaluran dana.
     *
     * @param BatchCsr $batch
     *
     * @return self
     */
    public function setBatch(BatchCsr $batch): self
    {
        $this->batch = $batch;

        return $this;
    }

    /**
     * Get Dana Program sebagai sumber dana utama.
     *
     * @return DanaProgram
     */
    public function getDanaProgram(): ?DanaProgram
    {
        return $this->danaProgram;
    }

    /**
     * Set Dana Program sebagai sumber dana utama.
     *
     * @param DanaProgram $danaProgram
     *
     * @return self
     */
    public function setDanaProgram(DanaProgram $danaProgram): self
    {
        $this->danaProgram = $danaProgram;

        return $this;
    }

    /**
     * Get Donatur penyandang dana.
     *
     * @return DanaBatchDonatur[]|Collection
     */
    public function getDonaturs()
    {
        return $this->donaturs;
    }

    /**
     * Assign Donatur penyandang dana.
     *
     * @param DanaBatchDonatur[]|Collection $donaturs
     *
     * @return self
     */
    public function setDonaturs(?array $donaturs): self
    {
        if ((!empty($donaturs) && is_array($donaturs)) || ($donaturs instanceof Collection)) {
            $this->donaturs->clear();

            foreach ($donaturs as $donatur) {
                $this->addDonatur($donatur);
            }
        } else {
            $this->donaturs->clear();
        }

        return $this;
    }

    /**
     * Get the entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get nama program pendanaan.
     *
     * @return string|null
     */
    public function getNamaProgram(): ?string
    {
        return $this->namaProgram;
    }

    /**
     * Set nama program pendanaan.
     *
     * @param string $namaProgram
     *
     * @return self
     */
    public function setNamaProgram(string $namaProgram): self
    {
        $this->namaProgram = $namaProgram;

        return $this;
    }

    /**
     * Transient fields untuk pengajuan revisi anggaran Dana Batch.
     *
     * @return DanaBatchRevisiAnggaran[]|Collection
     */
    public function getRequests(): ?array
    {
        return $this->requests;
    }

    /**
     * Transient fields untuk pengajuan revisi anggaran Dana Batch.
     *
     * @param DanaBatchRevisiAnggaran[]|Collection $requests
     *
     * @return DanaBatch
     */
    public function setRequests(?array $requests): self
    {
        $this->requests = $requests;

        return $this;
    }

    /**
     * Get collection of data revisi anggaran Dana Batch.
     *
     * @return DanaBatchRevisiAnggaran[]|Collection
     */
    public function getRevisiAnggarans()
    {
        return $this->revisiAnggarans;
    }

    /**
     * Assign data revisi anggaran Dana Batch.
     *
     * @param DanaBatchRevisiAnggaran[]|Collection $revisiAnggarans
     *
     * @return DanaBatch
     */
    public function setRevisiAnggarans(?array $revisiAnggarans): self
    {
        if ((!empty($revisiAnggarans) && is_array($revisiAnggarans)) || ($revisiAnggarans instanceof Collection)) {
            $this->revisiAnggarans->clear();

            foreach ($revisiAnggarans as $anggaran) {
                $this->addRevisiAnggaran($anggaran);
            }
        } else {
            $this->revisiAnggarans->clear();
        }

        return $this;
    }

    /**
     * Get tahun anggaran penggunaan dana.
     *
     * @return int
     */
    public function getTahun(): ?int
    {
        return $this->tahun;
    }

    /**
     * Set tahun anggaran penggunaan dana.
     *
     * @param int $tahun Tahun anggaran
     *
     * @return self
     */
    public function setTahun(int $tahun): self
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get nama program pendanaan.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->namaProgram;
    }

}
