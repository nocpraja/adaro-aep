<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\Security\UserAccount;
use App\Service\Security\AppUserInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait ClosedByTrait
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 2:57
 */
trait ClosedByTrait
{

    /**
     * @var UserAccount
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="closed_by", referencedColumnName="uid", nullable=true)
     * @Groups({"with_approval"})
     */
    private $closedBy;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="tgl_closed", type="datetime", nullable=true)
     * @Groups({"with_approval"})
     */
    private $tanggalClosed;


    /**
     * Get a User Account that closes the post.
     *
     * @return UserAccount
     */
    public function getClosedBy(): ?UserAccount
    {
        return $this->closedBy;
    }

    /**
     * Set a User Account that closes the post.
     *
     * @param UserAccount|AppUserInterface $userAccount
     *
     * @return self
     */
    public function setClosedBy(?UserAccount $userAccount): self
    {
        $this->closedBy = $userAccount;

        return $this;
    }

    /**
     * Get a datetime when the post is closed.
     *
     * @return DateTimeInterface
     */
    public function getTanggalClosed(): ?DateTimeInterface
    {
        return $this->tanggalClosed;
    }

    /**
     * Set a datetime when the post is closed.
     *
     * @param DateTimeInterface $dateTime A datetime when the post is closed
     *
     * @return self
     */
    public function setTanggalClosed(?DateTimeInterface $dateTime): self
    {
        $this->tanggalClosed = $dateTime;

        return $this;
    }

}