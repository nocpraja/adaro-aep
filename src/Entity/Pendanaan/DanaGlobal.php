<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="dana_global", indexes={
 *     @ORM\Index(name="dana_global_x1", columns={"kode"}),
 *     @ORM\Index(name="dana_global_x2", columns={"verified_by"}),
 *     @ORM\Index(name="dana_global_x3", columns={"posted_by"}),
 *     @ORM\Index(name="dana_global_x4", columns={"updated_by"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\DanaGlobalRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaGlobal")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 28/04/2019 20:33
 */
class DanaGlobal
{

    use AnggaranMultiYearsTrait, VerifiedByTrait;
    use PostedByFkTrait, PostedDateTrait, UpdatedByFkTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="dg_id", type="bigint")
     * @Groups({"without_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="string", length=50, options={
     *     "default": "NEW",
     *     "comment": "Valid values: NEW, UPDATED, VERIFIED, REJECTED"
     * })
     * @Groups({"without_rel"})
     */
    private $status = WorkflowTags::NEW;

    /**
     * @ORM\Column(name="kode", type="string", length=25, options={
     *     "comment": "Kode untuk referensi Riwayat Transaksi. Format: DG-yy.0000"
     * })
     * @Groups({"without_rel"})
     */
    private $kode;

    /**
     * @ORM\Column(name="jumlah", type="float", options={"comment": "Nilai anggaran dana (dalam Rupiah)"})
     * @Groups({"without_rel"})
     */
    private $anggaran = 0;

    /**
     * @var float
     *
     * @Assert\NotNull(message="Pendanaan.Jumlah.NotNull")
     */
    private $newValue;


    /**
     * Get nilai anggaran (dalam rupiah).
     *
     * @return float|null
     */
    public function getAnggaran(): ?float
    {
        return $this->anggaran;
    }

    /**
     * Set nilai anggaran.
     *
     * @param float $value Nilai anggaran (dalam rupiah)
     *
     * @return self
     */
    public function setAnggaran(float $value): self
    {
        $this->anggaran = $value;

        return $this;
    }

    /**
     * Get the entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get kode referensi untuk Riwayat Transaksi.
     *
     * @return string|null
     */
    public function getKode(): ?string
    {
        return $this->kode;
    }

    /**
     * Set kode referensi untuk Riwayat Transaksi.
     *
     * @param string $kode Valid format: DG-yy.0000
     *
     * @return self
     */
    public function setKode(string $kode): self
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Transient field untuk perubahan nilai anggaran.
     *
     * @return float
     */
    public function getNewValue(): ?float
    {
        return $this->newValue;
    }

    /**
     * Transient field untuk perubahan nilai anggaran.
     *
     * @param float $value Nilai anggaran (dalam rupiah)
     *
     * @return DanaGlobal
     */
    public function setNewValue(?float $value): self
    {
        $this->newValue = $value;

        return $this;
    }

    /**
     * Get status or progress pendanaan.
     *
     * @return string|null Salah satu dari nilai berikut: NEW, UPDATED, VERIFIED, REJECTED
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set status or progress pendanaan.
     *
     * @param string $status Valid values: NEW, UPDATED, VERIFIED, REJECTED
     *
     * @return self
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

}
