<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\Security\UserAccount;
use App\Service\Security\AppUserInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait ApprovedByTrait
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 2:57
 */
trait ApprovedByTrait
{

    /**
     * @var UserAccount
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="approved_by", referencedColumnName="uid", nullable=true)
     * @Groups({"with_approval"})
     */
    private $approvedBy;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="tgl_approved", type="datetime", nullable=true)
     * @Groups({"with_approval"})
     */
    private $tanggalApproved;


    /**
     * Get a User Account that approves the post.
     *
     * @return UserAccount
     */
    public function getApprovedBy(): ?UserAccount
    {
        return $this->approvedBy;
    }

    /**
     * Set a User Account that approves the post.
     *
     * @param UserAccount|AppUserInterface $userAccount The user account that approves the post
     *
     * @return self
     */
    public function setApprovedBy(?UserAccount $userAccount): self
    {
        $this->approvedBy = $userAccount;

        return $this;
    }

    /**
     * Get a datetime when the post is approved.
     *
     * @return DateTimeInterface
     */
    public function getTanggalApproved(): ?DateTimeInterface
    {
        return $this->tanggalApproved;
    }

    /**
     * Set a datetime when the post is approved.
     *
     * @param DateTimeInterface $dateTime A datetime when the post is approved
     *
     * @return self
     */
    public function setTanggalApproved(?DateTimeInterface $dateTime): self
    {
        $this->tanggalApproved = $dateTime;

        return $this;
    }

}