<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\Security\UserAccount;
use App\Service\Security\AppUserInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait VerifiedByTrait
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 2:58
 */
trait VerifiedByTrait
{

    /**
     * @var UserAccount
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="verified_by", referencedColumnName="uid", nullable=true)
     * @Groups({"with_approval"})
     */
    private $verifiedBy;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="tgl_verified", type="datetime", nullable=true)
     * @Groups({"with_approval"})
     */
    private $tanggalVerified;


    /**
     * Get a User Account that verifies the post.
     *
     * @return UserAccount
     */
    public function getVerifiedBy(): ?UserAccount
    {
        return $this->verifiedBy;
    }

    /**
     * Set a User Account that verifies the post.
     *
     * @param UserAccount|AppUserInterface $userAccount The user account that verifies the post
     *
     * @return self
     */
    public function setVerifiedBy(?UserAccount $userAccount): self
    {
        $this->verifiedBy = $userAccount;

        return $this;
    }

    /**
     * Get a datetime when the post is verified.
     *
     * @return DateTimeInterface
     */
    public function getTanggalVerified(): ?DateTimeInterface
    {
        return $this->tanggalVerified;
    }

    /**
     * Set a datetime when the post is verified.
     *
     * @param DateTimeInterface $dateTime A datetime when the post is verified
     *
     * @return self
     */
    public function setTanggalVerified(?DateTimeInterface $dateTime): self
    {
        $this->tanggalVerified = $dateTime;

        return $this;
    }

}