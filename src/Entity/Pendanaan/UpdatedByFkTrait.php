<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\Security\UserAccount;
use App\Service\Security\AppUserInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait UpdateByFkTrait
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   28/04/2019, modified: 25/05/2019 2:58
 */
trait UpdatedByFkTrait
{
    /**
     * @var UserAccount
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="uid", nullable=true)
     * @Gedmo\Blameable(on="create")
     * @Groups({"with_postdate"})
     */
    private $updatedBy;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="last_updated", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     * @Groups({"with_postdate"})
     */
    private $lastUpdated;


    /**
     * Get last modified datetime.
     *
     * @return DateTimeInterface
     */
    public function getLastUpdated(): ?DateTimeInterface
    {
        return $this->lastUpdated;
    }

    /**
     * Set the modified datetime.
     *
     * @param DateTimeInterface $lastUpdated
     *
     * @return self
     */
    public function setLastUpdated(?DateTimeInterface $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * Get the UserAccount who made the update.
     *
     * @return null|UserAccount
     */
    public function getUpdatedBy(): ?UserAccount
    {
        return $this->updatedBy;
    }

    /**
     * Set the UserAccount who made the update.
     *
     * @param UserAccount|AppUserInterface $updatedBy
     *
     * @return self
     */
    public function setUpdatedBy(?UserAccount $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

}