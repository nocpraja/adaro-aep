<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="ts_dana_global", indexes={
 *     @ORM\Index(name="ts_dana_global_x1", columns={"posted_by"})
 * }, options={"comment": "Data riwayat transaksi Dana GLobal"})
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\TransaksiDanaGlobalRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\TransaksiDanaGlobal")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 22/04/2019 15:46
 */
class TransaksiDanaGlobal
{

    use TransaksiTrait, PostedByFkTrait, PostedDateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="tsid", type="bigint")
     * @Groups({"without_rel"})
     */
    private $tid;


    /**
     * Get Entity ID.
     *
     * @return int|null
     */
    public function getTid(): ?int
    {
        return $this->tid;
    }

}
