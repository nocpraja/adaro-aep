<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\LastUpdateTrait;
use App\Entity\PostedByTrait;
use App\Entity\PostedDateTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="dana_batch_revisi", indexes={
 *     @ORM\Index(name="dana_batch_revisi_x1", columns={"bp_id"}),
 *     @ORM\Index(name="dana_batch_revisi_x2", columns={"refcode"})
 * }, options={"comment": "Data revisi anggaran untuk request revisi Dana Batch"})
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\DanaBatchRevisiAnggaranRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\AnggaranRevisiDanaBatch")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   10/05/2019, modified: 25/05/2019 2:59
 */
class DanaBatchRevisiAnggaran
{

    use PostedByTrait, PostedDateTrait, LastUpdateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="ids", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @ORM\Column(name="refcode", type="string", length=25)
     * @Assert\NotBlank(message="Pendanaan.KodeReferensi.notBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $refcode;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"without_rel", "with_rel"})
     */
    private $title;

    /**
     * @ORM\Column(name="jumlah", type="float")
     * @Assert\NotNull(message="Pendanaan.JumlahAnggaran.notNull")
     * @Groups({"without_rel", "with_rel"})
     */
    private $jumlah;

    /**
     * @ORM\Column(name="revnum", type="integer", options={"default": 1})
     * @Groups({"without_rel", "with_rel"})
     */
    private $revnum = 1;

    /**
     * @ORM\Column(name="status", type="string", length=50, options={"default": "NEW"})
     * @Groups({"without_rel", "with_rel"})
     */
    private $status = WorkflowTags::NEW;

    /**
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBatch")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaBatch", inversedBy="revisiAnggarans")
     * @ORM\JoinColumn(name="bp_id", referencedColumnName="bp_id", nullable=false, onDelete="CASCADE")
     */
    private $danaBatch;


    /**
     * @return DanaBatch
     */
    public function getDanaBatch(): ?DanaBatch
    {
        return $this->danaBatch;
    }

    /**
     * @param DanaBatch $danaBatch
     *
     * @return DanaBatchRevisiAnggaran
     */
    public function setDanaBatch(DanaBatch $danaBatch): self
    {
        $this->danaBatch = $danaBatch;

        return $this;
    }

    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Jumlah dana yang diambil dari program yang menjadi sumber dana revisi Dana Batch.
     *
     * @return float|null Jumlah dana (dalam rupiah)
     */
    public function getJumlah(): ?float
    {
        return $this->jumlah;
    }

    /**
     * Jumlah dana yang diambil dari program yang menjadi sumber dana revisi Dana Batch.
     *
     * @param float $jumlah Jumlah dana (dalam rupiah)
     *
     * @return DanaBatchRevisiAnggaran
     */
    public function setJumlah(float $jumlah): self
    {
        $this->jumlah = $jumlah;

        return $this;
    }

    /**
     * Get kode referensi sumber dana program yang menjadi sumber dana revisi Dana Batch.
     *
     * @return string|null
     */
    public function getRefcode(): ?string
    {
        return $this->refcode;
    }

    /**
     * Set kode referensi sumber dana program yang menjadi sumber dana revisi Dana Batch.
     *
     * @param string $refcode Kode referensi
     *
     * @return DanaBatchRevisiAnggaran
     */
    public function setRefcode(string $refcode): self
    {
        $this->refcode = $refcode;

        return $this;
    }

    /**
     * Revisi anggaran ke berapa?
     *
     * @return int
     */
    public function getRevnum(): ?int
    {
        return $this->revnum;
    }

    /**
     * Set revisi anggaran yang ke berapa.
     *
     * @param int $revnum Revisi anggaran ke berapa?
     *
     * @return DanaBatchRevisiAnggaran
     */
    public function setRevnum(int $revnum): self
    {
        $this->revnum = $revnum;

        return $this;
    }

    /**
     * Status verifikasi revisi anggaran Dana Batch.
     *
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set status verifikasi revisi anggaran Dana Batch.
     *
     * @param string $status Status verifikasi revisi anggaran
     *
     * @return DanaBatchRevisiAnggaran
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get nama program yang menjadi sumber dana revisi Dana Batch.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set nama program yang menjadi sumber dana revisi Dana Batch.
     *
     * @param string $title Nama program
     *
     * @return DanaBatchRevisiAnggaran
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

}
