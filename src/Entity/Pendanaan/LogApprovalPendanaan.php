<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;

use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="log_aprv_pendanaan", indexes={
 *     @ORM\Index(name="log_aprv_pendanaan_x1", columns={"dg_id"}),
 *     @ORM\Index(name="log_aprv_pendanaan_x2", columns={"db_id"}),
 *     @ORM\Index(name="log_aprv_pendanaan_x3", columns={"dp_id"}),
 *     @ORM\Index(name="log_aprv_pendanaan_x4", columns={"bp_id"}),
 *     @ORM\Index(name="log_aprv_pendanaan_x5", columns={"posted_by"})
 * }, options={"comment": "Data riwayat verifikasi dan approval pendanaan"})
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\LogApprovalPendanaanRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\LogApprovalPendanaan")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 3:01
 */
class LogApprovalPendanaan
{

    use PostedByFkTrait, PostedDateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="log_id", type="bigint")
     * @Groups({"without_rel"})
     */
    private $logId;

    /**
     * @ORM\Column(name="kategori", type="smallint", options={
     *     "comment": "Kategori log pendanaan. Valid values:
    1 = Dana Global
    2 = Dana Bidang
    3 = Dana Program
    4 = Dana Batch"
     * })
     * @Groups({"without_rel"})
     */
    private $kategori;

    /**
     * @ORM\Column(name="log_action", type="string", length=50, options={
     *     "comment": "Tindakan yang dilakukan"
     * })
     * @Groups({"without_rel"})
     */
    private $logAction;

    /**
     * @ORM\Column(name="log_title", type="string", length=255)
     * @Groups({"without_rel"})
     */
    private $logTitle;

    /**
     * @ORM\Column(name="from_status", type="string", length=50, options={
     *     "comment": "Status pendanaan sebelum tindakan verifikasi ataupun approval"
     * })
     * @Groups({"without_rel"})
     */
    private $fromStatus;

    /**
     * @ORM\Column(name="to_status", type="string", length=50, options={
     *     "comment": "Status pendanaan setelah tindakan verifikasi ataupun approval"
     * })
     * @Groups({"without_rel"})
     */
    private $toStatus;

    /**
     * @ORM\Column(name="refcode", type="string", length=25, options={"comment": "Kode referensi transaksi"})
     * @Groups({"without_rel"})
     */
    private $refcode;

    /**
     * @ORM\Column(name="keterangan", type="text", nullable=true)
     * @Groups({"without_rel"})
     */
    private $keterangan;

    /**
     * @ORM\Column(name="jumlah", type="float", nullable=true)
     * @Groups({"without_rel"})
     */
    private $jumlah;

    /**
     * @ORM\Column(name="tahun_awal", type="integer", nullable=true)
     * @Groups({"without_rel"})
     */
    private $tahunAwal;

    /**
     * @ORM\Column(name="tahun_akhir", type="integer", nullable=true)
     * @Groups({"without_rel"})
     */
    private $tahunAkhir;

    /**
     * @var DanaGlobal
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaGlobal")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaGlobal", inversedBy="riwayatVerifikasi")
     * @ORM\JoinColumn(name="dg_id", referencedColumnName="dg_id", onDelete="CASCADE")
     * @Groups({"with_parent"})
     */
    private $danaGlobal;

    /**
     * @var DanaBidang
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBidang")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaBidang")
     * @ORM\JoinColumn(name="db_id", referencedColumnName="db_id", onDelete="CASCADE")
     */
    private $danaBidang;

    /**
     * @var DanaProgram
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaProgram")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaProgram")
     * @ORM\JoinColumn(name="dp_id", referencedColumnName="dp_id", onDelete="CASCADE")
     */
    private $danaProgram;

    /**
     * @var DanaBatch
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBatch")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaBatch")
     * @ORM\JoinColumn(name="bp_id", referencedColumnName="bp_id", onDelete="CASCADE")
     */
    private $danaBatch;


    /**
     * @return DanaBatch
     */
    public function getDanaBatch(): ?DanaBatch
    {
        return $this->danaBatch;
    }

    /**
     * @param DanaBatch $danaBatch
     *
     * @return LogApprovalPendanaan
     */
    public function setDanaBatch(?DanaBatch $danaBatch): self
    {
        $this->danaBatch = $danaBatch;

        return $this;
    }

    /**
     * @return DanaBidang
     */
    public function getDanaBidang(): ?DanaBidang
    {
        return $this->danaBidang;
    }

    /**
     * @param DanaBidang $danaBidang
     *
     * @return LogApprovalPendanaan
     */
    public function setDanaBidang(?DanaBidang $danaBidang): self
    {
        $this->danaBidang = $danaBidang;

        return $this;
    }

    /**
     * @return DanaGlobal
     */
    public function getDanaGlobal(): ?DanaGlobal
    {
        return $this->danaGlobal;
    }

    /**
     * @param DanaGlobal $danaGlobal
     *
     * @return LogApprovalPendanaan
     */
    public function setDanaGlobal(?DanaGlobal $danaGlobal): self
    {
        $this->danaGlobal = $danaGlobal;

        return $this;
    }

    /**
     * @return DanaProgram
     */
    public function getDanaProgram(): ?DanaProgram
    {
        return $this->danaProgram;
    }

    /**
     * @param DanaProgram $danaProgram
     *
     * @return LogApprovalPendanaan
     */
    public function setDanaProgram(?DanaProgram $danaProgram): self
    {
        $this->danaProgram = $danaProgram;

        return $this;
    }

    /**
     * Get status pendanaan sebelum tindakan verifikasi ataupun approval.
     *
     * @return string
     */
    public function getFromStatus(): ?string
    {
        return $this->fromStatus;
    }

    /**
     * Set status pendanaan sebelum tindakan verifikasi ataupun approval
     *
     * @param string $status Status pendanaan
     *
     * @return LogApprovalPendanaan
     */
    public function setFromStatus(string $status): self
    {
        $this->fromStatus = $status;

        return $this;
    }

    /**
     * Get nilai anggaran (dalam rupiah).
     *
     * @return float|null
     */
    public function getJumlah(): ?float
    {
        return $this->jumlah;
    }

    /**
     * Set nilai anggaran.
     *
     * @param float|null $jumlah Nilai anggaran (dalam rupiah)
     *
     * @return LogApprovalPendanaan
     */
    public function setJumlah(?float $jumlah): self
    {
        $this->jumlah = $jumlah;

        return $this;
    }

    /**
     * Get log kategori.
     *
     * @return int|null Salah satu dari nilai berikut:<br>
     *                  1 = Dana Global<br>
     *                  2 = Dana Bidang<br>
     *                  3 = Dana Program<br>
     *                  4 = Dana Batch
     */
    public function getKategori(): ?int
    {
        return $this->kategori;
    }

    /**
     * Set log kategori.
     *
     * @param int $kategori Log kategori, valid values: <br>
     *                      1 = Dana Global<br>
     *                      2 = Dana Bidang<br>
     *                      3 = Dana Program<br>
     *                      4 = Dana Batch
     *
     * @return LogApprovalPendanaan
     */
    public function setKategori(int $kategori): self
    {
        $this->kategori = $kategori;

        return $this;
    }

    /**
     * Keterangan tambahan jika REJECTED, VERIFIED ataupun APPROVED.
     *
     * @return string|null
     */
    public function getKeterangan(): ?string
    {
        return $this->keterangan;
    }

    /**
     * Keterangan tambahan jika REJECTED, VERIFIED ataupun APPROVED.
     *
     * @param string|null $keterangan
     *
     * @return LogApprovalPendanaan
     */
    public function setKeterangan(?string $keterangan): self
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    /**
     * Tindakan verifikasi ataupun approval yang dilakukan.
     *
     * @return string|null
     */
    public function getLogAction(): ?string
    {
        return $this->logAction;
    }

    /**
     * Tindakan verifikasi ataupun approval yang dilakukan.
     *
     * @param string $action Tindakan yang dilakukan
     *
     * @return LogApprovalPendanaan
     */
    public function setLogAction(string $action): self
    {
        $this->logAction = $action;

        return $this;
    }

    /**
     * Get Entity ID.
     *
     * @return int|null
     */
    public function getLogId(): ?int
    {
        return $this->logId;
    }

    /**
     * Get log title.
     *
     * @return string|null
     */
    public function getLogTitle(): ?string
    {
        return $this->logTitle;
    }

    /**
     * Set log title.
     *
     * @param string $title Log title
     *
     * @return LogApprovalPendanaan
     */
    public function setLogTitle(string $title): self
    {
        $this->logTitle = $title;

        return $this;
    }

    /**
     * Get kode referensi transaksi pendanaan.
     *
     * @return string|null
     */
    public function getRefcode(): ?string
    {
        return $this->refcode;
    }

    /**
     * Set kode referensi transaksi pendanaan.
     *
     * Contoh format:
     * <pre>
     * DG-yy.0000 => untuk transaksi Dana Global
     * DB-yy.0000 => untuk transaksi Dana Bidang
     * DP-yy.0000 => untuk transaksi Dana Program
     * BP-yy.0000 => untuk transaksi Dana Batch
     * </pre>
     *
     * @param string $refcode Kode referensi
     *
     * @return LogApprovalPendanaan
     */
    public function setRefcode(string $refcode): self
    {
        $this->refcode = $refcode;

        return $this;
    }

    /**
     * Get periode akhir pendanaan.
     *
     * @return int|null
     */
    public function getTahunAkhir(): ?int
    {
        return $this->tahunAkhir;
    }

    /**
     * Set periode akhir pendanaan.
     *
     * @param int|null $tahun
     *
     * @return LogApprovalPendanaan
     */
    public function setTahunAkhir(?int $tahun): self
    {
        $this->tahunAkhir = $tahun;

        return $this;
    }

    /**
     * Get periode awal pendanaan.
     *
     * @return int|null
     */
    public function getTahunAwal(): ?int
    {
        return $this->tahunAwal;
    }

    /**
     * Set periode awal pendanaan.
     *
     * @param int|null $tahun
     *
     * @return LogApprovalPendanaan
     */
    public function setTahunAwal(?int $tahun): self
    {
        $this->tahunAwal = $tahun;

        return $this;
    }

    /**
     * Get status pendanaan setelah tindakan verifikasi ataupun approval.
     *
     * @return string|null
     */
    public function getToStatus(): ?string
    {
        return $this->toStatus;
    }

    /**
     * Set status pendanaan setelah tindakan verifikasi ataupun approval.
     *
     * @param string $status Status pendanaan
     *
     * @return LogApprovalPendanaan
     */
    public function setToStatus(string $status): self
    {
        $this->toStatus = $status;

        return $this;
    }

}
