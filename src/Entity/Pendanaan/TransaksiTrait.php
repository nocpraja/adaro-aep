<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait TransaksiTrait
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 28/04/2019 14:57
 */
trait TransaksiTrait
{

    /**
     * @ORM\Column(name="refcode", type="string", length=25, options={
     *     "comment": "Kode referensi transaksi"
     * })
     * @Groups({"without_rel"})
     */
    private $refcode;

    /**
     * @ORM\Column(name="ts_name", type="string", length=255)
     * @Groups({"without_rel"})
     */
    private $title;

    /**
     * @ORM\Column(name="tahun_awal", type="integer", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahunAwal;

    /**
     * @ORM\Column(name="tahun_akhir", type="integer", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tahunAkhir;

    /**
     * @ORM\Column(name="jumlah", type="float")
     * @Groups({"without_rel"})
     */
    private $jumlah;

    /**
     * @ORM\Column(name="saldo", type="float")
     * @Groups({"without_rel"})
     */
    private $saldo;


    /**
     * Get nilai transaksi (dalam Rupiah).
     *
     * @return float|null
     */
    public function getJumlah(): ?float
    {
        return $this->jumlah;
    }

    /**
     * Set nilai transaksi.
     *
     * @param float $jumlah Nilai transaksi dalam rupiah
     *
     * @return self
     */
    public function setJumlah(float $jumlah): self
    {
        $this->jumlah = $jumlah;

        return $this;
    }

    /**
     * Get kode referensi transaksi.
     *
     * @return string|null
     */
    public function getRefcode(): ?string
    {
        return $this->refcode;
    }

    /**
     * Set kode referensi transaksi.
     *
     * Contoh format:
     * <pre>
     * DG-yy.0000 => untuk transaksi Dana Global
     * DB-yy.0000 => untuk transaksi Dana Bidang
     * DP-yy.0000 => untuk transaksi Dana Program
     * BP-yy.0000 => untuk transaksi Dana Batch
     * </pre>
     *
     * @param string $code Kode referensi
     *
     * @return self
     */
    public function setRefcode(string $code): self
    {
        $this->refcode = $code;

        return $this;
    }

    /**
     * Get jumlah saldo dana yang tersedia (dalam Rupiah).
     *
     * @return float|null
     */
    public function getSaldo(): ?float
    {
        return $this->saldo;
    }

    /**
     * Set jumlah saldo dana yang tersedia.
     *
     * @param float $saldo Jumlah saldo dalam Rupiah
     *
     * @return self
     */
    public function setSaldo(float $saldo): self
    {
        $this->saldo = $saldo;

        return $this;
    }

    /**
     * Get periode akhir pendanaan.
     *
     * @return int|null
     */
    public function getTahunAkhir(): ?int
    {
        return $this->tahunAkhir;
    }

    /**
     * Set periode akhir pendanaan.
     *
     * @param int $tahun Tahun akhir
     *
     * @return self
     */
    public function setTahunAkhir(?int $tahun): self
    {
        $this->tahunAkhir = $tahun;

        return $this;
    }

    /**
     * Get periode awal pendanaan.
     *
     * @return int|null
     */
    public function getTahunAwal(): ?int
    {
        return $this->tahunAwal;
    }

    /**
     * Set periode awal pendanaan.
     *
     * @param int $tahun Tahun awal
     *
     * @return self
     */
    public function setTahunAwal(?int $tahun): self
    {
        $this->tahunAwal = $tahun;

        return $this;
    }

    /**
     * Get transaction name.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set transaction name.
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

}