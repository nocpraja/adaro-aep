<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="ts_dana_bidang", indexes={
 *     @ORM\Index(name="ts_dana_bidang_x1", columns={"db_id"}),
 *     @ORM\Index(name="ts_dana_bidang_x2", columns={"posted_by"})
 * }, options={"comment": "Data riwayat transaksi Dana Bidang"})
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\TransaksiDanaBidangRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\TransaksiDanaBidang")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 3:02
 */
class TransaksiDanaBidang
{

    use TransaksiTrait, PostedByFkTrait, PostedDateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="tsid", type="bigint")
     * @Groups({"without_rel"})
     */
    private $tid;

    /**
     * @var DanaBidang
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBidang")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaBidang")
     * @ORM\JoinColumn(name="db_id", referencedColumnName="db_id", onDelete="CASCADE")
     */
    private $danaBidang;


    /**
     * Get Entity ID.
     *
     * @return int|null
     */
    public function getTid(): ?int
    {
        return $this->tid;
    }

    /**
     * @return DanaBidang
     */
    public function getDanaBidang(): ?DanaBidang
    {
        return $this->danaBidang;
    }

    /**
     * @param DanaBidang $danaBidang
     *
     * @return TransaksiDanaBidang
     */
    public function setDanaBidang(?DanaBidang $danaBidang): self
    {
        $this->danaBidang = $danaBidang;

        return $this;
    }

}
