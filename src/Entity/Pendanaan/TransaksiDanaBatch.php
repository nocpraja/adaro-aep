<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Entity\PostedByFkTrait;
use App\Entity\PostedDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="ts_dana_batch", indexes={
 *     @ORM\Index(name="ts_dana_batch_x1", columns={"bp_id"}),
 *     @ORM\Index(name="ts_dana_batch_x2", columns={"posted_by"})
 * }, options={"comment": "Data riwayat transaksi Dana Batch"})
 * @ORM\Entity(repositoryClass="App\Repository\Pendanaan\TransaksiDanaBatchRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\TransaksiDanaBatch")
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 25/05/2019 3:02
 */
class TransaksiDanaBatch
{

    use TransaksiTrait, PostedByFkTrait, PostedDateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="tsid", type="bigint")
     * @Groups({"without_rel"})
     */
    private $tid;

    /**
     * @var DanaBatch
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Pendanaan\DanaBatch")
     * @ORM\ManyToOne(targetEntity="App\Entity\Pendanaan\DanaBatch")
     * @ORM\JoinColumn(name="bp_id", referencedColumnName="bp_id", onDelete="CASCADE")
     */
    private $danaBatch;


    /**
     * Get entity ID.
     *
     * @return int|null
     */
    public function getTid(): ?int
    {
        return $this->tid;
    }

    /**
     * @return DanaBatch
     */
    public function getDanaBatch(): ?DanaBatch
    {
        return $this->danaBatch;
    }

    /**
     * @param DanaBatch $danaBatch
     *
     * @return TransaksiDanaBatch
     */
    public function setDanaBatch(?DanaBatch $danaBatch): self
    {
        $this->danaBatch = $danaBatch;

        return $this;
    }

}
