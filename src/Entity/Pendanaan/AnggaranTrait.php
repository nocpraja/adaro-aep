<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Entity\Pendanaan;


use App\Models\Pendanaan\Workflow\WorkflowTags;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait AnggaranTrait
 *
 * @package App\Entity\Pendanaan
 * @author  Ahmad Fajar
 * @since   22/04/2019, modified: 07/05/2019 23:00
 */
trait AnggaranTrait
{

    /**
     * @ORM\Column(name="status", type="string", length=50, options={
     *     "default": "NEW",
     *     "comment": "Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED, VERIFIED_1,
    APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED"
     * })
     * @Assert\NotBlank(message="Pendanaan.Status.NotBlank")
     * @Groups({"without_rel", "with_rel"})
     */
    private $status = WorkflowTags::NEW;

    /**
     * @ORM\Column(name="kode", type="string", length=25, options={
     *     "comment": "Kode untuk referensi Riwayat Transaksi. Format:
     * DG-yy.0000 => untuk transaksi Dana Global
     * DB-yy.0000 => untuk transaksi Dana Bidang
     * DP-yy.0000 => untuk transaksi Dana Program
     * PB-yy.0000 => untuk transaksi Dana Batch"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $kode;

    /**
     * @ORM\Column(name="jumlah", type="float")
     * @Groups({"without_rel", "with_rel"})
     */
    private $anggaran;

    /**
     * @ORM\Column(name="alokasi", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $alokasi = 0;

    /**
     * @ORM\Column(name="realisasi", type="float", nullable=true, options={"default": 0})
     * @Groups({"without_rel", "with_rel"})
     */
    private $realisasi = 0;

    /**
     * @ORM\Column(name="tags", type="json", nullable=true, options={"jsonb": true})
     * @Groups({"without_rel", "with_rel"})
     */
    private $tags = [];

    /**
     * @var float
     */
    private $newValue;


    /**
     * Get jumlah anggaran yang dialokasikan (dalam rupiah).
     *
     * @return float|null
     */
    public function getAlokasi(): ?float
    {
        return $this->alokasi;
    }

    /**
     * Set jumlah anggaran yang dialokasikan (dalam rupiah).
     *
     * @param float|null $alokasi Jumlah alokasi dana (dalam rupiah)
     *
     * @return self|EntityDanaInterface
     */
    public function setAlokasi(?float $alokasi): EntityDanaInterface
    {
        $this->alokasi = $alokasi;

        return $this;
    }

    /**
     * Get nilai anggaran (dalam rupiah).
     *
     * @return float|null
     */
    public function getAnggaran(): ?float
    {
        return $this->anggaran;
    }

    /**
     * Set nilai anggaran (dalam rupiah).
     *
     * @param float $anggaran Nilai anggaran (dalam rupiah)
     *
     * @return self|EntityDanaInterface
     */
    public function setAnggaran(float $anggaran): EntityDanaInterface
    {
        $this->anggaran = $anggaran;

        return $this;
    }

    /**
     * Jumlah dana yang tersedia setelah dikurangi dengan alokasi dana.
     *
     * @Groups({"without_rel", "with_rel"})
     *
     * @return float
     */
    public function getDanaTersedia(): float
    {
        return $this->anggaran - $this->alokasi;
    }

    /**
     * Get kode referensi untuk Riwayat Transaksi.
     *
     * @return string|null
     */
    public function getKode(): ?string
    {
        return $this->kode;
    }

    /**
     * Set kode referensi untuk Riwayat Transaksi.
     *
     * Contoh format:
     * <pre>
     * DG-yy.0000 => untuk transaksi Dana Global
     * DB-yy.0000 => untuk transaksi Dana Bidang
     * DP-yy.0000 => untuk transaksi Dana Program
     * BP-yy.0000 => untuk transaksi Dana Batch
     * </pre>
     *
     * @param string $kode Kode referensi
     *
     * @return self|EntityDanaInterface
     */
    public function setKode(string $kode): EntityDanaInterface
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Transient field untuk perubahan nilai anggaran.
     *
     * @return float
     */
    public function getNewValue(): ?float
    {
        return $this->newValue;
    }

    /**
     * Transient field untuk perubahan nilai anggaran.
     *
     * @param float $value Nilai anggaran (dalam rupiah)
     *
     * @return self
     */
    public function setNewValue(float $value): self
    {
        $this->newValue = $value;

        return $this;
    }

    /**
     * Get jumlah anggaran yang terpakai atau telah terealisasi (dalam rupiah).
     *
     * @return float|null
     */
    public function getRealisasi(): ?float
    {
        return $this->realisasi;
    }

    /**
     * Set jumlah anggaran yang terpakai atau telah terealisasi (dalam rupiah).
     *
     * @param float|null $realisasi Jumlah realisasi pemakaian dana (dalam rupiah)
     *
     * @return self|EntityDanaInterface
     */
    public function setRealisasi(?float $realisasi): EntityDanaInterface
    {
        $this->realisasi = $realisasi;

        return $this;
    }

    /**
     * Jumlah dana yang tersisa setelah dikurangi dengan realisasi.
     *
     * @Groups({"without_rel", "with_rel"})
     *
     * @return float
     */
    public function getSisaDana(): float
    {
        return ($this->status == WorkflowTags::CLOSED ? $this->anggaran - $this->realisasi : $this->anggaran);
    }

    /**
     * Get status or progress pendanaan.
     *
     * @return string|null Salah satu dari nilai berikut: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED,
     *                     REJECTED, VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set status or progress pendanaan.
     *
     * @param string $status Valid values: NEW, UPDATED, PLANNED, ONGOING, CLOSED, VERIFIED, APPROVED, REJECTED,
     *                       VERIFIED_1, APPROVED_1, APPROVED_2, APPROVED_3, REVISI, REVISED, REQUEST_REJECTED
     *
     * @return self|EntityDanaInterface
     */
    public function setStatus(string $status): EntityDanaInterface
    {
        $this->status = $status;

        return $this;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    /**
     * @inheritDoc
     *
     * @return self|EntityDanaInterface
     */
    public function setTags(?array $tags): EntityDanaInterface
    {
        $this->tags = $tags;

        return $this;
    }

}