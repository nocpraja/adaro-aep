<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use App\Entity\Security\UserAccount;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait PostedByFkTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   18/08/2018, modified: 25/05/2019 2:56
 */
trait PostedByFkTrait
{

    /**
     * @var UserAccount
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="posted_by", referencedColumnName="uid", nullable=true)
     * @Gedmo\Blameable(on="create")
     * @Groups({"with_rel", "with_postdate"})
     */
    private $postedBy;

    /**
     * Get the UserAccount who made the post.
     *
     * @return null|UserAccount
     */
    public function getPostedBy(): ?UserAccount
    {
        return $this->postedBy;
    }

    /**
     * Set the UserAccount who made the post.
     *
     * @param UserAccount $postedBy
     */
    public function setPostedBy(?UserAccount $postedBy): void
    {
        $this->postedBy = $postedBy;
    }

}