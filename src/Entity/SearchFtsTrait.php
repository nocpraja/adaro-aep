<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Trait SearchFtsTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   26/10/2018, modified: 29/10/2018 18:08
 */
trait SearchFtsTrait
{
    /**
     * @var array
     *
     * @ORM\Column(name="search_fts", type="tsvector", nullable=true)
     */
    private $searchFts;


    /**
     * Get FTS field values.
     *
     * @return string[]
     */
    public function getSearchFts(): ?array
    {
        return $this->searchFts;
    }

    /**
     * Set collection of text to be indexed with FullText feature.
     *
     * @param string[] $values
     */
    public function setSearchFts(?array $values): void
    {
        $this->searchFts = $values;
    }

}