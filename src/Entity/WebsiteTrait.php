<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait WebsiteTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   01/11/2018, modified: 01/11/2018 05:11
 */
trait WebsiteTrait
{

    /**
     * @ORM\Column(name="website", type="string", length=200, nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $website;


    /**
     * Get the website address url.
     *
     * @return null|string
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * Set the website address url.
     *
     * @param null|string $website The website address url.
     * @return self
     */
    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

}