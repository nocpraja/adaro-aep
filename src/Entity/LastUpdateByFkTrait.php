<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Entity;


use App\Entity\Security\UserAccount;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait UpdatedByFkTrait
 *
 * @package App\Entity
 * @author  Ahmad Fajar
 * @since   29/11/2018, modified: 25/05/2019 2:56
 */
trait LastUpdateByFkTrait
{
    /**
     * @var UserAccount
     *
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Security\UserAccount")
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="uid", nullable=true)
     * @Gedmo\Blameable(on="update")
     * @Groups({"with_rel", "with_postdate"})
     */
    private $updatedBy;

    /**
     * Get the UserAccount who made the update.
     *
     * @return null|UserAccount
     */
    public function getUpdatedBy(): ?UserAccount
    {
        return $this->updatedBy;
    }

    /**
     * Set the UserAccount who made the update.
     *
     * @param UserAccount $updatedBy
     */
    public function setUpdatedBy(?UserAccount $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

}