<?php


namespace App\Entity\Finalisasi;

use App\Entity\Kegiatan\Kegiatan;
use App\Entity\MasterData\Mitra;
use App\Entity\Security\UserAccount;
use App\Models\Finalisasi\Workflow\WorkflowTags;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="finalisasi", indexes={
 *     @ORM\Index(name="finalisasi_x1", columns={"kegiatan_id"}),
 *     @ORM\Index(name="finalisasi_x2", columns={"mitra_id"}),
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Finalisasi\FinalisasiRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="Finalisasi\Finalisasi")
 *
 * @package App\Entity\Finalisasi
 * @author  Mark Melvin
 * @since   06/08/2020, modified: 06/08/2020 02:23
 */
class Finalisasi
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="finalisasi_id", type="bigint")
     * @Groups({"without_rel", "with_rel"})
     */
    private $id;

    /**
     * @var Kegiatan
     * @ORM\ManyToOne(targetEntity="App\Entity\Kegiatan\Kegiatan")
     * @ORM\JoinColumn(name="kegiatan_id", referencedColumnName="kegiatan_id", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $kegiatan;


    /**
     * @var Mitra
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterData\Mitra")
     * @ORM\JoinColumn(name="mitra_id", referencedColumnName="mitra_id", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $mitra;


    /**
     * @ORM\Column(name="judul", type="string", nullable=false, length=250)
     * @Groups({"without_rel", "with_rel"})
     */
    private $judul;

    /**
     * @ORM\Column(name="latarbelakang", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $latarbelakang;

    /**
     * @ORM\Column(name="tujuanprogram", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $tujuanprogram;

    /**
     * @ORM\Column(name="outcome", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $outcome;

    /**
     * @ORM\Column(name="ruanglingkup", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $ruanglingkup;

    /**
     * @ORM\Column(name="pemangkukepentingan", type="text", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $pemangkukepentingan;

    /**
     * @ORM\Column(name="status", type="string", length=20, options={
     *     "default": "NEW",
     *     "comment": "Valid values: NEW"
     * })
     * @Groups({"without_rel", "with_rel"})
     */
    private $status = WorkflowTags::NEW;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $created_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $created_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="presubmitted_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $presubmitted_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="presubmitted_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $presubmitted_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="submitted_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $submitted_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="submitted_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $submitted_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="verified1_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $verified1_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="verified1_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $verified1_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="verified2_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $verified2_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="verified2_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $verified2_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="approved1_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $approved1_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="approved1_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $approved1_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="approved2_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $approved2_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="approved2_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $approved2_by;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="approved3_date", type="datetime", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $approved3_date;

    /**
     * @var UserAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Security\UserAccount")
     * @ORM\JoinColumn(name="approved3_by", referencedColumnName="uid", nullable=true)
     * @Groups({"without_rel", "with_rel"})
     */
    private $approved3_by;




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return ?Kegiatan
     */
    public function getKegiatan(): ?Kegiatan
    {
        return $this->kegiatan;
    }

    /**
     * @param ?Kegiatan $kegiatan
     */
    public function setKegiatan(?Kegiatan $kegiatan): void
    {
        $this->kegiatan = $kegiatan;
    }

    /**
     * @return ?Mitra
     */
    public function getMitra(): ?Mitra
    {
        return $this->mitra;
    }

    /**
     * @param ?Mitra $mitra
     */
    public function setMitra(?Mitra $mitra): void
    {
        $this->mitra = $mitra;
    }

    /**
     * @return mixed
     */
    public function getJudul()
    {
        return $this->judul;
    }

    /**
     * @param mixed $judul
     */
    public function setJudul($judul): void
    {
        $this->judul = $judul;
    }

    /**
     * @return mixed
     */
    public function getLatarbelakang()
    {
        return $this->latarbelakang;
    }

    /**
     * @param mixed $latarbelakang
     */
    public function setLatarbelakang($latarbelakang): void
    {
        $this->latarbelakang = $latarbelakang;
    }

    /**
     * @return mixed
     */
    public function getTujuanprogram()
    {
        return $this->tujuanprogram;
    }

    /**
     * @param mixed $tujuanprogram
     */
    public function setTujuanprogram($tujuanprogram): void
    {
        $this->tujuanprogram = $tujuanprogram;
    }

    /**
     * @return mixed
     */
    public function getOutcome()
    {
        return $this->outcome;
    }

    /**
     * @param mixed $outcome
     */
    public function setOutcome($outcome): void
    {
        $this->outcome = $outcome;
    }

    /**
     * @return mixed
     */
    public function getRuanglingkup()
    {
        return $this->ruanglingkup;
    }

    /**
     * @param mixed $ruanglingkup
     */
    public function setRuanglingkup($ruanglingkup): void
    {
        $this->ruanglingkup = $ruanglingkup;
    }

    /**
     * @return mixed
     */
    public function getPemangkukepentingan()
    {
        return $this->pemangkukepentingan;
    }

    /**
     * @param mixed $pemangkukepentingan
     */
    public function setPemangkukepentingan($pemangkukepentingan): void
    {
        $this->pemangkukepentingan = $pemangkukepentingan;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedDate(): ?DateTimeInterface
    {
        return $this->created_date;
    }

    /**
     * @param DateTimeInterface|null $created_date
     */
    public function setCreatedDate(?DateTimeInterface $created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return UserAccount|null
     */
    public function getCreatedBy(): ?UserAccount
    {
        return $this->created_by;
    }

    /**
     * @param UserAccount|null $created_by
     */
    public function setCreatedBy(?UserAccount $created_by): void
    {
        $this->created_by = $created_by;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPresubmittedDate(): ?DateTimeInterface
    {
        return $this->presubmitted_date;
    }

    /**
     * @param DateTimeInterface|null $presubmitted_date
     */
    public function setPresubmittedDate(?DateTimeInterface $presubmitted_date): void
    {
        $this->presubmitted_date = $presubmitted_date;
    }

    /**
     * @return UserAccount|null
     */
    public function getPresubmittedBy(): ?UserAccount
    {
        return $this->presubmitted_by;
    }

    /**
     * @param UserAccount|null $presubmitted_by
     */
    public function setPresubmittedBy(?UserAccount $presubmitted_by): void
    {
        $this->presubmitted_by = $presubmitted_by;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getSubmittedDate(): ?DateTimeInterface
    {
        return $this->submitted_date;
    }

    /**
     * @param DateTimeInterface|null $submitted_date
     */
    public function setSubmittedDate(?DateTimeInterface $submitted_date): void
    {
        $this->submitted_date = $submitted_date;
    }

    /**
     * @return UserAccount|null
     */
    public function getSubmittedBy(): ?UserAccount
    {
        return $this->submitted_by;
    }

    /**
     * @param UserAccount|null $submitted_by
     */
    public function setSubmittedBy(?UserAccount $submitted_by): void
    {
        $this->submitted_by = $submitted_by;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getVerified1Date(): ?DateTimeInterface
    {
        return $this->verified1_date;
    }

    /**
     * @param DateTimeInterface|null $verified1_date
     */
    public function setVerified1Date(?DateTimeInterface $verified1_date): void
    {
        $this->verified1_date = $verified1_date;
    }

    /**
     * @return UserAccount|null
     */
    public function getVerified1By(): ?UserAccount
    {
        return $this->verified1_by;
    }

    /**
     * @param UserAccount|null $verified1_by
     */
    public function setVerified1By(?UserAccount $verified1_by): void
    {
        $this->verified1_by = $verified1_by;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getVerified2Date(): ?DateTimeInterface
    {
        return $this->verified2_date;
    }

    /**
     * @param DateTimeInterface|null $verified2_date
     */
    public function setVerified2Date(?DateTimeInterface $verified2_date): void
    {
        $this->verified2_date = $verified2_date;
    }

    /**
     * @return UserAccount|null
     */
    public function getVerified2By(): ?UserAccount
    {
        return $this->verified2_by;
    }

    /**
     * @param UserAccount|null $verified2_by
     */
    public function setVerified2By(?UserAccount $verified2_by): void
    {
        $this->verified2_by = $verified2_by;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getApproved1Date(): ?DateTimeInterface
    {
        return $this->approved1_date;
    }

    /**
     * @param DateTimeInterface|null $approved1_date
     */
    public function setApproved1Date(?DateTimeInterface $approved1_date): void
    {
        $this->approved1_date = $approved1_date;
    }

    /**
     * @return UserAccount|null
     */
    public function getApproved1By(): ?UserAccount
    {
        return $this->approved1_by;
    }

    /**
     * @param UserAccount|null $approved1_by
     */
    public function setApproved1By(?UserAccount $approved1_by): void
    {
        $this->approved1_by = $approved1_by;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getApproved2Date(): ?DateTimeInterface
    {
        return $this->approved2_date;
    }

    /**
     * @param DateTimeInterface|null $approved2_date
     */
    public function setApproved2Date(?DateTimeInterface $approved2_date): void
    {
        $this->approved2_date = $approved2_date;
    }

    /**
     * @return UserAccount|null
     */
    public function getApproved2By(): ?UserAccount
    {
        return $this->approved2_by;
    }

    /**
     * @param UserAccount|null $approved2_by
     */
    public function setApproved2By(?UserAccount $approved2_by): void
    {
        $this->approved2_by = $approved2_by;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getApproved3Date(): ?DateTimeInterface
    {
        return $this->approved3_date;
    }

    /**
     * @param DateTimeInterface|null $approved3_date
     */
    public function setApproved3Date(?DateTimeInterface $approved3_date): void
    {
        $this->approved3_date = $approved3_date;
    }

    /**
     * @return UserAccount|null
     */
    public function getApproved3By(): ?UserAccount
    {
        return $this->approved3_by;
    }

    /**
     * @param UserAccount|null $approved3_by
     */
    public function setApproved3By(?UserAccount $approved3_by): void
    {
        $this->approved3_by = $approved3_by;
    }


}