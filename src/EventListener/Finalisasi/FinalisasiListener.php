<?php


namespace App\EventListener\Finalisasi;

use App\Component\Controller\JsonController;
use App\Component\Workflow\Event\EnteredEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Finalisasi\Finalisasi;
use App\EventListener\GetUserTrait;
use App\Models\Finalisasi\Workflow\WorkflowTags;
use App\Service\Logger\AuditLogger;
use DateTime;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FinalisasiListener implements EventSubscriberInterface
{
    use GetUserTrait;

    private $logger;

    /**
     * FinalisasiListener constructor.
     *
     * @param ContainerInterface $container
     * @param AuditLogger        $logger
     */
    public function __construct(ContainerInterface $container,
                                AuditLogger $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }
    /**
     * Returns an array of event names this subscriber wants to listen to.
     * The array keys are event names and the value can be:
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     * For instance:
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            sprintf('%s.finalisasi.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::PRESUBMITTED) => 'onWorkflowTransition',
            sprintf('%s.finalisasi.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::SUBMITTED) => 'onWorkflowTransition',
            sprintf('%s.finalisasi.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::VERIFIED1) => 'onWorkflowTransition',
            sprintf('%s.finalisasi.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::VERIFIED2) => 'onWorkflowTransition',
            sprintf('%s.finalisasi.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::APPROVED1) => 'onWorkflowTransition',
            sprintf('%s.finalisasi.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::APPROVED2) => 'onWorkflowTransition',
            sprintf('%s.finalisasi.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::APPROVED3) => 'onWorkflowTransition',
        ];
    }

    public function onWorkflowTransition(EnteredEvent $event): void
    {
        /** @var Finalisasi $subject */
        $subject = $event->getSubject();
        $em = $this->logger->getEntityManager();
        $action = $event->getTransition()->getName();
        $context = $event->getContext();

        dump($action);
        switch ($action) {
            case WorkflowTags::STEP_PRESUBMIT:
                $subject->setPresubmittedBy($this->getUserAccount());
                $subject->setPresubmittedDate(new DateTime());
                break;
            case WorkflowTags::STEP_SUBMIT:
                $subject->setSubmittedBy($this->getUserAccount());
                $subject->setSubmittedDate(new DateTime());
                break;
            case WorkflowTags::STEP_VERIFY1:
                $subject->setVerified1By($this->getUserAccount());
                $subject->setVerified1Date(new DateTime());
                break;
            case WorkflowTags::STEP_VERIFY2:
                $subject->setVerified2By($this->getUserAccount());
                $subject->setVerified2Date(new DateTime());
                break;
            case WorkflowTags::STEP_APPROVE1:
                $subject->setApproved1By($this->getUserAccount());
                $subject->setApproved1Date(new DateTime());
                break;
            case WorkflowTags::STEP_APPROVE2:
                $subject->setApproved2By($this->getUserAccount());
                $subject->setApproved2Date(new DateTime());
                break;
            case WorkflowTags::STEP_APPROVE3:
                $subject->setApproved3By($this->getUserAccount());
                $subject->setApproved3Date(new DateTime());
                break;
        }

        $em->persist($subject);
        $em->flush();
        $this->logger->info($action, Finalisasi::class,
            $subject, [JsonController::CONTEXT_NON_REL]);
    }
}