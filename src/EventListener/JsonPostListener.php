<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\EventListener;


use App\Service\Logger\AuditLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class JsonPostListener
 *
 * @package App\EventListener
 * @author  Ahmad Fajar
 * @since   03/08/2018, modified: 10/04/2019 18:13
 */
class JsonPostListener implements EventSubscriberInterface
{

    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * @var Request
     */
    private $request;


    /**
     * JsonBodyListener constructor.
     *
     * @param AuditLogger $logger Audit logger service
     */
    public function __construct(AuditLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Convert request body to array if applicable.
     *
     * @param FilterControllerEvent $event
     */
    public function convertJsonStringToArray(FilterControllerEvent $event): void
    {
        $this->request = $event->getRequest();
        $body = $this->request->getContent();

        if ($event->isMasterRequest() && $this->isMethodAllowed() &&
            $this->request->getContentType() === 'json' && $body) {
            $data = json_decode($body, true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                $errorMsg = 'Invalid json body: ' . json_last_error_msg();
                $this->logger->error($errorMsg);

                throw new BadRequestHttpException($errorMsg);
            }

            $this->request->request->replace(is_array($data) ? $data : []);
        }
    }

    private function isMethodAllowed(): bool
    {
        return in_array($this->request->getMethod(), ['POST', 'PUT', 'PATCH', 'DELETE']);
    }

    /**
     * @inheritdoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            KernelEvents::CONTROLLER => 'convertJsonStringToArray',
        );
    }

}