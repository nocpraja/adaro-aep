<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\EventListener;


use App\Service\Annotation\UserAgent\UserAgentListener;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class UserAgentSubscriber
 *
 * @package App\EventListener
 * @author  Ahmad Fajar
 * @since   19/08/2018, modified: 10/04/2019 18:05
 */
class UserAgentSubscriber implements EventSubscriberInterface
{

    /**
     * @var UserAgentListener
     */
    private $annotationListener;


    /**
     * ClientUserAgentListener constructor.
     *
     * @param UserAgentListener $annotationListener
     */
    public function __construct(UserAgentListener $annotationListener)
    {
        $this->annotationListener = $annotationListener;
    }

    /**
     * Listen to kernel events and sets client User Agent name for future used.
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event): void
    {
        $request = $event->getRequest();

        if (null !== $request && $event->isMasterRequest()) {
            $userAgent = $request->headers->get('User-Agent');

            if (!empty($userAgent)) {
                $this->annotationListener->setUserAgent($userAgent);
            }
        }
    }

    /**
     * @inheritdoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => 'onKernelRequest',
        );
    }

}