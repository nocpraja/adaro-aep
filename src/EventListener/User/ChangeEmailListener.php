<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\User;


use App\Component\Controller\JsonController;
use App\Entity\Security\UserAccount;
use App\Event\User\RequestChangeEmailEvent;
use App\Repository\Security\UserAccountRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Mail\SimpleMailer;
use DateInterval;
use DateTime;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ChangeEmailListener
 *
 * @package App\EventListener\User
 * @author  Ahmad Fajar
 * @since   2019-04-18, modified: 2019-04-18 14:13
 */
final class ChangeEmailListener implements EventSubscriberInterface
{

    use EmailTokenTrait;

    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * @var SimpleMailer
     */
    private $mailer;


    /**
     * ChangeEmailListener constructor.
     *
     * @param AuditLogger           $logger     Audit logger service
     * @param UserAccountRepository $repository Entity repository instance
     * @param SimpleMailer          $mailer     Simple Mailer service
     */
    public function __construct(AuditLogger $logger, UserAccountRepository $repository, SimpleMailer $mailer)
    {
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->repository = $repository;
    }

    /**
     * Process pengiriman email KONFIRMASI perubahan email ke alamat email baru.
     *
     * @param RequestChangeEmailEvent $event
     *
     * @throws Exception
     */
    public function onConfirmChangeEmail(RequestChangeEmailEvent $event): void
    {
        $user = $event->getUserAccount();
        $token = $this->getToken($user, RequestChangeEmailEvent::CONFIRM);
        $duration = $this->getTokenDuration();
        $expiry = new DateTime();
        $expiry = $expiry->add(new DateInterval('PT' . $duration . 'M'));
        $user->setTokenKey($token->getId())
             ->setTokenExpiry($expiry);

        $this->logger->notice(sprintf('KONFIRMASI perubahan alamat email dengan token: %s.', $token->getId()),
                              UserAccount::class, $user, [JsonController::CONTEXT_NON_REL]);
        $this->mailer->setSubject('Konfirmasi perubahan alamat email')
                     ->setRecipients([$user->getNewEmail() => $user->getFullName()])
                     ->setTemplate('emails/change_email/confirm.html.twig')
                     ->setContext(['user'       => $user,
                                   'token'      => $token,
                                   'duration'   => $duration,
                                   'page_title' => 'Konfirmasi perubahan alamat email'])
                     ->sendHtmlEmail();
    }

    /**
     * Process pengiriman email REQUEST penggantian alamat email ke alamat email lama.
     *
     * @param RequestChangeEmailEvent $event
     *
     * @throws Exception
     */
    public function onRequestChangeEmail(RequestChangeEmailEvent $event): void
    {
        $user = $event->getUserAccount();
        $token = $this->getToken($user, RequestChangeEmailEvent::REQUEST);
        $duration = $this->getTokenDuration();
        $expiry = new DateTime();
        $expiry = $expiry->add(new DateInterval('PT' . $duration . 'M'));
        $user->setTokenKey($token->getId())
             ->setTokenExpiry($expiry);

        $this->logger->notice(sprintf('REQUEST penggantian alamat email dengan token: %s.', $token->getId()),
                              UserAccount::class, $user, [JsonController::CONTEXT_NON_REL]);
        $this->mailer->setSubject('Konfirmasi perubahan alamat email')
                     ->setRecipients([$user->getEmail() => $user->getFullName()])
                     ->setTemplate('emails/change_email/request.html.twig')
                     ->setContext(['user'       => $user,
                                   'token'      => $token,
                                   'duration'   => $duration,
                                   'page_title' => 'Konfirmasi perubahan alamat email'])
                     ->sendHtmlEmail();
    }

    /**
     * @inheritDoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            RequestChangeEmailEvent::REQUEST => 'onRequestChangeEmail',
            RequestChangeEmailEvent::CONFIRM => 'onConfirmChangeEmail'
        ];
    }

}