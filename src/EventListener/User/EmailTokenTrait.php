<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\User;


use App\Component\Util\TokenGenerator;
use App\Entity\Security\UserAccount;
use App\Repository\Security\UserAccountRepository;
use Symfony\Component\Security\Csrf\CsrfToken;

/**
 * Trait EmailTokenTrait
 *
 * @package App\EventListener\User
 * @author  Ahmad Fajar
 * @since   2019-04-18, modified: 2019-04-18 14:32
 */
trait EmailTokenTrait
{

    /**
     * @var UserAccountRepository
     */
    private $repository;


    /**
     * Gets doctrine entity repository instance.
     *
     * @return UserAccountRepository
     */
    public function getRepository(): UserAccountRepository
    {
        return $this->repository;
    }

    /**
     * Generate Token dan menjamin tidak terjadi duplikasi pada database.
     *
     * @param UserAccount $user      Persistent entity object
     * @param string      $eventName Nama event yang melakukan request generate Token
     *
     * @return CsrfToken
     */
    protected function getToken(UserAccount $user, string $eventName = ''): CsrfToken
    {
        $generator = new TokenGenerator();
        $token = $generator->generateToken($user->getUsername() . $eventName);
        $check = $this->repository->findOneBy(['tokenKey' => $token->getId()]);

        // prevents duplicate token in the database
        while (!is_null($check)) {
            $token = $generator->generateToken($user->getUsername());
            $check = $this->repository->findOneBy(['tokenKey' => $token->getId()]);
        }

        return $token;
    }

    protected function getTokenDuration(): int
    {
        // TODO: Ambil durasi token expire dari konfigurasi di database
        return 30;
    }

}