<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\User;


use App\Component\Controller\JsonController;
use App\Entity\Security\UserAccount;
use App\Event\User\ForgotPasswordEvent;
use App\Repository\Security\UserAccountRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Mail\SimpleMailer;
use DateInterval;
use DateTime;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ForgotPasswordListener
 *
 * @package App\EventListener\User
 * @author  Ahmad Fajar
 * @since   2019-04-15, modified: 2019-04-18 14:32
 */
final class ForgotPasswordListener implements EventSubscriberInterface
{

    use EmailTokenTrait;

    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * @var SimpleMailer
     */
    private $mailer;


    /**
     * ForgotPasswordListener constructor.
     *
     * @param AuditLogger           $auditLogger Audit logger service
     * @param UserAccountRepository $repository  Entity repository instance
     * @param SimpleMailer          $mailer      Simple Mailer service
     */
    public function __construct(AuditLogger $auditLogger, UserAccountRepository $repository, SimpleMailer $mailer)
    {
        $this->mailer = $mailer;
        $this->logger = $auditLogger;
        $this->repository = $repository;
    }

    /**
     * Process interested event and send related email.
     *
     * @param ForgotPasswordEvent $event The event data
     *
     * @throws Exception
     */
    public function sendEmailForgotPassword(ForgotPasswordEvent $event): void
    {
        $user = $event->getUserAccount();
        $token = $this->getToken($user);
        $duration = $this->getTokenDuration();
        $expiry = new DateTime();
        $expiry = $expiry->add(new DateInterval('PT' . $duration . 'M'));
        $user->setTokenKey($token->getId())
             ->setTokenExpiry($expiry);

        $this->logger->notice(sprintf('REQUEST reset password dengan token: %s.', $token->getId()),
                              UserAccount::class, $user, [JsonController::CONTEXT_NON_REL]);
        $this->mailer->setSubject('Perubahan kata sandi aplikasi Adaro CSRMS')
                     ->setRecipients([$user->getEmail() => $user->getFullName()])
                     ->setTemplate('emails/forgot_password/email.html.twig')
                     ->setContext(['user'       => $user,
                                   'token'      => $token,
                                   'duration'   => $duration,
                                   'page_title' => 'Perubahan kata sandi aplikasi Adaro CSRMS'])
                     ->sendHtmlEmail();
    }

    /**
     * @inheritDoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [ForgotPasswordEvent::NAME => 'sendEmailForgotPassword'];
    }

}