<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\User;


use App\Repository\Security\UserAccountRepository;
use App\Service\Logger\AuditLogger;
use App\Service\Security\AppUserInterface;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class UserVisitedListener is used to track visited page.
 *
 * @package App\EventListener
 * @author  Ahmad Fajar
 * @since   10/04/2019, modified: 2019-04-15 01:47
 */
final class UserVisitedListener implements EventSubscriberInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * @var UserAccountRepository
     */
    private $repository;


    /**
     * UserVisitedSubscriber constructor.
     *
     * @param ContainerInterface    $container   Symfony service container
     * @param AuditLogger           $auditLogger Audit logger service
     * @param UserAccountRepository $repository  Entity repository instance
     */
    public function __construct(ContainerInterface $container, AuditLogger $auditLogger,
                                UserAccountRepository $repository)
    {
        $this->logger = $auditLogger;
        $this->container = $container;
        $this->repository = $repository;
    }

    /**
     * Listen to kernel terminate events and do some interesting works.
     */
    public function onPageVisited(): void
    {
        if (!$this->container->has('security.token_storage')) {
            return;
        }
        if (null === ($token = $this->container->get('security.token_storage')->getToken())) {
            return;
        }
        if (is_object($user = $token->getUser()) && $user instanceof AppUserInterface) {
            $this->updateLastVisited($user);
        }
    }

    /**
     * Record last visited timestamp.
     *
     * @param AppUserInterface $user Current user logged in
     */
    private function updateLastVisited(AppUserInterface $user): void
    {
        try {
            $userAccount = $this->repository->findByUsername($user->getUsername());

            if (is_null($userAccount->getLastVisited())) {
                $userAccount->setLastVisited(new DateTime());
                $this->logger->getEntityManager()->flush();
            } else {
                $d1 = DateTimeImmutable::createFromMutable($userAccount->getLastVisited());
                $d2 = new DateTimeImmutable();

                if ($d2 > $d1->add(new DateInterval('PT1M'))) {
                    $userAccount->setLastVisited(new DateTime());
                    $this->logger->getEntityManager()->flush();
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            KernelEvents::TERMINATE => 'onPageVisited',
        );
    }

}