<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\Pendanaan;


use App\Component\Workflow\Event\EnterEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\DanaBidang;
use App\Entity\Pendanaan\DanaGlobal;
use App\Entity\Pendanaan\DanaProgram;
use App\EventListener\GetUserTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use DateTime;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UpdateListener
 *
 * @package App\EventListener\Pendanaan
 * @author  Ahmad Fajar
 * @since   28/04/2019, modified: 14/05/2019 2:40
 */
final class UpdateListener implements EventSubscriberInterface
{

    use GetUserTrait;


    /**
     * UpdateListener constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get the User account who made the update and store its data into entity object.
     *
     * @param EnterEvent $event The event object
     *
     * @throws Exception
     */
    public function onPendanaanUpdated(EnterEvent $event): void
    {
        /** @var DanaGlobal|DanaBidang|DanaProgram|DanaBatch $subject */
        $subject = $event->getSubject();
        $subject->setUpdatedBy($this->getUserAccount())
                ->setLastUpdated(new DateTime());
    }

    /**
     * @inheritDoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            sprintf('%s.dana_global.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTER, WorkflowTags::UPDATED) => 'onPendanaanUpdated',

            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTER, WorkflowTags::UPDATED) => 'onPendanaanUpdated',
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTER, WorkflowTags::REVISI)  => 'onPendanaanUpdated',

            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTER, WorkflowTags::UPDATED) => 'onPendanaanUpdated',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTER, WorkflowTags::REVISI)  => 'onPendanaanUpdated',

            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTER, WorkflowTags::UPDATED) => 'onPendanaanUpdated',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTER, WorkflowTags::REVISI)  => 'onPendanaanUpdated',
        ];
    }

}