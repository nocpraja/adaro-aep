<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\Workflow\Event\EnteredEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\DanaBidang;
use App\Entity\Pendanaan\DanaProgram;
use App\Entity\Pendanaan\LogApprovalPendanaan;
use App\EventListener\GetUserTrait;
use App\Models\Pendanaan\DanaBatchModel;
use App\Models\Pendanaan\DanaBidangModel;
use App\Models\Pendanaan\DanaGlobalModel;
use App\Models\Pendanaan\DanaProgramModel;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Service\Logger\AuditLogger;
use DateTime;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ClosingListener
 *
 * @package App\EventListener\Pendanaan
 * @author  Ahmad Fajar
 * @since   02/05/2019, modified: 08/05/2019 23:25
 */
final class ClosingListener implements EventSubscriberInterface
{

    use GetUserTrait;

    /**
     * @var DanaGlobalModel
     */
    private $danaGlobalModel;

    /**
     * @var DanaBidangModel
     */
    private $bidangModel;

    /**
     * @var DanaProgramModel
     */
    private $programModel;

    /**
     * @var DanaBatchModel
     */
    private $batchModel;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * ClosingListener constructor.
     *
     * @param ContainerInterface $container
     * @param DanaBatchModel     $batchModel
     */
    public function __construct(ContainerInterface $container, DanaBatchModel $batchModel)
    {
        $this->container = $container;
        $this->batchModel = $batchModel;
        $this->programModel = $batchModel->getProgramModel();
        $this->bidangModel = $this->programModel->getBidangModel();
        $this->danaGlobalModel = $this->bidangModel->getDanaGlobalModel();
        $this->logger = $this->danaGlobalModel->getLogger();
    }

    /**
     * Listen to workflow event closing Dana Batch, dan proses transaksi closing Dana Batch.
     *
     * @param EnteredEvent $event Event object
     *
     * @throws ORMException
     * @throws Exception
     */
    public function onDanaBatchClosed(EnteredEvent $event): void
    {
        /** @var DanaBatch $subject */
        $subject = $event->getSubject();
        $context = $event->getContext();
        $selisih = $this->prepareClosing($subject);
        $title = 'CLOSING ' . $subject->getNamaProgram();
        $danaProgram = $subject->getDanaProgram();
        $realisasi = $danaProgram->getRealisasi();
        $danaProgram->setRealisasi($realisasi + $subject->getRealisasi());

        $this->logger->getEntityManager()->flush();
        $this->batchModel->appendTransaction($subject, ($selisih * -1), $subject->getKode(), $title);
        $this->programModel->appendTransaction($danaProgram, $selisih, $subject->getKode(), $title, true);
        $this->logClosingAnggaran($subject, $selisih, $context);
    }

    /**
     * Listen to workflow event closing Dana Bidang, dan proses transaksi closing Dana Bidang.
     *
     * @param EnteredEvent $event Event object
     *
     * @throws ORMException
     * @throws Exception
     */
    public function onDanaBidangClosed(EnteredEvent $event): void
    {
        /** @var DanaBidang $subject */
        $subject = $event->getSubject();
        $context = $event->getContext();
        $selisih = $this->prepareClosing($subject);
        $title = 'CLOSING ' . $subject->getTitle();

        $this->logger->getEntityManager()->flush();
        $this->bidangModel->appendTransaction($subject, ($selisih * -1), $subject->getKode(), $title);
        $this->danaGlobalModel->appendTransaction($selisih, $subject->getKode(), $title);
        $this->logClosingAnggaran($subject, $selisih, $context);
    }

    /**
     * Listen to workflow event closing Dana Program, dan proses transaksi closing Dana Program.
     *
     * @param EnteredEvent $event Event object
     *
     * @throws ORMException
     * @throws Exception
     */
    public function onDanaProgramClosed(EnteredEvent $event): void
    {
        /** @var DanaProgram $subject */
        $subject = $event->getSubject();
        $context = $event->getContext();
        $selisih = $this->prepareClosing($subject);
        $title = 'CLOSING ' . $subject->getTitle();
        $danaBidang = $subject->getDanaBidang();
        $realisasi = $danaBidang->getRealisasi();
        $danaBidang->setRealisasi($realisasi + $subject->getRealisasi());

        $this->logger->getEntityManager()->flush();
        $this->programModel->appendTransaction($subject, ($selisih * -1), $subject->getKode(), $title);
        $this->bidangModel->appendTransaction($danaBidang, $selisih, $subject->getKode(), $title, true);
        $this->logClosingAnggaran($subject, $selisih, $context);
    }

    /**
     * Logs transaksi closing Dana Bidang, Program ataupun Batch.
     *
     * @param object $subject Persistent entity object
     * @param float  $jumlah  Sisa anggaran setelah dikurangi realisasi (dalam rupiah)
     * @param array  $context Additional context to apply
     */
    private function logClosingAnggaran(object $subject, float $jumlah, array $context = []): void
    {
        $em = $this->logger->getEntityManager();
        $message = 'CLOSING ' . $subject->getTitle() . ', nomor ' . $subject->getKode();

        $entity = new LogApprovalPendanaan();
        $entity->setLogAction(WorkflowTags::STEP_CLOSING)
               ->setRefcode($subject->getKode())
               ->setJumlah($jumlah)
               ->setFromStatus($context['status'] ?? WorkflowTags::ONGOING)
               ->setToStatus($subject->getStatus());

        if ($subject instanceof DanaBidang) {
            $entity->setKategori(WorkflowTags::DANA_BIDANG)
                   ->setLogTitle($subject->getTitle())
                   ->setTahunAwal($subject->getTahunAwal())
                   ->setTahunAkhir($subject->getTahunAkhir())
                   ->setDanaBidang($subject);
        } elseif ($subject instanceof DanaProgram) {
            $entity->setKategori(WorkflowTags::DANA_PROGRAM)
                   ->setLogTitle($subject->getTitle())
                   ->setTahunAwal($subject->getTahunAwal())
                   ->setTahunAkhir($subject->getTahunAkhir())
                   ->setDanaProgram($subject);
        } elseif ($subject instanceof DanaBatch) {
            $entity->setKategori(WorkflowTags::DANA_BATCH)
                   ->setLogTitle($subject->getNamaProgram())
                   ->setTahunAwal($subject->getTahun())
                   ->setDanaBatch($subject);
        }

        $em->persist($entity);
        $em->flush();

        $this->logger->info($this->logger->formatMessage($message), LogApprovalPendanaan::class,
                            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * Prepare closing Dana Bidang, Program ataupun Batch.
     *
     * @param DanaBidang|DanaProgram|DanaBatch|object $subject Persistent entity object
     *
     * @return float Sisa anggaran setelah dikurangi realisasi (dalam rupiah)
     * @throws Exception
     */
    private function prepareClosing(object $subject): float
    {
        $em = $this->logger->getEntityManager();

        if (empty($subject->getRealisasi())) {
            $subject->setRealisasi($subject->getAlokasi());
            $selisih = $subject->getAnggaran() - $subject->getAlokasi();
        } else {
            $selisih = $subject->getAnggaran() - $subject->getRealisasi();
        }

        $subject->setClosedBy($this->getUserAccount())
                ->setTanggalClosed(new DateTime())
                ->setAnggaran($subject->getRealisasi());
        $em->persist($subject);

        return $selisih;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::CLOSED) => 'onDanaBidangClosed',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::CLOSED) => 'onDanaProgramClosed',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::CLOSED) => 'onDanaBatchClosed',
        ];
    }

}