<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\DataObject\SortOrFilter;
use App\Component\Doctrine\ORM\QueryExpressionHelper;
use App\Component\Workflow\Event\EnteredEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\DanaBatchRevisiAnggaran;
use App\Entity\Pendanaan\DanaBidang;
use App\Entity\Pendanaan\DanaProgram;
use App\Entity\Pendanaan\LogApprovalPendanaan;
use App\EventListener\GetUserTrait;
use App\Models\Pendanaan\DanaBatchModel;
use App\Models\Pendanaan\DanaBidangModel;
use App\Models\Pendanaan\DanaGlobalModel;
use App\Models\Pendanaan\DanaProgramModel;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Service\Logger\AuditLogger;
use DateTime;
use Doctrine\ORM\ORMException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class RejectRequestListener
 *
 * @package App\EventListener\Pendanaan
 * @author  Ahmad Fajar
 * @since   04/05/2019, modified: 14/05/2019 3:42
 */
class RejectRequestListener implements EventSubscriberInterface
{

    use GetUserTrait;

    /**
     * @var DanaGlobalModel
     */
    private $danaGlobalModel;

    /**
     * @var DanaBidangModel
     */
    private $bidangModel;

    /**
     * @var DanaProgramModel
     */
    private $programModel;

    /**
     * @var DanaBatchModel
     */
    private $batchModel;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * RejectRequestListener constructor.
     *
     * @param ContainerInterface $container
     * @param DanaBatchModel     $batchModel
     */
    public function __construct(ContainerInterface $container, DanaBatchModel $batchModel)
    {
        $this->container = $container;
        $this->batchModel = $batchModel;
        $this->programModel = $batchModel->getProgramModel();
        $this->bidangModel = $this->programModel->getBidangModel();
        $this->danaGlobalModel = $this->bidangModel->getDanaGlobalModel();
        $this->logger = $this->danaGlobalModel->getLogger();
    }

    /**
     * Listen to workflow event reject revisi Dana Batch, dan proses transaksi reject revisi Dana Batch.
     *
     * @param EnteredEvent $event Event object
     *
     * @throws ORMException
     */
    public function onDanaBatchRevisiRejected(EnteredEvent $event): void
    {
        /** @var DanaBatch $subject */
        $subject = $event->getSubject();
        $context = $event->getContext();
        $em = $this->logger->getEntityManager();

        $subject->setUpdatedBy($this->getUserAccount())
                ->setLastUpdated(new DateTime());
        $em->flush();

        $filters = $this->createFilters($subject);
        $transaksi = $this->batchModel->lastTransaction($subject, $filters);

        if (!is_null($transaksi)) {
            $title = 'REJECT REVISI ' . $subject->getNamaProgram();
            $jumlah = $transaksi->getJumlah();
            $anggaran = $subject->getAnggaran();

            $subject->setAnggaran($anggaran + ($jumlah * -1));
            $em->flush();
            $this->batchModel->appendTransaction($subject, ($jumlah * -1), $subject->getKode(), $title);

            // Proses reject revisi anggaran
            $tags = $subject->getTags();
            $requests = $subject->getRevisiAnggarans()->filter(function ($revisi) use ($tags) {
                /** @var DanaBatchRevisiAnggaran $revisi */
                return $revisi->getRevnum() == $tags['revnum'];
            });

            /** @var DanaBatchRevisiAnggaran $request */
            foreach ($requests as $request) {
                $request->setStatus(WorkflowTags::REJECTED);
                $em->persist($request);

                if (substr($request->getRefcode(), 0, 2) == 'DP') {
                    $this->programModel->appendTransaction($subject->getDanaProgram(), $request->getJumlah(),
                                                           $subject->getKode(), $title, true);
                } else {
                    $danaBatch = $this->batchModel->getRepository()->findOneBy(['kode' => $request->getRefcode()]);
                    $this->batchModel->appendTransaction($danaBatch, $request->getJumlah(),
                                                         $subject->getKode(), $title, true);
                }
            }

            $this->logRejectRequest($subject, $jumlah, $title, $context);
        }
    }

    /**
     * Listen to workflow event reject revisi Dana Bidang, dan proses transaksi reject revisi Dana Bidang.
     *
     * @param EnteredEvent $event Event object
     *
     * @throws ORMException
     */
    public function onDanaBidangRevisiRejected(EnteredEvent $event): void
    {
        /** @var DanaBidang $subject */
        $subject = $event->getSubject();
        $context = $event->getContext();
        $em = $this->logger->getEntityManager();

        $subject->setUpdatedBy($this->getUserAccount())
                ->setLastUpdated(new DateTime());
        $em->flush();

        $filters = $this->createFilters($subject);
        $transaksi = $this->bidangModel->lastTransaction($subject, $filters);

        if (!is_null($transaksi)) {
            $title = 'REJECT REVISI ' . $subject->getTitle();
            $jumlah = $transaksi->getJumlah();
            $anggaran = $subject->getAnggaran();
            $subject->setAnggaran($anggaran + ($jumlah * -1));
            $em->flush();

            $this->bidangModel->appendTransaction($subject, ($jumlah * -1), $subject->getKode(), $title);
            $this->danaGlobalModel->appendTransaction($jumlah, $subject->getKode(), $title);
            $this->logRejectRequest($subject, $jumlah, $title, $context);
        }
    }

    /**
     * Listen to workflow event reject revisi Dana Program, dan proses transaksi reject revisi Dana Program.
     *
     * @param EnteredEvent $event Event object
     *
     * @throws ORMException
     */
    public function onDanaProgramRevisiRejected(EnteredEvent $event): void
    {
        /** @var DanaProgram $subject */
        $subject = $event->getSubject();
        $context = $event->getContext();
        $em = $this->logger->getEntityManager();

        $subject->setUpdatedBy($this->getUserAccount())
                ->setLastUpdated(new DateTime())
                ->setTags(null);
        $em->flush();

        $filters = $this->createFilters($subject);
        $transaksi = $this->programModel->lastTransaction($subject, $filters);

        if (!is_null($transaksi)) {
            $title = 'REJECT REVISI ' . $subject->getTitle();
            $jumlah = $transaksi->getJumlah();
            $anggaran = $subject->getAnggaran();
            $subject->setAnggaran($anggaran + ($jumlah * -1));
            $em->flush();

            $this->programModel->appendTransaction($subject, ($jumlah * -1), $subject->getKode(), $title);
            $this->bidangModel->appendTransaction($subject->getDanaBidang(), $jumlah,
                                                  $subject->getKode(), $title, true);
            $this->logRejectRequest($subject, $jumlah, $title, $context);
        }
    }

    /**
     * Create filters to find last transaction.
     *
     * @param object $subject Persistent entity object of DanaBidang, DanaProgram or DanaBatch
     *
     * @return SortOrFilter[]
     */
    private function createFilters(object $subject): array
    {
        return QueryExpressionHelper::createCriterias(
            [
                ['property' => 'refcode', 'value' => $subject->getKode(), 'operator' => 'eq'],
                ['property' => 'title', 'value' => 'REQUEST REVISI', 'operator' => 'startwith'],
            ]
        );
    }

    /**
     * Logs transaksi closing Dana Bidang, Program ataupun Batch.
     *
     * @param object $subject Persistent entity object
     * @param float  $jumlah  Nilai revisi anggaran yang direject (dalam rupiah)
     * @param string $message Audit log message
     * @param array  $context Additional context to apply
     */
    private function logRejectRequest(object $subject, float $jumlah, string $message, array $context = []): void
    {
        $em = $this->logger->getEntityManager();

        $entity = new LogApprovalPendanaan();
        $entity->setLogAction(WorkflowTags::STEP_REJECT_REQUEST)
               ->setRefcode($subject->getKode())
               ->setJumlah($jumlah)
               ->setFromStatus($context['status'] ?? WorkflowTags::REVISI)
               ->setToStatus($subject->getStatus());

        if ($subject instanceof DanaBidang) {
            $entity->setKategori(WorkflowTags::DANA_BIDANG)
                   ->setLogTitle($subject->getTitle())
                   ->setTahunAwal($subject->getTahunAwal())
                   ->setTahunAkhir($subject->getTahunAkhir())
                   ->setDanaBidang($subject);
        } elseif ($subject instanceof DanaProgram) {
            $entity->setKategori(WorkflowTags::DANA_PROGRAM)
                   ->setLogTitle($subject->getTitle())
                   ->setTahunAwal($subject->getTahunAwal())
                   ->setTahunAkhir($subject->getTahunAkhir())
                   ->setDanaProgram($subject);
        } elseif ($subject instanceof DanaBatch) {
            $entity->setKategori(WorkflowTags::DANA_BATCH)
                   ->setLogTitle($subject->getNamaProgram())
                   ->setTahunAwal($subject->getTahun())
                   ->setDanaBatch($subject);
        }

        $em->persist($entity);
        $em->flush();

        $this->logger->info($this->logger->formatMessage($message), LogApprovalPendanaan::class,
                            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REVISI_REJECTED) => 'onDanaBidangRevisiRejected',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REVISI_REJECTED) => 'onDanaProgramRevisiRejected',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REVISI_REJECTED) => 'onDanaBatchRevisiRejected',
        ];
    }

}