<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\Pendanaan;


use App\Component\Controller\JsonController;
use App\Component\Workflow\Event\EnteredEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Pendanaan\DanaBatch;
use App\Entity\Pendanaan\DanaBatchRevisiAnggaran;
use App\Entity\Pendanaan\DanaBidang;
use App\Entity\Pendanaan\DanaGlobal;
use App\Entity\Pendanaan\DanaProgram;
use App\Entity\Pendanaan\EntityDanaInterface;
use App\Entity\Pendanaan\LogApprovalPendanaan;
use App\EventListener\GetUserTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Service\Logger\AuditLogger;
use DateTime;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ApprovalListener
 *
 * @package App\EventListener\Pendanaan
 * @author  Ahmad Fajar
 * @since   27/04/2019, modified: 20/05/2019 4:44
 */
final class ApprovalListener implements EventSubscriberInterface
{

    use GetUserTrait;

    /**
     * @var AuditLogger
     */
    private $logger;


    /**
     * ApprovalListener constructor.
     *
     * @param ContainerInterface $container
     * @param AuditLogger        $logger
     */
    public function __construct(ContainerInterface $container, AuditLogger $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * Logs proses verifikasi dan approval setelah transaksi Dana Bidang/Program/Batch
     * berhasil diverifikasi ataupun diapproved.
     *
     * @param EnteredEvent $event
     *
     * @throws Exception
     */
    public function onDanaFromNewToOngoing(EnteredEvent $event): void
    {
        /** @var EntityDanaInterface $subject */
        $subject = $event->getSubject();
        $em = $this->logger->getEntityManager();
        $action = $event->getTransition()->getName();
        $context = $event->getContext();

        switch ($action) {
            case WorkflowTags::STEP_VERIFY:
                $subject->setVerifiedBy($this->getUserAccount())
                        ->setTanggalVerified(new DateTime());
                break;
            case WorkflowTags::STEP_APPROVAL_1:
            case WorkflowTags::STEP_APPROVAL_2:
            case WorkflowTags::STEP_APPROVAL_3:
            case WorkflowTags::STEP_VERIFY_REQUEST:
                $subject->setApprovedBy($this->getUserAccount())
                        ->setTanggalApproved(new DateTime());
                break;
        }

        $em->persist($subject);
        $this->dispatchLogData($subject, $action, $context);
    }

    /**
     * Logs proses verifikasi setelah transaksi Dana Global berhasil diverifikasi.
     *
     * @param EnteredEvent $event
     *
     * @throws Exception
     */
    public function onDanaGlobalVerified(EnteredEvent $event): void
    {
        /** @var DanaGlobal $subject */
        $subject = $event->getSubject();
        $em = $this->logger->getEntityManager();
        $action = $event->getTransition()->getName();

        if ($action == WorkflowTags::STEP_VERIFY) {
            $subject->setVerifiedBy($this->getUserAccount())
                    ->setTanggalVerified(new DateTime());
        }

        $em->persist($subject);
        $this->dispatchLogData($subject, $action, $event->getContext());
    }

    /**
     * Accept request revisi Dana Batch and mark each requests as VERIFIED.
     *
     * @param DanaBatch $subject Persistent entity object
     */
    private function danaBatchRevisiVerified(DanaBatch $subject): void
    {
        $em = $this->logger->getEntityManager();
        $tags = $subject->getTags();

        if (!empty($tags)) {
            $requests = $subject->getRevisiAnggarans()->filter(function ($revisi) use ($tags) {
                /** @var DanaBatchRevisiAnggaran $revisi */
                return $revisi->getRevnum() == $tags['revnum'];
            });
            foreach ($requests as $request) {
                /** @var DanaBatchRevisiAnggaran $request */
                $request->setStatus(WorkflowTags::VERIFIED);
                $em->persist($request);
            }
        }
    }

    /**
     * Prepare pencatatan transaksi approval Pendanaan.
     *
     * @param EntityDanaInterface|object $subject Dana Global, Dana Bidang, Dana Program ataupun Dana Batch
     * @param string                     $action  Approval action
     * @param array                      $context Additional context to apply
     */
    private function dispatchLogData(object $subject, string $action, array $context): void
    {
        $entity = new LogApprovalPendanaan();
        $entity->setJumlah($subject->getAnggaran())
               ->setLogTitle($subject->getTitle())
               ->setKeterangan($context['keterangan'] ?? null);

        if ($subject instanceof DanaGlobal) {
            $message = $action . ' transaksi Dana Global nomor: ' . $subject->getKode();
            $entity->setKategori(WorkflowTags::DANA_GLOBAL)
                   ->setTahunAwal($subject->getTahunAwal())
                   ->setTahunAkhir($subject->getTahunAkhir())
                   ->setDanaGlobal($subject);
        } elseif ($subject instanceof DanaBidang) {
            $message = $action . ' transaksi Dana Bidang nomor: ' . $subject->getKode();
            $entity->setKategori(WorkflowTags::DANA_BIDANG)
                   ->setTahunAwal($subject->getTahunAwal())
                   ->setTahunAkhir($subject->getTahunAkhir())
                   ->setDanaBidang($subject);
        } elseif ($subject instanceof DanaProgram) {
            $message = $action . ' transaksi Dana Program nomor: ' . $subject->getKode();
            $entity->setKategori(WorkflowTags::DANA_PROGRAM)
                   ->setTahunAwal($subject->getTahunAwal())
                   ->setTahunAkhir($subject->getTahunAkhir())
                   ->setDanaProgram($subject);
        } elseif ($subject instanceof DanaBatch) {
            $message = $action . ' transaksi Dana Batch nomor: ' . $subject->getKode();
            $entity->setKategori(WorkflowTags::DANA_BATCH)
                   ->setTahunAwal($subject->getTahun())
                   ->setDanaBatch($subject);

            if ($action == WorkflowTags::STEP_VERIFY_REQUEST) {
                $this->danaBatchRevisiVerified($subject);
            }
        }

        if (isset($message)) {
            $this->logApproval($subject, $entity, $action, $message, $context);
        }
    }

    /**
     * @param EntityDanaInterface|object $subject Dana Global, Dana Bidang, Dana Program ataupun Dana Batch
     * @param LogApprovalPendanaan       $entity  Unmanaged entity object
     * @param string                     $action  Approval action
     * @param string                     $message Transaction message for audit log
     * @param array                      $context Additional context to apply
     */
    private function logApproval(object $subject, LogApprovalPendanaan $entity, string $action,
                                 string $message, array $context = []): void
    {
        $em = $this->logger->getEntityManager();

        $entity->setLogAction($action)
               ->setRefcode($subject->getKode())
               ->setFromStatus($context['status'] ?? WorkflowTags::NEW)
               ->setToStatus($subject->getStatus());
        $em->persist($entity);
        $em->flush();

        $this->logger->info($this->logger->formatMessage($message), LogApprovalPendanaan::class,
                            $entity, [JsonController::CONTEXT_NON_REL]);
    }

    /**
     * @inheritDoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            sprintf('%s.dana_global.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REJECTED) => 'onDanaGlobalVerified',
            sprintf('%s.dana_global.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::VERIFIED) => 'onDanaGlobalVerified',

            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::PLANNED)    => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_1) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_2) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_3) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REJECTED)   => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::ONGOING)    => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_bidang.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REVISED)    => 'onDanaFromNewToOngoing',

            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::PLANNED)    => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_1) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_2) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_3) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REJECTED)   => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::ONGOING)    => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_program.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REVISED)    => 'onDanaFromNewToOngoing',

            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::PLANNED)    => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_1) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_2) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::APPROVED_3) => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REJECTED)   => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::ONGOING)    => 'onDanaFromNewToOngoing',
            sprintf('%s.dana_batch.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REVISED)    => 'onDanaFromNewToOngoing',
        ];
    }

}