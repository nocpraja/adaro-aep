<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\Kegiatan;


use App\Component\Workflow\Event\EnterEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Kegiatan\Kegiatan;
use App\EventListener\GetUserTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use DateTime;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UpdateListener
 *
 * @package App\EventListener\Kegiatan
 * @author  Ahmad Fajar
 * @since   16/05/2019, modified: 16/05/2019 4:55
 */
final class UpdateListener implements EventSubscriberInterface
{

    use GetUserTrait;


    /**
     * UpdateListener constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get the User account who made the update and store its data into entity object.
     *
     * @param EnterEvent $event The event object
     *
     * @throws Exception
     */
    public function onKegiatanUpdated(EnterEvent $event): void
    {
        /** @var Kegiatan $subject */
        $subject = $event->getSubject();
        $subject->setUpdatedBy($this->getUserAccount())
                ->setLastUpdated(new DateTime());
    }

    /**
     * @inheritDoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            sprintf('%s.kegiatan.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTER, WorkflowTags::UPDATED) => 'onKegiatanUpdated',
        ];
    }

}