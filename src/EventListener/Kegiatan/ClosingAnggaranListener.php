<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\Kegiatan;


use App\Component\Workflow\Event\EnteredEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Kegiatan\Kegiatan;
use App\EventListener\GetUserTrait;
use App\Models\Pendanaan\DanaBatchModel;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use DateTime;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ClosingAnggaranListener
 *
 * @package App\EventListener\Kegiatan
 * @author  Ahmad Fajar
 * @since   16/05/2019, modified: 19/05/2019 16:58
 */
final class ClosingAnggaranListener implements EventSubscriberInterface
{

    use GetUserTrait, LogTransitionTrait;

    /**
     * @var DanaBatchModel
     */
    private $danaBatchModel;


    /**
     * ClosingAnggaranListener constructor.
     *
     * @param ContainerInterface $container
     * @param DanaBatchModel     $danaBatchModel
     */
    public function __construct(ContainerInterface $container, DanaBatchModel $danaBatchModel)
    {
        $this->container = $container;
        $this->danaBatchModel = $danaBatchModel;
        $this->logger = $danaBatchModel->getLogger();
    }

    /**
     * Listen to workflow event closing Dana Batch, dan proses transaksi closing Dana Batch.
     *
     * @param EnteredEvent $event Event object
     *
     * @throws ORMException
     * @throws Exception
     */
    public function onKegiatanClosed(EnteredEvent $event): void
    {
        /** @var Kegiatan $subject */
        $subject = $event->getSubject();
        $context = $event->getContext();

        $selisih = $this->prepareClosing($subject);
        $title = 'CLOSING ' . $subject->getNamaKegiatan();
        $danaBatch = $subject->getDanaBatch();
        $realisasi = $danaBatch->getRealisasi();

        $danaBatch->setRealisasi($realisasi + $subject->getRealisasi());
        $this->logApproval($subject, WorkflowTags::STEP_CLOSING, $context);

        $this->danaBatchModel->appendTransaction($danaBatch, $selisih, $subject->getKode(), $title, true);
    }

    /**
     * Prepare closing Dana Bidang, Program ataupun Batch.
     *
     * @param Kegiatan|object $subject Persistent entity object
     *
     * @return float Sisa anggaran setelah dikurangi realisasi (dalam rupiah)
     * @throws Exception
     */
    private function prepareClosing(object $subject): float
    {
        $em = $this->logger->getEntityManager();

        if (empty($subject->getRealisasi())) {
            $subject->setRealisasi($subject->getAnggaran());
        }

        $selisih = $subject->getAnggaran() - $subject->getRealisasi();
        $subject->setClosed(true)
                ->setClosedBy($this->getUserAccount())
                ->setTanggalClosed(new DateTime());
        $em->persist($subject);

        return $selisih;
    }

    /**
     * @inheritDoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            sprintf('%s.kegiatan.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::CLOSED) => 'onKegiatanClosed',
        ];
    }

}