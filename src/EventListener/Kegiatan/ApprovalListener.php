<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\Kegiatan;


use App\Component\Controller\JsonController;
use App\Component\Workflow\Event\EnteredEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\LogApprovalKegiatan;
use App\EventListener\GetUserTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Service\Logger\AuditLogger;
use DateTime;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ApprovalListener
 *
 * @package App\EventListener\Kegiatan
 * @author  Ahmad Fajar
 * @since   16/05/2019, modified: 18/05/2019 14:52
 */
final class ApprovalListener implements EventSubscriberInterface
{

    use GetUserTrait, LogTransitionTrait;


    /**
     * ApprovalListener constructor.
     *
     * @param ContainerInterface $container
     * @param AuditLogger        $logger
     */
    public function __construct(ContainerInterface $container, AuditLogger $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    public function onWorkflowTransition(EnteredEvent $event): void
    {
        /** @var Kegiatan $subject */
        $subject = $event->getSubject();
        $em = $this->logger->getEntityManager();
        $action = $event->getTransition()->getName();
        $context = $event->getContext();

        switch ($action) {
            case WorkflowTags::STEP_SUBMIT:
                $subject->setSubmitted(true)
                        ->setSubmittedBy($this->getUserAccount())
                        ->setTanggalSubmitted(new DateTime());
                break;
            case WorkflowTags::STEP_REJECT:
                $subject->setSubmitted(false);
                break;
            case WorkflowTags::STEP_VERIFY:
                $subject->setVerifiedBy($this->getUserAccount())
                        ->setTanggalVerified(new DateTime());
                break;
            case WorkflowTags::STEP_CREATE_REALISASI:
                $subject->setPostedRealisasiBy($this->getUserAccount())
                    ->setTanggalPostedRealisasi(new DateTime());
                break;
            case WorkflowTags::STEP_SUBMIT_REALISASI:
                $subject->setPostedRealisasiBy($this->getUserAccount())
                        ->setTanggalPostedRealisasi(new DateTime());
                break;
            case WorkflowTags::STEP_REJECT_REALISASI:
                $subject->setRealisasi(null);
                break;
            case WorkflowTags::STEP_VERIFY_REALISASI:
                $this->sumRealisasiAnggaran($subject);
                break;
        }

        $em->persist($subject);
        $this->logApproval($subject, $action, $context);
    }

    private function sumRealisasiAnggaran(Kegiatan $subject): void
    {
        $total = 0.0;
        $children = $subject->getChildren();

        if ($children->count() > 0) {
            foreach ($children as $child) {
                $total += $child->getRealisasi();
            }
        }
        $subject->setRealisasi($total);
    }

    /**
     * @inheritDoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            sprintf('%s.kegiatan.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::SUBMITTED)           => 'onWorkflowTransition',
            sprintf('%s.kegiatan.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::VERIFIED)            => 'onWorkflowTransition',
            sprintf('%s.kegiatan.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REJECTED)            => 'onWorkflowTransition',
            sprintf('%s.kegiatan.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REALISASI_SUBMITTED) => 'onWorkflowTransition',
            sprintf('%s.kegiatan.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REALISASI_REJECTED) => 'onWorkflowTransition',
            sprintf('%s.kegiatan.%s.%s', WorkflowEvents::NAME,
                    WorkflowEvents::ENTERED, WorkflowTags::REALISASI_VERIFIED) => 'onWorkflowTransition',
        ];
    }

}
