<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener\Kegiatan;


use App\Component\Controller\JsonController;
use App\Entity\Kegiatan\Kegiatan;
use App\Entity\Kegiatan\LogApprovalKegiatan;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Service\Logger\AuditLogger;

/**
 * Trait LogTransitionTrait
 *
 * @package App\EventListener\Kegiatan
 * @author  Ahmad Fajar
 * @since   19/05/2019, modified: 19/05/2019 16:42
 */
trait LogTransitionTrait
{

    /**
     * @var AuditLogger
     */
    private $logger;

    protected function logApproval(Kegiatan $subject, string $action, array $context): void
    {
        $em = $this->logger->getEntityManager();
        $entity = new LogApprovalKegiatan();

        $entity->setLogAction($action)
               ->setLogTitle($subject->getNamaKegiatan())
               ->setKeterangan($context['keterangan'] ?? null)
               ->setFromStatus($context['status'] ?? WorkflowTags::NEW)
               ->setToStatus($subject->getStatus())
               ->setKegiatan($subject);
        $em->persist($entity);
        $em->flush();

        $message = $action . ' rencana Kegiatan nomor: ' . $subject->getKode();
        $this->logger->info($this->logger->formatMessage($message), LogApprovalKegiatan::class,
                            $entity, [JsonController::CONTEXT_NON_REL]);
    }

}