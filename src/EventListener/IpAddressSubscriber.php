<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\EventListener;


use Gedmo\IpTraceable\IpTraceableListener;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class IpAddressSubscriber
 *
 * @package App\EventListener
 * @author  Ahmad Fajar
 * @since   18/08/2018, modified: 10/04/2019 18:07
 */
class IpAddressSubscriber implements EventSubscriberInterface
{

    /**
     * @var IpTraceableListener
     */
    private $ipTraceListener;


    /**
     * IpTraceSubscriber constructor.
     *
     * @param IpTraceableListener $ipTraceListener
     */
    public function __construct(IpTraceableListener $ipTraceListener)
    {
        $this->ipTraceListener = $ipTraceListener;
    }

    /**
     * Listen to kernel events and sets remote IP Address for future used.
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event): void
    {
        $request = $event->getRequest();

        if (null !== $request && $event->isMasterRequest()) {
            $clientIp = $request->getClientIp();

            if (!empty($clientIp)) {
                $this->ipTraceListener->setIpValue($clientIp);
            }
        }
    }

    /**
     * @inheritdoc
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            KernelEvents::REQUEST => 'onKernelRequest',
        );
    }

}