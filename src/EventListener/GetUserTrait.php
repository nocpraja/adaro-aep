<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\EventListener;


use App\Service\Security\AppUserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait GetUserTrait
 *
 * @package App\EventListener
 * @author  Ahmad Fajar
 * @since   27/04/2019, modified: 27/04/2019 16:22
 */
trait GetUserTrait
{

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * Get UserAccount dari user yang sedang login.
     *
     * @return AppUserInterface|null
     */
    protected function getUserAccount(): ?AppUserInterface
    {
        if (!$this->container->has('security.token_storage')) {
            return null;
        }
        if (null === ($token = $this->container->get('security.token_storage')->getToken())) {
            return null;
        }

        return $token->getUser();
    }

}