<?php
/**
 * Description: TbeBeneficiaryApprovalListenerner.php generated by PhpStorm.
 * @project     adaro-aep
 * @package     App\EventListener\Beneficiary
 * @author      alex
 * @created     2020-02-29, modified: 2020-02-29 22:49
 * @copyright   Copyright (c) 2020
 */

namespace App\EventListener\Beneficiary;


use App\Component\Workflow\Event\EnteredEvent;
use App\Component\Workflow\WorkflowEvents;
use App\Entity\Beneficiary\BeneficiaryBeasiswa;
use App\Entity\Beneficiary\BeneficiaryIndividu;
use App\Entity\Beneficiary\BeneficiaryInstitusi;
use App\Entity\Beneficiary\BeneficiaryTbe;
use App\EventListener\GetUserTrait;
use App\Models\Pendanaan\Workflow\WorkflowTags;
use App\Service\Logger\AuditLogger;
use DateTime;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ApprovalListener
 * @package App\EventListener\Beneficiary
 * @author  Alex Gaspersz
 * @since   29/02/2020, modified: 03/03/2020 15:27
 */
class ApprovalListener implements EventSubscriberInterface
{
    use GetUserTrait, ApprovalTransitionTrait;

    /**
     * ApprovalListener constructor.
     *
     * @param ContainerInterface $container
     * @param AuditLogger        $logger
     */
    public function __construct(ContainerInterface $container,
                                AuditLogger $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * The array keys are event names and the value can be:
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     * For instance:
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            sprintf('%s.beneficiary.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::VERIFIED) => 'onWorkflowTransition',
            sprintf('%s.beneficiary.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::REJECTED) => 'onWorkflowTransition',
            sprintf('%s.beneficiary.%s.%s', WorkflowEvents::NAME,
                WorkflowEvents::ENTERED, WorkflowTags::APPROVED) => 'onWorkflowTransition',
        ];
    }

    /***
     * @param EnteredEvent $event
     *
     * @throws \Exception
     */
    public function onWorkflowTransition(EnteredEvent $event): void
    {
        /** @var BeneficiaryTbe|BeneficiaryBeasiswa|BeneficiaryIndividu|BeneficiaryInstitusi $subject */
        $subject = $event->getSubject();
        $em = $this->logger->getEntityManager();
        $action = $event->getTransition()->getName();
        $context = $event->getContext();

        $subjectClass = $em->getMetadataFactory()->getMetadataFor(get_class($subject))->getName();
        $biodata = false;
        if ($subjectClass == BeneficiaryInstitusi::class || $subjectClass == BeneficiaryIndividu::class) {
            $biodata = true;
        }

        switch ($action) {
            case WorkflowTags::STEP_REJECT:
                /** Update Individu / Institut Approval Data from entity relation or direct */
                $biodata ? $subject->setStatusVerifikasi(3) : $subject->getBiodata()->setStatusVerifikasi(3);
                !$biodata ? $subject->getBiodata()->setStatus($subject->getStatus()) : null;
                break;
            case WorkflowTags::STEP_VERIFY:
                $subject->setVerifiedBy($this->getUserAccount());
                $subject->setTanggalVerified(new DateTime());

                /** Update Individu / Institut Approval Data from entity relation or direct */
                $biodata ? $subject->setVerifiedBy($this->getUserAccount()) : $subject->getBiodata()->setVerifiedBy($this->getUserAccount());
                $biodata ? $subject->setTanggalVerified(new DateTime()) : $subject->getBiodata()->setTanggalVerified(new DateTime());
                $biodata ? $subject->setStatusVerifikasi(1) : $subject->getBiodata()->setStatusVerifikasi(1);
                !$biodata ? $subject->getBiodata()->setStatus($subject->getStatus()) : null;
                break;
            case WorkflowTags::STEP_APPROVAL:
                $subject->setApprovedBy($this->getUserAccount());
                $subject->setTanggalApproved(new DateTime());

                /** Update Individu / Institut Approval Data from entity relation or direct */
                $biodata ? $subject->setApprovedBy($this->getUserAccount()) : $subject->getBiodata()->setApprovedBy($this->getUserAccount());
                $biodata ? $subject->setTanggalApproved(new DateTime()) : $subject->getBiodata()->setTanggalApproved(new DateTime());
                $biodata ? $subject->setStatusVerifikasi(2) : $subject->getBiodata()->setStatusVerifikasi(2);
                !$biodata ? $subject->getBiodata()->setStatus($subject->getStatus()) : null;
                break;
        }

        $em->persist($subject);
        $this->logApproval($subject, $action, $context);
    }
}
