<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2019 - Ahmad Fajar
 */

namespace App\Service\Mail;


use App\Service\Logger\AuditLogger;
use App\Models\Security\AppSettingModel;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class SimpleMailer
 *
 * @package App\Service\Mail
 * @author  Ahmad Fajar
 * @since   2019-04-15, modified: 15/05/2019 18:49
 */
final class SimpleMailer
{
    const SECTION = 'MAILER';
    const FROMADDRESS = 'From';
    const FROMNAME = 'FromName';

    private $from = ['adarofoundation@adaro.com' => 'Admin Adaro CSRMS'];
    private $recipients = [];
    private $toCc = [];
    private $toBcc = [];
    private $context = [];
    private $checkFrom = true;
    private $subject = '';
    private $template = '';
    private $contentType = 'text/html';

    /**
     * @var AuditLogger
     */
    private $logger;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var AppSettingModel
     */
    private $appSettingsModel;


    public function __construct(AuditLogger $auditLogger, Environment $twig, Swift_Mailer $mailer, AppSettingModel $appSettingModel)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->logger = $auditLogger;
        $this->appSettingsModel = $appSettingModel;
    }

    /**
     * Gets email content type.
     *
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }

    /**
     * Sets email content type.
     *
     * @param string $contentType
     *
     * @return SimpleMailer
     */
    public function setContentType(string $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Gets data to pass to the template engine.
     *
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * Sets data to pass to the template engine.
     *
     * @param array $context
     *
     * @return SimpleMailer
     */
    public function setContext(array $context): self
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Gets sender email address.
     *
     * @return array
     */
    public function getFrom(): array
    {
        return $this->from;
    }

    /**
     * Sets sender email address.
     *
     * @param array $from Example: array('from-main@domain.org', 'from-other@domain.org' => 'Full Name')
     *
     * @return SimpleMailer
     */
    public function setFrom(array $from): self
    {
        if (!empty($from)) {
            $this->from = $from;
            $this->checkFrom = false;
        }

        return $this;
    }

    /**
     * Gets recipient email address.
     *
     * @return array
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    /**
     * Sets recipient email address.
     *
     * @param array $recipients Example: array('receiver@domain.org', 'other@domain.org' => 'Full Name')
     *
     * @return SimpleMailer
     */
    public function setRecipients(array $recipients): self
    {
        $this->recipients = $recipients;

        return $this;
    }

    /**
     * Gets email subject.
     *
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * Sets email subject.
     *
     * @param string $subject
     *
     * @return SimpleMailer
     */
    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Gets twig template filename to be used.
     *
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * Sets template filename to be used.
     *
     * @param string $template Twig template filename
     *
     * @return SimpleMailer
     */
    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Gets BCC email address.
     *
     * @return array
     */
    public function getToBcc(): array
    {
        return $this->toBcc;
    }

    /**
     * Sets BCC email address.
     *
     * @param array $toBcc Example: array('bcc-main@domain.org', 'bcc-other@domain.org' => 'Full Name')
     *
     * @return SimpleMailer
     */
    public function setToBcc(array $toBcc): self
    {
        $this->toBcc = $toBcc;

        return $this;
    }

    /**
     * Gets CC email address.
     *
     * @return array
     */
    public function getToCc(): array
    {
        return $this->toCc;
    }

    /**
     * Sets CC email address.
     *
     * @param array $toCc Example: array('cc-main@domain.org', 'cc-other@domain.org' => 'Full Name')
     *
     * @return SimpleMailer
     */
    public function setToCc(array $toCc): self
    {
        $this->toCc = $toCc;

        return $this;
    }

    /**
     * Send simple html email.
     */
    public function sendHtmlEmail(): void
    {
        if ($this->isRequirementValid()) {
            $this->contentType = 'text/html';

            try {
                $this->mailer->send($this->composeMail());
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }

    /**
     * Send simple plain text email.
     */
    public function sendPlainEmail(): void
    {
        if ($this->isRequirementValid()) {
            $this->contentType = 'text/plain';

            try {
                $this->mailer->send($this->composeMail());
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }

    /**
     * Render body message and compose Mail.
     *
     * @return Swift_Message|null
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function composeMail(): Swift_Message
    {
        $this->configureMailFrom();

        $body = $this->twig->render($this->template, $this->context);
        $message = new Swift_Message(trim($this->subject), $body, $this->contentType);
        $message->setFrom($this->from)
                ->setTo($this->recipients);

        if (!empty($this->toCc)) {
            $message->setCc($this->toCc);
        }
        if (!empty($this->toBcc)) {
            $message->setBcc($this->toBcc);
        }

        return $message;
    }

    /**
     * Sets configuration of field MailFrom from the database.
     */
    private function configureMailFrom()
    {
        if ($this->checkFrom) {
            // Ambil konfigurasi MailFrom dari database;
            $address = $this->appSettingsModel->getAttributeValue(self::SECTION, self::FROMADDRESS);
            $name = $this->appSettingsModel->getAttributeValue(self::SECTION, self::FROMNAME);
            $this->setFrom( [ $address => $name ]);
            return;
        }
    }

    /**
     * Memeriksa apakah requirement utama untuk pengiriman email sudah terpenuhi atau belum.
     *
     * @return bool
     */
    private function isRequirementValid(): bool
    {
        if (empty(trim($this->subject)) || empty(trim($this->template)) || empty($this->recipients)) {
            $data = ['subject' => $this->subject, 'recipients' => $this->recipients, 'template' => $this->template];
            $this->logger->error('Tidak dapat memproses pengiriman email. Salah satu mandatory field tidak terpenuhi.',
                                 $data);

            return false;
        }

        return true;
    }

}