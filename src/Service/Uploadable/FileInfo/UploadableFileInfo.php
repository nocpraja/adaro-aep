<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable\FileInfo;


use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class UploadableFileInfo untuk memproses informasi tentang UploadedFile.
 *
 * @package App\Service\Uploadable\FileInfo
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 01/06/2018 19:18
 */
class UploadableFileInfo implements FileInfoInterface
{
    /**
     * @var UploadedFile
     */
    private $uploadedFile;


    /**
     * UploadableFileInfo constructor.
     *
     * @param UploadedFile $uploadedFile
     */
    public function __construct(UploadedFile $uploadedFile)
    {
        $this->uploadedFile = $uploadedFile;
    }

    /**
     * Gets the base name of the file.
     *
     * @param string|null $suffix Optional suffix to omit from the base name returned
     * @return string The base name without path information
     */
    public function getBasename(string $suffix = null): string
    {
        $splInfo = new \SplFileInfo($this->getFilename());

        return $splInfo->getBasename($suffix);
    }

    /**
     * Gets the file extension.
     *
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->uploadedFile->guessExtension();
    }

    /**
     * Gets the filename without any path information.
     *
     * @return string
     */
    public function getFilename(): string
    {
        return $this->uploadedFile->getClientOriginalName() ?: $this->uploadedFile->getFilename();
    }

    /**
     * Returns the mime type of the file.
     *
     * The mime type is guessed using a MimeTypeGuesser instance, which uses finfo(),
     * mime_content_type() and the system binary "file" (in this order), depending on
     * which of those are available.
     *
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->uploadedFile->getMimeType();
    }

    /**
     * Gets the path without filename.
     *
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->uploadedFile->getPath();
    }

    /**
     * Gets the path to the file.
     *
     * @return string
     */
    public function getPathname(): string
    {
        return $this->uploadedFile->getPathname();
    }

    /**
     * Gets file size.
     *
     * @return int|null The filesize in bytes
     */
    public function getSize(): ?int
    {
        return $this->uploadedFile->getSize();
    }

    /**
     * Returns whether the file was uploaded successfully.
     *
     * @return bool True if the file has been uploaded with HTTP and no error occurred
     */
    public function isValid(): bool
    {
        return $this->uploadedFile->isValid();
    }

    /**
     * Moves the file to a new location.
     *
     * @param string $directory The destination folder
     * @param string $name      The new file name
     * @return File A file object representing the new file
     *
     * @throws FileException if the target file could not be created
     */
    public function move(string $directory, string $name = null): File
    {
        return $this->uploadedFile->move($directory, $name);
    }

}