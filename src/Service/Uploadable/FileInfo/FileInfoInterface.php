<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable\FileInfo;


use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;


/**
 * Interface FileInfoInterface
 *
 * @package App\Service\Uploadable\FileInfo
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 01/06/2018 19:18
 */
interface FileInfoInterface
{
    /**
     * Gets the base name of the file.
     *
     * @param string|null $suffix Optional suffix to omit from the base name returned
     * @return string The base name without path information
     */
    public function getBasename(string $suffix = null): string;

    /**
     * Gets the file extension.
     *
     * @return string|null
     */
    public function getExtension(): ?string;

    /**
     * Gets the filename without any path information.
     *
     * @return string
     */
    public function getFilename(): string;

    /**
     * Returns the mime type of the file.
     *
     * The mime type is guessed using a MimeTypeGuesser instance, which uses finfo(),
     * mime_content_type() and the system binary "file" (in this order), depending on
     * which of those are available.
     *
     * @return string|null
     */
    public function getMimeType(): ?string;

    /**
     * Gets the path without filename.
     *
     * @return string|null
     */
    public function getPath(): ?string;

    /**
     * Gets the path to the file.
     *
     * @return string
     */
    public function getPathname(): string;

    /**
     * Gets file size.
     *
     * @return int|null The filesize in bytes
     */
    public function getSize(): ?int;

    /**
     * Moves the file to a new location.
     *
     * @param string $directory The destination folder
     * @param string $name      The new file name
     * @return File A File object representing the new file
     *
     * @throws FileException if the target file could not be created
     */
    public function move(string $directory, string $name = null): File;

}