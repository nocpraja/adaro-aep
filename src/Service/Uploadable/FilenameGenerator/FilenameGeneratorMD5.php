<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable\FilenameGenerator;

/**
 * Class FilenameGeneratorMD5
 *
 * @package App\Service\Uploadable\FilenameGenerator
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 13/03/2018 00:32
 */
final class FilenameGeneratorMD5 implements FilenameGeneratorInterface
{

    /**
     * Generate a new filename
     *
     * @param string $filename       The filename without extension
     * @param string $extension      File extension without dot: jpg, gif, etc
     * @param object $object         The entity object
     * @param bool   $groupedMonthly Grouped the file within subdirectory
     * @param bool   $groupedDaily   Grouped the file within subdirectory
     * @return string
     */
    public static function generate($filename, $extension, $object = null, $groupedMonthly = false, $groupedDaily = false): string
    {
        $subdir = $groupedMonthly
                ? (date('Y') . DS . date('m'))
                : ($groupedDaily ? (date('Y') . DS . date('m') . DS . date('d')) : '');
        $name = md5(uniqid(($filename . $extension), true)) . '.' . $extension;

        return !empty($subdir) ? ($subdir . DS . $name) : $name;
    }

}