<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable\FilenameGenerator;

/**
 * Interface FilenameGeneratorInterface
 *
 * @package App\Service\Uploadable\FilenameGenerator
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 13/03/2018 00:37
 */
interface FilenameGeneratorInterface
{
    const ALPHANUMERIC = 'Alphanumeric';
    const MD5 = 'MD5';
    const NONE = 'NONE';
    const SHA1 = 'SHA1';
    const SUBTITLE = 'SUBTITLE';
    const VIDEO = 'VIDEO';

    /**
     * Generate a new filename
     *
     * @param string $filename       The filename without extension
     * @param string $extension      File extension without dot: jpg, gif, etc
     * @param object $object         The entity object
     * @param bool   $groupedMonthly Grouped the file within subdirectory
     * @param bool   $groupedDaily   Grouped the file within subdirectory
     * @return string
     */
    public static function generate($filename, $extension, $object = null, $groupedMonthly = false, $groupedDaily = false): string;

}