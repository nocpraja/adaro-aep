<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable;


use App\Service\Uploadable\FileInfo\FileInfoInterface;
use App\Service\Uploadable\Handler\AnnotationMetadataHandler;
use App\Service\Uploadable\Handler\UploadableFileHandler;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Mapping\ClassMetadata as BaseMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Proxy\Proxy;
use Psr\Log\LoggerInterface;

/**
 * Class UploadableEventSubscriber
 *
 * @package App\Service\Uploadable
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 27/07/2018 18:13
 */
class UploadableEventSubscriber implements EventSubscriber
{
    /**
     * Uploadable metadata handler.
     *
     * @var AnnotationMetadataHandler
     */
    private $annotationHandler;

    /**
     * Uploadable file handler.
     *
     * @var UploadableFileHandler
     */
    private $uploadableHandler;

    /**
     * Buffer to store files that will be removed.
     *
     * @var array
     */
    private $pendingRemovals = [];

    /**
     * @var LoggerInterface
     */
    private $logger;


    /**
     * UploadableEventSubscriber constructor.
     *
     * @param AnnotationMetadataHandler $annotationHandler
     * @param UploadableFileHandler     $uploadableHandler
     * @param LoggerInterface           $logger
     */
    public function __construct(AnnotationMetadataHandler $annotationHandler, UploadableFileHandler $uploadableHandler, LoggerInterface $logger)
    {
        $this->annotationHandler = $annotationHandler;
        $this->uploadableHandler = $uploadableHandler;
        $this->logger = $logger;
    }

    /**
     * Wrap uploadable field with FileInfo object for the given entity.
     *
     * @param ObjectManager     $manager  Doctrine object manager
     * @param FileInfoInterface $fileInfo The uploadable FileInfo object
     * @param object            $entity   The entity object
     */
    public function addFileInfoToEntity(ObjectManager $manager, FileInfoInterface $fileInfo, $entity): void
    {
        if (!$fileInfo instanceof FileInfoInterface) {
            $msg = 'You must pass an instance of FileInfoInterface for entity of class "%s".';
            throw new \InvalidArgumentException(sprintf($msg, get_class($entity)));
        }

        $metadata = $manager->getClassMetadata(get_class($entity));
        $config = $this->annotationHandler->readAnnotationMetadata($metadata->getReflectionClass());
        $this->setFieldValue($metadata, $entity, $config['fileNameField'], $fileInfo);
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            'prePersist',
            'preRemove',
            'preUpdate',
            'postLoad',
            'postFlush'
        ];
    }

    /**
     * Handle removal of files.
     *
     * @param PostFlushEventArgs $eventArgs
     */
    public function postFlush(PostFlushEventArgs $eventArgs): void
    {
        if (!empty($this->pendingRemovals)) {
            foreach ($this->pendingRemovals as $file) {
                $this->removeFile($file);
            }
            $this->pendingRemovals = [];
        }
    }

    /**
     * Handle injection of url prefix to the `filePathField`.
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function postLoad(LifecycleEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getObject();
        $manager = $eventArgs->getEntityManager();
        $metadata = $manager->getClassMetadata(get_class($entity));
        $config = $this->annotationHandler->readAnnotationMetadata($metadata->getReflectionClass());

        if (!empty($config) && $config['uploadable'] === true) {
            if ($config['filePathField']) {
                $this->setFieldValue($metadata, $entity, $config['filePathField'],
                                     $this->uploadableHandler->getBaseUrl());
            }
        }
    }

    /**
     * Process uploaded file and update the uploadable fields as necessary.
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function prePersist(LifecycleEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getObject();
        $manager = $eventArgs->getEntityManager();
        $metadata = $manager->getClassMetadata(get_class($entity));
        $this->processObject($manager, $metadata, $entity, [], false);
    }

    /**
     * Process removing file.
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function preRemove(LifecycleEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getObject();
        $manager = $eventArgs->getEntityManager();
        $metadata = $manager->getClassMetadata(get_class($entity));
        $config = $this->annotationHandler->readAnnotationMetadata($metadata->getReflectionClass());

        if (!empty($config) && $config['uploadable'] === true) {
            if ($entity instanceof Proxy) {
                $entity->__load();
            }
            $value = $metadata->getFieldValue($entity, $config['fileNameField']);
            $filename = $this->uploadableHandler->getTargetFile($value);
            $this->pendingRemovals[] = $filename;
        }
    }

    /**
     * Process removing old file, uploaded file and update the uploadable fields as necessary.
     *
     * @param PreUpdateEventArgs $eventArgs
     *
     * @throws \Exception
     */
    public function preUpdate(PreUpdateEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getObject();
        $manager = $eventArgs->getEntityManager();
        $metadata = $manager->getClassMetadata(get_class($entity));
        $config = $this->annotationHandler->readAnnotationMetadata($metadata->getReflectionClass());

        if (!empty($config) && $config['uploadable'] === true) {
            try {
                if ($eventArgs->hasChangedField($config['fileNameField'])) {
                    $oldValue = $eventArgs->getOldValue($config['fileNameField']);
                    $filename = $this->uploadableHandler->getTargetFile($oldValue);
                    $this->pendingRemovals[] = $filename;
                }
                $this->processObject($manager, $metadata, $entity, $config, true);
            } catch (\Exception $ex) {
                throw $ex;
            }
        }
    }

    /**
     * Process updating entity object.
     *
     * @param EntityManager $manager      Doctrine object manager
     * @param ClassMetadata $metadata     The entity metadata class
     * @param Object        $entity       The entity object
     * @param array         $config       The uploadable configuration
     * @param boolean       $notifyUpdate Notify the entity manager update listener?
     */
    protected function processObject(EntityManager $manager, ClassMetadata $metadata,
                                     $entity, array $config = [], bool $notifyUpdate = true): void
    {
        if (empty($config)) {
            $config = $this->annotationHandler->readAnnotationMetadata($metadata->getReflectionClass());
        }
        if (empty($config) || $config['uploadable'] === false) {
            return;
        }

        $fileObject = $metadata->getFieldValue($entity, $config['fileNameField']);

        if (!is_null($fileObject) && $fileObject instanceof FileInfoInterface) {
            if (!empty($config['allowedTypes'])) {
                // TODO: validate uploadable constraints
            }
            $filename = $this->uploadableHandler->moveFile($fileObject, $config, $entity);
            $this->logger->info(sprintf('Store file "%s"', $this->uploadableHandler->getTargetFile($filename)));
            $this->setFieldValue($metadata, $entity, $config['fileNameField'], $filename);

            if ($config['filePathField']) {
                $this->setFieldValue($metadata, $entity, $config['filePathField'],
                                     $this->uploadableHandler->getTargetPath());
            }
            if ($config['fileMimeTypeField']) {
                $this->setFieldValue($metadata, $entity, $config['fileMimeTypeField'],
                                     $this->uploadableHandler->getMimeType());
            }
            if ($config['fileSizeField']) {
                $this->setFieldValue($metadata, $entity, $config['fileSizeField'],
                                     $this->uploadableHandler->getFileSize());
            }
        }

        if ($notifyUpdate) {
            $uow = $manager->getUnitOfWork();
            $uow->recomputeSingleEntityChangeSet($metadata, $entity);
        }
    }

    /**
     * Process removing a file.
     *
     * @param string $filename
     */
    protected function removeFile($filename): void
    {
        if (is_file($filename)) {
            @unlink($filename);
        }
    }

    /**
     * Set new value to an entity Object.
     *
     * @param ClassMetadata|BaseMetadata $metadata  The entity metadata class
     * @param object                     $entity    The entity object
     * @param string                     $fieldName The fieldname
     * @param mixed                      $value     The new value
     */
    protected function setFieldValue($metadata, $entity, string $fieldName, $value): void
    {
        if ($metadata->hasField($fieldName)) {
            $metadata->setFieldValue($entity, $fieldName, $value);
        } else {
            $property = $metadata->getReflectionClass()->getProperty($fieldName);
            $property->setAccessible(true);
            $property->setValue($entity, $value);
        }
    }

}