<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable\Handler;


use Doctrine\Common\Annotations\Reader;

/**
 * Class AnnotationMetadataHandler
 *
 * @package App\Service\Uploadable\Handler
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 04/03/2018 23:46
 */
class AnnotationMetadataHandler
{
    /**
     * Annotation to define that the entity object is uploadable
     */
    const UPLOADABLE_CLASS = 'App\Service\Uploadable\Annotation\Uploadable';
    const UPLOADABLE_FILE_FIELD = 'App\Service\Uploadable\Annotation\FileName';
    const UPLOADABLE_FILE_MIMETYPE = 'App\Service\Uploadable\Annotation\FileMimeType';
    const UPLOADABLE_FILE_PATH = 'App\Service\Uploadable\Annotation\FilePath';
    const UPLOADABLE_FILE_SIZE = 'App\Service\Uploadable\Annotation\FileSize';

    /**
     * @var Reader
     */
    private $reader;


    /**
     * AnnotationMetadataHandler constructor.
     *
     * @param Reader $reader
     */
    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @return Reader
     */
    public function getReader(): Reader
    {
        return $this->reader;
    }

    /**
     * Check whether the entity object/class is uploadable or not.
     *
     * @param \ReflectionClass $reflectionClass
     * @return bool
     */
    public function isSupported(\ReflectionClass $reflectionClass): bool
    {
        return ($this->reader->getClassAnnotation($reflectionClass, static::UPLOADABLE_CLASS) !== null);
    }

    /**
     * Load the annotation metadata for the uploadable entity object.
     *
     * @param \ReflectionClass $reflectionClass
     * @return array
     */
    public function readAnnotationMetadata(\ReflectionClass $reflectionClass): array
    {
        if (!$this->isSupported($reflectionClass)) {
            return [];
        }

        $annotation = $this->reader->getClassAnnotation($reflectionClass, static::UPLOADABLE_CLASS);
        $config = [
            'uploadable'        => true,
            'appendNumber'      => $annotation->appendNumber,
            'allowedTypes'      => $annotation->allowedTypes,
            'filenameGenerator' => $annotation->filenameGenerator,
            'fileMimeTypeField' => false,
            'fileNameField'     => false,
            'filePathField'     => false,
            'fileSizeField'     => false
        ];

        foreach ($reflectionClass->getProperties() as $property) {
            if ($this->reader->getPropertyAnnotation($property, static::UPLOADABLE_FILE_FIELD)) {
                $config['fileNameField'] = $property->getName();
            }
            if ($this->reader->getPropertyAnnotation($property, static::UPLOADABLE_FILE_MIMETYPE)) {
                $config['fileMimeTypeField'] = $property->getName();
            }
            if ($this->reader->getPropertyAnnotation($property, static::UPLOADABLE_FILE_PATH)) {
                $config['filePathField'] = $property->getName();
            }
            if ($this->reader->getPropertyAnnotation($property, static::UPLOADABLE_FILE_SIZE)) {
                $config['fileSizeField'] = $property->getName();
            }
        }

        if (!$config['fileNameField'] && !$config['fileMimeTypeField']) {
            return [];
        }

        return $config;
    }

}