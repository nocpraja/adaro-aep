<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable\Handler;


use App\Service\Uploadable\FileInfo\FileInfoInterface;
use App\Service\Uploadable\FilenameGenerator\FilenameGeneratorInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Class UploadableFileHandler
 *
 * @package App\Service\Uploadable\Handler
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 02/06/2018 13:21
 */
class UploadableFileHandler
{
    /**
     * @var string
     */
    const GENERATOR_NAMESPACE = 'App\Service\Uploadable\FilenameGenerator';

    /**
     * The base Url prefix to be used when retrieving the file within browser.
     *
     * @var string
     */
    private $baseUrl;

    /**
     * The uploaded fileSize.
     *
     * @var integer
     */
    private $fileSize = 0;

    /**
     * Groups the uploaded files in subdirectory.
     *
     * @var bool
     */
    private $groupDaily = false;

    /**
     * Groups the uploaded files in subdirectory.
     *
     * @var bool
     */
    private $groupMonthly = false;

    /**
     * The uploaded file MimeType.
     *
     * @var string
     */
    private $mimeType;

    /**
     * The target directory to store the uploaded file.
     *
     * @var string
     */
    private $targetPath;


    /**
     * UploadableFileHandler constructor.
     *
     * @param string $targetPath The target directory to store the uploaded File
     * @param string $urlPrefix  A prefix to be used to generate url file location
     */
    public function __construct(string $targetPath, string $urlPrefix)
    {
        $this->targetPath = $targetPath;
        $this->baseUrl = $urlPrefix;
    }

    /**
     * The prefix to be used to generate url file location.
     *
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * Get the uploaded fileSize in bytes.
     *
     * @return int
     */
    public function getFileSize(): int
    {
        return $this->fileSize;
    }

    /**
     * The uploaded file MimeType.
     *
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * Get the real location of a file.
     *
     * @param string $filename The relative filename
     * @return string The full qualified filename
     */
    public function getTargetFile($filename): string
    {
        $baseDir = realpath($this->getTargetPath());

        return $baseDir . DS . str_replace('/', DS, $filename);
    }

    /**
     * Get root directory penyimpanan file.
     *
     * @return string
     */
    public function getTargetPath(): string
    {
        return $this->targetPath;
    }

    /**
     * Groups the uploaded files whithin subdirectory ?
     *
     * @return bool
     */
    public function isGroupDaily(): bool
    {
        return $this->groupDaily;
    }

    /**
     * @param bool $groupDaily
     */
    public function setGroupDaily(bool $groupDaily): void
    {
        $this->groupDaily = $groupDaily;
    }

    /**
     * Groups the uploaded files whithin subdirectory ?
     *
     * @return bool
     */
    public function isGroupMonthly(): bool
    {
        return $this->groupMonthly;
    }

    /**
     * @param bool $groupMonthly
     */
    public function setGroupMonthly(bool $groupMonthly): void
    {
        $this->groupMonthly = $groupMonthly;
    }

    /**
     * Moves the uploaded file to a new location.
     * Returns a new filename with relative location that can be prefix with urlPrefix.
     *
     * @param FileInfoInterface $fileObject The file object
     * @param array             $options    The available configuration
     * @param object|null       $entity     The entity object
     * @return bool|string
     */
    public function moveFile(FileInfoInterface $fileObject, array $options, $entity = null)
    {
        if (!is_null($fileObject) && !empty($options)) {
            try {
                $this->mimeType = $fileObject->getMimeType();
                $this->fileSize = $fileObject->getSize();
                $extension = $fileObject->getExtension();
                $fileNoExt = $fileObject->getBasename('.' . $extension);
                $generatorClass = $this->filenameGenerator($options['filenameGenerator']);
                $filename = $generatorClass::generate($fileNoExt, $extension, $entity, $this->isGroupMonthly(),
                                                      $this->isGroupDaily());

                // periksa apakah $filename berada dalam subdir atau tidak
                $splFile = new \SplFileInfo($filename);
                $filename = $splFile->getFilename();
                $filepath = $splFile->getPath();
                $destDir = $this->targetPath . (!empty($filepath) ? DS . $filepath : '');

                if (is_file($destDir . DS . $filename)) {
                    if ($options['appendNumber']) {
                        $counter = 1;
                        $filename = $splFile->getBasename('.' . $extension) . '-' . $counter . '.' . $extension;
                        while (is_file($destDir . DS . $filename)) {
                            $filename = $splFile->getBasename('.' . $extension) . '-' . (++$counter) . '.' . $extension;
                        }
                        $splFile = new \SplFileInfo(!empty($filepath) ? $filepath . DS . $filename : $filename);
                    } else {
                        throw new FileException(sprintf('File "%s" already exists!', $fileObject->getFilename()));
                    }
                }

                $fileObject->move($destDir, $filename);
                $result = str_replace(DS, '/', $splFile->__toString());

                return $result;
            } catch (FileException $ex) {
                throw $ex;
            }
        }

        return false;
    }

    /**
     * @param string $clazz FilenameGenerator className or constanta
     * @return string|FilenameGeneratorInterface
     */
    protected function filenameGenerator($clazz)
    {
        switch ($clazz) {
            case FilenameGeneratorInterface::NONE :
            case FilenameGeneratorInterface::MD5 :
                $generatorClass = static::GENERATOR_NAMESPACE . '\FilenameGeneratorMD5';
                break;
            case FilenameGeneratorInterface::SHA1 :
                $generatorClass = static::GENERATOR_NAMESPACE . '\FilenameGeneratorSHA1';
                break;
            case FilenameGeneratorInterface::VIDEO :
                $generatorClass = static::GENERATOR_NAMESPACE . '\FilenameGeneratorVideoAsset';
                break;
            case FilenameGeneratorInterface::ALPHANUMERIC :
                $generatorClass = static::GENERATOR_NAMESPACE . '\FilenameGeneratorAlphanumeric';
                break;
            default:
                $generatorClass = $clazz;
                break;
        }

        return $generatorClass;
    }
}