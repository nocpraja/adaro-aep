<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable\Annotation;


use Doctrine\Common\Annotations\Annotation;

/**
 * Class FileMimeType annotation for Uploadable's behavioral.
 * This service is tightly integrated with Symfony4.
 *
 * @Annotation
 * @Target("PROPERTY")
 *
 * @package App\Service\Uploadable\Annotation
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 04/03/2018 22:24
 */
class FileMimeType extends Annotation
{

}