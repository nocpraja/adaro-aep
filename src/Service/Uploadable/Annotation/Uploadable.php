<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Uploadable\Annotation;


use App\Service\Uploadable\FilenameGenerator\FilenameGeneratorInterface;
use Doctrine\Common\Annotations\Annotation;

/**
 * Class Uploadable annotation for Uploadable's behavioral.
 * This service is tightly integrated with Symfony4.
 *
 * @Annotation
 * @Target("CLASS")
 *
 * @package App\Service\Uploadable\Annotation
 * @author  Ahmad Fajar
 * @since   04/03/2018, modified: 04/03/2018 13:47
 */
class Uploadable extends Annotation
{
    /** @var boolean */
    public $appendNumber = false;

    /** @var string */
    public $filenameGenerator = FilenameGeneratorInterface::NONE;

    /** @var array */
    public $allowedTypes = [];

}