<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Security;


use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface AppUserInterface
 *
 * @package App\Service\Security
 * @author  Ahmad Fajar
 * @since   24/07/2018, modified: 24/07/2018 16:31
 */
interface AppUserInterface extends UserInterface
{

    /**
     * Checks whether the user's account has expired or not.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired(): bool;

    /**
     * Checks whether the user is enabled or not.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled(): bool;

}