<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Logger;


use App\Entity\Security\AuditLog;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AuditLogger is used to chain Symfony logger and database logger.
 *
 * @package App\Service\Logger
 * @author  Ahmad Fajar
 * @since   19/08/2018, modified: 27/04/2019 13:54
 */
final class AuditLogger
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SerializerInterface
     */
    private $serializer;


    /**
     * AuditLogger constructor.
     *
     * @param EntityManagerInterface $entityManager Doctrine entity manager
     * @param LoggerInterface        $logger        Psr6 Logger service
     * @param SerializerInterface    $serializer    Symfony4 serializer service
     */
    public function __construct(EntityManagerInterface $entityManager,
                                LoggerInterface $logger, SerializerInterface $serializer)
    {
        $this->em = $entityManager;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    /**
     * Log for something like action must be taken immediately.
     * Example: Entire website down, database unavailable, etc.
     *
     * This type of log level will not be logged to the database.
     *
     * @param string       $message The log message
     * @param array|object $data    The data to be appended
     * @param array        $context The serializer context to be used
     */
    public function alert(string $message, $data = null, array $context = []): void
    {
        $this->log(LogLevel::ALERT, $message, null, $data, $context, false);
    }

    /**
     * Log for debuging information. This type of log level will not be logged to the database.
     *
     * @param string       $message The log message
     * @param array|object $data    The data to be appended
     * @param array        $context The serializer context to be used
     */
    public function debug(string $message, $data = null, array $context = []): void
    {
        $this->log(LogLevel::DEBUG, $message, null, $data, $context, false);
    }

    /**
     * Log for something like: runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * This type of log level will not be logged to the database.
     *
     * @param string       $message The log message
     * @param array|object $data    The data to be appended
     * @param array        $context The serializer context to be used
     */
    public function error(string $message, $data = null, array $context = []): void
    {
        $this->log(LogLevel::ERROR, $message, null, $data, $context, false);
    }

    /**
     * Helper function to standardize log message format.
     *
     * @param string $text Log message
     *
     * @return string Formatted log message
     */
    public function formatMessage(string $text): string
    {
        $names = explode(' ', $text);
        $names[0] = strtoupper($names[0]);
        $str = rtrim(implode(' ', $names), ' .');

        return $str . '.';
    }

    /**
     * Get doctrine entity manager.
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * Log for interesting events. This type of log level can be logged to the database.
     *
     * @param string       $message    The log message
     * @param string       $entityName The entity or module name
     * @param array|object $data       The data to be appended
     * @param array        $context    The serializer context to be used
     * @param bool         $chained    If <var>true</var> then it will be logged to the database
     */
    public function info(string $message, string $entityName, $data = null,
                         array $context = [], bool $chained = true): void
    {
        $this->log(LogLevel::INFO, $message, $entityName, $data, $context, $chained);
    }

    /**
     * Logs with an arbitrary level. This type of log level can be logged to the database.
     *
     * @param string       $level      Describe the {@see LogLevel}
     * @param string       $message    The log message
     * @param string       $entityName The entity or module name
     * @param array|object $data       The data to be appended
     * @param array        $context    The serializer context to be used
     * @param bool         $chained    If <var>true</var> then it will be logged to the database
     */
    public function log(string $level, string $message, string $entityName = null,
                        $data = null, array $context = [], bool $chained = true): void
    {
        $_context = $context;
        if (!empty($data)) {
            if (!empty($context)) {
                $_context = ['groups' => $context];
            }
            $json = $this->serializer->serialize($data, 'json', array_merge(
                                                          ['json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS],
                                                          $_context)
            );
        }

        if (true === $chained) {
            $msg = $message . (isset($json) ? ' ' . PHP_EOL . $json : '');
            $entity = new AuditLog();
            $entity->setLevel($level)
                   ->setMessage($msg)
                   ->setEntityName($this->shortClassName($entityName));
            $this->em->persist($entity);
            $this->em->flush();
        }

        $this->logger->log($level, $message, (isset($json) ? json_decode($json, true) : []));
    }

    /**
     * Log for normal but significant events.
     * This type of log level can be logged to the database.
     *
     * @param string       $message    The log message
     * @param string       $entityName The entity or module name
     * @param array|object $data       The data to be appended
     * @param array        $context    The serializer context to be used
     * @param bool         $chained    If <var>true</var> then it will be logged to the database
     */
    public function notice(string $message, string $entityName, $data = null,
                           array $context = [], bool $chained = true): void
    {
        $this->log(LogLevel::NOTICE, $message, $entityName, $data, $context, $chained);
    }

    /**
     * Log for Exceptional occurrences that are not errors.
     * This type of log level can be logged to the database.
     *
     * @param string       $message    The log message
     * @param string       $entityName The entity or module name
     * @param array|object $data       The data to be appended
     * @param array        $context    The serializer context to be used
     * @param bool         $chained    If <var>true</var> then it will be logged to the database
     */
    public function warning(string $message, string $entityName, $data = null,
                            array $context = [], bool $chained = true): void
    {
        $this->log(LogLevel::WARNING, $message, $entityName, $data, $context, $chained);
    }

    private function shortClassName(string $clazz): string
    {
        $_cls = explode('\\', $clazz);
        $_len = count($_cls);

        return $_cls[$_len - 1];
    }

}