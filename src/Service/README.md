### Direktori: src\Service

Direktori ini dipergunakan untuk script PHP yang berhubungan dengan **Service/Dependency Injection**.
Direktori sebaiknya disusun dalam beberapa sub-direktori yang disesuaikan dengan topik 
task process yang dilakukan oleh service tersebut.

Contoh struktur direktori:
```text
-- Service
   |--- Security
   |--- Uploadable
   |--- Notification
```