<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Annotation;


use Doctrine\Common\Annotations\Annotation;

/**
 * Class UserAgent
 *
 * @Annotation
 * @Target("PROPERTY")
 *
 * @package App\Service\Annotation
 * @author  Ahmad Fajar
 * @since   19/08/2018, modified: 10/04/2019 18:16
 */
final class UserAgent extends Annotation
{
    /** @var string */
    public $on = 'create';

    /** @var string|array */
    public $field;

    /** @var mixed */
    public $value;

}