<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Annotation\UserAgent\Mapping\Driver;


use Gedmo\Exception\InvalidMappingException;
use Gedmo\Mapping\Driver\AbstractAnnotationDriver;

/**
 * Class UserAgentAnnotation
 *
 * @package App\Service\Annotation\UserAgent\Mapping\Driver
 * @author  Ahmad Fajar
 * @since   19/08/2018, modified: 10/04/2019 18:16
 */
class Annotation extends AbstractAnnotationDriver
{

    /**
     * Annotation field for UserAgent
     */
    const USER_AGENT = 'App\\Service\\Annotation\\UserAgent';

    /**
     * List of types which are valid for UserAgent
     *
     * @var array
     */
    protected $validTypes = array(
        'string',
        'text',
    );

    /**
     * Read extended metadata configuration for a single mapped class.
     *
     * @param object $meta
     * @param array  $config
     *
     * @return void
     */
    public function readExtendedMetadata($meta, array &$config)
    {
        $class = $this->getMetaReflectionClass($meta);
        // property annotations
        foreach ($class->getProperties() as $property) {
            if ($meta->isMappedSuperclass && !$property->isPrivate() ||
                $meta->isInheritedField($property->name) ||
                isset($meta->associationMappings[$property->name]['inherited'])
            ) {
                continue;
            }

            if ($userAgent = $this->reader->getPropertyAnnotation($property, self::USER_AGENT)) {
                $field = $property->getName();

                if (!$meta->hasField($field)) {
                    throw new InvalidMappingException("Unable to find UserAgent [{$field}] as mapped property in entity - {$meta->name}");
                }
                if ($meta->hasField($field) && !$this->isValidField($meta, $field)) {
                    throw new InvalidMappingException("Field - [{$field}] type is not valid and must be 'string' - {$meta->name}");
                }
                if (!in_array($userAgent->on, array('update', 'create', 'change'))) {
                    throw new InvalidMappingException("Field - [{$field}] trigger 'on' is not one of [update, create, change] in class - {$meta->name}");
                }
                if ($userAgent->on == 'change') {
                    if (!isset($userAgent->field)) {
                        throw new InvalidMappingException("Missing parameters on property - {$field}, field must be set on [change] trigger in class - {$meta->name}");
                    }
                    if (is_array($userAgent->field) && isset($userAgent->value)) {
                        throw new InvalidMappingException("UserAgent extension does not support multiple value changeset detection yet.");
                    }
                    $field = array(
                        'field'        => $field,
                        'trackedField' => $userAgent->field,
                        'value'        => $userAgent->value,
                    );
                }
                $config[$userAgent->on][] = $field;
            }
        }
    }

}