<?php
/*
 * Web aplikasi Adaro Education Program built with Symfony4.
 *
 * Copyright (C) 2018 - Ahmad Fajar
 */

namespace App\Service\Annotation\UserAgent;


use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Gedmo\AbstractTrackingListener;
use Gedmo\Mapping\Event\AdapterInterface;

/**
 * Class UserAgentListener
 *
 * @package App\Service\Annotation\UserAgent
 * @author  Ahmad Fajar
 * @since   19/08/2018, modified: 19/08/2018 01:58
 */
class UserAgentListener extends AbstractTrackingListener
{

    /**
     * @var string
     */
    private $userAgent;

    /**
     * Sets remote/client user agent name.
     *
     * @param null|string $name
     */
    public function setUserAgent(?string $name): void
    {
        $this->userAgent = $name;
    }

    /**
     * Get value for update field.
     *
     * @param ClassMetadata    $meta
     * @param string           $field
     * @param AdapterInterface $eventAdapter
     *
     * @return null|string
     */
    protected function getFieldValue($meta, $field, $eventAdapter): ?string
    {
        return $this->userAgent;
    }

    /**
     * Get the namespace of extension event subscriber.
     * used for cache id of extensions also to know where
     * to find Mapping drivers and event adapters
     *
     * @return string
     */
    protected function getNamespace()
    {
        return __NAMESPACE__;
    }

}